\version "2.23.82"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Moving in Stereo"
  subtitle = "The Cars - Year - Album"
  poet = "use octaver, pick"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 2
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    %\override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=100
  %\omit Voice.StringNumber
  \compressEmptyMeasures
  \override MultiMeasureRest.expand-limit = 1
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*3
  r1\fermata
  R1*9
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r |
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  c'1 ~ |
  c2. ~ c8. (e,16) |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r |
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  c'2 ~ c8 c16 g c4 ~ |
  c2 ~ c |
  \break
  b2 fis' |
  a,4 r8 b^\markup{bend to C} b a\staccato e'4 |
  r8 b^\markup{bend to C} b a e'\staccato c b a |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r |
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  r4 c' ~ c4. c16 b |
  a2. \tuplet 3/2 {r8 d,-> dis->} |
  \break
  e4 r e4 r | % 32
  e r r8 d\glissando fis4 |
  \repeat volta 2 {
    e4 r e r |
    e r r8 d\staccato e16 (fis8.) |
  }
  \break
  e4 r e r | %  36
  c'2 ~ c4. c16 b |
  a2. \tuplet 3/2 {r8 d,-> dis->} |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r | % 39
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  c'2 ~ c4. c16 b |
  a2. r8 d,16 dis |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r | % 44
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  c'2 ~ c4. c16 b |
  a2. r8 d,16 dis |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r | % 49
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  c'2 ~ c4. c16 b |
  a2. r8 d,16 dis |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r | % 54
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  c'2 ~ c4. c16 b |
  a2. r8 d,16 dis |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r | % 59
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  \break
  c'1 ~ |
  c1 |
  c2. \grace d32 dis4 |
  b2 fis'4 b,8 fis'\glissando|
  \break
  b8\staccato b\staccato a\staccato a\staccato g\staccato g\staccato fis\staccato fis\staccato |
  g\staccato g\staccato fis\staccato fis\staccato e\staccato  e\staccato  d\staccato d\staccato |
  b\staccato b\staccato a\staccato a\staccato g\staccato g\staccato fis\staccato fis\staccato |
  g\staccato g\staccato fis\staccato fis\staccato e\staccato e\staccato d\staccato d\staccato |
  \break
  \repeat volta 3 {
    e4 r e r | % 54
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r r8 d\staccato e16 (fis8.) |
  \break
  \repeat volta 3 {
    e4^\markup{\bold "play 3x"} r e r | % 73
    e r r8 d8\staccato e16 (fis8.)|
  }
  e4 r e r |
  \break
  c'2 ~ c8 g c4 ~ |
  c ~ c8. g16 c2 |
  c4. c8 ~ c8. g16 c4 ~ |
  c ~ c8. g16 c4 ~ c8. g16 |
  b1\fermata
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }
 electricBassPart = \new StaffGroup <<
   \new Staff { \clef "bass_8" \electricBass }
   \new TabStaff \with {
     stringTunings = #bass-tuning
   } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
