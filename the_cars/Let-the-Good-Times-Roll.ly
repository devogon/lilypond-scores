\version "2.19.84"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Let the Good Times Roll"
  subtitle = "(The Cars - 1978)"
 % instrument = "Bass"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=103
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*4
  R1*8
  %%\break
  g8 g r4 g8 g r gis |
  a8 a r4 a8 a r fis
  g8 g r4 g8 g r gis |
  a8 a r4 a8 a r4 |
  %%\break
  b8\staccato b8 r4 b8\staccato b8 r4 |
  b8\staccato b8 r4 b8\staccato b8 r a |
  g8\staccato g r4 g8\staccato g r4 |
  g8\staccato g r4 g8\staccato g r fis\glissando |
  %%\break
  e'4. b8 ~ b4 e ~ |
  e1 |
  %%\break
  \repeat percent 3 {g,8\staccato g r4 g8\staccato g r4} |
  b8 d cis4\staccato b8 d cis4 |
  %\break
  g8\staccato g r4 g8\staccato g r4 |
  a8\staccato a r4 a8\staccato a r4 |
  \repeat percent 3 {b8\staccato dis e dis b\staccato dis e dis} |
  %\break
%  b8\staccato dis e dis b\staccato dis e dis
  a\staccato dis e dis a\staccato dis e dis |
  \repeat percent 2 {b8\staccato dis e dis b\staccato dis e dis} |
  %\break
  g,8 g r4 g8 g r gis |
  a8 a r4 a8 a r fis
  g8 g r4 g8 g r gis |
  a8 a r4 a8 a r4 |
  %\break
  b8\staccato b r4 b8\staccato b r4  |
    b8\staccato b r4 b8\staccato b a\staccato a |
    g8\staccato g r4 g8\staccato g r4 |
    g8\staccato g r4 g8\staccato g r g\glissando |
    e'4. b'8 ~ b4 e,4 ~ |
    e1 |
    %\break
    b8\staccato b r4 b8\staccato b r4 |
    b8\staccato b r4 b8 b fis' (g) |
    a,8\staccato a r4 a8\staccato a gis (a) |
    a8\staccato a r4 a8\staccato a dis e |
  %\break
    % end page 1
  g,8\staccato g r4 g8\staccato g r4 |
  g8\staccato g r4 g8\staccato g  r g\glissando |
  e'4. b'8 ~ b4 e, ~ |
  e1 |
  %\break
  \repeat percent 3 {b8\staccato b r4 b8\staccato b r4} |
  b8 d cis4\staccato r8 d cis4 |
  %\break
  b8\staccato b r4 b8\staccato b r4 |
  a8\staccato a r4 a8\staccato a r4 |
  \repeat percent 3 {b8\staccato dis e dis b\staccato dis e dis} |
  %b8\staccato dis e dis b\staccato dis e dis |
  %\break
  %b8\staccato dis e dis b\staccato dis e dis |
  a8\staccato dis e dis a\staccato dis e dis |
  \repeat percent 2 {b8\staccato dis e dis b\staccato dis e dis} |
  %\break
  g,8 g r4 g8 g r4 | % 65
  a8 a r4 a8 a\grace g'16 a8 (g) |
  g,8 g r4 g8 g r4 |
  a8 a r4 a8 a r4 |
  %\break
  b8\staccato b r4 b8\staccato b8 r4 | % 69
  b8\staccato b r4 b8\staccato b a\staccato a |
  g8\staccato g r4 g8\staccato g r4 |
  g8\staccato g r4 g8\staccato g r g\glissando |
  e'4. b8 ~ b4 e ~ |
  e1 |
  %\break
  b8\staccato b r4 b8\staccato b r4 | % 75
  b8\staccato b r4 b8\staccato b fis' (g) |
  a,8\staccato a r4 a8\staccato a r4 |
  a8\staccato a r4 a8\staccato gis' (a4) |
  %\break
  g8\staccato g r4 g8\staccato g r4 | % 79
  g8\staccato g r4 g8\staccato g r g8\glissando |
  e4. b'8 ~ b4\glissando e,4 ~ |
  e1 |
  %\break
  b8\staccato b r4 b8\staccato b r4 | % 83
  b8\staccato b r4 b8\staccato b r a |
  a8\staccato a r4 a8\staccato a r4 |
  a8\staccato a r4 a8\staccato a r g |
  %\break
  g8\staccato g r4 g8\staccato g r4 | % 87
  g8\staccato g r4 g8\staccato g r g\glissando |
  e'4. b'8 ~ b4 e,4 ~ |
  e1 |
  %\break
  g,8\staccato g r4 g8\staccato g  r4 | % 91
  g8\staccato g r4 g8\staccato g  r gis |
  a8\staccato a r4 a8\staccato a r4 |
  a8\staccato a r4 a8\staccato a r gis |
  %\break
  g8\staccato g r4 g8\staccato g  r4 | % 95
  g8\staccato g r4 g8\staccato g  r4 |
  e'4. b'8 ~ b4\glissando e,4 ~ |
  e2\grace d'16 e4\grace a,16 b4 |
  e,1\fermata
  \bar "|."
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
  %   \new TabStaff \with {
%       stringTunings = #bass-tuning
%     } \electricBass
  >>
  \layout { }
}
