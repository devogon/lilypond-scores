\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Moondance"
  composer = "Van Morrison"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
  \tempo " " 4=120
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = {
   \global
  a4 a b e | % 1
  a4 e b e | % 2
  a8 a ais4 b  b | % 3
  c'4 b b e | % 4
  \break
  a8 a ais4 b e | % 5
  a4 ais4 b e | % 6
  a4 ais b e8 g | % 7
  c'4 b8 a b4 e8 g | % 8
  a8 a ais4 b e | % 9
  \break
  a4 ais b e8 g | % 10
  a8 a ais4 b e | % 11
  a4 fis e c | % 12
  a8 a ais4 b ais8 b | % 13
  c'4 b a8 e g e | % 14
  \break
  a8 a ais4 b ais8 b | % 15
  c'4 a e' d'8 b | % 16
  a8 a ais4 b e8 g | % 17
  a4 fis b e8 g | % 18
  \break
  a8 a ais4 b a!8 b | % 19
  c'4 b g e | % 20
  d4 f g gis | % 21
  a8 a a,4 b, c | % 22
  d4 e f g | % 23
  \break
  a8 a 	a,4 c cis | % 24
  d4 e f g | % 25
  a8 a a,4 b, c | % 26
  d4 d d r | % 27
  e8 e e4 e,8 g, a,4 | % 28
  \break
  a,8 c d4 e f8 a | % 29
  a8 c' d'4 e,8 g, a,4 | % 30
  r8 a, c d r e g a | % 31
  a8 c' d'4 e,8 g, a,4 | % 32
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
  \hspace #1
}

\markup {
  \teeny
  \date
}