\version "2.19.57"
\language "nederlands"

\header {
  title = "Libertango"
  instrument = "Bass"
  composer = "Astor Piazzolla"
  arranger = "Marcel den Os"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo "Tango" 4=144
}

contrabass = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-numbers
  % Music follows here.
  \override NoteHead.style = #'cross
  \repeat volta 2 {
  \mark \default
  d4.\f^\markup{Claps} d8\staccato (d4) d4\staccato | % 1
  d4.\staccato d8\staccato (d4) d4\staccato | % 2
  }
  \mark \default
  d4.\staccato d8\staccato (d4) d4\staccato | % 3
  d4.\staccato d8\staccato (d4) d4\staccato | %
  d4.\staccato d8\staccato (d4) d4\staccato | %
  d4.\staccato d8\staccato (d4) d4\staccato | %
  d4.\staccato d8\staccato (d4) d4\staccato | %
  d4\staccato r r2
  \mark \default
  \bar "||"
    \revert NoteHead.style
    r2 r4\mf e16 e16 e16 e16 | % 9
    e4\marcato r r2 | % 10
    r2 r4 e16 e16 e16 e16 |% 11
}

\score {
  \new Staff \with {
    instrumentName = "Contrabass"
    shortInstrumentName = "Cb."
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}
