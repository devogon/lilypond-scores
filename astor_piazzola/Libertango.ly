\version "2.19.81"
\language "nederlands"

date = \markup {\small{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Libertango"
  instrument = "Bass"
  composer = "Astor Piazzolla"
  arranger = "James Kazik"
%  meter = "Tango"
  copyright = \markup{\small "1975 Edizioni Curci S.r.l. Milan and..."}
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
    indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo "Tango"  4=144
}

contrabass = \relative c {
  \global
  \repeat volta 2 {
  a4.\mf^\markup{\italic pizz.} e'8 ~ e4 a, | % 1
  a4. e'8 ~ e4 a, | % 2
  a4. fis'8 ~ fis4 a, | % 3
  a4. fis'8 ~ fis4 a, | % 4
  a4. f'8 ~ f4 a, | % 5
  a4. f'8 ~ f4 a, | % 6
  \break
  a4. e'8 ~ e4 a, | % 7
  a4. e'8 ~ e4 a, | % 8
  \mark \markup{\box 9}
  g4. e'8 ~ e4 g, | % 9
  g4. e'8 ~ e4 g, | % 10
  fis4. c'8 ~ c4 b | % 11
  fis4. c'8 ~ c4 b | % 12
  \break
  f4. c'8 ~ c4 b | % 13
  f4. c'8 ~ c4 b | % 14
  e,4. b'8 ~ b4 e, | % 15
  }
  \alternative {
    {e4. b'8 ~ b4 e,} % 16
    {e4. b'8 ~ b4 e,} % 17
  }
  \mark \markup{\box 18}
  a4. a'8 ~ a4 a, | % 18
  \break
  a4. a'8 ~ a4 a, | % 19
  a4. fis'8 ~ fis4 a, | % 20
  a4. fis'8 ~ fis4 a, | % 21
  a4. f'8 ~ f4 a, | % 22
  a4. f'8 ~ f4 a, | % 23
  a4. e'8 ~ e4 a, | % 24
  a4. e'8 ~ e4 a, | % 25
  \break
  \mark \markup{\box 26}
  g4. e'8 ~ e4 g, | % 26
  g4. e'8 ~ e4 g, | % 27
  fis4. c'8 ~ c4 b | % 28
  fis4. c'8 ~ c4 b | % 29
  f'4. c'8 ~ c4 b | % 30
  f4. c'8 ~ c4 b | % 31
  \break
  e,4. b'8 ~ b4 e, | % 32
  e4. b'8 ~ b4 e, | % 33
  \mark \markup{\box 34}
  bes4.\f bes8 ~ bes4 bes | % 34
  a4. a8 ~ a4 a | % 35
  d4. a8 ~ a4 a | % 36
  d4. d8 ~ d4 a | % 37
  \break
  as4. as8 ~ as4 as | % 38
  g4. g8 ~ g4 g | % 39
  c4. c8 ~ c4 c | % 40
  b4. f'8 ~ f4 e | % 41
  \mark \markup{\box 42}
  a,4. a8 ~ a4 a | % 42
  g4. g8 ~ g4 g | % 43
  \break
  fis4. fis8 ~ fis4 fis | % 44
  b4. b8 ~ b4 b | % 45
  f4. c'8 ~ c4 b | % 46
  e,4. b'8 ~ b4 e, | % 47
  a4. e'8 f4 e | % 48
  a8\mp\< g f e d c b a\! | % 49
  \break
  \mark \markup{\box 50}
  \repeat volta 2 {
    a4. e'8 ~ e4 a, | % 50
    a4. e'8 ~ e4 a, | % 51
    a4. fis'8 ~ fis4 a, | % 52
    a4. fis'8 ~ fis4 a, | % 53
    a4. f'8 ~ f4 a, | % 54
    a4. f'8 ~ f4 a, | % 55
    \break
    a4. e'8 ~ e4 a, | % 56
  }
  \alternative {
    {a4. e'8 ~ e4 a, }
    {a4. e'8 ~ e4 a, }
  }
  \mark \markup{\box 59}
  \repeat volta 2 {
    a4. e'8 ~ e4 a, | % 59
    a4. e'8 ~ e4 a, | % 60
    a4. fis'8 ~ fis4 a, | % 61
    a4. fis'8 ~ fis4 a, | % 62
    \break
    a4. f'8 ~ f4 a, | % 63
    a4. f'8 ~ f4 a, | % 64
    a4. e'8 ~ e4 a, | % 65
  }
  \alternative {
    {a4. e'8 ~ e4 a, | } % 66
    {a'8\mf\< g f e d c b gis\! | } % 67
  }
  a8->\ff a-> a-> r r2 | % 68
  \bar "|."
}

\score {
  \new Staff \with {
 %   instrumentName = "Contrabass"
 %   shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}
