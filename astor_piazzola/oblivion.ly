\version "2.19.30"
\language "nederlands"
				% Oblivion
				% by Astor Piazzolla
				% arranged by Robert Longfield
				% 04491250
				% c 1984 by A. Pagani S.r.l. Ediioni Musicali, Fino Mornasco (CO) Italy
				% This arrangement c 2014 by A. Pagani S.r.l. Edizioni Musicali, Fino Mornasco (CO) Italy
				% Rightts for usa controlled by Stella Solaris Music
				% All rights reserverd used by permission

date = \markup \tiny {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Oblivion"
%  instrument = "Bass"
  composer = "Astor Piazzolla"
%  arranger = "Robert Longfield"
  copyright = \markup {\tiny "1984 A. PAgani S.r.l Edizioni Musicali, Fino Mornasco (CO) Italy"}
				% Remove default LilyPond tagline
  tagline = \date
%  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  system-system-spacing =
    #'((basic-distance . 10)
       (minimum-distance . 10)
       (padding . -1)
       (stretchability . 70))
}

\layout {
  indent = 0.0\cm
  #(layout-set-staff-size 16)
    ragged-last-bottom = ##t
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



global = {
  \key f \major
  \time 4/4
  \tempo "Slowly" % 4=60
}

contrabass = \relative c {
  \global
%  \markup{\bold "play this a bit louder than the rest of the bass section."}
  d4\mp^\markup{\italic "pizz."} r8 d'8 (d4) d, | % 1
  d4 r8 d'8 (d4) d, | % 2
  d4 r8 d'8 (d4) d, | % 3
  d4
  r8 d'8 (d4) d, | % 4
      \mark \markup \center-column { \box 5}
  d4 r8 d'8 (d4) d, | % 5
  d4 r8 d'8 (d4) d, | % 6
  d4 r8 d'8 (d4) d, | % 7
%  \break
  d4\mp r8 d'8\< (d4) d,\! | % 8
  g,4\mp r8 g' c,4 c | % 9
  f,4 r8 f' bes,4 bes | %  10

  e,4 r8 e' e4 e | % 11
  a4 r8 a a4\> a, | % 12
  \mark \markup \center-column { \box 13}
  d4\!\pp r8 d' (d4) d, | % 13
  d4 r8 d' (d4) d, | % 14
%  \break
  d4 r8 d' (d4) d, | % 15
  d4 r8 d'\< (d4) d, | % 16
  a'4\!\mp r8 a a4 r | % 17
  d,4 r8 d d4 r | % 18
  g4 r8 g g4 r | % 19
  f4 r8 f f4 r | % 20

  \mark \markup \center-column { \box 21}
  e4\mf r8 g g4 e | % 21
%  \break
  a,4 r8 e' e4 a, | % 22
  d4 r8 d d4 d  | % 23
  c4 r8 c c4 c | % 24
  b!4 r8 b b4 b | % 25
  a4 r8 a a4 a | % 26

  d r8 d'\< (d4) d,\! | % 27
  d4 r8 d'\> (d4) d,\! | % 28
%  \break
  \mark \markup \center-column { \box 29}
  g,4\mp\downbow^\markup{\italic "arco"} r8 d'\downbow (d4) g, | % 29
  c4 r8 c' c4 c, | % 30
  f4 r8 f\downbow (f4) c | % 31

  bes4 r8 bes' bes4 bes, | % 32
  e4 r8 e\downbow (e4) e | % 33
  a,4 r8 a' a4 a, | % 34
  d4 r8 d\downbow (d4) f | % 35
%  \break
  fis4 r8 fis\< fis4 d\! | % 36

  \mark \markup \center-column { \box 37}
  g4\f\downbow r8 g\downbow (g4) g, | % 37
  c4 r8 c' c4 c, | % 38
  f4 r8 f\downbow (f4) c | % 39
  bes4 r8 bes' bes4 bes, | % 40
  e4 r8 e\downbow (e4) e | % 41
  a,4 r8 a' a4 a, | % 42
  d4\downbow d2 d4 | % 43
%    \break
  d4 d2\> r4\! | % 44
  d4\p^\markup{\italic "pizz."} r8 d' (d4) d, | % 45
  d4 r8 d' (d4) d, | % 46
  \mark \markup \center-column { \box 47}
  d4 r8 d' (d4) d, | % 47
  d4 r8 d' (d4) d, | % 48

  d4 r8 d' (d4) d, | % 49
  d4 r8 d'\< (d4) d,\! | % 50
%  \break
  a4\mp r8 g' c,4 c | % 51
  f,4 r8 f' bes,4 bes | % 52
  e,4 r8 e' e4 e | % 53
  a4 r8 a\> a4 a,\! | % 54

  \mark \markup \center-column { \box 55}
  d4\pp r8 d' (d4) d, | % 55
  d4 r8 d' (d4) d, | % 56
  d4 r8 d' (d4) d, | % 57
%  \break
  d4 r8 d'\< (d4) d,\! | % 58
  a'4\mp r8 a a4 r | % 59
  d,4 r8 d d4 r | %  60

  g4 r8 g g4 r  | % 61
  f4 r8 f f4 r | % 62
  \mark \markup \center-column { \box 63}
  e4\mf r8 g g4 e | % 63
  a,4 r8 e' e4 a, | % 64
%  \break
  d4 r8 d d4 d | % 65
  c4 r8 c c4 c | % 66

  b!4 r8 b b4 b | % 67
  a4 r8\> a a4 r\! | % 68
  \mark \markup \center-column { \box 69}
  d2.\downbow\mp\tenuto^\markup{\italic "arco"} (d4) | % 69
  c2._\markup{\italic "dim. poco a poco"} (c4)\tenuto | % 70
  bes2. (bes4)\tenuto | % 71
  a2.\>_\markup{\italic "molto rit."} (a4)\tenuto | % 72
  << d1\pp\fermata { s4 s\> s s\! } >> | % 73
  }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Contrabass"
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout {
    \context {
      \Score
      \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16)
      % http://lilypond.org/doc/v2.18/Documentation/notation/changing-horizontal-spacing
    }
  }
  \midi { }
}
% \markup {
%   \teeny
%   \date
% }