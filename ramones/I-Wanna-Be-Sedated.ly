\version "2.19.54"
\language "nederlands"

\header {
  title = "I Wanna Be Sedated"
  instrument = "Bass"
  composer = "Joey Ramone (Ramones)"
  piece = "(Road to Ruin)"
  copyright = "September 1978, Sire Records"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=164
}

electricBass = \relative c, {
  \global
  % Music follows here.
  dis8 e e e e e e e  | % 1
  e8 e e e e e e e | % 2
  dis8 e e e e e e e | % 3
  \repeat percent 2 {e e e e e e e e} % 4-5
  e8 e e e e e e a, | % 6
  a4 a8 a a a a a | % 7
  \repeat percent 2 {e' e e e e e e e} % 8-9
  e8 e e e e e e a, | % 10
  a8 a a a a a a a | % 11
  e'4 e8 e e e e a, | % 12
  b4 b8 b b b b b | % 13
  e4 e8 e e e e e | % 14
  b4 b8 b b b b b | % 15
  e4 e8 e e e e e | % 16
  b4 b8 b b b b b | % 17
  e4 e8 e e e e e | % 18
  a,4 a8 a a a a a | % 19
  b4 b8 b b b b e, | % 20
  \repeat percent 3 { e8 e e e e e e e } % 21-23
  e'8 e e e e e e a, | % 24
  a4 a8 a a a a a | % 25
}

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
