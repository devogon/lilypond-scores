\version "2.19.54"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "I Wanna Be Sedated"
  subtitle = "vocal melody"
  instrument = "Bass"
  composer = "Joey Ramone (Ramones)"
  piece = "(Road to Ruin)"
  copyright = "September 1978, Sire Records"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key e \major
  \time 4/4
  \tempo 4=164
}

electricBass = \relative c {
  \global
  % Music follows here.
  b8 b b b b b b4|
  b8 b cis b ~ b a gis4 |
  r8 a a gis fis4 e8 fis ~ |
  fis8 gis4. r2 |
  gis8 gis gis gis ~ gis gis4 gis8 ~ |
  gis8 fis gis4 fis16 e8. r4 |
  r8 a8 a gis fis4 e8 fis8 ~ |
  fis8 gis4. r2 |
  \bar "|."
}

words = \lyricmode {
    twen- ty twen- ty twen -ty four hours to g- o- o- o
    i wan- na be se da- ted
    no- thin to do_ no where to go_ _ _
    i  wa- nna be se da- ted
}
\score {
  \new Staff { \clef "bass_8" \electricBass }
  \addlyrics { \words }
  \layout { }
}
