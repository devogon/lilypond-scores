\version "2.23.13"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}


\header {
  title = "I Wanna Be Sedated"
   instrument = "Bass"
  composer = "Joey Ramone (Ramones)"
  piece = "(Road to Ruin)"
  copyright = "September 1978, Sire Records"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=163
}

aeighths = {a8\2 a\2 a\2 a\2 a\2 a\2 a\2 a\2 }
bhighquarter = {b'4\2 b8\2 b\2 b\2 b\2 b\2 b\2 }
bhigheighths = {b8\2 b\2 b\2 b\2 b\2 b\2 b\2 b\2 }
eeighths = {e,8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 }


electricBass = \relative c, {
  \global
  \omit Voice.StringNumber
  % Music follows here.
   dis8\3 (e\3) e\3 e\3 e\3 e\3 e\3 e\3  | % 1
  e8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 2
  dis8\3 (e\3) e\3 e\3 e\3 e\3 e\3 e\3 | % 3
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 4
  \break
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 5
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 6
  a4\2 a8\2 a\2 a\2 a\2 a\2 a\2 | % 7
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 8
  \break
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 9
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 10
  a4\2 a8\2 a\2 a\2 a\2 a\2 a\2 | % 11
  e4\3 e8\3 e\3 e\3 e\3 e\3 e\3 | % 12
  \break
  b'4\2 b8\2 b\2 b\2 b\2 b\2 b\2 | % 13
  e,8\3 e8\3 e8\3 e\3 e\3 e\3 e\3 e\3 | % 14
  b'8\2 b8\2 b\2 b\2 b\2 b\2 b\2 b\2 | % 15
  e,8\3 e8\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 16
  \break
  b'4\2 b8\2 b\2 b\2 b\2 b\2 b\2 | % 17
  e,4\3 e8\3 e\3 e\3 e\3 e\3 e\3 | % 18
  a8\2 a\2 a\2 a\2 a\2 a\2 a\2 a\2 | % 19
  b\2 b8\2 b\2 b\2 b\2 b\2 b\2 b\2 | % 20
 \break
  dis,8\3 (e\3) e\3 e\3 e\3 e\3 e\3 e\3  | % 21
  e8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 22
  e8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 23
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 24
  \break
  a4\2 a8\2 a\2 a\2 a\2 a\2 a\2 | % 25
  e\3 e8\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 26
  e\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 27
  e\3 e\3 e\3  e\3 e\3 e\3 e\3 e\3 | % 28
  \break
  a4\2 a8\2 a\2 a\2 a\2 a\2 a\2 | % 29
  e4\3 e8\3 e\3 e\3 e\3 e\3 e\3 | % 30
  b'\2 b\2 b\2 b\2 b\2 b\2 b\2 b\2 | % 31
  e,\3 e8\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 32
  \break
  b'\2 b\2 b\2 b\2 b\2 b\2 b\2 b\2 | % 33
  e,\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 | % 34
  b'4\2 b8\2 b\2 b\2 b\2 b\2 b\2 | %  35
  e,4\3 e8\3 e\3 e\3 e\3 e\3 e\3 | % 36
  \break
  \aeighths % 37
  \bhigheighths % 38
  \eeighths %  39
  \aeighths % 40
  \break
  b8\2 b\2 b\2 b\2 b\2 b\2 b\2 b\2 | % 41
  \eeighths | % 42
  e8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3  % 43
  \aeighths % 44
  \break
  b8\2 b\2 b\2 b\2 b\2 b\2 b\2 b\2 % 45
    e,8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3  % 46
    dis8\3 (e\3) e\3 e\3 e\3 e\3 e\3 e\3  | % 47
    e8\3 e\3 e\3 e\3 e\3 e\3 e\3 e\3 ( | % 48
    \break
  ges\3) ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 49
  ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3  | % 50
  b4\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 | % 51
  ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 52
  \break
  ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 53
  ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 54
  b4\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 |% 55
  ges4\3 ges8\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 56
  \break
  des'8\2 des\2 des\2 des\2 des\2 des\2 des\2 des\2 | % 57
  ges,\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 58
  des'8\2 des\2 des\2 des\2 des\2 des\2 des\2 des\2 | % 59
  ges,\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 60
  \break
  des'4\2 des8\2 des\2 des\2 des\2 des\2 des\2 | % 61
  ges,4\3 ges8\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 62
  b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2  | % 63
  des8\2 des8\2 des\2 des\2 des\2 des\2 des\2 des\2 | % 64
  \break
  f,\3 ges8\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 65
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 66
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 67
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 68
  \break
  b4\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2  | % 69
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 70
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 71
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 72
  \break
  b4\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 | % 73
  ges4\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 74
  des'8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 | % 75
  ges,8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 76
  \break
   des'8\2 des\2 des\2 des\2 des\2 des\2 des\2 des\2 | % 77
  ges,\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 78
  des'8\2 des\2 des\2 des\2 des\2 des\2 des\2 des\2 | % 79
  ges,\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 ges\3 | % 80
  \break
  b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 | % 81
  des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 | % 82
  ges,8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 83
  b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2  | % 84
  \break
  des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2  | % 85
  ges,8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 86
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 | % 87
  b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 | % 88
  \break
  des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 | % 89
  ges,8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 90
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 91
  b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2  | % 92
  \break
  des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 | %  93
  ges,8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 94
  ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3 ges8\3  | % 95
  b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 b8\2 | % 96
  \break
  des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 des8\2 | % 97
  ges,8\3 s s s s s s s % 98
  s s s s s s s s
  s s s s s s s s
  \bar "|."
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout { }
}

% sources
% https://www.youtube.com/watch?v=s4J1rX0v-lA
% https://www.bigbasstabs.com/ramones_bass_tabs/i_wanna_be_sedated_fixed.html
% https://www.songsterr.com/a/wsa/ramones-i-wanna-be-sedated-bass-tab-s18631
