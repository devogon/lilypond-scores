\version "2.23.8"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

% #(define bars-per-line 4)
%
% #(define line-break-every-nth-engraver
% (lambda (context)
% (make-engraver
% (acknowledgers ((paper-column-interface engraver grob
% source-engraver)
% (let ((current-bar (ly:context-property context 'currentBarNumber)))
% (if (= (remainder current-bar bars-per-line) 1)
% (if (eq? #t (ly:grob-property grob 'non-musical))
% (set! (ly:grob-property grob 'line-break-permission) 'force)
% (set! (ly:grob-property grob 'line-break-permission) '())))))))))

\header {
  title = "Foreplay/Longtime"
  subtitle = "Boston - Boston - 1976"
  composer = "Words/Music: Tom Scholz"
  poet = "bass: Fran Sheehan"
  tagline = \date
}

\paper { indent = 0.0\cm }

global = {
  \key ges \major
  \time 4/4
}

electricBass = \relative c, {
  \global
  \compressEmptyMeasures

  % Music follows here.
  R1*8
  \break
  bes4\4\f r r2 |
  bes4\4 r r2 |
  bes1\4 ~ |
  bes1\4 ~ |
  bes1\4 ~ |
  bes1\4 |
  R1*3
  r4 r2 bes'4\3\glissando
  \break
  bes,4 c!4 des c |
  bes as g! f |
  c'! des bes es |
  f f, f' f |
  \break
  bes, c! des c! |
  bes as g! f |
  c'! des bes es |
  f1|
  \break
  bes,4 c! bes c! |
  des as des as |
  des as bes f |
  ges des' es f |
  \break
  bes,2. as4 |
  des2. as4 |
  bes2.\4 c!4|
  des2. as4 |
  \break
  bes\4 as des as |
  bes c! des as |
  bes as ges es' |
  f f f f |
  \break
  bes,1 ~
  bes ~
  bes1 ~
  bes2.. as8 |
  R1*4
  \break
  bes4 c! des c!
  bes as g! f |
  c'! des bes es |
  f1 |
  \break
  bes,4 c! bes c! |
  des as des as |
  des as bes f |
  ges des' es f |
  \break
  bes,2. as4 |
  des2. as4 |
  bes2.\4 c!4|
  des2. as4 |
  \break
  bes\4 as des as |
  bes\4 c! des as |
  bes\4 as ges es'\3 |
  f f f f |
  \break
  bes,1 ~
  bes1
  c!1 ~
  c!
  \break
  c!1 ~
  c!
  c!1 ~
  c!
  \break
  c!1 ~
  c!
  c!1 ~
  c!
  \break
  c!1 ~
  c!
  \bar "||"
  \mark \markup{\right-align "Interlude to Longtime"}
  R1*2
  \bar "||"
  \break
  \mark \markup{Intro}
  r1
  \key es \major
  R1*4
  \break
  c4\4^\markup{2:23} c\4 c\4 c\4
  c4\4 c\4 c\4 c\4 |
  c4\4 c\4 c\4 c\4 |
  c4\4 c\4 c8\4\mf a!\4 c4\4\glissando |
  \bar "||"
  \break
  \mark \markup{Guitar Solo}
  \key f \major
  f,4\f\4 \repeat unfold 14 {f} f8 e |
  \break
  \repeat unfold 15 {f4} f8 e |
   \break
   \mark \markup{Verse 1}
   \repeat unfold 15 {f4} f8 e |
   \break
  \repeat unfold 13 {f4} f8. bes16\4 d8\3 d\3 es\3 bes\4\glissando |
  \break
  f4 f f f f f f f8 e |
  f4 f f f |
  f f8 g16 bes\4 d8\3 d\3 es\3 es\3 |
  \break
  f4\3 f\3 f\3 f\3 |
  f4\3^\markup{es gracenote!} f\3 f\3 f8\3 bes,\4 |
  c8\4 c\4 es\3 f16\3 g\3 c\2 g\3 c,8\4 c\4 bes\4 |
  c4\4 c8\4 e\3 f\3 g\3 g16\3 f\3 d\4 c\4 |
  \break
  \repeat unfold 7 {bes8\4} bes16\4 bes\4 |
  bes8\4 bes\4 bes\4 bes\4 bes'\2 bes\2 a16\2 g\2 d8\3 |
  \break
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout {\omit Voice.StringNumber
  \override Glissando #'breakable = ##t }
}
