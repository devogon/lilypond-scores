\version "2.23.82"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

%% https://onlinebasscourses.com/learn-basslines/back-in-black-ac-dc-bass-guitar-tutorial/


\header {
  title = "Back in Black"
  subtitle = "AC/DC - 1980 - Back in Black"
  instrument = "Bass"
  composer = "bass: Cliff Williams"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

global = {
  \key g \major
  \time 4/4
  \compressEmptyMeasures
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*2
  \repeat volta 2 {
    e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r r2 |
    e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato b16 gis8 b16 a8 b16 ais16 ~ ais b b8 |
  }
  \break
  \mark \markup{\box Verse}
  \repeat volta 2 {
    e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r r2 |
    e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato b16 gis8 b16 a8 b16 ais16 ~ ais b b8 |
    \break
    e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r r2 |
    e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato b16 gis8 b16 a8 b16 ais16 ~ ais b b8 |
    \bar "||" \break
    \mark \markup{\box Chorus}
    b8 b b b b8. a16 ~ a8 b |
    b8 b b b b8. a16 ~ a8 b |
    e,8 a a a a8. g16 ~ g8 a |
    a8 a a a a8. g16 ~ g8 a |
    \break
    a8 a r e8 b'8. a16 ~ a8 b|
    b8 b b b b8. a16 ~ a8 b |
  }
  \alternative {
    { g8 g16 g ~ g g g8 r g g cis |
      d8.-> d16-> ~ d8 d-> d2 |}
    {\break
     g,8 g16 g ~ g g g8 ~ g16 cis (d) g ~ g4 |
     d8. a'16 ~ a8 d ~ d2 |}
  }
  \mark \markup{\box Solo}
  \repeat volta 2 {
    e,,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r8 e,16 e e8 e'16 e e8 e,16 e |
    \break
    e4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r8 e,8 a8.-> e16-> ~ e8 a-> |
    e4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r8 e,16 e e8 e'16 e e8 e,16 e |
    \break
    e4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r8 e,16 e e8 e'16 e e8 e,16 e |
  }
  \mark \markup{\box Chorus}
  r8 b' b b b8. a16 ~ a8 b ~|
  b8 b b b b8. a16 ~ a8 b ~|
  \break
  b8 a a a a8. g16 ~ g8 a ~ |
  a8 a a a a8. g16 ~ g8 a ~|
  a8 a r e8 b'8. a16 ~ a8 b ~|
  b8 b b b b8. a16 ~ a8 b |
  \break
  g8 g16 g ~ g g g8 ~ g16 cis (d) g ~ g4 |
  d8. a16 ~ a8 d ~ d2 |
  \bar "||"
  \mark \markup{\box Interlude}
  e,4->\staccato r r2 |
  e4->\staccato r8. b'16 a8 b16 ais r b b8 |
  \break
  % start second page here
  \repeat percent 2 {e16 d cis b cis gis8 b16 a!8 b16 ais r b b8 |}
  \repeat percent 2 {a16 a' fis e fis cis8 e16 d8 e16 dis r e e8 | } \break
  \repeat percent 2 {e16 d cis b cis gis8 b16 a!8 b16 ais r b b8 |}
   \bar "||"
   \mark \markup{\box Chorus}
  b8 b b b b8. a16 ~ a8 b ~|
  b8 b b b b8. a16 ~ a8 b ~|
  \break
  b8 a a a a8. g16 ~ g8 a ~ |
  a8 a a a a8. g16 ~ g8 a ~|
  a8 a r e8 b'8. a16 ~ a8 b ~|
  b8 b b b b8. a16 ~ a8 b ~ |
  \break
  b g16 g ~ g g g8 r g g cis |
  d8. d16 ~ d8 d ~ d2 |
  a1 ~ |
  a2. ~ a8 e' |
  \break
  \bar "||"
  \mark \markup{\box Outro}
  \repeat volta 2 {
   e,4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato r8 e,16 e e8 e'16 e e8 e,16 e |
        e4->\staccato r8 d'16 d d4->\staccato r8 cis16 cis |
    cis4->\staccato_\markup{Repeat to fade} r8 e,8 a8.-> e16-> ~ e8 a-> |
  }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  %  \new TabStaff \with {
  %    stringTunings = #bass-tuning
  %  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
