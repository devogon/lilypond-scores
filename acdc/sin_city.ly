\version "2.19.54"
\language "nederlands"

\header {
  title = "Sin City"
  instrument = "Bass"
  composer = "Angus Young - AC/DC"
  copyright = "5 mei 1978 Atlantic Records"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 4/4
  \tempo 4=128
    \set countPercentRepeats = ##t
}

lowe = {  e4 e'8 e e b b b }

electricBass = \relative c, {
  \global
  % Music follows here.
  r16 r2. r8. | %  1
  g8 a b d a b e,4 ( | % 2
  e1 | % 3
  e1 | %  4
  e4) e'4. b4. | %  5
  d4. a2 (a8 | %  6
  a4) e'4. b4. | %  7
  d4. a2 (a8 | %  8
  a4) e'4. b4. | %  9
  % \break
  d4. a2 (a8 | %  10
  a4) e'4. b4. | %  11
  d4. a4 e8 g4 | %  12
  \repeat percent 7 { e8 e e e e e e e } % 13-19
  \repeat percent 4 {d'8 d d d d d d d } % 20-23
  \repeat percent 15 {e,8 e e e e e e e} % 24-38
  e8 e e e e e g4  | % 39
  \repeat percent 4 {a8 a a a a a a a} % 40-43
  \repeat percent 3 {b8 b b b b b b b} % 44 46
  b8 b b b b b d e | % 47
  e,8 e e' e, e e e e
  \repeat percent 13 {e8 e e e e e e e} % 48-62
  \repeat percent 4  {d'8 d d d d d d d} % 63-66
  d8 d d d d d d'4\glissando
  \repeat percent 15 {e,,8 e e e e e e e} % 67-81
  % test from here!
  \repeat percent 4 {d'8 8 8 8 8 8 8 8} % 82-85

  e,4 e'8 e e b b b | % 86
  d8 d d a a a g4 | % 87
  \repeat percent 10 {e4 e'8 e e b b b | % 88
  d8 d d a a a g4 | } % 89
  e4 e'8 e e b b b | % 108
  d8 d d a a e g e' | % 109
 %  e4 e'8 e e b b b | % 90
%   d8 d d a a a g4 | % 91
  % \break
%   e4 e'8 e e b b b | % 92
%   d8 d d a a a g4 | % 93
  % e4 e'8 e e b b b | % 94
%   d8 d d a a a g4 | % 95
%   e4 e'8 e e b b b | % 96
%   d8 d d a a a g4 | % 97
  % \break
%     e4 e'8 e e b b b | % 98
%   d8 d d a a a g4 | % 99
%   e4 e'8 e e b b b | % 100
%   d8 d d a a a g4 | % 101
%   e4 e'8 e e b b b | % 102
%   d8 d d a a a g4 | % 103
%   % \break
%     e4 e'8 e e b b b | % 104
%   d8 d d a a a g4 | % 105
%   e4 e'8 e e b b b | % 106
%   d8 d d a a a g4 | % 107
%   e4 e'8 e e b b b | % 108
  % \break
  %d8 d d a a a g e' | % 109
  e,4 e'8 e e b b b | % 110
  d8 d d a a a g4  | % 111
  e4  e'8 e e b b b | % 112
  d8 d d a a a g4  | % 113
  % \break
  e4  e'8 e e b b b | % 114
  d8 d d a a a g4  | % 115
  e4  e'8 e e b b b | % 116
  d8 d d a a a g g | % 117
  \repeat percent 4 {a8 8 8 8 8 8 8 8} % 118-121
  \repeat percent 3 {b8 8 8 8 8 8 8 8} % 122-124
  \repeat unfold 6 {b8} d e | % 125
  \repeat percent 15 {e,8 e e e e e e e} % 126-140
  \repeat percent 3 {d'8 d d d d d d d} %
  \repeat unfold 6 {d8} d4 (| % 143
  d1) | % 144
  \tempo 4=108
  %d1 | % 145
  \tempo 4=86
  r8 r2 r4. %d2) g,4 a | % 146
  \tempo 4=90
  % b d a b | % 147
%   e,1 | % 148
%   e2. r4 | % 149
  r4 g,8 a b d a b % 150
  e,1
  \bar "|."
}

\score {
  \new Staff \with {
   % instrumentName = "Electric bass"
    %shortInstrumentName = "E.Bs."
  } { \clef "bass_8" \electricBass }
  \layout { }
}

\markup { \teeny \date }