\version "2.19.48"
\language "nederlands"

\header {
  title = "Thunderstruck"
  subtitle = "(The Razor‘s Edge)"
  instrument = "Bass"
  composer = "Angus Young - AC/DC"
  meter = "4/4"
  copyright = "1990 AC/DC"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=134
}

electricBass = \relative c, {
  \global
  % Music follows here.
  \partial 4 { r4}
  \repeat unfold 480 {b8}
  R1
  R1
 \repeat unfold 160 {b8}
  b4 a e r
  r2 r4 a
  b4 a e r
  r2 r4 a
  b4 a e r
  r2 r4 a
  b4 a e r
  r1
  % start tuxguitar measure 92 here
  b'4. a8 a a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a e fis |
  a8 a a e (e) e e e |
  b'8 b b a (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a d, e |
  a8 a a e (e) e e e |
  \repeat unfold 64 {b'8}
  R1*18
  b4. a8 (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a e fis |
  a8 a a e (e) e e e |
  b'8 b b a (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b b b fis gis e ( |
  e) e e e e e e e |
  b'8 b b b b fis gis e ( |
  e) e e e e e e e |
  b'8 b b b b fis gis e ( |
  e) e e e e e e e |
  b'8 b b b b fis gis e ( |
  e) e e e e e e e |
  b'4. a8 (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a e fis |
  a8 a a e (e) e e e |
  b'4. a8 (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a e fis |
  a8 a a e (e) e e e |
  b'4. a8 (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a e fis |
  a8 a a e (e) e e e |
  b'4. a8  (a) a d, e |
  a8 a a e (e) e e e |
  b'8 b b a (a) a e fis |
  a8 a a e (e) e e e |
  b'1 (
  b1)
  b1 (
  b1)
  b4 r2 r4 |
  \bar "|."
}

classicalGuitar = \relative c' {
  \global
  % Music follows here.

}

cello = \relative c {
  \global
  % Music follows here.

}

electricBassPart = \new Staff \with {
  instrumentName = "Bass"
  %shortInstrumentName = "E.Bs."
} { \clef "bass_8" \electricBass }

classicalGuitarPart = \new Staff \with {
  instrumentName = "Guitar"
  shortInstrumentName = "Gt."
} { \clef "treble_8" \classicalGuitar }

celloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
} { \clef bass \cello }

\score {
  <<
    \electricBassPart
%    \classicalGuitarPart
%    \celloPart
  >>
  \layout { }
}
