\version "2.21.6"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Highway to Hell"
  subtitle = "AC/DC - Highway to Hell - 1979"
  instrument = "Bass"
  arranger = "arr by: Josh Fossgreen"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key a \major
  \time 4/4
  \tempo 4=115
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s2
  s1*23
  e1
  s1
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/fis
  a2 s4. d8
  s1
  s1*16
  e1
  s1
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/a
  a1
  s2 g4 d:/fis
  a2 s4. d8
  s1*5
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/a
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/fis
  a1
  s2 g4 d:/a
  a1
  s2 g4 d:/fis
  a2 s4. d8
  s2 g4 d
  a1
  s2 g4:/a d:/a
  a1
  s2 g4 d:/a
  a1
  s2 g4 d:/fis
  a2. s8 d
  s1*2
  a1
}

electricBass = \relative c, {
  \global
				% Music follows here.
  \override NoteHead.style = #'cross

  \partial 2 r8^\markup{\italic guitar} d d d %  x heads
    \revert NoteHead.style
  \bar "||"
  \repeat unfold 8 {r1}
  \bar "||"
  \break
  r1^\markup{\italic "Verse 1"}
  \repeat unfold 14 {r1}
  e,4 e8 e e e e e |
  e8 e e e e e e e |
  \bar "||"
  \break
  a4\staccato^\markup{\italic "Chorus 1"} a\staccato a\staccato a\staccato
  a\staccato a\staccato g'!\2 fis
  a,4\staccato a\staccato a\staccato a\staccato
  a\staccato a\staccato g'!\2 fis
  a,4\staccato a\staccato a\staccato a\staccato
  a4\staccato a\staccato g'!\2 a\2
  a,4\staccato a\staccato a\staccato a8 d\3 (
  d1\3)
  r1
  \bar "||"
  \break
  r1^\markup{\italic "Verse 2"}
  \repeat unfold 14 {r1}
  e,4 e8 e e e e e |
  e8 e e e e e  e8 e |
  \bar "||" \break
  a4\staccato^\markup{\italic "Chorus 2"} a\staccato a\staccato a\staccato
  a\staccato a\staccato g'!\2 fis
  a,\staccato a\staccato a\staccato a\staccato
  a\staccato a\staccato g'!\2 a\2
  a,\staccato a\staccato a\staccato a\staccato
  a8 e fis e g!4 fis
  a\staccato a\staccato a\staccato a8 d\3 (
  d1\3)
  d8->\3 d\3 d\3 d8->\3 d\3 d\3 d4\3 (
  d1\3)
  d8->\3 d\3 d\3 d->\3 d\3 d\3 d->\3 d\3 |
  d\3 d->\3 d\3 d\3 d\3 e, fis e |
    \break
  \bar "||"
  a4\staccato^\markup{\italic "Solo"} a\staccato a\staccato a\staccato
  a\staccato a\staccato g'!\2 fis
  a,4\staccato a\staccato a\staccato a\staccato
  a8 a cis e g!4\2 fis
  a,\staccato a\staccato a\staccato a\staccato
  a\staccato a8 e' g!4\2 a\2
  a,\staccato a\staccato a\staccato a\staccato
  a8 e fis e g!4 fis
  \break
  \bar "||"
  a\staccato^\markup{\italic "Chorus 3"} a\staccato a\staccato a\staccato
  a4 cis8 e g!4\2 fis
  a,\staccato a\staccato a\staccato a\staccato
  a4 cis8 e g!4\2 a\2
  a,\staccato a\staccato a\staccato a\staccato
  a8 e fis e g!4 fis
  a\staccato a\staccato a\staccato a8 d
  r2 g!4\2 d\3
  \repeat unfold 12 {a\staccato}
  a\staccato a\staccato g'!4\2 a\2
  a,\staccato a\staccato a\staccato a\staccato
  a\staccato a8 e g!4 fis
  a\staccato a\staccato a\staccato a8 d\3 (
  d1\3)
  r1\fermata^\markup{\center-align \italic "guitar noodles"}
  a1:32\fermata_\markup{\italic "on a highway to hell  "}\4 % vibrato
  a4\4 r r2
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout {
    \context { \ChordNames
     \override ChordName #'font-size = #.75
    }
  }
}
