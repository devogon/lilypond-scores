\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "John Williams themes for bass"
  instrument = "Bass"
  composer = "John Williams"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
    \remove "New_fingering_engraver"
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=100
}

electricBass = \relative c, {
  \global
  % Music follows here.
  r2.^\markup{\box "Star Wars"} \tuplet 3/2 {d8\4 d\4 d\4} \bar "||" |
  g2\3 d'2\2 |
  \tuplet 3/2 {c8\2 b\2 a\3 } g'2\1 d4\3 |
  \tuplet 3/2 {c8\2 b\2 a\3 } g'2\1 d4\3 |
  \tuplet 3/2 {c8\3 b\3 c\3 } a2.\2 |
  \bar "|."
  \break
  r2^\markup{\box "Indiana Jones"} r4 b8.\3 c16\3 |
  \bar "||"
  \repeat volta 2 {
    d8\3 g4.\2 ~ g4 a,8.\3 b16\3 |
    c2.\3 d8.\3 e16\2 |
    fis8\2 c'4.\1 ~ c4 e,8.\3 fis16\1|
    g4\2 a\1 b\1 b,8.\3 c16\3 |
    d8\3 g4.\2 ~ g4 a8.\1 b16\1 |
    c2.\1 d,8.\3 d16\3 |
    b'4\1 a8.\1 d,16\3
    b'4\1 a8.\1 d,16\3 |
    b'4\1 a8.\1 d,16\3 c'4\1 b,8.\3 c16\3 |
  }
  \break
  \time 12/8
  g4\3^\markup{\box Superman} g8\3 d'4.\2 ~ d4 d8\2 e\1 r16 d\2 c8\2 |
  d2.\2 ~ d2  \tuplet 3/2 {d,8\4 d\4 d\4} |
  g4\3 g8\3 d'4.\2 ~ d4 d8\2 e\1 r16 d16\2 c8\2 |
  e8\1 d2.\2 ~ d4. \tuplet 3/2 {g,8\3 g\3 g\3} |
  fis'2.\1 d2\2 \tuplet 3/2 {g,8\3 g\3 g\3} |
  fis'2.\1 d2\2 \tuplet 3/2 {g,8\3 g\3 g\3} |
  fis'8\1 e\1 fis\1 g2..\1 \tuplet 3/2 {g,8\3 g\3 g\3} |
  g1.\2
  \bar "|."
}

\score {
  \new StaffGroup \with {
    \consists "Instrument_name_engraver"
%    instrumentName = "Electric bass"
  } <<
    \new Staff { \clef
                 "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout { }
}
