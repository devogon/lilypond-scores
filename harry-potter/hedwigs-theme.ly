\version "2.18.2"
\language "nederlands"

%#(define my-shown-barnums '(7 13 18 23 27 29 35 36))
%#(define ((every-bar-number-of-list-visible ls) barnum mp)
%  (if (member barnum ls) #t #f))
\header {
  title = "Hedwig's Theme"
  instrument = "contrabass"
  composer = "John Williams"
  arranger = \markup {\tiny "(devogon)"}
%  meter = "4/4"
  copyright = "2002 Warner-Barham Music LLC"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
 \context {

     % \Voice
     \consists "Melody_engraver"
      \override Stem #'neutral-direction = #'()
   }
 }

global = {
  \key g \major
  \time 3/8
  \tempo "Misterioso" 4=60
  \compressFullBarRests
  \numericTimeSignature
}


contrabass = {
  \global
  \override MultiMeasureRest.expand-limit = #1                             % Music follows here.
  \partial 8 r8 
  R1*3/8*14
  e4^\markup{pizz.}\pp r 8 | % 15
  R1*3/8 | % 16
  b4 r8 | % 17
  b4 r8 | % 18
  e4 r8 | % 19
  R1*3/8 | % 20
  b4 r8 | % 21
  b4 r8 | % 22
    \mark \markup \center-column { \box A }
  e4 r8 | % 23
  R1*3/8 | % 24
  e4 r8 | % 25
  R1*3/8 | % 26
  e4 r8 | % 27
  R1*3/8 | % 28
  e4 r8 % 29
  R1*3/8 | % 30
  e4 r8 | % 31
  R1*3/8 | % 32
  g4^\markup{"arco"} r8 | % 33
  aes4 r8 | % 34
  a!4 r8 | % 35
  ais4\! r8  | % 36
  b4 r8 | % 37
  R1*3/8 | % 38
  b4^\markup{"pizz."} r8 | % 39
  b,4 r8 | % 40
  \repeat volta 2 {
    \mark \markup \center-column { \box B }
    e4\p r8 | % 41
    R1*3/8 | % 42
    e4 r8 | % 43
    R1*3/8 | % 44
    e4 r8 | % 45
    R1*3/8 | % 46
    e4 r8 | % 47
    R1*3/8*3 | % 48 49 50
    g4^\markup{"arco"}\> r8 | % 51
    aes4 r8 | % 52
    a!4 r8 | % 53
    ais4\! r8 | % 54
    b4 r8 | % 55
    R1*3/8 | % 56
    b4\p^\markup{"pizz."} r8 | % 57
    b,4 r8 | % 58
    e4 r8 | % 59
    R1*3/8 | % 60
    b4 r8 | % 61
    b4 r8 | % 62
    \mark \markup \center-column { \box C }
    e4 r8 | % 63
    R1*3/8 | % 64
    e4 r8 | % 65
    R1*3/8 | % 66
    e4 r8 | % 67
    R1*3/8 | % 68
    e4 r8 | % 69
    R1*3/8 | % 70
    e4 r8 | % 71
    R1*3/8 | % 72
    g4^\markup{"arco"}\> r8 | % 73
    aes4 r8 | % 74
    a!4 r8 | % 75
    ais4\! r8 | % 76
    b4 r8 | % 77
    R1*3/8 | % 78
    b,4 r8 | % 79
    b4 r8 | % 80
}
  \alternative {
    {
      e4 r8 | % 81
      R1*3/8 | % 82
      b4 r8 | % 83
      b4 r8 | % 84
    }
    {
      b4 r8 | % 85
      b4 r8 | % 86
      e4\mf r8 | % 87
      e4 r8 | % 88
      e4\> r8 | % 89
      e4\! r8 | % 90
      b4\f r8 | % 91
      c4 r8 | % 92
      b4 r8 | % 93
      e,4. | % 94
    }
  }
  \bar "|."
}

\score {
  
  \new Staff { \clef bass
               \relative c
               \contrabass
             }
  \layout {
    \context {
      \Score
      \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/18)
    }
  }
}
