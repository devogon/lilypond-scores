\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "So What"
  instrument = "Bass"
  composer = "Miles Davis"
  poet = "bass: Paul Chambers"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=80
}

contrabass = \relative c {
  \global
  % Music follows here.
  \time 5/4 a8 e' gis2. a,8 e' | % 1
  \time 4/4 a2. a,8 e' | % 2
  g1 | % 3
  \time 3/4 \tuplet 3/2 {a8 e g a f a e a f} | % 4
  \time 9/8 g2 g,8 e' a, dis gis, | % 5
  \break
  \time  4/4
  dis'2 d! | % 6
  d1 | % 7
  dis2 \tuplet 3/2 {e,8 a ais e' ais, a!} | % 8
  e1 | % 9
  r8 \autoBeamOff d' a' [b] c[ d]    e r \autoBeamOn  | % 10
  d2 e4. d8 | % 11
  \break
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 12
  d8 a4. r2 | % 13
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 14
  d2 r | % 15
  r8 e4. e4 e | % 16
  d4. a8 ~ a2 | % 17
  \break
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 18
  d2 r | % 19
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | %  20
  d2 r | % 21
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 22
  d2 r | % 23
  \break
  r8 e4. e4 e | % 24
  d4. a8 ~ a2 | % 25
  r8 \autoBeamOff dis, ais' [c] d!8 [d] f r \autoBeamOn | % 26
  dis2 r | % 27
  r8 \autoBeamOff dis, ais' [c] d!8 [d] f r \autoBeamOn | % 28
  dis8 ais4. r2 | %  29
  \break
  r8 \autoBeamOff dis, ais' [c] d!8 [d] f r \autoBeamOn | % 30
  dis2 r | % 31
  r8 \autoBeamOff dis, ais' [c] d!8 [d] f r \autoBeamOn | % 32
  dis8 ais4. r2 | % 33
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 34
  d2 r | % 35
  \break
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 36
  d8 a4. r2 | % 37
  r8 \autoBeamOff d, a' [b] c [d] e r \autoBeamOn | % 38
  d2 r | % 39
  r8 e4. e4 e | % 40
  d4. a8 ~ a2 | % 41
  r8 a,4. ~ a4 ~ a | % 42
  \break
  \tempo 4=134
  d8 d c4 b ais  | % 43
  a g f a | % 44
  d8 d e4 f a8 c | % 45
  d4 cis c! ais  | % 46
  a4 g f e  | % 47
  d4 c b ais | % 48
  a4 g f a | % 49
  \break
  d8 d e4 f a8 c | % 50
  d4 cis c! ais | % 51
  a4 g f e | % 52
  d8 d c4 b ais | % 53
  g4 a f a | % 54
  d8 d e4 f a8 b | % 55
  \break
  c4 d e f | % 56
  d cis c! ais | % 57
  a4 g f e | % 58
  dis cis c! f | % 59
  gis4 ais8 c cis4 c! | % 60
  ais4 a! gis fis | % 61
  \break
  f4 c gis c | % 62
  cis8 cis f4 fis g | % 63
  gis4 ais8 c cis4 dis8 eis | % 64
  f4 dis cis c! | % 65
  ais4  a! gis e | % 66
  d8 d c4 b ais | % 67
  \break
  a4 g f a | % 68
  d8 d e4 f a8 c | % 69
  d4 e f e | % 70
  d4 cis c! ais | % 71
  a4 g f e | % 72
  d8 d c4 b ais | % 73
  a4 g f a | % 74
  \break
  d8 f' d,4 a d | % 75
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
