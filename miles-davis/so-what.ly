\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "So What"
  instrument = "Bass"
  composer = "Miles Davis"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \partial 4
  \tempo "Slowly and Freely" 4=68
  \set Score.markFormatter = #format-mark-box-alphabet
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s4
  s1*11
  d1:m7
  s1*7
  d1:m7
  s1*7
  es1:m7
  s1*7
  d1:m7
  s1*23
  es1:m7
  s1*7
  d1:m7
  s1*7
  e1:m7
  s1*15
  f1:m7
  s1*7
  \break
  e1:m7
  s1*7
  e1:m7
  s1*18
  f1:m7
  s1*7
  e1:m7
  s1*31
  f1:m7
  s1*9
  e1:m7
  s1*7
  b1:m7
  s1*18
  c1:m7
  s1*7
  \bar "||"
  b1:m7
  s1*24
  c1:m7
  s1*7
  b1:m7
  s1*4
  e1:m7
  s1*2
  d1:m7
  s1*15
  e1:m7
  s1*8
  d1:m7
  s1*7
  d1:m7
  s1*17
  d1:m7
}

electricBass = \relative c, {
  \global
  % Music follows here.
  a8\mf e' |
  gis2. a,8 e' |
  a2. a,8 e' |
  g2.\fermata a,8 e' |
  \break
  a8 a, f' a, e' a, f' g ~ |
  g2\fermata r8 g, e' a, |
  es' as, es'2 d4 ~ |
  \break
  d1 |
  d1\fermata
  \time 2/4
  \break
  es,4. es8 |
  \time 4/4
  a bes es bes a2 ~ |
  \time 2/4
  a\fermata |
  \mark \default
  \time 4/4
  r8^\markup{\bold "A Tempo - Moderately"} d a' b c d e c |
  % \pageBreak
  d2_\markup{\box "Dr. Play Time"} e4. d8 |
  r8 d, a' b c d e c |
  d8 a,4. r2 |
  r8 d a' b c d e c |
  \break
  d2 r2 |
  r8 e4. e4 e |
  d4. a8 r2 |
  r8 d, a' b c d e c |
  \break
  d2^\markup{Davis, Coltrane, Adderley} r |
  r8 d, a' b c d e c |
  d8 a4. r2 |
  r8 d,8 a' b c d e c |
  % \pageBreak
  d2 r |
  r8 e4. e4 e |
  d4. a8 r2 |
  r8 es bes' c des es f des |
  \break
  es2\segno r |
  r8 es, bes' c des es f des |
  es8 bes4. r2 |
  r8 es, bes' c des es f des |
  % \pageBreak
  es2 r |
  r8 es, bes' c des es f des |
  es8 bes4. r2 |
  r8 d, a' b c d e c |
  \break
  d2 r |
  r8 d, a' b c d e c |
  d8 a4. r2 |
  r8 d, a' b c d e c |
  % \pageBreak
  d2 r |
  r8 e4. e4 e |
  d4.\coda^\markup{To Coda} a8 r2 |
  r1^\markup{\box "Davis Solo"} |
  \break
  \mark \default
  d,8_\markup{\box Chambers} d c4 ces bes |
  a g f a |
  d8 d e4 f a8 a |
  d4 des c bes |
  \break
  a4 g f e |
  d c ces bes |
  a g f a |
  d8 d e4 f a8 a |
  \break
  d4 des c bes |
  a4 g f e |
  d c ces bes |
  a g f a |
  % \pageBreak
  d e f a |
  cis d e f |
  g c, b bes |
  a g f e |
  \break
  es4 des2 f4 |
  as c8 des es4 c |
  bes beses as ges |
  f c as c |
  \break
  des f fis g |
  as  bes8 c des4 es8 e! |
  f4 es d c |
  bes a g e |
  \break
  d c ces bes |
  a g f a |
  d e f a8 b^\markup{x} |
  d4 e f e |
  \break
  d des c bes |
  a g f e |
  d c ces bes |
  a g f a |
  % \pageBreak
  r1_\markup{w/walking bass sim.}
  R1*3
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  % \pageBreak
  \mark \default
  R1*3
  \break
  r4 r r2^\markup{Coltrane solo - bar 112}
  R1*2
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
    R1*3
  \break
    R1*3
 % \pageBreak
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
 \break
  R1*4
  \break
  R1*4
  \break
  R1*4
  \break
  R1*4
 \break
 R1*4
 % \pageBreak
  R1*3
 \break
  R1*3
   \break
   R1*3
   \break
  r1 \bar "||" \mark \default r2 r4^\markup {\box "Adderly Solo"} r4 r1
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  % \pageBreak
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
  \break
  R1*3
 \break
  R1*3
 \break
  R1*3
 % \pageBreak
  R1*4
  \break
  R1*2
  \break
  R1*2
  \break
  R1*2
  \break
  R1*2
  \break
  R1*2
  \break
  R1*2
  \break
  R1*2
  \break
  R1*3
  % \pageBreak
  R1*2^\markup {\box Adderley} r1^\markup{Davis, Coltrane}
  \break
  \mark \default
  r1^\markup{Evans (also Bmin7, Emin7)} R1*3
  % \pageBreak
  R1*4
  \break
  R1*4
  % \pageBreak
  R1*4
  \break
  R1*4
  % \pageBreak
  R1*3
  \break
  R1*3
  % \pageBreak
  R1*3
  \break
  R1*4
  % \pageBreak
\mark \markup{\box F}
  d8_\markup{\box Chambers} d c4 b a |
  \tuplet 3/2 {e'8 d c} b4 cis e |
  d8 d c4 b a |
  \tuplet 3/2 {e'8 d c} b4 cis e |
  \break
    d8 d c4 b a |
  \tuplet 3/2 {e'8 d c} b4 cis e |
  d8 d c4 b a |
  d8 d a' b c d e c |
  \break
  d2 d4. d8 |
  r8 d, a' b c d e c |
  d8 a4. r2 |
  r8 d, a' b c d e c |
  % \pageBreak
  %\break
  d2 r |
  r8 e4. e4 e |
  d4. a8 r2 |
  r8 es bes' c des es f des |
  \break
  d4.\coda^\markup{Coda} a8 r2 |
  r8 d, a' b c d e c |
  d2_\markup{time on cim} r |
  \break
  r8 d, a' b c d e c |
  d8 a4. r2 |
  % \pageBreak
%  \break
  r8 d, a' b c d e c |
  d2 r |
  r8 e4. e4 e |
  d4. a8 r2 |
  r8 d, a' b c d e c |
  \break
  d2 r |
  r8 d, a' b c d e c |
  d8 a4. r2 |
  \break
  r8 d, a' b c d e c |
  d2 r |
  r8^\markup{fade out} e4. e4 e |
  d4. a8 r2 |
  \bar "|."


}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup \with {
  \consists "Instrument_name_engraver"
%  instrumentName = "Electric bass"
} <<
  \new Staff { \clef "bass_8" \electricBass }
%  \new TabStaff \with {
%    stringTunings = #bass-tuning
%  } \electricBass

>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
