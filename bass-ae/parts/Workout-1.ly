\version "2.19.57"
\language "nederlands"

headingtext = \markup \column \normal-text {
  \line{\small "Let's start with an easy rock beat with lots of chromatic runs. Take your time and"}
  \line{\small "concentrate on articulating every note clearly and evenly."}
  }

\header {
  title = "Workout 1"
%  subtitle = \headingtext
%  instrument = "Bass"
  % Remove default LilyPond tagline
  tagline = ##f
}


\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  print-page-number = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



global = {
  \key c \major
  \time 4/4
  \tempo 4=108
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

thechords = \chordmode {
  c:5 s2.
  g4:5 s2.
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  g4:5 s2.
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  g4:5 s2.
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  g4:5 s2.
  c4:5 s2.
  gis8:5 a:5 ais:5 b:5 c4:5 s4
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  g8:5 s s2.
  c4:5 s2.
  g4:5 s2.
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
  g8:5 s8 s2.
  c4:5 s2.
  f8:5 fis:5 g4:5 s2
  c4:5 s2.
}
electricBass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 2 {
    \bar ".|:"
  c4 c'4 r8 e,8 f fis |
  g4 g, r8 g a g |
  c4 c' b8 bes a e |
  \break
  f8 fis g4 r8 g, a g |
  c4 c'4 r8 e,8 f fis |
  g4 g, r8 g a g |
  \break
  c4 c' b8 bes a e |
  f8 fis g g, gis a ais b |
  c4 c' r8 e, f fis |
  \break
  g4 g, r8 g a g |
  c4 c' b8 bes a e |
  f8 fis g4 r8 g, a g |
  \break
  c4 c' r8 e, f fis |
  g4 g, r8 g a g |
  c4 c' e,,8 f fis g |
  \break
  gis8 a ais b c4 r |
  \bar "||"
  c4 c' b8 bes a e |
  f8 fis g4 r8 g, a g |
  \break
  c4 c'4 r8 e,8 f fis |
  f8 fis g g, gis a ais b |
  c4 c' b8 bes a e |
  \break
  f8 fis g4 r8 g, a g |
  c4 c'8 b bes a as g |
  ges f e es d des c g |
  \bar "||"
  \break
  c4 c' r8 e, f fis |
  g4 g, r8 g a g |
  c4 c' b8 bes a e |
  \break
  f8 fis g g, gis a ais b |
  c4 c' r8 e, f fis |
  g8 g, gis a bes a gis g |
  \break
  c4 c' b8 bes a e |
  f8 fis g g, gis a ais b |
  c4 c' r2 |
  }
}

\score {
  <<
    \new ChordNames {
      \thechords
    }
  \new Staff \with {
   % instrumentName = "Electric bass"
   % shortInstrumentName = "E.Bs."
  } { \clef "bass_8" \electricBass }
  >>
  \layout { }
}

\markup {
  \teeny
  \date
}