\version "2.23.82"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Go Your Own Way"
  subtitle = "Fleetwood Mac - 1977 - Rumours"
  arranger = "bass: John McVie"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=134
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*2
\mark \markup{VERSE}
\repeat volta 2 {
  \repeat unfold 24 {f,8} |
  r8 c' c c c c c4\glissando |
  \repeat  unfold 23 {bes8} c
}
\alternative {
  {\repeat unfold 8 {f,8}}
  {f'8 f f c f c e c}
}
\break
\mark \markup{CHORUS}
\repeat volta 2 {
  d8 d d f e d c bes |
  bes bes bes \grace f'32 (g8 ~ g) f e d |
  c8 c c c c c c c |
  c c c \grace g'32 (a8 ~ a) g f e
}
d8 d d f e d c bes |
bes bes bes \grace f'


32 (g8 ~ g) f e d |
c8 c c c c c c c |
c c c c c c bes g |
\bar "||"

\break
\mark \markup{VERSE}
\repeat volta 2 {
  \repeat unfold 24 {f8}
  r8 c' c c c c c bes |
  \repeat unfold 23 {bes8} c |
}
\alternative {
  {f8 f f f f, f f f}
  {f'8 f f c f c e c}
}

\break
\mark \markup{CHORUS}
\repeat volta 2 {
  d8 d d f e d c bes |
  bes bes bes \grace f'32 (g8 ~ g) f e d |
  c8 c c c c c c c |
}
\alternative {
  {c c c \grace g'32 (a8 ~ a) g f e}
  {c8 c c c c c bes g}
}
\bar "||"

\break
\mark \markup{INTERLUDE}
\repeat unfold 24 {f8}
c'8 c c c c c c a |
\break
\repeat unfold 23 {bes8} c |

\repeat unfold 23 {f,8} c' |
r8 c c c c c c a |
\repeat unfold 23 {bes8} c |
f8 f f c f c e c | % 48

\break
\mark \markup{CHORUS}
\repeat volta 2 {
  d8 d d f e d c bes |
  bes8 bes bes \grace f'32 (g8 ~ g) f e d |
  c8 c c c c c c c |
  c8 c c \grace g'32 (a8 ~ 8) g f e | %  52
}
d8 d d f e d c bes |
bes8 bes bes \grace f'32 (g8 ~ g) f e d |
c8 c c c c c c4 |
\break
c8 c c \grace g'32 (a8 ~ a4) g8 f |
d8 d d f e d c bes ~ |
bes8 bes bes \grace f'32 (g8 ~ g) f e d |
c4 c c8 c c c |
c c c c c c c d |
\bar "||"
\break

\mark \markup{GUITAR SOLO}
d8 d d d d d d a |
bes bes bes bes bes bes bes bes |
b! (c) c c c c c c |
c b ~ (c) c c c c cis |
d d d d d d d a |
\break
\repeat unfold 8 {bes} |
b! (c) c c c c c c |
c c c c c c c cis |
d d d d d d d a |
\repeat unfold 8 {bes} |
\break
c c c c c c c c | % 71
c c c c c c c c |
d d d d d d a a |
bes bes bes bes bes bes bes b! |
\repeat unfold 8 {c} |
\break
\repeat unfold 8 {c} | % 76
c (d) d d d d d a |
\repeat unfold 8 {bes} |
\repeat unfold 8 {c} |
\repeat unfold 8 {c} |
\bar "||"

\mark \markup{OUTRO}
d d d d d d d a | % 81
\repeat unfold 8 {bes} |
\repeat unfold 8 {c} |
\repeat unfold 8 {c} |
\repeat unfold 7 {d} a |
\break

bes bes bes bes bes bes  b~ (c) |
\repeat unfold 8 {c} |
\repeat unfold 8 {c} |
cis (d) d d d d d a |
\break

\repeat unfold 8 {bes} |
\repeat unfold 8 {c} |
\repeat unfold 8 {c} |
\bar "|."
}

chordsPart = \new ChordNames \chordNames

% electricBassPart = \new StaffGroup <<
%   \new Staff { \clef "bass_8" \electricBass }
%   \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } \electricBass
% >>
electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
