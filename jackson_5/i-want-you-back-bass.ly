\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "I Want You Back"
  arranger = "The Jackson 5"
  subtitle = "album: Diana Ross presents the Jackson 5"
  instrument = "Bass"
  poet = "bass: Wilton Felder"
  copyright = "yep, probably"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key as \major
  \time 4/4
  \tempo 4=98
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  as4 r r8 ces c!16 es f des |  % 1
  r2 r16  ces c! des ~ des d! es e! | % 2
  f4 c des8. as16 r4 | % 3
  bes4 es8. as,16 r2 | % 4
  \break
  as4 r r8 ces c!16 es f des |  % 5
  r2 r16  ces c! des ~ des d! es e! | % 6
  f4 c des8. as16 r4 | % 7
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | % 8
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c  | % 9
  des8. des16 c8. c16 bes16 bes8 es16 r bes es e! | % 10
  f8. \override NoteHead.style = #'cross des16 \revert NoteHead.style c4 des8. as16 r4 | % 11
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | % 12
  \bar "||"
  \break
  \mark \markup{\box vocals}
  as,4 r r8 ces8 c!16 es f des | % 13
  r2 r16  ces c! des ~ des d! es e! | % 14
  f4 c des8. as16 r4 | % 15
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | % 16
  \break
  as,4 r r8 ces8 c!16 es f des | % 17
  r2 r16  ces c! des ~ des d! es e! | % 18
  f4 c des8. as16 r4 | % 19
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | % 20
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c   | % 21
  des8. des16 c8. c16 bes16 bes8 es16 r bes es e! | % 22
  f4 c des8. as16 r4 | % 23
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | % 24
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c   | % 25
  des8. des16 c8. c16 bes16 bes8 es16 r bes es e! | % 26
  f4 c des8. as16 r4 | % 27
  r1
  \break
  \repeat unfold 3 {r2 r16 as as as as4}
  \time 2/4
  r1
  \break
  \time 4/4
  as4 r r8 ces8 c!16 es f des | %
  r2 r16  ces c! des ~ des d! es e! | %
  f4 c des8. as16 r4 | %
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | %
  \break
  as,4 r r8 ces8 c!16 es f des | %
  r2 r16  ces c! des ~ des d! es e! | %
  f4 c des8. as16 r4 | %
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | %
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c  | %
  des16 as' des des, ~ des des des bes ~ bes bes bes8 es16 bes es e! | %
  f8. f16 c4 des8. as16 ~ as4 | %
  bes4 es8. as,16 r8 es'16 f as f as8\staccato | %
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c   | % 46
  des8. des16 c8. c16 bes16 bes8 es16 r bes es e! | % 47
  f8. f16 c4 des8. as16 r4 | % 48
  r1 | % 49
  \break
  r2 r4 r16 f' as c, ~ | % 49
  c4 r16 f as as, as4 r16  f' as c, ~ | % 50
  c4 r16 des f as, ~ as4 r | % 51
  \break
  f'16 as \override NoteHead.style = #'cross des,16 \revert NoteHead.style c ~ c es g des ~ des f as as, ~ as c  es8 | %52
  des16 as' des des, ~ des des des bes ~ bes bes bes8 es16 bes es e! | % 53
  \break
  f16 as \override NoteHead.style = #'cross des,16 \revert NoteHead.style c ~ c es g des ~ des f as as, ~ as c  es8 | % 54
  f16 as \override NoteHead.style = #'cross des,16 \revert NoteHead.style c ~ c es g des ~ des f as as, ~ as c  es8 | % 55
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c   | % 56
  des8 des16 des c8. c16 bes16 bes8 es16 r bes es e! | % 57
  f8. f16 c4 des8. as16 r4 | % 58
  bes4 es r16 as, as as as4 | % 59
  \break
  r2 r16 as as as as4 | % 60
  \time 2/4
  r4 es'16 f as f | % 61
  \time 4/4
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c | % 62
  des16 as' des des, ~ des des des bes ~ bes bes bes8 es16 bes es e! | % 63
  \break
  f8. f16 c4 des8. as16 ~ as4 | % 64
  bes4 es4 r16 as, as as ~ as4 | % 65
  r2 r16 as as as ~ as4 | % 66
  \time 2/4
  r4  es'16 f as f | % 67
  \time 4/4
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c   | % 68
  des16 as' des des, ~ des des des bes ~ bes bes bes8 es16 bes es e! | % 69
  f16 c f,8 ~ f16 c'8 c16 des8. as16 r4 | % 70
  bes4 es r16 as, as as ~ as4 | % 71
  \break
  r2 r16 as16 as as ~ as4 | % 73
  \time 2/4
  r4  es'16 f as f | % 74
  \time 4/4
  as8. as16 g8. g16 f16 f8\staccato es16 r c es c   | % 75
  des8 des16 des c8. c16 bes16 bes8 es16 r bes es e! | % 76
  \break
  f8. f16 c4 d8. as16 ~ as4 | % 77
  bes4 es r16 as, as as ~ as4 | % 78
  r2 r16 as as as ~ as4 | % 79
  \time 2/4
  r4  es'16 f as f | % 80
  \time 4/4
  \break
  as8. as16 g8. g16 f16 f8\staccato es16 r c c8\staccato | % 81
  des16 as' des des, ~ des des des bes ~ bes bes bes8 es16 bes es e! | % 82
  f4 c des8. as16 r4  | %
  bes4 es r16 as, as as ~ as4\staccato | %
  \bar "|."

}
% \override NoteHead.style = #'cross des16 \revert NoteHead.style
chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
