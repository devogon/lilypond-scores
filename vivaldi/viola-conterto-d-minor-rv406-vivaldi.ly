\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}


\header {
  title = "Viola Concerto D Minor"
  subtitle = "arranged from Cello Concerto RV406"
  instrument = "Basso"
  composer = "Antonio Vivaldi (1680-1743)"
  arranger = "arr: G.H. Kruithof"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
    page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative c' {
  \global
  \key d \minor
  \time 4/4
  \tempo 4=128
  % Music follows here.
  d8 f d f d f d f | % 1
  a,4 r a8 cis a cis | % 2
  a8 cis a cis d4 r | % 3
  d,8 f d f g,4 r | % 4
  c8 e c e f,4 r | % 5
  bes8 d bes d e,4 r | % 6
  % \break
  a8 cis a cis d4 r | % 7
  a4 r r2 | % 8
  R1*4 | % 9-12
  r8 r r a d d a' a, | % 13
  d4 r r2 | % 14
  R1*12 | % 15-26
  r2 a'8 c a c | % 27
  a8 c a c e,4 r | %  28
  % \break
  e8 gis e gis e gis e gis | % 29
  a,4 r a8 a a a | % 30
  d8 d d d g g g g | % 31
  c,8 c c c f f f f | % 32
  f8 f d d e e e e | % 33
  a,8 a' d, e a,4 r | % 34
  % \break
  R1 *17 | % 35 -
  r2 f'8 a f a | % 52
  c,8 e c e f a fis a | % 53
  g8 bes g bes d, fis d fis | % 54
  g8 bes g bes g bes g bes | % 55
  a8 cis a cis d a f a | % 56
  % \break
  cis8 cis cis a e f f e | % 57
  e f a a a a, bes a | % 58
  a4 r r2 | % 59
  R1*15 | % 60-74
  d8 f d f a,4 r8 r | % 75
  c8 e c e f,4 r8 r | % 76
  bes8 d bes d e,4 r8 r | % 77
  % \break
  a8 cis a cis d4 r8 r | % 78
  a'4 r r2 | % 79
  R1*4 | % 80 83
  r8 r r a,8 d d a' a, | % 84
  d4\fermata r r2 | % 85
  \bar "|."
}

andante = \relative c' {
  \global
  \key f \major
  \time 4/4
  \tempo Andante 4=75
  g16 fis g8 r a bes16 a bes8 r fis | % 1
  g16 fis g8 r cis, d4 r8 d' | % 2
  c16 b! c8 r a b16 a b8 r g | % 3
  a16 g a8 r d, g,4 r | % 4
  R1*15
  \bar "|."
}


minuetto = \relative c {
  \global
  \key f \major
  \time 3/8
  \tempo Minuetto 8=78
%  \set Score.currentBarNumber = #
  \repeat volta 2 {
  d4 d8 | % 1
  d4 d8 | % 2
  g8 cis, a | % 3
  d4 d8 | % 4
  d8 e f | % 5
  e8 c gis | % 6
  a8 e' e, | % 7
  a16 f' e8 e, | % 8
  a16 f' e8 e, | % 9
  a8 c gis | % 10
  a8 e' e, | % 11
  a4.
  }
  \repeat volta 2 {
    R1*8*3/8 |
    % \break
    d8 f d | % 21
    d8 f f | % 22
    R1*2*3/8 |
    d8 g cis, | % 25
    d4 r8 | % 26
    d8 a' a, | % 27
    d4. | % 28
  }
  R1*28*3/8
  \bar "||"
  R1*12*3/8
  \bar "||"
  R1*16*3/8
  \bar "||"
  d4 d8 | % 85
  d4 d8 | % 86
  g8 cis, a | % 87
  d4 d8 | % 88
  % \break
  d8 e f | % 89
  e8 c gis | % 90
  a8 e' e, | % 91
  a16 f' e8 e, | % 92
  a16 f' e8 e, | % 93
  a8 c gis | % 94
  a8 e' e, | % 95
  a4. | % 96
  R1*8*3/8 |
  d8 f d | % 105
  d8 f d | % 106
  R1*2*3/8 |
  d8 g cis, | % 109
  % \break
  d4 r8 | % 110
  d8 a' a, | % 111
  d4. | % 112
  \repeat volta 2 {
    R1*12*3/8
  }
  R1*16*3/8 |
  d4 d8 | % 141
  d4 d8 | % 142
  g cis, a | % 143
  d4 d8 | % 144
  d8 e f | % 145
  e8 c gis | % 146
  a8 e' e, | % 147
  a16 f' e8 e, | % 148
  % \break
  a16 f' e8 e, | % 149
  a8 c gis | % 150
  a8 e' e, | % 151
  a4. | % 152
  R1*8*3/8 |
  d8 f d | %
  d8 f d | %
  R1*2*3/8
  d8 a' cis, | %
  d4 r8 | %
  d8 a' a, | %
  d4.
  \bar "|."
}
\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\score {
  \new Staff { \clef bass \andante }
  \layout { }
}

\score {
  \new Staff { \clef bass \minuetto }
  \layout { }
}