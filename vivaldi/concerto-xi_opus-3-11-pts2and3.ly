\version "2.19.57"

\language "nederlands"

\header {
  title = "Concerto XI"
  subtitle = "Opus 3.11"
  subsubtitle = "pts 2 & 3"
  composer = "Antonio Vivaldi"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
    page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
   \key f \major
   \time 12/8
   \tempo "2. Largo e Spiccato" 4=60
   d4 d8 d4 d8 d4 d8 cis4 cis8 | % 1
   d4 d8 g4 g8 fis8. g16 fis8 f!4 f8 | % 2
   g8. a16 g8 a4 a,8 d4. r4 r8 | % 3
   \break
   R1*13*12/8 % 4-16
   r2^\markup{\bold 17} r4 d4\f d8 d4 d8  % 17
   d4 d8 cis4 cis8 d4 d8 g4 g8 | % 18
   fis8. g16 fis8 f!4 f8 g8. a16 g8 a4 a,8 | % 19
   d1. | % 20
  \bar "|."
}

contrabastwo = \relative c {
  \global
  \key f \major
  \time 4/4
  \tempo "3. Allegro" 4=120
  \partial 8 r8 | % 1
  R1*6 % 2-7
  d4 r g r
  c,4 r f r
  bes,4 r e r
  a,4 r d r
  d4 r a d'16 d d d | % 11
  \break
  cis16 cis cis cis c! c c c b b b b bes bes bes bes | % 12
  a16 a a a d, d d d g g g g a a a a | % 13
  d,4 r r2 | % 14
  R1*8 % 15-22
  \break
  a4 r d r
  g4 r c, r
  f4 r b,! r
  e4 r a r
  a4 r gis a16 a a a | % 27
  \break
  gis16 gis gis gis g! g g g fis fis fis fis f! f f f | % 28
  e16 e e e a, a a a d d d d e e e e | % 29
  a,4 r d\p r | % 30
  \break
  g4 r c r | % 31
  f,4 r bes r | % 32
  e,4 r a r | % 33
  d,4 r d r | % 34
  R1*9 % 35-43
  r2^\markup{\bold 44} a8\f cis b! a | % 44
  d16 d d d d d d d a'4 r8 a, | % 45
  \break
  d8 b! e e, a4 r | % 46
  R1*3 % 47-49
  r8^\markup{\bold 50} a8 a a d4 r8 d | % 50
  a4 r r2 | % 51
  r8 a a a d4 r8 d |  % 52
  a4 r r2 | % 53
  R1*5 % 54-58
  \break
  d8 d' d,4 r8 d'8 d,4 | % 59
  r8 d' d,4 r8 g g,4 | % 60
  r8 g' g,4 r8 a' a,4 | % 61
  r8 g' g,4 r8 a' a,4 | % 62
  r8 g' g,4 r8 a' a,4 | % 63
  r8 g' g,4 r8 a' a,8 g | % 64
  \break
  f8 d'' d,4 r8 d' d,4 | % 65
  r8 a' a,4 r8 a' a,4 | % 66
  r8 a' a,4 r8 a' a,4 | % 67
  d4 d'16 d d d cis cis cis cis c! c c c | % 68
  \break
  b!16 b b b bes bes bes bes a a a a d, d d d | % 69
  g16 g g g a a a a d,4 d16\p d d d | % 70
  \break
  cis16 cis cis cis c! c c c b! b b b bes bes bes bes  | % 71
  a16 a a a d\f d d d g, g g g a a a a | % 72
  d1\fermata | % 73
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabastwo }
  \layout { }
}


\markup {
  \teeny
  \date
}