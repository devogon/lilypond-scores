\version "2.19.57"

\language "nederlands"

\header {
  title = "Concerto XI"
  subtitle = "Opus 3.11"
  composer = "Antonio Vivaldi"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
    page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key f \major
  \time 3/4
  \tempo "Allegro" 4=126
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
    R1*3/4*19 % 1-19
    d8 d d d d d | % 20
    g,8 g g g g g | % 21
    c8 c c c c c | % 22
    f,8 f f f f f | % 23
    % \break
    bes8 bes bes bes d d | % 24
    e8 e e e e e | % 25
    f8 f fis fis fis fis | % 26
    g8 g gis gis gis gis | % 27
    a4 d, a | % 28
    d4 r r | % 29
    % \break
    d4 r r | % 30
    d2.\fermata | % 31
    \time 4/4
    \tempo "Adagio e Spiccato"
    e8 e e e a,2\fermata | % 32
    d8 d d d g,2\fermata | % 33
    g8 g c c a2\fermata | % 34
    \tempo "Allegro" r8 d16 e f8 f e e16 f g8 e | % 35
    % \break
    f8 d d'4 g, c | % 36
    f,4 bes e, a | % 37
    d,4 g4. f8 e4 | % 38
    d4 a'2 gis4 | % 39
    a4 c8 c c bes16 a bes8 bes | % 40
    % \break
    bes8 e, a a a g16 f g8 g | % 41
    g8 c, f f e a4 g8 | % 42
    f4 r8 d g e a a, | % 43
    % \break
    d8 a'16 g f e d c bes8 g'16 f e d c bes | % 44
    g8 f'16 e d c bes a g8 e'16 d c bes a g | % 45
    % \break
    f8 f' g g cis, d a' a, | % 46
    d4 r8 c d b! e e, | % 47
    a4 r r8 bes' g e | % 48
    r8 a f d r g e c | % 49
    % \break
    f4. f8 gis, a e' e, | % 50
    a4 d'\f g, c | % 51
    f,4 bes e, a | % 52
    d, g cis, d | % 53
    a'4 a, d r | % 54
    r2 d4 r8 g | % 55
    % \break
    c,4 r8 f bes,4 r8 e | % 56
    a,4 r8 d g,4 r8 c | % 57
    f,4 r8 f' gis, gis gis gis | % 58
    a4 r8 a d4 r8 d | % 59
    g4 r8 g c,4 r8 c | % 60
    % \break
    f4 r8 f gis,4. gis8 | % 61
    a4 r8 a d b! e e, | % 62
    a8 a16 b! c8 c b8 b16 c d8 b | % 63
    c8 a a'4 d, g | % 64
    c, f b,! e | % 65
    % \break
    a,4 d2 cis4 | % 66
    d4 r8 d g e a a, | % 67
    d8 d16 e f g a bes c8 c,16 d e f g a | % 68
    % \break
    bes8 bes,16 c d e f g a8 a,16 bes c d e f | % 69
    g8 g,16 a b! c d e f8 f,16 g a b c d | % 70
    % \break
    e8 d e e, a a' f d | % 71
    g4 r8 g a16 g f e d8 f | % 72
    g8 e a a, d bes'16 a g f e d | % 73
    % \break
    c8 a'16 g f e d c bes4 r8 bes | % 74
    c8 a d d g, g' f d | % 75
    g8 e a a, d bes'16 a g f e d | % 76
    % \break
    c8 a'16 g f e d c bes8 g'16 f e d c bes | % 77
    a8 d a' a, d g, d' d | % 78
    g,4 r8 g'8 c a d d, | % 79
    % \break
    g4 r8 d g e a a, | % 80
    d4 r r2 | % 81
    R1*5 % 82-86
    d4^\markup{87} d'2 cis4 | % 87
    d4 c8 d e4 e, | % 88
    a,1 ( | % 89
    a1 ) | % 90
    a1 ( | % 91
    a1 ) | % 92
    a1 ( | % 93
    a1 ) | % 94
    a1 ( | % 95
    a1 ) | % 96
    a1 ( | % 97
    a1 ) | % 98
    a1 ( | % 99
    a1 ) | % 100
    d1 ( | % 101
    d1 ) | % 102
    gis,2 a2 | % 103
    d1\fermata | % 104
    \bar "|."
    }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

% \markup \column {
%   \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
%   \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
%   \hspace #1
% }

\markup {
  \teeny
  \date
}