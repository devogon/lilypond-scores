\version "2.19.55"
\language "nederlands"

\header {
  title = "Violin Concerto in G major"
  subtitle = "RV 310"
  instrument = "Basso"
  composer = "Antonio Vivaldi (1680-1743)"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  ragged-last-bottom = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key g \major
 % \time 4/4
 % \tempo 4=120
}

scoreABcMusic = \relative c {
  \global
  \time 4/4
  \tempo 4=120
  % Music follows here.
  r1 | % 1
  g'4 d g r8 g | % 2
  d'8 d d d, g g g g,\p | %  3
  d''8 d d d, g g g g,\f | % 4
  d'4 e fis g ( | % 5
  g4) fis8 g a4 a, | % 6
  d8 d c c b b b b | % 7
  \break
  c8 c c c cis cis cis cis | % 8
  d8 d d d d d d d | % 9
  e8 e e e e e e e | % 10
  fis8 fis fis fis fis fis fis fis | % 11
  g4 r r2 | % 12
  r1 | % 13
  r1 | % 14
  \break
  r1 | % 15
  g,4 r g r | % 16
  g'4 d g g, | % 17
  r1 | % 18
  r1 | % 19
  r2 d''8 d d d, | % 20
  a'8 a a a, d'8 d d d, | % 21
  \break
  a'8 a a a, d4 r | % 22
  r1 | % 23
  r1 | % 24
  r1 | % 25
  r1 | % 26
  r1 | % 27
  r1 | % 28
  r1 | % 29
  d4\f r d r | % 30
  d4 a d r8 d | % 31
  \break
  a'8 a a a, d d d d\p | % 32
  g8 g g g, d' d d d\f | % 33
  e8 e e e d d d d | % 34
  cis8 cis cis cis fis fis e e | % 35
  d4 r b8 cis d e | % 36
  fis4 fis, b r | % 37
  \break
  r1 | % 38
  r1 | % 39
  r1 | % 40
  r1 | % 41
  r1 | % 42
  r1 | % 43
  \break
  r1 | % 44
  r2 b4 r | % 45
  b4 r b fis | % 46
  b8 a g4 fis r | % 47
  d'4 r d a | % 48
  d4. c8 b4 r | % 49
  g'4 r g d | % 50
  g4 g, r2 | % 51
  r1 | % 52
  \break
  r1 | % 53
  r1 | % 54
  g8 g g g b b b b | % 55
  c8 c c c cis cis cis cis | % 56
  d8 d d d d d d d | % 57
  e8 e e e e e e e | % 58
  fis8 fis fis fis fis fis fis fis | % 59
  \break
  g8 b,16 c d8 d g,4 r | % 60
  g4 r g' d | % 61
  g,4 r g r | % 62
  g4 r8 b c4 d | % 63
  g,1\fermata
  \bar "|."
}

scoreABcFigures = \figuremode {
  \global
  \override Staff.BassFigureAlignmentPositioning #'direction = #DOWN
  % Figures follow here.

}

\score {
  <<
    \new Staff \with {
 %     instrumentName = "Basso Continuo"
 %     shortInstrumentName = "B.c."
    } { \clef bass \scoreABcMusic }
    \new FiguredBass \scoreABcFigures
  >>
  \layout { }
  \header {
    piece = "I"
  }
}

scoreBBcMusic = \relative c {
  \global
  \time 3/4
  \tempo 4=120
  % Music follows here.
  e4 e e | % 1
  e4 r r | % 2
  b4 b b | % 3
  b4 r r | % 4
  e4 e e | % 5
  e4 r r | % 6
  gis4 gis gis | % 7
  gis4 r r | % 8
  a4 a a | % 9
  a4 r r | % 10
  fis4 fis fis | % 11
  b,4 r r | % 12
  e4 r r | % 13
  fis4 r r | % 14
  b,4 b b | % 15
  b4 b b | % 16
  e4 r r | % 17
  a,4 a a | % 18
  a4 a a | % 19
  d4 d d | % 20
  dis4 dis dis | % 21
  e4 r r | % 22
  dis4 dis dis | % 23
  e8 a b4 b, | % 24
  e4 e e | % 25
  e2. | % 26
  a,2. | % 27
  b4 b b | % 28
  e8 a b4 b, | % 29
  e4 e e | % 30
  a4 b b, | % 31
  e,2.\fermata
  \bar "|."
}

scoreBBcFigures = \figuremode {
  \global
  \override Staff.BassFigureAlignmentPositioning #'direction = #DOWN
  % Figures follow here.

}

\score {
  <<
    \new Staff \with {
 %     instrumentName = "Basso Continuo"
 %     shortInstrumentName = "B.c."
    } { \clef bass \scoreBBcMusic }
    \new FiguredBass \scoreBBcFigures
  >>
  \layout { }
  \header {
    piece = "II"
  }
}


scoreCBcMusic = \relative c {
  \global
  \time 3/8
  \tempo 4=58
  % Music follows here.
  \repeat volta 2 {
    g'16 a b a b g | % 1
    d16 e fis e fis d | % 2
    g16 a b a b g | % 3
    d'8 d, r | % 4
    g16\p a b a b g | % 5
    d16 e fis e fis d | % 6
    g16 a b a b g | % 7
    a16\f b cis b cis a | % 8
    b16 a b cis d e | % 9
    a,16 g a b cis d | % 10
    g,16 fis g a b cis | % 11
    fis,16 e fis g a b | % 12
    e,16 d e fis g a | % 13
    d,4.
  }
  \compressFullBarRests
  %\override MultiMeasureRest #'expand-limit = 1
%  \repeat unfold 28 {R8*3}
  R8*84
  e16 fis g fis g e | % 43
  b16 cis dis cis dis b | % 44
  e16 fis g fis g e | % 45
  b'8 b, r | % 46
  e16\p fis g fis g e | % 47
  b16 cis dis cis dis b | % 48
  e16 fis g fis g e | % 49
  fis16\f gis ais gis ais fis | % 50
  g16 fis g a b cis | % 51
  fis,16 e fis g a b | % 52
  e,16 d e fis g a | % 53
  d,16 cis d e fis g | % 54
  cis,16 b cis d e fis | % 55
  b,4 r8 | % 56
  \repeat unfold 3 {b4 r8}
  \repeat unfold 4 {e4 r8}
  \repeat unfold 4 {a,4 r8}
  \repeat unfold 4 {d4 r8}
  g8 r b | % 72
  c8 r cis | % 73
  d8 r dis | % 74
  e8 r e, | % 75
  fis4 e8 | % 76
  d8 b d | % 77
  e4.  | % 78
  fis4 e8 | % 79
  d8 b d | % 80
  e4. | % 81
  fis8. e16 d8 | % 82
  e8 ais,4 | % 83
  b8. cis16\p d b | % 84
  e8 ais,4 | % 85
  b8. cis16\f d b | % 86
  e8 fis fis, | % 87
  b16 cis d cis d b | % 88
  fis'16 gis ais gis ais fis | % 89
  b,16 cis d cis d b | % 90
  fis'8 fis, r | % 91
  b16\p cis d cis d b | % 92
  fis'16 gis ais gis ais fis | % 93
  b,16 cis d cis d b | % 94
  fis'8 fis, r | % 95
  d'16\f e fis e fis d | % 96
  a16 b cis b cis a | % 97
  d16 e fis e fis d | % 98
  a4 r8 | % 99
  d16\p e fis e fis d | % 100
  a16 b cis b cis a | % 101
  d16 e fis e fis d | % 102
  a'16\f b cis b cis a | % 103
  b16 a b cis d e | % 104
  a,16 g a b cis d | % 105
  g,16 fis g a b cis | % 106
  fis,16 e fis g a b | % 107
  e,16 d e fis g a | % 108
  d,8 r r | % 109
  d8 r r | % 110
  d8 r r | % 111
  g8 r r | % 112
  c,8 r r | % 113
  g'8 r r | % 114
  c,8 r r | % 115
  g8 r r | % 116
  d'8 r r | % 117
  d8 r r | % 118
  g8 g, g' | % 119
  d'8 d, d' | % 120
  g,8 g, g' | % 121
  d'8 d, r | % 122
  g8\p g, g' | % 123
  d'8 d, d' | % 124
  g,8 g, g' | % 125
  d'8 d, r | % 126
  b'4\f r8 | % 127
  c4 r8 | % 128
  cis4 r8 | % 129
  d4 r8 | % 130
  dis4 r8 | % 131
  e4 r8 | % 132
  e,4 r8 | % 133
  fis4 r8 | % 134
  fis4 r8 | % 135
  g4 r8 | % 136
  g8 d' r | % 137
  g,8 c r | % 138
  fis,8 b r | % 139
  e,8 a r | % 140
  d,8 g b, | % 141
  c8 d d | % 142
  g,4 r8 | % 143
  g'8\p d' r | % 144
  g,8 c r | % 145
  fis,8 b r | % 146
  e,8 a r | % 147
  d,8 g b, | % 148
  c8 d d | % 149
  g,8 r g | % 150
  c8 d d | % 151
  g,4.\fermata | % 152
  \bar "|."
}

scoreCBcFigures = \figuremode {
  \global
  \override Staff.BassFigureAlignmentPositioning #'direction = #DOWN
  % Figures follow here.

}

\score {
  <<
    \new Staff \with {
 %     instrumentName = "Basso Continuo"
 %     shortInstrumentName = "B.c."
    } { \clef bass \scoreCBcMusic }
    \new FiguredBass \scoreCBcFigures
  >>
  \layout { }
  \header {
    piece = "III"
  }
}

\markup {
  \teeny
  \date
}