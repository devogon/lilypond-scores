\version "2.19.57"

\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "le Quattro Stagioni"
  subtitle = "Estate"
  composer = "Antonio Vivaldi"
  instrument = "contrabas"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key bes \major
  \time 3/8
  %\tempo "" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

contrabasOne = {
   \global
  \tempo "Allegro non molto"
  \mark \default
  R1*30*3/8
 % g,8 r g r g, r g r | % 49
 % d8 r r4 d8 r r4 | % 50
 \break
  \mark \default
  \time 4/4
  \tempo \markup{"Allegro" \italic "e tutto sopra il canto"}
  R1*18
  \break
  g,8 r g r g, r g r |
  d8 r r4 d8 r r4 |
  d8 r r4 d8 r r4 |
  \time 3/8
  R1*38*3/8
  \break
  d8\f r r | % 90
  g,8 r r |
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r | % 100
  d8 r r
  a,16 r cis a, a, a, |
  a,16 r d a, a, a, |
  a,16 r e! a, a, a, |
  a,16 r d a, a, a, |
  a,16 r cis a, a, a, |
  a,16 r d a, a, a, |
a,16 r d a, a, a, |
  a,16 r cis a, a, a, |
  d8 d a, |
  r8 a, d|
  r8 g g, |
  r8 g g, |
  r8 d' d |
  r8 g g, |
  d4 r8 |
  R1*38*3/8 |
  g,8\f r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  d8 r r
  g,8 r r
  c8 r r
  d8 r r
  g,8 r r
  c8 r r
  cis8 r r
  d8 r r
  g,8 r r
  R1*3*3/8
  g,4.\fermata
  \bar "|."
}

contrabasTwo = {
   \global
  \time 4/4


}

contrabasThree = {
   \global

   s1 }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabasOne }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabasTwo }
  \layout { }
}
