\version "2.19.57"

\language "nederlands"

\header {
  title = "Sinfonia \"Al Santo Sepolcro\""
  composer = "A. Vivaldi"
%  poet = "poet"
  instrument = "violoncello/basso"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key d \major
  \time 4/4
  \tempo "Adagio molto" 4=72
  \set Score.markFormatter = #format-mark-box-alphabet
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

contrabas = \relative c {
   \global
  R1*2 % 1-2
  b1 ( | % 3
  b2) a | % 4
  g4. e'8 ais,2 | % 5
  a4 g fis eis | % 6
  fis1 ( | % 7
  % \break
  fis1) | % 8
  fis'1 | % 9
  e1 | % 10
  dis4 cis b2 ( | % 11
  b2) ais | % 12
  b2 e8 d cis b | % 13
  ais2 b ( | % 14
  % \break
  b1) (| % 15
  b1) | % 16
  fis1\fermata | % 17
  fis2\p fis4 r | % 18
  b2\pp b4 r8 b | % 19
  eis,1 | % 20
  fis1\fermata | % 21
  % \break
  \tempo "Allegro ma poco" 4=120
  R1*2 % 22-23
  r2 fis8 fis' fis fis | % 24
  fis4 e d8 d d d | % 25
  cis4 cis c!8 c c c | % 26
  b4 b r b'8 b, | % 27
  % \break
  e4 e a,8 a a a | %28
  d4 d r g8 g | % 29
  ais,4 b cis cis | % 30
  fis,4 r e'2 ( | % 31
  e1) ( | % 32
  e2) dis8 b b' b | % 33
  % \break
  b4 ais a,!8 a' a a | % 34
  gis2 g,!8 g' g g | % 35
  fis4 fis, fis'2 ( | % 36
  fis4) e a8 a a a | % 37
  gis4 gis fis,8 fis' fis fis | % 38
  fis4 e d8 d d d | % 39
  % \break
  cis4 cis c!8 c c c | % 40
  b4 b r b'8 b, | % 41
  e2 a,8 a' a a | % 42
  d,4 r8 d e e fis fis  | % 43
  b,4 r r gis'8 fis | % 44
  eis2 fis8 fis fis fis | % 45
  % \break
  b,2 e | % 46
  r4 a8 a, d4. d8 | % 47
  g1 | % 48
  fis4 eis fis2 | % 49
  b,4 b'8 b, e8 e e e | % 50
  a,4 a'8 a, d d d d | % 51
  % \break
  g,4 g'8 g, fis'2 ( | % 52
  fis1\pp) ( | % 53
  fis1) ( | % 54
  fis1) ( | % 55
  fis1) ( | % 56
  fis1) ( | % 57
  % \break
  fis1) ( | % 58
  fis1) ( | % 59
  fis1) ( | % 60
  fis1) ( | % 61
  fis2) fis, | % 62
  b1\fermata | % 63
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}



\markup {
  \teeny
  \date
}