\version "2.19.81"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}


\header {
  title = "Le Quattro Stagioni"
  subtitle = "Estate"
  instrument = "Contrabas, Cello"
  composer = "Antonio Vivaldi (1680-1743)"
  meter = "Allegro non Molto" % not very fast
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \minor
  \numericTimeSignature
  \time 3/8
%  \tempo 4=100
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative g, {
  \global
  % Music follows here.
    r8_"Allegro non molto - Pianissimo"^"Languideza per il caldo" g' d |
  r g d |
  r d' d, |
  r d' d, |
  g,4 r8 |	% 5
  d'4 r8 |
  d4 r8 |
  g,4 r8 |
  g4 r8 |
  c4. |	% 10
  d4.^\fermata |
  R4.*2 |
d'8 ( c bes |
a16 g fis8  ees!) |	% 15
d4. ~ |
d4. |
g8 ( fis ees! | % not explicitly flat here (key sig), but marking it ! for consistency with everywhere else
d16 c bes8  a) |
g4 r8 |		% 20
r8 g' g, |
r c' c, |
r g' g, |
r d' d |
r8 g,4 |		% originally written g4 r8	 % 25
r8 g' g, |
r c c |
r g' g, |
r d' d |
r g,4 |		% 30
\bar "||"
\time 4/4
\mark "B"
g8._"Allegro" g'16 g,4	r8 r16 g' g,4 |
g8. g'16 g,4	r8 r16 g' g,4 |
r8 r16 g' g,4 	g'8 fis g g, |
d'8. d'16 fis,4	r8 r16 d' fis,!4 |
r8 r16 d' fis,4	fis8 g a d, |	% 35
g,8. g'16 g,4	r8 r16 g' g,4 |
g'8 a bes g 	a,8. a'16 a,4 |
r8 r16 a' a,4	a'8 bes c a |
bes8. d16 fis,4	r8 r16 d' fis,!4 |
r8 r16 g g,4 	r8 r16 g' g,4 |	% 40
r8 r16 ees'' g,4 	r8 r16 ees' g,4 |
r8 r16 c e,4 	r8 r16 c' e,!4 |
r8 r16 c' f,4 	r8 r16 c' f,4 |
r8 r16 d' fis,4	r8 r16 d' fis,!4 |
r8 r16 g bes,4 	r8 r16 g' bes,4 |	% 45
r8 r16 f'' a,4 	r8 r16 f' a,4 |
r8 r16 ees' g,4 	r8 r16 ees' g,4 |
r8 r16 d' f,4	r8 r16 d' fis,!4 |
g,16 g' g g g g g, g' 	g, g' g g g g g, g' |
d16 d d d d d d d 	d d d d d d d d |	% 50
d16 d d d d d d d 	d d d d d d d d |
\bar "||"
\time 3/8
g,8_"Pianissimo" g' d |
r g, d' |
r g g, |
r c' c, |	% 55
r g' g, |
r d' d |	% originally written d' d, r
r g,4 |
\mark "C"
g4. ~ |
g4. ~ |	% 60
g4. ~ |
g4. ~ |
g16. g'32 g,4 ~ |
g16. f'32 g,4 ~ |
g16. ees'32 g,4 ~ |	% 65
g16. d'32 g,4 ~ |
g4. ~ |
g4. ~ |
g4. ~ |
g4. |	% 70
R4.*7 |
\mark "D"
R4.*12 |
d'16_"Forte"^"Venti diuersi" d' d, d d d |	% 90
a a' a, a a a |
d d' d, d d d |
d d' d, d, d d |
d' d' d, d d d |
d d' d, d d d |  % 95
d d' d, d d d |
d d' d, d d d |
d d' d, d d d |
d d' d, d d d |
d d' d, d d d |  % 100
d d' d, d d d |
a a' cis, a a a |
a a' d, a a a |
a a' e a, a a |
a a' d, a a a |  % 105
a a' cis, a a a |
a a' d, a a a |
a a' d, a a a |
a a' cis, a a a |
d8_"Pianissimo" d a |  % 110
r a d |
r g g, |
r g' g, |
r d'' d, |
r a' a, |  % 115
% \break
\mark "E"
d4.^"Il Pianto del Villanello" |
gis4. |
g!4. |
fis4. |
f!4. |  % 120
e!4. |
ees!4. |
dis4. |
e |
ais, |  % 125
% \break
a! |
gis |
g! |
fis |
f! |  % 130
e! |
ees! |
d |
b''! |
bes! |  % 135
aes ~ |
aes |
g |
f |
ees ~ |  % 140
ees |
d4 c8 |
bes a g |
c4. ~ |
c4. ~ |  % 145
c4. ~ |
c4. ~ |
c4. ~ |
c4. ~ |
c4. |  % 150
g'4. ~ |
g4. |
fis8 d g |
ees16 c d8 d |
g,16_"Forte" g' g, g g g' |  % 155
d d' d, d d d |
d d' d, d d d |
d d' d, d d d |
d d' d, d d d |
d d' d, d d d |  % 160
d d' d, d d d |
d d' d, d d d |
g,16 g' g, g g g |
\stemUp c c' c, c, c c \stemNeutral |
d' d' d, d, d d |  % 165
g g' g, g g g |
c c' c, c c c |
cis cis'? cis, cis cis cis |	% top c not marked sharp
\stemUp d d' d, d d d \stemNeutral |
%#(override-auto-beam-setting '(end 1 32 3 8) 1 8)
%#(override-auto-beam-setting '(end 1 32 3 8) 2 8)
g,32 g' f ees	d ees d c 	bes c bes a |  % 170
g g' f ees	d ees d c 	bes c bes a |
g g' f ees	d ees d c 	bes c bes a |
g g' f ees	d ees d c 	bes c bes a
\cadenzaOn g2.^\fermata \cadenzaOff
\bar "||"
}

contrabass-adagio = \relative g, {
  \global
    \tempo "Adagio"
  % Music follows here.
  \mark \markup{\box F}
  \time 4/4
  R1*2 | % 1-2
  r2 g8_\markup{presto}^\markup{Tuoni} g16 g g g g g | % 3
  g16 g g g g g g g g4 r  | % 4
  R1*3_\markup{Adagio}
  r2 bes8_\markup{Presto} bes16 bes bes bes bes bes | % 8
  % \break
  bes16 bes bes bes bes bes bes bes bes4 r | % 9
  R1*6_\markup{Adagio} | % 10-15
  g8_\markup{Presto} g16 g g g g g g g g g g g g g | % 16
  g4 r r2_\markup{Adagio} | %  17
  % \break
  R1*2 | % 18-19
  g8_\markup{Presto} g16 g g g g g g g g g g g g g | % 20
  g4 r r2_\markup{Adagio} r1
  \bar "||"
}

contrabass-presto = \relative g, {
  \global
  \tempo "Presto"
  \time 3/4
  \mark \markup{\box G}
  % Music follows here.
  g'8^\markup{"Tempo impettuoso d' Estate"} r g, r g r | % 1
  f'8 r g, r g r | % 2
  es'8 r g, r g r | % 3
  % \break
  d'8 r g, r g r | % 4
  r2.\fermata | %  5
  d''8 r d, r d r | % 6
  c' r d, r d r | % 7
  % \break
  bes'8 r d, r d r | % 8
  a'8 r d, r d r | % 9
  R1*3/4 | % 10
  g,8 g g g g g | % 11
  % \break
  g8g g g g g | % 12
  c8 c c c c c | % 13
  c8 c c c c c | % 14
  es8 es es es es es | % 15
  % \break
  es8 es es es es es | % 16
  g8 g g g g g | % 17
  g8 g g g g g | % 18
  g8 g g g g g | % 19
  % \break
  g8 r d' r d r | % 20
  d,8 d d  d d d | % 21
  d8 d d  d d d | % 22
  % \break
  d8 d d  d d d | % 23
  d8 d d  d d d | % 24
  d8 d d  d d d | % 25
  % \break
  d8 d d  d d d | % 26
  d8 d d  d d d | % 27
  d8 d d  d d d | % 28
  % \break
  g,8 g g g g g | % 29
  g8 g g g g g | % 30
  g8 g g g g g | % 31
  g8 r g r g r | % 32
  % \break
  c8 r c r c r | % 33
  g8 r g r g r | % 34
  d'8 r d r d r | % 35
  % \break
  g,8 r g r g r | % 36
  g8 r g r g r | % 37
  d'4 r r | % 38
  R1*3/4 | % 39
  % \break
  d4 r r | % 40
  R1*12*3/4 | % 41-52
  d8 a d a d a | % 53
  d8 a d a d a | % 54
  d8\f c' a f d r | %  55
  % \break
  g,8 f' d bes g r| % 56
  c8 bes' g e! c r | % 57
  f,8 e'! c a f r | % 58
  bes8 a' f d bes r | % 59
  % \break
  es d bes g es' r | %  60
  as,8 g' es c as r |
  b!8 a' f d b! r |
  g8 f' d b! g r |
  % \break
  c8 b'! g es c r | % 64
  r8 b'! g es c r |
  r8 b'! g es c r |
  es8 es es es es es |
  % \break
  f8 f f f f f | % 68
  g8 g g g g, g |
  c8 c c c c c |
  bes' c, c c c c |
  % \break
  as' c, c c c c | % 72
  g'8 c, c c c r |
  c8 c' c c, c c' |
  g,4 r r |
  % \break
  c8 c' c c, c c' | % 76
  bes,4 r r |
  es8 es' es es, es es' |
  g,,4 r r |
  g8 g' g g, g g' |
  % \break
  as,4 r r | % 81
  a8 a' a a, a a' |
  bes,4 r r |
  b!8 b'! b b, b b' |
  % \break
  c,8 c c c c c | % 85
  as'8 c, as' c, as' c, |
  b!8 b b b b b |
  % \break
  g'8 bes, g' bes, g' bes, | % 88
  a8 a a a a a | % 89
  f'8 as, f' as, f' as, |
  % \break
  g8 g' g g g g | % 91
  es' g, es' g, es' g, |
  fis8 fis fis fis fis fis |
  % \break
  d'8 f, d' f, d' f, | % 94
  e!8 e e e e e |
  c'8 es, c' es, c' es, |
  % \break
  d2._\markup{Tasto Solo} ~
  d2. ~
  d2. ~
  d2. ~
  d4 r r |
  g,8 g g g g g |
  g8 g g g g g |
  % \break
  c8 c c c c c | % 104
  c8 c c c c c |
  es8 es es es es es |
  es8 es es es es es |
  % \break
  g8 g g g g g | % 108
  g4 g, g |
  g4 g g |
  g g g |
  g4 g g |
  g4 r r |
  % \break
  R1*3/4 | % 114
  bes8 bes bes bes bes bes |
  d4 r r |
  R1*3*3/4 |
  d8 c' a a fis d |
  % \break
  bes8 bes bes bes bes bes | % 121
  c8 c c c c c |
  d8 d d d d d |
  % \break
  g,8 g bes bes bes bes | % 124
  c8 c c c c c |
  d8 d d d d d |
  % \break
  g,8 g g g g g | % 127
  g8 g g g g g |
  g g g g g g |
  g2. |
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\score {
  \new Staff { \clef bass \contrabass-adagio }
  \layout { }
}

\score {
  \new Staff { \clef bass \contrabass-presto }
  \layout { }
}