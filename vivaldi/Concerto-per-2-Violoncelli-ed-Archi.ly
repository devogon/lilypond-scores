\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Concerto per 2 Violoncelli ed Archi"
  subtitle = "in Sol minore RV 531"
  subsubtitle = "checkme"
  instrument = "Violoncello e Basso"
  composer = "Antonio Vivaldi (1680-1743)"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "Allegro" 4=120
}

allegroA = \relative c {
  \global
  % Music follows here.
  g4\downbow r g\downbow r | % 1
  g4\downbow r g r | % 2
  g4 r g r | % 3
  g4 r g r | % 4
  g4 r g r | % 5
  g8-> g g g g4\tenuto r | % 6
  \break
  g8 g g g g4 r | % 7
  g8 g g g g4 r\breathe| % 8
    \ottava #-1
  bes8\staccato r a\staccato r g\staccato r f\staccato r | % 9
  es8\staccato r d\staccato \ottava #0 r g g g g | % 10
  g8 g g g c c c c | % 11
  \break
  c8 c c c f f f f | % 12
  f8 f f f bes, bes bes bes  | % 13
  bes8 bes bes bes es es es es | % 14
  es8 es es es a, a a a | % 15
  \break
  a8 a a a d d d d | % 16
  d8 d d d g,4 r | % 17
  g4 r g r | % 18
  g4 r \ottava #-1 g16_\markup{check me!} g g g bes g g g | % 19
  \break
  bes16 g g g bes g g g b! g g g b g g g | % 20
  b!16 g g g b g g g c g g g c g g g | % 21
  c16 g g g c g g g cis g g g cis g g g | % 22
  \break
  cis16 g g g cis g g g \ottava #0 d'4 r | % 23
  d4 r d r | % 24
  \repeat unfold 8 {g,8}
  g8 g g g d'4 r16 c16 bes a | % 26
  g8 g' c, d g,4\breathe g'\mp ~ | % 27
  \break
  g4 fis g8 bes, c d | % 28
  g,4 g'2 fis4 | % 29
  g8 bes, c d g,4 g | % 30
  c4 c f, f | % 31
  bes4 bes es es | % 32
  g,4 g bes r | % 33
  \break
  bes4 r es r | % 34
  es r f r
  f, r bes r
  bes r es r
  es r f r
  f, r bes8 bes bes bes | % 39
  \break
  c8 c c c f f f f | % 40
  bes, bes bes bes es es es es | % 41
  a,8 a a a d d d d | % 42
  g,8 g g g c c c c | % 43
  f,4 r f r | % 44
  \break
  f4 r bes8 bes' es, f | % 45
  bes,4 r bes r | % 46
  bes r bes r | % 47
  bes r bes r | % 48
  bes4 r8 bes a4 bes | % 49
  c4 d es f | % 50
  \break
  bes,4 c8 d es4 f8 g | % 51
  a,4 bes8 c d4 es8 f | %  52
  g,4 a8 bes c4 d8 es | % 53
  f,4 g8 a bes4 r | % 54
  b!2 c8 r es r | % 55
  \break
  f8 r d r es r c r | % 56
  d8 r b! r c4 es8 c | % 57
  g8 r g r c r es r | % 58
  f8 r d r es r c r | % 59
  \break
  d8 r b! r c4 es8 c | % 60
  f4 f c8 c c c | % 61
  c\< c c c\! c\> c c c\! | % 62
  bes8\< bes bes bes\! bes\> bes bes bes\! | % 63
  a8\< a a a\! a\> a a a\! | % 64
  \break
  g16 es' es es g es es es g e e e g e e e | % 65
  as16 f f f as f f f a fis fis fis a fis fis fis | % 66
  bes16 g g g bes g g g b! g g g b g g g | % 67
  \break
  c16 g g g c g g g d' g, g g d' g, g g | % 68
  es'4 r g, r | % 69
  c, r g' r | % 70
  c, r g' r | % 71
  bes, r es r | % 72
  \break
  as,4 r d r | % 73
  bes4 r as8 g \ottava #-1 es16 f g as | % 74
  bes8 es, es16 f g as bes8 es, es16 f g as \ottava #0 | % 75
  bes8 es a, bes es es es es | % 76
  \break
  e!8 e e e f f f f | % 77
  d8 d d d es es es es | % 78
  c8 c c c d d d d | % 79
  g,4 r g r | % 80
  g r g r | % 81 | % 81
  \break
  d'4 r d r | % 82
  d r d r
  g g g g
  c, c c c
  d d d d
  bes bes bes bes
  \break
  es4 es es es | % 88
  a, a a a |
  d d d d
  g, r g r
  g r d''8 g, d d' | % 92
  \break
  g,,16 g g g bes g g g b g g g b g g g | % 93
  c16 g g g c g g g cis g g g cis g g g | % 94
  \break
  d'4 r d r | % 95
  \repeat unfold 12 {g,8} d'4 r16 c bes a % 96-97
  g8 g' c, d g,2\fermata | % 98
  \bar "|."
}

\score {
  \new Staff { \clef bass \allegroA }
  \layout { }
    \header {
    piece = \markup{\box 1}
  }
}

largo = \relative c {
  \key f \major
  \time 4/4
  \tempo "Largo" 4=50
  % Music follows here.
  \repeat volta 2 {
    g'8 g, r g c g r g | % 1
    c8 g bes g c4. c8 | % 2
    d4 c bes g | % 3
    a4 d a'4. a,8 | % 4
    d8 g a a, d2 | % 5
  }
  \break
  \ottava #-1 d8 d, r d' g, d r d' \ottava #0 | % 6
  g8 d f d g,4. g8 | % 7
  a4 r8 d g4 c, | % 8
  f4 bes, e a, | % 9
  d4 g, c4. c8 | % 10
  \break
  f4 r8 g fis4. fis8 | % 11
  g4 r8 c, d4. d8 | % 12
  g8 g, bes g r g bes g | % 13
  r8 g bes g r g bes g | % 15
  r8 g bes g r2 | % 16
  g4 d' g,2\fermata
  \bar "|."
}

\score {
  \new Staff { \clef bass \largo }
  \layout { }
    \header {
    piece = \markup{\box 2}
  }
}
\pageBreak
allegroB = \relative c {
  \key f \major
  \time 3/4
  \tempo "Allegro" 4=120
  % Music follows here.
  g'8 f es d c g' | % 1
  fis e d c bes d' | % 2
  c bes a g fis es16 (d) | % 3
  g8 d bes a g g' | % 4
  bes8 a g f es bes' | % 5
  a8 g f es d f | % 6
  \break
  es8 d c bes a g16 (f) | % 7
  bes'8 f d c bes bes' | % 8
  d8 c bes a g d' | % 9
  cis8 b! a g f a | % 10
  g8 f e d cis bes16 (a) | % 11
  d'8 a f e d a' | % 12
  \break
  d,8 e f d a' a, | % 13
  d e f d a' a, | % 14
  d8 d' a f d r | % 15
  g,8 a bes a bes g | % 16
  c4 r r | % 17
  f,8 g a g a f | % 18
  \break
  bes4 r r | % 19
  es,8 f g f g es | % 20
  a4 r r | % 21
  d,8 e! fis e fis d | % 22
  \repeat unfold 3 {g4 g8 g g g} % 23-25
  \break
  \repeat unfold 3 {d'4 d8 d d d} % 26-28
   d4 r r | % 29
   g,8 g' d bes g r | % 30
   a8 a' e cis a r | % 31
   \break
   d,8 d' a f d r | % 32
   g4 r r | % 33
   a4 r r | % 34
   a4 r r | % 35
   a2. ~ | % 36
   a2. ~ | % 37
   a2. | % 38
   d4 g, a | % 39
   \break
   d'8 c bes a g d' | % 40
   cis8 b! a g f a | % 41
   g8 f e d cis b16 (a) | % 42
   d8 d' a f d d' | % 43
   f,8 e d c bes f' | % 44
   e8 d c bes a c' | % 45
   \break
   bes8 a g f e d16 c
   f8 f' c a f a
   a g f e d a'
   gis fis e d c e
   d c b! a gis fis16 e
   a8 e' c b! a a'
   \break
   a8 b! c d e e,
   a8 b! c d e e,
   a,4 r8 a c a
   gis4 r8 e' gis e
   a,4 r8 a8 c a
   gis4 r8 e' gis e
   \break
   \repeat unfold 9 {a,4}
   d4 d d
   f f f
   e e e
    e e e
    d d d
    d d d
    c c c
    e e e
    f f f
     f f f
     bes, bes bes
     bes f' f,
     bes'8\mf a g f es bes'
     \break
     a8 g f es d f
     es d c bes a g16 f
     bes8 f' d c bes bes'
     f' e d c bes f
     e' d c bes a c
     bes a g f e d16 c
     \break
     f8 f' a, g f b!
     c bes as g f c'
     b! a g f es g
     f es d c b! a!16 g
     c8 g' es d c g'
     c,8 d es c g' g,
     \break
     c8 d es c g' g,
     r1*3/4
     \repeat tremolo 4 g16 \repeat tremolo 4 g16 \repeat tremolo 4 g16
     r1*3/4
     \repeat tremolo 4 c16 \repeat tremolo 4 c16 \repeat tremolo 4 c16
     r1*3/4
     \repeat tremolo 4 bes16 \repeat tremolo 4 bes16 \repeat tremolo 4 bes16
     r1*3/4
     \repeat tremolo 4 d16 \repeat tremolo 4 d16 \repeat tremolo 4 d16
     r1*3/4
     \repeat tremolo 4 g,16 \repeat tremolo 4 g16 \repeat tremolo 4 g16
     g4 r r
     d'4 r r
     g,4 r r
     d'4 r r8 d
     g,2 r4
     c2 r4
     c2 r4
     \break
     d2 r4
     d2 r4
     es2 r4
     es2 r4
     d2 r4
     g8 d es bes c d
     g,2 r4
     c2 r4
     c2 r4
     \break
     d2 r4
     d2 r4
     es2 r4
     es2 r4
     d2 r4
     g8 d es bes c d
     g,4 g g
     d4 d d
     \break
     d4 d d
     g'8 f es d c g'
     fis8 e d c bes d'
     c8 bes a g fis e16 d
     g8 d bes a g d'
     \break
     g8 a bes g d' d,
     g,8 a bes g d' d,
     g'8 a bes g d' d,
     g,8 a bes g d' d,
     g2.\fermata
     \bar "|."
}

\score {
  \new Staff { \clef bass \allegroB }
  \layout { }
  \header {
    piece = \markup{\box 3}
  }
}