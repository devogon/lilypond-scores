\version "2.19.36"
\language "nederlands"

\header {
  title = "Birthday"
  instrument = "Bass"
  composer = ""
  meter = "140"
  copyright = ""
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=140
  \clef bass
}

gstart = {a,8 a, cis e g fis4  e8}

cstart = {c8 c fis a c' b4 a8 }

estart = {e8 e gis b d' cis'4 b8 }

dstart = { d8 d fis a c' b4 a8 }

erep = { e8 e e e e e e e }
electricBass = \absolute {
  \global
  % Music follows here.
  \partial 2 r2 |
  \repeat unfold 4 { \gstart }
  \repeat unfold 2 { \dstart }
  \repeat unfold 2 { \gstart }
  \repeat unfold 2 { \estart }
  \repeat unfold 6 { \gstart }
  \repeat unfold 2 { \dstart }
  \repeat unfold 2 { \gstart }
  \repeat unfold 2 { \estart }
  \repeat unfold 2 { \gstart }
  g,8 r r4 r2 |
  R 1*7 |
  \repeat unfold 7 { \erep }
  e8 e e e e e d d |
  c8 c e e f f fis fis |
  g8 g f f e e d d |
  c8 c e e f f fis fis |
  g8 g f f e e d d |
  c8 c e e f f fis fis |
  g8 g f f e e d d |
  c8 c e e f f fis fis |
  g8 g gis gis a a ais ais |
  b8 b b b b b b <b e> |
  e8 e d32 e16. e8 e d e e |
  \repeat unfold 4 { \gstart }
  \repeat unfold 2 { \dstart }
  \repeat unfold 2 { \gstart }
  \repeat unfold 2 { \estart }
  \repeat unfold 2 { \gstart }
  a,8 e r g r a r g |
  e8 d c a, g,2 |
  a,8 e r g r a r c' |
  e'8 d' d'16 c' a8 g2 |
  c8 c e e f f fis fis |
  g8 g f f e e d d |
  c8 c e e f f fis fis |
  g8 g f f e e d d |
  c8 c e e f f fis fis |
  g8 g f f e e d d |
  c8 c e e f f fis fis |
  g8 g gis gis a a ais ais |
  b8 b b b b b b <b e> |
  e8 e d32 e16. e8 e d e e |
  \repeat unfold 4 { \gstart }
  \repeat unfold 2 { \dstart }
  \repeat unfold 2 { \gstart }
  \repeat unfold 2 { \estart }
  r8 a, cis e g fis e a, |
  r8 a, cis e g fis e a, (|
  a,1) |
  R1*5 |
  \bar "|."
}

\score {
  \new Staff \with {
    %midiInstrument = "electric bass (finger)"
    %instrumentName = "Electric bass"
    %shortInstrumentName = "Electric bass"
  } { \clef "bass_8" \electricBass }
  \layout { }
%  \midi { }
}
