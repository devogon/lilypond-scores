\version "2.23.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}


\header {
  title = "She Loves You"
  subtitle = "The Beatles"
  instrument = "Bass"
  composer = "Lennon/McCartney"
  arranger = "bass: Paul McCartney"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=151
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  
}

electricBass = \relative c, {
  \global
  % Music follows here.
    R1	 |
    e4. e8 b4. d8 |
    e4. e8 b4. e8 |
    a,4. a8 e'4. e8 |
    \break
    a,4. a8 e'4. a,8 |
    c4. c8 g'4. g8 |
    c,4. c8 g'4. c,8 |
    g'2 d4. f8 |
    \break
    g2 d8 d e fis |
    g4. g8 d4 g |
    e4. e8 e4 r |
    b4. b8 b4 r |
    \break
    d4. d8 d4 r |
    g4. g8 g4 r |
    e4. e8 e4 r |
    b4. b8 b4 r |
    \break
    d4. d8 d4 r |
    g4. b,8 ~ b4 d |
    g4. g8 d4 g |
    e4. e8 b4. e8 |
    \break
    e4. e8 b4 e |
    c4. c8 g'4. b,8 |
    c4. c8 g'4 c, |
    d4. d8 a'4. cis,8 |
    \break
    d4. d8 a' d, e fis |
    g4. g8  d4 g |
    e4. e8 e4 r |
    b4. b8 b4 r |
    \break
    d4. d8 d4 r |
   g4. g8 g4 r |
    e4. e8 e4 r |
    b4. b8 b4 r |
    \break
      d4. d8 d4 r |
    g4. b,8 ~ b4 d |
    g4. g8 d4 g |
    e4. e8 b4. e8 |
    \break
    e4. e8 b4 e |
    c4. c8 g'4. b,8 |
    c4. c8 g'4 c, |
    d4. d8 a'4. cis,8 |
    \break
    d4. d8 a'4 d, |
    e4. e8 b4. d8 |
    e4. e8 b4. e8 |
    a,4. a8 e'4. e8 |
    \break
    a,4. a8 e'4. a,8 |
    c4 c c r |
    d4. d8 a'4. d,8 |
    g4. g8 d4. d8 |
    \break
    g4. g8  d8 d e fis |
    g4. g8 d4 g |
    e4. e8 e4 r |
    b4. b8 b4 r |
    \break
    d4. d8 d4 r |
   g4. g8 g4 r |
    e4. e8 e4 r |
    b4. b8 b4 r |
    \break
    d4. d8 d4 r |
    g4. b,8 ~ b4 d |
    g4. g8 d4 g |
    e4. e8 b4. e8 |
    \break
    e4. e8 b4 e |
    c4. c8 g'4. b,8 |
    c4. c8 g'4 c, |
    d4. d8 a'4. cis,8 |
    \break
    d4. d8 a'4 d, |
    e4. e8 b4. d8 |
    e4. e8 b4. e8 |
    a,4. a8 e'4. e8 |
    \break
    a,4. a8 e'4. a,8 |
    c4 c c r |
    d4. d8 a'4. d,8 |
    g4. g8 d4 g8 fis |
    \break
      e4. e8 e2 |
      c4 c c r |
      d4. d8 a'4 d, |
      g4. g8 d4 g8 fis |
      \break
      e4. e8 e2 |
      c4 c c r |
      d1 |
      r1|
      \break
      g4. g8  d4 d8 fis |
      g4. g8 e4 g8 fis |
      e4. e8 b4. d8 |
      e4. e8 b2 |
      \break
      c4. c8 g'4. b,8 |
      c4 c g'4. c,8 |
      g'1 ~ |
      g1 |
      \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff \with {
    midiInstrument = "electric bass (finger)"
  } { \clef "bass_8" \electricBass }
 %  \new TabStaff \with {
%     stringTunings = #bass-four-string-tuning
%   } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
  \midi {
    \tempo 4=150
  }
}
