\version "2.19.84"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Day Tripper"
  subtitle = "The Beatles - album - year"
  instrument = "Bass"
  tagline = \date
}

\paper  {
    #(define fonts
    (make-pango-font-tree "MuseJazz Text"
                          "Nimbus Sans"
                          "Luxi Mono"
                          (/ staff-height pt 20)))
  indent = 0.0\cm
}

\layout { \omit Voice.StringNumber
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

global = {
  \key e \major
  \time 4/4
%  \override Score.RehearsalMark.font-family = #'sans
}

electricBass = \relative c, {
  \global

  R1*2 |
  e,4. g!8 gis b e d! ~ |
  d4 b8 fis' ~ fis b, d! e |
  \repeat volta 2 {
    \repeat percent 2 {
      e,4. g!8 gis b e d! ~ |
      d4 b8 fis' ~ fis b, d! e |
    }
  }

  \mark \markup {Vocal}
  \bar "||"
  \repeat percent 2 {
    e,4.\segno g!8 gis b e d! ~ |
    d4 b8 fis ~ fis b d! e |

  a,4. c!8 cis e a g! ~ |
  g4 e8 b' b e, g! a |
    e,4. g!8 gis b e d! ~ |
  d4 b8 fis' ~ fis b, d! e |
  \repeat unfold 7 {fis,8} e |
  \repeat unfold 5 {fis8} a gis g! |
  \repeat unfold 7 {fis8} e |
  \repeat unfold 5 {fis8} a gis g! |
  \repeat unfold 8 {a8} |
  \repeat unfold 8 {gis8} |
  \mark \markup{To Coda}
  \repeat unfold 8 {cis8} |
  }
  \mark \markup{Guitar Solo}

  \repeat percent 4 {b8 b b b b b b a b8 b d! d cis cis c! c} |
  \mark \markup{Half Feel}
  \repeat percent 4 {b8 b r4 r b8 a b8 b r4 r8 d! cis c! }

  e,1 ~ e
  e4. g!8 gis b e d! ~ |
  d4 b8 fis' ~ fis^\markup{D.S. al Coda} b, d! e |
\bar "||"
  cis8\coda cis cis cis \tuplet 3/2 {cis4 cis cis} |
  gis1 |
  e1 ~ |
  e1 |
  e4. g!8 gis b e d! ~ |
  d4 b8 fis' ~ fis b, d! e |
  \bar "||"
  \mark \markup{Vocal}
  \repeat percent 4 {
  e,4. g!8 gis b e d! ~ |
  d4 b8 fis' ~ fis b, d! e |
  }
  e1 ~ |
  e1 |
  e,4. g!8 gis b e d! ~ |
  d4 b8 fis' ~ fis b, d! e |
  \bar "|."
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout { }
}
