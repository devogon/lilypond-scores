\version "2.22.1"

\header {
  title = "Let It Be"
  subtitle = "(The Beatles - Let It Be - 1970)"
  instrument = "Bass"
  composer = "McCartney/Lennon"
  poet = "McCartney/Lennon"
}

\paper {
  indent = 0.0\cm
}

global = {
  \key c \major
  \time 4/4
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  e c d\3 g\2 |
  c\1 g2\2 g4\2 |
  a\1 g\2 f c'\1\glissando |
  c,2\3 e4 g\2

}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
