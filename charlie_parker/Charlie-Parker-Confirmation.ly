\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Confirmation"
  instrument = "Bass"
  composer = "Charlie Parker"
  arranger = "Chris Fitzgerald"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
%  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=100
}

contrabass = \relative c {
  \global
  % Music follows here.
  \mark \markup{\box A}
  f4^\markup{Intro Chorus} a, c f | % 1
  e bes' a e
  d a' g d
  c g' f a,
  \break
  bes d f bes | % 5
  a e d a'
  g b,! d f
  g d c e
  \bar "||"
  \mark \markup{\box A}
  \break
  f g a f | % 9
  e gis a a,
  d a' g d
  c e f a,
  \break
  bes d f bes | % 13
  a es d a'
  g b,! c e,!
  f a bes b!
  \bar "||"
  \break
  \mark \markup{\box B}
  c d es e! | % 17
  f g as a!
  bes d, g ges
  f a bes d,
  \break
  es f ges g | % 21
  as c, es d
  des f as fis
  g d c e
  \bar "||"
  \break
  \mark \markup{\box A}
  f a g f | % 25
  e bes' a es
  d a' g b,!
  c e, f a
  \break
  bes d f bes
  a es d a'
  g b,! c e,!
  f a c e
  \bar "||"
  f1
\break
 \mark \markup{\box A}
a4^\markup{Outtro Chorus} g f a | %
g f e g
f e d f
es c f es
\break
d f bes gis
a g fis a
b! a g f
e g c, e
\bar "||"
\break
\mark \markup{\box A}
a g f a
g bes a g
f d g f
es c f e
\break
d f bes gis
a g fis a
bes g e c
f a, d cis
\bar "||"
\break
\mark \markup{\box B}
c es g bes
a bes c cis
d f, c' bes
a bes c bes
\break
des bes ges es
ges bes des c
as f as bes
a g f e
\break
\mark \markup{\box A}
g e f g
a bes a g
f d g f
es c f e
\break
d f bes gis
a es d a'
g d e c
f a, d c
\bar "||"
f,1
\bar "|."
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  f1:maj7
  e2:m7dim
  a:7
  d:m7 g:7
  c:m7 f:7
  bes1:7
  a2:m7 d:7
  g1:7
  g2:m7 c:7
  f1:maj7
  e2:m7dim a:7
  d:m7 g:7
  c:m7 f:7
  bes1:7
  a2:m7 d:7
  g:m7 c:7
  f1:maj7
  c:m7
  f:7
  bes:maj7
  s1
  es:m7
  as:7
  des:maj7
  g2:m7 c:7
  f1:maj7
  e2:m7dim a:7
  d:m7 g:7
  c:m7 f:7
  bes1:7
  a2:m7 d:7
  g:m7 c:7
  f1:maj7
  s1
  f1:maj7
  e2:m7dim a:7
  d:m7 g:7
  c:m7 f:7
  bes1:7
  a2:m7 d:7
  g1:7
  g2:m7 c:7
  f1:maj7
  e2:m7dim a:7
  d:m7 g:7
  c:m7 f:7
  bes1:7
  a2:m7 d:7
  g:m7 c:7
  f1:maj7
  c:m7
  f:7
  bes:maj7
  s1
  es:m7
  as:7
  des:maj7
  g2:m7 c:7
  f1:maj7
  es2:m7dim a:7
  d:m7 g:7
  c:m7 f:7
  bes1:7
  as2:m7 d:7
  g:m7 c:7
  f1:maj7
  s1
}

contrabassPart = \new Staff { \clef bass \contrabass }

chordsPart = \new ChordNames \chordNames

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}
