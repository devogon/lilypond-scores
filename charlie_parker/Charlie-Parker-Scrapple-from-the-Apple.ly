\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Scrapple from the Apple"
  instrument = "Bass"
  composer = "Charlie Parker"
%  meter = "erm"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "Medipm Bop" 4=120
}

contrabass =  {
  \global
  % Music follows here.
     \repeat volta 2 {
   r8 fis g bes a g f! d | % 1
   g8 c' r4 r8 bes4 a8 | % 2
   bes8 g  \tuplet 3/2 {d' bes g} a d g des | % 3
   r4 r8 a8 ~ a bes4 a8 | % 4
   \break
   c8 a \tuplet 3/2 {g f g~}  g4 r8 f' | % 5
   d4 r8 e ~ e d \tuplet 3/2 {a bes b!} | % 6
   c4. a8 bes a gis a | % 7
   }
   \alternative {
     {c' a bes c' r2}
     {f8 c e f r2}
   }
   \bar "||"
   \break
   \improvisationOn
    a4^\markup{Solo} a a a
    a4 a a a
    d4 d d d
    d4 d d d
    \break
    g4 g g g
    g4 g g g
    c4 c c c
    c4 c c c
    \improvisationOff
    \break
    \bar "||"
   r8 fis g bes a g f! d | % 1
   g8 c' r4 r8 bes4 a8 | % 2
   bes8 g  \tuplet 3/2 {d' bes g} a d g des | % 3
   r4 r8 a8 ~ a bes4 a8 | % 4
   \break
   c8 a \tuplet 3/2 {g f g~}  g4 r8 f' | % 5
   d4 r8 e ~ e d \tuplet 3/2 {a bes b!} | % 6
   c4. a8 bes a gis a | % 7
   f8 c e f r2
   \bar "|."

}

chordNames = \chordmode {
  \global
  % Chords follow here.
  g1:7
  c:7
  g:7
  c:7
  f:maj7
  bes2:7 bes:dim7
  f:maj7 g:7
  a:7 d:7
  f1:6
  a\breve:7
  d:7
  g:7
  c:7
  g1:7
  c:7
  g:7
  c:7
  f:maj7
  bes2:7 bes:dim7
  f:maj7 g4:7 c:7
  f1:6
}

contrabassPart = \new Staff { \clef bass \contrabass }

chordsPart = \new ChordNames \chordNames

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}
