\version "2.19.57"
\language "nederlands"

\header {
  title = "Nobody Does It Better"
  composer = "Marvin Hamlisch"
  poet = "Carole Bayer Sager"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g \major
  \time 4/4
  \tempo "Andante con Amore" 4=110
}

contrabass = \relative c {
  \global
  \set Score.markFormatter = #format-mark-box-alphabet
  % Music follows here.
  R1 | % 1
  g'1\mf | % 2
  g2 r | % 3
  g,1 | % 4
  g2 r | % 5
  R1 | % 6
  \break
  \mark \default
  \repeat volta 2 {
    c2 r | % 7
    c2 r | % 8
    g'2 r | % 9
    g2 r4 g,\upbow  | % 10
    c2 r | % 11
    c2 r | % 12
    g'2 r | % 13
    \break
    g2 r4 g,\upbow | % 14
    \mark \default
    c2_\markup{cresc.} r | % 15
    c2 r | % 16
    b2\mf r | % 17
    e2 r | % 18
    a2 r | % 19
    \break
    a2_\markup{dim.} d, | % 20
    g2 r | % 21
    g1 | % 22
  }
  \alternative {
    { g2 r | %
      g,1\downbow | %
      g2 r | %
      r1 | %
    }
    {\tempo "Animato"
     g2 r | %
    }
  }
  b2\mf r | % 28
  c2 r | % 29
  c2 r | % 30
  g2 r | % 31
  b2 r | % 32
  a2. r4 | % 33
  \break
  as2. r4 | % 34
  \mark \default
  g2 r | % 35
  b2_\markup{cresc.} r | % 36
  c2 r | % 37
  c2 r | % 38
  b2 e  | % 39
  a2 d | % 40
  \break
  g,2 r | % 41
  g1\> | % 42
  g,2 r\! | % 43
  r1 | % 44
  \mark \default
  c2\f r | % 45
  c2 r | % 46
  g'2 r | % 47
  \break
  g2 r4 g, | % 48
  c2 r | % 49
  c2 r | % 50
  g'2 r | % 51
  g2 r4 g, | % 52
  \mark \default
  c2 r | % 53
  \break
  c2 r | % 54
  b2 r | % 55
  e2 r | % 56
  a2 r | % 57
  a2 d, | % 58
  g2 r | % 59
  \break
  g1\mf^\markup{\bold rall.}\downbow | % 60
  a2 r | % 61
  a2\upbow d,\upbow | % 62
  g2\p^\markup{\bold molto rall.} r | % 63
  g,1\upbow | % 64
  g1\pp\fermata | % 65
  g4\staccato r r2 | % 66
  \bar "|."
}

\score {
  \new Staff \with {
    instrumentName = "Contrabass"
    shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}

\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
  \hspace #1
}

\markup {
  \teeny
  \date
}