\version "2.18.2"

\header {
  title = "Játék"
  instrument = "contrabas"
  composer = "Szőnyi Erzsébet"
  copyright = "1953 Zeneműkiadó Vállalat, Budapest"
  tagline = ##f
}

\paper {
  indent = 0.0\cm
}

\layout {
  \context {
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 2/4
  \tempo "Allegretto" 2=84
  \compressFullBarRests
  \numericTimeSignature
}

contrabas = \relative c {
  \global
  R2*3 | % 1-3
  r4 a'\upbow\mf^\markup{\tiny "leggiero"} | % 4
  e4 fis | % 5
  g8 (fis) e4 | % 6
  r4 a\upbow | % 7
  e4 fis\< | % 8
  g8 (fis) e (d)\! | % 9
  e4 a, | % 10
  d4 d8 (a) | % 11
  d4 e | % 12
  g8 (fis) e (fis) | % 13
  d4 d8 (a) | % 14
  d4 e | % 15
  g8 (fis) e (d) | % 16
  a2 (a) (a) | % 17-19
  \time 3/4
  R4*3 | % 20
  \time 2/4
  R2*2 | % 21-22
  g'4\mf g8 (d) | % 23
  g4 a | % 24
  c8 (b) a (b) | % 25
  g4 g8 (d) | % 26

  g4 a | % 27
  g8 (f) e (f) | % 28
  e2 (e) (e) | % 29-31
  R2*3 | % 32-34

  r4 d'4\p\upbow | % 35
  a8 (b) c4 | % 36
  g4 a | % 37
  R2*2 | % 38 39
  a8 (b) c4 | % 40
  g4^\markup{\italic \tiny "ril."} a\> | % 41

  b4\! g | % 42
  a2^\markup{\italic "a tempo"} | % 43
  r4 a\mf\upbow | % 44
  e4 fis | % 45
  g8 (fis) e4 | % 46
  r4 a4\upbow\< | % 47

  e4 fis | % 48
  g8 (fis\!) e (d) | % 49
  e8 fis g a | % 50
  b2\f (b) (b) | % 51-53
  c2 ( |% 54
  c) (c\downbow) (c) (c) | % 55-58
  R2 | % 59
  r4 a8\downbow\f g | % 60
  a4 g8 (a) | % 61
  c!4 a8 g | % 62

  a4 g8 (a) | % 63
  c!8 (b) a g | % 64
  a2 (| % 65
  a2) (| % 66
  a8) r8 r4 | % 67
  a,8^\markup{\tiny \italic "pizz."} r8 r4 | % 68
  \bar "|."
}

\score {
  \new Staff { \clef bass
               \relative c
               \contrabas
             }
  \layout {
    \context {
      \Score
    \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/18)}}
    }