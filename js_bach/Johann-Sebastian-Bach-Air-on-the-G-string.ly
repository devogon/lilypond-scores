\version "2.19.35"
\language "nederlands"

\header {
  title = "Air on the G-string"
  subtitle = "BWV 1068"
  instrument = "Contrabas"
  composer = "Johann Sebastian Bach"
  meter = "40"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo "Air" 4=40
}

contrabass = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  d'8 r cis r b r a r |
  g r gis r a r g! r |
  fis r e r dis r b' r |
  \break

  e, r d r cis r a' r |
  d r cis r b r gis r |
  }
  \alternative {
    { a r e r a,4 r }
    { a'8 r e r a,2 }
  }
  \break
  \repeat volta 2 {
    \set Score.currentBarNumber = #7
    a'8 r g r fis r e r |
    dis r b r e r d! r |
    cis r b r ais r cis r |
    b r fis' r b r a r |
    \break
    gis r fis r e r d r |
    cis r e r a r g r |
    fis r g r gis r a r |
    ais r b r e r d! r |
    \break
    cis r a r d r c r |
    b r a r g r fis r |
    e r d r a r d r |
    \override Score.RehearsalMark
  #'break-visibility = #begin-of-line-invisible
  a' r a, r d2\mark \markup { \musicglyph #"scripts.ufermata" } |
  }
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}
