\version "2.16.2"
\language "nederlands"
#(set-global-staff-size 28)

\header {
  title = "Johannes Passion"
  subtitle = "koraal nr 21"
  composer = "Johann Sebastian Bach"
%  meter = "4/4"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 4/4
  \tempo adagio % 4=66
}

cello = \relative c' {
  \global
  % Music follows here.
  e4 e e e | % 1
  d4 c b2 | % 2
  c4 d e e | % 3
  d c b2 | % 4
  a4 b c a| % 5
  a8 g f4 e2 | % 6
  f4 g a a | % 7
  g4 f e2 | % 8
  a4 gis a b | % 9
  c4 b a2 | % 
  c4 d e e | % 
  d4 c b2 | % 
  a4 b c a | % 
  a8 g f4 e2 | % 
  f4 g a a | % 
  g4 f e2 (| % 
  e1) | \bar "|:"% end
  
}

contrabass = \relative c {
  \global
  % Music follows here.
    e4 e a g       | % 1
    f4 e8 (d) e2   | % 2
    a4 g cis, cis  | % 3
    d4 a e'2       | % 4
    a4 gis a f     | % 5
    cis4 d a2      | % 6
    d4 c f, fis    | % 7
    g4 gis a2      | % 8
    f'4 e8 (d) c4 b | % 9
    a4 b8 (c) d2    | % 10
    ees4 d cis d8 (e) | % 11
    f8 (g) a4 gis2 | % 12
    fis4 gis a a,8 (b) | % 13
    cis4 d a2 | % 14
    d4 c f,8 (g) a b | % 15
    c4 d e2 (| % 16
    e1) | \bar "|:"
}

celloPart = \new Staff \with {
%  instrumentName = \markup {\teeny "Cello"}
} { \clef bass \cello }

contrabassPart = \new Staff \with {
 % instrumentName = \markup {\teeny "Contrabass"}
} { \clef bass \contrabass }

\score {
\new StaffGroup <<
  <<
    \celloPart
    \contrabassPart
  >>
  >>
  \layout { }
}
\markup {
  \teeny
  \date
}