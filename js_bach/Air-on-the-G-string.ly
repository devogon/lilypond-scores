\version "2.19.82"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Air on the G-string"
%  subtitle = "BWV 1068"
  piece = "BWV 1068, mvmt 2"
  instrument = "Contrabas"
  composer = "Johann Sebastian Bach"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo "Air" 4=40
}

contrabass = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    d8 d' cis cis, b b' a a, | %  1
    g8 g' gis gis, a a' g! g,! | % 2
    fis fis' e e, dis dis' b b' | % 3
    e,, e' d d, cis cis' a a' | % 4
    \break
    d,8 d' cis cis, b b' gis e | % 5
  }
  \alternative {
    {a8 d, e e, a16 (b cis d e g fis e) }%| % 6
    {a8 d, e e, a2}
  }
  \break
  \repeat volta 2 {
    a8 a' g g, fis fis' e e, | % 8
    dis8 dis' fis b, e e' d! d,! | % 9
    cis8 cis' b b, gis b cis a | % 10
    \break
    b8 g' e fis b, b' a a, | % 11
    gis8 gis' fis fis, e e' d d, | % 12
    cis8 cis' d e a, a' g g, | % 13
    \break
    fis8 fis' g g, gis gis' a a, | % 14
    ais8 ais' b b, e e' d d, | % 15
    cis cis' a cis d d, c! c'! | % 16
    \break
    b8 b, a a' g g, fis fis' | % 17
    e8 e, d d' cis a d g | % 18
    a8 g a a, d,2
    \fermata
  }
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
