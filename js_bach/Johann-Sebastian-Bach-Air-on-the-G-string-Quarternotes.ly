\version "2.19.82"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Air on the G-string"
  subtitle = "quarter notes"
  instrument = "Contrabas"
  composer = "Johann Sebastian Bach"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo "Air" 4=40
}

contrabass = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  d4\f^\markup{pizzicato} d' cis cis,
  b4 b' a a,
  g g' gis gis,
  a a' g g,
  fis fis' e e
  dis fis b, dis
  e, e' d d | % 7
  cis e a, cis | % 8
  d d' cis cis,
  b a gis e' | % 10
  a, d e e, | % 11
  }
  \alternative {
    {a8 b cis d e g fis e}
    {a,4 a' e cis}
  }
  \repeat volta 2 {
    a4 a' g g, | % 14
    fis fis' e e,
    dis' dis fis b,
    e, e' d d | % 17
    cis cis' b b, | % 18
    ais b cis fis,
    b g' e fis | % 20
    b, b' g r | % 21
    d2\ff^\markup{arco} ~ d8 fis e d | % 22
    b'2. a8 gis | % 23
    fis16 e a8 a,4 b4. cis16 d | % 24
    cis4. b8 a4 r
    fis4\mf fis' g g, | % 26
    gis4 gis' a a,
    ais4\< ais' b b, | % 28
    e,4\!\f e' d d
    cis e a, cis | % 30
    d d' c! c, | % 31
    b b' a, d
    g, g' fis fis,
    e e' d d | % 34
    cis a d g,
    a g a a' | % 36
  }
  \alternative {
    {d,2. r4}
    {d'2 d,4\fermata r}
  }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
  } { \clef bass \contrabass }
  \layout { }
}
