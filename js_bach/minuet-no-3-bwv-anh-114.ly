\version "2.19.57"

\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Minuet No. 3"
  subtitle = "BWV Anh. 114"
  composer = "J. S. Bach"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 3/4
%  \tempo "Allegretto"
  \set Score.markFormatter = #format-mark-box-alphabet
}

celloA = {
   \global
   \tempo  "Allegretto"
   \mark \markup{\bold \box A}
   g4\downbow\mf c8_\markup{\italic "con grazia"} (d) e f | % 1
   g4 c4\staccato\upbow ~ c\staccato\upbow | % 2
   a4 f8 (g) a b | % 3
   c'4 c\staccato\upbow ~ c\upbow\staccato | % 4
   f4 g8 (f) e d | % 5
   \break
   e4 f8 (e) d c | % 6
   b4 c8 (d) e c | % 7
   \grace e8\> (d2.) | % 8
   g4\!\downbow\mp c8 (d) e f | % 9
   g4 c\staccato ~ c\staccato | % 10
   \break
   a4 f8 (g) a b | % 11
   c'4 c\staccato ~ c\staccato  | % 12
   f4 g8 (f) e d | % 13
   e4 f8 (e) d c | % 14
   d4_\markup{\italic "2a volta"} e8 (d) c\> b, | % 15
   c2.| % 16
}

celloAa = {
   \global
   %\tempo  "Allegretto"
   \mark \markup{\bold \box A}
   g4\downbow\mf c8_\markup{\italic "con grazia"} (d) e f | % 1
   g4 c4\staccato\upbow ~ c\staccato\upbow | % 2
   a4 f8 (g) a b | % 3
   c'4 c\staccato\upbow ~ c\upbow\staccato | % 4
   f4 g8 (f) e d | % 5
   \break
   e4 f8 (e) d c | % 6
   b,4 c8 (d) e c | % 7
   \grace e8\> (d2.) | % 8
   g4\!\downbow\mp c8 (d) e f | % 9
   g4 c\staccato ~ c\staccato | % 10
   \break
   a4 f8 (g) a b | % 11
   c'4 c\staccato ~ c\staccato  | % 12
   f4 g8 (f) e d | % 13
   e4 f8_\markup{\italic "calando"} (e) d c | % 14
   d4 e8 (d\>) c_\markup{\italic rit.} b, | % 15
   c2.\!\p | % 16
}

celloB = {
  \break
  \mark \markup {\bold \box B}
  e'4\!\f\downbow c'8 (d') e' c' | % 17
  d'4 g8 (a b g) | % 18
  c'4 a8 b c' g | % 19
  fis4\> e8 fis d4 | % 20
  d8\!\p\< (e) fis g a b | % 21
  \break
  c'4\! b a  | % 22
  b4 d\staccato (fis\staccato) | % 23
  g2.\> | % 24
  g4\!\downbow\p c8 b, c4 | % 25
  a4 c8 b, c4 | % 26
  \break
  g4\staccato (f\staccato) e\staccato | % 27
  d8 (c) b, c d4 | % 28
  g,8\< (a,) b, c d e | % 29
  f4\! e d | % 30
  e8 (f) c4\staccato\> (b,\staccato)  | % 31
  c2. | % 32
  \break
}

celloC = \relative c {
  \key ees \major
  \mark \markup{\bold \box C}
  es'4\!\downbow\mp_\markup{\italic dolce} d\upbow (c\upbow)  | % 49
  d4 (g,) g | % 50
  c4 c,8 (d) es f | % 51
  g2 g4 | % 52
  as4 bes8(as) g f | % 53
  g4 as8 (g) f es | % 54
  \break
  f4 g8 (f) es\> f | % 55
  d2. | % 56
  es'4\!\downbow\mp_\markup{\italic dolce} d4\tenuto (c) | % 57
  d4 (g,) g | % 58
  c4 c,8 (d) es f | % 59
  \break
  g2\< g4 | % 60
  bes4\! c8 (bes) as g | % 61
  as4 bes8 (as) g f | % 62
  g4 c f,\> | % 63
  es2. | % 64
  \break
}
celloD = \relative c {
  \mark \markup{\box D}
  g'4\f_\markup{\italic rin}\! es8 (f) g a! | % 65
  bes4 c (d) | % 66
  es4 c8 (d) es c | % 67
  d4 (c8 d) bes4 | % 68
  bes,8\mf (c) d es f g | % 69
  \break
  as4 g f | % 70
  bes4 es,\tenuto\> (d\tenuto) | % 71
  es2. | % 72
  c4\p\downbow\! g'8 f g4 | % 73
  c,4 as'8 g as4 | % 74
  c,8 (g') b,! (f') c (es) | % 75
  \break
  d2. | % 76
  g,8\downbow\mf (a!) b!\upbow c d es | % 77
  f4\upbow es d | % 78
  es8\>_\markup{\italic "molto rit."} (f16 g) c,4\tenuto (b!\tenuto) | % 79
  c2.\!\pp
  \break
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \repeat volta 2 {\celloA} \celloB \celloB \celloC \celloD \celloAa \bar "|."}
  \layout { }
}

