\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "White Wedding"
  subtitle = "1982"
  composer = "Billy Idol"
  poet = "bass: Phil Feit"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


global = {
  \key c \major
  \time 4/4
  \tempo "Pop Rock" 4=143
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
  \global
  r4^\markup{\box Intro} r2.\f
  \repeat unfold 6 {b8 b d e fis fis e d}
  \repeat unfold 8 {e,8}
  \repeat unfold 8 {d'8}
  \repeat unfold 2 {b8 b d e fis fis e d}
  \repeat unfold 8 {d8}
  \repeat unfold 8 {e,8}
  \repeat unfold 32 {8}
  \bar "||"
  \break
  b'8^\markup{\box Verse}
  \repeat unfold 15 {b}
  \repeat unfold 8 {a8}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b'8}
  \repeat unfold 8 {a8}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b'8}
  \repeat unfold 16 {a8}
  \repeat unfold 16 {b8}
  \bar "||"
  \break
  a8^\markup{\box Chorus}
  \repeat unfold 7 {a}
  \repeat unfold 8 {e}
  \repeat unfold 8 {b'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  \repeat unfold 8 {d'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  d'1
  \repeat unfold 8 {b8}
  \repeat unfold 8 {b}
  \bar "||"
  \break
  b8^\markup{\box Verse}
  \repeat unfold 15 {b}
  \repeat unfold 8 {a}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b'}
  \repeat unfold 8 {a}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b'}
  \repeat unfold 16 {a}
  \repeat unfold 16 {b}
  \bar "||"
  \break
    a8^\markup{\box Chorus}
  \repeat unfold 7 {a}
  \repeat unfold 8 {e}
  \repeat unfold 8 {b'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  \repeat unfold 8 {d'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  d'1
  \repeat unfold 8 {b8}
  \repeat unfold 16 {b}
  \bar "||"
  \break
  b8^\markup{\box Interlude} % 22 bars
  b b d e fis fis e d
  b b d e fis fis e d
  \repeat unfold 8 {e,}
  \repeat unfold 8 {d'}
  b b d e fis fis e d
  b b d e fis fis e d
  \repeat unfold 8 {d}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b}
  \repeat unfold 16 {e,}
  \repeat unfold 16 {b'}
  \repeat unfold 8 {d}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b}
  \repeat unfold 8 {d}
  \repeat unfold 8 {e,}
  \repeat unfold 16 {b'}
  % \repeat unfold 8 {d}
  % \repeat unfold 8 {e,}
  % \repeat unfold 24 {b'}
  \bar "||"
  \break
   b8^\markup{\box Verse}
  \repeat unfold 15 {b}
  \repeat unfold 8 {a}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b'}
  \repeat unfold 8 {a}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b'}
  \repeat unfold 16 {a}
  \repeat unfold 16 {b}
  \bar "||"
  \break
  a8^\markup{\box Chorus}
  \repeat unfold 7 {a}
  \repeat unfold 8 {e}
  \repeat unfold 8 {b'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  \repeat unfold 8 {d'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  d'1
  \repeat unfold 8 {b8}
  \repeat unfold 24 {b}
  \bar "||"
  \break
  b8^\markup{\box Interlude}
  \repeat unfold 111 {b8}
  \bar "||"
  \break
  e,8^\markup{\box Outro}
  \repeat unfold 7 {e}
  \repeat unfold 8 {d'}
  \repeat unfold 16 {b}
  \repeat unfold 8 {d}
  \repeat unfold 8 {e}
  \repeat unfold 16 {b}
  \repeat unfold 8 {e,}
  \repeat unfold 8 {d'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {b}
  \repeat unfold 8 {e,}
  \repeat unfold 8 {d'}
  \repeat unfold 8 {b}
  \repeat unfold 8 {b}
  \bar "|."
}
\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}