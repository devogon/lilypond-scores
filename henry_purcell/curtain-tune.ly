\version "2.18.2"

\header {
  dedication = ""
  title = "13 Curtain Tune"
  instrument = "Contrabas"
  composer = "H. Purcell"
  copyright = "© JPv Coolwijk"
  tagline = ""
}
\layout {
  indent = 0.0\cm
}

%#(set-global-staff-size 26)

\score {
  \relative c {
    \clef bass
    \time 3/4
    \key bes \major
    g4 r d' | % 01
    \repeat volta 2 {
      ees4 r e! | % 02
      fis4 r a | % 03
      bes d d, | % 04
      g,4 r d' | % 05
      ees4 r e! | % 06
      fis4 r a | % 07
      bes4 d d,  | % 08
      \break
      g,4 r d' | % 09
      ees4 r e! | % 10
      fis4 r a | % 11
      bes4 d d,  | % 12

      g,4 r d' | % 13
      ees4 r e! | % 14
      fis4 r a | % 15
      \break
      bes4 d d,  | % 16

      g,4 r d' | % 17
      ees4 r e! | % 18
      fis4 r a | % 19
      bes4 d d,  | % 20

      g,4 r d' | % 21
      ees4 r e! | % 22
      fis4 r a | % 23
      \break
      bes4 d d,  | % 24
      g,4 r d' | % 25
      ees4 r e! | % 26
      fis4 r a | % 27
      bes4 d d,  | % 28
      g,4 r d' | % 29
      ees4 r e! | % 30
      fis4 r a | % 31
      \break
      bes4 d d,  | % 32
      g,4 r d' | % 33
      ees4 r e! | % 34
      fis4 r a | % 35
      bes4 d d,  | % 36
      g,4 r d' | % 37
      ees4 r e! | % 38
      fis4 r a | % 39
      \break
      bes4 d d,  | % 40



      g,4 r d' | % 41
      ees4 r e! | % 42
      fis4 r a | % 43
      bes4 d d,  | % 44
      g,4 r d' | % 45
      ees4 r e! | % 46
      fis4 r a | % 47
      \break
      bes4 d d,  | % 48

      g,4 r d' | % 49
      ees4 r e! | % 50
      fis4 r a | % 51
      bes4 d d,  | % 52

      g,4 r d' | % 53
      ees4 r e! | % 54
      fis4 r a | % 55
      \break
      bes4 d d,  | % 56

      g,4 r d' | % 57
      ees4 r e! | % 58
      fis4 r a | % 59
      bes4 d d,  | % 60
      g,4 r d' | % 61
      ees4 r e! | % 62
      fis4 r a | % 63
      \break
      bes4 d d,  | % 64
      g,4 r d' | % 65
      ees4 r e! | % 66
      fis4 r a | % 67
      bes4 d d,  | % 68
      g,4 r d' | % 69
      ees4 r e! | % 70
      
      fis4 r a | % 71
      \break
      bes4 d d,  | % 72
      g,4 r d' | % 73
      ees4 r e! | % 74
      fis4 r a | % 75
      bes4 d d,  | % 76
      \break
      g,4 r d' | % 77
      ees4 r e! | % 78
      fis4 r a | % 79
      bes4 d d,  | % 80
    } \alternative {
      {g,4 r d'}
      {g,4 r r}
    }
    \bar "|."
    }
  }
%}
