\version "2.19.81"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Abdelazer Suite or"
  subtitle = "the Moor's Revenge"
  instrument = "Contrabas"
  composer = "Henry Purcell"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  page-breaking = #ly:page-turn-breaking
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \time 4/4
  %\tempo 4=100
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

overture = \relative c {
  \global
  \key f \major
  % Music follows here.
  \repeat volta 2 {
  d2\f a | % 1
  d2. r16 f e d | % 2
  cis4 r16 c d e a,4 r16 a b! c | % 3
  d2 a2 | % 4
  bes4 r16 bes a g fis4 r16 fis g a | % 5
  \break
  g4 r16 a' g f e4. d16 c | % 6
  f,2 f'4 r16 a g f | % 7
  e4 r16 e f g c,4 r16 c d e | % 8
  a,4 r16 a bes c f,4 r16 f g a | % 9
  \break
  bes4 r16 bes c d g,4 r16 g a bes  | % 10
  c4 r16 c d e f4 r16 f g a | % 11
  bes4 r16 g a bes c4. c,8 | % 12
  f1 | % 13
  e1 | % 14
  d2 ~ d4. c8 | % 15
  \break
  bes1 | % 16
}
  \alternative {
    {a4. bes8 a g f e}
    {a2. r4}
  }
  \repeat volta 2 {
    R1*4 | % 19-22
    r2 r4 r8 a' | % 23
    bes8 a a d, cis a' a c,! | % 24
    \break
    bes8 a' a g a4 a,8 g | % 25
    f8 g a4 d r8 d | % 26
    a4 r8 a d4 r8 d | % 27
    c4 r8 c f4 r8 f | % 28
    e4 r8 e a g f e | % 29
    \break
    f8 d d a f d' d f, | % 30
    e8 d' d cis d g, g c! | % 31
    c8 a a d d bes c c | % 32
    f,4 r8 f g4 r8 g | % 33
    a4 r8 a' e4 r8 e | % 34
    \break
    f4 r8 f cis4 r8 cis | % 35
    d8 bes' bes a c, bes' bes a | % 36
    bes,8 a' a g a g g fis | % 37
    g8 g, g c c f, f g | % 38
    g8 e e a a f g g' | % 39
    \break
    c,4 r8 c d4 r8 d | % 40
    e4 r8 cis d4 r8  b! | % 41
    gis4 r8 gis a d e e, | % 42
    a8 a' a f f bes bes e, | % 43
    \break
    e8 a a d, g g, a a' | % 44
    d,8 d d a a c c g | % 45
    g8 bes bes f g g' a a, | % 46
  }
  \alternative {
    {d2. r4}
    {d1}
  }
}

rondeau = \relative c {
  \global
  \key f \major
  % Music follows here.
  \time 3/2
  \tempo 2=75
  \set Score.currentBarNumber = #49
  \repeat volta 2 {
    d2 d c | % 49
    bes2 g a | % 50
    f1 g2 | % 51
    e1 f2 | % 52
    d'2 d e | % 53
    cis1 d2 | % 54
    f,2 a bes | % 55
    a1 d2 | % 56
  }
    \break
  \repeat volta 2 {
    f1 e2 | % 57
    d2 bes c | % 58
    f2 e d | % 59
    c2 bes a | % 60
    bes 2 a g  | %
    c2 bes a | %
    d2 c bes | %
    c2 c f, | % 64
  }
  \break
  d'2 d c | % 65
  bes2 g a | % 66
  f1 g2 | % 67
  e1 f2 | % 68
  d'2 d e | % 69
  cis1 d2 | % 70
  g,2 a bes | % 71
  a1 d2 | % 72
  \break
 % \repeat volta 2 {
    a'1^\markup{\teeny "repeat -to 80- removed"} a,2 | % 73
    e'1 e,2 | % 74
    a1 d2 | % 75
    e,1 a2 | %
    a'1 a,2 | % 77
    d1 d2 | % 78
    cis1 a2 | % 79
    d4 c bes2 a | % 80
 % }
  \break
  d2 d c | % 81
  bes2 g a | % 82
  f1 g2 | % 83
  e1 f2 | % 84
  d'2 d e | % 85
  cis1 d2 | % 86
  g,2 a bes | % 87
  a1 d2 | % 88
  \bar "|."
}

air = \relative c {
  \global
    \time 2/2
  \tempo 2=90
  \key d \major
    \set Score.currentBarNumber = #89
  % Music follows here.
  \partial 4 r4 | %
  \repeat volta 2 {
    r2 r4 d | %  89
    fis4. g8 a4 d | % 90
    a,2 a4 a | % 91
    \break
    d4. e8 fis4 d | % 92
    b b' g e | % 93
    a4. b8 cis4 a | % 94
    fis2 d4 b | % 95
    \break
    e4. fis8 gis4 e | % 96
    cis4 d e e, | % 97
  }
 \alternative {
   {a2. r4}
   {a2. r4}
 }
 \repeat volta 2 {
   r2 r4 d'4 | % 100
  a2 a4 a | % 101
  e4. fis8 e4 fis | % 102
\break
  g4.a8 g4 a | % 103
  b2 b4 e, | % 104
  b2 b4 e | % 105
  c!4 a b2 | % 106
  \break
  e,2. e'4 | % 107
  a4. b8 cis8 b a g | % 108
  fis8 g a g fis e d4 | % 109
  \break
  g2 r4 e | % 110
  a2 r4 fis | % 111
  b2 r4 b | % 112
  cis4 a d d, | % 113
  g4 e a a, | % 114
 }
 \alternative {
   {d2. r4}
   {d1}
 }
 \bar "|."
}

airfour = \relative c {
  \global
    \time 2/2
  \tempo 4=100
  \key g \major
    \set Score.currentBarNumber = #117
  % Music follows here.
  \repeat volta 2 {
    g'4. g16 a b4 g | % 117
    d'4 d, g4. fis16 e | % 118
    d8. d16 d8 c16 b a8 a16 b c8 b16 a | % 119
    \break
    e'8. e16 d8 e16 d c8 c 16 d e8 d16 c | % 120
    g'8. g16 g8 a16 g fis8. fis16 fis8 g16 fis | % 121
    e8. e16 a8. a,16 d4. d'8 | % 122
    \break
    cis8. a16 d8. d,16 a'4. a16 b | % 123
    cis8. a16 d8. d,16 a'8. a16 d8. d,16 | % 124
  }
  \alternative {
    {g8. e16 a8. a,16 d8 d'16 c! b8 b16 a}
    %\break
    {g8. e16 a8. a,16 d4 ~ d8 d}
  }
  \break
  \repeat volta 2 {
    g8 g16 fis e8. e16 b'8 b16 a g8 g16 fis | % 127
    e8. a16 b8. b,16 e4 r8 e | % 128
    \break
    fis8. fis16 gis8. gis16 a8. a,16 d8. d16 | % 129
    e,8. g16 e'8. e,16 a4 r8 a'16 g! | % 130
    fis8. fis16 g8. g,16 d'8. d16 g8. g16 | % 131
    \break
    fis8. fis16 g8. g,16 d'8 d16  e fis8. d16 | % 132
    g8 g16 a b8. g16 c8 a16 b c8. a16 | % 133
    \break
    d4 r8 d, g8. g,16 d'8. d'16 | % 134
  }
  \alternative {
    {e8. c16 d8. d,16 g4 ~ g8 d}
    {e'8. c16 d8. d,16 g4 ~ g8 r}
  }
  \bar "|."
}

minuet = \relative c {
  \global
    \time 3/4
  \tempo 4=90
  \key g \major
    \set Score.currentBarNumber = #137
  % Music follows here.
  g'2 g4 | % 137
  fis2 fis4 | % 138
  e4 a a, | % 139
  d4 d'8 c b a | % 140
  g2 g4 | % 141
  fis2 fis4 | % 142
  e4 a a, | % 143
  \break
  d2. | % 144
  \repeat volta 2 {
    g2 fis4 | % 145
    g2 c,4 | % 146
    g'2 fis4 | % 147
    e4 c8 d e (c) | % 148
    d2 c4 | % 149
    b2 g4 | % 150
    \break
    d'2 c4 | % 151
    b2 g4 | % 152
    d'2 c4 | % 153
    b4 e8 d c b | % 154
    a4 d d | % 155
  }
  \alternative {
    {g,2 ~ g8 r}
    {g2.\fermata}
  }
  \bar "|."
}

airsix = \relative c {
  \global
    \time 4/4
  \tempo 2=60
  \key bes \major
    \set Score.currentBarNumber = #158
  % Music follows here.
  \partial 8 bes'16 a  | %
  \repeat volta 2 {
    g8 d' a d bes g d' d, | % 158
    g4 fis g fis | % 159
    g4 a bes a | % 160
    bes4 c d8 c bes a | % 161
    \break
    bes8 g c, d es4 es | % 162
  }
  \alternative {
    {d2 ~ d4. bes'16 a}
    {d,2 ~ d4. g16 a}
  }
  \repeat volta 2 {
  bes8^\markup{\italic \underline "does this repeat from here?"} f d es16 f g8 es c f | % 165
  \break
  bes,2. a4 | % 166
  g4  es' f8 f16 g f es d c | % 167
  bes4 es a, d | % 168
  g,8 a bes d es es f a | % 169
  \break
  bes8 a16 g f es d c bes4 r8 bes' | % 170
  a4 g f es | % 171
  d4 c bes8 d es c | % 172
  d8 d'16 es d c bes a g4 f! | % 173
  \break
  es4 d g f | % 174
  es4 d g d | % 175
  }
  \alternative {
    {g,2 ~ g4. g'16 a}
    {g,1\fermata}
  }
}

jig = \relative c {
  \global
    \time 6/8
  \tempo 4=100
  \key bes \major
    \set Score.currentBarNumber = #178
  % Music follows here.
  \partial 8 d8 |
  \repeat volta 2 {
    g4. r4 d8 | % 178
    g4. r4 d8 | % 179
    g4. r4 d8 | % 180
  }
  \alternative {
    {g4. r4 d8}
    {g4. r4  g8}
  }
  \repeat volta 2 {
    d'4. r4 g,8 | % 183
    c4. r4 f,8 | % 184
    \break
    bes4. r4 f8 | % 185
    bes,4. r4 bes8 | % 186
    g4. r4 c8 | % 187
    a4. r4 g8 | % 188
    c4. r4 d8 | % 189
  }
  \alternative {
    {g,4. ~g8 r g'} % 190
    {g,2.} % 191
    }
    \bar "|."
}

hornpipe = \relative c {
  \global
    \time 3/4
  \tempo 4=60
  \key d \major
    \set Score.currentBarNumber = #89
  % Music follows here.
}

airnine = \relative c {
  \global
  \defaultTimeSignature
    \time 2/2
  \tempo 4=60
  \key bes \major
  \set Score.currentBarNumber = #205
  % Music follows here.
  \repeat volta 2 {
    \partial 4 g4  | %
    g'2 fis | % 205
    g2 f2 | % 206
    es2 es | % 207
    d4 d8 es d c bes a | % 208
    g2 a | % 209
    bes2 bes' a f | % 210
    \break
    g4 d es c | % 211
    f4 c f d | % 212
    g4 es f f, | % 213
  }
  \alternative {
    {bes2 ~ bes8 r g4}
    {bes2 ~ bes8 r bes4}
  }
  \break
  \repeat volta 2 {
    bes'2 bes, | % 216
    f'2 f4 c | % 217
    d4 bes c c | % 218
    f,2 r4 bes' | % 219
    d4 g, r a | % 220
    c4 f, r bes | % 221
    \break
    d4 g, r a | % 222
    c4 f, r g | % 223
    bes e,! r a | % 224
    f4 e! d e | % 225
    f g a a, | % 226
    d2 d'4 c | % 227
   \break
   b!4 a b g | % 228
   c4 c, g' f | % 229
   es2 es | % 230
   d4 e! fis d | % 231
   g,4 g' f f, | % 232
   \break
   es'4 es d c | % 233
   bes4 g d' d | % 234
  }
  \alternative {
    {g,2 ~g8 r bes4}
    {g1}
  }
  \bar "|."
}

\score {
  \new Staff { \clef bass \overture }
  \layout {} \header { piece = "I. Overture"}
}

\score {
  \new Staff { \clef bass \rondeau }
  \layout {} \header { piece = "II. Rondeau"}
}

\score {
  \new Staff { \clef bass \air }
  \layout {} \header { piece = "III. Air"}
}

\score {
  \new Staff { \clef bass \airfour }
  \layout {} \header { piece = "IV. Air"}
}

\score {
  \new Staff { \clef bass \minuet }
  \layout {} \header { piece = "V. Minuet"}
}

\score {
  \new Staff { \clef bass \airsix }
  \layout {} \header { piece = "VI. Air"}
}

\score {
  \new Staff { \clef bass \jig }
  \layout {} \header { piece = "VII. Jig"}
}

\score {
  \new Staff { \clef bass \air }
  \layout {} \header { piece = "VIII. Hornpipe"}
}

\score {
  \new Staff { \clef bass \airnine }
  \layout {} \header { piece = "IX. Air"}
}