\version "2.19.57"
\language "nederlands"

\header {
  title = "When I Am Laid In Earth"
  subtitle = "Dido and Aeneas"
%  subsubtitle = \markup \center-column { \override #'(line-width . 100) \tiny \justify {
%  subsubtitle = \markup{\teeny  \justify {This famous example of a ground bass is arranged here as a duet; the five-bar phrase, first heard alone, is repeated three times under the melody.}}
  instrument = "Contrabas"
  composer = "Henry Purcell"
  meter = "Largo"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  % bookTitleMarkup = \markup {
  %   \column {
  %     \fill-line { \fromproperty #'header:title }
  %     \null
  %     \justify-field #'header:intro
  %   }
  % }

}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}


global = {
  \key bes \major
  \numericTimeSignature
  \time 3/2
  \tempo 4=100
}

intro = "This famous example of a ground bass is arranged here as a duet; the five-bar phrase, first heard alone, is repeated three times under the melody."

contrabassGround = \relative c' {
  \global
  \clef bass
  g1\pp g2 | % 1
  fis1 f!2 | % 2
  e!1 es2 | % 3
  d1 bes2 | % 4
  c2 d2 (d) | % 5
  \bar "||"
}

contrabassII = \relative c' {
  \global
  % Music follows here.
  % g'1\pp g2 | % 1
  % fis1 f!2 | % 2
  % e!1 es2 | % 3
  % d1 bes2 | % 4
  % c2 d2 (d) | % 5
  % \bar "||"
  % \break
  g1\mp g2 | % 6
  fis1 f!2 | % 7
  e!1 es2 | % 8
  d1 bes2 | % 9
  c2 d2 (d) | % 10
  g1 g2 | % 11
  fis1 f!2 | % 12
  e!1 es2 | % 13
  d1 bes2 | % 14
  c2 d2 (d) | % 15
  g1 g2 | % 16
  fis1 f!2 | % 17
  e!1 es2 | % 18
  d1 bes2 | % 19
  c2 d2 (d) | % 20
  g,1. | % 21
}

contrabassI = \relative c {
  \global
  % Music follows here.
				%  R1*3/2*5 | % 1-5
  \override TextSpanner #'dash-fraction = #'()
  g'2\mp (a) bes | % 6
  bes2 (a) b! | % 7
  c4. (bes8) a4. (g8) fis4. (g8) | % 8
  fis1\> d'4.\! (es8) | % 9
  d4. (c8) bes2 a | % 10
  bes1\< es2\!\f\startTextSpan | % 11
  es4 a, a2 d | % 12
  d4\stopTextSpan g, a2 g | % 13
  fis1\> d'4.\! (es8) | % 14
  d4. (c8) bes2 c4 a | % 15
  bes4.\< (c8) d2\!\startTextSpan es\f | % 16
  es4 a, a2 d | % 17
  d4\stopTextSpan g,\startTextSpan a2\> g | % 18
  a1\! d2\mp | % 19
  c4\stopTextSpan bes bes2 a4 g | % 20
 % g1.\> s1*0\!
 g1.\> | % 21
  \bar "|."
  <>\!
}

contrabassIPart = \new Staff \with {
  midiInstrument = "contrabass"
} { \clef bass \contrabassI }

contrabassIIPart = \new Staff \with {
  midiInstrument = "contrabass"
} { \clef bass \contrabassII }


\score {
%  \new GrandStaff <<
  \contrabassGround
%    \contrabassIIPart
%  >>
  \header {
    piece = \markup{\tiny "ground bass"}
  }
  \layout { }
  \midi { }
}

\score {
  \new GrandStaff <<
    \contrabassIPart
    \contrabassIIPart
  >>
  \header {
%    piece = "ground bass"
  }
  \layout { }
  \midi { }
}

\markup \center-column { \override #'(line-width . 100) \tiny \justify {
  \line {This famous example of a ground bass is arranged here as a duet;}
  \line {the five-bar phrase, first heard alone, is repeated three times under the melody.} }}
\markup { \vspace #4 }

\markup {
  \teeny
  \date
}