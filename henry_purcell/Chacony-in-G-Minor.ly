\version "2.19.57"
\language "nederlands"

\header {
  title = "Chacony in G Minor"
  subtitle = "Z. 730"
%  instrument = "Basso"
  composer = "Henry Purcell"
  copyright = \markup \small {"R. D. Tennent 2012. Licensed under a Creative Commons Attribution-Share Alike  license."}
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
  \tempo 4=80
}

contrabass = \relative c' {
  \global
				% Music follows here.
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
  g2 g4 | % 1
  fis4 fis4.. fis16\< | % 2
  g4\! es2\tenuto | % 3
  d4 d4.. d16 | % 4
  b!4 b4.. b16 | % 5
  c4 c2 | % 6
  d2.\< | % 7
  \break
  g,2.\> | % 8
  g'2\! g4 | % 9
  fis4 fis4.. fis16\< | % 10
  g4\! es2\tenuto | % 11
  d4 d4.. d16 | % 12
  b!4 b4.. b16 | % 13
  c4 c2 | % 14
  d2.\< | % 15
  \break
  g,2.\> | % 16
  g'2\! g4 | % 17
  fis4 fis4.. fis16\< | % 18
  g4\! es2\tenuto | % 19
  d4 d4.. d16 | % 20
  b!4 b4.. b16 | % 21
  c4 c2 | % 22
  d2.\< | % 23
  \break
  g,2.\> | % 24
  g'2\! g4 | % 25
  fis4 fis4.. fis16\< | % 26
  g4\! es2\tenuto | % 27
  d4 d4.. d16 | % 28
  b!4 b4.. b16 | % 29
  c4 c2 | % 30
  d2.\< | % 31
  \break
  g,2.\> | % 32
  g'2\! g4 | % 33
  fis4 fis4.. fis16\< | % 34
  g4\! es2\tenuto | % 35
  d4 d4.. d16 | % 36
  b!4 b4.. b16 | % 37
  c4 c2 | % 38
  d2.\< | % 39

  \break
  g,2.\> | % 40
  g'4\downbow\! f\upbow es\upbow | %  41
  d2 d4 | % 42
  es4\downbow f\upbow f,\upbow | % 43
  bes2 bes4 | % 44
  g2.\downbow | % 45
  c2\upbow c'4\upbow\f ( | % 46
  c2) c4 | %47
  \break
  b!2. | % 48
  bes2. | % 49
  a2. | % 50
  g4\upbow g,2\downbow | % 51
  a2. | % 52
  d2. | % 53
  g2 g4 | % 54
  fis4 fis4.. fis16\< | % 55
  g4\! es2\tenuto | % 56
  \break
  d4 d4.. d16 | % 57
  b!4 b4.. b16 | % 58
  c4 c2 | % 59
  d2.\< | % 60
  g,2.\> | % 61
  R1*3/4*8\! % 62-69
%  r8 es'8 d c bes a | % 69
%  R1*3/4 | % 69  
  g'8 fis g a bes g | % 70
  \break
  fis8 e! fis g a fis | % 71
  g8 f es d es c | % 72
  d8 es d c d a | % 73
  b!8 c b a b g | % 74
  c8 b! c d es c | % 75
  \break
  d8 es d c bes a | % 76
  g2.  | % 77
  g'2 g4 | % 78
  fis4 fis4. fis8  | % 79
  g4 es2\tenuto | % 80
  d4 d4. d8 | % 81
  b!4 b4. b8 | % 82
  \break
  c4 c2 | % 83
  d2. | % 84
  g,2. | % 85
  R1*3/4*8  | % 86-93
  g'2 g4 | % 94
  fis4 fis4. fis8 | % 95
  g4 es2\tenuto | % 96
  d4\mf d4. d8 | % 97
  b!4_\markup{\italic più \dynamic f} b4. b8 | % 98
  \break
  c4 c2  | % 99
  d2. | % 100
  g,2. | % 101
  g'2 g4 | % 102
  fis4 fis4.. fis16\<  | % 103
  g4\! es2\tenuto | % 104
  d4 d4.. d16 | % 105
  b!4 b4.. b16 | % 106
  \break
  c4 c2 | % 107
  d2. | % 108
  g,2 g'4 | % 109
  a2.  | % 110
  bes4 es,2 | % 111
  f2. | % 112
  g2. | % 113
  as2. | % 114
  g2 g4 | % 115
  f2. | % 116
  \break
  g4 g,2 | % 117
  c8 d c bes c a | % 118
  d8 es d c d bes | % 119
  es8 f es d es c | % 120
  f8 es f g a f | % 121
  \break
  bes8 a bes c bes a | % 122
  g2 g4 | % 123
  fis4 fis4.. fis16\< | % 124
  g4\! es2\tenuto | % 125
  d4 d4.. d16 | % 126
  b!4 b4.. b16 | % 127
  c4 c2 | % 128
  \break
  d2.\< | % 129
  g,2.\> | % 130
  g'2\! g4 | % 131
  fis4 fis4.. fis16\< | % 132
  g4\! es2\tenuto | % 133
  d4 d4.. d16 | % 134
  b!4 b4.. b16 | % 135
  c4 c2 | % 136
  \break
  d2.\< | % 137
  g,2.\> | % 138
  g'2\! g4 | % 139
  fis4 fis4.. fis16\< | % 140
  g4\! es2\tenuto | % 141
  d4 d4.. d16 | % 142
  b!4 b2 | % 143
  c4 c2 | % 144
  d2.\< | % 145
  \break
  g,2.\> | % 146
  g'2\! g4 | % 147
  fis4 fis4.. fis16\< | % 148
  g4\! es2\tenuto | % 149
  d4 d4.. d16 | % 150
  b!4 b4.. b16 | % 151
  c4 c2 | % 152
  d2.\< | % 153
  g,2.\> <>\! | % 154
  
  \bar "|."
}

gminorscale = \relative c {
% flatted 3 6 7
% w h w w h h w
  \key g \minor
  r4 g4 a bes
  c d es
  fis g 
  g fis es
  d c bes
  a g4
  \bar "||"
  g'4 a bes
  c d es
  fis g 
  g fis es
  d c bes
  a g4 r4
  
  \bar "|."
}
\score {
  \new Staff \with {
   % instrumentName = "Contrabass"
   % shortInstrumentName = "Cb."
   % midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}
\score {
  \new Staff \with {
   } { \clef bass \gminorscale }
  \layout { }
  \header {
    piece = "G minor scale"
  }
%  \midi { }
}

\markup{
  \teeny
  \date
}