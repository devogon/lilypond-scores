\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Mr. Blue Sky"
%  subtitle = "Electric Light Orchestra"
  subsubtitle = "from the album “Out of the Blue”"
  instrument = "Bass"
  composer = "Electric Light Orchestra"
  poet = "words and music by Jeff Lynne"
%  meter = "175"
  copyright = "Electric Light Orchestra"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
 % \partial 2
  \tempo 4=175
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*8 | % 1
  f4\staccato^\markup{\bold Intro} f4\staccato f4\staccato f4\staccato
  f4\staccato f4\staccato f4\staccato f4\staccato   f4\staccato f4\staccato f4\staccato f4\staccato
  f4\staccato f4\staccato f4\staccato f4\staccato
  \bar "||"
  % \break
  f4\staccato^\markup{\bold Verse} f4\staccato f4\staccato f4\staccato
  f4\staccato f4\staccato f4\staccato f4\staccato
  f4\staccato f4\staccato f4\staccato f4\staccato
  e\staccato e\staccato a,\staccato a\staccato
  % \break
  d4\staccato d4\staccato d4\staccato d4\staccato
  g\staccato g\staccato g\staccato g\staccato
  e\staccato e\staccato e\staccato e\staccato
  a\staccato a\staccato a\staccato a\staccato
  % \break
  ais\staccato ais\staccato ais\staccato ais\staccato
  c,\staccato c\staccato c\staccato c\staccato
  f\staccato f\staccato f\staccato f\staccato
  c\staccato c\staccato c\staccato c\staccato
  % \break
  f\staccato^\markup{\bold Verse} f\staccato f\staccato f\staccato
  f\staccato f\staccato f\staccato f\staccato
  f\staccato f\staccato f\staccato f\staccato
  e\staccato e\staccato a,\staccato a\staccato
  % \break
  d\staccato d\staccato d\staccato d\staccato
  g\staccato g\staccato g\staccato g\staccato
  e\staccato e\staccato e\staccato e\staccato
  a\staccato a\staccato a\staccato a\staccato
  % \break
  ais\staccato ais\staccato ais\staccato ais\staccato
  c\staccato c\staccato c\staccato c\staccato
  f\staccato f\staccato f\staccato f\staccato
  c\staccato c\staccato c\staccato c\staccato
  % \break
  d4^\markup{\bold Chorus} r2. |
  c8 r c4 r2 |
  ais4 r2. |
  a8 r a4 r2 |
  % \break
  g4 r2. |
  f8 r f4 r2 |
  dis2 g4 ais |
  ais,2 d4 f |
  % \break
  d'4 r2. |
  c8 r c4 r2 |
  ais4 r2. |
  a8 r a4 r2 |
  % \break
  g4 r2. |
  f8 r f4 r2 |
  dis2 g4 ais |
  ais,2 d4 f |
  % \break
  c4^\markup{\bold Bridge} c c c
  c' c c c
  % \break
  f,4\staccato^\markup{\bold "Guitar Solo"} f\staccato f\staccato f\staccato
  f\staccato f\staccato f\staccato f\staccato
  f\staccato f\staccato f\staccato f\staccato
  e\staccato e\staccato a,\staccato a\staccato
  % \break
  \repeat unfold 4 {d\staccato }
  \repeat unfold 4 {g\staccato}
  \repeat unfold 4 {e\staccato}
  \repeat unfold 4 {a\staccato}
  % \break
  \repeat unfold 4 {ais\staccato}
  \repeat unfold 4 {c,\staccato}
  \repeat unfold 4 {f\staccato}
  \repeat  unfold 4 {c\staccato}
  % \break
  f\staccato^\markup{\bold Verse} f\staccato f\staccato f\staccato
  \repeat unfold 8 {f\staccato}
  e\staccato e\staccato a,\staccato a\staccato
  % \break
  \repeat unfold 4 {d\staccato }
  \repeat unfold 4 {g\staccato}
  \repeat unfold 4 {e\staccato}
  \repeat unfold 4 {a\staccato}
  % \break
  \repeat unfold 4 {ais\staccato}
  \repeat unfold 4 {c,\staccato}
  \repeat unfold 4 {f\staccato}
  \repeat  unfold 4 {c\staccato}
  % \break
  d'4^\markup{\bold Chorus} r2. |
  c8 r c4 r2 |
  ais4 r2. |
  a8 r a4 r2 |
  % \break
  g4 r2. |
  f8 r f4 r2 |
  dis2 g4 ais |
  ais,2 d4 f |
  % \break
  d'4 r2. |
  c8 r c4 r2 |
  ais4 r2. |
  a8 r a4 r2 |
  % \break
  g4 r2. |
  f8 r f4 r2 |
  dis2 g4 ais |
  ais,2 d4 f |
  % \break
  r8 d f4 a4. ais,8 | % 95
  e'4 f r2 |
  r4 g4. f8 d4 |
  a4 a' r2 |
  % \break
  r4 ais r2 | % 99
  f4 f' r2 | %
  dis4. ais4. g4 |
  d'4. ais4. f4 |
  % \break
  \repeat unfold 8 {c\staccato}
  \repeat unfold 8 {f\staccato}
  % \break
  \repeat unfold 4 {f\staccato}
  e\staccato e\staccato a\staccato a\staccato
  \repeat unfold 4 {d,\staccato}
  \repeat unfold 4 {g\staccato}
  % \break
  \repeat unfold 4 {e\staccato}
  \repeat unfold 4 {a\staccato}
  \repeat unfold 4 {ais,\staccato}
  \repeat unfold 4 {c\staccato}
  % \break
  \repeat unfold 4 {f\staccato}
  \repeat unfold 4 {c\staccato}
  % \break
  f4\staccato^\markup{\bold Bridge} \repeat unfold 3 {f\staccato}
  \repeat unfold 8 {f\staccato}
  e\staccato e\staccato a\staccato a\staccato
  % \break
  \repeat unfold 4 {d,\staccato}
  \repeat unfold 4 {g\staccato}
  \repeat unfold 4 {e\staccato}
  \repeat unfold 4 {a\staccato}
  % \break
  \repeat unfold 4 {ais,\staccato}
  \repeat unfold 4 {c\staccato}
  \repeat unfold 4 {cis\staccato}
  \repeat unfold 4 {dis\staccato}
  % \break
  d'4 r2. |
  c8 r c4 r2 |
  ais4 r2. |
  a8 r a4 r2 |
  % \break
  g4 r2. |
  f8 r f4 r2 |
  dis2 g4 ais |
  ais,2 d4 f |
  % \break
  r8 d f4 a r8 ais, |
  e'4 f r2 |
  r4 g4. f8 d4 |
  c4 a' r2 |
  % \break % end of pg 5
  r4 ais r2 |
  f4 f' r2 |
  dis4. ais4. g4 |
  d'4. ais4. f4 |
  % \break
  d'4 c a f |
  c' a f d |
  g, a ais b |
  c d e f |
  % \break
  g a ais g |
  c a f d |
  ais' g dis c |
  ais f' d ais |
  % \break
  d'4 c a f |
  c' a f d |
  g, a ais b
  c d e f
  % \break
  g g a ais
  c a f d
  dis2 dis
  dis4 dis dis2
  % \break
  ais2 ais4 ais
  ais1 ~
  ais2 f'2 ~
  f1 ~
  f1
  % \break
  cis8^\markup{\bold Outro} cis8 \repeat unfold 3 {cis8 cis8}
  \repeat unfold 56 {cis8 cis8}
  ais1 ~
  ais1
  gis1
  ais1
  dis1
  d1
  cis1 ~
  cis1
  \grace cis'8\glissando dis1 (d1)
  dis,1   (d1\fermata)
  \bar "|."
}

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
