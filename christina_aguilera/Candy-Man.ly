\version "2.19.50"
\language "nederlands"

\header {
  title = "Candy Man"
  composer = "Christina Aguilera"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
%  system-system-spacing = #'((basic-distance . 0.1) (padding . 0))
 % ragged-last-bottom = ##f
 % ragged-bottom = ##f
  page-count = #1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
  \tempo 4=165
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  \set Score.markFormatter = #format-mark-box-letters
  \set countPercentRepeats = ##t
}

thenotes = {
  es4 g bes c
  es4 c bes g
}

theothernotes = {
  bes4 bes, bes' a!
  as4 c, des d!
}

clapping = {
  r4 d r d r d r d
}

contrabass = \relative c' {
  \global
  % Music follows here.
  g8 g es4 es c8 es (
  es8) es es es es2
  R1*2
 % \break
  g8 g es c es es es es (
  es8) es f d es2
  \improvisationOn
  c4 r r2
  \improvisationOff
  r1
%  \break
  \mark \default
  \repeat volta 2 {
    es4^\markup{"first time only"} g bes c
  es4 c bes g
    es4 g bes c
    es4 c bes g
     es4 g bes c
  es4 c bes g
   es4 g bes c
  es4 c bes g
  as4 c, des d!
  es4 f g as
  %\thenotes
   es4 g bes c | % 21
  es4 c bes g | % 22
  \theothernotes

   es4 g bes c
  es4 c bes g
   es4 g bes c
  es4 c bes g
  }
  \mark \default
  \improvisationOn
  \repeat percent 4 {r4^\markup {"sing and clap hands"} d r d r d r d}
  \improvisationOff
  \bar "||"
  \mark \default
  as'4 c, des d!
  es4 f g as
  \thenotes
  bes4 r r2
  as4 r r2
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  \mark \default
   es4 g bes c | % 45
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  \theothernotes
es4 g bes c
  es4 c bes g

  \mark \default
  %\markup {"sing and clap hands"}
  \improvisationOn
  \repeat percent 10 {r4^\markup {"sing and clap hands"} d r d r d}
  r4 c r c
  \improvisationOff
  R1
  \bar "||"
  \mark \default
  as'4 c, des d!
  es4 f g as
es4 g bes c
  es4 c bes g

  \theothernotes
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  es4 g bes c
  es4 c bes g
  \mark \default
  \improvisationOn
				%  \repeat percent 3 { \clapping }
  \repeat percent 3 {r4^\markup {"clap hands"} d r d r d r d}
%  r4 d r d r d r d
 %   r4 d r d r d r d
  \improvisationOff
  g8^\markup {"sing"} g es4 es c8 es (
  es8) es es es es2
  R1*2
%  \break
  g8 g es c es es es es (
  es8) es f d es2
  R1*2
  g4 es8 c es4 c
  es4 es g2
  R1*2
  g8 es es c es4 c8 es (
  es8 ) es f d es2
  R1
  r4 es\upbow r2
  \bar "|."

}

verseone = \lyricmode {
  Tar -- zan and Jane were swing -- ing on a vine   Sip pin' from a bot -- tle of vod -- ka dou -- ble wine  _  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ __ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
Tar -- zan and Jane were swing -- ing on a vine   Sip pin' from a bot -- tle of vod -- ka dou -- ble wine
Jane lost her grip and down she fell Squared her self a way as she let out a yell
}

\score {
  <<
  \new Staff \with {
    %instrumentName = "Contrabass"
    %shortInstrumentName = "Cb."
    %midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \addlyrics \verseone

  %\layout { }
  %\midi { }
  >>
}
