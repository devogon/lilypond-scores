\version "2.19.57"
\language "nederlands"

\header {
  title = "De Koekoek & Dans"
  instrument = "Cello"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key f \major
  \time 3/4
  \tempo "Allegro" 4=120
}

scoreACello = \relative c' {
  \key f \major
  \time 3/4
  \tempo "Allegro" 4=120
  % Music follows here.
    \repeat volta 2 {
    f2.  | % 1
    c2. | % 2
    c2. | % 3
    f2. | % 4
  }
  \break
  c2. | % 9
  g'2. | % 10
  f2. | % 11
  c2. | % 12
  f2._\markup{(zachter)} | % 13
  c2. | % 14
  c2. | % 15
  f2. | % 16
  \bar "|."
}

scoreBGlobal = {
  \key f \major
  \time 4/4
  \tempo "Allegro" 4=120
}

scoreBCello = \relative c {
\key f \major
  \time 4/4
  \tempo "Allegro" 4=120
  % Music follows here.
      f4 c f f | % 1
    bes,4 c f f | % 2
    d4 c f a | % 3
    g4 g f f | % 4
      f4 c f f | % 5
    bes,4 c f f | % 6
    d4 c f a | % 7
    g4 g f f | % 8
  \repeat volta 2 {
    a4 a f f | % 9
    c4 c f f | % 10
    a4 a f f | % 11
    c4 c f2 | % 12
  }
}

\score {
  \new Staff { \clef bass \scoreACello }
  \header {
    piece = "De Koekoek"
  }
  \layout { }
}
\score {
  \new Staff { \clef bass \scoreBCello }
  \header {
    piece = "Dans"
  }
  \layout { }
}

\markup {
  \teeny
  \date
}