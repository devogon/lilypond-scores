\version "2.19.57"
\language "nederlands"

\header {
  title = "De Koekoek"
  composer = "Anoniem (19e eeuw)"
  meter = "Allegro"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key f \major
  \numericTimeSignature
  \time 3/4
  \tempo "Allegro" 4=120
}

flute = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    c4->\staccato a\staccato r | % 1
    c4->\staccato a\staccato r | % 2
    g4 f g | % 3
    f2. | % 4
  }
  g4 g a | % 9
  bes!2 g4 | % 10
  a4 a bes! | % 11
  c2 a4 | % 12
  c4\staccato-> a\staccato r | % 13
  c4\staccato-> a\staccato r | % 14
  bes!4 a g | % 15
  f2. | %
  \bar "||"
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    a4\staccato-> f\staccato r | % 1
    a4\staccato-> f\staccato r | % 2
    e4 f e | % 3
    f2. | % 4
  }
  \break
  e4 e f | % 9
  g2 e4 | % 10
  f4 f g | % 11
  a2 f4 | % 12
  r4_\markup{(zachter)} c'4\staccato-> a\staccato | % 13
  r4 c\staccato-> a\staccato  | % 14
  g4 f e | % 15
  f2. | %
  \bar "|."
}

cello = \relative c' {
  \global
  % Music follows here.
  \repeat volta 2 {
    f2.  | % 1
    c2. | % 2
    c2. | % 3
    f2. | % 4
  }
  c2. | % 9
  g'2. | % 10
  f2. | % 11
  c2. | % 12
  f2._\markup{(zachter)} | % 13
  c2. | % 14
  c2. | % 15
  f2. | % 16
  \bar "|."
}

contrabass = \relative c' {
  \global
  % Music follows here.
  \repeat volta 2 {
    f2.  | % 1
    c2. | % 2
    c2. | % 3
    f2. | % 4
  }
  c2. | % 9
  g'2. | % 10
  f2. | % 11
  c2. | % 12
  f2. | % 13
  c2. | % 14
  c2. | % 15
  f2. | % 16
  \bar "|."
}

flutePart = \new Staff \with {
  instrumentName = "Flute"
  shortInstrumentName = "Fl."
  midiInstrument = "flute"
} \flute

violinPart = \new Staff \with {
  instrumentName = "Violin"
  shortInstrumentName = "Vl."
  midiInstrument = "violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \cello }

contrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \contrabass }

\score {
  <<
    \flutePart
    \violinPart
    \celloPart
    \contrabassPart
  >>
  \layout { }
  \midi { }
}

\markup {
  \teeny
  \date
}