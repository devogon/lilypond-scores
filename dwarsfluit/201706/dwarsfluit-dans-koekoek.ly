\version "2.19.57"
\language "nederlands"

\header {
  title = "De Koekoek"
  instrument = "dwarsfluit"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

scoreAGlobal = {
  \key f \major
  \time 4/4
  \tempo "Allegro" 4=120
}

dans = \relative c' {
  \key f \major
  \time 4/4
  \tempo "Allegro" 4=120
  % Music follows here.
    \clef treble
  % Music follows here.
  f4^\markup{\teeny + dubbelduimklep}_\markup{f} g_\markup{g} a_\markup{a} a_\markup{a} | % 1
  bes!_\markup{"b♭"} g_\markup{g} a_\markup{a} a_\markup{a} | % 2
  f_\markup{f} g_\markup{g} a_\markup{a} c_\markup{c} | % 3
  bes!2_\markup{"b♭"}-> a_\markup{a}-> | %4
  f4_\markup{f} g_\markup{g} a_\markup{a} a_\markup{a} | %  5
  bes!4_\markup{"b♭"} g_\markup{g} a_\markup{a} a_\markup{a} | % 6
  f_\markup{a} g_\markup{g} a8_\markup{a} (bes!_\markup{"b♭"}) c4_\markup{a} | % 7
  bes!2_\markup{"b♭"}-> a_\markup{a}-> | % 8
  \repeat volta 2 {
    c2_\markup{a}-> c_\markup{a}-> | % 9
    c8_\markup{c}-> bes!_\markup{"b♭"} a_\markup{a} g_\markup{g} f2_\markup{f} | % 10
    c'2_\markup{c}-> c_\markup{c}-> | % 11
    c8_\markup{c}-> bes!_\markup{"b♭"} a_\markup{a} g_\markup{g} f2_\markup{f} | % 12
  }
}

koekoek = \relative c'' {
   \key f \major
  \numericTimeSignature
  \time 3/4
  \tempo "Allegro" 4=120
  % Music follows here.
    \repeat volta 2 {
    c4->\staccato_\markup{c} a\staccato_\markup{a} r | % 1
    c4->\staccato_\markup{c} a\staccato_\markup{a} r | % 2
    g4_\markup{g} f_\markup{f} g_\markup{g} | % 3
    f2._\markup{f} | % 4
  }
  \break
  g4_\markup{g} g_\markup{g} a_\markup{a} | % 9
  bes!2_\markup{"b♭"} g4_\markup{g} | % 10
  a4_\markup{a} a_\markup{a} bes!_\markup{"b♭"} | % 11
  c2_\markup{c} a4_\markup{a} | % 12
  c4\staccato_\markup{c}-> a\staccato_\markup{a} r | % 13
  c4\staccato_\markup{c}-> a\staccato_\markup{a} r | % 14
  bes!4_\markup{"b♭"} a_\markup{a} g_\markup{g} | % 15
  f2._\markup{f} | %
  \bar "||"
}

\score {
  \new Staff \koekoek
  \header {
    piece = "De Koekoek"
  }
  \layout { }
}

\score {
  \new Staff \dans
  \header {
    piece = "Dans"
  }
  \layout { }
}

\markup {
  \teeny
  \date
}