\version "2.19.57"
\language "nederlands"

\header {
  title = "Dans"
 % instrument = "Viool"
  composer = "Praetorius"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key f \major
  \time 4/4
  \tempo "Allegro" 4=120
}

flute = \relative c' {
  \global
  \clef treble
  % Music follows here.
  f4^\markup{\teeny + dubbelduimklep} g a a | % 1
  bes! g a a | % 2
  f g a c | % 3
  bes!2-> a-> | %4
  f4 g a a | %  5
  bes!4 g a a | % 6
  f g a8 (bes!) c4 | % 7
  bes!2-> a-> | % 8
  \repeat volta 2 {
    c2-> c-> | % 9
    c8-> bes! a g f2 | % 10
    c'2-> c-> | % 11
    c8-> bes! a g f2 | % 12
  }
}

violin = \relative c' {
  \global
  % Music follows here.
  f4 e f f | % 1
  f4 e f f | % 2
  f4 e f a | % 3
  g2-> f-> | % 4
  f4 e f f | % 5
  f4 e f f | % 6
  f4 e f a | % 7
  g2-> f-> | % 8
  \repeat volta 2 {
    a4-> a f-> f | % 9
    e4-> e f2 | % 10
    a4-> a f-> f | % 11
    e4-> e f2
  }
}

cello = \relative c {
  \global
  % Music follows here.
    f4 c f f | % 1
    bes,4 c f f | % 2
    d4 c f a | % 3
    g4 g f f | % 4
      f4 c f f | % 5
    bes,4 c f f | % 6
    d4 c f a | % 7
    g4 g f f | % 8
  \repeat volta 2 {
    a4 a f f | % 9
    c4 c f f | % 10
    a4 a f f | % 11
    c4 c f2 | % 12
  }
}

contrabass = \relative c {
  \global
  % Music follows here.
    f4 c f f | % 1
    bes,4 c f f | % 2
    d4 c f a | % 3
    g4 g f f | % 4
      f4 c f f | % 5
    bes,4 c f f | % 6
    d4 c f a | % 7
    g4 g f f | % 8
  \repeat volta 2 {
    a4 a f f | % 9
    c4 c f f | % 10
    a4 a f f | % 11
    c4 c f2 | % 12
  }
}

flutePart = \new Staff \with {
  instrumentName = "Flute"
  shortInstrumentName = "Fl."
  midiInstrument = "flute"
} \flute

violinPart = \new Staff \with {
  instrumentName = "Violin"
  shortInstrumentName = "Vl."
  midiInstrument = "violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \cello }

contrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \contrabass }

\score {
  <<
    \flutePart
    \violinPart
    \celloPart
    \contrabassPart
  >>
  \layout { }
  \midi { }
}

\markup {
  \teeny
  \date
}