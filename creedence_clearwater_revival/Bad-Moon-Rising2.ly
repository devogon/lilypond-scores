\version "2.25.16"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Bad Moon Rising"
  subtitle = "Creedence Clearwater Revival - 1969 - Green River"
  arranger = "Bassist: Stu Cook"
  composer = "John Fogerty"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  \mark \markup{Intro}
  r1
  d4 d d d |
  \bar "||"
  \break
  \mark \markup{1st Verse}
 \repeat volta 4 { d d a g
  d' d d d
  }
  \bar "||" \break
  \mark \markup{Chorus}
    g, g g g
    d' d d d
    a a g g
    d' d d d
\bar "||"

   \break
   \mark \markup{Verse 2}
   \repeat volta 3 {
     d d a g
     d' d d d
                }
                d d a g
                d' d d d
\bar "||"

\break
\mark \markup{Chorus}
g, g g g
d' d d d
a a g g
d' d d8 a b cis
\bar "||"

\break
\mark \markup{Solo}
d4 d a g
d' a d8 a b cis
d4 d a g
d' a d8 d e fis
g4 d g d8 g
d4 g, d' g,8 d'
a4 a g g
d' a d a
\bar "||"

\break
\mark \markup{Verse 3}
\repeat volta 3 {
     d d a g
     d' d d d
                }
                d d a g
                d' d d d
\bar "||"

\break
 \mark \markup{Chorus}
    g, g g g
    d' d d d
    a a g g
    d' d d8 d e fis
\bar "||"

    \break
    \mark \markup{Outro}
    g4 g g g
    d d d d
    a a g g
    d' d d2
    \bar "|."

  }

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
