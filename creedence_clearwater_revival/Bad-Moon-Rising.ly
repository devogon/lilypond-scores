\version "2.21.6"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

% this version from bassbuzz course - easy songs

\header {
  title = "Bad Moon Rising"
  subtitle = "Creedence Clearwater Revival - Green River - 1969"
  composer = "John Fogerty"
  tagline = \date
}

\paper {
  indent = 0.0\cm
  page-count =  1
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=90
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  s1
  d1
  d2 a4 g
  d1
  s2 a4 g
  d1
  s2 a4 g
  d1
  s2 a4 g
  d1
  g1
  d1
  a2 g
  d1
  d2 a4 g
  d1
  s2 a4 g
  d1
  g1
  d1
  a2 g
  d1
  g1
  d1
  a2 g
  d1
  d2 a4 g
  d1
  s2 a4 g
  d1
  g1
  d1
  a2 g
  d1
  d2 a4 g
  d1
  s2 a4 g
  d1
  g1
  d1
  a2 g
  d1
  g1
  d1
  a2 g
  d1
  g1
  d1
  a2 g
  d1
}

electricBass = \relative c, {
  \global
  \mark \markup{Intro}
  r1
  d4\3 d\3 d\3 d\3 |
  \bar "||"
  \mark \markup{Verse 1}
  d4\3 d\3 a\4 g |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  \break
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  g,\4 g\4 g\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  \break
  d'4\3 d\3 d\3 d\3 |
  \mark \markup{Verse 2}
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  \break
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d8\3 a\4 |
  g4\4 g\4 g\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  \break
  d'4\3 d\3 d8\3 a\4 b\3 cis\3 |
  \bar "||"
  \mark \markup {Solo}
  d4\3 d\3 a\4 g\4 |
  d'4\3 a\4 d8\3 a\4 b cis |
  d4\3 d\3 a\4 g\4 |
  d'4\3 a\4 d8\3 d\3 e\2 fis\2 |
  \break
  g4\2 d\3 g\2 d\3 |
  d\3 a\4 d\3 a\4 |
  a\4 a\4 g\4 g\4 |
  d'\3 a\4 d\3 a\4 |
  \mark \markup{Verse 3}
  d\3 d\3 a\4 g\4 |
  d'\3 d\3 d\3 d\3 |
  \break
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d\3 d\3 |
  g,\4 g\4 g\4 g\4 |
  \break
  d'4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'\3 d\3 d8\3 d\3 e\2 fis\2 |
  \break
  g4\2 g4\2 g4\2 g4\2 |
  d4\3 d\3 d\3 d\3 |
  d4\3 d\3 a\4 g\4 |
  d'4\3 d\3 d2\3\fermata
  \bar "||"
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
