\version "2.19.55"
\language "nederlands"

\header {
  title = "16. Zagen, zagen"
  % Remove default LilyPond tagline
  tagline = ##f
  instrument = "dwarsfluit"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  %\tempo 4=100
}

flute = \relative c'' {
  \global
  % Music follows here.
  d4 d e e |
  d8 e d c b4 g |
  d'4 d e e8 e |
  d8 e d c b4 g |
  %\break
  c8 c c c c2 |
  c8 c c c c2 |
  e4 d8 d c4 b8 b |
  a2 g |
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Flute"
 %   shortInstrumentName = "Fl."
  } \flute
  \addlyrics {
    Za -- gen, za -- gen, wie -- de wie -- de wa -- gen,
    Jan kwam thuis, om een bo -- ter -- ham te vra -- gen;
    Va -- der was niet thuis,
    Moe -- der was niet thuis
    Piep! zei de muis in 't voor huis.
  }
  \layout { }
}


% \markup \column {
%   \line {Zagen, zagen, wiedewiedewagen,}
%   \line {Jan kwam thuis, om een boterham te vragen;}
%   \line {Vader was niet thuis,}
%   \line {Moeder was niet thuis -}
%   \line {Piep! zei de muis in 't voorhuis.}
% }
