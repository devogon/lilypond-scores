\version "2.19.54"
\language "nederlands"

\header {
  title = "Suite for four double basses"
%  subtitle = "for four double basses"
%  instrument = "Contrabas"
  composer = "Colin Brumby"
  piece = \markup{\huge \bold I \smaller "(bass II)"}
  copyright = \markup{\teeny "1978 Yorke Edition, 31 Thornhill Square, London N1 1BQ, England"}
  % Remove default LilyPond tagline
  tagline = \markup{\teeny "YE 0046"}
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
%  system-system-spacing = #'((basic-distance . 0.1) (padding . 0))
  ragged-last-bottom = ##f
  ragged-bottom = ##f

}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
%    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #8
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
  \partial 4
  \tempo "Pesante" 4=60
}

scoreAContrabassI = \relative c {
  \global
  % Music follows here.

}

scoreAContrabassII = \relative c {
  \global
  % Music follows here.
  \bar ".|:"
  \repeat volta 2 {
    r4
    r8\mp c'8\downbow\> (b4) (b8)\! r8 | % 1
    r8_\markup{\italic sim.} c8 (b4) (b8) r8 | % 2
    r8 c8 (b4) (b8) r8 | % 3
    r8 c8 (b4) (b8) r8 | % 4
    r8 c8 (b4) (b8) r8 | % 5
    % \break
    r8 e8 (dis4) (dis8) r8 | % 6
    r8 e8\mf\> (c4) (b8)\! r8 | % 7
    r8 e8\< (c4)\> (b8)\! r8 | % 8
    r8 e8\< (c4)\> (b8)\! r8 | % 9
    e2\tenuto\> r4\! | % 10
    r4 e4_\markup{\italic dim.} dis8^\markup{\italic poco a} (e8) | % 11
    % \break
    dis8^\markup{\italic poco} r8 r4 d!4 | % 12
    cis8 (d) cis8 r r4 | % 13
    c4 b8\upbow (c) b4 | % 14
    eis4 e!2\tenuto | % 15
    dis2\tenuto d!4\p ( | % 16
    d2\fermata ) % 17
  }
  r4\fermata
  \bar "||"
  % \break
  \repeat volta 2 {
    \tempo "Allegro"
    r8^\markup{\left-align "pizz."} e,8\mp c'8_\markup{\italic cres.} r8 a, e' | % 18
    r8 b' f' r d, g | % 19
    r8 g\mf e'_\markup{\italic dim.} r c, d | % 20
    r8 b' e r e, e, | % 21
    r8 e'\mp c'_\markup{\italic cresc.} r a, e' | % 22
    % \break
    r8 b' f' r d, g | % 23
    r8 g\mf e'_\markup{\italic dim.} r c ,d | % 24
    r8 b' e r e, e, | % 25
    r8 e'_\markup{\italic sim.} c' r a, e' | % 26
    r8 b' f' r d, g | % 27
    % \break
    r8 g e' r c, d | % 28
    r8 b' e r e, e, | % 29
    r8 e' c' r a, e' | % 30
    r8 b' f' r d, g | % 31
    r8 g e' r c, d | % 32
    % \break
    r8 b' e r e, e, | % 33
    r8 e'\mp c'_\markup{\italic cresc.} r a, e' | % 34
    r8 e\mf c_\markup{\italic dim.} r b e | % 35
    r8 e_\markup{\italic sim.} c' r a, e' | % 36
    r8 e c r b e | % 37
  }
  % \break
  r8 fis\mp c'_\markup{\italic cresc.} r c a | % 38
  r8 a\mf b_\markup{\italic cresc.} r b a | % 39
  \clef tenor
  \tempo "Più mosso"
  d4.\f^\markup{arco} d8 d4 | % 40
  d2\tenuto d4 | % 41
  d4._\markup{\italic sim.} d8 d4 | % 42
  d2\tenuto r4 | % 43
  % \break
  d4\upbow d d | % 44
  d4 d d | % 45
  d2\mf\tenuto d4\tenuto_\markup{\italic cresc.} ( | % 46
  d4) d2\tenuto | % 47
  f4.\f\tenuto f8 f4 | % 48
  f2\tenuto r4 | % 49
  f4._\markup{\italic sim.} f8 f4  | % 50
  f2 f4 | % 51
  % \break
  f4\upbow\mp_\markup{\italic cresc.} f f | % 52
  f4 f f | % 53
  f2\tenuto f4\tenuto ( | % 54
  f4) f2\tenuto | % 55
  d4.\f d8 d4 | % 56
  d2\tenuto r4 | % 57
  d4._\markup{\italic sim.} d8 d4 | % 58
  d2\tenuto d4 | % 59
  % \break
  d4\upbow\mp d_\markup{\italic cresc.} d | % 60
  d4 d d | % 61
  d2\tenuto d4\tenuto ( | % 62
  d4) d2\tenuto | % 63
  f4.\f f8 f4 | % 64
  as2 r4 | % 65
  f4._\markup{\italic sim.} f8 f4 | % 66
  % \break
  as2 r4 | % 67
  f4\mp\tenuto as_\markup{\italic cresc.} f | % 68
  as4 f as | % 69
  f2\tenuto as4\tenuto ( | % 70
  as4) gis2\f\tenuto\> ( | % 71
  gis2\fermata\!) r4\fermata | % 72
  \clef bass
  \repeat volta 2 {
    r8^\markup{a tempo} e,8^\markup{pizz.}\mp c'_\markup{\italic cresc.} r a, e' | % 73
    % \break
    r8 b' f' r d, g | % 74
    r8 g\mf e'_\markup{\italic dim.} r c, d | % 75
    r8 b' e r e, e, | % 76
    r8 e' c' r a, e' | % 77
    r8 b' f' r d, g | % 78
    % \break
    r8 g e r c d | % 79
    r8 b e r e e, | % 80
    r8 e'_\markup{\italic sim.} c' r a, e' | % 81
    r8 b' f' r d, g | % 82
    r8 g e' r c, d | % 83
    % \break
    r8 b' e r e, e, | % 84
    r8 e' c' r a, e' | % 85
    r8 b' f' r d, g | % 86
    r8 g e' r c, d | % 87
    r8 b' e r e, e, | % 88
    % \break
    r8 e'_\markup{\italic sim.} c' r a, e' | % 89
    r8 e\mf c'_\markup{\italic dim.} r b, e | % 90
    r8 e c' r a, e' | % 91
    r8 e c r b e | % 92
  }
  r2.^\markup{arco} | % 93
  cis'2.\fermata\mp_\markup{\italic dim.} % 94
  \bar "|."
}

scoreAContrabassIII = \relative c {
  \global
  % Music follows here.

}

scoreAContrabassIV = \relative c {
  \global
  % Music follows here.

}

scoreAContrabassIPart = \new Staff \with {
  instrumentName = "Contrabass I"
  shortInstrumentName = "Cb. I"
} { \clef bass \scoreAContrabassI }

scoreAContrabassIIPart = \new Staff \with {
  instrumentName = "II "
%  shortInstrumentName = "Cb. II"
} { \clef bass \scoreAContrabassII }

scoreAContrabassIIIPart = \new Staff \with {
  instrumentName = "Contrabass III"
  shortInstrumentName = "Cb. III"
} { \clef bass \scoreAContrabassIII }

scoreAContrabassIVPart = \new Staff \with {
  instrumentName = "Contrabass IV"
  shortInstrumentName = "Cb. IV"
} { \clef bass \scoreAContrabassIV }

\score {
  <<
 %   \scoreAContrabassIPart
    \scoreAContrabassIIPart
 %   \scoreAContrabassIIIPart
 %   \scoreAContrabassIVPart
  >>
  \layout { }
}

\markup {
  \teeny
  \date
}