\version "2.19.57"
\language "nederlands"

\header {
  title = "Suite for four double basses"
  subtitle = "(full score)"
  instrument = "Contrabas"
  composer = "Colin Brumby"
  meter = "Pesante"
  copyright = "1978 Yorke Edition, 31 Thornhill Square, London N1 1BQ, England"
  tagline = "YE 004"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
%  \partial 4
  \tempo 4=60
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

\include "ye0046/cbI-partI.ly"
\include "ye0046/cbII-partI.ly"

scoreAContrabassII = \relative c {
  \global
  % Music follows here.

}

scoreAContrabassIII = \relative c {
  \global
  % Music follows here.

}

scoreAContrabassIV = \relative c {
  \global
  % Music follows here.

}

scoreAContrabassIPart = \new Staff \with {
  instrumentName = "I"
  % shortInstrumentName = "Cb. I"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassI }

scoreAContrabassIIPart = \new Staff \with {
  instrumentName = "II"
  % shortInstrumentName = "Cb. II"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassII }

scoreAContrabassIIIPart = \new Staff \with {
  instrumentName = "III"
  % shortInstrumentName = "Cb. III"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassIII }

scoreAContrabassIVPart = \new Staff \with {
  instrumentName = "IV"
  % shortInstrumentName = "Cb. IV"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassIV }

\score {
  \new StaffGroup
  <<
    \scoreAContrabassIPart
    \scoreAContrabassIIPart
%    \scoreAContrabassIIIPart
%    \scoreAContrabassIVPart
  >>
  \layout { }
  \header {
        piece = "I"
    }
  \midi { }
}
