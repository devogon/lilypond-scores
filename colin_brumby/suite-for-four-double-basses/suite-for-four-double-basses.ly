\version "2.19.57"
\language "nederlands"

\header {
  title = "Suite for four double basses"
  subtitle = "(full score)"
  instrument = "Contrabas"
  composer = "Colin Brumby"
  copyright = "1978 Yorke Edition, 31 Thornhill Square, London N1 1BQ, England"
  tagline = "YE 0046"
}

\paper {
  #(set-paper-size "a4")
  % annotate-spacing = ##t
%      print-all-headers = ##t
%system-system-spacing = #'((basic-distance . 0.4) (padding . 2))
  ragged-last-bottom = ##f
  ragged-bottom = ##f

}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

scoreAGlobal = {
  \key c \major
  \time 3/4
  \partial 4
  \tempo "Pesante" 4=60
}

\include "ye0046/cbI-partI.ly"
\include "ye0046/cbII-partI.ly"

\include "ye0046/cbIII-partI.ly"

\include "ye0046/cbIV-partI.ly"

scoreAContrabassIPart = \new Staff \with {
  instrumentName = "I"
%  shortInstrumentName = "I"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassI }

scoreAContrabassIIPart = \new Staff \with {
  instrumentName = "II"
%  shortInstrumentName = "Cb. II"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassII }

scoreAContrabassIIIPart = \new Staff \with {
  instrumentName = "III"
%  shortInstrumentName = "Cb. III"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassIII }

scoreAContrabassIVPart = \new Staff \with {
  instrumentName = "IV"
%  shortInstrumentName = "Cb. IV"
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabassIV }

\score {
  \new StaffGroup
  <<
    \scoreAContrabassIPart
    \scoreAContrabassIIPart
    \scoreAContrabassIIIPart
    \scoreAContrabassIVPart
  >>
  \header {
    piece = \markup{\fontsize #4 \bold "I"}
  }
  \layout { }
  \midi { }
}

scoreBGlobal = {
  \key c \major
  \time 4/4
  \tempo "Larghetto" 4=60
}

\include "ye0046/cbI-partII.ly"
\include "ye0046/cbII-partII.ly"


scoreBContrabassIII = \relative c {
  \scoreBGlobal
  % Music follows here.
  r1 | % 1
  r8 b'\mp\> (b4) (b) (b8) r\! | % 2
  r8 b\mf\> (b4) (b8) r\! b\downbow\f r | % 3
  c2.\upbow\p_\markup{\italic sub.}\< (c8) r\fermata\f\! | % 4
  r1 | % 5
  r8 b\mp\> (b4) (b) (b8) r\! | % 6
  \break
  r8 b\mf\> (b4) (b8)\! r b\downbow\f r | % 7
  c2\upbow\mp_\markup{\italic sub.}\< (c8) r\f\! r4 | % 8
  r4 a,8\mf^\markup{pizz.}_\markup{\italic dim.} r g' r r4 | % 9
  r2 fis8 r fis, r | % 10
  r4 fis8 r e' r r4 | % 11
  \break
  fis2.\p^\markup{arco} (fis8) r\fermata | % 12
  r1 | % 13
  r8 b\mp\> (b4) (b4) (b8\!) r | % 14
  r8 b8\mf\> (b4) (b8) r\! b\downbow\f r | % 15
  c2.\upbow\p\< (c8\f\!) r | % 16
  b2.\pp\fermata (b8) r\fermata | % 17
  \break
  \time 2/4
  \repeat volta 2 {
    a8->\f r r a-> | % 18
    r4 a8-> a\p_\markup{\italic sub.} | % 19
    b8_\markup{\italic cresc.} b cis cis | % 20
    d8->\f r r d-> | % 21
    r4 d8-> cis\p_\markup{\italic cresc.} | % 22
    cis8 b b a | % 23
  }
}

scoreBContrabassIV = \relative c {
  \scoreBGlobal
  % Music follows here.
  e,2.\p\> (e8) r\! | % 1
  e2\mp\> (e4) (e8) r\! | % 2
  e2\mf\> (e8)\! r e8\downbow\f r | % 3
  a2.\p\upbow_\markup{\italic sub.}\< (a8) r\fermata\f\! | % 4
  e2.\p\> (e8) r\! | % 5
  e2.\mp\> (e8) r\! | % 6
  e2\mf\> (e8)\! r e\f\downbow r | % 7
  a2\mp\upbow_\markup{\italic sub.}\< (a8) r\!\f r4 | % 8
  r2 g'8\mf^\markup{pizz.}_\markup{\italic dim.} r g, r | % 9
  r4 g8 r fis' r r4 | % 10
  r2 e8 r e, r | % 11
  b'2.\p^\markup{arco} (b8) r\fermata | % 12
  e,2.\p\> (e8) r\! | % 13
  e2.\mp\> (e8) r\! | % 14
  e2\mf\> (e8) r\! e\downbow\f r | % 15
  a2.\downbow\p\< (a8)\f r\! | % 16
  e2.\pp\fermata (e8) r\fermata | % 17
  \time 2/4
  \repeat volta 2 {
    a8\f-> r r a-> | % 18
    r4 a8-> d\p_\markup{\italic sub.} | % 19
    b8_\markup{\italic cresc.} e cis fis | % 20
    d8\f-> r r d-> | % 21
    r4 d8-> fis\p_\markup{\italic cresc.} | % 22
    cis8 e b d | % 23
  }
}

scoreBContrabassIPart = \new Staff \with {
  instrumentName = "I"
%  shortInstrumentName = "Cb. I"
  midiInstrument = "contrabass"
} { \clef bass \scoreBContrabassI }

scoreBContrabassIIPart = \new Staff \with {
  instrumentName = "II"
 % shortInstrumentName = "Cb. II"
  midiInstrument = "contrabass"
} { \clef bass \scoreBContrabassII }

scoreBContrabassIIIPart = \new Staff \with {
  instrumentName = "III"
%  shortInstrumentName = "Cb. III"
  midiInstrument = "contrabass"
} { \clef bass \scoreBContrabassIII }

scoreBContrabassIVPart = \new Staff \with {
  instrumentName = "IV"
%  shortInstrumentName = "Cb. IV"
  midiInstrument = "contrabass"
} { \clef bass \scoreBContrabassIV }

\score {
  \new StaffGroup
  <<
    \scoreBContrabassIPart
    \scoreBContrabassIIPart
    \scoreBContrabassIIIPart
    \scoreBContrabassIVPart
  >>
  \header {
    piece = \markup {\fontsize #4 \bold "II"}
  }
  \layout { }
  \midi { }
}
