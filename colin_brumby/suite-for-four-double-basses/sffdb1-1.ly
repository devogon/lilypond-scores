\version "2.19.57"
\language "nederlands"

\header {
  title = "Suite"
  subtitle = "for four double basses"
  subsubtitle = "I"
  instrument = "Contrabas I"
  composer = "Colin Brumby"
  piece = "I"
  copyright = "1978 Yorke Edition, 31 Thornhill Square, London N1 1BQ, England"
  tagline = "YE 0046"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

scoreAGlobal = {
  \key c \major
  \numericTimeSignature
  \time 3/4
  \tempo "Pesante" 4=60
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "ye0046/cbI-partI.ly"

\score {
  \new Staff \with {
    instrumentName = "cb I"
%    shortInstrumentName = ""
  } { \clef bass \scoreAContrabassI }
  \layout { }
}
\markup{
  \teeny
  \date
}