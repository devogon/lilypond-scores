\version "2.19.57"
\language "nederlands"

\header {
  title = "Suite"
  subtitle = "for four double basses"
  subsubtitle = "II"
  instrument = "Contrabas II"
  composer = "Colin Brumby"
  piece = "II"
  copyright = "1978 Yorke Edition, 31 Thornhill Square, London N1 1BQ, England"
  tagline = "YE 0046"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

scoreBGlobal = {
  \key c \major
  \numericTimeSignature
  \time 4/4
  \tempo "Larghetto" 4=60
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "ye0046/cbII-partII.ly"

\score {
  \new Staff \with {
    instrumentName = "cb II"
%    shortInstrumentName = ""
  } { \clef bass \scoreBContrabassII }
  \layout { }
}
\markup{
  \teeny
  \date
}