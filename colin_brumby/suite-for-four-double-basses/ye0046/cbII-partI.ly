scoreAContrabassII = \relative c {
   \scoreAGlobal
%  \global
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  \repeat volta 2 {
    \partial 4 r4 | % 0
    r8 c'8\mp\downbow (b4\>) (b8\!) r | % 1
    r8 c_\markup{\italic sim.} (b4) (b8) r | % 2
    r8 c (b4) (b8) r | % 3
    r8 c (b4) (b8) r | % 4
    r8 c_\markup{\italic cresc.} (b4) (b8) r | % 5
    r8 e (dis4) (dis8) r | % 6
    r8 e\mf\> c4 (b8\!) r | % 7
    r8 e\< c4\> (b8\!) r | % 8
    r8 e_\markup{\italic sim.} c4 (b8) r | % 9
    e2\tenuto r4 | % 10
    r4 e4_\markup{\italic dim. poco a poco} dis8 (e) | % 11
    dis8 r8 r4 d!4 | % 12
    cis8 (d) cis8 r r4 | % 13
    c4 b8 (c) b4 | % 14
    eis4 e!2\tenuto | % 15
    dis2\tenuto d!4\p ( | % 16
    d2\fermata)  % 17 or is this 16?
  }
   \partial 4 r4\fermata % 17
   \repeat volta 2 {
     r8^\markup{pizz.} e,\mp c'_\markup{\italic cresc.} r a, e' | % 18
     r8 b' f' r d, g | % 19
     r8 g\mf e' r c,  d | % 20
     r8 b' e r e, e, | % 21
     r8 e'\mp c'_\markup{\italic cresc.} r a, e' | % 22
     \break
     r8 b' f' r d, g | % 23
     r8 g\mf e'_\markup{\italic dim.} r c, d | % 24
     r8 b' e r e, e, | % 25
     r8 e'_\markup{\italic sim} c' r a, e' | % 26
     r8 b' f' r d, g | % 27
     r8 g e' r c, d | % 28
     \break
     r8 b' e r e, e, | % 29
     r8 e' c' r a, e' | % 30
     r8 b' f' r d, g | % 31
     r8 g e' r c, d | % 32
     r8 b' e r e, e, | % 33
     r8 e'\mp c'_\markup{\italic cresc.} r a, e' | % 34
     \break
     r8 e\mf c_\markup{\italic dim.} r b e | % 35
     r8 e_\markup{\italic sim.} c' r a, e' | % 36
     r8 e c r b e | % 37
   }
   r8 fis\mp c'_\markup{\italic cresc.} r c a | % 38
   r8 a\mf b_\markup{\italic cresc.} r b a | % 39
   \clef tenor
   d4.\f^\markup{arco} d8 d4 | % 40
   \break
   d2\tenuto r4 | % 41
   d4._\markup{\italic sim.} d8 d4 | % 42
   d2\tenuto r4 | % 43
   d4\upbow\mp d_\markup{\italic cresc.} d | % 44
   d4 d d | % 45
   d2\tenuto\mf_\markup{\italic cresc.} d4\tenuto ( | % 46
   d4) d2\tenuto | % 47
   f4.\f f8 f4 | % 48
   \break
   f2\tenuto r4 | % 49
   f4._\markup{\italic sim.} f8 f4 | % 50
   f2 r4 | % 51
   f4\upbow\mp_\markup{\italic cresc.} f f | % 52
   f4 f f | % 53
   f2\tenuto f4\tenuto ( | % 54
   f4) f2\tenuto | % 55
   d4.\f d8 d4 | % 56
   \break
   d2\tenuto r4 | % 57
   d4._\markup{\italic sim.} d8 d4 | % 58
   d2\tenuto r4 | % 59
   d4\mp_\markup{\italic cresc.} d d | % 60
   d4 d d | % 61
   d2\tenuto d4\tenuto ( | % 62
   d4) d2\tenuto | % 63
   f4.\f f8 f4 | % 64
   \break
   aes2\tenuto r4 | % 65
   f4._\markup{\italic sim.} f8 f4 | % 66
   aes2\tenuto r4 | % 67
   f4\mp_\markup{\italic cresc.} aes4 f | % 68
   aes4 f aes | % 69
   f2\tenuto aes4\tenuto ( | % 70
   a4) gis2\tenuto\f\> (  | % 71
   g2\fermata) r4\!\fermata
   \clef bass
   \break
   \repeat volta 2 {
     r8^\markup{pizz.} e,\mp_\markup{\italic cresc.} c' r a, e' | % 73
     r8 b' f' r d, g | % 74
     r8 g\mf_\markup{\italic dim.} e' r c, d | % 75
     r8 b' e r e, e, | % 76
     r8 e'_\markup{\italic sim.} c' r a, e' | % 77
     \break
     r8 b' f' r d, g | % 78
     r8 g e' r c, d | % 79
     r8 b' e r e, e, | % 80
     r8 e'_\markup{\italic sim.} c' r a, e' | % 81
     r8 b' f' r d, g | % 82
     \break
     r8 g e' r c, d | % 83
     r8 b' e r e, e,  | % 84
     r8 e' c' r a, e' | % 85
     r8 b' f' r d, g | % 86
     r8 g e' r c, d | % 87
     r8 b' e r e, e, | % 88
     \break
     r8 e'_\markup{\italic sim.} c' r a, e | % 89
     r8 e'\mf_\markup{\italic dim.} c r b e | % 90
     r8 e c' r a, e' | % 91
     r8 e c r b e | % 92
   }
   r1*3/4^\markup{arco} % 93
   cis'2.\mp\fermata_\markup{\italic dim.} % 94
   \bar "|."
}