scoreBContrabassI = \relative c' {
  \override Hairpin #'minimum-length = #7
  \scoreBGlobal
  % Music follows here.
  R1*2 | % 1,2
  r2 r4 g'8\downbow\f r | % 3
  f2.\p\upbow_\markup{\italic sub.}\< (f8\!) r\f\fermata | % 4
  r1^\markup{a tempo} | % 5
  r1 | % 6
  r2 r4 g8\downbow\f r | % 7
  fis2\mp\upbow_\markup{\italic sub.}\< (fis8\f\!) r g4\mf\downbow_\markup{\italic dim.} | % 8
  fis8 (g) fis4 (fis8) r fis4 | % 9
  e8 (fis) e4 (e8) r e4 | % 10
  d8 (e) d4 (d8) r e4 | % 11
  dis2.\p (dis8) r\fermata | % 12
  r1^\markup{a tempo} | % 13
  r1 | % 14
  r2 r4 g8\downbow\f r | % 15
  fis2.\upbow\p\< (fis8\!\f) r | % 16
  gis2.\pp\fermata (gis8) r\fermata | % 17
  \clef tenor
  \time 2/4
  \tempo "Allegro con brio" 4=116
  \break
  \repeat volta 2 {
    e8\f-> cis16 d e8-> c16 d | % 18
    e8\f-> cis16 d e8-> fis\p_\markup{\italic sub.} | %  19
    fis8_\markup{\italic cresc.} gis gis a | % 20
    a8\f-> r r fis-> | % 21
    r4 a8-> a\p_\markup{\italic cresc.} | % 22
    gis8 gis fis fis | % 23
  }
  \break
  \repeat volta 2 {
    e8-> \clef bass ais,\mp_\markup{\italic cresc.} b cis | % 24
    d8 e fis\mf e_\markup{\italic dim.} | % 25
    d8 cis b ais | % 26
    b8 fis\mp_\markup{\italic sim.} gis a | % 27
    b8 cis d\mf cis | % 28
    b8 a gis b | % 29
  }
  \break
  \repeat volta 2 {
    \clef tenor
    e8\f-> cis16 d e8 dis16 e | % 30
    e8-> cis16 d e8-> fis\p_\markup{\italic cresc.} | %  31
    fis8 gis gis a | % 32
    a8\f-> r r fis-> | % 33
    r4 a8-> a\p_\markup{\italic cresc.} | % 34
    gis8 gis fis fis | % 35
  }
  \break
  \repeat volta 2 {
    e8-> fis\mf_\markup{\italic dim.} e d | % 36
    cis8 b ais\mp_\markup{\italic cresc.} b | % 37
    cis8 d e fis | % 38
    gis8 a!_\markup{\italic sim.} gis fis | % 39
    eis8 d cis d | % 40
    eis8 fis gis gis | % 41
  }
  \break
  \repeat volta 2 {
    e8\f-> cis16 d e8-> dis16 e | % 42
    e8-> cis16 d e8-> fis\p_\markup{\italic cresc.} | % 43
    fis8 gis gis a | % 44
    a8\f-> r r fis-> | % 45
    r4 a8-> a\p_\markup{\italic cresc.} | % 46
    gis8 gis fis fis | % 47
  }
  \break
  eis8-> r r eis-> | % 48
  r1*2/4 | % 49
  r8 gis8\mp\upbow\staccato\< g!8\downbow\staccato\> gis\staccato\! | % 50
  r8 gis8\mp\upbow\staccato\< g!8\downbow\staccato\> gis\staccato\! | % 51
  r8 gis8_\markup{\italic cresc.}\upbow g!8\downbow gis | % 52
  g8 gis\tenuto g! gis | % 53
  g!8-> r r g-> | % 54
  \break
  \set Score.currentBarNumber = #55
  r1*2/4 | % 55
  r8 gis\mp\< a\> gis\! | % 56
  r8 gis\mp\< a\> gis\! | % 57
  r8 gis_\markup{\italic cres.} a\tenuto gis | % 58
  a8 gis\tenuto a gis | % 59
  \repeat volta 2 {
    a8-> f\mf_\markup{\italic dim.} e d | % 60
    cis8 bes a\mp_\markup{\italic cresc.} b | % 61
    \break
    cis8 d e f | % 62
    g a\mf_\markup{\italic dim.} g f | % 63
    e8 d cis\mp_\markup{\italic cresc.} d | % 64
    e8 f g g | % 65
  }
  \repeat volta 2 {
    a8\f-> fis16 gis a8-> fis16 gis | % 66
    a8-> fis16 gis a8-> a\p_\markup{\italic cresc.} | % 67
    gis8 gis fis fis | % 68
    \break
    e8-> e16 d cis8-> e16 d | % 69
    cis8-> e16 d cis8-> fis8\p_\markup{\italic cresc.} | % 70
    fis8 gis gis a | % 71
  }
  \repeat volta 2 {
    bes8-> bes\p_\markup{\italic cresc.} a bes | % 72
    a8 bes a bes | % 73
    b!8-> b\p_\markup{\italic sim.} b\tenuto b | % 74
    b8 b\tenuto b b | % 75
  }
  b8-> r r4 | % 76
  r8 b\p_\markup{\italic cresc.} bes b! | % 77
  bes8 b! bes b | % 78
  b-> r r4 | % 79
  r8 gis8\p_\markup{\italic cresc.} g!\tenuto gis | % 80
  g8 gis\tenuto g! gis | % 81
  \repeat volta 2 {
    g8-> r r4 | % 82
    r8 cis,\mp_\markup{\italic cresc.} d e | % 83
    \break
    f8 g a\mf\> g | % 84
    f8\! r r4 | % 85
    r8 a,_\markup{\italic sim.} d cis | % 86
    d8 cis d g | % 87
  }
  e8\f-> cis16 d e8-> cis16 d | % 88
  e8-> cis16 d e8-> fis-> | % 89
  d16 e fis8-> d16 e fis8-> | % 90
  \break
  d16 e fis8-> eis\ff-> e!\tenuto ( | % 91
  e8) eis-> e!4\tenuto | % 92
  eis8\p\tenuto_\markup{\italic sub. cresc.} e! eis e!\tenuto | % 93
  eis8 e! eis\tenuto e! | % 94
  eis8 e!\tenuto eis e! | % 95
  a2\ff\tenuto  | % 96
  g8\tenuto r e4\tenuto ( | % 97
  e2\fermata) | % 98
  \bar "|."
}