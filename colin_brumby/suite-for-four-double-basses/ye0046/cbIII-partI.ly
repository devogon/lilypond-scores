scoreAContrabassIII = \relative c {
  \scoreAGlobal
  \once\override Hairpin #'minimum-length = #7
  \repeat volta 2 {
    \partial 4 e4\pp\upbow\<  % 0
    e4\mp\> (e8\!) r e4_\markup{\italic sim.} | % 1
    e4 (e8) r e4 | % 2
    e4 (e8) r e4 | % 3
    e4 (e8) r e4 | % 4
    e4 (e8) r b4 | % 5
    \break
    b'4 (b8) r b4 | % 6
    b4\mf\> (b8\!) r b4\< | % 7
    b4\> (b8\!) r b4_\markup{\italic sim.} | % 8
    b4 (a8) r b4 | % 9
    b2\tenuto r4 | % 10
    r4 fis2_\markup{\italic dim. poco a poco} ( | % 11
    \break
    d8) r r4 g | % 12
    e8 (g) e r r4 | % 13
    a4 b8 (a) b4 | % 14
    d4 cis2\tenuto | % 15
    fis,2\tenuto b4\p ( | % 16
    b2\fermata) % 17
  }
  \break
  e,4\tenuto\fermata\>
  \repeat volta 2 {
    g,8\!_\markup{\italic cresc.}^\markup{pizz.} r c' e, r e | % 18
    d8 r f' b, r g | % 19
    c,8\mf_\markup{\italic dim.} r e' g, r d | % 20
    e8 r e' b r e,, | % 21
    a8\mp_\markup{\italic cresc.} e'' r e, e r | % 22
    \break
    d8 r f' b, r g | % 23
    c,8\mf_\markup{\italic dim.} r e' g, r d | % 24
    e8 r e' b r e,, | % 25
    a8 r c' e, r e | % 26
    d8 r f' b, r g | % 27
    c,8 r e' g, r d | % 28
    \break
    e8 r e' b r e, | % 29
    a,8 r c' e, r e | % 30
    d8 r f' b, r g | % 31
    c,8 r e' g, r d | % 32
    e8 r e' b r e,, | % 33
    a8\mp_\markup{\italic cresc.} r c' e, r e | % 34
    \break
    d8\mf_\markup{\italic dim.} r c e r e | % 35
    g,8_\markup{\italic sim.} r c' e, r e | % 36
    d8 r c e r e | % 37
  }
  % 38 here
  g,8\mp_\markup{\italic cresc} r c' e r a, | % 38
  f8\mf_\markup{\italic cresc.} r b dis r a | % 39
  b4.\f^\markup{arco} b8 b4 | % 40
  \break
  bes2\tenuto r4 | % 41
  b4._\markup{\italic sim} b8 b4 | % 42
  bes2\tenuto r4 | % 43
  b4\upbow\mp_\markup{\italic cresc.} bes b! | % 44
  bes4 b! bes | % 45
  b4\mf_\markup{\italic cresc.} (bes) b!\tenuto ( | % 46
  bes4) b!\tenuto (bes) | % 47
  des4.\f des8 des4 | % 48
  \break
  d2\tenuto r4 | % 49
  des4._\markup{\italic sim.} des8 des4 | % 50
  d2 r4 | % 51
  des4\mp\upbow_\markup{\italic  cresc.} d! des | % 52
  d4 des d! | % 53
  des4\tenuto (d!) des\tenuto ( | % 54
  d4) des\tenuto (d!) | % 55
  f,4.\f f8 f4 | % 56
  \break
  b2\tenuto r4 | % 57
  f4._\markup{\italic sim.} f8 f4 | % 58
  b2\tenuto r4 | % 59
  f4\mp_\markup{\italic cresc.} b f | % 60
  b4 f b | % 61
  f4\tenuto (b) f\tenuto ( | % 62
  b) f\tenuto (b) | % 63
  d4.\f d8 d4 | % 64
  \break
  f2\tenuto r4  | % 65
  d4._\markup{\italic sim.} d8 d4 | % 66
  f2\tenuto r4 | % 67
  d4\mp_\markup{\italic cresc.} f d | % 68
  f4 d f | % 69
  d2\tenuto f4\tenuto ( | % 70
  f4) d2\tenuto\f\> (  | % 71
  d2\tenuto) e,4\!\tenuto\fermata\mp\> | % 72
  \break
  a,8\!_\markup{\italic cresc.} r c'^\markup{pizz.} e, r e | % 73
  d8 r f' b, r g | % 74
  c,8\mf_\markup{\italic dim.} r e' g, r d | % 75
  e8 r e' b r e,, | % 76
  a8_\markup{\italic sim.} r c' e, r e | % 77
  \break
  d8 r f' b, r g | % 78
  c,8 r e' g, r d | % 79
  e8 r e' b r e,, | % 80
  g8 r c' e, r e | % 81
  d8 r f' b, r g | % 82
  \break
  c,8 r e' g, r d | % 83
  e8 r e' b r e,, | % 84
  a8 r c' e, r e | % 85
  d8 r f' b, r g | % 86
  c,8 r e' g, r d | % 87
  e8 r e' b r e,, | % 88
  \break
  a8_\markup{\italic sim.} r c' e, r e | % 89
  d8\mf_\markup{\italic dim.} r c e r e | % 90
  a,8 r c' e, r e | % 91
  d8 r c e r e
  a,8\mp r r2^\markup{arco}
  a'2.\fermata_\markup{\italic dim.}
  \bar "|."
}