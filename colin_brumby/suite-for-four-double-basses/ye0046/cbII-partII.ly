scoreBContrabassII = \relative c {
  \scoreBGlobal
  % Music follows here.
    R1*2 | % 1
  r4 c'\mf\> (c8) r c\!\f\downbow r | %3
  e2.\upbow_\markup{\italic p sub.}\< (e8\f) r\fermata\! | % 4
  \tempo "a tempo"
  R1*2
  %\break
  r4 c\mf\> (c8) r c\!\f\downbow r | % 7
  e2.\upbow\mp_\markup{\italic sub.}\< (e8\f) r\! | % 8
  r4 c\upbow\mf b8_\markup{\italic dim.} (c) b4 ( | % 9
  b8) r b4 a8 (b) a4 ( | % 10
  a8) r a4\upbow g8 (a) g4 | % 11
  \break
    \breathe b2.\p (b8) r\fermata | % 12
  R1*2 % 13-14
  r4 c\mf\> (c8) r c\!\f\downbow r | % 15
  e2.\upbow\p\< (e8\f) r\! | % 16
  e2.\upbow\pp (e8) r\fermata | % 17
  \break
  \repeat volta 2 {
    \time 2/4
    \tempo "Allegro con brio" 4=116
    cis8\f-> r r e\> | % 18
    r4 cis8\> d\p_\markup{\italic sub.} | % 19
    d8_\markup{\italic cresc.} e e fis | % 20
    \clef tenor
    fis\f-> a16 gis f8-> a16 gis | % 21
    fis8-> a16 gis f8-> f\p_\markup{\italic cresc.} | % 22
    e8 e d d | % 23
  }
  \break
  \repeat volta 2 {
    \clef bass
    cis8-> r r4 | % 24
    r8 ais\mp b8_\markup{\italic cresc.} cis | % 25
    d8 e fis\mf e_\markup{\italic dim.} | % 26
    d8 r r4 | % 27
    r8 fis,\mp gis_\markup{\italic sim.} a | % 28
    b8 cis d d | % 29
  }
  \break
  \repeat volta 2 {
    cis8->\f r r e-> | % 30
    r4 cis8-> d\p | % 31
    d8_\markup{\italic cresc.} e e fis | % 32
    \clef tenor
    fis->\f a16 gis fis8 a16 gis | % 33
    fis8-> a16 gis fis8-> fis\p_\markup{\italic cresc.} | % 34
    e8 e d d | % 35
  }
  \break
  \repeat volta 2 {
    \clef bass
    cis8-> r r4 | % 36
    r8 fis8\mf e_\markup{\italic dim.} d | % 37
    cis8 b ais\mp b_\markup{\italic cresc.} | % 38
    b8 r r4 | % 39
    \clef tenor
    r8 a'_\markup{\italic sim.} gis fis | % 40
    eis8 d cis e | % 41
  }
  \break
  \repeat volta 2 {
    \clef bass
    cis8\f-> r r e-> | % 42
    r4 cis8-> d\p_\markup{\italic cresc.} | % 43
    d8 e e fis | % 44
    \clef tenor
    fis8->\f a16 gis fis8 a16 gis  | % 45
    fis8-> a16 g fis8-> fis\p_\markup{\italic cresc.} | % 46
    e8 e d d | % 47
  }
  \break
  cis8-> r r cis-> | % 48
  \set Score.currentBarNumber = #48
  r1*2/4 | % 49
  r8\mp e8\< f8\> e\! | % 50
  r8 e8\staccato\< f\staccato\> e\staccato\! | % 51
  r8 e_\markup{\italic cresc.} f\tenuto e | % 52
  f8 e\tenuto f e | % 53
  es8-> r r es-> | % 54
  \break
  \set Score.currentBarNumber = #55
  r1*2/4 | % 55
  r8 f8\mp\< g\> f\! | % 56
  r8 f\< g\> f\! | % 57
  r8 f_\markup{\italic cresc.} g\tenuto f | % 58
  g8 f\tenuto g f | % 59
  \repeat volta 2 {
    f8-> r r4 | % 60
    r8 f\mf e_\markup{\italic dim.} d | % 61
    cis8 bes a\mp bes_\markup{\italic cresc.} | % 62
    cis8 r r4 | % 63
    r8 a'\mf g_\markup{\italic dim.} f | % 64
    e8 d cis e | % 65
  }
  \repeat volta 2 {
     fis8\f-> r r a-> | % 66
     r4 fis8-> fis\p | % 67
     e8_\markup{\italic cresc.} e d d | % 68
     cis8-> r r cis-> | % 69
     r4 e8-> d\p | % 70
     d8_\markup{\italic cresc.} e e fis | % 71
  }
  \repeat volta 2 {
    g8->g\p g\tenuto_\markup{\italic cresc.} g | % 72
    g8 g\tenuto g g | % 73
    g8-> g_\markup{\italic sim.} f\tenuto g | % 74
    f8 g\tenuto f g | % 75
  }
  \break
  gis8 r r4 | % 76
  r8 g\p f\tenuto g | % 77
  f8 g\tenuto f g | % 78
  f8-> r r4 | % 79
  r8 e\p f\tenuto_\markup{\italic cresc.} e | % 80
  f8 e\tenuto f e | % 81
  \repeat volta 2 {
    e8-> r r cis\mp_\markup{\italic cresc.} | % 82
    d8 e f g | % 83
    \break
    a8\mf\> g f e\! | % 84
    d8 a_\markup{\italic sim.} b cis | % 85
    d8 e f g | % 86
    a g f f | % 87
  }
  cis8\f-> r r e-> | % 88
  r4 cis8-> d-> | % 89
  r4 fis8-> r | % 90
  \break
  r8 d-> cis\ff-> cis\tenuto ( | % 91
  cis8) cis-> cis4\tenuto | % 92
  cis8\p\tenuto_\markup{\italic sub.} cis cis_\markup{\italic cresc.} cis\tenuto | % 93
  cis8 cis cis\tenuto cis | % 94
  cis8 cis\tenuto cis_\markup{\italic rit.} cis | % 95
  f2\ff\tenuto_\markup{\italic poco.} | % 96
  d8\tenuto_\markup{\italic rit.} r cis4\tenuto ( | % 97
  c2)\fermata
  \bar "|."
}