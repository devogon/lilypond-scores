scoreAContrabassI = \relative c {
				\scoreAGlobal
%  \global
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  \repeat volta 2 {
   \partial 4 r4 | % 0
    R1*3/4 | % 1
    r2 r8 \override Hairpin #'minimum-length = #7 b'8\<\mp | % 2
    c4\!\> (e)\! r8 b8_\markup{\italic sim.} | % 3
    c4 (e) r8 b | % 4
    c4 (e_\markup{\italic cresc.}) r8 b | % 5
    \break
    fis'2 r8 fis | % 6
    \clef tenor
    g4\mf\> (a\!) r8 fis\< | % 7
    g4\!\> (a\!) r8 fis_\markup{\italic sim.} | % 8
    g4 (a) r8 fis | % 9
    gis2\tenuto b4 | % 10
    a8_\markup{\italic "dim. poco a poco"} (b) a4. r 8 | % 11
    \break
    a4 g8 (a) g4 ( | % 12
    g8) r g4 fis8 (g) | % 13
    fis2 eis4 | % 14
    b'!4 ais2\tenuto a!2\tenuto gis4 ( | % 15
    g2\fermata)
  }
  \tempo "Allegro"
  \clef bass
 \partial 4 r4\fermata  % 17
 \repeat volta 2 {
   R1*3/4*8 | % 18-25
   \clef tenor
   a4.\mp_\markup{\italic cresc.} b8 (c4) | % 26
   b2 c8 (d) | % 27
  c4. b8 (a4)  % 28
  a2 gis4 | % 29
  a4._\markup{\italic sim.} b8 (c4) | % 30
  b2 c8 (d) | % 31
  c4. b8 (a4) | % 32
  a2 gis4 | % 33
  a2 r4 | % 34
  R1*3/4*3 | % 35-37
  }
  R1*3/4*2 | % 38, 39
  gis4.^\markup{"Più mosso"} gis8 gis4 | % 40
  \break
  as2\tenuto r4 | % 41
  gis4._\markup{\italic sim.} gis8 gis4 | % 42
  as2\tenuto r4 | % 43
  gis4\mp\upbow as4_\markup{\italic cresc.} gis4 | % 44
  as4 gis as | % 45
  gis2\mf\tenuto_\markup{\italic cresc.} gis4\tenuto ( | % 46
  gis4) gis2\tenuto | % 47
  as4.\f as8 as4 | % 48
  \break
  g2\tenuto r4 | % 49
  as4._\markup{\italic sim.} as8 as4 | % 50
  g2 r4 | % 51
  as4\mp\upbow g4_\markup{\italic cresc.} as | % 52
  g4 as g | % 53
  as4\tenuto (g) as\tenuto ( | % 54
  g4) as (g) | % 55
  gis4.\f gis8 gis4 | % 56
  \break
  gis2\tenuto r4 | % 57
  gis4._\markup{\italic sim.} gis8 gis4 | % 58
  gis2\tenuto r4 | % 59
  gis4\mp_\markup{\italic cresc.} gis gis | % 60
  gis4. gis8 gis4 | % 61
  gis2\tenuto gis4\tenuto ( | % 62
  gis4) gis2\tenuto | % 63
  b!4.\f b!8 b!4 | % 64
  \break
  b2\tenuto r4 | % 65
  b4._\markup{\italic sim.} b8 b4 | % 66
  b2\tenuto r4 | % 67
  b4\mp b_\markup{\italic cresc.} b | % 68
  b4 b b | % 69
  b2\tenuto b4\tenuto (| % 70
  b4) b2\f\tenuto\> ( | % 71
  b2\tenuto) r4\fermata\! | % 72
  \bar "||"
  \break
  \repeat volta 2 {
    \mark "a tempo"
    R1*3/4*8_\markup{pizz.} | % 73
%    R1*3/4*7 | % 74 - 80
    a4.\mp_\markup{\italic cresc.} b8 (c4) | % 81
    b2 c8 (d) | % 82
    \break
    c4.\mf_\markup{\italic dim.} b8 (a4) | % 83
    a2 gis4 | % 84
    a4._\markup{\italic sim.} b8 (c4) | % 85
    b2 c8 (d) | % 86
    c4. b8 (a4) | % 87
    a2 gis4 | % 88
    a2 r4 | % 89
    R1*3/4*3 | % 90 - 92
  }
  r1*3/4 % 93
  \clef bass
  e2.\mp_\markup{\italic dim.}\fermata
  \bar "|."
 }