scoreAContrabassIV = \relative c {
  \scoreAGlobal
  \repeat volta 2 {
    \partial 4 r4 | % 0
    a4\mp\> (e) (e8\!) r | % 1
    a4_\markup{\italic sim.} (e) (e8\!) r | % 2
    a4 (e) (e8\!) r | % 3
    a4 (e) (e8\!) r | % 4
    a4 (e) (e8\!) r | % 5
    \break
    b'4_\markup{\italic cresc.} fis' (b,8) r | % 6
    e4\mf a,\> (b8) r\! | % 7
    e4_\markup{\italic sim.} a, (b8) r | % 8
    e4 a, (b8) r | % 9
    e,2\tenuto r4 | % 10
    r4 c'_\markup{\italic dim. poco a poco} b8 (c) | % 11
    \break
    b8 r r4 bes | % 12
    a8 (bes) a r r4 | % 13
    a4 g8 a g4 ( | % 14
    g4) fis2\tenuto | % 15
    b2\tenuto e,4\p ( | % 16
    e2\fermata) % 17
  }
  \break
  \partial 4 r4\fermata
  \repeat volta 2 {
    a8\mp_\markup{\italic cresc.}^\markup{pizz.} e' r e a, r | % 18
    d8 b' r b d, r | % 19
    e8\mf_\markup{\italic dim.} g r g c, r | % 20
    e8 b' r b e, r | % 21
    a,8\mp_\markup{\italic cresc.} e' r e a, r | % 22
    \break
    d8 b' r b d, r | % 23
    e8\mf_\markup{\italic dim.} g r g c, r | % 24
    e8 b' r b e, r | % 25
    a,8_\markup{\italic sim.} e' r e a, r | % 26
    d8 b' r b d, r | % 27
    c8 g' r g c, r | % 28
    \break
    e8 b' r b e, r | % 29
    a,8 e' r e a, r | % 30
    d8 b' r b d, r | % 31
    c8 g' r g c, r | % 32
    e8 b' r b e, r | % 33
    a,\mp_\markup{\italic cresc.} e' r e a, r | % 34
    \break
    d8\mf_\markup{\italic dim.} e r e b r | % 35
    a8_\markup{\italic sim.} e' r e a, r | % 36
    d8 e r e b r | % 37
  }
  a8\mp_\markup{\italic cresc.} fis' r e' c r | % 38
  f,8\mf_\markup{\italic cresc.} a r dis b r | % 39
  e,4.\mf^\markup{arco} e8 e4 | % 40
  \break
  f2\tenuto r4 | % 41
  e4._\markup{\italic sim.} e8 e4 | % 42
  f2\tenuto r4 | % 43
  e4\upbow\mp_\markup{\italic cresc.} f e | % 44
  f4 e f | % 45
  e4\mf\tenuto_\markup{\italic cresc.} (f) (e\tenuto) ( | % 46
  f4) e\tenuto (f) | % 47
  des4.\f des8 des4 | % 48
  \break
  d2\tenuto r4 | % 49
  des4._\markup{\italic sim.} des8 des4 | % 50
  d2 r4 | % 51
  des4\upbow\mp_\markup{\italic cresc.} d!4 des | % 52
  d!4 des d! | % 53
  des4\tenuto (d!4) des\tenuto ( | % 54
  d!4) des\tenuto ( d!) | % 55
  bes4.\f bes8 bes4 | % 56
  \break
  e,2\tenuto r4 | % 57
  bes'4._\markup{\italic sim.} bes8 bes4 | % 58
  e,2\tenuto r4 | % 59
  bes'4\mp e,4_\markup{\italic cresc.} bes' | % 60
  e,4 bes' e, | % 61
  bes'4\tenuto (e,) bes'\tenuto ( | % 62
  e,4) bes'\tenuto (e,)  | % 63
  g4.\f g8 g4
  \break
  des'2\tenuto r4 | % 65
  g,4. g8_\markup{\italic sim.} g4 | % 66
  des'2\tenuto r4 | % 67
  g,4\mp_\markup{\italic cresc.} des' g, | % 68
  des'4 g, des' | % 69
  g,2\tenuto des'4\tenuto ( | % 70
  d4) e,2\tenuto\f\> ( | % 71
  e2)\fermata r4\fermata\! | % 72
  \break
  \repeat volta 2 {
    a8\mp^\markup{pizz.}_\markup{\italic cresc.} e' r e a, r | % 73
    d8 b' r b d, r | % 74
    e8\mf_\markup{\italic dim.} g r g c, r | % 75
    e8 b' r b e, r | % 76
    a,8_\markup{\italic sim.} e' r e a, r | % 77
    \break
    d8 b' r b d, r | % 78
    e8 g r g c, r | % 79
    e8 b' r b e, r | % 80
    a,8_\markup{\italic sim.} e' r e a, r | % 81
    d8 b' r b d, r | % 82
    \break
    c8 g' r g c, r | % 83
    e8 b' r b e, r | % 84
    a,8 e' r e a, r | % 85
    d8 b' r b d, r | % 86
    e8 g r g c, r | % 87
    e8 b' r b e, r | % 88
    \break
    a,8_\markup{\italic sim.} e' r e a, r | % 89
    d8\mf_\markup{\italic dim.} e r e b r | % 90
    a8 e' r e a, r | % 91
    d8 e r e b r | % 92
  }
  a8\mp r r2^\markup{arco} | % 93
  a2.\fermata_\markup{\italic dim.} | % 94
}