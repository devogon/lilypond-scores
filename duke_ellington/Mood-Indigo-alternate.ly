\version "2.19.50"
\language "nederlands"

\header {
  title = "Mood Indigo"
  subtitle = "(alternate)"
  instrument = "Bass"
  composer = "Duke Ellington, Albany Brigard and Irving Mills"
  arranger = "Roger Pemberton"
%  meter = "Mod. Slow Ballad"
  copyright = "1931 Mills Music, Inc."
  % Remove default LilyPond tagline
%  tagline = ##f
  tagline = \markup{\smaller "(AZZ Combo Pak #3 - 3)"}
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key as \major
  \time 4/4
  \tempo "Mod. Slow Ballad" 4=72
}

bass = \relative c {
  \global
  % Music follows here.
  as4\mp es g as
  bes4 f d! bes
  es4 es des g,
  aes' g f es

  as4 es g as | % 5
  bes4 bes, d! f | % 6
  e!4 e b'! e,!
  es4 es bes' es,

  as4 as bes c | % 9
  es,4 bes' as c
  des4 c bes as
  ges bes, des as' | % 12
  as4 as g as
  bes4^\markup{\italic "To Coda"} f d! bes
  es4\coda es g es
  as8. as16 g4 f es | % 16

  as4 as, c es
  f4 bes, c d!
  bes4 c des es
  as8. ges16 f4 bes8. fes16 es4
  as4 as, c es | % 21
  f4 bes, c d!
  f4 g as bes
  es,8. es16 des4 c bes

  r8 as'4.-> r8 as4.-> | % 25
  r8 as4.-> r8 as4.->
  r8 as4.-> r8 as4.->
  des4 ces bes es,
  as8. as16 g4 f es
  bes4 c d! f
  bes4 f es g
  as4 f bes, es

  \repeat volta 2 {
    as4^\markup{\box "Open for Jazz Solos"} as ges as | % 33
    bes4 f d! bes
    bes4 c des es
    as g f es
    as4 as es as | % 37
    bes,4 c d! f
    e!4 b'! gis e
    es4 es f g

    as4 as ges f | % 41
    es4 es as, c
    des4 es f as
    ges4 des bes ges
    as4 bes c es
    bes'4 f d! bes
    bes4 c des es
    as es f g

    as4 g f es | % 49
    d!4 c bes d
    des4 bes es g
    as4 f bes es,
    as g f es
    d!4 c bes d
    f4 g as bes
    es,4 bes g es

    as4 bes c des | % 57
    es4 f ges as
    des4 as f des
    des4 as' fes es
    as4 g as a!
    bes4 f d! bes
    bes'4 as g es
    as4 f bes e,^\markup{\box \right-align "After all solos D.C. Al Coda"}
  }
  \break
  es2\coda es,2
  es'4 es f g
  as4 f es c
  as'1\fermata^\markup{"arco"}
  \bar "|."
%  \pageBreak

}

\score {
  \new Staff \with {
 %   midiInstrument = "acoustic bass"
 %   instrumentName = "Bass"
 %   shortInstrumentName = "Bs."
  } { \clef "bass_8" \bass }
  \layout {
    ragged-last = ##t
  }
  \midi { }
}

\markup {
  \teeny
  \date
}