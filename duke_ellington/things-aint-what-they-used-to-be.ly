\version "2.19.57"
\language "nederlands"

\header {
  title = "Things Ain‘t What They Used To Be"
  instrument = "Contrabas"
  composer = "Duke Ellington"
  arranger = "Mercer Ellington"
  meter = "Medium Swing"
  % Remove default LilyPond tagline
  tagline = ##f
}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key d \major
  \time 4/4
%  \tempo 4=108
}

contrabass = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    \tuplet 3/2 {r8 a d} \tuplet 3/2 {fis a b} a4 b8 d, (
    d4.) fis8 r2
    \tuplet 3/2 {r8 a, d} \tuplet 3/2 {fis a b} a4 b8 d (
    d4.) fis8 r2
    \tuplet 3/2 {r8 d, g} \tuplet 3/2 {b d e} d4 e8 g, (
    g4.) b8 r b a fis
    \tuplet 3/2 {r8 a, d} \tuplet 3/2 {fis a b} a4 b8 d, (
    d4.) fis8 r4 r8 a8 |
    d8 cis a4 (a4.) a8 |
    \tuplet 3/2 {d cis a} \tuplet 3/2 {d cis a} b4 cis8 d ( |
    d1)
    r1
  }
    % aca x4
    % dfd x2
    %  aca x2
    % ege

  \key c \major
  \break
  \repeat volta 2 {
  d,4 d' c a
  d,4 c b bes
  a4 b c cis
  \tuplet 3/2 {d8 e f} \tuplet 3/2 {fis8 g gis} \tuplet 3/2 {a8 gis g!} \tuplet 3/2 {fis8 f! e} |
  \break
  g4 b, c cis
  d4 g \tuplet 3/2 {fis8 g gis} \tuplet 3/2 {a g fis} |
  d4 d' c8 a a,4 |
  d4 fis, g gis |
  \break
  a4 d g gis
  \tuplet 3/2 {a8 g e} a,4 (cis) e |
  d4 fis g gis
  a4 g fis a,
  \break
  d4 e' d c
  b4 bes a g
  fis4 g a8 a, b4
  c4 cis d8 g d' g,
  \break
  \tuplet 3/2 {g'4^\markup{g} a ais} b8 \override NoteHead.style = #'cross g, \revert NoteHead.style ais g  | % 29
  b,4 d g, gis
  a4 d fis a
  d4 \tuplet 3/2 {e4 f fis} g8^\markup{\teeny g} gis |
  \break
  a4^\markup{a} aes g fis
  fis4 d8 cis a as g f |
  d4 c b bes
  a g e a
  \break
  d4 d' cis a
  g4 gis a8 \override NoteHead.style = #'cross d, \revert NoteHead.style a4
  d4 c b bes
  a8 c d4 fis a |
  \break
  g4 ais b d
  g4^\markup{g} a^\markup{a} g a,,
  d4 a' as g
  fis4 d b fis
  \break
  e4 fis g gis
  a4 g'8 g e g a e |
  d8 d c'4 b bes
  a4 g fis e
  \bar "|."
}
}


contrabassPart = \new Staff \with {
%  instrumentName = "Contrabass"
%  shortInstrumentName = "Cb."
} { \clef bass \contrabass }

\score {
  <<
    \contrabassPart
  >>
  \layout { }
}

\markup	{
  \teeny
  "2x thema, 1x dubbele stops, 2x walking, 2x thema, eind vertragen akkord D7"
  \vspace #1
}
\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
  \vspace #1
}
\markup {
  \teeny
  \date
}