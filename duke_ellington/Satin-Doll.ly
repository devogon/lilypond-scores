\version "2.19.50"
\language "nederlands"

\header {
  title = "Satin Doll"
  instrument = "Bass"
  composer = "Duke Ellington, Johnny Mercer and BillyStrayhorn"
  arranger = "Roger Pemberton"
  copyright = "1953 Tempo Music, Inc."
  tagline = "Z Combo Pak #3 - 2"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

#(define (custom-script-tweaks ls)
  (lambda (grob)
    (let* ((type (ly:prob-property
                    (assoc-ref (ly:grob-properties grob) 'cause)
                    'articulation-type))
           (tweaks (assoc-ref ls type)))
      (if tweaks
          (for-each
            (lambda (x) (ly:grob-set-property! grob (car x) (cdr x)))
            tweaks)))))

customScripts =
#(define-music-function (parser location settings)(list?)
#{
  \override Script.before-line-breaking =
    #(custom-script-tweaks settings)
#})

#(define my-settings
   '(
    ("coda" . ((color . (0 1 0.3))(padding . 0.5)))
    ("segno" . ((color . (1 0 1))(padding . 0.5)))
  )
)

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 4/4
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
  \tempo "Moderate" 4=112
}



contrabass = \relative c {
  \global
  \customScripts #my-settings
  % Music follows here.
%  \override Score.BarNumber.break-visibility = ##(#t #t #t)
  \repeat percent 3 { r2^\markup{\box "Solo"} \tuplet 3/2 {r8 g'\f d} g,4  }
  r2 \tuplet 3/2 {r8 a'\> e} \tuplet 3/2 {g e cis\!} |
  \repeat volta 2 { %\override Staff.BarLine.color = #red
      d4.\segno^\markup{"Take repeat on D.S."} d8 g,4. g8 |
      \override Staff.BarLine.color = #black
                    d'4. d8 g,4. dis'8
                    e4. e8 a,4. a8
                    e'4. e8 \tuplet 3/2 {a,8 a' e} \tuplet 3/2 {g8 cis, e} |
                    a4 a d, a'
                    as as des, g
  }
                    \alternative {
                      { c4 c b b   bes bes a a }
                      { c b a g   f8. f16 e4 d c }
                    }
  g'4 g c c | % 15
  g g c, e | % 16
  f8.\< fis16 g8 as8\!\f (as8.\>) g16 fis8 f!\!\mf\marcato
  r1
  a4 a d d
  a a d, fis
  g8.\< (a16 ais8) b (b8.) (c16 cis8\! d\marcato) |
  r1
  \bar "||"
  d,4. d8 g,4. g8
  d'4. d8 g,4. dis'8
  e4. e8 a,4. a8
  e'4. e8 \tuplet 3/2 {a,8 a' e} \tuplet 3/2 {g8 cis, e}
  a4 a d, a' | % 27
  as as des, g
  c b a g^\markup{"to coda"}\coda
  e a, cis e
  \repeat volta 2 {
    \repeat percent 2 {d4 d g g}
    \repeat percent 2 {e4 e a a}
    a4 a d, d
    as'4 as des, des
  }
  \alternative {
    { c4 c b' b bes bes a a }
    { c,4 d e g  c g e c }
  }
  \bar "||"
  g'4 a bes d
  c bes a g
  f g a c
  f c a f
  a b c e
  d c b a
  g a b c
  d b a g
  \bar "||"
  d d g g
  d d g g
  \repeat percent 2 {e e a a}
  a a d, d
  as' as des, des
  c d e g
  a, a b cis^\markup \column {\right-align \line {"After all solos"} \right-align \line {\with-color #magenta "D.S. al Coda"} }
  \bar ":|]"
  \break
  f8.\coda f16 e4 d c
  \bar "||"
  \repeat percent 3 {r2^\markup{\box "solo"} \tuplet 3/2 {r8 g'\f d} g,4}
  r2 \tuplet 3/2 { r8 c' g} \repeat tremolo 4 c,16\fermata |
  \bar "|."

}

\score {
  \new Staff \with {
   % instrumentName = "Contrabass"
   % shortInstrumentName = "Cb."
   % midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}

\markup {
  \teeny
  \date
}