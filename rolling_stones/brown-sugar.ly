\version "2.19.83"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Brown Sugar"
  subtitle = "Rolling Stones"
  subsubtitle = "Sticky Fingers - 1971"
  instrument = "Bass"
  composer = "Mick Jagger/Keith Richards"
  arranger = "producer: Jimmy Miller"
  copyright = "Rolling Stone Records"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
   ragged-last = ##t
 %  ragged-bottom = ##t
}

\layout {
  \omit Voice.StringNumber
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=130
}

% https://tabs.ultimate-guitar.com/tab/the-rolling-stones/brown-sugar-bass-1694502

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*2
  \mark \markup{\small "3x"}
  \repeat volta 3 {
    g'8\2 g\2 g\2 g\2 g\2 g\2 g\2 g\2
    c,8 c c c c c c c
  }
  \break
  \set Score.currentBarNumber = #9
  \repeat unfold 6 {es8\3 } r c|
  \repeat unfold 8 {c}
  as as as4 bes8\4 bes\4 bes\4  c
  c8 c e c e f ges g\2 |
  \break
  es8\3 es\3 es\3 es\3 g\2 bes g\2 c,16\3 c\3 |
  \repeat unfold 7 {c8} as |
  as as as bes\4 bes\4 bes\4 bes4\4 |
  c8 c ges'\2 ges\2 f ges g\2 c |
  \bar "||"
  \break
  \mark \markup{1st Verse}
  \repeat unfold 8 {c,8}
  \repeat unfold 6 {c8} es g
  f4 f8 f f f f f |
  f8 es g4 f8 es g d|
  \repeat unfold 14 {c8} c4
  \repeat unfold 7 {bes'8} b8
  \repeat unfold 7 {c8} bes8
  \bar "||"
  \break
  \mark \markup{Chorus}
  g,4 g  \grace a8\4 (b8\4) d\3 e\3 g\2 ~ |
  g8\2 g4\2 g8\2 e\3 d\3  b\4 c |
  c c c g c c c g |
  c c c d\3 e f ges g\2 |
  \break
  g,4 g  \grace a8\4 (b8\4) d\3 e\3 g\2 ~ |
  g4 g\2 es8\3 d\3 c\3 a\3 |
  c8 g c4 c8 c a c|
  c8 c c c c c c es\3 |
  \break
  \bar "||"
  \mark \markup{Interlude}
  \repeat unfold 3 {es4\3} r |
  c e f8 ges g\2 as\2 ~ |
  as\2 as\2 as\2 bes bes bes r c, ~ |
  c4 d8\3 f ges bes b c |
  \bar "||"
  \break
  \mark \markup{2nd Verse}
  c,4 c8 c c a c c |
  \repeat unfold 7 {c8} ges' |
  f8 f f8 f f f f f |
  f8 es g4 f8 es g d|
  \repeat unfold 14 {c8} c4
  \repeat unfold 7 {bes'8} b8
  \repeat unfold 6 {c8} c4 |
  \bar "||"
  \break
  \mark \markup{Chorus}
  g,4 g  \grace a8\4 (b8\4) d\3 e\2 g\2 ~ |
  \repeat unfold 5 {g8\2} d g4\2|
  c,8 c c g c c c g |
  c c c d\3 e f ges g\2 |
  \break
  \repeat unfold 4 {g8\2} \grace a,8\4 (b8\4) d\3 e\3 g\2 ~ |
  \repeat unfold 4 {g8\2} e8\3 d\3 g4\2 |
  c,8 c c g c c c g|
  c8 c c c c c c es\3 |
  \break
  \bar "||"
  \mark \markup {Sax Solo}
  es4\3 es\3 es8\3 es\3 es\3 c |
  c4 e\3 f8 ges g\2 as,\4 |
  as4\4 as\4 bes8\4 bes\4 bes\4 c |
  c8 c c c c c c c ( |
  \break
  es4\3) es\3 es8\3 g4.\2 |
  r8 g\2 g4\2 e8\3 f\2 ges\2 g\2 |
  as,4\4 as\4 bes8\3 bes\3 bes4\3 |
  c4 d\3 e8\3 f ges g\2 |
  \break
  es8\3 es\3 es4\3 es8\3 g\2 es4\3 |
  \repeat unfold 8 {c8}
  as8\4 as\4 as4\4 bes8\4 bes\4 bes4\4 |
  c4 e\3 f8 ges g\2 es\3 |
  \repeat unfold 6 {es8\3} es4\3 |
  \repeat unfold 6 {c8\3} c4\3 |
  \repeat unfold 4 {as8\4} \repeat unfold 4 {bes8\4} |
  c4 e\3 f ges |
  \break
  \bar "||"
  \mark \markup{Chorus}
  \repeat unfold 8 {g8\2}
  \repeat unfold 7 {c8} d,\2
  \repeat unfold 6 {c8} a c |
  \repeat unfold 5 {c8} d\3 e\2 f |
  \break
  g\2 g, g g \grace a\4 b\4 c d\3 g\2 |
  g\2 g\2 g\2 g\2 e\3 d\3 g\2 b, |
  c c c g c c c g |
  c c c d\3 e f ges g\2 |
  \bar "||"
  \break
  \mark \markup {3rd Verse}
  \repeat unfold 6 {c,8} a\3 c |
  c c c c c c d4\3 |
  f8 f f f f f f f |
  f es g4 f 8 es g d8 |
  \break
  \repeat unfold 6 {c8} a\3 c |
  c c c c c c c4\3 |
  \repeat unfold 8 {bes'8}
  c8 bes c4 c c |
  \break
  \bar "||"
  \mark \markup {Chorus}
  \repeat volta 4 {
    g,4 g \grace a8\4 b\4 d\3 e g\2 |
    g4\2 g8\2 g\2 e\3 d4.\3 |
    c8 c c g c c c g |
  }
  \alternative {
    {c8 c c c e f ges g\2 \break}
    {c, c c c c c c c}
  }
  g8\4 g8\4 g8\4 g8\4 \grace a8\4 b\4 d\3 e\3 g\2 |
  r8 g4\2 g8\2 e\3 d\3 b\4 g |
  c8 c a\4 g c c a\4 g |
  \break
  c8 c a\4 g c c a\4 g |
  g8\4 g8\4 g8\4 g8\4 \grace a8\4 b\4 d\3 e\3 g\2 |
  g8\2 g8\2 g8\2 g8\2 e\3 d\3 b\4 g |
  c8 c a\4 g c c c g |
  \break
  c8 c a\4 g c f ges g\2 |
  g,8\4 g8\4 g8\4 g8\4 \grace a8\4 b\4 d\3 e\3 g\2 |
  g8\2 g8\2 g8\2 g8\2 e\3 d\3 b\4 g |
  c8 c c  g c c c c |
  \break
  c8 c e e f f g\2 g\2 |
  \repeat volta 2 {
    g,8\4 g8\4 g8\4 g8\4 \grace a8\4 b\4 d\3 e\3 g\2 |
    g8\2 g8\2 g8\2 g8\2 e\3 d\3 b\4 g |
    c c c g c c c c |
    \break
    c c c c c c e f |
  }
  \repeat unfold 16 {g8\2}
  c, c c c c c c g |
  \break
  c c c c c c e f |
  \mark \markup {Outro}
  \repeat volta 3 {
    \repeat unfold 16 {g8\2}
  }
  \alternative {
    {c, c c g c c c g \break
    c c c c c  e f ges
    }
    {c, c e e f g\2 g, g  c c g c ~ c2}
  }
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
