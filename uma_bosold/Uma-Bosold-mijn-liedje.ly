\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "mijn liedje"
  composer = "Uma Bosold"
  %poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


%\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g  \major
  \time 4/4
  \tempo 4=96
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c' {
   \global
  e fis g a b a g2 g4 a fis g e fis e2 g4 a fis c' c1
   }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef treble \contrabas }
  \layout { }
}



