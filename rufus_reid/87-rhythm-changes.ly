\version "2.19.80"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "The Evolving Bassist"
  subtitle = "rhythm changes (bz 87)"
  instrument = "Bass"
  composer = "Rufus Reid"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
}

contrabass = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    bes'4 as g b! |
    c4 e, f es |
    d4 c b g |
    c4 bes a f' |
    \break
    bes4 d as f |
    g es ges es |
  }
  \alternative {
    {f d g b,  c es f a \break}
    {d, c bes f bes f' g gis}
  }
  \bar "||"
  g4 bes c g'^\markup{g} |
  fis4 d e fis |
  \break
  g4 d e fis |
  g4 b, d d, |
  g4 d bes d |
  e c g' e |
  \break
  es4 c g' d' |
  c4 f, ges a |
  \bar "||"
  bes4 f b! g |
  c4 g f es' |
  \break
  d4 f g f |
  es4 c a8. ges16 f4 |
  bes,4 a as bes |
  g8. c16 es4 ges c |
  \break
  bes4 as g ges |
  f g a c |
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
