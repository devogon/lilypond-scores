\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Wedding March"
  instrument = "Violoncello"
  composer = "Richard Wagner"
  arranger = "Wanda Sobieska"
  copyright = \markup{\tiny {2008 - avail at www.freegigmusic.com}}
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 2/4
  \tempo "Moderato" %4=83
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

cello = \relative c {
  \global
  % Music follows here.
  R1*2
  \repeat volta 2 {
  bes'4\p_\markup{\italic "sempre legato"} bes8 r16 bes | % 5
  bes4. r8 | % 6
  bes4 f8 r16 f | % 7
  bes4. r8 | % 8
  bes4 bes8. bes16 | % 9
  bes2 (  | % 10
  bes4) g8. g16 | % 11
  f4. r8\breathe | % 12
  \break
  bes4 bes8 r16 bes | % 13
  bes4. r8 | % 14
 % \break
  bes4 f8 r16 f | % 15
  bes4. r8 | % 16
  bes4 bes8. bes16 |
  d,4 g8. g16 |
  es4 f8. f16 |
  r8\p \tuplet 3/2 {bes16\> bes bes} bes8 bes|
  g4\!-> f8-> es-> |
  c4-> c-> |
  \break
  f,4\mf f8. f16 |
  f4. r8 |
  g'4\p f8 es |
  c4 c |
 % \break
  g'4\p d8. g16 | % 27
  g4.\< g8\p | % 28
  g2\! ( |
  g4) g |
  d4. d8 |
  d4. d8 |
  b!4 b8 b |
  e!4 e8 e|
  \break
  d4 cis8. cis16 |
  d4. d8 |
  g4\p\< g8 g\! |
  f4\> f |
  f4\p\! f8 f |
 % \break
  bes2 | % 40
  bes4 g8 g |
  f4_\markup{\italic dim.} g |
  a4 a8.\p a16 |
  d,4 r |
  }
  bes'4 bes8 r16 bes |
  \break
  bes4. r8 |
  bes4 f8 r16 f |
  bes4. r8 |
  bes4 bes8. bes16 |
  bes2 ( |
  bes4) g8. g16 |
%  \break
  f4. r8 | % 52
  bes4 bes8 r16 bes |
  bes4. r8 |
  bes4 f8 r16 f |
  bes4. r8 |
  \break
  bes4 bes8._\markup{\italic cresc.} bes16 |
  d,4 g8. g16 |
  es2\f |
  f4 f_\markup{\italic dim.} |
  bes4\p bes,8. bes16 |
%  \break
  bes4_\markup{\italic cresc.} bes |
  bes4 bes8. bes16 | % 64
  bes4 bes8. bes16 bes4 bes | % 65
  bes4 bes |
  bes4_\markup{\italic dim.} bes |
  bes4 bes |
  bes2\> (
  bes2) ( |
  bes4\!\pp) r4
  \bar "|."
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}
