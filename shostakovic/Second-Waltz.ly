\version "2.19.82"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Second Waltz"
  instrument = "Contrabas, Tuba"
  composer = "Dmitri Shostakóvic"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \numericTimeSignature
  \time 3/4
  \tempo 4=152
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative c' {
  \global
  % Music follows here.
  R1*3/4*4
  \repeat volta 2 {
    g2.\p ( | %
    es2 d4 | %
    c2. ~ ) | %
    c4 c ( d | %
    es c es | %
    g2 as4 | %
    g2. | %
    \break
    f2. ) | %
    f2. ( | %
    d2 c4 | %
    b!2. ) ~ | %
    b4 ( g b! | %
    d4 b! d | %
    f4 g as | %
    fis2. | %
    g2. ) | %
    es'2. ( | %
    d2 c4 | %
    bes2 as4 | %
    \break
    f2. ) | %
    d'2. ( | %
    c2 bes4 ) |  %
    bes2. | %
    r4 es,\staccato f\staccato | %
    g4\staccato g8 (f g as) | %
    f4\staccato f8 ( es f g ) | %
    es4\staccato r g\staccato | %
    r4 es4\staccato f\staccato | %
    g4\staccato g8 (f g as) | %
    \break
    f4\staccato f8 ( es f g ) | %
    es4\staccato r g\staccato | %
    r4 c\staccato\mf d\staccato | %
    es4\staccato es8 (d es f) | %
    d4\staccato d8 ( c d es ) | %
    c4\staccato r r | %
    R1*3/4*3
    g2.\mf ( | %
    \break
    e2 d4 | %
    c2. ) ~ | %
    c4 c ( d | %
    es4 c es | %
    g2 as4 ) | %
    g2. ( | %
    f2. ) | %
    f2. (d2 c4 | %
    b!2. ) ~ | %
    b4 g ( b! | %
    \break
    d4 b! d  | %
    f4 g as ) | %
    fis2. ( | %
    g2. ) | %
    es'2. ( | %
    d2 c4 | %
    bes2 as4 | %
    f2. ) | %
    d'2. (
    c2 bes4 )
    bes2.
    r4 es,4\f f4\staccato
    \break
    g4\staccato g8 (f g as ) |
    f4\staccato f8 ( es f g )  | %
    es4\staccato r g\staccato | %
    r4\mf es\staccato f\staccato | %
    g4\staccato g8 ( f g as ) | %
    f4\staccato f8 ( es f g ) | %
    es4\staccato r g\staccato | %
    r4\f c\staccato d\staccato | %
    \break
    es4\staccato es8 ( d es f ) | %
    d4\staccato d8 (c d es ) | %
    c4\staccato r r | %
    r2 bes4-> | %
    es2. | %
    es2. | %
    es4 (d c) | %
    bes4 g bes | %
    d2. | %
    d2. | %
    c4 (bes g) | %
    \break
    es4 ( f g ) | %
    c2.
    bes2.
    bes4 ( as g)
    f4 ( es f)
    g2 (bes4)
    f2 (bes4)
    g2 (bes4)
    es2 (f4)
    g2.
    g2.
    g4 ( f es)
    \break
    d4 (bes d)
    f2.
    f2.
    f4 (es d)
    c4 (g bes)
    es2.
    es2.
    es2.
    f4 (es f)
    g2 (es4)
    bes4 ( c d)
    \break
    es4 ( f es)
    des4 (c b!)
    c2\f ( es,4 ~
    es4 d es)
    c'2 ( es,4 ~
    es4 des' c ) | %
    c2 (bes4)
    a!2 (bes4)
    f'2 (es4)
    des4 (c b! )
    \break
    c2 (es,4 ~ | %
    es4 d es)  | %
    c'2 ( es,4 ~ | %
    es4 f g )
    as2 (bes4)
    c2.
    d4 ( c d)
    es2 (des4)
    c8\f (b!) c\staccato as\staccato es\staccato d\staccato | %
    \break
    es8 (c) d\staccato f\staccato es\staccato as\staccato | %
    c8 (b!) c\staccato f\staccato es\staccato c\staccato | %
    as8 (g) as\staccato des\staccato c\staccato as\staccato | %
    es8 (d) es\staccato bes'\staccato g\staccato es\staccato | %
    des8 (c) des\staccato es\staccato g\staccato des'\staccato | %
    \break
    es,8 ( g) bes\staccato des\staccato f\staccato es\staccato | %
    des8 (bes) g\staccato f\staccato d\staccato b'!\staccato | %
    c8 (b!) c\staccato as\staccato es\staccato d\staccato | %
    es8 ( c) d\staccato f\staccato es\staccato as\staccato | %
    c8 (b!) c\staccato f\staccato es\staccato c\staccato | %
    \break
    as8 (g) as\staccato des\staccato c\staccato as\staccato | %
    f8 (e!) f\staccato des'\staccato bes\staccato g\staccato | %
    f8 (e!) d\staccato c\staccato d\staccato es\staccato  | %
    f8 (g) as\staccato bes\staccato c\staccato d\staccato  |%
    es8 (f) es\staccato d\staccato es\staccato f\staccato  | %
    g2.\fermata | %
    r1*3/4
    \break
    r1*3/4
    r1*3/4
    r1*3/4
    g,2.\mf (
    es2 d4
    c2.) ~
    c4 c4 (d
    es4
    c es
    g2 as4
    g2.
    f2. )
    f2. (
    d2 c4
    \break
    b!2. ) ~
    b4 g4 ( b
    d4 b d
    f g as
    fis2.
    g2. )
    es'2.\mp (
    d2 c4
    bes2 as4
    f2. )
    d'2. (
    \break
    c2 bes4
    bes2. )
    r4 es,4\staccato\mf f\staccato
    g4\staccato g8 (f g as) | %
    f4\staccato f8 ( es f g ) | %
    es4\staccato r4 g\staccato
    r4\f es\staccato f\staccato
    g4\staccato g8 ( f g as) | %
    \break
    f4\staccato f8 (es f g) | %
    es4\staccato r g\staccato |%
    r4\ff c\staccato d\staccato
    es4\staccato es8 ( d es f) | %
    d4\staccato d8 ( c d es) | %
    c4\staccato r g
    c4\fff r2
  }

}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
