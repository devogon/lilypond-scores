\version "2.19.54"
\language "nederlands"

\header {
  title = "Shlof main Kind"
  instrument = "Contrabas"
  poet = \markup{\huge \bold \circle "5"}
				% Remove default LilyPond tagline
  piece = \markup \column {
    \line{\italic \small "Dat beeld raak ik nooit meer kwijt...hoe hij daar stord....een schim uit de hel"}
    \line{.}
  }
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 4/4
  \tempo "Andante" 4=80
}
% andante 76-108 (at a walking pace)

contrabass = \relative c {
  \global
  % Music follows here.
  a1\p | % 1
  a1 | % 2
  d2 g, | % 3
  c2 e | %  4
  a,1 | % 5
  d1 | % 6
  g,1 | % 7
  c1 | %  8
  \break
  f,1\> | % 9
  f2 g\! | % 10
  c2. (cis4) | % 11
  d1 | % 12
  d1 | % 13
  e2\< a,2\! | % 14
  d2\> e2 | % 15
  a,2 r\!
  \bar "||"
  \break
  a1\p | % 17
  a1 | % 18
  d2 g, | % 19
  c2 e | %  20
  a,1 | % 21
  d1 | % 22
  g,1 | % 23
  c1 | %  24
  \break
  f,1\> | % 25
  f2 g\! | % 26
  c2. (cis4) | % 27
  d1 | % 28
  d1 | % 29
  e2\< a,2\! | % 29
  d2\> e2 | % 30
  a,1\! | % 31
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup \column {
  \line {\tiny  \italic {. …mijn vader was sportinstructeur…}}
  \line {\tiny \italic {hij reed op een motor..dat was heel bijzonder in die tijd.. mijn moeder zei dat hij heel sportief was en sterk…}}
  \line {\tiny \italic \underline {Vader zit in het verzet...}}
  \line {\tiny {\box \bold "6. Curtain Tune"}}
  \hspace #1

}
\markup {
  \teeny
  \date
}