\version "2.19.54"
\language "nederlands"

\header {
  title = "Shlof main Kind"
  instrument = "Contrabas"
  poet = \markup{\huge \bold \circle "3"}
  % Remove default LilyPond tagline
  tagline = ##f
  piece = \markup \column {
    \line {\small \italic {...midden in de oorlog, in 1943...}}
    \line{.}
  }
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 4/4
  \tempo "Andante" 4=80
}
% andante 76-108 (at a walking pace)

contrabass = \relative c {
  \global
  % Music follows here.
  a1\pp | % 1
  a1 | % 2
  d2 g, | % 3
  c2 e | %  4
  a,1 | % 5
  d1 | % 6
  g,1 | % 7
  c1 | %  8
  \break
  f,1\> | % 9
  f2 g\! | % 10
  c2. (cis4) | % 11
  d1 | % 12
  d1 | % 13
  e2\< a,2\! | % 14
  d2\> e2 | % 15
  a,2 r\!
  \bar "||"
  \break
  a1\p | % 17
  a1 | % 18
  d2 g, | % 19
  c2 e | %  20
  a,1 | % 21
  d1 | % 22
  g,1 | % 23
  c1 | %  24
  \break
  f,1\> | % 25
  f2 g\! | % 26
  c2. (cis4) | % 27
  d1 | % 28
  d1 | % 29
  e2\< a,2\! | % 29
  d2\> e2 | % 30
  a,1\! | % 31
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup \column {
  \line {\small "pianissimo at begin, graduate to forte at end"}
  \line {.}
  \line {\small \italic "M: ik fietste elke dag van Roden naar Groningen en ik..."}
  \line {\small \italic "B: zat u daar op school?"}
  \line {\small \italic "M.: Ik was 18 en wachtte op de liefde, zoals iedereen van 18"}
  \line {.}
  \line {\small \underline "snel naar volgende lied" }
  \hspace #1
  \line {\box \bold "4. Liebesfreud"}
  \hspace #1
}

\markup {
\teeny \date }