\version "2.19.54"
\language "nederlands"

\header {
  title = "Liebesleid"
 % subtitle = "(2)"
  instrument = \markup { \center-column { \line{"Violoncello II"} \line{"Contrabas"} } }
  composer = "F. Kreisler"
  arranger = "R. Yates"
  poet = \markup{\huge \bold \circle "10"}
  % Remove default LilyPond tagline
  tagline = ##f
  piece = \markup \column {
    \line {\italic \small "Laat mij uw koffer dragen, hij is veel te zwaar voor U..."}
    \hspace #1
  }
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
  \tempo "Tempo di Ländler con sentimento"
}

contrabass = \relative c {
  \global
  % Music follows here.
  a4\mf\>^\markup{"pizz."} r r
  a r r
  a r r
  a r r\!
  a r r
  c r r
  d r r
  d r r
  \break
  b r r
  g r r
  c r r
  c r r
  a r r
  f r r
  d' r r
  d r r
  \break
  e, r r
  e r r
  r^\markup{\teeny "(vc. arco)"} r e
  a r r
  a r r
  a r cis
  d r a
  \break
  d4 r r % was d8 r r4 r
  g, r r
  g r r
  c r r
  c r r
  f, r r
  \break
  a r r
  d r r
  d r r
  e, r r
  e r r
  a r e
  \break
  a r r
  f2.\f
  f2.\f
  e4 r r
  e r r
  d'4 r r
  d r r
  \break
  c r r
  c r r
  d^\markup{\teeny "(vc. pizz)"} r r
  b r r
  e, r r
  e r r
  e r r
  \break
  e r r
  a r e^\markup{\teeny "(vc. arco)"}
  a r r
  f\f r r
  f\f r r
  e2.
  e4 r r
  \break
  d' r r
  d r r
  c r r
  c r r
  d r r
  d r r
  \break
  e, r r
  e r r
  e r r
  e r r
  a r e a r
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup {
  \teeny
  \date
}