\version "2.19.55"
\language "nederlands"

\header {
  title = "Curtain Tune"
  instrument = "Contrabas"
  composer = "Henry Purcell"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

scoreAGlobal = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
 % \tempo 4=100
}

scoreAContrabass = \relative c {
  \scoreAGlobal
  \mark \markup {\italic \small "Vader zit in het verzet..."}
  % Music follows here.
  \bar ".|:"
    \repeat volta 5 {
      g4 r d'
      ees4 r e!
      fis4 r a
      bes d d,\mark \markup{\huge \bold "5x"}
      %g,4 r d' | % 05
    }
    \bar ":|."
    g,4
    \bar "|."

}

\score {
  \new Staff \with {
  %  instrumentName = "Contrabass"
   % shortInstrumentName = "Cb."
  } { \clef bass \scoreAContrabass }
  \header {
    piece = \markup{ \huge \bold \circle "6"}
  }
  \layout { }
}

\markup \column {
  \line {\tiny \italic{ zo raar, ik heb hem niet gekend, maar toch is hij altijd zo aanwezig}}
  \line {\tiny \italic {geweest in mijn leven..juist daarom: de aanwezige afwezige vader….}}
  \line {\tiny \italic {ik heb het gevoel dat ik \underline {altijd de dochter-van}  geweest ben..}}
  \line {\tiny {orkest: \box "de dochter van, de dochter van"}}
  \line {\tiny \italic {Ze moest eens weten….ik ga haar niet vertellen dat ik een kind van een NSBer ben}}
\line {\tiny \italic {ik ga haar niet vertellen dat ik mijn leven lang het spuug van me af moet vegen..dat ik altijd moet uitkijken}}
\line {\tiny \italic {waarom vertel je het haar niet?}}
  \line {\tiny {(lange stilte)}}
  \line {\tiny {\italic{ ..dat hebben wij \underline {nooit aan iemand verteld}} \box \bold "..."}}
%  \hspace #1
}

scoreBGlobal = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
%  \tempo 4=100
}

scoreBContrabass = \relative c {
  \scoreBGlobal
  % Music follows here
  \bar ".|:"
    \repeat volta 5 {

      g4 r r
      ees'4 r r
      fis4 r r
      bes r r\mark \markup{\bold "4x"}
      %g,4 r d' | % 05
    }
    \bar ":|."
    g,4
    \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
 %   shortInstrumentName = "Cb."
  } { \clef bass \scoreBContrabass }
  \header {
    piece = \markup{ \huge \bold \circle "7"}
  }
  \layout { }
}

\markup \column {
%  \line {\small {twee keer spelen}}
%  \hspace #1
%  \line {\small {1)    eerste keer baslijn (5x), pizzicato, zacht, stop op cue}}
%  \hspace #1
%  \line {\small {\box "orkest: de dochter van de dochter van"}}
%  \line {\small {\box "lange stilte"}}
%  \line {\small {2) \italic{ ..dat hebben wij nooit aan iemand verteld} \box{"Curtain Tune alweer"}}}
				%  \line {\small {tweede keer alleen eerste note elke maat}}
  \line {\tiny {\italic "kijk, daar gaat de bus naar Westerbork"}}
  \line {\tiny {\italic "Kent u Westerbork? Ik ga er nooit heen. zon akelige plek..."}}
  \line {\tiny \box \bold "8. Prayer"}
  \hspace #1
}
\markup {
  \teeny
  \date
}