\version "2.18.2"

\header {
  dedication = ""
  title = "Schindler's list"
 % subtitle = "Easy Strings"
  instrument = ""
  composer = "John Williams"
  arranger = "Rachel Yates"
  poet = \markup{\bold \circle \huge "8"}
  copyright = "© 2015"
  tagline = ""
}
\layout {
  indent = 0.0\cm
}

%#(set-global-staff-size 26)
global =
{  \compressFullBarRests
   \numericTimeSignature}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\score {

  \relative c {
      \global
    \clef bass
    \time 3/4
    \key f \major
    \tempo "Ballad"
                                % music here
           \override MultiMeasureRest #'expand-limit = 1
      R1*3/4*3
      \time 4/4
      a1 | % 4
      a1 | % 5
      R1*2
      r2 g2 | % 8
      r2 f2 | % 9
      e2 a2 | % 10
      d1 | % 11
      g,2 a2 | % 12
      bes2 a2 | % 13
      g2 a2 | % 14
      d1 | % 15
      d4 d2 d4 | % 16
      g,2 f4 e | % 17
      d'2 g,2 | % 18
      g4 c f,2 | % 19
      e2 a | %  20
      d1 | % 21
      e,2 a2 | %22
      bes2 a2 | %23
      g2 a2 | %24
      d1\> | %25
      r1\! | %26
      r2 d2 | %27
      r1 | %28
      g,2. e4 | %29
      a2 a | % 30
      a2 a | % 31
      a4 d d2 | % 32
      b!4\<^\markup{\bold \tiny "rallentando"} b b b\! | % 33
      \bar "||" \key c \major
      a2 a4 a | % 34
      d4 g, c e, | % 35
      a2 d | % 36
      g,2 c | % 37
      b2 e, | % 38
      a1 | % 29
      b4^\markup{"pizz"} r e, r  | % 40
      f r e r
      d'2^\markup{"arco"} e,
      a1
      r1\pp
      <d a>1^\markup{\tiny "arpeggiate"}\<
      r1\!
      r1
        \override Hairpin.circled-tip = ##t
      a1\> (
      a2.) r4\!

    \bar "|."

  }
}

\markup \column {
  \line {\bold "daarna..."}
  \hspace #1
  \line { \italic{Waarom zei U dat Westerbork zon akelige plek is?} \box{"lage e - contrabassen"}}
  \hspace #1
  \line { \italic{Dat is beter voor ze...} \box{"Lage E,"} vervolgens een losse G vanuit de cellos...}
  \hspace #1
  \line{ \italic{levenslang de dochter-van te zijn...} \box{"prayer"}}
  \hspace #1
}


\markup {
  \teeny
  \date
}