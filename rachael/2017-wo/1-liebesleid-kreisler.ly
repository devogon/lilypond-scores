\version "2.19.54"
\language "nederlands"

\header {
  title = "Liebesleid"
  subtitle = "(intro)"
  instrument = \markup { \center-column { \line {"Contrabas"} \line {"Violoncello II"} } }
  composer = "F. Kreisler"
  poet = \markup{\huge \bold \circle "1"}
  arranger = "R. Yates"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
  \tempo "Tempo di Ländler con sentimento"
}

contrabass = \relative c {
  \global
  % Music follows here.
  a4\mf\>^\markup{"pizz."} r r
  a r r
  a r r
  a r r\!
  a r r
  c4 r r
  \break
  d4 r r
  d4 r r
  b r r
  g r r
  c r r
  c r r
  \break
  a r r
  f r r
  d' r r
  d r r
  e, r r
  e r r
  \break
  a r e
  a r r
  a r r
  c4 r r
  d4 r r
  d4 r r
  \break
  b r r
  g r r
  c r r
  c r r
  a r r
  f r r
  \break
  d' r r
  d r r
  e, r r
  e r r
  r r e
  a r r
  \break
  \repeat volta 2 {
    a r r
    c r r
    d r r
    d r r
    b r r
    g r r
    \break
    c r r
    c r r
    a r r
    f r r
    d' r r
    \break
    d r r
    e, r r
    e r r
    a r e
    a r r
    \bar ":|."
  }
%  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup \column {
%  \line {\small {Herma: niemand mocht het weten:}}
  \line {\small {Ilse: \italic { de buren niet,} }}
  \line {\small {Herma: \italic { op school niet,}}}
  \line {\small {Boukje: \italic { wat je ouders in de oorlog deden niet}}}
  \hspace #1
  \line {\small {Allen: \italic { niet, niet...niet.} Orkest herhaalt ook: \box{"Niet, niet niet.."} begin \bold soft, get \bold louder}}
  \line {\small {Herma: \italic { Niemand heeft er iets mee te maken. Naar orkest \underline{"hou je mond."}}}}
  \hspace #1
  \line {\small {Ria: \italic {  Soldaat van Oranje is een verhaal dat al langer duurt dan de hele oorlog.}}}
\line {\small { Ilse: \italic { Anne Frank het meest gelezen boek... Oorlogswinter, Oeroeg...verhalen verhalen verhalen}}}
  \line {\small {Boukje \italic { Wat is een verhaal.? Waar over te vertellen? Er is geen beginnen aan..}}}
  \line {\small \box \bold "2. Liebesleid"}
  \hspace #1
}


\markup {
  \teeny
  \date
}