\version "2.19.54"
\language "nederlands"

\header {
  title = "Liebesfreud"
  instrument = \markup{\small "Violoncello II / Contrabas"} 
  composer = "F. Kreisler"
  arranger = "arr. by R. Yates"
  poet = \markup{\huge \bold \circle "4"}
  piece = \markup \column {
    \line{\small \italic "M.: Ik was 18 en wachtte op de liefde, zoals iedereen van 18"}
    \line{.}
  }
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key a \major
  \numericTimeSignature
  \time 3/4
  \tempo "Ballad" 4=69
}

contrabass = \relative c {
  \global
  % Music follows here.
  \partial 4 r
  \set Score.currentBarNumber = #69
  a^\markup{\italic pizz.} r r
  e r r
  b' r r
  e, r r
  b' r r
  e, r r
  \break
  a r r
  e r r
  a r r
  e r r
  d' r r
  b r r
  e, r r
  \break
  e r r
  a r r
  e r r
  a^\markup{\teeny "(vc. arco)"} r r
  e r r
  b' r r
  e, r r
  \break
  b' r r
  e, r r
  a r r
  e r r
  a r r
  e r r
  \break
  d' r r
  b r r
  e, r r
  e r r
  a^\markup{\italic "rit."} r r
  a r r
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}


piep = \relative c {
%  \global
  g''^\markup{\tiny \italic screechy}
}


\markup \column {
  \line {\tiny {\italic{...kinderwagen} }}%\box{"de screechy geluid."} \teeny {Achter de kam, piep geluiden}}}
}

%\tiny {tot aan \italic "een baby moet ook frisse lucht"}

\score {
  \new Staff { \clef tenor \piep }
  \layout { }
  \header {
    piece = " "
  }
}


\markup \column {
%  \line {\tiny \bold freeze }
%  \hspace #2
  \line { \tiny {\italic{Niet zo leuk???}}}% \teeny "- at kam, across d and g squealing / damp strings near end of fingerboard"} }
}

eng = \relative c {
  <g'' d>^\markup{\tiny \italic "lelijk" \teeny "- at kam, damp strings near end of fingerboard"}
}

\score {
  \new Staff { \clef tenor \eng }
  \layout { }
  \header {
    piece = " "
  }
}


\markup \column {
  \line {\tiny \bold freeze }
%  \hspace #2
%  \line { \tiny {\italic{Niet zo leuk???} \box{"hard lelijk geluid"} \teeny "- at kam, across d and g squealing / damp strings near end of fingerboard"} }
%  \hspace #2
  \line { \tiny {tweede keer \italic {niet zo leuk??}}}% \box{"weer het geluid"} }}
}

engweer = \relative c {
  <g'' d>^\markup{\tiny \italic "lelijk"}
}
\score {
  \new Staff { \clef tenor \engweer }
  \layout { }
  \header {
    piece = " "
  }
}

\markup \column {
  \line {\tiny {meteen daarna \box "zakken"}}
  \line {\tiny { \italic{hij wordt lid van de NSB   } \box {"recht staan... "}}} % \box{"kloppen op de achterkant - use heel of hand"}.... }}
%  \line {\tiny { ...... zacht maar toenemend - in 4/4 time - tot aan \italic{cue van Rachael}}}%een schim uit de hel} }}
  }
taptap = \relative c {
  \tempo 4=38
  \xNotesOn
  d^\markup{\tiny { ...... zacht maar toenemend - in 4/4 time}}
  \repeat unfold 11 {d}
  \xNotesOff
}
\score {
  \new Staff { \clef bass \taptap }
  \layout { }
  \header {
    piece = " "
  }
}
\markup \column {
%  \line {\tiny { \italic{hij wordt lid van de NSB} \underline {"recht staan... "}\box{"kloppen op de achterkant - use heel of hand"}.... }}
%  \line {\tiny { ...... zacht maar toenemend - in 4/4 time - tot aan \italic{cue van Rachael}}}%een schim uit de hel} }}
%  \hspace #1
  \line {\tiny \italic {Twee jaar later komt  hij weer  thuis…als een schim …uitgehongerd en kapot…hij was gedeserteerd..}}
  \line {\tiny \italic {Dat beeld raak ik nooit meer kwijt...hoe hij daar stord....\underline {een schim uit de hel}}}
  \line {\bold \tiny {quick transition to next piece}}
  \line {\small \bold \box "5. Shlof main kind"}
  \hspace #1
}

\markup {
  \teeny
  \date
}