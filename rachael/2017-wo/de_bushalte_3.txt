﻿DE BUSHALTE

Muziek:  Liebesleid 
3x de eerste 20 maten herhalen. (plokjes)

Ria
Er is veel kracht voor nodig om te herinneren
Altijd weer die pijn te voelen
die waanzinnig kan maken

Boukje
Er is veel kracht voor nodig om te vergeten
De pijn te ontkennen 
Je kunt er gek van worden

Ilse
De wereld is opgedeeld in krankzinnigen die zich herinneren en krankzinnigen die vergeten.

Herma
Zonder persoonlijke verhalen over de oorlog raken we de geschiedenis kwijt, zeggen ze …

Ria 
Na de oorlog waren er geen verhalen, iedereen zweeg
Boukje
Je moest niet laten merken wie je was, 
Herma
niemand mocht het weten: 
Ilse
de buren niet, 
Herma
op school niet,
Boukje
wat je ouders in de oorlog deden niet…
Allen
niet, niet…niet.  Orkest herhaalt ook: “Niet, niet niet…..”
Herma
Niemand heeft er iets mee te maken. Naar orkest: Houd je mond.

Ilse
Onderduiken: 
Ria
dat  werd in de oorlog, maar ook na de oorlog geleerd, 
Herma
alles binnenskamers houden, 
Boukje
niet willen weten, 
Ria
geen emoties…
Ilse
Alsjeblieft geen emoties.


Herma
Hoe zou je kunnen vertellen…het is niet geleerd, er waren geheimen…

Boukje 
Anderen zijn met onze verhalen aan de loop gegaan, hebben ze uitvergroot, hun conclusies getrokken, hun keuzes gemaakt om een patroon te laten zien…van zwart en wit naar grijs en fout en goed.

Ria
Soldaat van Oranje is een verhaal dat al langer duurt dan de hele oorlog. 
Ilse
Anne Frank  het meest gelezen boek…Oorlogswinter, Oeroeg…verhalen verhalen verhalen

Boukje
Wat is een  verhaal….? Waar over te vertellen?  Er is geen beginnen aan…..

Muziek: Liebesleid..

4 oudere dames: Mannie, dochter van NSB burgemeester van Roden, hand in verband
		  Berthy, Joodse overlevende
		  Alter ego van Mannie,  links genoteerd
		 Alter ego van Berthy, rechts genoteerd
Bankje en eventueel een bushalte bordje 

Nieuw: op het bankje zitten Berthy en Mannie. Ilse  (A Berthy) staat achter Berthy, Herma (A Mannie) achter Mannie.

Berthy		Sommige dingen kunnen niet gezegd worden.
		Alleen maar gedacht.

A Berthy	Dat ben ik. Ik ben alles wat alleen maar gedacht kan worden.

A Mannie	Ik ook. Ik ben alleen maar gedachten.

Mannie		Precies. En dat houden we zo. 

			(Mannie wendt zich abrupt naar Berthy) 
			M …  hallo, stopt hier de bus naar Roden?
			B. …  ja hoor, dat klopt…....
			M…..fijn, dank u wel…..gaat u ook naar Roden?
			B …   ik ga op bezoek bij oude vrienden, dat doe ik elk jaar in mei..
							Ga je haar vertellen dat je elk jaar bloemen 							neerlegt bij het oorlogsmonument…voor							vader?
			M….ik heb vroeger een paar jaar in Roden gewoond.
			B…  O ja? Dat is toevallig…ik ook, ik ben er zelfs geboren…
							
							..midden in de oorlog, in 1943…..

Muziek: Schlaf mein Kind. 1x.
op de zolder van het onderduikadres…de dokter blijft slapen, zodat het niet zou opvallen…en niemand zou merken dat er een baby’tje  kwam  …ze zijn zo blij met mij..
							 een jaar eerder is  een  broertje 								geboren..mamma had hevige weeën, 							 	maar er was met moeite een ziekenhuis te 							vinden waar ze kon bevallen omdat ze Joods 							is…na 3 dagen is mijn broertje 	overleden							…Mijn broertje Levi….Muziek stop.

			M. ..ik fietste elke dag van Roden naar Groningen en ik…
			B…  zat u daar op school?
			M.   Ik was 18 en wachtte op de liefde, zoals iedereen van 18

			Muziek: Liebesfreud. Sander danst met A Mannie (= Herma)

			..     als ik er aan denk zie ik mezelf weer door de bossen van
			       Roden fietsen…mooie herinnering…
			B. …mijn moeder vertelde mij dat ook ….dat de bossen bij Roden
			        zo mooi waren…daar wandelden mijn ouders met mij in de kinderwagen
	
							ik ben  een baby..

							Muziek: onheilspellend…..

we wonen als muizen op zolder…’s nachts als het donker is en  niemand op straat is  gaan we naar uiten…vader wordt gek van dat binnen 	zitten, …niks voor hem. Overdag zit hij te spinnen om maar iets om 	handen te hebben, maar ’s nachts houdt hij het niet meer uit, dan moet hij naar buiten  …een baby moet toch ook frisse lucht hebben…

Muziek stop.

			M…Uw ouders wonen daar niet meer?
			B…( schudt nee)
							na 2 jaar worden we verraden. Eerst wordt							mijn vader gearresteerd en een paar dagen 							later pakken ze mijn moeder en mij op…

			B. Nee, we zijn verhuisd naar Apeldoorn
			     Mijn moeder vond het niet zo leuk in Roden

							Niet zo leuk??? 

							Muziek: Akkoord.


Het is er verschrikkelijk. Op een nacht worden we wakker .geweldig lawaai, iemand heeft een steen door de ruit 	gegooid. Ik begin te krijsen en mamma ook..buiten staat een man die zegt : kom naar buiten..ik heb nieuws over je man…mamma gilt zo hard dat de buren wakker worden en de man wegvlucht…niet zo leuk?

Muziek: Akkoord.
																						…het is alsof ze het 							ons kwalijk nemen dat we niet in het kamp 							gebleven zijn…er is nog zoveel haat 							               tegen de Joden…het voelt alsof we steeds							opnieuw verraden worden….
			
			B. .   .en U, woont uw familie daar nog?
			M.….Nee, wij hebben er ook maar een paar jaar gewoond. 
			         Mijn vader kreeg een baan  in Roden..

Vader heeft een goedlopende kruidenierszaak in Den Haag
Goeie zakenman..maar zijn zaak gaat kapot door de crisis. Vader 
ergert zich  eraan dat de hardwerkende  middenstand de dupe is , 
hij wordt lid van de NSB.

Muziek: In 4/4 op de kast van het instrument kloppen.

De Partij biedt hem  een baan in Roden aan. Vader heeft niks met
uniformen, we hoeven ook niet naar de Jeugdstorm…maar vader
praat  altijd over politiek, thuis…we worden er gek van..
Hij gelooft echt dat Duitsland de oorlog zal winnen…
Hij gelooft  in de Partij, in Mussert
Mijn broer is 16 jaar: een puber..hij wil bij de 
SS vechten…mijn vader is er faliekant op tegen, hij verscheurt de
aanmeldingspapieren ..maar hij gaat toch..
Twee jaar later komt  hij weer  thuis…als een schim …uitgehongerd en
kapot…hij was gedeserteerd..
Dat beeld raak ik nooit meer kwijt…hoe hij daar stond…een schim uit de hel.

Muziek: Schlaf mein Kind 2x.


			B. …mijn vader was sportinstructeur…hij reed op een 
			    motor..dat was heel bijzonder in die tijd.. mijn moeder
			    zei dat hij heel sportief was en sterk…
				
							Vader zit  in het verzet…
				
							Muziek: begin Schlaf(1e 20 maten)
							

							Sabotage..hij brengt pamfletten rond…dat 							soort dingen…als   moeder een 									Jodenster moet gaan  dragen spijkert hij als 
							protest een Jodenster tegen de 								buitendeur..hij brengt Joden naar hun 								onderduikadres, hij verliest daardoor zijn 							baan.. het  wordt te gevaarlijk…ze moeten 							zelf onderduiken….

							Muziek: stop.
				  
			M. Leeft uw vader niet meer? 			( tot B. )
							Niet huilen, haal maar even diep adem….wil 							je dat ik wegga?
			B. (schudt nee)

			B. Nee helaas niet…

			…..zo raar, ik heb hem niet gekend, maar toch is hij altijd zo aanwezig 				      geweest in mijn leven..juist daarom: de aanwezige afwezige vader….ik heb 			      het gevoel dat ik altijd de   dochter-van  geweest ben..

			Orkest; Praat de actrices na, door elkaar heen.
“De dochter van….de dochter van…..de dochter, de dochter van..
  
			J. Soms gebeurt dat, hè…dat je levenslang  het –kind-van - bent…

Vertel mij wat..ik heb het er maar druk mee

			B. O ja, heeft u dat ook?

			M. zo zou je dat kunnen noemen


Ze moest eens weten….ik ga haar niet
vertellen dat ik een kind van een NSBer ben
ik ga haar niet vertellen dat ik mijn leven lang het spuug
van me af moet vegen..dat ik altijd moet uitkijken

								waarom vertel je het haar niet?
 ( lange stilte)

…dat hebben wij nog nooit aan iemand verteld

Muziek: Iets dreigends/onheilspellends. (niet zo lang, een minuutje).

( tot Mannie)   kom op..sterk blijven..niet breken)			 
			M. ( gebaart: ga weg, ga weg..)

			B.      Gek eigenlijk, hè…zitten we hier op de bus naar Roden te wachten
			    ….. praten we ineens over onze vaders…

			…      .kijk, daar gaat de bus naar Westerbork…
			 …      kent u Westerbork?.....Ik ga er nooit heen,
			          het is zo’n akelige plek….

Muziek: Schindler’s List.

Westerbork…
 ….een verschrikkelijke plek…we zijn daar na de oorlog…
voor straf  ..ze treiteren ons..er zijn verkrachtingen
er is daar niks…geen wcpapier, 
geen maandverband, geen eten…niks
de bewakers  lachen ons uit
en schreeuwen tegen ons. Ik snap dat wel,
ze hebben ook veel meegemaakt….
Maar deze haat…?

…..zou zij ook in het  kamp geweest zijn?

			M. Waarom zei u dat? Dat Westerbork zo’n akelige plek is ?..

							Muziek: Lage bastoon.

							Mag ik nou eindelijk eens weg..vertel het 							haar maar…praat het van je af, misschien 							krijg ik dan weer rust…je wordt van mij alleen 							maar verdrietig  ( troostend naar B)
						
							( naar alter ego Manny)…kom , laten we gaan,
							Laten we ze met rust laten…ze komen er 							samen wel uit..
							Dat is beter voor ze…..

							Muziek: Lage bas stop.
							+ stukje Liebesleid.
					  

			B. Mijn moeder en ik hebben in de oorlog in kamp Westerbork gezeten
			    gelukkig waren de laatste transporten toen al weg..we hebben het 				    overleefd….maar we konden niet meteen terug naar Roden, er moest eerst 
			    nog woonruimte gezocht worden…woonde u daar toen  ook , in die tijd??

( paniek) Actrice stopt het orkest. 
O, mijn god…dit gaat verkeerd…Orkest: Praat haar na:”Verkeerd. Dit gaat verkeerd”
Ze is Joods, ze mag het niet weten…Orkest: Praat haar na:”Ze is joods. Ze is joods….ze is joods.”
Ik moet mijn mond houden…laat haar maar
praten…vragen stellen…ik moet vragen stellen..
	
			M .  Wat is er met uw vader gebeurd?

			B. Mijn vader zat in het concentratiekamp Neuengamme..toen 
			     de oorlog bijna afgelopen was hebben de Duitsers de gevangenen die nog 			     leefden  naar Lubeck laten lopen….. daar werden ze in een schip geladen.. 
			     De Engelsen hebben per ongeluk het schip gebombardeerd…Mijn vader 			     was nog steeds sterk en kreeg het voor elkaar om overboord te springen    			     en naar de kust te zwemmen. Daar is hij  alsnog door een Duitse soldaat  			     doodgeschoten….Wij hoorden dat pas jaren later….mijn moeder heeft 			     haar  leven lang om hem gehuild…… ( alter ego troost haar)
			 
			M. Nou begrijp ik waarom u zei…levenslang de dochter-van te zijn….

Muziek: Prayer.

Bedenk iets…laat haar doorpraten….

 			B. Maar hoe zat het dan met uw vader? U zei toch dat u dat gevoel ook kende
                  		    Levenslang  kind-zijn-van?

			Orkest: Praat weer na:” het kind zijn van….het kind van….

			M……………( hevig in de weer met haar pijnlijke hand…)

Ga ik het haar vertellen?
								Ja, dat lijkt me nu eindelijk wel eens 								een goed idee..waarom niet?

Muziek: Nocturne (of iets anders wat verlangen uitdrukt)

Ze lijkt zo aardig….
Gewoon zeggen hoe erg het is om levenslang veroordeeld
te zijn voor iets wat je niet gedaan hebt
Je buitengesloten  te voelen, …een levenlang zwijgen over
je afkomst…niet opvallen, geen moeilijke discussies,
Onschuldig in een soort gevangenis te zitten
Ik zou het zo graag gewoon willen zeggen:..
Ik? Ik ben een dochter van een NSBer…
      ik heb geen Joden vergast 
      en ik hou van mijn ouders…

			B.  Wat is er met uw hand?
			      Heeft u daar pijn?
			M. het is een hardnekkige splinter 
			      ik krijg em d’r met geen mogelijkheid uit..
			B.  Oei…is hij gaan ontsteken?
			M. Ja, dat gebeurt af en toe….
			B.  Zit hij er al zo lang?...maar daar moet u mee naar de dokter…
			     Hij moet eruit gesneden worden…anders blijft u daar last van houden…
			     Dat snijden doet wel pijn, maar dan gaat het echt beter…gelooft u me 			     maar…


			M. Kijk, daar komt de bus…

			B. Laat mij uw koffer dragen….hij is veel te zwaar voor u……

Muziek: Liebesleid.

…………………………………………muziek…………………………………………………..
    


