\version "2.18.2"

\header {
  dedication = ""
  title = "Curtain Tune"
  instrument = "Contrabas"
  composer = "H. Purcell"
  copyright = "© JPv Coolwijk"
  poet = \markup{ \huge \bold \circle "6" \circle \huge \bold "7"}
  piece = \markup \column {
    \line{\small \italic "Vader zit in het verzet..."}
    \line{.}
  }
  tagline = ""
}
\layout {
  indent = 0.0\cm
}

%#(set-global-staff-size 26)
date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}



\score {
  \relative c {
    \clef bass
    \time 3/4
    \key bes \major
    g4^\markup{\italic pizz.} r d' | % 01
    \repeat volta 2 {
      ees4 r e! | % 02
      fis4 r a | % 03
      bes d d, | % 04
      g,4 r d' | % 05
      ees4 r e! | % 06
      fis4 r a | % 07
      bes4 d d,  | % 08
      g,4 r d' | % 09
      ees4 r e! | % 10
      fis4 r a | % 11
      bes4^\markup{\smaller "12"} d d,  | % 12

      g,4 r d' | % 13
      ees4 r e! | % 14
      fis4 r a | % 15
      bes4 d d,  | % 16

      g,4^\markup{\box "2e, stop"} \bar "|.|" r d' | % 17
      ees4 r e! | % 18
      fis4 r a | % 19
      bes4 d d,  | % 20

      g,4 r d' | % 21
      ees4 r e! | % 22
      fis4 r a | % 23
      bes4 d d,  | % 24
      g,4 r d' | % 25
      ees4 r e! | % 26
      fis4 r a | % 27
      bes4 d d,  | % 28
            g,4 r d' | % 29
      ees4 r e! | % 30
      fis4 r a | % 31
      bes4 d d,  | % 32
            g,4 r d' | % 33
      ees4 r e! | % 34
      fis4 r a | % 35
      bes4 d d,  | % 36
            g,4 r d' | % 37
      ees4 r e! | % 38
      fis4 r a | % 39
      bes4 d d,  | % 40



      g,4 r d' | % 41
      ees4 r e! | % 42
      fis4 r a | % 43
      bes4 d d,  | % 44
      g,4 r d' | % 45
      ees4 r e! | % 46
      fis4 r a | % 47
      bes4 d d,  | % 48

      g,4 r d' | % 49
      ees4 r e! | % 50
      fis4 r a | % 51
      bes4 d d,  | % 52

      g,4 r d' | % 53
      ees4 r e! | % 54
      fis4 r a | % 55
      bes4 d d,  | % 56

      g,4 r d' | % 57
      ees4 r e! | % 58
      fis4 r a | % 59
      bes4 d d,  | % 60
      g,4 r d' | % 61
      ees4 r e! | % 62
      fis4 r a | % 63
      bes4 d d,  | % 64
            g,4 r d' | % 65
      ees4 r e! | % 66
      fis4 r a | % 67
      bes4 d d,  | % 68
            g,4 r d' | % 69
      ees4 r e! | % 70
      fis4 r a | % 71
      bes4 d d,  | % 72
            g,4 r d' | % 73
      ees4 r e! | % 74
      fis4 r a | % 75
      bes4 d d,  | % 76

                  g,4 r d' | % 77
      ees4 r e! | % 78
      fis4 r a | % 79
      bes4 d d,  | % 80
    } \alternative {
      {g,4 r d'}
      {g,4 r r}
    }
    \bar "|."
    }
  }
%}
\score {
  \relative c {
  \clef bass
%      \set countPercentRepeats = ##t
    \key bes \major

    \time 3/4
  \repeat volta 5 {
    \mark \markup{\bold "5x"}
      g4 r d'
      ees4 r e! 
      fis4 r a 
      bes d d, 
      %g,4 r d' | % 05
    }
%    g,4
    \bar "|."
  }
}

\markup \column {
  \line {\tiny {twee keer spelen}}
  \hspace #1
  \line {\tiny {1)    eerste keer baslijn (5x), pizzicato, zacht, stop op cue}}
  \hspace #1
  \line {\tiny {\box "orkest: de dochter van de dochter van"}}
  \line {\tiny {\box "lange stilte"}}
  \line {\tiny {2) \italic{ ..dat hebben wij nooit aan iemand verteld} \box{"Curtain Tune alweer"}}}
  \line {\tiny {tweede keer alleen eerste note elke maat}}
  \line {\tiny {Kent u Westerbork? Ik ga er nooit heen. zon akelige plek... \box {"Prayer"}}}
  \hspace #1
}
\markup {
  \teeny
  \date
}