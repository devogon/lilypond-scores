\version "2.16.2"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
 % dedication = "http://bassoridiculoso.blogspot.com"
  title = "Autumn Leaves"
  subsubtitle = "from Cannonball Addeerly’s “Something Else” "
  instrument = "Bass"
  composer = "Sam Jones"
 % meter = "Allegroorsomethnig"
 copyright = "(Blue Note BP 1595)"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

global = {
  \key bes \major
  \time 4/4
  \tempo "tempo" 4=115
}

bass = \relative c, {
  \global
  \grace s16 % hack to move the time "C" forward of the first repeat bar
  % music follows here
  \repeat volta 24 {
    \bar "|:"
     g4 bes8 d8 e4 d8 r |
         \mark \markup{\teeny #"x24"}
  }
  g,4 r4 r2 |
  c'2^\markup {\teeny "Cmin7"} c |
  f,2^\markup {\teeny "F7"} c'4 f, |
  bes?2^\markup {\teeny "Bbmaj7"} f4. d8 |
  \break
  ees2^\markup {\teeny "Ebmaj7"} bes'4 ees,4 | % 6
  a,2^\markup {\teeny "Amin7b5"} a |
  d2^\markup {\teeny "D7"} d4 fis,4 |
  g2^\markup {\teeny "Gmin7"} d' |
  \break
  g4.^\markup {\teeny "Gmin7"} d8 bes4 d | % 10
  \bar "||"
  c2^\markup {\teeny "Cmin7"} c |
  f,2^\markup {\teeny "F7"} f4 a |
  bes2^\markup {\teeny "Bbmaj7"} bes4 d |
  \break
  ees2^\markup {\teeny "Ebmaj7"} bes'4 ees, | % 14
  a,2^\markup {\teeny "Amin7b5"} a |
  d2^\markup {\teeny "D7"} d4 fis,4 |
  g2^\markup {\teeny "Gmin7"} d'|
  g4.^\markup {\teeny "Gmin7"} d8 \times 2/3 {g r d} ees!4 |
  \break
  d2^\markup {\teeny "Amin7b5"} d4 cis' | % 19
  d4^\markup {\teeny "D7b9"} \times 2/3 {r8 d a} d,4 (\times 2/3 {d) fis!8} |
  g2^\markup {\teeny "Gmin7"} d |
  g,2^\markup {\teeny "Gmin7"} \times 2/3 {g'4 d8 } cis4 |
  c2^\markup {\teeny "Cmin7"} c'4 e,! |
  \break
  f2^\markup {\teeny "F7"} f4 a | % 24
  bes2^\markup {\teeny "Bbmaj7"} f4 (\times 2/3 {f) a,8} |
%  bes2^\markup {\teeny "Ebmaj7"} \times 2/3 {g'4 d8} cis4 |
  bes2^\markup {\teeny "Ebmaj7"} f'4 bes, |
  a2^\markup{\teeny {"Amin7" \flat"5"}} a |
  d2 d4 fis |
  \break
  g2 g4. g,8 | % 29
  bes4 d g, bes |
  a2 a |
  d2 d4 d |
  g2 d4 (\times 2/3 {d) a8} |
  \break % end of pg 1
  g2 d'4 b'! | % 34
  c2 c4 g |
  f4 c bes f' |
  bes,4 d g f |
  es4_\markup{R} bes'_\markup{5} g_\markup{3} es_\markup{R} |
  \break
  a,4 b! c cis | % 39
  d4 es f fis |
  g d bes d |
  g, bes d bes |
  c e! g ges |
  \break
  f4 a c b! | % 44
  bes b! c \times 2/3 {b d,8} |
  es4_\markup{R} bes'_\markup{5} g_\markup{3} es_\markup{R} |
  a,4 b! c cis |
  d4_\markup{R} es_\markup{b9} f_\markup{b3} fis_\markup{\sharp 3} |
  \break
  g4 d bes d | % 49
  g,4 bes d g |
  d e! f a |
  cis d a d, |
  g d bes d |
  \break
  g,4 bes d des | % 54
  c4 c' g ges |
  f c b! b' |
  bes g f a, |
  bes f' d bes |
  \break
  a4 b! c cis | % 59
  d e! f fis |
  g d bes d |
  g, ges g! as |
  a! b! c cis |
  \break
  d4 d as' as | % 64
  g8. a,16 bes8 r d4 g, |
  bes' d, g bes, |
  c e! g ges |
  f f c' b! |
  \break
  bes4 f bes, d | % 69
  es_\markup{R} bes'_\markup{5} g_\markup{3} es_\markup{R} |
  a, b! c cis |
  d e! f fis |
  g d bes d8 es, |
  \break % end page 2
  % begin page 3/7
  g4 ges g! bes | % 74
  c4 g ges e! |
  f4 g as a! |
  bes d f <e! e,>  |
  es4_\markup{R} bes'_\markup{5} g_\markup{3} es_\markup{R} |
  \break
  a, b! c a | % 79
  d c bes a
  g bes e! d
  a' g d a
  d e! fis a
  \break
  a,4 d e! fis | % 84
  g g, bes d
  g f es d
  c8. a16 bes4 a g
  f g as a!
  \break
  bes4 d f a | % 89
  bes f bes, f'
  a, bes c cis
  d es f fis
  g_\markup{R} d_\markup{5} bes_\markup{3} d_\markup{5}
  \break
  g,4 bes d g | % 94
  a, b! c a
  e'! es d as'
  g_\markup{R} d_\markup{5} bes_\markup{3} d_\markup{5}
  g,4_\markup{R} bes d b'!
  \break
  c4^\markup{\teeny{Trumpet Chorus 1-4:21}} cis d g, | % 99
  c, e! f a,
  bes8. bes16 d4 g f
  es_\markup{R} bes'_\markup{5} g_\markup{3} es8_\markup{R} a, ~
  a8 a bes4 c cis |
  \break
  d4 es f fis | % 104
  g d bes d8 e,!
  g4 ges g! b!
  c g ges e!
  f g as a!
  \break
  bes4 d f e!! | % 109
  es_\markup{R} bes'_\markup{5} g_\markup{3} es_\markup{5}
  a,4 b! c cis
  d es f fis
  g_\markup{R} d_\markup{5} bes_\markup{3} d_\markup{5}
  \break % end page 3/7 - correct
}

\score {
  \new Staff \with {
%    instrumentName = "Bass"
  } { \clef "bass_8" \bass }
  \layout { }
}
