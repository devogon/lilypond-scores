\version "2.19.81"
\language "nederlands"

% \include "openlilylib"
% \useLibrary Stylesheets
% \useNotationFont lilyjazz

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Afternoon in Paris"
  instrument = "Bass"
  composer = "John Lewis"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
 %   #(define fonts
%     (set-global-fonts
%       #:music "lilyjazz"
%       #:brace "lilyjazz"
%       #:factor (/ staff-height pt 20)
%   ))
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \partial 4
  \tempo 4=108
}

contrabass = \relative c {
  \global
  % Music follows here.
  e8 g
  \repeat volta 2 {
    r8 d4^\markup{\italic "C maj7"} c8 b c d e |
    es8^\markup{\italic "C-7"} g, bes d c4^\markup{\italic "F7"} d8 f |
    r8 c4^\markup{\italic {B \flat maj7}} bes8 a bes c d |
    des8^\markup{\italic {B \flat-7}} f, as c bes4^\markup{\italic {E\flat7}} c8 es |
    \break
    r8^\markup{\italic {A\flat maj7}} c4 as8 g bes as g ~ |
    g2^\markup{\italic {D-7}} ~ g8^\markup{\italic {G7\flat 9}} bes as g ~ |
    g1^\markup{\italic {C maj7}} |
  }
  \alternative {
    {r2^\markup{\italic {D-7}} r4^\markup{\italic {G-7}} e'8 g}
    {r2^\markup{\italic {C maj7}} a,4 bes8 c ~ }
  }
  \break
  c2.^\markup{\italic {D-7}} d8 e ~ |
  e2.^\markup{\italic {G7}} d8 c ~ |
  c2.^\markup{\italic {C maj7/E}} e8 a, ~ |
  a2.^\markup{\italic {A7\small{(9)}}} b8 c ~ |
  c2.^\markup{\italic {D-7}} d8 e ~ |
  e2.^\markup{\italic {G7}} f8 g ~ |
  g2^\markup{\italic {C maj7}} g8 e f g ~ |
  g2.^\markup{\italic {D-7}} e8^\markup{\italic {G7}} g |
  \break
  r8^\markup{\italic {C maj7}} d4 c8 b c d e |
  es8^\markup{\italic {C-7}} g, bes d c4^\markup{\italic {F-7}} d8 f |
  r8^\markup{\italic {B\flat maj7}} c4 bes8 a bes c d |
  des8^\markup{\italic {B\flat-7}} f, as c bes4^\markup{\italic {E\flat7}} c8 es |
  \break
  r8^\markup{\italic {A\flat maj7}} c4 as8 g b as g ~ |
  g2^\markup{\italic {D-7}} ~ g8^\markup{\italic {DG7\flat\small{(9)}}} bes as g ~ |
  g1^\markup{\italic {C maj7}} ~
  g1^\markup{\italic {D-7 "    " G7}}
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}
