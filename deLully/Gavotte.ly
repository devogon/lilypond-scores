\version "2.19.54"
\language "nederlands"

\header {
  title = "Gavotte"
  instrument = "Bass"
  composer = "Jean Baptiste de Lully"
  arranger = "Alwin Schroeder"
 % meter = "Allegretto grazioso"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \minor
  \time 2/2
  \partial 2
  \tempo "Allegretto grazioso" 4=100
}

contrabass = \relative c {
  \global
  % Music follows here.
  r2 | % 1
  d4\pp r e r | % 2
  f4 r d r | % 3
  bes4 r g r | % 4
  a4 r f r | % 5
  e'4 r a, r | % 6
  d4 r g, r | % 7
  a4 r a r | % 8
  d4 r r2 | % 9
  a'4 r g r | % 10
  e4 r c r | % 11
  f4 r g bes | % 12
  a2 r | % 13
  g,4 r f r | % 14
  e4 r f r | % 15
  g4 r a r | % 16
  d4 r f r | % 17
  e4 r a, r | % 18
  d4\> r g,\! r | % 19
  a1 | % 20
  a1 | % 21
  d4 r f r | % 22
  e4 r f r | % 23
  g4 r bes r | % 24
  g r g, r | % 25
  % pg 18
  a4 r d r | % 26
  e4 r f r | % 27
  g4 r bes r | % 28

  g4 r g, r | % 29
  a4 r d r | % 30
  a4 r d r | % 31

  a4 r r2 | % 32
  a4 r a r | % 33
  a1 ( | % 34
  a2) r | % 35

  d4 r e r | % 36
  f4 r bes, r | % 37
  g4 r e r | % 38
  a4 r f' r | % 39

  e4 r a, r | % 40
  d4 r g, r | % 41
  a4 r a r | % 42
  d4 r r2  | % 43

  a'4 r g r | % 44
  e4 r c r
  f4 r g bes
  a2 r
  g,4 r f r
  e4 r f r
  g4 r a r
  d4 r f r
  e4 r a, r
  d4 r g, r
  a1 a1
  d2
  \bar "|."
}

\score {
  \new Staff \with {
  %  instrumentName = "Contrabass"
  %  shortInstrumentName = "Cb."
   % midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}
