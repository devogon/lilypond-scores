\version "2.19.31"
\language "nederlands"

date = \markup {\teeny {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Prélude et Nuit"
 % subtitle =  "extrait de Nativitatem Domini Canticum"
%  instrument = "Orgue et basse de violon"
  composer = "M.A. Charpentier"
%  meter = "2/2"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
    page-count = 1
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



global = {
  \key bes \major
  \time 2/2
  \tempo 4=60
    \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

scoreAContrabass = \relative c {
  \global
  % Music follows here.
  r1 | % 1
  r4 c'2 (c8) c | % 2
  b!1 | % 3
  c4\upbow bes\upbow aes g | % 4
  f2. f4 | % 5
  c'2. bes4 | % 6
  aes1 | % 7
  g1 | % 8

  c,1 | % 9
  g'2. f4 | % 10
  ees4\upbow d\upbow ees4. f8 | % 11
  d1 | % 12
  d1 | % 13 (moved up an octave from rachel's print)
  g,1\breathe | % 14
  g'2. f4 | % 15
  ees2 ees | % 16

  bes'4\upbow bes\upbow aes g | % 17
  f4\upbow g\upbow aes bes | % 18
  c1 | % 19
  b!2 bes | % 20
  a2 aes2 | % 21
  g1 | % 22
  f1 | % 23

  e!2 ees | % 24
  d1 | % 25
  c1 | % 26
  b!2 bes | % 27
  a2 aes | % 28

  g1 ( | % 29
  g1) | % 30
  g1 | % 31
  fis2 f | % 32
  g1 | % 33
  c1 | % 34
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreAContrabass }
  \layout { }
  \header {
    piece = "Prélude"
  }
  \midi { }
}

scoreBContrabass = \relative c' {
  \global
%    \override Score.BarNumber.break-visibility = ##(#t #t #t)
  % Music follows here.
  % Nuit
%  c2^\markup{tous, sans flute, avec sourdine et lentement} (bes) | % 1
  c2 (bes) | % 1
  aes2 (g) | % 2
  f2 aes | % 3
  e!2 c | % 4
  f2 (ees) | % 5
  d2 (c) | % 6
  b!2 (c) | % 7
  \break
  g2 f' | % 8
  es2 (d)
  c2 ees
  b!2 g
  c2 (bes)
  a2 (g)
  fis1 (| % 14

  f1) | % 15
  \break
  g2 (f) | %
  ees'2 (d)
  c1
  d \breathe
  g,1 ( | %
  g1) | % 21
  g1 ( | % 22
  g1) | % 23
  g'2 (f)| % 24
  \break
  ees2 (d) | % 25
  c2 (d) |
  ees2 (c) |
  g'2 (f) |
  ees2 (d) |
  c2 (ees) |

  b!2 (g) | % 31
  \break
  c2 (bes)
  aes2 (g)
  f2 (aes)
  e!2 (c') % yeah, i cannot play this lower "c"
  f,2 (e!)

  f2 (g) | % 37
  aes2 (g) |
  aes2 (f) |
  \break
  g2 (f)
  g1 |
  c1\fermata | % original an octave lower
  R1*2 |

  g'2 (a) | % 45
  bes2 fis |
  g2 d |
  ees2 (c) |
  d2 (e!) |
  \break
  fis2 (d) |
  g2 (f) |

  ees2 (c) | % 52
  d2 (c) |
  bes2 (g) |
  fis1 ( |
  fis1) |
  g2 a |
  bes2 c |

  d1 | % 59
  d1 | % original is down an octave
  g,1 |
  d'2 (ees) |
  d2 g, |
  d'1 | % original is up an octave
  g,1 |
  r1 |

  \time 3/2
  R1. |
  g'2 d ees |
  b!1 c2 |
  g1 f2 |
  e!1 f2 |

  c'2 g aes | % 72
  e!1 f2 |
  c'2 c2 bes | % original (first c) is up ano ctave
  aes1 g2 |

  f2 f' ees | % 76
  d1. |
  ees1 d2 |
  c1 f2 |
  g1. |
  c,1. |

  c2 g aes | %82
  e!1 f2 |
  c'2 c bes | % first c is an oct. higher than orig
  a2 b!2. b4 |

  c1 c2 | % 86
  g'2 d ees |
  b!1 c2 |
  \repeat unfold 13 {g1.}
  \repeat unfold 3 {c1.}
  c1.\fermata
  R1*3
  \bar "||"
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreBContrabass }
  \layout { }
  \header {
    piece = "Nuit"
  }
%  \midi { }
}