\version "2.19.31"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}


\header {
  title = "Réveil des Bergers"
  instrument = "Orgue et basse de violon"
  composer = "Marc Antoine Charpentier"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


global = {
  \key c \major
  \time 2/2
  \tempo "Tous, avec flutes, fort et vif" %4=60
}

contrabass = \relative c {
  \global
  % Music follows here.
  r2 r4 c' | % 1
  b4 g c e, | % 2
  f1 | % 3
  e1 | % 4
  f2. f8 g | % 5
  a8 a g f e4 e8 f | % 6

  g2. g8 f | % 7
  e4 g c, c'8 bes | % 8
  a4 c f, e | % 9
  d4 c g' g, | % 10
  c1 | % 11
  c1\downbow | % 12
  \bar "|."
}

\score {
  \new Staff \with {
  %  instrumentName = "Contrabass"
   % shortInstrumentName = "Contrabass"
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
%  \midi { }
}
