\version "2.19.57"
\language "nederlands"

\header {
  title = "Strange Brew"
  subsubtitle = "(jack bruce)"
  instrument = "Bass"
  composer = "Eric Clapton..."
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  page-count = 1
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 4/4
  \tempo 4=104
}

bass = \relative c, {
  \global
  % Music follows here.
  \partial 4. r4. | % 0
      %\set Score.currentBarNumber = #1
  a8^\markup{\teeny A7+9} a r4 g8 \override NoteHead.style = #'cross
 g \revert NoteHead.style g c | % 1
  a8 a r4 g8 e g gis | % 2
  a8 a r e g8 e g c | % 3
  a8 a r e g8 e g c | % 4
  % \break
  % from here, missing alternate notes
  d8^\markup{\teeny d7+9} d r a c a c f | % 5
  d8 d r a c a c g | % 6
  a8^\markup{\teeny A7+9} a r e g e g c| % 7
  a8 a r e g e a d | % 8
  e8^\markup{\teeny E7+9} e r \override NoteHead.style = #'cross e \revert NoteHead.style a e a e | % 9
  d8 d r f (f) d f d | % 10
  a8^\markup{\teeny a7+9} a r e g e g gis | % 11
  a8 a r e g e g gis | % 12
  \bar "||"
%  \repeat volta 2 {
 %   \set Score.currentBarNumber = #14
 % \break
    a8^\markup{\teeny A7+9} a r cis (cis) a g gis | % 13
    a8^\markup{\teeny D7+9} a r r d4 c | % 14
    a8^\markup{\teeny A7+9} a r e g e g gis | % 15
  a8 a r e g e g a16 c | % 16
  % \break
  d8^\markup{\teeny D7+9} d r a c a c cis | % 17
  d8 d r c (c) a c a | % 18
  a8^\markup{\teeny A7+9} a r g (g) e g gis | % 19
  a8 a r g (g) g16 (a) c8 d | % 20
  % \break
  e8^\markup{\teeny E7+9} e r g (g) e g e^\markup{\teeny D7+9} | % 21
  d8 d r c (c) a c a | % 22
  a8^\markup{\teeny A7+9} a r e' (e) g e a | % 23
  a,8 a r e' (e) g e a | % 24
  \bar "||"
  % \break
  a,8 a r e' r g r a | % 25
  d,8 d r d r c d c | % 26
  a8 a r e' (e) g e a | % 27
  a,8 a r a' r g r e | % 28
  % \break
  d8 d r a' r c4 d8 | % 29
  d,8 d r g r gis4 a8 | % 30
  a,8 a r d r c4 g8 | % 31
  a8 a r a r g r e | % 32
  % \break
  e'8 e r e16 (d) e8 e16 (d) g8 a16 (g) | % 33
  d8 d r d c a c4 | % 34
  a8 a r e' r g r a | % 35
  a,8 a r e' (e) g4 g8 | % 36
  \bar "||"
  % a,8^\markup{\box "gtr solo"} a r e' r d g,16 (a) c8 | % 37
    a,8 a r e' r d g,16 (a) c8 | % 37
a8 d r a' (a) g f16 e d8 | % 38
  a8 a r e' r g r a | % 39
  a,8 a r e' r g r a | % 40
  % \break
  d,8 d r a' r c r d | % 41
  d,8 d r fis, (fis) g4 gis8 | % 42
  a8 a r e'8 (e) g4 a8 | % 43
  a,8 a r e' r g r a | % 44
  % \break
  e8 e r g (g) e4 es8 | % 45
  d8 d r f d g4 g16 (a) | % 46
  a8 a r e r g4 a8 | % 47
  a8 a r e (e) g4 gis8 | % 48
  \bar "||"
  a,8 a r e (e) g4 c8 | % 49
  d8 d r fis, (fis) g4 gis8 | % 50
  a8 a r e (e) g4 gis8 | % 51
  a8 a r e (e) a4 cis8 | % 52
  % \break
  d8 d r a (a) c4 cis8 | % 53
  d8 d r a (a) d (d16) a c8 | % 54
  a8 a r e (e) g4 gis8 | % 55
  a8 a r cis (cis) d4 dis8 | % 56
  % \break
  e8 e r g (g) e\staccato e\staccato es\staccato | % 57
  d8 d r c (c) d (d16) a c8 | % 58
  a8 a r4 r2 | % 59
  r4 r8 e e g g gis | % 60
  \bar "||"
  a8 a r e (e) g4 a8 | % 61
  d8 d r fis, (fis) g4 gis8 | % 62
  a8 a r e (e) g4 gis8 | % 63
  a8 a r e (e) c'4 cis8 | % 64
  % \break
  d8 d r a (a) c4 cis8 | % 65
  d8 d r a (a) d (d16) a c8 | % 66
  a8 a r e (e) g4 gis8 | % 67
  a8 a r cis (cis) d4 dis8 | % 68
  % \break
  e8 e r g (g) e\staccato e\staccato es\staccato | % 69
  d8 d r c (c) d4 c8 | % 70
  a4 r r2 | % 71
  r1\fermata | % 72
  a4 r r2 | % 73
  \bar "|."
}

\score {
  \new Staff { \clef "bass_8" \bass }
  \layout { }
}

\markup {\teeny \typewriter {
         "git revision: "  \gitRevisionNumber
         "// git commit: " \gitCommit
                  }
}
\markup {
  \teeny
  \date
}