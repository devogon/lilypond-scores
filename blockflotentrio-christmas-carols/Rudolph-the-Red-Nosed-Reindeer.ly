\version "2.19.50"
\language "nederlands"

\header {
  title = "Rudolph, the Red-Nosed Reindeer"
  instrument = "Bass"
  composer = "Johnny Marks"
  arranger = "Irmhild Beutler"
 % meter = "1/2 = 154"
  copyright = "1949 Saint Nicholas Music Publishing/Chappell & Co., Inc."
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 2=154
}

scoreAViolinI = \relative c'' {
  \global
  % Music follows here.

}

scoreAViolinII = \relative c'' {
  \global
  % Music follows here.

}

scoreACello = \relative c {
  \global
  % Music follows here.

}

scoreAContrabass = \relative c {
  \global
  % Music follows here.
  % based on part 2 tenorblockflöte
  d2 r | % 1
  g4 r g r | % 2
  e4 r g r | % 3
  b,4 r b r | % 4
  r4 e4 r g | % 5
  e'4 r g, r  | % 6
  r4 e r fis | % 7
  r4 bes, c r | % 8
  r4 f r f | % 9
  b,4 r d r | % 10
  d1\trill | % 11
  c8 r4. a'8 r4. | % 12
  r4 e r g | % 13
  c4 r g r | % 14
  r4 e r fis | % 15
  r8 g,4 r4. c4 | % 16
  r4 f r f | % 17
  b,4 r d r | % 18
  d1\trill | % 19
  e4 r c4 r | % 20
  c4 r c r | % 21
  d4 r e r | % 22
  a,4 r d r | % 23
  c1 | % 24
  d4 r d r | % 25
  g4 r g r | % 26
  e4 r g r | % 27
  b,4 r b r | % 28
  r4 e r g | % 29
  c4 r a r | % 30
  r4 e r fis | % 31
  r8 g,4 r4. c4 | % 32
  r4 f r f | % 33
  b,4 r d r | % 34
  d1\trill  | % 35
  e4 r d r | % 36
  d1\trill | % 37
  e4 r d r | % 38
  d1\trill | % 39
  e4 r e2 | % 40
}

scoreAViolinIPart = \new Staff \with {
  instrumentName = "Violin I"
  shortInstrumentName = "Vl. I"
  midiInstrument = "violin"
} \scoreAViolinI

scoreAViolinIIPart = \new Staff \with {
  instrumentName = "Violin II"
  shortInstrumentName = "Vl. II"
  midiInstrument = "violin"
} \scoreAViolinII

scoreACelloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \scoreACello }

scoreAContrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabass }

\score {
  <<
%    \scoreAViolinIPart
%    \scoreAViolinIIPart
%    \scoreACelloPart
    \scoreAContrabassPart
  >>
  \layout { }
  \midi { }
}
