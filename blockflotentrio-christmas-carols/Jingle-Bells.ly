\version "2.19.50"
\language "nederlands"

\header {
  title = "Jingle Bells"
  instrument = "Bass"
  composer = "Amerikanische Volksweise"
  arranger = "Irmhild Beutler"
  copyright = "1949 Saint Nicholas Music Publishing/Chappell & Co., Inc."
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=180
}

scoreAViolinI = \relative c'' {
  \global
  % Music follows here.

}

scoreAViolinII = \relative c'' {
  \global
  % Music follows here.

}

scoreACello = \relative c {
  \global
  % Music follows here.

}

scoreAContrabass = \relative c {
  \global
  % Music follows here.
    f,4 r f r | % 1
    f4 r c' r | % 2
    e4 r d r | % 3
    a2 r | % 4
    \break
    \repeat volta 2 {
      a4 r bes r | % 5
      a4 r f' r | % 6
      a,4 r bes r | % 7
      f4 r f' r | % 8
      \break
      bes,4 r c r | % 9
      c4 r c' r | % 10
      g4 r e r | % 11
      f2 r | % 12
      \break
      a,4 r bes r | % 13
      a4 r f' r | % 14
      a,4 r bes r | % 15
      f4 r f' r | % 16
      \break
      bes,4 r c r | % 17
      c4 r c' r | % 18
      g4 r e r | % 19
      f4 r e r | % 20
      \break
      r4 d r d | % 21
      r d r d | % 22
      r d r d | % 23
      r4 c e r | % 24
      \break
      g2 f4 r | % 25
      e4 f2 r4 | % 26
      d4 r b! r | % 27
      c4 r bes r | % 28
      \break
      r4 d r d | % 29
      r4 d r d | % 30
      r4 d r d | % 31
      r4 c e r | % 32
      \break
      g4 r f r | % 33
      e4 r f r | % 34
      e4 r d r | % 35
    }
    \alternative {
      { g,2. r4 }
      { g2 g4 r }
    }
}

scoreAViolinIPart = \new Staff \with {
  instrumentName = "Violin I"
  shortInstrumentName = "Vl. I"
  midiInstrument = "violin"
} \scoreAViolinI

scoreAViolinIIPart = \new Staff \with {
  instrumentName = "Violin II"
  shortInstrumentName = "Vl. II"
  midiInstrument = "violin"
} \scoreAViolinII

scoreACelloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \scoreACello }

scoreAContrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \scoreAContrabass }

\score {
  <<
%    \scoreAViolinIPart
%    \scoreAViolinIIPart
%    \scoreACelloPart
    \scoreAContrabassPart
  >>
  \layout { }
  \midi { }
}
