\version "2.25.4"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Message in the Bottle"
  subtitle = "The Police - ‘Regatta de Blanc’ (1979)"
  composer = "Bass: Sting"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key a \major
  \time 4/4
  \tempo "Uptempo Pop/Rock" 4=152
}

electricBass = \relative c, {
  \global
  r1^\markup{w/pick}
  r1
  r1
  r2
  r4.
  cis8
  \break

  \mark \markup{Verse}
  \repeat volta 4 {
    cis4. a8 (a4.) b8 |
    b4. fis'8 (fis4.) cis8 |
    cis4. a8 (a4.) b8 |
    b4. fis'8 (fis4.) cis8^\markup{x4} |
  }
  \break

  \mark \markup{Bridge}
  \segno a8 a a a a a a a |
  d d d (e) e e e e |
  a,4\staccato a8 a a a a a |
  d d d (e) e e e e |
  \break
  \repeat volta 2 {fis8 fis fis fis fis fis fis a |
                   d,8 d d d d d d a' | }
  fis8 fis fis fis fis fis fis a |
  d,8 d d d d d a'4^\markup{\right-align To Coda} |
  \bar "||"
\break
\bar ".|:"
\mark \markup{Chorus}
  \repeat volta 2 {
    cis,4 cis cis r8 cis |
    \tuplet 3/2 {a4\staccato a\staccato a\staccato} gis4. a8
  }
  \break
  cis4 cis cis r8 cis |
  \tuplet 3/2 {a4\staccato a\staccato a\staccato} gis2 |
  fis1 ~
  fis
  \bar "||"
  \break

  \mark \markup{Verse}
  cis'4\glissando \hideNotes
  \grace { a16\glissando }
  \unHideNotes r4 r2 |
  r1
  r1
  r2 r4. cis8 |
  \break

   \repeat volta 4 {
    cis4. a8 (a4.) b8 |
    b4. fis'8 (fis4.) cis8 |
    cis4. a8 (a4.) b8 |
    b4.^\markup{D.S. al Coda (w/repeats)} fis'8 (fis4.) cis8^\markup{x3} |
  }
  \break
\coda
  \mark \markup{Chorus}
  \repeat volta 7 {
    cis4 cis cis r8 cis |
    \tuplet 3/2 {a4\staccato a\staccato a\staccato} gis4. a8^\markup{x7}
  }
  cis4 cis cis r8 cis |
    \tuplet 3/2 {a4\staccato a\staccato a\staccato} gis2 |
   \break
   fis1 ~
     fis
     r
     r2 r8 gis r cis |
    \bar "||"
    \break

    \mark \markup{Verse}
    \repeat volta 4 {
    cis4. a8 (a4.) b8 |
    b4. fis'8 (fis4.) cis8 |
    cis4. a8 (a4.) b8 |
    b4. fis'8 (fis4.) cis8^\markup{x4} |
  }
  \break

   \mark \markup{Bridge}
  \segno a8 a a a a a a a |
  d d d (e) e e e e |
  a,4\staccato a8 a a a a a |
  d d d (e) e e e e |
  \break
  \repeat volta 2 {fis8 fis fis fis fis fis fis a |
                   d,8 d d d d d d a' | }
  fis8 fis fis fis fis fis fis a |
  d,8 d d d d d a'4 |
  \bar "||"
\break

 \mark \markup{Chorus}
  \repeat volta 7 {
    cis,4 cis cis r8 cis |
    \tuplet 3/2 {a4\staccato a\staccato a\staccato} gis4. a8^\markup{x7}
  }
  \break
  cis4 cis cis r8 cis |
    \tuplet 3/2 {a4\staccato a\staccato a\staccato} gis2 |
      fis1 ~
     fis
     \bar "||"
     \break

     \mark \markup{Outro}
  r1
  r1
  r1
  r1
  r1
  r1
  r1
     r2 r4. cis'8 |
     \break
     \repeat volta 2 {
        cis4. a8 (a4.) b8 |
    b4. fis'8 (fis4.) cis8 |
    cis4. a8 (a4.) b8 |
    b4.^\markup{Repeat to fade} fis'8 (fis4.) cis8 |
     }
}

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
