\version "2.19.83"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Every Breath You Take"
  subtitle = "The Police"
  subsubtitle = "Synchronicity - 1983"
  instrument = "Bass"
  composer = "Sting"
  meter = "Moderate Rock (116)"
  % Remove default LilyPond tagline
  tagline = \date
  poet = "all pitches sound one quarter-step flat"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key a \major
  \time 4/4
  \partial 4
  \tempo "Moderate Rock" 4=116
  \set Score.markFormatter = #format-mark-box-alphabet
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s4
  a1
  s1
  fis1:m
  s1
  d1:sus2
  e1:sus2
  a1
  s1
  a1
  s1
  fis1:m
  s1
  d1:sus2
  e1:sus2
  fis1:m
  s1
  a1
  s1
  d1:sus2
  s2 d1:7
  a1
  s1
  b1
  s1
  e1:sus2
  s1
  a1
  s1
  fis1:m
  s1
  d1:sus2
  e1:sus2
  fis1:m
  f1
  s1
  g1
  s1
  f1
  s1
  g1
  s1
  f1
  s1
  a1
  s1
  fis1:m
  s1
  d1:sus2
  e1:sus2
  fis1:m
  s1
  a1
  s1
  fis1:m
  s1
  d1:sus2
  e1:sus2
  fis1:m
  s1
  s1
  s1
  a1:
  s1
  fis1:m
  d1:sus2
  a1
  s1
  fis1:m
  d1:sus2
  a1
  s1
  fis1:m
  d1:sus2
}

bass = \relative c, {
  \global
  % Music follows here.
  \mark \default
  \partial 4 r8 e, |
  \repeat unfold 8 {a8\staccato} | % 1
  \repeat unfold 8 {a8} |
  fis8 fis16 fis fis8 fis fis fis fis fis |
  \repeat unfold 5 {fis8} fis16 fis fis8 fis |
  \break
  d'8\staccato d16 d \repeat unfold 5 {d8\staccato} e\staccato |
  \repeat unfold 8 {e8}
  a,8 a a a a a a a a a^\markup{1. Every breath you} a a a a a a |
  \break
  \mark \default
  \repeat volta 2 {
    a8^\markup{day...}^\markup{take...}^\markup{\bold "1st and 2nd verses"} \repeat unfold 15 {a8}
    \repeat unfold 16 {fis}
    \break
    \repeat unfold 8 {d'}
    \repeat unfold 8 {e8}
    \break
    \repeat unfold 8 {d}
    \repeat unfold 8 {e}
  }
  \alternative {
    {% grace e16
       \grace {e8} fis8 fis16 fis fis8 fis fis e r fis |
    fis16 fis fis8^\markup{2. every single} fis fis fis fis e e}
    {a,8 a16  a \repeat unfold 8 {a8} a^\markup{Oh can't you} a a a a cis}
  }
  \mark \default
  \bar "||"
  d8\segno^\markup{\bold "Pre-chorus"} d^\markup{see...} \repeat unfold 6 {d8}
  d8 c! c c c c c d |
  \break
  \repeat unfold 15 {a8} ais |
  \repeat unfold 16 {b8}
  \break
  \repeat unfold 11 {e8} e^\markup{Every move you} e e e e
  \bar "||"
  \mark \default
  a,8^\markup{\bold "Chorus"} a^\markup{make...} \repeat unfold 14 {a8}
  \repeat unfold 16 {fis8}
  \repeat unfold 7 {d'8}
  e8 e e^\markup{\teeny "2nd time" to coda} e e e e e e\coda
  \repeat unfold 16 {fis8}
  \bar "||"
  \break
  \mark \default
  f!8^\markup{\bold "Bridge"} f^\markup{Singe you've gone...} f, f' f f f, f' | % 35
  f!8 f f, f' f f f, f' | % 36
  g,!8 g g' g, g g g' g, | % 37
  g!8 g g' g, g g' g, g' | % 38
  \break
  f,!8^\markup{39} f f' f, f f f' f, |
  f!8 f f' f, f f'16 f f,8 f' |
  g,!8^\markup{41} g g' g, g g g' g, |
  g!8 g g' g, g g' g, g' |
  f,!8 f f' f, f f f' f, |
  \break
  f!8 f f' f, f f'16 f f,8 f'16 f | % 44
  \repeat volta 2 {
    \repeat unfold 8 {a,8\staccato}
    \repeat unfold 8 {a8}
    \repeat unfold 16 {fis8}
    \repeat unfold 7 {d'8}
    \repeat unfold 9 {e8}
  }
  \alternative {
    {\repeat unfold 12 {fis,8} fis' e e e}
    {\repeat unfold 10 {a,8} a^\markup{Oh} a a^\markup{can't}^\markup{\italic D.S.}\segno a^\markup{\italic al Coda}\coda a cis}
  }
  \bar "||"
  % coda
  \break
  \repeat unfold 2 {fis8\staccato fis16\staccato fis\staccato} \repeat unfold 10 {fis8\staccato} fis16 a fis (e) |
  \repeat unfold 7 {d8} e | % 57
  \repeat unfold 8 {e8} |
  \break
  \repeat unfold 12 {fis8} fis8->\glissando (e) e\glissando (fis) |
  \repeat unfold 8 {fis,8} |
  \repeat unfold 5 {fis'8} \repeat unfold 3 {e} |
  \break
  \repeat unfold 16 {a,8}
  \repeat unfold 7 {fis8} d' |
  \repeat unfold 8 {d8}
  \break
  \mark \default
  a8^\markup{\bold Outro} \repeat unfold 7 {a8}
  \repeat unfold 8 {a8}
  \repeat unfold 7 {fis8}
  \repeat unfold 9 {d'}
  \break
  \repeat unfold 16 {a8}
  \repeat unfold 2 {fis8 fis16 fis} fis8 fis fis a |
  \repeat unfold 7 {d8} e |
  \break
  \repeat volta 2 {
    \repeat unfold 8 {a,8}
    \repeat unfold 8 {a}
    \repeat unfold 7 {fis} a  |
    d8 d d^\markup{\tiny \left-align "(play 7 times and fade)"} d d d16 d d8 e
  }
}

chordsPart = \new ChordNames \chordNames

bassPart = \new Staff { \clef "bass_8" \bass }

\score {
  <<
%    \chordsPart
    \bassPart
  >>
  \layout { }
}
