\version "2.19.84"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Roxanne"
  subtitle = "The Police"
  subsubtitle = "Outlandos d‘Amour - 1978"
  instrument = "Bass"
  composer = "Sting"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \minor
  \time 4/4
  \tempo 4=132
  \set Score.markFormatter = #format-mark-box-alphabet
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  \mark \markup{\box \bold Intro}
  r1
  r1
  r8 g' g4\staccato r2 |
  r8 f8 f4\staccato r2 |
  r8 es8 es4\staccato r2 |
%  \break
  r8 d d4\staccato r2 |
  r8 c c4\staccato r2 |
  r8 f, f4\staccato r4 f8 g8-> ~ |
  g1 ~ |
  g1
  \bar "||"
  \break
  \mark \markup{\box \bold Verse}
  \repeat volta 2 {
    \bar ".|:"
    r8 g' g4\staccato r2 |
    r8 f f4\staccato r2 |
    r8 es es4\staccato r2 |
    r8 d d4\staccato r2 |
%    \break
    r8 c c4\staccato r2 |
    r8 f, f4 r4 f8 g8-> ~ |
    g1 ~ |
    g1 |
  }
  \break
  r8 c c4\staccato   \override NoteHead.style = #'cross c8 \revert NoteHead.style c c c |
  r8 f, f4\staccato r2 |
  r8 g g4\staccato r2 |
  r8 g g4\staccato r2 |
%  \break
  r8 c c4\staccato \override NoteHead.style = #'cross c8 \revert NoteHead.style c c c |
  r8 f, f4\staccato r2 |
  g8\< g g g g g g g |
  g8 g g g\~\ff r c-> r bes-> ~
  \bar "||"
  \break
  \mark \markup{\box \bold Chorus}
  \repeat unfold 12 {bes8 } r es-> r f-> ~ |
  f8 f f f f f f f |
  e! (f) f f r f,-> r g-> ( |
%  \break
  g) g g  g fis (g) g g |
  fis (g) g g r c-> r bes-> ~ |
  bes bes bes bes a (bes) bes bes |
  a (bes) bes bes r es-> r f-> ~|
%  \break
  f8 f f f e! (f) f f |
  e!8 (f) f f e,!-> f4 g8-> ~ |
  g1 ~ |
  g2. r8 f |
%  \break
  g4\staccato\mf r r r8 g |
  f4\staccato r r r8 f |
  g4\staccato r r r8 g |
  f4\staccato r r2
%  \bar "||"
  \break
     \bar ".|:"
  \repeat volta 2 {

    r8 g' g4\staccato r2 |
%    \override NoteHead.style = #'cross f8 \revert NoteHead.style f f4\staccato \override NoteHead.style = #'cross \repeat unfold 4 {f8}  \revert NoteHead.style  |
    r8 f f4\staccato r2 |
  %r8 es es4\staccato \override NoteHead.style = #'cross es8 es  \revert NoteHead.style  r4 |
  r8 es es4\staccato r2 |
%  r8 d d4\staccato \override NoteHead.style = #'cross d8 d  \revert NoteHead.style  r4 |
  r8 d d4\staccato r2 |
%    \break
%    r8 c8 c4\staccato  \override NoteHead.style = #'cross c8 c c c  \revert NoteHead.style |
  r8 c c4\staccato r2 |
 % r8 bes bes4\staccato r4 \override NoteHead.style = #'cross g8 \revert NoteHead.style  g ~ |
 r8 f, f4\staccato r4 f8 g-> ~ |
    g1 ~ |
    g1 |
  }
  \break
  r8 c8 c4\staccato  r2 |%\override NoteHead.style = #'cross c8 c c c  \revert NoteHead.style |
  r8 f, f4\staccato r2 | %\override NoteHead.style = #'cross  f8 f r f  \revert NoteHead.style |
  r8 g g4\staccato r2 | %\override NoteHead.style = #'cross g8 g g4\staccato \override NoteHead.style = #'cross  g8 r g g  \revert NoteHead.style |
  r8 g g4\staccato r2 | %\override NoteHead.style = #'cross \repeat unfold 4 {g8} \revert NoteHead.style  |
%  \break
  r8 c c4\staccato r2 | %\override NoteHead.style = #'cross  \repeat unfold 4 {c8}  \revert NoteHead.style |
  r8 f, f4\staccato r f8 g-> ~ | %\override NoteHead.style = #'cross \repeat unfold 4 {f8}  \revert NoteHead.style |
  g8\< g g g g g g g |
  g g g g\!\ff r c r bes ~ |
  \bar "||"
  \break
  \mark \markup{\box \bold Chorus}
  \repeat volta 2 {
\bar ".|:"
    \repeat unfold 8 {bes8 } |
    a (bes) bes bes r es-> r f-> ~ |
  f8 f f f f f f f |
  e! (f) f f r f,-> r g-> ( |
%  \break
  g) g g  g fis (g) g g |
  fis (g) g g r c-> r bes-> ~ |
  bes bes bes bes a (bes) bes bes |
  a (bes) bes bes r es-> r f-> ~|
%  \break
  f8 f f f e! (f) f f |
  e!8 (f) f f r f,-> r g-> ~ |
    g g g g fis (g) g g |
    fis^\markup{Repeat to fade} (g) g g r c r bes~
  }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  % \new TabStaff \with {
  %   stringTunings = #bass-four-string-tuning
  % } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
