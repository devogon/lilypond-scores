\version "2.19.83"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Every Breath You Take"
  subtitle = "The Police"
  subsubtitle = "Synchronicity - 1983"
  instrument = "Bass"
  composer = "Sting"
  meter = "Moderate Rock (116)"
  % Remove default LilyPond tagline
  tagline = \date
  poet = "all pitches sound one quarter-step flat"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
    \omit Voice.StringNumber
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key des \major % could also be aes major
  \time 4/4
%  \partial 4
  \tempo "Moderate Rock" 4=116
  \set Score.markFormatter = #format-mark-box-alphabet
}

chordNames = \chordmode {
  \global
				% Chords follow here.
}

electricBass = \relative c, {
  \global
		% Music follows here.

  r2 r4 r8 e,! | % 0:19
  \repeat unfold 16 {as8}
  \repeat unfold 16 {f}
  \break
  des'8 des16 des r8 des des des des des | % 0:28
  \repeat unfold 8 {es\3}
  \repeat unfold 16 {as,}
  \break
  \repeat unfold 16 {as}
  \repeat unfold 16 {f}
  \break
  \repeat unfold 7 {des'} es\3 | % 0:44
  \repeat unfold 8 {es\3}
  \repeat unfold 5 {f\3} es\3 es\3 f\3 |
  f\3 f\3 f\3 f\3 f\3 f\3 es\3 es\3 |
  \break
  \repeat unfold 8 {as}
  as as16 as r8 \repeat unfold 5 {as}
  \repeat unfold 16 f 
  \break
  \repeat unfold 8 {des} | % 1:00
  \repeat unfold 8 {es\3}
  \repeat unfold 15 {as,} c
  \break
  \repeat unfold 8 {des} | % 1:10
  b! b b b b b b b 
  \repeat unfold 15 {as} a!\4
  \break
  \repeat unfold 16 {bes\4} % 1:17
  \repeat unfold 16 {es\3}
  \break
  \repeat unfold 16 {as,} % 1:26
  \repeat unfold 16 {f}
  \break
  \repeat unfold 7 {des'} es\3 | % 1:33
  \repeat unfold 8 {es\3}
  \repeat unfold 16 {f\3}
  \break
  e!\3 e\3 e, e'\3 e\3 e\3 e, e'\3 | % 1:41
  e!\3 e\3 e, e'\3 e\3 e, e'\3 e, |
  fis fis fis' fis, fis fis fis' fis, |
  fis fis fis' fis, fis fis' fis, fis' |
  \break
  e,! e e'\3 e, e e e'\3 e, | % 1:50
  e! e e'\3 e, e e'\3 e, e'\3 |
  fis, fis fis' fis, fis fis fis' fis, |
  fis fis fis' fis, fis fis' fis, fis' |
  \break
  e,! e e'\3 e, e e e'\3 e, | % 1:58
  e! e e'\3 e, e e e'\3 e, |
  \repeat unfold 16 as
  \break
  \repeat unfold 16 {f} % 2:06
  \repeat unfold 8 {des'}
  \repeat unfold 8 {es\3}
  \break
  \repeat unfold 8 {f,} % 2:14
  f f f f'\3 f\3 f\3 es\3 es\3 |
  \repeat unfold 16 {as,}
  \break
  \repeat unfold 16 {f} % 2:23
  \repeat unfold 7 {des'} es
  \repeat unfold 8 {es\3}
  \break
  \repeat unfold 15 {as,} c % 2:30
  \repeat unfold 8 {des}
  b! b b b b b b b |
  \break
  \repeat unfold 15 {as} a!\4 % 2:39
  \repeat unfold 16 {bes\4}
  \break
  \repeat unfold 16 {es\3} % 2:47
  \repeat unfold 8 {as,}
  as as16 as r8 \repeat unfold 5 {as}
  \break
  \repeat unfold 16 {f} % 2:56
  \repeat unfold 8 {des'}
  \repeat unfold 8 {es\3}
  \break
  \repeat unfold 15 {f\3} es % 3:03
  \repeat unfold 7 {des} es\3
  \repeat unfold 8 {es\3}
  \break
  \repeat unfold 13 {f\3} es\3 es\3 f\3 % 3:12
  \repeat unfold 8 {f,\3}
  \repeat unfold 5 {f'\3} es\3 es\3 es\3
  \break
  \mark \markup {8x}
  \repeat volta 8 {
  \repeat unfold 16 {as,} % 3:19
  \repeat unfold 8 {f}
  \repeat unfold 7 {des'} es\3 |
  }
  % \break
  % \repeat unfold 16 {as,} % 3:29
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
  % \repeat unfold 16 {as,} % 3:36
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
  % \repeat unfold 16 {as,} % 3:45
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
  % \repeat unfold 16 {as,} % 3:52
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
  % \repeat unfold 16 {as,} % 4:01
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
  % \repeat unfold 16 {as,} % 4:09
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
  % \repeat unfold 16 {as,} % 4:17
  % \repeat unfold 8 {f}
  % \repeat unfold 7 {des'} es |
  % \break
}


chordsPart = \new ChordNames \chordNames

%bassPart = \new Staff { \clef "bass_8" \electricBass }

bassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
%    \chordsPart
    \bassPart
  >>
  \layout { }
}
