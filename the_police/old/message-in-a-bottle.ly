\version "2.19.84"

\header {
  title = "Message In A Bottle"
  subtitle = "(The Police - 1979 - Reggatta de Blanc)"
  instrument = "Bass"
  composer = "Sting"
  meter = "Uptempo Pop/Rock"
  piece = "w/pick"
}

\paper {
  indent = 0.0\cm
}

\layout {
  \omit Voice.StringNumber
}

global = {
  \key a \major
  \time 4/4
  %\tempo "Upbeat Pop/Rock"
%    \set countPercentRepeats = ##t
  %    \set repeatCountVisibility = #(every-nth-repeat-count-visible 2)
}

electricBass = \relative c, {
  \global
  \mark \markup{\smallCaps "Intro"}
  r1
  r1
  r1
  r2 r4. cis8 |
  \break
  \mark \markup{\smallCaps "1st Verse"}
%  \mark \markup{\tiny "is this four or five times through?"}
  \repeat volta 4 {
    cis4.\glissando\4 a8\4 ~ a4. b8\4 |
    b4.\4 fis'8\3 ~ fis4. cis8\4 |
    cis4.\glissando\4 a8\4 ~ a4. b8\4 |
  }
  \alternative {
      {b4.\4 fis'8\3 ~ fis4. cis8\4 }
      {b4.\4 fis'8\3 ~ fis2}
    }
  \break

  \mark \markup{ \smallCaps "Bridge"}
  \repeat unfold 8 {a,8\4} % a a a a a a a |
  d8\3 d\3 d\3 e\3 ~ e\3 e\3 e\3 e\3 |
  \repeat unfold 8 {a,8\4}
  d8\3 d\3 d\3 e\3 ~ e\3 e\3 e\3 e\3 |
 % \break
  \repeat percent 3 { \repeat unfold 7 {fis8\3}  a\2
  \repeat unfold 7 {d,\3}  a'\2 |}
  \break

\mark \markup{ \smallCaps "Chorus"}
    cis,4 cis cis4.  cis8 |
    a8\4 a4\4 a8\4 gis4. gis8 |
    cis4 cis cis4.  cis8 |
    a8\4 a4\4 a8\4 gis4. gis8 |
%    \break
    cis4 cis cis4.  cis8 |
    a8\4 a4\4 a8\4 gis2\staccato |
    fis1 ~
    fis
  \bar "||"
  \break % \set repeatCountVisibility = #(every-nth-repeat-count-visible 5)

  \mark \markup{\smallCaps "2nd Verse"}
  cis'2 r |
  r1
  r1
  r2. r8 cis\f\4 |
 % \break
  \repeat volta 3 {
    \mark \markup{3x}
    cis4.\glissando\4 a8\4 ~ a4. b8\4 |
    b4.\4 fis'8\3 ~ fis4. cis8\4 |
    cis4.\glissando\4 a8\4 ~ a4. b8\4 |
    b4.\4 fis'8\3 ~ fis4. cis8\4
  }
  \break

  \mark \markup{\smallCaps "Bridge"}
  \repeat percent 2 {
    \repeat unfold 8 {a8\4}
    \repeat unfold 3 {d8\3} e8\3 ~ e\3 e\3 e\3 e\3 |
  } %\break
   \repeat percent 2 {fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 a8\2 |
   d,8\3 d8\3 d8\3 d8\3 d8\3 d8\3 d8\3 a'8\2 | }
%       fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 a8\2 |
%    d,8\3 d8\3 d8\3 d8\3 d8\3 d8\3 d8\3 a'8\2 |
      fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 fis8\3 a8\2 |
   d,8\3 d8\3 d8\3 d8\3 d8\3 d8\3  a'4\2 |
  \break

  \mark \markup{\smallCaps "Chorus"}
  \repeat percent 6 {
    cis,4 cis cis4.  cis8 |
    a8\4 a4\4 a8\4 gis4. gis8 |
  }
  cis4 cis cis4.  cis8 |
  a8\4 a4\4 a8\4 gis2 |
  %\break
  fis1 ~
  fis
  fis
  r2 r8 fis4 fis8
  \bar "||" \break


  \mark \markup{\underline \smallCaps "3rd Verse"}
  \repeat percent 7 {
    cis'4.\glissando\4 a8\4 ~ a4. b8\4 |
    b4.\4 fis'8\3 ~ fis4. cis8\4 |
  }
%  \break
  cis4.\glissando\4 a8\4 ~ a4. b8\4 |
  b4.\4 fis'8\3 ~ fis2\3|
  \bar "||"
  \break

  \mark \markup{\smallCaps "Bridge"}
  \repeat unfold 8 {a,8\4} % a a a a a a a |  \set repeatCountVisibility = #(every-nth-repeat-count-visible 5)
  d8\3 d\3 d\3 e\3 ~ e\3 e\3 e\3 e\3 |
  \repeat unfold 8 {a,8\4}
  d8\3 d\3 d\3 e\3 ~ e\3 e\3 e\3 e\3 |
  %\break
  \repeat percent 2 { \repeat unfold 7 {fis8\3}  a\2
  \repeat unfold 7 {d,\3}  a'\2 |}

  \repeat unfold 7 {fis8\3}  a\2
  \repeat unfold 6 {d,\3}  a'4\2 |
  \bar "||"
  \break

  \mark \markup{\smallCaps \underline "Chorus"}
   \repeat percent 6 {
     cis,4 cis cis4.  cis8 |
    a8\4 a4\4 a8\4 gis4. gis8 |
  }
  cis4 cis cis4.  cis8 |
    a8\4 a4\4 a8\4 gis2 |
    fis1
   \repeat unfold 7 {r1}
   r2. r8 cis'\4 |
   \bar "||"
   \break

   \mark \markup{\smallCaps "Outro"}
 %   \set repeatCountVisibility = #(every-nth-repeat-count-visible 0)
  \repeat percent 6 {
      cis4.\glissando\4^\markup{"6x"} a8\4 ~ a4. b8\4 |
      b4.\4 fis'8\3 ~ fis4. cis8\4 |
  }
  \repeat percent 6 {
      cis4.\glissando\4^\markup{"6x"} a8\4 ~ a4. b8\4 |
      b4.\4 fis'8\3 ~ fis4. cis8\4 |
  }
  \repeat percent 6 {
      cis4.\glissando\4^\markup{"6x"} a8\4 ~ a4. b8\4 |
      b4.\4 fis'8\3 ~ fis4. cis8\4 |
  }
  \repeat percent 6 {
      cis4.\glissando\4^\markup{"6x"} a8\4 ~ a4. b8\4 |
      b4.\4 fis'8\3 ~ fis4. cis8\4 |
  }
  \bar "|."
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBassPart = \new StaffGroup <<
  \new Staff \with {
    midiInstrument = "electric bass (finger)"
  } { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

chordsPart = \new ChordNames \chordNames

\score {
  <<
    \electricBassPart
    \chordsPart
  >>
  \layout { }
  \midi {
    \tempo 4=152
  }
}
