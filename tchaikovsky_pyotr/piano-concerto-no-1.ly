\version "2.19.54"
\language "nederlands"

% piano concerto no.1 in b-flat minor op23 - 1 allegro
% https://en.wikipedia.org/wiki/Piano_Concerto_No._1_(Tchaikovsky)

\header {
  title = "Piano Concerto No. 1 in B-flat minor"
  composer = "Pyotr Ilyich Tchaikovsky"
  opus = "Opus 23"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key c \major
  \time 3/4
  \tempo 4=52
}

cello = \relative c {
  \global
  % Music follows here.
  \partial 2 r4 r
  r4\ff g4 r |
  r4 fis r |
  r4 f e |
  es' d g |
  c8 r r4 r |
  r4 r8 g8 (e d |
  c4) e4. (d8) |
  \break
  f4. (e8 b c) |
  a4 d4. (e8) |
  a4. d,8 (b a) |
  g4 d'4. (e8) |
  g4. (d8) d (e) |
  g4. (e8) a (g) |d4 ( \tuplet 3/2 {d8->) b'-> a->} \tuplet 3/2 {g8-> f-> d->}
  \break
  c4 e4. (d8) |
  f4. (e8) b (c) |
  a4 d4. (e8) |
  g4. f8 (e f) |
  e8 a, (e') c (b) c (|
  \break
  f) c (f) b, (c) a |
  g\tenuto (a\tenuto b\tenuto c\tenuto d\tenuto e\tenuto) |
  d8 (e) a,4.-> ( b8) |
  c2. (
  c2) r4
  \bar "|."
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}

\markup {
  \teeny
  \date
}