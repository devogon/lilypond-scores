\version "2.19.46"
\language "nederlands"

\header {
  title = "Concerto Grosso No. 8"
  subtitle = "Fatto Per la notte di natale"
  subsubtitle = "Christmas Concerto"
  instrument = "Basso"
  composer = "Arcangelo Corelli"
  arranger = "Augener"
%  meter = "meter"
%  piece = "Vivace"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \numericTimeSignature
  \time 3/4
%  \tempo "Vivace" 4=172
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  %\markLengthOn
 % \defineBarLine "[" #'("" "[" "")
 % \defineBarLine "]" #'("]" "" "")
   \override Score.BarNumber.break-visibility = ##(#t #t #t)

}

scoreAContrabassI = \relative c {
  \global
    \key bes \major
  % Music follows here.
  \tempo "Vivace" 4 = 172
  % Music follows here.
  g4\f r r
  a4 r r
  bes4 b! r8 b
  c4 c c
%  \mark \markup{\box "5"}
  fis2 g4\< (
  g4 ) es2\!
  d2.\fermata
  \bar "||"
  \break
  \tempo "Grave" 4 = 40
  \time 4/2
  g,2\p g'1 fis2
  g2 bes2 es,1
 % \mark \markup{\box "10"}
  d1 es2 e!_\markup{\italic "cresc."}
  d1 g,2 g' (
  g2 ) f4 g a2. g4_\markup{\italic "dim"}
  fis4 d4 g2 c,1
 % \mark \markup{\box "15"}
  d1\p es
  f1. fis2
  g1 c,1
  b!2 c'1\< bes!2
  as2.  as4\staccato  d,1\!
%  \mark \markup{\box "20"}
  cis1\> d
  \override Staff.NoteHead.style = #'altdefault
  g,\breve\fermata\!
  \bar "||"
}

scoreAContrabassII = \relative c {
  \global
  \key bes \major
  % Music follows here.
  \bar ".|:"
  \time 4/4
  \tempo "Allegro" 4 = 120
  \repeat volta 2 {
  r1
  r8 g\f bes g c a d d
  g,4 r r2
  r8 bes d bes es c f f,
 % \mark \markup{\box "5"}
 bes4 r r2
  r2 r4 r8 d
  \break
  es8 d es c d fis g bes,
  c a d d g,4 r
  r1
  %\mark \markup{\box "10"}
  r1
  r2 f'4\mf-> r8 d
  es8 f g d es c f f,
  \break
  bes4 r r2
  r1
 % \mark \markup{\box "15"}
   bes8\f-> c d e! f g a fis
  g8->\< a bes c\! d->\> d, f d\!
  es8\< f g a\! bes->\> bes, d bes\!
  \break
  c8 d es c d d' es-> bes
  c8-> a d d, g4-> f->
 % \mark \markup{\box "20"}
 es2-> d->\>
  es2\p\! d
  %\bar ":\][|:"
  }
    \repeat volta 2 {
    R1
    r4 d\f g g,
    R1
    \break
  %  \mark \markup{\box "25"}
    r4 c f f,
    R1
    g'4->\mf r8 f g a bes g
    a8 cis d f, g e! a a,
    d4 r r2
    \break
    r4 g c, r
    r2 r4 f
    bes,8 bes' d bes f' f, a f
    g8 a bes c d d, f d
    es8 f g a bes bes, d bes
    c8 d es c d d' es bes
    c8 a d d, g bes\p d bes
    f' f, a f g a bes c
    d8 d, f d es f g a
    bes bes, d bes c d es c
    d8 d' es bes c a d d,
    g8 f\f es c d d es bes
    c8 a d d g,2
  }
}

scoreAContrabassIII = \relative c {
  \global
  \key bes \major
  % Music follows here.
  \tempo "Adagio" 4 = 70
  \time  4/4
  es4\p r d r |
  c4 r bes8\f es bes' bes,
  es4 r r2
  r2 r4 r8 g
  a8 bes es, f bes,4 r
  r2 es4\p bes
  c4 g as bes
  es8 g as as, bes es bes' bes,
  \bar "||"
  \break
  \tempo "Allegro" 4 = 120
  es8 es es es b! b b b |
  c8 c c c c' c c c |
  fis,8 fis fis fis g g g g |
  e!8 e e e f f f f |
  d8 d d d es es es es |
  g,8 g g g as as as as |
  a8 a a a bes bes bes bes |
  g8 g g g as as as as |
  bes8 bes bes bes es es es es |
  g,8 g g g as as as as |
  bes8 bes bes bes es4 r
  d4 r c r
  \break
  bes2 r\fermata
  \bar "||"
  \tempo "Adagio" 4 = 70
  es4\p r d r
  c4 r bes8\f es bes' bes,
  es4 r r2 r2 r4 r8 g
  \break
  a8 bes es, f bes,4 r
  r2 es4\p bes
  c4 g as bes
  es8 g as as, bes es bes' bes,
  \break
  es4 d c bes
  as4 r as\p r8 as
  bes4 es bes2
  es1
  \bar "|."
}



scoreAContrabassIV = \relative c {
  \global
  \key bes \major
  % Music follows here.
  \tempo "Vivace" 4 = 172
  \time 3/4
  \bar ".|:"
  \repeat volta 2 {
    R1*3/4*4
    g'4\f g,2
    f'2.
    es4 es2
    d2.}
  \repeat volta 2 {
      R1*3/4*1
      g4 bes, g
      R1*3/4*1
      f'4 a, f
      R1*3/4*1
      a'2 d,4
      g,4 a2
      d2.
      b!4 r r
      c4 r r
      a4 r r
      bes2 bes'4
      e,!4 fis2
      g2 bes,4 c4 d2
      g,2 bes4\p c2 c4
      d2 es4
      c4 d d
      g,2.
  }
}

scoreAContrabassV = \relative c {
  \global
  \key bes \major
  % Music follows here.
  \tempo "Allegro" 4 = 120
  \time 2/2
  \partial 2 r2
  \repeat volta 2 {
    R1*1
    r2
    g'4\f fis
    g4 d es2
  d2 r
  r1
  r2 g4\p fis
  \break
  g4 d es2
  d2 r
  r1
  r2 g4\f b!
  c4 f, g g,
  c2 r
  \break
  r2 c'4 b!
  c4 g r2
  r2 d'4 cis
  d4 a r2
  d4 c r2
  bes4 a r2
    \break
    e!4 d a' a,
    d2 r
    d'4\p c r2
    bes4 a r2
    e!4 d a' a,
    d2
  }
  \break
  \repeat volta 2 {
  \partial 2 r2
  R1*2
  bes'4\f bes, r2
  bes'4 bes, r2
  r1
  r2 d'4 d,
  es4 d r2
  \break
  r2 es4 f
  g4 f r2
  r2 bes4 es,
  f4 bes, f' f,
  bes2 r
  des2\p r2
  \break
  es2 r
  f4\f bes,4 f' f,
  bes2 r
  g'2\p r
  g4\f c, g' g,
  c2 r
  \break
  a'2\p r
  a4\f d, a' a,
  d2 r
  r1
  r2 g4 fis
  g4 d es2
  \break
  d2 r
  r1
  r2 bes'4 bes,
  c2. c4
  d2 r
  r1
  r2 bes
  \break
  c2 d
  es2 bes
  c2 d
  g4 f es d
  es d c b!
  c4 g' b, g'
  \break
  c,4\p g' b,! g'
  c,4\f d es c
  d4 g, d' d
  g2 r4 c,\p
  d4 g, d' d
  g,2 %^\markup{"continue into pastorale ad libitum"}
  }
}

scoreAContrabassVI = \relative c {
  \global
  \slurDashed
  \key g \major
  % Music follows here.
  \time 12/8
  \tempo "Largo (broadly)" 4 = 5
  g4. r4 r8 r2.
  R1*12/8*4
  g4 (a8) b4 (g8) c4 (b8) c (b a)
  b4 (a8) b4 (g8) c8 (b) c d4 (d8)
  \break
  g,4. r4 r8 r2.
  R1*12/8*2
  r4 r8 r8 r8 fis' g4 (d8) a'4 (a,8) |
  d4. d'4.\p d2.
  \break
  d4. g,4. g2.
  g4 (a8) b4 (b,8) e4. d4.
  c4. b4. a4. g4. |
  d'4. r4 r8 r2.
  R1*12/8*2
  \break
  r4 r8 r8 r8 b'8 a4 (g8) c,4 (d8) |
  g,4 (a8) b4 (g8) c4 c8 r r cis |
  d4 d8 r r dis e4 e,8 r r f'
  \break
  g4. r4 r8 r2.
  r2. r4 r8 r r g\p
  c,4 (g8) d'4 (d8) g,4. c4.
  r4\fermata r8 b4.\p r2.\fermata
  \break
  g4\f (g8) d'4 (d8) g4. r8 r8 e'8
  dis4 (b8) e,4 (e'8) dis4 (b8) r r a\p
  gis4 (e8) a,4 (a'8) gis4 (e8) r r b'\f
  \break
  ais4 (fis8) b,4 (b'8) ais4 (f8) r4 r8 | %^\markup{"page 8 second staff, meause 29"}
  r1*12/8*1
  r4 r8 r r e e4. d4 (e8) |
  fis4 (e8) fis4 (fis,8) b4. r4 r8 |
  \break
  r2. r4 r8 r r d
  e4. r8 r e fis4. b,4.
  fis'4. fis,4. b2.
  R1*12/8*5
  \break
  g4\p (a8) b4 (g8) c4 (b8) c (b a)
  b4 (a8) b4 (g8) c (b c) d4 (d8)
  g,4. r4 r8 r2.
  \break
  R1*12/8*2
  r4 r8 r r fis' g4 (d8) a'4 (a,8)
  d4. r8 r g fis4 (e8) fis4 (d8)
  \break
  g4 (a8) b4 (g8) fis4 (e8) fis4 (d8)
  g4 (a8) b4 (g8) e4.\p r8 r fis
  g4. r8 r g c,4.\pp r8 r c
  \break
  b4. r8 r b c4 (g8) d'4 (d8)
  g,4. r8 r b c4 (b8) c4 (a8)
  b4 (a8) b4 (g8) c4 (b8) c (b a)
  \break
  b4 (a8) b4 (g8) c4 (b8) a4 (g8)
  d'4. g,4. d'4. r8 r d
  g,4. r4 r8 d'4.\pp r4 r8
  g,4 r4 r8 r2.\fermata
  \bar "|."
}

scoreAContrabassIPart = \new Staff \with {
 % instrumentName = "Contrabass I"
%  shortInstrumentName = "Cb. I"
} { \clef bass \scoreAContrabassI }

scoreAContrabassIIPart = \new Staff \with {
 % instrumentName = "Contrabass II"
 % shortInstrumentName = "Cb. II"
} { \clef bass \scoreAContrabassII }

scoreAContrabassIIIPart = \new Staff \with {
 % instrumentName = "Contrabass III"
 % shortInstrumentName = "Cb. III"
} { \clef bass \scoreAContrabassIII }

scoreAContrabassIVPart = \new Staff \with {
 % instrumentName = "Contrabass IV"
 % shortInstrumentName = "Cb. IV"
} { \clef bass \scoreAContrabassIV }

scoreAContrabassVPart = \new Staff \with {
 % instrumentName = "Contrabass V"
 % shortInstrumentName = "Cb. V"
} { \clef bass \scoreAContrabassV }

scoreAContrabassVIPart = \new Staff \with {
 % instrumentName = "Contrabass VI"
 % shortInstrumentName = "Cb. VI"
} { \clef bass \scoreAContrabassVI }

\score {
% <<
    \scoreAContrabassIPart
  %  \scoreAContrabassIIPart
  %  \scoreAContrabassIIIPart
  %  \scoreAContrabassIVPart
  %  \scoreAContrabassVPart
  %  \scoreAContrabassVIPart
 % >>
  \layout { }
}

\score {
   \scoreAContrabassIIPart
  \layout { }
}

\score {
   \scoreAContrabassIIIPart
  \layout { }
}

\score {
   \scoreAContrabassIVPart
  \layout { }
}

\score {
   \scoreAContrabassVPart
  \layout { }
}

\score {
   \scoreAContrabassVIPart
  \layout { }
  \header {
    piece = "Pastorale ad libitum."
  }
}


\markup {
  \teeny
  \date
}