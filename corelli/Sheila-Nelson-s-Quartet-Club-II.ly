\version "2.19.81"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Sheila Nelson‘s Quartet  Club II"
  instrument = "Cello"
  composer = "Arcangelo Corelli"
  meter = "Vivace"
  copyright = "Copyright 1992, by Boosey & Hawkes Music Publishers, Ltd."
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
  %\tempo 4=100
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    c2\f d4 | % 1
    e4. d8 c4 | % 2
    d4 e4. (f8) | % 3
    g2. | % 4
    e2 e,4 | % 5
    f2 e4 | % 6
    \break
    f4 g2 | % 7
    c2. | % 8
    \breathe c2\downbow r4 | % 9
    g2 r4 | % 10
    a4 d2 | % 11
    g,2 r4 | % 12
    \break
    c2 r4 | % 13
    c2 b4 | % 14
    c4 d d | % 15
    g,4.\> a8 f4 | % 16
    e2\!\p r4 | % 17
    f'8 e f g f e | % 18
    d2.\mp | % 19
    \break
    g8 f g a g f | % 20
    e2.\mf | % 21
    a4. b8 g4 | % 22
    f4. g8 e4 | % 23
    d2 c4 | % 24
    f,4 g2 | % 25
    \break
    c'4. d8 b4 | % 26
    a4.\tenuto\p b8 g4\tenuto  | % 27
    f4. g8 e4 | % 28
    d2 c4 ~  | % 29
    c4 g'2 | % 30
    c,2.  | % 31
  }
  \break
  \repeat volta 2 {
    c8\mp b c d e fis | % 32
    g8 fis g a b gis | % 33
    a8 e a, b c d | % 34
    e8 d e fis gis e | % 35
    a2 g!4 | % 36
    \break
    f2 e4 | % 36
%    f2 e4 | % 37
    d2 c4 | % 37
    b2 a4 | % 38
    d4 e e, | % 39
    a2 g4 ~  | % 40
    g4 d'2 | % 42
    \break
    g,4 e'\p_\markup{\italic cresc.} c | % 42
    f4 g g, | % 43
    c2. | % 44
    d2. | % 45
    e2. | % 46
    f2.\f | % 47
    g2 e,4\mf | % 48
    \break
    f2 f4 | % 50
    g2 g4 | % 51
    a2\downbow d4 | % 52
    g,2\downbow f4  ~ | % 53
    f4 g2 | % 54
    c2\f\downbow   % 55
  }
    \break
    \repeat volta 2 {
      \partial 4 e,4\downbow
    f4 g2 | % 56
    a2 e'4 | % 57
    f4 g2 | % 58
    a2 e4 | % 59
    f4 g g, | % 60
    c2. | % 61
    }
  \bar "|."
}

\score {
  \new Staff \with {
    instrumentName = "Cello"
    shortInstrumentName = "Cl."
  } { \clef bass \cello }
  \layout { }
}
