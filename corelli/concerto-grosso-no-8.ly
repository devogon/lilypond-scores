\version "2.19.46"
\language "nederlands"

\header {
  title = "Concerto Grosso No. 8"
  subtitle = "(Fatto per la notte di Natale)"
  subsubtitle = "Christmas Concerto"
  instrument = "Basso"
  composer = "Arcangelo Corelli"
  arranger = "Augener"
  %meter = "Vivace"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
  \compressFullBarRests
\markLengthOn
}

contrabass = \relative c {
  \global
  \tempo "Vivace" 4=172
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
  % Music follows here.
  g4\f r r
  a4 r r
  bes4 b! r8 b
  c4 c c
  \mark \markup{\box "5"} fis2 g4\< (
  g4 ) es2\!
  d2.\fermata
  \bar "||"
  \break
  \tempo "Grave" 4 = 40
  \time 4/2
  g,2\p g'1 fis2
  g2 bes2 es,1
  \mark \markup{\box "10"} d1 es2 e!_\markup{\italic "cresc."}
  d1 g,2 g' (
  g2 ) f4 g a2. g4_\markup{\italic "dim"}
  fis4 d4 g2 c,1
  \mark \markup{\box "15"} d1\p es
  f1. fis2
  g1 c,1
  b!2 c'1\< bes!2
  as2.  as4\staccato  d,1\!
  \mark \markup{\box "20"} cis1\> d
  \override Staff.NoteHead.style = #'altdefault
  g,\breve\fermata\!
  \bar "||"
  \bar ".|:"
  \break
  \time 4/4
  \tempo "Allegro" 4 = 120
  \repeat volta 2 {
  r1
  r8 g\f bes g c a d d
  g,4 r r2
  r8 bes d bes es c f f,
  \mark \markup{\box "5"} bes4 r r2
  r2 r4 r8 d
  \break
  es8 d es c d fis g bes,
  c a d d g,4 r
  r1
  \mark \markup{\box "10"} r1
  r2 f'4\mf-> r8 d
  es8 f g d es c f f,
  \break
  bes4 r r2
  R1
  \mark \markup{\box "15"} bes8\f-> c d e! f g a fis
  g8->\< a bes c\! d->\> d, f d\!
  es8\< f g a\! bes->\> bes, d bes\!
  \break
  c8 d es c d d' es-> bes
  c8-> a d d, g4-> f->
  \mark \markup{\box "20"} es2-> d->\>
  es2\p\! d
  }
  \repeat volta 2 {
    R1
    r4 d\f g g,
    R1
    \break
    \mark \markup{\box "25"} r4 c f f,
    R1
    g'4->\mf r8 f g a bes g
    a8 cis d f, g e! a a,
    d4 r r2
    \break
  }
}

\score {
  \new Staff \with {
    instrumentName = "Contrabass"
    shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
  \header {
    piece = "PART I"
  }
}
\markup {
  \teeny
  \date
}