\version "2.22.0"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Roundabout"
  subtitle = "Yes - (Fragile - 1971)"
  instrument = "Bass"
  arranger = "bass: Chris Squire"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key e \minor
  \time 4/4
  \tempo 4=100
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

leadEcross = {e,8  \override NoteHead.style= #'cross e16 e \revert NoteHead.style}

riffOne = {  e,8  \override NoteHead.style= #'cross e16 e \revert NoteHead.style fis8   \override NoteHead.style = #'cross fis16 fis \revert NoteHead.style g8   \override NoteHead.style = #'cross g16 \revert NoteHead.style a (b)   \override NoteHead.style = #'cross b \revert NoteHead.style d (e) |}

riffOneLead = { e8)  \override NoteHead.style= #'cross e16 e \revert NoteHead.style fis8   \override NoteHead.style = #'cross fis16 fis \revert NoteHead.style g8   \override NoteHead.style = #'cross g16 \revert NoteHead.style a (b)   \override NoteHead.style = #'cross b \revert NoteHead.style d (e) |}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1
  r2 r4. e,8 ( |
  \bar "||"
  e8 ) e fis   \override NoteHead.style = #'cross fis16 fis \revert NoteHead.style g8   \override NoteHead.style = #'cross g16 \revert NoteHead.style a (b)   \override NoteHead.style = #'cross b \revert NoteHead.style d (e) |
  \repeat unfold 7 { \riffOne }
  \bar "||"
  \mark \markup{	\box "verse 1"}
  \repeat unfold 5 { \riffOne }
  \time 2/4
  e,8  \override NoteHead.style= #'cross e16 e \revert NoteHead.style g8 a (|
  \time 4/4
  a) a b4 c d |
  e4 g,8 a (a2) |
  b2 a |
  \time 2/4
  g2
  \time 4/4
  g1 (
  g2 ) ( g4.) e8 ~
  %e8 e^\markup{fix crossed notes from here on} fis fis16 fis g8 g16 a (b) b d (e) |
  \riffOneLead
  \riffOne
  e,8 e16 e fis8 fis16 fis g8 g16 g a8 b ( |
  b2) (b4.) e,8 ( |
  \bar "||"
  \mark \markup{\box "verse 2"}
  \riffOneLead
  \riffOne
  \riffOne
  \riffOne
  \riffOne
  \time 2/4
  \leadEcross g8 a ( |
  \time 4/4
  a8) a b4 c d |
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  % \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
