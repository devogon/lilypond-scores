\version "2.19.84"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "De Quarantaine - Canon"
  composer = "W.A. Mozart"
  instrument = "Fluit, Hobo, Klarinet, Cello, Bas" 
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=108
}

flute = \relative c'' {
  \global
  % Music follows here.
  d,4\mf d e e fis fis g g
  a d b g
  fis2 e
  \break
  d8 (e) fis (g) a4 b8 (cis) d4
  a b8 (a) g4 a fis g8 (fis) e4 d2 d

}

cello = \relative c {
  \global
  % Music follows here.
    d4\mf d e e fis fis g g
  a d b g
  fis2 e
  \break
  d8 (e) fis (g) a4 b8 (cis) d4
  a b8 (a) g4 a fis g8 (fis) e4 d2 d
  \bar "|."
}

flutePart = \new Staff \with {
  instrumentName = "Flute"
} \flute

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

\score {
  <<
    \new StaffGroup <<
    \flutePart
    \celloPart
                     >>
  >>
  \layout { }
}
