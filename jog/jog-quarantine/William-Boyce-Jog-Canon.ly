\version "2.19.84"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Jog - Canon"
  subtitle = "(Quarantine)"
  instrument = "Cello"
  composer = "William Boyce"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=160
}

cello = \relative c {
  \global
  \mark \markup{1}
  d4\downbow\mf d8 d d4 cis |
  cis8 cis cis cis cis4 cis |
  b4 b8 b b4 b |
  a2 a |
  \break
  b2. cis4 |
  d8 (cis b a) b (a g fis) |
  g2 a |
  d,1 |
  \break
    \mark \markup{2}
  d4\downbow d fis d |
  a' e a b8 (cis) |
  d8 cis b cis d4 e |
  cis2 a ( |
  \break
  a4) a\upbow g2 |
  fis2 d' ( |
  d4 ) b cis2 |
  d1 |
  \break
    \mark \markup{3}
  d2.\downbow fis4 |
  e8 d cis b a2 |
  a4 b gis2 |
  a2. fis4 |
  \break
  d2 e |
  d2. d'4 |
  e2 e |
  fis1
  \bar "|."
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}
