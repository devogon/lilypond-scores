\version "2.21.6"

\header {
  title = "Crazy"
  instrument = "Bass"
  composer = "Gnarls Barkley"
}

\paper {
  indent = 0.0\cm
 % page-count = 1
}

global = {
  \key es \major
  \time 4/4
  \tempo 4=112
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
  s1
  es1
  s1
  as1
  s1
  g1
  s1
  c1:m
}

electricBass = \relative c, {
  \global
  % Music follows here.
 r1
  \bar "||"
  \mark \markup{Verse 1}
  c4\3 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 g |
  \mark \markup{Verse 2 & 3}
  \repeat volta 2 {
    c4 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 c |
  \mark \markup{Chorus 1 & 2}
  c4 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 g8 g |
  \mark \markup{Post Chorus}
  c4 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 g8 g |
  }
  \mark \markup{Verse 4}
  c4 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 c |
  \mark \markup{Chorus 3}
  c4 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 g8 g |
  \mark \markup{Post Chorus}
  c4 c8 c c4 c |
  c4 c8 c c4 c |
  es4\3 es8\3 es\3 es4\3 es\3 |
  es4\3 es8\3 es\3 es4\3 es\3 |
  as,4 as8 as as4 as |
  as4 as8 as as4 as |
  g4 g8 g g4 g |
  g4 g8 g g4 g8 g |
  c4 r r2 |
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
