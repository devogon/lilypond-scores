\version "2.22.0"

date = \markup{\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Hey  Jude"
  subtitle = "Beatles - (single) - 1968"
  instrument = "Flute"
  composer = "John Lennon and Paul McCartney"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key bes \major
  \time 4/4
  \tempo "Slow rock" 4=76
}

flute = \relative c'' {
  \global
  \compressMMRests
  % Music follows here.
  R1*3 |
  r2 r4 f |
  d2 (d8) d f g |
  c,2 r4 c8 d |
  \break
  es4 bes' (bes8) bes a f | %  7
  g8 f16 (es d2) r8 f |
  g8 g4 g8 c16 bes8 a16 (a) bes g8 |
  f2 bes,8 c d g \( ( | % 10
  \break
  g) f\) r f es d (a) bes ( |
  bes ) bes (bes4) r f' |
  d2 (d8) d f g |
  c,2. c8 d |
  \break
  es4 bes' (bes8) bes a f | %
  g8 f16 (es d2) r8 f |
  g8 g r g c16 bes8 a16 (a) bes g8 |
  \break
  f2 bes,8 c d g ( | %
  f4.) f8 es d4 (a8) |
  a bes (bes2) r4 |
  r8. bes16 bes'16 as8 g16 (g8) f f es16 g ( | % 21
  \break % begin line 6 below
  g4) bes8 g (g4) bes8 es, ( |
  es4) bes'8 g (g) a es f ( |
  f4)  g8 f (f4) es8 d ( |
  d4) c8 \( bes (bes2) \) |
  \break
  r8. bes16  bes'8 g    g f   \tuplet 3/2 {f8 es g (} |
  g4) bes8 g8 (g4) bes8 es ( |
  es4) bes8 g (g) f es f ( |
  f4) g8 f (f) es4 (es16) d \( ( |
  \break
  d8.) c16\) bes4 (bes8)  bes f' g |
  as8. (g16) as4 a!8 bes c4 |
  c2 r |
  \time 2/4
  r4 f, |
  \time 4/4
  \break
  d2 (d8) d f g |
  c,2. c8 d |
  es4 bes' (bes8) bes a f |
  g8 f16 (es d2) r8 f |
  \break % page breaks here in images
  g8^\markup{next page begins} g r g c16 bes8 a16 (a) bes g8 |
  f2 bes,8 c d g ( |
  f4.) f8 es d4 (a8) |
  a bes (bes2) r4 |
  \break
  r8. bes16 bes'16 as8 g16 (g8) f f es16 g ( |
  g4) bes8 g (g4) bes8 es, ( |
  es4) bes'8 g (g) f es f ( |
  \break
  f4) g8 f (f4) es8 d ( |
  d4) es8 \(bes (bes2) \) |
  r8. bes16  bes'8 g g f \tuplet 3/2 {f8 es g (} |
  g4) bes8 g (g4) bes8 es, ( |
  \break
  es4) bes'8 g (g) f es f ( |
  f4) g8 f f es4 (es16) d \( ( |
  d8.) c16 \) bes4 (bes8) bes f' g |
  as8. (g16) as4 a!8 bes c4 |
  \break
  c2 r |
  \time 2/4
  r4 f,16 (g8.) |
  \time 4/4
  \( bes16 f g c, d4 (d8) \)d f g |
  c,2. c8 d |
  \break
  es4 bes' (bes8) bes a f |
  g8 f16 (es d2) r8 f |
  g8 g4 g8 c16 bes8 a16 (a) bes g8 |
  \break
  f4 r bes,8 c d g ( |
  f4) r8 f es d4 a8 ( |
  a) bes cis d e! f a bes |
  cis,8 d e! f bes2 |
  \break
  \repeat unfold 2 {
  bes,2 d4 f |
  c'16. bes32 c8 bes2. |
  c16. bes32 c8 bes2 as8 (g) |
  f1 |
  \break
}
  bes,2 d4 f |
  c'16. bes32 c8 bes2. |
  c16. bes32 c8 bes2 as8 (g) |
  f1\fermata |
  \bar "|."
}

\score {
  \new Staff \flute
  \layout { }
}
