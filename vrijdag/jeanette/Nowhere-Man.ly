\version "2.22.0"

date = \markup{\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Nowhere Man"
  subtitle = "Beatles - Rubber Soul - 1965"
  instrument = "Flute"
  composer = "John Lennon and Paul McCartney"
  tagline = \date
}

\paper {
  indent = 0.0\cm
  page-count = 1
}

global = {
  \key bes \major
  \time 4/4
  \tempo "Bright rock" 4=122
}

flute = \relative c'' {
  \global
  \compressMMRests
  % Music follows here.
  R1*8 |
  f4 f bes2 |
  a4 g f2 |
  es8 es4 a8 (a4) a |
  \break
  f4 es d2 |
  c8 c4 es8 (es4) es |
  d8 c4 bes8 (bes4.) c8 |
  bes8 (c4) bes8 (bes) bes4. ( |
  \break
  bes4) r4 r2 |
  f'4 f bes bes |
  a g f2 |
  es8 es4 g8 (g4) g |
  \break
  f4 es d2 |
  c8 c4 es8 (es4) es |
  d8 c4 bes8 (bes4.) c8 |
  bes8 \( c4 bes8 (bes2) \) |
  \break
  r2 d8 f4 f8 ( |
  f2.) g4 |
  bes,8 c (c4) d8 f4 f8 ( |
  f4.) f8 (f) g4. |
  \break
  bes,8 c (c4) d8 f4 f8 (|
  f2.) r8 f |
  bes4 (a8 g f4.) f8 |
  g4 f8 es f2 ( |
  \break
  f4) r4 r2 |
  f4 f bes2 |
  a4 g f2 |
  es8 es4 a8 (a4) a |
  \break
  f4 es d2 |
  c8 c4 es8 (es4) es |
  d8 c4 bes8 (bes4.) c8 |
  bes8 (c4) bes8 (bes) bes4. ( |
  \break
  bes4) r4 r2 |
  f'4 f bes bes |
  a g f2 |
  es8 es4 g8 (g4) g |
  \break
  f4 es d2 |
  c8 c4 es8 (es4) es |
  d8 c4 bes8 (bes4.) c8 |
  bes8 \( c4 bes8 (bes2) \) |
  \break
   r2 d8 f4 f8 ( |
  f2.) g4 |
  bes,8 c (c4) d8 f4 f8 ( |
  f4.) f8 (f) g4. |
  \break
  bes,8 c (c4) d8 f4 f8 (|
  f2.) r8 f |
  bes4 (a8 g f4.) f8 |
  g4 f8 es f2 ( |
  \break
  f4) r4 r2 |
  f4 f bes2 |
  a4 g f2 |
  es8 es4 g8 (g4) g |
  \break
  f4 es d2 |
  c8 c4 es8 (es4) es |
  d8 c4 bes8 (bes4.) c8 |
  bes8 (c4) bes8 (bes) bes4. ( |
  \break
  bes4) r4 r2 |
  c8 c4 es8 (es4) es |
  d8 c4 bes8 (bes4.) c8 |
  c8 bes bes2.\fermata
  \bar "|."
}

\score {
  \new Staff \flute
  \layout { }
}
