\version "2.22.0"

date = \markup{\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Day Tripper"
  subtitle = "Beatles - Rubber Soul - 1965"
  instrument = "Flute"
  composer = "John Lennon and Paul McCartney"
  tagline = \date
}

\paper {
  page-count = 1
  indent = 0.0\cm
}

global = {
  \key bes \major
  \time 4/4
  \tempo "Medium rock" 4=132
}

flute = \relative c'' {
  \global
  % Music follows here.
  \compressMMRests
  R1*4
  es'8 d c bes (bes) bes4. |
  r2 r4 r8 bes |
  es8 d c bes (bes) d, d d ( |
  \break
  d4) r r2 |
  es'8 d c bes (bes) es,4. |
  r2 r4 r8 bes' |
  es8 d c bes (bes) d, d d ( |
  \break
  d8) d4. r8 f as f |
  c'2 \( (c8) bes c4\) |
  bes8 c4. r2 |
  d8 c4 bes8 (bes) c4 bes8 ( |
  \break
  bes4 ) r4 r8 g bes g |
  d'8 \( c4. (c8) bes c \) bes \( (|
  bes) a4 \) r8 r d a b! ( |
  b4) r r8. b!16 b8 b |
  \break
  a4 r4 r2 |
    es'8 d c bes (bes) bes4. |
 r2 r4 r8 bes |
  es8 d c bes (bes) d, d d ( |
  \break
  d4) r r2 |
  es'8 d c bes (bes) es,4. |
  r2 r4 r8 bes' |
  es8 d c bes (bes) d, d d ( |
  \break
  d8) d4. r8 f as f |
   c'2 \( (c8) bes c4\) |
  bes8 c4. r2 |
  d8 c4 bes8 (bes) c4 bes8 ( |
  \break
  bes4) r4 r8 g bes g |
  d'8 \( c4. (c8) bes c \) bes \( (|
  bes) a4 \) r8 r d a b! ( |
  b4) r r8. b!16 b8 b |
  \break
  a4 r r2 |
  a1 |
  bes1 |
  c1 |
  bes1 |
  \break
  a1 |
  bes1 |
  es8 d c bes (bes) bes4. |
  r2 r4 r8 bes |
  \break
  es8 d c bes (bes) d, d d ( |
  d4) r r2 |
  es'8 d c bes (bes) es,4. |
  r2 r4 r8 bes' |
  \break
  es8 d c bes (bes) d, d d ( |
  d8) d4. r8 f as f |
   c'2 \( (c8) bes c4 \) |
  bes8 c4. r2 |
  \break
  d8 c4 bes8 (bes) c4 bes8 ( |
  bes4) r4 r8 g bes g |
  d'8 \( c4. (c8) bes c\) bes \( (|
  bes) a4\) r8 r d a b! ( |
  \break
  b4) r4 r8. b!16 bes8 bes |
  a4 r r2 |
  r4 d4 c8 d4 r8 |
  r1 |
  r4 d4 c8 d r4 |
  \break
  r1 |
  r4 d4 c8 d4 r8 |
  r1
  r4 d4 c8 d8 r4 |
  bes4 r r2 |
  \bar "|."
}

\score {
  \new Staff \flute
  \layout { }
}
