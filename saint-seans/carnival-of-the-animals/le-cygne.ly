\version "2.19.80"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "le Cygne"
  subtitle = "from Carnival of the Animals"
  instrument = "Violoncello"
  composer = "Camille Saint-Saëns"
  meter = "Adagio"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \numericTimeSignature
  \time 6/4
 % \tempo "Adagio" 4=100
}

cello = \relative c'' {
  \global
  % Music follows here.
  \partial 1 r1
  g4\downbow\p-4 (fis b,-1) e-4 (d g,-2) | % 2
  a2-1 ~ (a8 b) c2 r4 | % 3
  e,2-1 fis8-3 (g-4) a-0 b c d-4 e-1 fis-3 | % 4
  \break
  b2.-3 ~ b8 r r4 r | % 5
  g4 (fis b,) e (d g,) | % 6
  ais2-1 ~ ( ais8 b) cis2.-4 | % 7
  fis,4.-3 gis8-1 ais b cis-1 d e fis-1 gis ais-1 | % 8
  \break
  d2.-3 ~ d8 r r4 r4 | % 9
  d4-3 (b-1 g-4) e (fis g) | % 10
  d2-1 ~ (d8 e) fis2-4 r4 | % 11
  c'4-2 (a f!-4) d ( e f) | % 12
  \break
  c2-1 ~ (c8 d) e2-4 r4 | % 13
  e4\< (a,-0 b) c2 d8-1 e | % 14
  fis2.-4\!\> (e2\!) r4 | % 15
  e4\downbow\< (a,-0 b) cis2 d8-4 e\!-1) | % 16
  \break
  f!2.-2\> fis2. | % 17
  g4\!\p\downbow (fis b,) e4 (d g,) | % 18
  a2 ~  (a8 b) c2 r4 | % 19
  e,2 fis8 (g) a b c d e fis | % 20
  \break
  b1.\downbow\< | % 21
  b4\!\downbow\mf (a e-1) g-4 (f-3 c-1) | % 22
  e\downbow\<-4 (d) g,\upbow a (b) g-4\! | % 23
  a2.->-1 c4_\markup{\italic dim.} (d b) | % 24
  \break
  e2.^\markup{\italic rit.}-2 \tempo "Lento" e4\upbow ( fis d) | % 25
  g1.-4\pp ~ | % 26
  g2. ~ g8 r r4 r4 | % 27
  \partial 1 r1\fermata | %
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Cello"
%    shortInstrumentName = "Cl."
  } { \clef tenor \cello }
  \layout { }
}
