\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Danse Macabre"
  instrument = "Contrabas"
  composer = "Camille Saint-Saëns"
  opus = "Op. 40"
  copyright = "1874"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \minor
  \numericTimeSignature
  \time 3/4
  \tempo 2.= 65
}

contrabass = \relative c {
  \global
  % Music follows here.
  d'2.\p^\markup{pizz.} |
  \repeat unfold 11 {d2.}
  \repeat unfold 5 {r1*3/4}
  d,4^\markup{pizz.} r2
  a'4 r2 |
  c,4 r2 |
  fis4 r2 |
  a,4 r2 |
  c4 r2 |
  es4 r2 |
  \bar "||"
  d4 r2 |
  \repeat unfold 5 {r1*3/4}
  % \break
  \repeat unfold 2 {r1*3/4}
  \bar "||"
  g,4\p g' d |
  \repeat unfold 3 {g,4 g' d} |
  f, as' es |
  f, as' es |
  % \break
  f, as' es |
  f, as' es |
  \repeat unfold 4 {g, g' d}
  % \break
  \repeat unfold 4 {g, g' d}
  g, g' d\mf |
  \bar "||"
  g,4 r2
  g4 r2
  % \break
  \repeat unfold 3 {g4 r2}
  a4 r2
  a4 r2
  d4 r2
  \repeat unfold 4 {g,4 r2}
  % \break
  g4 r2 |
  a4 r2
  a4 r a |
  \repeat unfold 4 {d4 r r} |
  % \break
  \repeat unfold 4 {c4 r r} |
  \repeat unfold 3 {d4 r r} |
  % \break
  d4 r r |
  \repeat unfold 4 {c4 r r} |
  \repeat unfold 3 {r1*3/4}
  % \break
  r4 r d\staccato\sfz
  \bar "||"
  \repeat unfold 4 {g4\staccato r r}
  f4 r r |
  % \break
  \repeat unfold 3 {f4\staccato r r }
  \repeat unfold 3 {g4\staccato r r }
  % \break
  g4\staccato r r
  \repeat unfold 4 {f4\staccato r r }
  g\staccato r d
  \bar "||"
  g,4 r r
  g4 r r |
  % \break
  \repeat unfold 3 {g4\staccato r r }
  \repeat unfold 2 {a4\staccato r r }
  d4\staccato r r
  \repeat unfold 4 {g,4\staccato r r }
  % \break
  g4\staccato r r
  a\staccato r r
  r a\staccato a\staccato
  \repeat unfold 4 {d4\staccato r r }
  % \break
  \repeat unfold 4 {c4\staccato r r }
  \repeat unfold 3 {d4\staccato r r }
  % \break
  d4\staccato r r
  \repeat unfold 4 {c4\staccato r r }
  \repeat unfold 3 {r1*3/4}
  % \break
  r4 r d4\staccato\sfz
  g,4\staccato\mf^\markup{arco} r d'\staccato
  \bar "||"
  g\staccato r g\staccato
  f\staccato r f\staccato
  e!\staccato r e\staccato
  es\staccato r es\staccato
  d\staccato e!\staccato g\staccato
  f\staccato e!\staccato d\staccato
  e!\staccato r e\staccato
  % \break
  f4 r2
  a4 r2
  d,4 r2
  g4 r2
  f4 r2
  % \break
  a4 r2
  fis4 r2
  g4 r2
  d4 r2
  g4 r2
  % \break
  c,4 r2
  g'4 r2
  d4 r2
  cis4 r2
  d4 r2
  % \break
  a8\staccato a\staccato a4\staccato r
  g8\staccato g\staccato g4\staccato r
  g'8\staccato g\staccato g4\staccato r
  d8\staccato d\staccato d4\staccato r
  e!8\staccato e\staccato e4\staccato r
  % \break
  d8\staccato d\staccato d4\staccato r
  g4 r g
  f r f
  e! r e
  es r es
  % \break
  d4\< e! g
  f e! d
  f2 (f4\!
  e!8) r^\markup{pizz.} r4 e,! |
  e!4 r e
  % \break
  \repeat unfold 4 {e!4 r e}
  e! e e
  % \break
  \repeat unfold 5 {e! r e}
  % \break
  e! r e
  e! r e
  e! e e
  e! r e
  a4 r a
  a r a
  % \break
  \repeat unfold 7 {a r a}
  % \break
  \repeat unfold 6 {a r a}
  a r a^\markup{arco}
  % \break
  \key b \major
  b'2.\p ~ b2. ~ b2. ~ b2.
  b2. ~ b2. ~ b2. ~ b2.
  b2. ~ b2.
  b2. ~ b2.
  b2. ~ % \break  b2. ~ b2. ~ b2.
  b2. ~ b2. ~ b2. ~ b2.
  b2. ~ b2.
  b2. ~ b2.
  b2. ~ % \break b2. ~ b2. ~ b2.
  b2. ~ b2.
  b2. ~ b2.
  r4 b,8\staccato\mf dis\staccato b\staccato cis\staccato
  dis4\staccato cis\staccato b\staccato
  r4 g!8\staccato b\staccato g\staccato a\staccato
  % \break
  b4\staccato a!\staccato g!\staccato
  r4 b8\staccato dis\staccato b\staccato cis\staccato
  dis4\staccato cis\staccato b\staccato
  r4 g!8\staccato b\staccato g\staccato a\staccato
  b4\staccato a!\staccato g!\staccato
  b4\staccato\p r2
  b4\staccato r2
  % \break
  bes4\staccato\mp r2
  bes4\staccato  r2
  a!4\staccato r2
  a!4\staccato r2
  as4\staccato\mf r2
  % \break
  as4\staccato r2
  \bar "||"
  \key g \minor
  cis2\upbow\f cis4
  r1*3/4
  cis2\upbow cis4
  r1*3/4
  b!2\upbow b4
  r1*3/4
  a2\upbow a4
  r1*3/4
  cis'2\>\mf cis4
  bes2 bes4
  % \break
  a2 a4
  g2 g4
  fis2 fis4
  g2 g4\!
  fis2\p fis4
  g2 g4
  c,2 c4
  bes2 bes4
  a2 a4
  g2 g4
  \repeat volta 2 {
    d4\pp\< 	r r
    % \break
    d' r r
    d r r
    d r r\!
    r1*3/4
    bes'4\staccato\p^\markup{pizz.} r2
    % \break
    g4\staccato r2
    es4\staccato r2
  }
  d4\staccato r2
  \bar "||"
  d4\staccato^\markup{arco}
  \repeat unfold 8 {d\staccato}
  % \break
  d-. d-. d-.
  \repeat unfold 12 {a-.}
  % \break
  \repeat unfold 12 {g-.}
  a-. a-. a-.
  % \break
  \repeat unfold 9 {a-.}
  \bar "||"
  d2.\pp\trill ~ d2. ~ d2. ~ d2. ~ d2. ~
  % \break
  d2. ~ d2. ~ d2. ~ d2.
  \bar "||"
  \repeat unfold 4 {g4\staccato g8\staccato g g g\staccato}
  % \break
    \repeat unfold 2 {g4 g8\staccato g g g\staccato}
    g4\staccato fis8\staccato fis fis fis\staccato
    e4\staccato a8\staccato a a a\staccato
    d,4\staccato g8\staccato g g g\staccato
    g4\staccato g8\staccato g g g\staccato
    % \break
    \repeat unfold 4 {g4\staccato g8\staccato g g g\staccato}
    a4\staccato fis8\staccato fis fis fis\staccato
    % \break
    fis4\staccato fis8\staccato fis e! e\staccato
    d4\mf d8\staccato d d d\staccato
    \repeat unfold 3 {d4 d8\staccato d d d\staccato}
    % \break
    \repeat unfold 4 {d4 d8\staccato d d d\staccato}
    d4\staccato r2
    bes'4\f bes r
    % \break
    c4 c r
    fis, fis r
    g g r
    c, c r
    % \break
    d d r
    d d r
    r g r
    bes bes r
    c c r
    % \break
    fis, fis r
    g g r
    c, c r
    d d r
    d d r
    % \break
    \repeat unfold 5 {d d r}
    d a' r
    % \break
    a\ff bes c
    f, a bes
    es, g as
    c8 c c c c4
    d8 d d d d4
    g, bes c
    a f bes
    g es as
    % \break
    c,8 c c c c4
    d8 d d d d4
    g,8 bes g a bes4
    c8 b! bes a as4
    g8 bes g a bes4
    c8 b! bes a as4
    % \break
    \repeat unfold 3 {g8 bes g a bes4}
    g8\fff bes g a bes g
    a8 bes g a bes g
    % \break
    a8 bes g a bes g
    a8-> bes-> g-> a-> bes-> g->
    a8-> bes-> g-> a-> bes-> g->
    es'2.\pp ~ es2.\>  ~
    \bar "||"
     es2. ~ es2. ~ es2. ~ es2. ~
    % \break
    es2.\!
    es2.\f\> ~ es2. ~ es2. ~ es2. ~ es2. ~
    es2.\!\p\> ~ es2. ~ es2. ~ es2.\! ~ es2.\pp\fermata
    \bar "||"
    \tempo "Andante"
    g,2.^\markup{arco} ~ g2. ~ g2. ~
    % \break
    g2. ~ g2. ~ g2. ~ g2. ~ g2.
    as2. ~
    as4 r r
    r4 r2
    r4 r2
    r4 \tempo "A tempo" r2
    r4 r2
    r4 g8\staccato^\markup{pizz.} bes a bes
    % \break
    a'8 g g-. a-. bes-. g-.
    d'4-. r r
    r4 r2
    r4 r g,,^\markup{pizz.}
    a bes g
    d' r r\pp
    d r r
    g, r2
    \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
