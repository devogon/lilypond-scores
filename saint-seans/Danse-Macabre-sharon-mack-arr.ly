\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Danse Macabre"
  instrument = "Bass"
  composer = "Camille Saint-Saëns"
  arranger = "Sharon Mack"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \minor
  \numericTimeSignature
  \time 6/8
  \tempo "Adagio"
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative c {
  \global
  % Music follows here.
  R1*6/8*2
  r4. r8 r d\p^\markup{pizz.} | % 3
  a' c, g' a, c es |
  \tempo "Allegro"
  d8 r r r4. |
  R1*6/8*2
  r4. r8 r d
  \break
  g4 r8 g4 r8 | % 9
  g4 r8 g4 r8 | %
  f4 r8 f4 r8 | %
  f4 r8 f4 r8 | %
  g4^\markup{arco}\mf r8 g4 r8 | %
  g4 r8 g4 r8 | %
  f4 r8 f4 r8 | %
  \break
  f4 r8 f4 r8 | % 16
  \time 3/8
  g4 r8
  \time 6/8
  g4^\markup{pizz} r8 g4 r8 | % 18
  g4 r8 g4 r8 | %
  g4 r8 a4 r8 | %
  a4 r8 d,4 r8 | %
  g4 r8 g4 r8 | %
  \break
  g4 r8 g4 r8 | % 23
  g4 r8 a4 r8
  \time 3/8
  a4 r8 | %
  \time 6/8
  d,4 r2 | %
  r1*6/8
  es4 r8 es4 r8 | %
  es4 r8 es4 r8 | %
  d4 r8 r4. | %
  \break
  r1*6/8 | % 31
  es4 r8 es4 r8 | %
  es4 r8 es4 r8 | %
  d4 r8 r4. |
  r4. r8 r8 d^\markup{arco}
  \repeat volta 2 {
    g4 r8 g4 r8 |
    g4 r8 g4 r8 |
    \break
    f4 r8 f4 r8 |
    f4 r8 f4 r8 |
  }
  \time 3/8
  g4 r8
  \time 6/8
  \repeat unfold 5 {g4 r8}
  a4 r8 a4 r8 d,4 r8
  \break
  \repeat unfold 5 {g4 r8}
  a4 r8
  \time 3/8
  a4 r8
  \time 6/8
  d,4 r8 d4 r8
  4 r8 d4 r8
  \break
  es4 r8 r4. | % 51
  r1*6/8
  d4 r8 d r r |
  d4 r8 d4 r8
  es4 r8 r4.
  R1*6/8*2
  \break
  r4. r8 r d\mp\upbow | % 58
  g16-> g g8 g f16-> f f8 f
  e!16-> e e8 e es16-> es es8 es |
  d16 d e! e g g f f e e d d |
  \time 3/8
  e!8 a a, | % 62
  \time 6/8
  \break
  d16 f e! f e d a'8. a,16 bes c | % 63
  d16 f e! f e d g8. g16 f e
  f16 e! f g f g a8. f16 g a
  \time 3/8
  g16\< f e! f e d
  \time 6/8 g4\!\mf r8 r4. |
  \break
  R1*6/8*6 | % 68+
  \time 3/8
  r4 d8 (
  \time 6/8
  g4\f) g8 ( f4) f8 (
  e!4) e8 (es4) es8 (
  d e! g) f e d
  f4 f8 e!4 r8
  \break
  e!4\p r8 e4 r8
  \repeat unfold 4 {e!4 r8}
  e4\< r8
  \repeat unfold 7 {e!4 r8}
  \break
  a4 r8 a4r8\!
  a4\mp r8
  \repeat unfold 7 {a4 r8}
  a4\< r8 a4 r8
  a4 r8 a4 r8
  \break
  a4 r8 a4 r8\!
  a4\f ais8 b!4 r8
  b!2.\p ~ b4.
  g4. ~ g2. g4. b!4.
  R1*6/8*8
  b!2.\mf ~
  \break
  b4. e,!4. ~
  e!2. ~
  e!4. b'!4.
  r1*6/8
  r4. r8 b!16\mf dis b cis |
  dis8 cis b! r g16 b g a |
  b!8 a g r4. |
  \break
  d2.\p | % 115
  d8 d d d4._\markup{cresc poco a poco} ~
  d4. d8 d d
  d2. d2. d2.\mf\<
  d'4 d8 d4.\~
  d4\f d8 d4 d8
  d4 d8 d4 d8
  \break
  d4. d4.| % 124
  d4. d4.
  d8\mp\< d16 d d d d8 d d |
  d8 d16 d d d d8 d d\! |
  g,4\ff r8 r4.
  r1*6/8
  \break
  g8 g16 g g g a8 a16 a a a | % 130
  a8 a16 a a a d,8 d'16 c bes a |
  g4 r8 r4. |
  r1*6/8 |
  g8 g16 g g g a8 a16 a a a |
  \break
  a8 a16 a a a d8\mf d16 d d d | % 135
  des8-> r r des-> r r |
  des-> r r d!-> r r |
  d-> r r d-> r r |
  g,8-> g r bes-> bes r |
  \break
  c8-> c r fis,-> fis r | % 140
  g8-> g r d-> d r |
  g8-> g r bes-> bes r |
  c8-> c r fis,-> fis r |
  g8-> g r d-> d r |
  g8-> g r d->\< d r
  \break
  g8-> g r d-> d r | % 146
  d8-> d r d-> d r\!
  g8\ff bes c f, a bes |
  es, g a c-> c r |
  d,8-> d r g-> g r
  g bes c f, a bes
  \break
  es,8 g a c-> c r | % 152
  \time 3/8
  d,8-> d r
  \time 6/8
  g8 a bes c bes as
  g a bes c bes as
  g8 g bes g g bes
  g g bes g g bes
  \break
  \repeat unfold 12 {g8} % 158
  \tempo "Andante"
  es8\p r8 r r4.
  g2.:32\sfz\> ~ g2.:32
  g4\! r8 r4.
  \break
  R1*6/8*4  % 164
  r1*6/8^\markup{\bold "molto rit."}
  r8 g16\pp bes^\markup{a tempo} g a bes8 a g |
  R1*6/8*2
  r4. d'8^\markup{pizz} r r
  g,8 r r r4.
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
