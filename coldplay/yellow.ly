\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Yellow"
  subtitle = "Coldplay"
  subsubtitle = "Parachutes (2000)"
%  poet = "poet"
  arranger = "arr: T. Kenrick 2009"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


global = {
  \key b \major
  \time 4/4
  \tempo 4=86
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
   \mark \markup{Intro}
   r1*4
   \break
  \bar "||"
  % \break
  \mark \markup{Chorus}
  b8\segno
  \repeat unfold 15 {b8}
  \repeat unfold 16 {fis8}
  % \break
  \repeat unfold 16 {e}
  \repeat unfold 16 {b'8}
  % \break
  \repeat volta 3 {
    \mark \markup{Verse - x3}
    \break
      \repeat unfold 16 {b8}
      \repeat unfold 16 {fis'8}
      \repeat unfold 16 {e8}
  }
   \break
  \repeat unfold 16 {b'8}
  % \break
  \mark \markup{Chorus}
  e,8 e e fis ~ fis fis b4 |
  gis4. fis8 ~ fis fis fis fis |
  e8 e e fis ~ fis fis b4 |
  gis4. fis8 ~ fis fis fis fis |
  e8 e e fis ~ fis fis b4 |
  gis4. fis8 ~ fis fis fis fis |
  e1 ~
  e1^\markup{D.S.}
  \break
  \repeat volta 2 {\mark \markup{Bridge}
    \repeat unfold 16 {b8}
    \repeat unfold 16 {fis8}
    \repeat unfold 16 {e8}
    \repeat unfold 16 {b'8}
  }
    \repeat unfold 16 {fis8}
    \repeat unfold 16 {e8}
    b'1\fermata
    \bar "|."
}


verseone = \lyricmode  {
look _ at _ the _ stars _ _ _
look how they shine for you _ _ _ _ _ _ _ _ _ _
and all the things you do,
yeah, they were all yellow _ _ _ _ _ _ _ _ _ _ _
i came along,
i wrote a song for _ _ _ _ _ you
and all the things you do
and it was called "yellow"
so then i t ook my turn
oh what a thing to have done
and it was all yellow
%}
%chorusone = \lyricmode {
your skin,
o yeah, your skin and bones
turn into something beautiful
you know, you know i love you so,
you know i love you so.
%}
%versetwo = \lyricmode {
i swam across
i jumped across for you,
oh, what a thing to do
'cause you were all yellow
i drew a line,
i drew a line for you,
oh, what a thing to do
and it was all yellow.
%}

%chorustwo = \lyricmode {
your skin
oh yeah, your skin and bones
turn into somethnig beautiful,
and you know
for you i bleed myself dry,
for you I bleed myself dry.
%}

%mycoda = \lyricmode {
it's true, look how they shine for youu
look how they shine for you
look how they shine for,
look how they shine for  you,
look how they shine for you,
look how they shine.
look at the stars,
look how they shine for you
and all the things they do.
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
%  } { \clef bass \contrabas \addlyrics \verseone}
    } { \clef bass \contrabas }
  \layout { }
}
