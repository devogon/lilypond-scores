\version "2.19.81"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Newe Padouan, Intrada,
  Dantz und Galliarda"
  instrument = "violoncello"
  composer = "Paul Peuerl"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 2/2
  \tempo 4=100
    \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

xviipadouan = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    g'2 d4 d | % 1
    g,2. a4 | % 2
    bes2. c4 | % 3
    d4 bes f'2 | % 4
    bes,2. g4 | % 5
    d'2. bes4 | % 6
    a2. g4 | % 7
    \break
    g'2. f8 e | % 8
    d4 d a'2 | % 9
    d,2 r4 d | % 10
    g4 g, d' bes | % 11
    es4 c d g, | % 12
    d'1 | %
    g,1 | %
  }
  \break
  \repeat volta 2 {
    R1 | % 15
    g'4. f8 es2 | % 16
    R1 | %
    f4. es8 d2 | %
    R1 | %
    es4. d8 c2 | %
    \break
    r2 r4 bes | % 21
    f'8 es d es f es d c | % 22
    bes2 bes8 c d es | % 23
    f4 es8 d cis4 d | % 24
    a'2. a4 | % 25
    d,1 | %
  }
  \break
  \repeat volta 2 {
    g4 g, d' g, | % 27
    bes8 c d4 a' a, | % 28
    d1 | %
    g4 g, d' g, | %
    bes8 c d4 a' a, | %
    d2. d4 | %
    \break
    c2 f | % 33
    d2 bes | % 34
    es2 bes | % 35
    f'4 g c, f | % 36
    \break
    bes,4 es c f | % 37
    d1 | %
  }
  \alternative {
    {g,1}
    {g\longa}
  }
}

xviiiintrada = \relative c {
  \global
  \time 3/2
  % Music follows here.
  \repeat volta 2 {
    r2 r d | % 1
    g,1 bes2 | % 2
    es1 bes2 | %
    f'1 f,2 | %
    bes1 c2 | %
  %  \break
    d1 es2 | %
    d2. c4 bes2 | %
    f'1 f,2 | %
    bes1 bes2 | %
  %  \break
    bes1 f2 | %
    g1 g2 | %
    d'1. | %
    d1
  }
 % \break
  \repeat volta 2 {
    \partial 2 d2 |
    g1 g2 | %
    c,1 c2 | %
    f1 f4 e | %
  %  \break
    d2. c4 bes2 | %
    f'1 bes,2 | %
    f'1 f,2 | %
    bes1. bes
  }
%  \break
  \repeat volta 2 {
    \partial 2 bes2 | %
    g2. a4 bes c | %
    d1 g,2 | %
    bes2. c4 d2 | %
    a1 d2 | %
  %  \break
    c2. d4 es2 | %
    d1 d2 | %
    g,1. | %
  }
  \alternative {
    {g1}
    {g\longa}
  }
  \bar "|."
}

xixdantz = \relative c {
  \global
  \time 2/2
  % Music follows here.
  r2 r4 g' | % 1

}

xxgalliarda = \relative c {
  \global
  \time 3/2
  % Music follows here.

}

\score {
  \new Staff { \clef bass \xviipadouan }
  \layout { } \header {piece = "XVII Padouan"}
}

\score {
  \new Staff { \clef bass \xviiiintrada }
  \layout { } \header {piece = "XVIII Intrada"}
}

\score {
  \new Staff { \clef bass \xixdantz }
  \layout { } \header {piece = "XIX Dantz"}
}

\score {
  \new Staff { \clef bass \xxgalliarda }
  \layout { } \header {piece = "XX Galliarda"}
}
