\version "2.19.55"
\language "nederlands"

\header {
  title = "Pavan: The Earl of Salisbury"
  subtitle = "(Four Pieces for Four Cellos)"
  instrument = "Cello"
  composer = "William Byrd"
  arranger = "arr. by Doreen Smith"
  meter = "Majestico"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key f \major
  \time 2/2
  \tempo 2=60
}

scoreACelloI = \relative c' {
  \global
  % Music follows here.
  r4 d2\downbow f4 | % 1
  e2. a,4 | % 2
  b!8. (c16 d2) cis4 | % 3
  d2. d4 | % 4
  f2 (f8) f (e d ) | % 5
  e2 (e8) e (d c) | % 6
  d2. d4 | % 7
  cis2._\markup{\italic dim.} r4 | % 8
  r4 d2\downbow\p (f4) | % 9
  e2. a,4 ( | % 10
  b!8.  c16) d2 ( cis4) | % 11
  d2. d4 | % 12
  f2 (f8) f (e d) | % 13
  e2 (e8) e (d c) | % 14
  d2. d4 | % 15
  cis2. e,4 | % 16
  f2_\markup{\italic cresc.} g2 | % 17
  a4. (g8 f4) c'4 | % 18
  f2 g^\markup{g} | % 19
  \break
  a4. (g8) f4 a-> ( | % 20
  a4) g4. ( a8) f4 | % 21
  e4. (d8) cis (d) e (f) | % 22
  e8 (d) d2 (cis4) | % 23
  d2. e,4\mf | % 24
  f2_\markup{\italic cresc.} g2 | % 25
  a4. (g8 f4) c' | % 26
  f2 g | % 27
  a4. (g8) f4 a ( | % 28
  a4) g4. ( a8) f4 | % 29
  e4. (d8) cis (d) e (f) | % 30
  e8 (d)_\markup{\italic dim. e rit} d2 ( cis4) | % 31
  d1\fermata | % 32
  \bar "|."
}

scoreACelloII = \relative c {
  \global
  % Music follows here.
  <f a>1\downbow\f | % 1
  a1 | % 2
  d,2 e | % 3
  a,4. (d8) a2 | % 4
  r4 a'4\downbow bes2 ( | % 5
  bes8) bes (a g) a2 ( | % 6
  a8) a (g f) g2 | % 7
  a2._\markup{\italic dim.} r4 | % 8
  <f a>1\downbow\p | % 9
  a1 | % 10
  d,2 e | % 11
  a,4. (d8) a2 | % 12
  r4 a'4\downbow_\markup{\italic poco cresc.} bes2 ( | % 13
  bes8) bes (a g) a2 (| % 14
  a8) a ( g f) g2 | % 15
  \break
  a2. r4 | % 16
  r4 d,2\downbow_\markup{\italic cresc.} e4 | % 17
  f2. a4 ( | % 18
  a4) d2 e4 | % 19
  f2 d4 f4 | % 20
  e2. d4 | % 21
  cis4 a2 r4 | % 22
  r2 a4.\upbow g8 | % 23
  fis2. r4 | % 24
  r4 d2\mf\downbow_\markup{\italic cresc.} e4 | % 25
  f2. a4 ( | % 26
  a4) d2 e4 | % 27
  f2 d4 f4\ff | % 28
  e2. d4 | % 29
  cis4 a2 r4 | % 30
  r2 a4.\upbow\mp_\markup{\italic dim. e rit.} (g8) | % 31
  fis1\fermata | % 32
  \bar "|."
}

scoreACelloIII = \relative c {
  \global
  % Music follows here.
  d4.\f d8 a4 d ( | % 1
  d4) cis8 b! cis (d) e f | % 2
  g4 d g2 | % 3
  fis1 | % 4
  d1 | % 5
  c2 e | % 6
  f2 (f8) f (e d) | % 7
  c2. r4 | % 8
  d4.\p (d8\tenuto) a4 d ( | % 9
  d4) cis8 (b!) (cis d e f) | % 10
  g4 (d) g2 | % 11
  fis1 | % 12
  d1_\markup{\italic poco cresc.} | % 13
  c2 e | % 14
  f2 (f8) (f e d) | % 15
  e2. r4 | % 16
  r1 | % 17
  c1_\markup{\italic cresc.}\downbow | % 18
  r8 f\upbow bes4. g8 c4 | % 19
  c2 a | % 20
  <a e>2. r4 | % 21
  r4 a2\downbow g4 (| % 22
  g8 a) f4 e2 | % 23
  d2. r4 | % 24
  r1 | % 25
  c1\mf\downbow_\markup{\italic cresc.}| % 26
  r8 f\upbow bes4. g8 c4 | % 27
  <c f,>2 <a d,>| % 28
  <a e>2.\ff r4 | % 29
  r4 a2\downbow g4 ( | % 30
  g8) a f2_\markup{\italic dim. e rit} e4 | % 31
  d1\fermata | % 32
  \bar "|."
}

scoreACelloIV = \relative c {
  \global
  % Music follows here.
  <a d>1\f | % 1
  a2 a  | % 2
  g4. (f8) e2 | % 3
  d'1 | % 4
  d1 | % 5
  c1 | % 6
  bes1 | % 7
  a4._\markup{\italic dim.} e'8 (a,4) a | % 8
  <a d>1\p | % 9
  a2 a | % 10
  g4. (f8) e2 | % 11
  d'1 | % 12
  d1_\markup{\italic poco cresc.} | % 13
  c1 | % 14
  bes1 | % 15
  a4. e'8 (a,4) a (| % 16
  a8)_\markup{\italic cresc.} f bes4. g8 c4 | % 17
  f,1\downbow | % 18
  r8 f\upbow bes4. g8 c4 | % 19
  f,2 d' | % 20
  cis2. d4 | % 21
  a1\upbow ( | % 22
  a2) a | % 23
  <a d>2. a4\mf ( | % 24
  a8) f_\markup{\italic cresc.} bes4. g8 c4 | % 25
  f,1 | % 26
  r8 f\upbow bes4. g8 c4 | % 27
  f,2 d' | % 28
  cis2. d4 | % 29
  f,1\upbow ( | % 30
  a2)_\markup{\italic dim. e rit.} a | % 31
  <a d>1\fermata
  \bar "|."
}

scoreACelloIPart = \new Staff \with {
  instrumentName = "I"
  shortInstrumentName = "I"
} { \clef bass \scoreACelloI }

scoreACelloIIPart = \new Staff \with {
  instrumentName = "II"
  shortInstrumentName = "II"
} { \clef bass \scoreACelloII }

scoreACelloIIIPart = \new Staff \with {
  instrumentName = "III"
  shortInstrumentName = "III"
} { \clef bass \scoreACelloIII }

scoreACelloIVPart = \new Staff \with {
  instrumentName = "IV"
  shortInstrumentName = "IV"
} { \clef bass \scoreACelloIV }

\score {
 \new StaffGroup
  <<
   \scoreACelloIPart
   \scoreACelloIIPart
   \scoreACelloIIIPart
   \scoreACelloIVPart
  >>
  \layout { }
}

\markup{
  \teeny
  \date
}