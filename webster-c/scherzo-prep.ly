\version "2.19.57"

\language "nederlands"

\header {
  title = "Scherzo"
  subtitle = "preperation for..."
  composer = "C. Webster"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key d \major
  \time 2/4
  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
  d2
  e
  fis
  g
  a
  b
  cis-2
  \clef treble
  \break
  d2-4
  e2-1
  fis-4
  g
  a-1
  b-3
  cis-2
  \break
  d-3
  cis-2
  b-3
  a-1
  g
  fis-4
  e-1
  \break
  d-4
  \clef bass
  cis-2
  b
  a
  g
  fis
  e
  d
  \bar "||"
  \break
  d4\downbow^\markup{var 1} e
  fis g
  a b
  cis d_\markup{etc.}
  \bar "||"
  \break
  d,8\downbow^\markup{var 2} d e e
  fis fis g g
  a a b b
  cis cis d d_\markup{etc.}
  \bar "||"
  \break
  d,16\downbow^\markup{var 3} d d d e e e e |
  fis fis fis fis g g g g |
  a a a a b b b b |
  cis cis cis cis d d d d_\markup{etc.} |
  \bar "||"
  \break
  d,16\downbow^\markup{var 4} d  e e fis fis g g |
  a a b b cis cis d d_\markup{etc.} |
  \bar "|."
    }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
  \hspace #1
}

\markup {
  \teeny
  \date
}