\version "2.19.57"

\language "nederlands"

\header {
  title = "Scherzo"
  composer = "C. Webster"
 % poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key d \major
  \time 2/4
  \tempo "Presto" % 4=110
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
  R2*4
  \repeat volta 2 {
    d16\p d fis fis  a-1 a b b |
    a a d-4 d cis-2 cis b-4 b |
    a a a a fis-2 fis fis fis |
    \break
    g-4 g g g  e-1 e e e |
    d d fis-4 fis a\< a b b |
    a16 a d-4 d e-1\! e fis-4 fis |
    e-1 e d-2 d cis-1 cis b-4 b_\markup{4th pos} |
    \break
    a16\>-1 a a a a a a a\! |
  }
  g'-3\fz\flageolet g fis-2 fis e-4 e d-2 d |
  cis-1 cis cis cis a-2 a a a |
  g'-3\fz g fis-2 fis e-4 e d-2 d |
  \break
  cis-1 cis cis cis a-2 a a a |
  fis16_\markup{2 \teeny "1/2" pos}\p-1 fis a_\markup{3rd pos}-4 a c! c a a |
  fis-1 fis d-0 d e-1_\markup{1st pos} e fis-1_\markup{2 \teeny "1/2" pos} fis |
  g-2 g b b d-4 d b-1_\markup{\underline {2 \teeny "1\2"}} b |
  \break
  g-2 g g g d d d d |
  c'!-2 c b b a-4_\markup{\underline "3rd pos"} a c c |
  b-1_\markup{\underline {2 \teeny "1/2" pos}} b b b g-2 g g g |
  a-4_\markup{\underline {3rd pos}} a g g fis-4_\markup{\underline {1st pos}} fis e e |
  \break
  fis fis fis fis d d d d |
  fis-4 fis a-1 a c!-4_\markup{\underline {2nd pos}} c a-1_\markup{\underline {1st pos}} a |
  fis-4 fis d d e-1 e fis-1_\markup{\underline {2 \teeny "1/2"} pos} fis |
  g-2_\markup{\italic cresc} g b-1 b d-4_\markup{\underline {3rd pos}} d c!-2_\markup{\underline {2 \teeny "1/2" pos}} c |
  \break
  b-1 b d-1_\markup{\underline{4th pos}} d e e fis-2_\markup{\underline{6th pos}} fis |
  g\f\flageolet-3 g fis-4_\markup{\underline{5 \teeny "1/2" pos}} fis e-1 e d-4_\markup{\underline{3rd pos}} d |
  cis-2 cis b-4_\markup{\underline{1st pos}} b a-1 a g g |
  fis fis fis fis d d d d |
  \break
  d-4_\markup{\italic "rit e. dim"} d d d d d d d |
  \key g \major
  b'2-1_\markup{\underline{2 \teeny "1/2" pos}}\mf |
  d,8-0 (g-2 b-1 d-4) |
  b2-1 |
  \break
  a2-4_\markup{\underline{3rd pos}} |
  g2-1 |
  fis4-4_\markup{\underline{1st pos}} (e) |
  a2-4_\markup{\underline{3rd pos}} |
  d,2-1
  c'2-4_\markup{\underline{5th pos}} |
  e,8-1 (a-1 c-4 e-2_\markup{\underline{5th pos}}) |
  c2-4 |
  b4-4_\markup{\underline{4th pos}} (a) |
  \break
  g2-2_\markup{\underline{2 \teeny "1/2" p}} fis4-1 (g) |
  b2-4_\markup{\underline{4th pos}} |
  a4-1_\markup{\italic "poco rit."} (ais) |
  b2-4^\markup{\italic "a tempo"} |
  d,8-0 (g-2_\markup{\underline{2 \teeny "1/2" pos}} b d) |
  b2-1 |
  a2-4 |
  g2-2 |
  \break
  fis4-1 (g-2) |
  fis'2 |
  e |
  d8-4 (b-1 g-2 d-0) e-1 (g-4 b-2 d-4) |
  e,2-1_\markup{\underline{1st pos}} |
  fis-2_\markup{\underline{2nd pos}} |
  g2_\markup{\italic {rit .e dim}} ( |
  g4) r |
  \bar "||"
  \break
  \key d \major
  d16\flageolet\p^\markup{Tempo 1} d fis fis a a b b |
  a a d d cis cis b b |
  a a a a fis-2 fis fis fis |
  g g g g e e e e |
  \break
  d16 d fis fis a\< a b b |
  a a\! d d e e fis fis |
  e e d d cis\> cis b b |
  a a a a a a a a\! |
  \break
  g'\fz g fis fis e e d d |
  cis-1 cis cis cis a-2 a a a |
  g'\fz g fis fis e e d d |
  cis-2 cis cis cis a-4 a a a |
  \break
  d,\p^\markup{\italic stringendo} d d d fis-2 fis fis fis |
  g-4 g g g e-1_\markup{\underline {1st pos}} e e e |
  d d d d fis-2 fis fis fis |
  g g g g e e e e |
  \break
  d_\markup {\italic cresc.} d d d fis-4 fis fis fis |
  a-1 a a a b b b b |
  a a a a d-1 d d d |
  \break
  e-4 e e e eis-2 eis eis eis |
  fis4\f-4 r |
  a,\downbow d,\flageolet |
  d4 g\flageolet
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

\markup { \teeny {found at google books - in parts}}
\markup {
  \teeny
  \date
}