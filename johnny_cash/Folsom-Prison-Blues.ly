\version "2.21.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Folsom Prison Blues"
  instrument = "Bass"
  composer = "Johnny Cash"
  arranger = "arr by: Josh Fossgreen"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key fis \major
  \time 4/4
  \tempo 4=105
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s1
  cis1:7
  fis1
  fis1
  s1
  s1
  s1
  b1
  s1
  fis1
  s1
  cis1:7
  s1
  fis1
  fis1
  s1
  s1
  s1
  b1
  s1
  fis1
  s1
  cis1:7
  s1
  fis1
  fis1
  s1
  s1
  s1
  b1
  s1
  fis1
   s1
   cis1:7
   s1
   fis1
   fis1
   s1
   s1
   s1
   b1
   s1
   fis1
   s1
   cis1:7
   s1
   fis1
   cis1:7
   fis1
}

electricBass = \relative c, {
  \global
  % Music follows here.
  r1
  cis'4 gis\2 cis gis\2 |
  fis cis fis cis |
  \mark \markup{Verses 1 & 2}
  \repeat volta 2 {
    \repeat unfold 4 {fis cis fis cis}
    b' fis b fis |
    b fis b fis |
    fis cis fis cis
    fis cis fis cis
    cis' gis\2 cis gis\2
    cis gis\2 cis gis\2
    fis cis fis cis
  }
  \break \mark \markup{Solo 1}
  \repeat unfold 4 {fis cis fis cis}
  b' fis b fis |
  b fis b fis |
  fis cis fis cis
  fis cis fis cis
  cis' gis\2 cis gis\2 |
  cis gis\2 cis gis\2 |
  fis cis fis cis
  \break \mark \markup{Verse 3 and Solo 2}
  \repeat volta 2 {
    \repeat unfold 4 {fis cis fis cis}
    b' fis b fis
    b fis b fis
    fis cis fis cis
    fis cis fis cis
    cis' gis\2 cis gis\2
    cis gis\2 cis gis\2
    fis cis fis cis
  } \break
  \mark \markup{Verse 4}
  \repeat unfold 4 {fis cis fis cis}
  b' fis b fis
  b fis b fis
  fis cis fis cis
  fis cis fis cis
  cis' gis\2 cis gis\2
  cis gis\2 cis gis\2
  fis cis fis cis
  cis'^\markup{\italic Tag} gis\2 cis gis\2
  fis cis fis2\fermata
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
