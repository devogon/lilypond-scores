\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Schism"
  subtitle = "Tool - Lateralus 2001"
  instrument = "Bass"
  composer = "Justin Chancellor"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \numericTimeSignature
  \tempo 4=208
   \time 5/4

}



electricBass = \relative c, {
  \global

  % Music follows here.
  <d bes'>\mf^\markup{\bold Intro}^\markup{\small Freely} <f a> <g c> <bes d>2 | % 1
  <f bes>4 <f a> <g c> <bes d> (<d, d'>) ~ | % 2
  \bar "||"
  \time 4/4
  <d d'>2 ~ <d d'>4. \tuplet 3/2 {d16^\markup{\small "In time"} (c' d)} | % 3
  \break
  \time 5/8
  \repeat volta 4 {
    f8 d, e' d,8 \tuplet 3/2 {d16 (c' d)} | % 4
    \time 7/8
    \autoBeamOff
    f8 [d, e'] d, [g'] d, \autoBeamOn \tuplet 3/2 {d16 (c' d)} | % 5
    \time 5/8
    f8 d, e' d,8 \tuplet 3/2 {d16 (c' d)} | % 6
    \break
    \time 7/8
    f8 [d, e'] d, [g'] d, \autoBeamOn \tuplet 3/2 {d16 (c' d^\markup{\bold x4})} | % 7
  }
  \time 5/8
  \repeat volta 2 {
    \mark \default
    f8^\markup{\bold "Verse 1"} [d, e']  d, \tuplet 3/2 {d16 (c' d)} | % 8
    \time 7/8
    f8 [d, e'] d, [g'] d, \autoBeamOn \tuplet 3/2 {d16 (c' d^\markup{\bold x4})} | % 9
    \break
    \time 5/8
    e8 [d, d']  d, \tuplet 3/2 {d16 (c' d)} | % 10
    \time 7/8
    e8 [d, d'] d, [g'] d, \autoBeamOn \tuplet 3/2 {d16 (c' d^\markup{\bold x4})} | % 11
  }
  \repeat volta 2 {
    \time 5/8
    f8^\markup{\bold Bridge} [d, e']  d, \tuplet 3/2 {d16 (c' d)} | %12
    \break
    \time 7/8
    \autoBeamOff
    f8 [d, e'] d, [g'] d, \autoBeamOn \tuplet 3/2 {d16 (c' d)} | % 13
    \time 5/8
    f8 d, e' d, \tuplet 3/2 {d16 (c' d)} | % 14
  }
  \alternative {
    {
      \time 7/8
      \autoBeamOff
      f8 [d, e'] d, [g'] d, \autoBeamOn \tuplet 3/2 {d16 (c' d)} | % 15
          \break
    }
    { \time 7/8
      \autoBeamOff
      f8 [d, e'] d, [g'] d, \autoBeamOn \tuplet 3/2 {a16 (g' a)} | %
    }
  }
  \time 6/8
  \mark \default
  \repeat volta 2 {
  c8^\markup {Chorus} a, b'! a,4 \tuplet 3/2 {a16 (g' a)} | % 17

  \time 7/8 \autoBeamOff
  c8 [a, b'!] a, [d'] a, \autoBeamOn \tuplet 3/2 {a16 (g' a)}
  \break
  \time 6/8
    c8 f,, b'! f,4 \tuplet 3/2 {f16 (g' a)} | % 19
   \time 7/8
  }
  \alternative {
    {c8 [f,, b'!] f, c'' f,, \tuplet 3/2 {f16 (g' a)}}
        {c8 [f,, b'!] f, c'' f,, \tuplet 3/2 {f16 (g' a)}}
  }
  \break
  \time 5/8
  \repeat volta 4 {
    \mark "A"
    f'8^\markup{\bold "Verse 2"} f, e' f, \tuplet 3/2 {f16 (c' d)} | % 22
    \time 7/8 \autoBeamOff
    f8 [f, e'] f, [g'] f, \autoBeamOn \tuplet 3/2 {f16 (c' d)} | % 23
    \time 5/8
    e8 f, d' f, \tuplet 3/2 {f16 (c' d)} | % 24
    \break
      \time 7/8
  }
  \alternative
  {
    { \autoBeamOff e8 [f, d'] f, [g'] f, \tuplet 3/2 {f16 (e' f)} }
    {}
  }
}

% https://www.scribd.com/doc/206185806/Schism-Tool-download-bass-transcription-and-bass-tab-in-best-quality-www-nicebasslines-com

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
