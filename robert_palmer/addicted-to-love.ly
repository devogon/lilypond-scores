\version "2.19.84"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Addicted to Love"
  subtitle = "Robert Palmer - Riptide - 1985"
  instrument = "Bass"
  composer = "Robert Palmer"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=111
}

electricBass = \relative c, {
  \global
  R1*8
  % \break
  \repeat volta 2 {
    r8 a\f\4 d\3 a\4 c a\4 g a\4 | % 9
    r8 a\4 d\3 a\4 c a\4 g a\4 | % 10

    r8 g\4 c\3 g\4 b g\4 f g\4 | % 11
    r8 g\4 c\3 g\4 b g\4 f g\4 |
    % \break
    r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 13
    r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 14
    r8 a\4 d\3 a\4 c a\4 g a\4 | % 15
    r8 a\4 d\3 a\4 c a\4 g a\4 | % 16
    r8 a\4 d\3 a\4 c a\4 g a\4 | %
    r8 a\4 d\3 a\4 c a\4 g a\4 | % 18
    r8 g\4 c\3 g\4 b g\4 f g\4 | % 19
    r8 g\4 c\3 g\4 b g\4 f g\4 | % 20
    r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 21
    r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 22
    r8 a\4 d\3 a\4 c a\4 g a\4 | % 23
  }
  \alternative {
    { r8 a\4 d\3 a\4 c a\4 g a\4 | }
    { r8 a\4 d\3 a\4 c a\4 g fis\4 ~ | }
  }
  fis4.\4 fis8 fis4. d'8\3 ~ |
  d4.\3 d8\3 d\3 d4\3 a8\4 | % 27
  r8 a\4 fis'\2 a,\4 g'\2 a,\4 fis'\2 a,\4 | % 28
  r8 a\4 fis'\2 a,\4 g'\2 a,\4 fis'\2 fis,\4 ~ | % 29
  fis4. fis8 fis4. d'8\3 ~ | % 30
  d4.\3 d8\3 d\3 d4\3 a8\4 |
  d2\3 ~ d4. e8\3 |
  fis'4\1 d,4\4 e2\4 | % 33
  r8 a,\4 d\3 a\4 c a\4 g a\4 | % 34
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 35
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 36
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 37
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 38
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 39
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 40
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 41
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 42
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 43
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 44
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 45
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 46
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 47
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 48
  r8 a\4 d\3 a\4 c a\4 g fis\4 ~ | % 49
  fis4. fis8 fis4. d'8\3 ~ | % 50
  d4. \3 d8\3 d\3 d4\3 a8\4 | % 51
  r8 a\4 fis'\2 a,\4 g'\2 a,\4 fis'\2 a,\4 | % 52
  r8 a\4 fis'\2 a,\4 g'\2 a,\4 fis'\2 fis,\4 ~ | % 53
  fis4. fis8 fis4. d'8\3 ~ | % 54
  d4.\3 d8\3 d\3 d4\3 a8\4 | % 55
  d2\3 ~ d4.\3 e8\3 | % 56
  fis'4\1 d,4\4 e2\4 | % 57
  r8 a,\4 d\3 a\4 c a\4 g a\4 | % 58
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 59
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 60
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 61
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 62
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 63
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 64
  r8 a\4 d\3 a\4 c a\4 g fis\4 | % 65
  R1*2
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 68
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 69
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 70
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 71
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 72
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 73
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 74
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 75
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 76
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 77
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 78
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 79
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 80
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 81
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 82
  r8 a\4 d\3 a\4 c a\4 g fis\4 ~ | % 83
  fis4. fis8 fis4. d'8\3 ~ | % 84
  d4.\3 d8\3 fis\2 d4\3 a8\4 | % 85
  r8 a\4 fis'\2 a,\4 g'\2 a,\4 fis'\2 a,\4 | % 86
  r8 a\4 fis'\2 a,\4 g'\2 a,\4 fis'\2 fis,\4 ~ | % 87
  fis4. fis8 fis4. d'8\3 ~ | % 88
  d4.\3 d8\3 d\3 d4\3 a8\4 | % 89
  d2\3 ~ d4.\3 e8\3 | % 90
  fis'4\1 d,\4 e2\4 | % 91
  r8 a,\4 d\3 a\4 c a\4 g a\4 | % 92
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 93
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 94
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 95
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 96
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 97

  r8 a\4 d\3 a\4 c a\4 g a\4 | % 98
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 99
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 100
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 101
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 102
  r8 g\4 c\3 g\4 b g\4 f g\4 | % 103
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 104
  r8 a\4 d\3 a\4 c a\4 c\3 d\3 | % 105
  r8 a\4 d\3 a\4 c a\4 g a\4 | % 106
  r8 a\4 d\3 a\4 c a\4 c4\3 ~ | % 107
  c1 |
  R1*2
  \bar "|."
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout { \omit Voice.StringNumber
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1}
}
