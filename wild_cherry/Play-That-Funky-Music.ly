\version "2.19.84"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Play That Funky Music"
  subtitle = "(Wild Cherry - Wild Cherry - 1976)"
  instrument = "Bass"
  composer = "Allen Wenz"
  tagline = \date
}

global = {
  \key g \major
  \time 4/4
}

\paper {
  indent = 0.0\cm
}

makePercent =
    #(define-music-function (parser location note) (ly:music?)
       "Make a percent repeat the same length as NOTE."
       (make-music 'PercentEvent
                   'length (ly:music-length note)))

chordNames = \chordmode {
  \global
  R1 * 4
  e1:9
  s1 * 3
  e1:9
  s1 * 55
  g1:9
  s1 * 4
  bes1:9
  s1 * 2
  e1:9
  s1 * 3
  e1:9
  s1 * 7
  g1:9
  s1 * 7
  a1:9
  s1 * 3
}

electricBass = \relative c, {
  \global
  R1 * 4
  \break
  \repeat volta 2 {
    \repeat unfold 4 {e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3)} |
    \bar "||"
    \break
    \mark \markup{\box "Verse"}
    \set countPercentRepeats = ##t
    \set repeatCountVisibility = #(every-nth-repeat-count-visible 2)

%    \repeat percent 4 {
       e,4\staccato\segno r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3) |
     e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3) |
      \repeat percent 7 { e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3) |
       e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3) |
       \break
    }
    \break
    e,4\staccato r r2 |
    R1 * 2
    r2 r8 e8 f! fis |
    \bar "||"
    \break
    \mark \markup{\box "Chorus"}
    \repeat unfold 4 {g4\staccato r8 g'\2 g\staccato\2 [r16 g,] c (d\3) f!\2 (g\2) | }
    \break
    g,4\staccato r8 g'8\2 g4\staccato\2 g,8 a\3 |
    bes4 r8 bes' bes4 r |
    c4 d8 b d4 b8 a\2 |
    b4 a8\2 b a\2 g\2 e\3 d\3^\markup{\right-align "To Coda"} |
  }
  \break
  \repeat unfold 4 {e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3)} |
  \bar "||"
  \mark \markup{\box "Guitar Solo"}
  \repeat volta 2 {
    \repeat unfold 3 {e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3)} |
    e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e^\markup{\right-align "DS al Coda"}\3)
  }
  \break
  e,4\staccato\coda r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3)
  \repeat unfold 3 {e,4\staccato r8 d'\3 e\3 [r16 e,] a\4 (b\4) d\3 (e\3)} |
  \break
  \bar "||"
  g,4\staccato r r2
  R1*6
  r2 g8 g'\2 gis, gis'\2 |
  \bar "||"
  \break
  \mark \markup {\box "Chorus"}
  \repeat volta 2 {
    \repeat unfold 3 {a,4\staccato r8 a'\2 a\2[ r16 a,] d\3 (e\3) g\2 (a\2) | }
    a,4\staccato r8 a'\2 a\2 [r16 a,\3] d\3 (e\3) g\2 (a\2_\markup{\italic \right-align "Repeat and fade"}) |
  }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout {
    \compressFullBarRests
    \override MultiMeasureRest #'expand-limit = 1
  \omit Voice.StringNumber
  }
}
