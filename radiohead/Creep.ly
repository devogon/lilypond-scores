\version "2.21.6"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

%source https://www.youtube.com/watch?v=-o-ewEaXEu0

\header {
  title = "Creep"
  subtitle = "(Radiohead - Pablo Honey - 1992)"
  instrument = "Bass"
  composer = "Radiohead"
  arranger = "bass: Colin Greenwood"
  tagline = \date
}


\paper  {
  indent = 0.0\cm
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=92
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  g1
  s1
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s
  g1
  s
  b1
  s
  c1
  s
    c1:m
  s
  g1
  s
  b1
  s
  c1
  s
    c1:m
  s
  g1
  s
  b1
  s
  c1
  s
    c1:m
  s
  g1
  s
  b1
  s
  c1
  s
  c1:m
  s1
  g1
  s
}

electricBass = \relative c, {
  \global
    \mark \markup {Intro}
    g4 (g8.) g16 g8 g (g) g | % 1
    g4 (g8.) g16 g8 g (g) g | % 2
    b4 (b8.) fis16 b8 b (b) fis | % 3
    b4 (b8.) fis16 b8 b (b) b | % 4
    c4 (c8.) g16 c8 c (c) g | % 5
    c4 (c8.) g16 c8 c (c) b | % 6
    c4 (c8.) g16 c8 c (c) b | % 7
    c4 d\3\glissando {\once \hideNotes g32\4} es8.. fis,4\glissando | % 8
    \break
    \mark \markup {Verse}
    g4 (g8.) g16 g8 g (g) g | % 9
    g4 (g8.) g16 g8 g (g) g | % 10
    b4 (b8.) fis16 b8 b (b) fis | % 11
    b4 (b8.) fis16 b8 b (b) fis | % 12
    c'4 (c8.) g16 c8 c (c) g | % 13
    c4 (c8.) g16 c8 c (c) b | % 14
    c4 (c8.) g16 c8 c (c) b | % 15
    c4 d\3\glissando {\once \hideNotes g32\4} es8.. f!4\glissando | % 16
    g,4 (g8.) g16 g8 g (g) g | % 17
    g4 (g8.) g16 g8 g (g) g | % 18
     b4 (b8.) fis16 b8 b (b) fis | % 19
    b4 (b8.) fis16 b8 b (b) fis | % 20
    c'4 (c8.) g16 c8 c (c) b | % 21
    c4 (c8.) g16 c8 c (c) c16 (d\3) | % 22
    c4 (c8.) g16 c8 c (c) g | % 23
    c4 d\3\glissando {\once \hideNotes g32\4} es8.. f!4\glissando | % 24
    \break
    \mark \markup {Chorus}
    g,4 (g8.) g16 g8 g (g) g | % 25
    g4 (g8.) g16 g8 g (g) g | % 26
    b4 (b8.) fis16 b8 b (b) fis | %
    b4 (b8.) fis16 b8 b (b) b | % 28
    c4 (c8.) g16 c8 c (c) g | %
    c4 (c8.) g16 c8 c (c) g | % 30
    c4 (c8.) g16 c8 c (c) b | % 31
    c1 | % 32
    \mark \markup {Verse}
    \break
    g4 (g8.) g16 g8 g (g) g | % 33
    g4 (g8.) g16 g8 g (g) g | % 34
    b4 (b8.) fis16 b8 b (b) b | % 35
    b4 (b8.) fis16 b8 b (b) fis | % 36
    c'4 (c8.) g16 c8 c (c) g | % 37
    c4 (c8.) g16 c8 c (c) b | % 38
    c4 (c8.) g16 c8 c (c) b | % 39
    c4 d\3\glissando {\once \hideNotes g32\4} es8.. f,!4\glissando | % 40
    g4 (g8.) g16 g8 g (g) d'\3 | %
    g,4 (g8.) g16 g8 g (g) g | % 42
    b4 (b8.) fis16 b8 b (b) fis | % 43
    b4 (b8.) fis16 b8 b (b) fis | % 44
    c'4 (c8.) g16 c8 c (c) g | % 45
    c4 (c8.) g16 c8 c (c) b | % 46
    c4 (c8.) g16 c8 c (c) b | % 47
    c4 d\3\glissando {\once \hideNotes g32\4} es8.. f!4\glissando | % 48
    \break
    \mark \markup {Chorus}
    g,4 (g8.) g16 g8 g (g) g | %
    g4 (g8.) g16 g8 g (g) g | % 50
    b4 (b8.) fis16 b8 b (b) fis | % 51
    b4 (b8.) fis16 b8 b (b) fis | % 52
    c'4 (c8.) g16 c8 c (c) g | %  53
    c4 (c8.) g16 c8 c (c) g | % 54
    c4 (c8.) g16 c8 c (c) b | %55
    c4 d\3\glissando {\once \hideNotes g32\4} es8..\3 f!4\3\glissando | % 56
    g,4 (g8.) g16 g8 g (g) d'\3 | % 57
    g,4 (g8.) g16 g16 (a\4) g8 (g) fis | % 58
    b4 (b8.) fis16 b8 b (b) fis | % 59
    b4 (b8) g16 g g (a\4) g8 (g) fis | % 60
    c'4 (c8.) g16 c8 c (c) g | % 61
    c4 (c8) c16 c  \grace c32 d8\3 c (c) b | % 62
    c4 (c8.) g16 c8 c (c) c | % 63
    c8 c\3\glissando d8\3 d\3 es\3 es\3 f4 | % 64
    g4\2 (g8.\2) d16\3 g8\2 g\2 (g\2) g, | % 65
    g'4\2 (g8\2) g16\2 g\2 g\2 (a\2) g8\2 (g4\2)\glissando | % 66
    b,4 (b8.) fis16 b8 b (b) fis | % 67
    b4 (b8) fis b fis b d\3 | % 68
    c4 (c8.) g16 c8 c (c) g | % 69
    c4 (c8.) g16 c8 c (c) d\3 | % 70
    c1 (| % 71
    c1) | % 72
    \mark \markup {Outro}
    g2. (g8) g8  | % 73
    g1 | % 74
    b2. (b8) b8  | % 75
    b1 | % 76
    c2. (c8) c8 | % 77
    c1 | % 78
    c2. (c8) c | % 79
    c1 | % 80
    g2. (g8) g | % 81
    g1 | % 82
    b2. (b8) b  | % 83
    b2. (b8) b  | % 84
    c2. (c8) c | % 85
    c2. (c8) c | % 86
    c1 ( | % 87
    c1) | % 88
    g'8\2\glissando a\2 a16\2 (b8.\2) (b2\2) | % 89
    R1 % 90
    \bar "||"
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
