\version "2.19.82"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Sarabande"
  instrument = "Contrabas"
  composer = "Johann Pachelbel"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent=0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \numericTimeSignature
  \time 3/4
  \tempo 4=64
}

contrabass = \relative c, {
  \global
  % Music follows here.
  d2 d4
  a'2 a4
  b2 b4
  fis2 fis4
  g2 b4
  fis2 b4
  e,2 a4
  \break
  d,2.
  d4 d d
  a'4 a a
  b4 b b
  fis4 fis fis
  g4 g b
  fis4 fis b
  \break
  e,4 e a
  d,2.
  r4 r a'
  fis2 fis4
  e4 a a
  d, fis d
  e4 fis fis
  \break
  b4 g e
  a4 a a
  d,2.
  a'4 a a
  fis4 d d
  e4 a a
  d,4 fis d
  \break
  e4 fis fis
  b4 g e
  a a a
  d,2.
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
  \header {
    piece = \markup{\bold \larger "original"}
  }
}

\paper {
  fonts = #
  (make-pango-font-tree
   "Architects Daughter"
   "DejaVu Sans"
   "DejaVu Sans Mono"
   (/ (* staff-height pt) 2.5))
}
\paper {
  fonts = #
  (make-pango-font-tree
   "MuseJazz"
   "DejaVu Sans"
   "DejaVu Sans Mono"
   (/ (* staff-height pt) 2.5))
}
\paper {
  fonts = #
  (make-pango-font-tree
   "Architects Daughter"
   "DejaVu Sans"
   "DejaVu Sans Mono"
   (/ (* staff-height pt) 2.5))
}

contrabassUp = \relative c {
  \global
  % Music follows here.
  d2 d4
  a'2 a4
  b2 b4
  fis2 fis4
  g2 b4
  fis2 b4
  e,2 a4
  \break
  d,2.
  d4 d d
  a'4 a a
  b4 b b
  fis4 fis fis
  g4 g b
  fis4 fis b
  \break
  e,4 e a
  d,2.
  r4 r a'
  fis2 fis4
  e4 a a
  d, fis d
  e4 fis fis
  \break
  b4 g e
  a4 a a
  d,2.
  a'4 a a
  fis4 d d
  e4 a a
  d,4 fis d
  \break
  e4 fis fis
  b4 g e
  a a a
  d,2.
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabassUp }
  \layout { }
  \header {
    piece = \markup{\bold \larger "octave hoger"}
  }
}
