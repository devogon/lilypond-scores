\version "2.19.54"
\language "nederlands"

\header {
  title = "Prayer"
  subtitle = "from “jewish Life” no 1"
  instrument = "Contrabas"
  composer = "Ernest Bloch"
  arranger = "arr: Ben de Ligt"
  poet = \markup{\bold \circle \huge 8}
				% Remove default LilyPond tagline
  piece = \markup \column {
    \line{\italic \small "Kent u Westerbork? Ik g a er nooit heen. zo'n akelige plek..."}
    \hspace #1
  }
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}
date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key as \major
  \time 4/4
  \tempo 4=80
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative c {
  \global
  % Music follows here.
  \partial 4 r4
    bes1\p (
    bes2.) bes4
    f'2 bes4 (f)
    c2.\> r4
    bes1\p (
    bes2) r
    R1*2
    \break
    as'!2\mf bes,2
    c1
    f,2 (f8\fermata^\markup{\italic breve}) r8 r4
    bes1 (
    bes2.) bes'4
    f2_\markup{\italic poco dim.} bes,4 (f')
    c2. r4
    \break
    bes1\< (
    bes2.\!)_\markup{\italic poco accel.} es4
    \tempo \markup{\tiny "a tempo"}
    f2 bes,2_\markup{\italic poco dim.} (
    es2) r
    as,!2\f bes
    c1\>^\markup{\italic poco rit.}
  f,2\!\p (f8) r r4
  \bar "||"
    \time 2/4
    \break
    \tempo \markup{\tiny "Poco più mosso"}
    R2*15
    \tempo \markup{\tiny "Poco più vivo"}
  R2*5
  \break
    R2*2
    f2\p (
    f2\>) (
    f4\!)\fermata r
  \tempo \markup{\tiny "Tempo 1"}
  \bar "||"
    \time 4/4
    bes'1\f
    bes2. bes4
    f2\> (bes4\p) f
    c2. r4
    \break
    bes'1\p\< (
    bes2.) es,4\!
    f2\f\>
    bes,2
    es2\!\p ( es8) r r4
    as!2\f\> ( bes2)
    c2 ( c,)\!
    des1\pp (
    \break
    des1) (
    des1) (
    des8) r r4 r2
    bes1\f\> (
    bes4) c2.\!\p (
    c2) es\pp
    c1\fermata
    \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}


\markup \column {
%  \line {\small {\bold "daarna..."}}
				%  \hspace #1
    \line {\small { \italic {…..zou zij ook in het  kamp geweest zijn?}}}
  \line {\small { \italic{Waarom zei U dat Westerbork zon akelige plek is?}}}
  \hspace #1
}

lage_e = \relative c {
  %\global
   % Music follows here.
   e,1 (e e e^\markup{\tiny "stop on cue"})
}

\score {
  \new Staff { \clef bass \lage_e }
  \layout { }
  \header {
    piece = " "
  }
}

\markup \column {
 % \line {\small { \italic{Dat is beter voor ze...} \rounded-box{"Lage E,"} vervolgens een losse G vanuit de cellos...}}
				%  \hspace #1
  \line {\small {\italic "oh, mijn god! dit gaat verkeerd"} \box \small "- verkeerd! dit gaat verkeerd!"}
%  \hspace #1
  \line {\small {\italic {Ze is Joods, ze mag het niet weten} fluister:  \box " ze is joods, ze is joods..."}}
  \hspace #1
  \line {\small {B. \italic {Maar hoe zat het dan met uw vader? U zei toch dat u dat gevoel}}}
  \line {\small {\italic {   ook kende Levenslang \underline {kind-zijn-van}?}\box "kind zijn van" 3x    (zuren, naar zeggen,)}}
  \hspace #1				%  \line {\small { \italic{levenslang de dochter-van te zijn...} \box{"prayer"}}}
  \line {\small {\italic {Ga ik het haar vertellen?}}}
\line {\small {\italic {Ja, dat lijkt me nu eindelijk wel eens een goed idee.. waarom niet?}}}
  \line {\small \box \bold "9. Nocturne"}
  \hspace #1
}

\markup {
  \teeny
  \date
}