\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

% https://www.basslessons.be transcribed by Ari Dagan

\header {
  title = "Born Under A Bad Sign"
  subtitle = "album: Born Under A Bad Sign"
  composer = "Albert King"
  poet = "bass: Donald Duck Dunn"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


global = {
  \key cis \major
  \time 4/4
%  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

thechords = \chordmode  {
  cis1:7
  s1
  s1
  s1
  cis1:7
  s1
  s1
  s1
  gis1:7
  s1
  cis1:7
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  gis1:7
  fis1:7
  cis1:7
  s1
  s1
  s1
  s1
  s1
  gis1:7
  fis1:7
  gis1:7
  fis1:7
  cis1:7
  s1
  cis1:7
  s1
  s1
  s1
}

contrabas = \relative c,  {
   \global
  r4. fis8 gis b! cis e! |
  cis4. fis,8 gis b! e,4 |
  cis'4. fis,8 gis b! cis e! |
  \break
  \mark \markup {\box vocals}
  cis4. fis,8 gis b! e,4 |
  cis'4. fis,8 gis b cis e! |
  cis4. fis,8 gis b! e,!4 |
  \break
  gis4. gis8 gis4. g!8 |
  fis2 fis8 gis e! fis |
  cis'4. fis,8 gis b! cis e! |
  cis4. fis,8 gis b!4 b8 |
  \break
  % \mark \segno
  \repeat volta 2 {
    cis4.\segno fis,8 gis b!4 b8 |
    cis4. fis,8 gis b!4 b8 |
    cis4. fis,8 gis b!4 b8 |
    cis4. fis,8 gis b! cis e! |
    \break
    cis4. fis,8 gis b! e,!4 |
    cis'4. fis,8 gis b! cis e! |
    cis4. fis,8 gis b! e,!4 |
    cis'4. eis,8 ~ eis fis4 fis8 | % put an x gracenote before the last fis8
    \break
    gis4. gis8 gis4. g!8 |
    fis2 fis8 gis e! fis |
    cis'4. fis,8 gis b! cis e! |
    cis4. fis,8 gis b!4 b8
    \once \override Score.RehearsalMark #'font-size = #4
    \mark \markup { \musicglyph #"scripts.coda" }
  }
  \break
  \repeat percent 4 {cis4. fis,8 gis b!4 b8}
  \break
  gis4. gis8 gis a! gis g! |
  fis4. fis8 fis eis fis fis | % also gracenote before last fis
  gis4. gis8 gis a! gis g! |
  fis4\staccato r fis8 gis e! fis |
  \break
  cis'4. fis,8 gis b! cis e! |
  cis4. fis,8 gis b!4^\markup{\bold "D.S. al coda"} b8
    \once \override Score.RehearsalMark #'font-size = #4
    \mark \markup { \musicglyph #"scripts.coda" }
  %\mark \markup {circle thingy}
  \break
  cis4. fis,8 gis b! e,!4 |
  cis'4. fis,8 gis b!4 b8
  \repeat volta 2 {
    cis4. fis,8 gis b! e,!4 |
    cis'4. fis,8_\markup{\italic "repeat & fade"} gis b!4 b8
  }
    }

chordsPart = \new ChordNames \thechords

bassPart = \new Staff {\clef bass \contrabas}

\score {
  <<
    \chordsPart
    \bassPart
  >>
}
%
% \score {
%   \new Staff \with {
% %    instrumentName = "Contrabass"
% %    shortInstrumentName = "Cb."
%   } { \clef bass \contrabas }
%   \layout { }
% }
