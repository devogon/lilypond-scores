\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Coro"
  instrument = "Violoncello"
  composer = "Georg Frederic Handel"
  arranger = "Wanda Sobieska"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \numericTimeSignature
  \time 3/4
%  \tempo 4=100
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    d4 d' cis
    d a b
    fis fis fis
    b,2.
    e4 g e
    a cis, a
    d4 d8 d d d
    d4 fis a
  }
  \repeat volta 2 {
      d4 d, d'
      cis cis, cis'
      b b b
      fis2 d4
      g b, cis
      d fis b
      a a8 a a a
      a4 cis a
      \break
      d4 d, fis a a,2
      d'4 d, fis
      a a,2 d'4 d, a' d fis, a
      d,4 d8 d d d
      d2.
  }
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}
