\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "La Paix"
  instrument = "Violoncello"
  composer = "Georg Frederic Handel"
  arranger = "Wanda Sobieska"
%  meter = "Largo alla Sicilliana"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent=0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \numericTimeSignature
  \time 12/8
  \tempo "Largo alla Sicilliana" % 4=100
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    d4. (d4) d8 a'4. (a8) e cis | % 1
    a8 cis e a e d cis a cis d fis  a | % 2
    d8 a fis d fis a d a fis d4. | % 3
    g4 fis8 e4 d8 a4. (a4) cis'8 | % 4
    \break
    d8 e16 d e d cis8 d16 cis d cis b8 cis16 b cis b a4. ( | % 5
    a4.)( a4.)(a4.)(a8) cis e | % 6
    cis8 a d, e4 a,8 e'4 a8 e4 a8 | % 7
    cis,4 d8 e4 fis8 d e e, a4.
  }
  \break
  \repeat volta 2 {
    a'8 e cis a4 cis'8 d a fis d fis a | % 9
    d4. c!4. b8. c16 b8 b g d | % 10
    g4. d8 fis d a'4. e8 g e | % 11
    b'4. fis8 d fis g d fis, a a' g | % 12
    \break
    fis8 d g a4 d,8 a'4 d8 a4 d,8 | % 13
    cis'4 d8 a4 b8 g a a, d4 d'8 | % 14
    cis8.\trill b32 cis d8 cis8.\trill b32 cis d8 a4 d8 a4 d8 | % 15
    a4 d8 a4 b8 g a a, d4. | %
  }
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}
