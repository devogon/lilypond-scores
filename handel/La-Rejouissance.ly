\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "La Rejouissance"
  instrument = "Violoncello"
  composer = "G.F. Handel"
  arranger = "Wanda Sobieska"
  % Remove default LilyPond tagline
  tagline = \date
  copyright = \markup{\tiny"2008 | available at www.freegigmusic.com"}
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo  4=50
  \partial 8
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  \partial 8 r8 |
  r4 r8 d d4 r | % 1
  r4 r8 d d4 r | % 2
  r8 d fis a d a fis d | % 3
  d'8 a fis d d' a fis d | % 4
  d'8 d, cis d a' a16 b cis8 a | % 5
 \break
  d,8 d' cis d a8. d,16 a'8. d,16 | % 6
  a'8 a, cis e a e cis a | % 7
  d8 b e e, a4. %r8 % 8
				}
%    r4 r8 d d4 r % | % 9
%   r4 r8 d d4 r | % 10
%   r8 d fis a d a fis d | % 11
%   d'8 a fis d d' a fis d | % 12
%   d'8 d, cis d a' a16 b cis8 a | % 13
%   d,8 d' cis d a8. d,16 a'8. d,16 | % 14
%   a'8 a, cis e a e cis a | % 15
%   d8 b e e, a4. % 16
  \repeat volta 2 {
    a8
    a'4 r8 d, a a16 b cis8 d | % 17
    a8 a16 b cis8 d a a16 b cis8 a | % 18
 %   \break
    d'8 d, b g d' d' b g | % 19
    d'8 d, b g d' d16 e fis8 d | % 20
  g8 d b cis d fis a d | % 21
    \break
    e,8 fis16 g a8 a, d4. fis8 | % 22
%    \break
    a8 a, a cis d fis, fis b | % 23
    e,8 a a g fis a d d | % 24
    g8 g, d' fis e d cis a | % 25
    d g a a, d4. % a8 | % 26
% 	  \break			%  }
%     a4 r8 d a a16 b cis8 d | % 28
%     a8 a16 b cis8 d a a16 b cis8 a | % 28
%  %   \break
%     d8 d b g d' d b g | % 29
%     d'8 d b g d' d16 e fis8 d | % 30
%   g8 d b cis d fis a d, | % 31
%     \break
%     e8 fis16 g a8 a, d4. fis8 | % 32
% %    \break
%     a8 a, a cis d fis, fis b | % 33
%     e,8 a a g fis a d d | % 34
%     g8 g, d' fis e d cis a | % 35
%     d g a a, d4.  % 36
%   \bar "|."
  }
}
\score {
  \new Staff \with {
  } { \clef bass \cello }
  \layout { }
}
