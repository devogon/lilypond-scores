\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "La Rejouissance"
  subtitle = "eenvoudige baspartij"
  instrument = "Violoncello"
  composer = "G.F. Handel"
  arranger = "Wanda Sobieska"
  % Remove default LilyPond tagline
  tagline = \date
  copyright = \markup{\tiny"2008 | available at www.freegigmusic.com"}
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo  4=50
}

cello = \relative c {
  \global
  % Music follows here.
%  \repeat volta 2 {
  \partial 8 r8 |
  r4 r8 d d4 r | % 1
  r4 r8 d d4 r | % 2
  r8 r r a d r r d | % 3
  d8 r r d d r r d  | % 4
  d8 r r d a' r r a | % 5
 \break
  d,8 r r d a'8. d,16 a'8. d,16 | % 6
  a'8 r r e a r r a | % 7
  d,8 b e e, a4. r8 % 8
				% }
    r4 r8 d d4 r % | % 9
  r4 r8 d d4 r | % 10
  r8 r r a d r r d | % 11
  \break
  d8 r r d d r r d | % 12
  d8  r r d a' r r a | % 13
%  \break
  d,8 r r d a'8. d,16 a'8. d,16 | % 14
  a'8 r r e a r r a, | % 15
  d8 b e e, a4. % 16
    \break
%  \repeat volta 2 {
    a8
    a4 r8 d a r r d | % 17
    a8 r r d a r r a | % 18
 %   \break
    d8 r r g, d' r r g, | % 19
    d'8 r r g, d' r r d | % 20
  g8 r r r d r r r | % 21
    \break
    e8 r r a, d4. fis8 | % 22
%    \break
    a8 a, a cis d fis, fis b | % 23
    e,8 a a g fis a d d | % 24
    g8 g d fis e d cis a | % 25
    d g a a, d4. a8 | % 26
	  \break			%  }
    a4 r8 d a r r d | % 27
    a8  r r d a r r a | % 28
 %   \break
    d8 r r g, d' r r g, | % 29
    d'8 r r g, d' r r d | % 30
    g8  r r r d r r r | % 31
    \break
    e8  r r a, d4. fis8 | % 32
%    \break
    a8 a, a cis d fis, fis b | % 33
    e,8 a a g fis a d d | % 34
    g8 g d fis e d cis a | % 35
    \breathe
  d g a a, d2 | % 36
  \bar "|."
}

\score {
  \new Staff \with {
  } { \clef bass \cello }
  \layout { }
}
