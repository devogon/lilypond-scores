\version "2.19.57"

\language "nederlands"

\header {
  title = \markup{Salomo \teeny "III Act"}
%  subtitle = "III Act"
  composer = "G. F. Händel"
  instrument = "Bassi"
  % poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  % annotate-spacing = ##t
  %system-system-spacing = #'((basic-distance . 0.1) (padding . 0))
  ragged-last-bottom = ##f
  ragged-bottom = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key bes \major
  \time 4/4
  \tempo "Sinfonia" %4=110
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
   bes'8 bes a a g g f f | % 1
   es8 es f f bes4 bes, | % 2
   d8 d g g c, c e! e | % 3
   f8 bes c c, f4 a,8 c | % 4
   f8 f es es d d g g | % 5
   c,8 c f f bes4 bes, | % 6
   bes'8 bes bes bes a a a a | % 7
   g8 g g g f f f f | % 8
   es8 es es es d d d d | % 9
   c8 c c es f f f a, | % 10
   % \break
   bes8 c d es f g a bes | % 11
   es,8 es es es f f f f | % 12
   f8 f f f f f f f | % 13
   % \break
   f8 f f f f f f f | %  14
   bes,8 c d es f es d es | % 15
   f8 es f f, bes4 r | % 16
   R1*3
   % \break
   bes'8 bes a a g g f f | % 20
   es8 es f f bes,4 r  | % 21
   R1*4 | % 22, 23, 24, 25
   f'8 f e! e d d c c | % 26
   % \break
   bes8 bes c c f bes, a g | % 27
   f4 r r2 | % 28
   R1*2 | % 29, 30
   f'8 f f f e! e e e | % 31
   % \break
   d8 d d d c c c c | % 32
   bes8 bes bes bes a a a a | %  33
   g8 g g g c4 r | % 34
   R1*2 | % 35, 36
   % \break
   bes8 bes bes bes c c c c | %37
   c8 c c c c c c c | % 38
   c8 c c c c c c c | % 39
   f8 d bes c f4 r | % 40
   % \break
   R1*2 | % 41, 42
   bes8 bes a a g g f f | % 43
   es8 es f f bes,4 r | % 44
   R1*4 | % 45, 46, 47, 48
   g'8 g f f es es d d | % 49
   % \break
   c8 c d d g,4 r | %  50
   b'!8 (b) b (b) c (c) c (c) | % 51
   a8 (a) a (a) bes bes bes bes | % 52
   g8 g g g a a a a | % 53
   % \break
   fis8 fis fis fis g g g g | % 54
   a8 a a a bes bes bes bes | % 55
   c8 f d c d d c c | % 56
   % \break
   bes8 g f f es es bes c | % 57
   d8 c d d g,4 r | % 58
   es''8 es es es d d d d | % 59
   % \break
   c8 c c c bes bes bes bes  | % 60
   as8 as as as g g g g | % 61
   f8 f f f es4 r | % 62
   R1*3 | % 63, 64, 65
   % \break
   es8 es es es e! e e e | % 66
   f8 f f f fis fis fis fis | % 67
   g8 g g g g g g g | % 68
   % \break
   as8 as as as a! a a a | %69
   bes4 r r2 | % 70
   R1*3 | % 71, 72, 73
   bes8 bes bes bes f f f f | % 74
   c8 c es es g g b! b | % 75
   % \break
   c8 c g g c, c c' c | % 76
   a8 a f f bes, bes bes c | % 77
   d8 d d d es es es es | % 78
   % \break
   e!8 e e e f4 a | % 79
   bes8 bes bes bes a a a a | % 80
   g8 g g g f f f f | % 81
   es8 es es es d d d d | % 82
   % \break
   c8 c c es f f f a, | % 83
   bes8 c d es f g a bes | % 84
   es,8 es es es f f f f | % 85
   % \break
   \repeat unfold 16 {8}  % 86, 87
   bes,8 c d es f es d es | % 88
   f8 es f f, bes4 r | % 89
   \bar "|."
    }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

%\markup \column {
%  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
%  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
%  \hspace #1
%}

\markup {
  \teeny
  \date
}