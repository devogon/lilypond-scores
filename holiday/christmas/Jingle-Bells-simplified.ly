\version "2.19.50"
\language "nederlands"

\header {
  title = "Jingle Bells"
  subtitle = "simplified"
  arranger = "J. Pierpont"
  meter = "med. fast"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=100
}

violin = \relative c'' {
  \global
				% Music follows here.
  b4\staccato b\staccato b2 | % 1
  b4\staccato b\staccato b2 | % 2
  b4 d g, a | % 3
  b1 | % 4
  c4 c c c | % 5
  c4 b b b8 b | % 6
  b4 a a b | % 7
  a2 d | % 8
  b4\staccato b\staccato b2 | % 9
  b4\staccato b\staccato b2 | % 10
  b4 d g, a | % 11
  b2. r4 | % 12
  c4 c c c | % 13
  c4 b b b8 b | % 14
  d4 d c a | % 15
  g1 | % 16
  \bar "|."
  % fis,4\staccato fis\staccato fis2 | % 1
  % fis4\staccato fis\staccato fis2  | % 2
  % fis4 a d,4. e8  | % 3
  % fis1 | % 4
  % g4 g g4. g8 | % 5
  % g4 (fis) fis fis8 fis | % 6
  % fis4 e e fis | % 7
  % e2 a | % 8
  % fis4\staccato fis\staccato fis2 | % 9
  % fis4\staccato fis\staccato fis2  | % 10
  % fis4 a d,4. e8  | % 11
  % fis2. r4 | % 12
  % g4 g g4. g8 | % 13
  % g4 (fis) fis fis8 fis | % 14
  % a4 a fis e | % 15
  % d1 | % 16
  % \bar "|."
  % one horse o -- pen sleigh
}

flute = \relative c'' {
  \global
  % Music follows here.
  b4 b b2 | % 1
  b4 b b2 | % 2
  b4 d g, a | % 3
  b1 | % 4
  c4 c c c | % 5
  c4 b b b8 b | % 6
  b4 a a b | % 7
  a2 d | % 8
  b4 b b2 | % 9
  b4 b b2 | % 10
  b4 d g, a | % 11
  b2. r4 | % 12
  c4 c c c | % 13
  c4 b b b8 b | % 14
  d4 d c a | % 15
  g1 | % 16
  \bar "|."
}

cello = \relative c {
  \global
  % Music follows here.
  r4 d8 d r4 d8 d | % 1
  r4 d8 d r4 d8 d | % 2
  r4 a'8 a r4 a8 a | % 3
  r4 a8 a r4 a8 a | % 4
  r4 g8 g r4 g8 g | % 5
  r4 fis8 fis r4 fis8 fis | % 6
  r4 e8 e r4 e8 e | % 7
  r4 a,8 a r4 a8 a | % 8
  r4 d8 d r4 d8 d | % 9
  r4 d8 d r4 d8 d | % 10
  r4 a'8 a r4 a8 a | % 11
  r4 a8 a r4 a8 a | % 12
  r4 g8 g r4 g8 g | % 13
  r4 fis8 fis r4 fis8 fis | % 14
  r4 g8 g r4 a, | % 15
  d1| % 16
  \bar "|."
}

contrabass = \relative c {
  \global
  % Music follows here.
  d2 a | % 1
  d2 a | % 2
  d2 a | % 3
  d2 a | % 4
  e'2 a, | % 5
  e'2 a, | % 6
  e'2 a, | % 7
  e'2 a, | % 8
  d2 a | % 9
  d2 a | % 10
  d2 a | % 11
  d2.  r4 | % 12
  d2 a | % 13
  d2 a| % 14
  e'2 a, | % 15
  e1   | % 16
  \bar "|."
}

words = \lyricmode {
  Jin -- gle bells, jin -- gle bells, jin -- gle all the way
  oh, what fun it is to ride in a one horse o -- pen sleigh!
    Jin -- gle bells, jin -- gle bells, jin -- gle all the way
  oh, what fun it is to ride in a one horse o -- pen sleigh!
}
violinPart = \new Staff \with {
  instrumentName = "Violin"
  shortInstrumentName = "Vl."
  midiInstrument = "violin"
} \violin

flutePart = \new Staff \with {
  instrumentName = "Flute"
  shortInstrumentName = "Fl."
  midiInstrument = "flute"
} \flute

celloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \cello }

contrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \contrabass }

\score {
  <<
    \violinPart
    \flutePart
    \celloPart
    \contrabassPart
  >>
  \layout { }
  \midi { }
}

\markup { \column {
  \line{ Jingle bells, jingle bells, }
  \line{ Jingle all the way, }
  \line{ O what fun it is to ride }
  \line{ in a one-horse open sleigh!}
  \line{ Jingle bells, jingle bells, }
  \line{ Jingle all the way, }
  \line{ O what fun it is to ride }
  \line{ In a one-horse open sleigh! }
          }
}