\version "2.19.50"
\language "nederlands"

\header {
  title = "One Horse Open Sleigh"
  composer = "James Lord Pierpont"
  meter = "Allegretto"
%  copyright = "18XX"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key g \major
  \time 4/4
%  \tempo 4=100
}

violin = \relative c'' {
  \global
  % Music follows here.
  d,4\mp b' a g | % 1
  d2. d8 d | % 2
  d4 b' a g | % 3
  e2. e4 | % 4
%  \break
  e4 c' b a | % 5
  fis2. fis4 | % 6
  d'4 d c a | % 7
  b1 | % 8
  d,4 b' a g | % 9
%  \break
  d2. d4 | % 10
  d4 b' a g | % 11
  e2. e4 | % 12
  e4 c' b a | % 13
  d4 d d d | % 14
%  \break
  e4 d c a | % 15
  g2. r4 | % 16
  \repeat volta 2 {
  b4\f b b2 | % 1
  b4 b b2 | % 2
  b4 d <g, c,>4. a8 | % 3
  b2. r4 | % 4
  c4 c c4. c8 | % 5
  c4 b b b8 b| % 6
  }
  \alternative {
    { b4 a a b| % 7
      a2 d2 | % 8
    }
    {
      d4 d c a | % 7
      g2 <b g'>4-> r4
    }
  }
}

flute = \relative c'' {
  \global
  % Music follows here.
  R1*16
  \repeat volta 2 {
    b4\f b b2 | % 1
  b4 b b2 | % 2
  b4 d g, a | % 3
  b2. r4 | % 4
  c4 c c4. c8 | % 5
  c4 b b b8 b| % 6
  }
  \alternative {
    { b4 a a b| % 7
      a2 d2 | % 8
    }
    {d4 d c a | % 7
      g1
    }
  }
}

cello = \relative c {
  \global
  % Music follows here.
  b4\mp d c b | % 1
  b2 g4 b8 b | %  2
  b4 d c b | % 3
  c2 g4 c | % 4
  c4 e d c | % 5
  d2 a4 d | % 6
  fis4 fis fis2 | % 7
  g2 d | % 8
  b4 d c b | % 9
  b2 g4 b8 b | % 10
  b4 d c b | % 11
  c2 g4 c | % 12
  c4 e d c | % 13
  fis4 fis fis2 | % 14
  d2 fis2 | % 15
  g2 <d b> | % 16
  \repeat volta 2 {
    g,4\f g' d g | %  1
    g,4 g' d g | % 2
    g,4 g' e4. fis8 % 3
    g,4 g' d g | % 4
    c,4 e g c | % 5
    g,4 g' d g | % 6
  }
  \alternative {
    {
      g,4 cis e a
      d,4 c b a
    }
    {
      fis'4 d e fis
      g2 <g, d' b'>4-> r4
    }
  }
  \bar "|."
}

contrabass = \relative c {
  \global
  % Music follows here.
  R1^\markup{\tiny "G"}
  R1
  R1
  R1^\markup{\tiny "C"}
  R1^\markup{\tiny "Am"}
  R1^\markup{\tiny "D7"}
  R1
  R1^\markup{\tiny "G"}
  R1
  R1
  R1
  R1^\markup{\tiny "C"}
  R1^\markup{\tiny "Am"}
  R1^\markup{\tiny "G"}
  R1^\markup{\tiny "D7"}
  R1^\markup{\tiny "G - D7"}
  \repeat volta 2 {
    g2^\markup{\tiny "G"} d' | % 1
    g,2 d' | %  2
    g,2 e' | % 3
    g,2 d' | % 4
    c2^\markup{\tiny "C"} g' | % 5
    g,2^\markup{\tiny "G"} d' | % 6
  }
  \alternative {
    {	g,2^\markup{\tiny "A7"} e' | % 7
        d2^\markup{\tiny "D7"} g | %  8
    }
    {	d2 fis^\markup{\tiny "? (D7= D Fis A C)"} | %  7
        g,2^\markup{\tiny "G"} r | % 8
    }
  }
}

violinPart = \new Staff \with {
  instrumentName = "Violin"
  shortInstrumentName = "Vl."
  midiInstrument = "violin"
} \violin

flutePart = \new Staff \with {
  instrumentName = "Flute"
  shortInstrumentName = "Fl."
  midiInstrument = "flute"
} \flute

celloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \cello }

contrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \contrabass }

\score {
  <<
    \violinPart
    \flutePart
    \celloPart
    \contrabassPart
    \new GrandStaff << >>
  >>
  \layout { }
  \midi { }
}

\markup {
  \teeny
  \date
}