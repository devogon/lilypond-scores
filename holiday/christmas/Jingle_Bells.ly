\version "2.19.50"
% automatically converted by musicxml2ly from /home/ack/MuseScore2/Scores/Jingle_Bells.xml
\pointAndClickOff

\include "articulate.ly"

\header {
  encodingsoftware =  "MuseScore 2.0.3"
  title =  "Jingle Bells"
  composer =  Traditional
  encodingdate =  "2016-12-12"
}

PartPOneVoiceOne =  \relative a {
  \clef "treble" \key d \major \numericTimeSignature\time 4/4 | % 1
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp a4. \stemUp a16
  \stemUp a16 | % 2
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp b4. \stemUp b16
  \stemUp b16 | % 3
  \stemUp b8 \stemUp g'8 \stemUp fis8 \stemUp e8 \stemUp cis4. \stemUp
  cis16 \stemUp cis16 | % 4
  \stemUp a'8 \stemUp a8 \stemUp fis8 \stemUp e8 \stemUp fis4. \stemUp
  a,16 \stemUp a16 | % 5
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp a4. \stemUp a16
  \stemUp a16 | % 6
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp b4. \stemUp b16
  \stemUp b16 | % 7
  \stemUp b8 \stemUp g'8 \stemUp fis8 \stemUp e8 \stemUp a8 \stemUp a8
  \stemUp a8 \stemUp a8 | % 8
  \stemUp b8 \stemUp a8 \stemUp fis8 \stemUp e8 \stemUp d4 -. \stemUp
  a'4 -- | % 9
  \stemUp fis8 -. \stemUp fis8 -. \stemUp fis4 \stemUp fis8 -. \stemUp
  fis8 -. \stemUp fis4 | \barNumberCheck #10
  \stemUp fis8 \stemUp a8 \stemUp d,8. \stemUp e16 \stemUp fis2 | % 11
  \stemUp g8 \stemUp g8 \stemUp g8. \stemUp g16 \stemUp g8 ( \stemUp
  fis8 ) \stemUp fis8 \stemUp fis16 \stemUp fis16 | % 12
  \stemUp fis8 \stemUp e8 \stemUp e8 \stemUp fis8 \stemUp e4 \stemUp a4
  | % 13
  \stemUp fis8 -. \stemUp fis8 -. \stemUp fis4 \stemUp fis8 -. \stemUp
  fis8 -. \stemUp fis4 | % 14
  \stemUp fis8 \stemUp a8 \stemUp d,8. \stemUp e16 \stemUp fis2 | % 15
  \stemUp g8 \stemUp g8 \stemUp g8. \stemUp g16 \stemUp g8 ( \stemUp
  fis8 ) \stemUp fis8 \stemUp fis16 \stemUp fis16 | % 16
  \stemUp a8 \stemUp a8 \stemUp fis8 \stemUp e8 \stemUp d2 | % 17
  \stemUp fis8 - "pizz." \stemUp fis8 \stemUp fis4 \stemUp fis8
  \stemUp fis8 \stemUp fis4 | % 18
  \stemUp fis8 \stemUp a8 \stemUp d,8. \stemUp e16 \stemUp fis2 | % 19
  \stemUp g8 \stemUp g8 \stemUp g8. \stemUp g16 \stemUp g8 \stemUp fis8
  \stemUp fis8 \stemUp fis8 | \barNumberCheck #20
  \stemUp fis8 \stemUp e8 \stemUp e8 \stemUp fis8 \stemUp a4 \stemUp
  a,4 | % 21
  \stemUp fis'8 \stemUp fis8 \stemUp fis4 \stemUp fis8 \stemUp fis8
  \stemUp fis4 | % 22
  \stemUp fis8 \stemUp a8 \stemUp d,8. \stemUp e16 \stemUp fis2 | % 23
  \stemUp g8 \stemUp g8 \stemUp g8. \stemUp g16 \stemUp g8 \stemUp fis8
  \stemUp fis8 \stemUp fis8 | % 24
  \stemUp a8 \stemUp a8 \stemUp fis8 \stemUp e8 \stemUp d2 | % 25
  \stemUp fis8 -. - "arco" \stemUp fis8 -. \stemUp fis4 \stemUp fis8
  -. \stemUp fis8 -. \stemUp fis4 | % 26
  \stemUp fis8 \stemUp a8 \stemUp d,8. \stemUp e16 \stemUp fis2 | % 27
  \stemUp g8 \stemUp g8 \stemUp g8. \stemUp g16 \stemUp g8 ( \stemUp
  fis8 ) \stemUp fis8 \stemUp fis16 \stemUp fis16 | % 28
  \stemUp fis8 \stemUp e8 \stemUp e8 \stemUp fis8 \stemUp e4 \stemUp a4
  | % 29
  \stemUp fis8 -. \stemUp fis8 -. \stemUp fis4 \stemUp fis8 -. \stemUp
  fis8 -. \stemUp fis4 | \barNumberCheck #30
  \stemUp fis8 \stemUp a8 \stemUp d,8. \stemUp e16 \stemUp fis2 | % 31
  \stemUp g8 \stemUp g8 \stemUp g8. \stemUp g16 \stemUp g8 ( \stemUp
  fis8 ) \stemUp fis8 \stemUp fis16 \stemUp fis16 | % 32
  \stemUp a8 \stemUp a8 \stemUp fis8 \stemUp e8 \stemUp d2 | % 33
  \stemDown d''16 \stemDown cis16 \stemDown b16 \stemDown a16
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown
  d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown g16
  \stemDown a16 \stemDown b16 \stemDown cis16 | % 34
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown
  d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown d16
  \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown g16 \stemDown
  a16 \stemDown b16 \stemDown cis16 | % 35
  \stemDown e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown
  e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown e16
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown cis16
  \stemDown a16 \stemDown b16 \stemDown cis16 | % 36
  \stemDown e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown
  e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown e16
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown cis16
  \stemDown a16 \stemDown b16 \stemDown cis16 | % 37
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown
  d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown d16
  \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown g16 \stemDown
  a16 \stemDown b16 \stemDown cis16 | % 38
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown
  d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown d16
  \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown g16 \stemDown
  a16 \stemDown b16 \stemDown cis16 | % 39
  \stemDown e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown
  e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown e16
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown cis16
  \stemDown a16 \stemDown b16 \stemDown cis16 | \barNumberCheck #40
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown
  fis16 \stemDown e16 \stemDown d16 \stemDown cis16 \stemDown d2 | % 41
  \stemDown e'16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown
  e16 \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown e16
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown cis16
  \stemDown a16 \stemDown b16 \stemDown cis16 | % 42
  \stemDown d16 \stemDown cis16 \stemDown b16 \stemDown a16 \stemDown
  fis16 \stemDown e16 \stemDown d16 \stemDown cis16 \stemDown d2
  \fermata \bar "|."
}

PartPTwoVoiceOne =  \relative a {
  \clef "treble" \key d \major \numericTimeSignature\time 4/4 | % 1
  r8 \stemUp a8 \stemUp a8 \stemUp a8 r4 r8 \stemUp a16 \stemUp a16 | % 2
  r8 \stemUp a8 \stemUp a8 \stemUp a8 r4 r8 \stemUp b16 \stemUp b16 | % 3
  r8 \stemUp b8 \stemUp b8 \stemUp b8 r4 r8 \stemUp cis16 \stemUp cis16
  | % 4
  r8 \stemUp cis8 \stemUp cis8 \stemUp cis8 r4 r8 \stemUp a16 \stemUp
  a16 | % 5
  r8 \stemUp a8 \stemUp a8 \stemUp a8 r4 r8 \stemUp a16 \stemUp a16 | % 6
  r8 \stemUp a8 \stemUp a8 \stemUp a8 r4 r8 \stemUp b16 \stemUp b16 | % 7
  r8 \stemUp b8 \stemUp b8 \stemUp b8 r4 r8 \stemUp cis16 \stemUp cis16
  | % 8
  r8 \stemUp cis8 \stemUp cis8 \stemUp cis8 \stemUp d4 -. \stemUp d4
  -- | % 9
  \stemDown a''8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -.
  \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. |
  \barNumberCheck #10
  \stemDown d,8 -. \stemDown d8 -. \stemDown d8 -. \stemDown d8 -.
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 11
  \stemDown cis,8 -. \stemDown cis8 -. \stemDown cis8 -. \stemDown cis8
  -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. | % 12
  \stemDown d8 -. \stemDown d8 -. \stemDown d8 -. \stemDown d8 -.
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 13
  \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -.
  \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 14
  \stemDown d,8 -. \stemDown d8 -. \stemDown d8 -. \stemDown d8 -.
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 15
  \stemDown cis,8 -. \stemDown cis8 -. \stemDown cis8 -. \stemDown cis8
  -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. | % 16
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -.
  \stemDown fis2 | % 17
  r8 \stemUp a,8 r8 \stemUp a8 r8 \stemUp a8 r8 \stemUp a8 | % 18
  r8 \stemUp d,8 r8 \stemUp d8 r8 \stemUp d8 r8 \stemUp d8 | % 19
  r8 \stemUp a'8 r8 \stemUp a8 r8 \stemUp a8 r8 \stemUp a8 |
  \barNumberCheck #20
  r8 \stemUp d,8 r8 \stemUp d8 r8 \stemUp d8 r8 \stemUp d8 | % 21
  r8 \stemUp a'8 r8 \stemUp a8 r8 \stemUp a8 r8 \stemUp a8 | % 22
  r8 \stemUp d,8 r8 \stemUp d8 r8 \stemUp d8 r8 \stemUp d8 | % 23
  r8 \stemUp a'8 r8 \stemUp a8 r8 \stemUp a8 r8 \stemUp a8 | % 24
  r8 \stemUp d,8 r8 \stemUp d8 r8 \stemUp d8 r8 \stemUp d8 | % 25
  \stemDown a''8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -.
  \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 26
  \stemDown d,8 -. \stemDown d8 -. \stemDown d8 -. \stemDown d8 -.
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 27
  \stemDown cis,8 -. \stemDown cis8 -. \stemDown cis8 -. \stemDown cis8
  -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. | % 28
  \stemDown d8 -. \stemDown d8 -. \stemDown d8 -. \stemDown d8 -.
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 29
  \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -.
  \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. |
  \barNumberCheck #30
  \stemDown d,8 -. \stemDown d8 -. \stemDown d8 -. \stemDown d8 -.
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -. | % 31
  \stemDown cis,8 -. \stemDown cis8 -. \stemDown cis8 -. \stemDown cis8
  -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. \stemDown b8 -. | % 32
  \stemDown a'8 -. \stemDown a8 -. \stemDown a8 -. \stemDown a8 -.
  \stemDown fis2 | % 33
  \stemDown fis8 -. \stemDown fis8 -. \stemDown fis4 \stemDown fis8 -.
  \stemDown fis8 -. \stemDown fis4 | % 34
  \stemDown fis8 \stemDown a8 \stemDown d,8. \stemDown e16 \stemDown
  fis2 | % 35
  \stemDown g8 \stemDown g8 \stemDown g8. \stemDown g16 \stemDown g8
  \stemDown fis8 \stemDown fis8 \stemDown fis16 \stemDown fis16 | % 36
  \stemDown fis8 \stemDown e8 \stemDown e8 \stemDown fis8 \stemDown e4
  -. \stemDown a4 -- | % 37
  \stemDown fis8 -. \stemDown fis8 -. \stemDown fis4 \stemDown fis8 -.
  \stemDown fis8 -. \stemDown fis4 | % 38
  \stemDown fis8 \stemDown a8 \stemDown d,8. \stemDown e16 \stemDown
  fis2 | % 39
  \stemDown g8 \stemDown g8 \stemDown g8. \stemDown g16 \stemDown g8
  \stemDown fis8 \stemDown fis8 \stemDown fis16 \stemDown fis16 |
  \barNumberCheck #40
  \stemDown a8 \stemDown a8 \stemDown fis8 \stemDown e8 \stemDown d2 | % 41
  \stemDown g8 \stemDown g8 \stemDown g8. \stemDown g16 \stemDown g8
  \stemDown fis8 \stemDown fis8 \stemDown fis16 \stemDown fis16 | % 42
  \stemDown a8 \stemDown a8 \stemDown b8 \stemDown cis8 \stemDown d2
  \bar "|."
}

PartPThreeVoiceOne =  \relative d {
  \clef "alto" \key d \major \numericTimeSignature\time 4/4 | % 1
  \stemUp d2.. \stemUp d16 \stemUp d16 | % 2
  \stemUp d2.. \stemUp e16 \stemUp e16 | % 3
  \stemUp e2.. \stemUp fis16 \stemUp fis16 | % 4
  \stemUp fis2.. \stemUp d16 \stemUp d16 | % 5
  \stemUp d2.. \stemUp d16 \stemUp d16 | % 6
  \stemUp d2.. \stemUp e16 \stemUp e16 | % 7
  \stemUp e2.. \stemUp fis16 \stemUp fis16 | % 8
  \stemUp fis2 \stemUp d4 -. \stemUp d4 -- | % 9
  \stemDown d'8 -. \stemDown d8 -. \stemDown d4 \stemDown d8 -.
  \stemDown d8 -. \stemDown d4 | \barNumberCheck #10
  \stemDown d8 \stemDown fis8 \stemUp a,8. \stemUp cis16 \stemDown d2
  | % 11
  \stemDown e8 \stemDown e8 \stemDown e8. \stemDown e16 \stemDown e8 (
  \stemDown d8 ) \stemDown d8 \stemDown d16 \stemDown d16 | % 12
  \stemUp d8 \stemUp a8 \stemUp a8 \stemUp d8 \stemDown cis4 \stemDown
  e4 | % 13
  \stemDown d8 -. \stemDown d8 -. \stemDown d4 \stemDown d8 -.
  \stemDown d8 -. \stemDown d4 | % 14
  \stemDown d8 \stemDown fis8 \stemUp a,8. \stemUp cis16 \stemDown d2
  | % 15
  \stemDown e8 \stemDown e8 \stemDown e8. \stemDown e16 \stemDown e8 (
  \stemDown d8 ) \stemDown d8 \stemDown d16 \stemDown d16 | % 16
  \stemUp d8 \stemUp a8 \stemUp b8 \stemUp cis8 \stemDown d2 | % 17
  \stemDown d4 - "pizz." \stemUp a4 \stemDown d4 \stemUp a4 | % 18
  \stemDown d4 \stemUp g,4 \stemDown d'4 \stemUp g,4 | % 19
  \stemDown d'4 \stemUp a4 \stemDown d4 \stemUp a4 | \barNumberCheck
  #20
  \stemDown d4 \stemUp g,4 \stemDown d'4 \stemUp g,4 | % 21
  \stemDown d'4 \stemUp a4 \stemDown d4 \stemUp a4 | % 22
  \stemDown d4 \stemUp g,4 \stemDown d'4 \stemUp g,4 | % 23
  \stemDown d'4 \stemUp a4 \stemDown d4 \stemUp a4 | % 24
  \stemDown d4 \stemUp g,4 \stemDown d'4 \stemUp g,4 | % 25
  \stemDown d'8 -. - "arco" \stemDown d8 -. \stemDown d4 \stemDown d8
  -. \stemDown d8 -. \stemDown d4 | % 26
  \stemDown d8 \stemDown fis8 \stemUp a,8. \stemUp cis16 \stemDown d2
  | % 27
  \stemDown e8 \stemDown e8 \stemDown e8. \stemDown e16 \stemDown e8 (
  \stemDown d8 ) \stemDown d8 \stemDown d16 \stemDown d16 | % 28
  \stemUp d8 \stemUp a8 \stemUp a8 \stemUp d8 \stemDown cis4 \stemDown
  e4 | % 29
  \stemDown d8 -. \stemDown d8 -. \stemDown d4 \stemDown d8 -.
  \stemDown d8 -. \stemDown d4 | \barNumberCheck #30
  \stemDown d8 \stemDown fis8 \stemUp a,8. \stemUp cis16 \stemDown d2
  | % 31
  \stemDown e8 \stemDown e8 \stemDown e8. \stemDown e16 \stemDown e8 (
  \stemDown d8 ) \stemDown d8 \stemDown d16 \stemDown d16 | % 32
  \stemUp d8 \stemUp a8 \stemUp b8 \stemUp cis8 \stemDown d2 | % 33
  r8 \stemDown fis8 \stemDown fis8 \stemDown fis8 r8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 | % 34
  r8 \stemDown fis8 \stemDown fis8 \stemDown fis8 r8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 | % 35
  r8 \stemDown g8 \stemDown g8 \stemDown g8 r8 \stemDown a8 \stemDown
  a8 \stemDown a8 | % 36
  r8 \stemDown fis8 \stemDown fis8 \stemDown fis8 r8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 | % 37
  r8 \stemDown fis8 \stemDown fis8 \stemDown fis8 r8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 | % 38
  r8 \stemDown fis8 \stemDown fis8 \stemDown fis8 r8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 | % 39
  r8 \stemDown g8 \stemDown g8 \stemDown g8 r8 \stemDown a8 \stemDown
  a8 \stemDown a8 | \barNumberCheck #40
  r8 \stemDown cis,8 \stemDown d8 \stemDown e8 \stemDown fis2 | % 41
  r8 \stemDown g8 \stemDown g8 \stemDown g8 r8 \stemDown a8 \stemDown
  a8 \stemDown a8 | % 42
  r8 \stemDown cis,8 \stemDown d8 \stemDown e8 \stemDown fis2 \bar
  "|."
}

PartPFourVoiceOne =  \relative a, {
  \clef "bass" \key d \major \numericTimeSignature\time 4/4 | % 1
  \stemUp a4 -. \stemUp a4 -. \stemUp a4 -. \stemUp a8 \stemUp a16
  \stemUp a16 | % 2
  \stemUp a4 \stemUp a4 \stemUp a4 \stemUp a8 \stemUp b16 \stemUp b16
  | % 3
  \stemUp b4 \stemUp b4 \stemUp b4 \stemUp b8 \stemUp cis16 \stemUp
  cis16 | % 4
  \stemUp cis4 \stemUp cis4 \stemUp cis4 \stemUp cis8 \stemUp a16
  \stemUp a16 | % 5
  \stemUp a4 -. \stemUp a4 -. \stemUp a4 -. \stemUp a8 \stemUp a16
  \stemUp a16 | % 6
  \stemUp a4 \stemUp a4 \stemUp a4 \stemUp a8 \stemUp b16 \stemUp b16
  | % 7
  \stemUp b4 \stemUp b4 \stemUp b4 \stemUp b8 \stemUp cis16 \stemUp
  cis16 | % 8
  \stemUp cis8 \stemUp cis8 \stemUp cis8 \stemUp cis8 \stemDown d4 -.
  \stemDown d4 -- | % 9
  r8 \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 r8
  \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 |
  \barNumberCheck #10
  r8 \stemDown a'16 \stemDown a16 r8 \stemDown a16 \stemDown a16 r8
  \stemDown a16 \stemDown a16 r8 \stemDown a16 \stemDown a16 | % 11
  r8 \stemDown g16 \stemDown g16 r8 \stemDown g16 \stemDown g16 r8
  \stemDown fis16 \stemDown fis16 r8 \stemDown fis16 \stemDown fis16 | % 12
  r8 \stemDown e16 \stemDown e16 r8 \stemDown e16 \stemDown e16 r8
  \stemUp a,16 \stemUp a16 r8 \stemUp a16 \stemUp a16 | % 13
  r8 \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 r8
  \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 | % 14
  r8 \stemDown a'16 \stemDown a16 r8 \stemDown a16 \stemDown a16 r8
  \stemDown a16 \stemDown a16 r8 \stemDown a16 \stemDown a16 | % 15
  r8 \stemDown g16 \stemDown g16 r8 \stemDown g16 \stemDown g16 r8
  \stemDown fis16 \stemDown fis16 r8 \stemDown fis16 \stemDown fis16 | % 16
  r8 \stemDown a16 \stemDown a16 r8 \stemUp a,8 \stemDown d2 | % 17
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp a4. \stemUp a16
  \stemUp a16 | % 18
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp b4. \stemUp b16
  \stemUp b16 | % 19
  \stemDown b8 \stemDown g'8 \stemDown fis8 \stemDown e8 \stemUp cis4.
  \stemUp cis16 \stemUp cis16 | \barNumberCheck #20
  \stemDown a'8 \stemDown a8 \stemDown fis8 \stemDown e8 \stemDown fis4.
  \stemUp a,16 \stemUp a16 | % 21
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp a4. \stemUp a16
  \stemUp a16 | % 22
  \stemUp a8 \stemUp fis'8 \stemUp e8 \stemUp d8 \stemUp b4. \stemUp b16
  \stemUp b16 | % 23
  \stemDown b8 \stemDown g'8 \stemDown fis8 \stemDown e8 \stemDown a8
  \stemDown a8 \stemDown a8 \stemDown a8 | % 24
  \stemDown b8 \stemDown a8 \stemDown fis8 \stemDown e8 \stemDown d4
  -. \stemDown a'4 -- | % 25
  r8 \stemDown d,16 \stemDown d16 r8 \stemDown d16 \stemDown d16 r8
  \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 | % 26
  r8 \stemDown a'16 \stemDown a16 r8 \stemDown a16 \stemDown a16 r8
  \stemDown a16 \stemDown a16 r8 \stemDown a16 \stemDown a16 | % 27
  r8 \stemDown g16 \stemDown g16 r8 \stemDown g16 \stemDown g16 r8
  \stemDown fis16 \stemDown fis16 r8 \stemDown fis16 \stemDown fis16 | % 28
  r8 \stemDown e16 \stemDown e16 r8 \stemDown e16 \stemDown e16 r8
  \stemUp a,16 \stemUp a16 r8 \stemUp a16 \stemUp a16 | % 29
  r8 \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 r8
  \stemDown d16 \stemDown d16 r8 \stemDown d16 \stemDown d16 |
  \barNumberCheck #30
  r8 \stemDown a'16 \stemDown a16 r8 \stemDown a16 \stemDown a16 r8
  \stemDown a16 \stemDown a16 r8 \stemDown a16 \stemDown a16 | % 31
  r8 \stemDown g16 \stemDown g16 r8 \stemDown g16 \stemDown g16 r8
  \stemDown fis16 \stemDown fis16 r8 \stemDown fis16 \stemDown fis16 | % 32
  r8 \stemDown a16 \stemDown a16 r8 \stemUp a,8 \stemDown d2 | % 33
  \stemDown d'8 \stemDown d8 \stemDown d8 \stemDown d8 \stemDown d8
  \stemDown d8 \stemDown d8 \stemDown d8 | % 34
  \stemDown a8 \stemDown a8 \stemDown a8 \stemDown a8 \stemDown a8
  \stemDown d,8 ( \stemDown e8 \stemDown fis8 ) | % 35
  \stemDown g8 \stemDown g8 \stemDown g8 \stemDown g8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 \stemDown fis8 | % 36
  \stemDown e8 \stemDown e8 \stemDown fis8 \stemDown fis8 \stemDown g8
  \stemDown g8 \stemDown a8 \stemDown a8 | % 37
  \stemDown d8 \stemDown d8 \stemDown d8 \stemDown d8 \stemDown d8
  \stemDown d8 \stemDown d8 \stemDown d8 | % 38
  \stemDown a8 \stemDown a8 \stemDown a8 \stemDown a8 \stemDown a8
  \stemDown d,8 ( \stemDown e8 \stemDown fis8 ) | % 39
  \stemDown g8 \stemDown g8 \stemDown g8 \stemDown g8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 \stemDown fis8 | \barNumberCheck #40
  \stemDown a8 \stemDown a8 \stemDown b8 \stemDown cis8 \stemDown d8
  \stemDown d8 \stemDown d8 \stemDown d8 | % 41
  \stemDown g,8 \stemDown g8 \stemDown g8 \stemDown g8 \stemDown fis8
  \stemDown fis8 \stemDown fis8 \stemDown fis8 | % 42
  \stemDown a8 \stemDown g8 \stemDown fis8 \stemDown e8 \stemUp a,2
  \bar "|."
}

PartPFiveVoiceOne =  \relative a,, {
  \clef "bass_8" \key d \major \numericTimeSignature\time 4/4 | % 1
  \stemUp a2.. \stemUp a16 \stemUp a16 | % 2
  \stemUp a2.. \stemUp e16 \stemUp e16 | % 3
  \stemUp e2.. \stemUp e16 \stemUp e16 | % 4
  \stemUp e2.. \stemUp a16 \stemUp a16 | % 5
  \stemUp a2.. \stemUp a16 \stemUp a16 | % 6
  \stemUp a2.. \stemUp e16 \stemUp e16 | % 7
  \stemUp e2.. \stemUp e16 \stemUp e16 | % 8
  \stemUp e8 \stemUp e8 \stemUp e8 \stemUp e8 \stemUp a4 -. \stemUp a4
  -- | % 9
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | \barNumberCheck
  #10
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | % 11
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 12
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 13
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | % 14
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | % 15
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 16
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 17
  \stemDown d4. \stemDown d8 \stemUp a4. \stemUp a8 | % 18
  \stemDown d4. \stemDown d8 \stemUp g,4. \stemUp g8 | % 19
  \stemDown d'4. \stemDown d8 \stemUp a4. \stemUp a8 | \barNumberCheck
  #20
  \stemDown a'4. \stemDown a4 \stemDown g8 \stemDown fis8 \stemDown e8
  | % 21
  \stemDown d4. \stemDown d8 \stemUp a4. \stemUp a8 | % 22
  \stemDown d4. \stemDown d8 \stemUp g,4. \stemUp g8 | % 23
  \stemDown d'4. \stemDown d8 \stemUp a4. \stemUp a8 | % 24
  \stemDown a'4. \stemDown a4 \stemDown g8 \stemDown fis8 \stemDown e8
  | % 25
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | % 26
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | % 27
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 28
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 29
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | \barNumberCheck
  #30
  \stemDown d4 \stemUp a4 \stemDown d4 \stemUp a4 | % 31
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 32
  \stemDown e'4 \stemUp a,4 \stemDown e'4 \stemUp a,4 | % 33
  \stemDown d4 \stemDown d4 \stemUp cis4 \stemUp cis4 | % 34
  \stemUp b4 \stemUp b4 \stemUp a4 \stemUp a4 | % 35
  \stemUp g4 \stemUp g4 \stemDown d'4 \stemDown d4 | % 36
  \stemUp cis4 \stemUp cis4 \stemUp a4 \stemUp a4 | % 37
  \stemDown d4 \stemDown d4 \stemUp cis4 \stemUp cis4 | % 38
  \stemUp b4 \stemUp b4 \stemUp a4 \stemUp a4 | % 39
  \stemUp g4 \stemUp g4 \stemDown d'4 \stemDown d4 | \barNumberCheck
  #40
  \stemUp cis4 \stemUp cis4 \stemUp a4 \stemUp a4 | % 41
  \stemDown d4 \stemDown d4 \stemUp cis4 \stemUp cis4 | % 42
  \stemUp a4 \stemUp a4 \stemDown d2 \bar "|."
}


% The score definition
\score {
  <<

    \new Staff
    <<
      \set Staff.instrumentName = "Violin 1"
      \set Staff.shortInstrumentName = "Vln. 1"

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \PartPOneVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Violin 2"
      \set Staff.shortInstrumentName = "Vln. 2"

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPTwoVoiceOne" {  \PartPTwoVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Viola"
      \set Staff.shortInstrumentName = "Vla."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPThreeVoiceOne" {  \PartPThreeVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Cello"
      \set Staff.shortInstrumentName = "C."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPFourVoiceOne" {  \PartPFourVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Bass"
      \set Staff.shortInstrumentName = "B."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPFiveVoiceOne" {  \PartPFiveVoiceOne }
      >>
    >>

  >>
  \layout {}
}
\score {
  \unfoldRepeats \articulate {

    \new Staff
    <<
      \set Staff.instrumentName = "Violin 1"
      \set Staff.shortInstrumentName = "Vln. 1"

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \PartPOneVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Violin 2"
      \set Staff.shortInstrumentName = "Vln. 2"

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPTwoVoiceOne" {  \PartPTwoVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Viola"
      \set Staff.shortInstrumentName = "Vla."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPThreeVoiceOne" {  \PartPThreeVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Cello"
      \set Staff.shortInstrumentName = "C."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPFourVoiceOne" {  \PartPFourVoiceOne }
      >>
    >>
    \new Staff
    <<
      \set Staff.instrumentName = "Bass"
      \set Staff.shortInstrumentName = "B."

      \context Staff <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPFiveVoiceOne" {  \PartPFiveVoiceOne }
      >>
    >>

  }
  \midi {\tempo 4 = 100 }
}

