\version "2.19.50"
\language "nederlands"

\header {
  title = "Jingle Bells"
  arranger = "J. Pierpont"
  meter = "med. fast"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=100
}

violin = \relative c'' {
  \global
  % Music follows here.
  a,8 fis' e d cis4. cis16 cis | % 1
  a8 fis' e d cis4. cis16 cis | % 2
  b8 g' fis e cis4. cis16 cis | % 3
  a'8 a fis e fis4. a,16 a | % 4
  a8 fis' e d cis4. a16 a | % 5
  a8 fis' e d b4. b16 b | % 6
  b8 g' fis e a a a a | % 7
  b8 a fis e d4\staccato a'4\tenuto | % 8
  fis8\staccato fis\staccato fis4 fis8\staccato fis\staccato fis4 | % 9
  fis8 a d,8. e16 fis2 | % 10

}

flute = \relative c'' {
  \global
  % Music follows here.
  b4 b b2 | % 1
  b4 b b2 | % 2
  b4 d g a | % 3
  b1 | % 4
  c4 c c c | % 5
  c4 b b b8 b | % 6
  b4 a a b | % 7
  a2 d2 | % 8
  b4 b b2 | % 9
  b4 b b2 | % 10
  b4 d g a | % 11
  b2. r4 | % 12
}

cello = \relative c {
  \global
  % Music follows here.
  a4\staccato a\staccato a\staccato a8 a16 a | % 1
  a4 a a a8 b16 b | % 2
  b4 b b b8 cis16 cis | % 3
  cis4 cis cis cis8 a16 a | % 4
  a4\staccato a\staccato a\staccato a8 a16 a | % 5
  a4 a a a8 a16 a | % 6
  b4 b b b8 b16 b | % 7
  cis8 cis cis cis d4\staccato d\tenuto | % 8
  r8 d16 d r8 d16 d r8 d16 d r8 d16 d | % 9
  r8 a'16 a r8 a16 a r8 a16 a r8 a16 a | % 10
  r8 g16 g r8 g16 g r8 fis16 fis r8 fis16 fis | % 11
  r8 e16 e r8 e16 e r8 a,16 a r8 a16 a | % 12
  r8 d16 d r8 d16 d r8 d16 d r8 d16 d | % 13
  r8 a'16 a r8 a16 a r8 a16 a r8 a16 a | % 14
  r8 g16 g r8 g16 g r8 fis16 fis r8 fis16 fis | % 15
  r8 a16 a r8 a,8 d2 | % 16
  a8 fis' e d a4. a16 a | % 17
  a8 fis' e d b4. b16 b | % 18
}

contrabass = \relative c {
  \global
  % Music follows here.
  a2.. a16 a | % 1
  a2.. e16 e | % 2
  e2.. e16 e | % 3
  e2.. a16 a | % 4
  a2.. a16 a | % 5
  a2.. a16 a | % 6
  e2.. e16 e | % 7
  e8 e e e a4\staccato a\tenuto | % 8
  d4 a d a | % 9
  d4 a d a | % 10
  e'4 a, e' a, | % 11
  e'4 a, e' a, | % 12
  d4 a d a | % 13
  d4 a d a | % 14
  e'4 a, e' a, | % 15
  e'4 a, e' a, | % 16
  d4. d8 a4. a8 | % 17
  d4. d8 g,4. g8 | % 18

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
  shortInstrumentName = "Vl."
  midiInstrument = "violin"
} \violin

flutePart = \new Staff \with {
  instrumentName = "Flute"
  shortInstrumentName = "Fl."
  midiInstrument = "flute"
} \flute

celloPart = \new Staff \with {
  instrumentName = "Cello"
  shortInstrumentName = "Cl."
  midiInstrument = "cello"
} { \clef bass \cello }

contrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
  midiInstrument = "contrabass"
} { \clef bass \contrabass }

\score {
  <<
    \violinPart
 %   \flutePart
    \celloPart
    \contrabassPart
  >>
  \layout { }
  \midi { }
}
