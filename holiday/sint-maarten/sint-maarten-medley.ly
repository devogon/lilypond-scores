\version "2.19.57"

\language "nederlands"

date = \markup {\teeny {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Sint Maarten Medley"
  composer = "Diversen"
  arranger = "Marcel den Os"
%  poet = "poet"
  instrument = "Flute"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key f \major
  \time 12/8
  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

flute = \relative c'' {
   \global
   \repeat volta 2 {
      a'1.
      d,1.
   }
   d2. a'2. |
   g2. a2. |
   a2. f4 g8 a4 g8 |
   \break
   a4 ( g8 f4 e8 d4 d8 d4) a'8 |
   d,2. a'2. |
   g2. a2. |
   a2. f4 g8 a4 g8 |
   \break
   a4 ( g8 f4 e8 d4 d8 d4) a'8 | % 10
   d4\mf ( d8 d4 d8) a4 ( a8 a4 a8) | % 11
   c4 ( c8 a4 c8 d4 a8 a4 a8) | % 12
   \break
   a4 (a8 a4) a8 (f4 g8 a4) g8 | % 13
   a4 ( g8 f4 e8 d4 d8 d4) a'8 | % 14
   d4\mf (d8 d4 d8) a4 ( a8 a4 a8) | % 15
   \break
   c4 ( c8 a4 c8 d4 a8 a4 a8) | % 16
   a4 (a8 a4) a8 (f4 g8 a4) g8 | % 17
   a4 ( g8 f4 e8 d4 d8 d4) a'8 | % 18
   \break
   \repeat volta 2 {
     d4\mf\< (d8 d4 d8) a4 ( a8 a4 a8) | % 19
     g4 g8 g4 g8 a4 a8 a4 a8 | % 20
     a4\!\> a8 a4 a8 f4 g8 a4 g8 | % 21
     d4 d8 d4 e8 d4. d4 d8\! | % 22
   }
   g4.\mf r4. g4. r4. | % 23
   a4. r4. a4. r4. | % 24
   b4. r4. b4. r4. | % 25
   a4. r4. a4. r4. | % 26
   \break
   \repeat volta 2 {
     d4. d4. r2. | % 27
     c4. c4. r2. | % 28
     bes4. bes4. r2. | % 29
     a4. a4. r2. | % 30
   }
   \break
   \repeat volta 2 {
     g4. b!4. d4. b4. | % 31
     a4. a4. g4. r4. | % 32
     g4. b!4. d4. b4. | % 33
     a4. a4. g4. r4.  | % 34
     \break
     d'4. g,4. a4. g4. | % 35
     a8 a a a b! a g4. r8 r b | % 36
     d4. g,4. d'4. g,4. | % 37
     \break
     a8 a a a b! a g4. r8 r b | % 38
   }
   g4. r4. g4. r4. | % 39
   a4. r4. a4. r4. | % 40
   bes4. r4. bes4. r4. | % 41
   \break
   a4. r4. a4. r4.  | % 42
   \repeat volta 2 {
     d4. d4. r2. | % 43
     c4. c4. r2. | % 44
     bes4. bes4. r2. | % 45
     \break
   }
   \alternative {
     {a4. a4. r2.}
     {a2. a2.}
   }
     \break
    \repeat volta 2 {
      d,4\mf\< d8 d4 d8 a'4 a8 a4 a8  | % 48
      g4 g8 g4 g8 a4 a8 a4 a8 | % 49
      a4\!\> a8 a4 a8 f4g8 a4 g8 | % 50
      d4 d8 d4 e8 d4. d4 d8\! | % 51
    }

}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef treble \flute }
  \layout { }
}
