\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Goedenavond, heeft u even"
  composer = "composer"
  arranger = "Marcel den Os"
  poet = "Kasper en Ton Peters"
  instrument = "flute 2"
  % Anne Caesar van Wieren en Eva Waterbolk
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
%  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

fluteTwo = \relative c'' {
   \global
   \partial 4 r4 |
    c2 a | % 1
    b2 e | % 2
    g2 e | % 3
    d2 r | % 4
    f2 a | % 5
    g2 e | % 6
    \break
    g2 g | % 7
    g4\staccato r r2 | % 8
    e4\staccato\tenuto\mf r e\staccato\tenuto r | % 9
    d4\staccato\tenuto r d\staccato\tenuto r | % 10
    d4\staccato\tenuto r d\staccato\tenuto r | % 11
    e4\staccato\tenuto r e\staccato\tenuto r | % 12
    \break
    a4\staccato\tenuto r a\staccato\tenuto r | % 13
    e4\staccato\tenuto r e\staccato\tenuto r | % 14
    g4\staccato\tenuto r r2 | % 15
    r1 | % 16
    a1 | % 17
    g1 | % 18
    b,2. c4 | % 19
    \break
    d4 r r2 | % 20
    e4\staccato\tenuto\mf r e\staccato\tenuto r | % 21
    d4\staccato\tenuto r d4\staccato\tenuto r | % 22
    d4\staccato\tenuto r d4\staccato\tenuto r | % 23
    e4\staccato\tenuto r e4\staccato\tenuto r | % 24
    a4\staccato\tenuto r a4\staccato\tenuto r | % 25
    \break
    e4\staccato\tenuto r e4\staccato\tenuto r | % 26
    g4\staccato\tenuto r r2 | % 27
    r4 g\staccato\tenuto c,\staccato\tenuto r\mf | % 28
    \bar "||"
    c2 a | % 29
    b2 e | % 30
    g2 e | % 31
    d2 r | % 32
    \break
    f2 a | % 33
    g2 e | % 34
    g2 g | % 35
    g4\staccato\tenuto r r2 | % 36
    \bar "||"
    e4\staccato\tenuto r e4\staccato\tenuto r | % 37
    a4\staccato\tenuto r a4\staccato\tenuto r | % 38
    \break
    d,4\staccato\tenuto r d4\staccato\tenuto r | % 39
    e4\staccato\tenuto r e4\staccato\tenuto r  | % 40
    a4\staccato\tenuto r a4\staccato\tenuto r | % 41
    e4\staccato\tenuto r e4\staccato\tenuto r | % 42
    g4\staccato\tenuto r r2 | % 43
    r1 | % 44
    a1 | % 45
    \break
    g1 | % 46
    b,2. c4 | % 47
    d4 r r2 | % 48
    e4\staccato\tenuto\mf r e4\staccato\tenuto r  | % 49
    d4\staccato\tenuto r d4\staccato\tenuto r  | % 50
    d4\staccato\tenuto r d4\staccato\tenuto r  | % 51
    \break
    e4\staccato\tenuto r e4\staccato\tenuto r | % 52
    a4\staccato\tenuto r a4\staccato\tenuto r | % 53
    e4\staccato\tenuto r e4\staccato\tenuto r | % 54
    g4\staccato\tenuto r r2 | % 55
    r4 g\staccato\tenuto c,\staccato\tenuto r\mf | % 56
    \bar "||"
    c2 a | % 57
    \break
    b2 e | % 58
    g2 e | % 59
    d2 r | % 60
    f2 a | % 61
    g2 e | % 62
    g2 g | % 63
    g4\staccato g\staccato\tenuto c,\staccato\tenuto r | % 64
    \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef treble \fluteTwo }
  \layout { }
}
