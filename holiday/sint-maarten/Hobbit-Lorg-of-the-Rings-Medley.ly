\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Hobbit-Lorg of the Rings Medley"
  subtitle = "Hey, Ho to the Bottle I Go, Over Misty Mountains, Main theme"
  composer = "... Shore"
  arranger = "Marcel den Os"
  instrument = "flute"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g \major
  \time 4/4
  \tempo "Cheerful" 4=95
  \set Score.markFormatter = #format-mark-box-alphabet
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

flute = \relative c'' {
   \global
   \numericTimeSignature
  R1*4
  \time 2/4
  r1*2/4 |
  \time 4/4
  R1*4
  \bar "||"
  \key bes \major
  d1\ff\> (
  d1\fermata )
  \break
  \tempo "Maestoso" 4=56
  R1*10\!
  \time 3/4
  r1*3/4
  \tempo "poco accel" 4=68
  \time 4/4
  r1
  r1
  bes'4\f d8. e16 e2 |
  r1
  d,1
  \break
  f2 a (
  a4) a,8 e' d2 |
  c1
  d1
  \bar "||"
  r1
  bes4\f d8. e16 e2 |
  r1 | % 34
  \break
  r1 | % 35
  d1\mf | % 36
  g4 a8 bes bes2^\markup{\italic rit.}

  \tempo "Maestoso" 4=56
  bes,1
  f2 e!4 f |
  bes1
  a1
  \break
   R1*2
  \tempo "Maestoso" 4=48
  R1*3
  \bar "||"
  \key c \major
  r1^\markup{\italic accel.}
  \tempo "Accel" 4=68
  R1*3
  R1
  d'4\f e8. f16 f2 |
  \break
  R1
  b,2 c
  c1
  b1
  \time 3/4
  e,2^\markup{\italic rit.} d4 |
  \time 4/4
  a1\fermata
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef treble \flute }
  \layout { }
}
