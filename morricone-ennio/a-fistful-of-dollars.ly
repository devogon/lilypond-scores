\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}


\header {
  title = "A Fistful of Dollars"
  instrument = "Bass"
  composer = "Ennio Morricone"
  arranger = "Gianfranco Tammaro"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

bass = \relative c {
  \global
  % Music follows here.
   g8\2^\markup{Intro} (a\2) d4 a4\2 d,\3 d'
 %  \break
   a4\3^\markup{Whistled part} e'\1 f g f e d\2 c\2 a\3 c\2 d\2
  % \break
   a4\3 e'\1 f g f e d\2 c\2 a\3 c\2 a\3
   a4\3 e'\1 f g f e d\2 e\1 f\1 g\1 a\1
   c\1 a a f\2 cis'\1 a a f\2 d\2 g\1
   d\2 e\1 f\1 d\2 c\2 d\2
   \break
   a\3^\markup{"bass solo #1"} \tuplet 3/2 {d\2 c\2 d\2} \tuplet 3/2 {d\2 c\2 d\2} a\3 d\2 e\1 f\1 e\1 d\2 c\2 a\3 c\2 \tuplet 3/2 {d\2 c\2 d\2} c\2 a\3
   a\3 \tuplet 3/2 {d\2 c\2 d\2} a\3 \tuplet 3/2 {d\2 c\2 d\2} f\1 g\1 a\1 g\1 f\1 e\1 c\2 e\1 \tuplet 3/2 {f\1 e\1 f\1} f\1 e\1 d\2 c\2 d\2 g\1 f\1 d\2 c\2 d\2
   \break
   a\3^\markup{"bass solo #2"} d\2 e\1 f\1 g\1 f\1 e\1 d\2 c\2 a\3 c\2 a\3
   c\3 f\2 g\1 a\1 ais\1 a\1 g\1 f\1 g\1 f\1 e\1 d\2
   f\2 a\1 c\1 a\1 a\1 f\2 f\2 a\1 d\1 a\1 f\2 d\3
   a'\1 f\2 d\3 a'\1 f\2 g\2 e\2 d\3
   a\3 d\2 e\1 f\1 e\1 d\2 c\2  d\2 d'\prallprall\1
   \bar "|."
}

chordsPart = \new ChordNames \chordNames

bassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \bass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \bass
>>

\score {
  <<
    \chordsPart
    \bassPart
  >>
  \layout { }
}

\markup {fix this - pitch values aren't correct as i only just entered the notes}