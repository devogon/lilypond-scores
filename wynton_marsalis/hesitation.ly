\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Hesitation"
  composer = "Wynton Marsalis"
  arranger = "bassist: Ron Carter"
  poet = "label: CBS"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



%\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key bes \major
  \time 4/4
  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
  bes_\markup{\italic \bold {B\flat 7}} g' bes, b!_\markup{\italic \bold {G7}} | % 1
  c4_\markup{\italic \bold {Cm7}} d es e!_\markup{\italic \bold {F7}} | % 2
  f4_\markup{\italic \bold {Dm}} fis g_\markup{\italic \bold {G7}} es | % 3
  e!4_\markup{\italic \bold {Cm7}} c f_\markup{\italic \bold {F7}} f, | % 4
  \break
  bes4_\markup{\italic \bold {B\flat 7}} b! c d | % 5
  es4_\markup{\italic \bold {E\flat 7}} g as_\markup{\italic \bold {E\super{o}}} a! | % 6
  bes_\markup{\italic \bold {Dm7}} g es'_\markup{\italic \bold {G7}} e! | % 7
  d4_\markup{\italic \bold {Cm7}} e! es_\markup{\italic \bold {F7}} d | % 8
  \break
  bes4_\markup{\italic \bold {B\flat 7}} e! d e_\markup{\italic \bold {G7}} | % 9
  es4_\markup{\italic \bold {Cm7}} d c d_\markup{\italic \bold {F7}} | % 10
  d,_\markup{\italic \bold {Dm7}} cis' d_\markup{\italic \bold {G7}} cis | % 11
  c4_\markup{\italic \bold {Cm7}} bes a_\markup{\italic \bold {F7}} f | % 12
  \break
  bes,4_\markup{\italic \bold {B\flat 7}} d g bes | % 13
  es,4_\markup{\italic \bold {E\flat}} bes es e!_\markup{\italic \bold {E\super{o}}} | % 14
  f4_\markup{\italic \bold {B\flat 7}} a, d f | % 15
  bes, d g a | % 16
  \break
  bes4_\markup{\italic \bold {D7}} gis a g! | % 17
  fis4 a, d a' | % 18
  g4_\markup{\italic \bold {G7}} d a c | % 19
  b!4 d g d' | % 20
  \break
  c4_\markup{\italic \bold {C7}} d g, d' | % 21
  c4 g es' e! | % 22
  f4_\markup{\italic \bold {F7}} f, es es' | % 23
  d4 d, c c' | % 24
  \break
  bes4_\markup{\italic \bold {B\flat 7}} bes, d_\markup{\italic \bold {G7}} bes' | % 25
  es,4_\markup{\italic \bold {Cm7}} e! f_\markup{\italic \bold {F7}} bes | % 26
  a4_\markup{\italic \bold {Dm7}} f g_\markup{\italic \bold {G7}} es | % 27
  c4_\markup{\italic \bold {Cm7}} e,! f_\markup{\italic \bold {F7}} a | % 28
  \break
  bes4_\markup{\italic \bold {B\flat 7}} d f bes | % 29
  es,4_\markup{\italic \bold {E\flat}} bes' es, e!_\markup{\italic \bold {E\super{o}}} | % 30
  f4_\markup{\italic \bold {B\flat 7}} g es' e! | % 31
  f_\markup{\italic \bold {Cm7}} fis g a^\markup{etc.} | % 32
  \bar "||"
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}
