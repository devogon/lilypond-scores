\version "2.22.0"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "I‘ll Take You There"
  subtitle = "(Be Attitude : Respect Yourself) - 1972"
  instrument = "Bass"
  tagline = \date
  poet = "bass: David Hood"
  arranger = "The Staple Sisters"
}

\paper {
  indent = 0.0\cm
  page-count = 1
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
  \omit Voice.StringNumber

}

chordNames = \chordmode {
  \global
  % Chords follow here.
  c1
  s
  f
  s
  f
  c
  f
  s1*10
  c1
  f
  s1*12
  c1
  f
  s1*6
  c1
  f
  s1*6
  c1
  f
  c
  f
  c
  f
  c
  f
  c
  f
}

electricBass = \relative c, {
  \global
  % Music follows here.
  c4\staccato r r2 |
  r1
  f4. c8 r2 |
  r1
  f4. c8 r2 |
  \break
  \bar "||"
  \mark \markup{\box Verse}
  \repeat percent 6 {
    c4\staccato r8 c r g16 a\4 c8 a16\4 g
    f4\staccato f'\staccato g,8 a\4 c a\4
  }
  \bar "||"
  \break
  \mark \markup{\box Chorus}
  \repeat percent 7 {
    c4\staccato r8 c r g16 a\4 c8 a16\4 g
    f4\staccato f'\staccato g,8 a\4 c a\4
  }
  \repeat percent 4 {
    c4\staccato r8 c r g16 a\4 c8 a16\4 g
    f4\staccato f'\staccato g,8 a\4 c a\4
  }
  \bar "||"
  \break
   \mark \markup{\box Solos}
   \repeat percent 4 {
    c4\staccato r8 c r g16 a\4 c8 a16\4 g
    f4\staccato f'\staccato g,8 a\4 c a\4
  }
  \bar "||"
  \break
  \mark \markup{\box Bridge}
  e''2.\2 a8 g f8. c16\2 f,4\3 (f\3) a8\2 d |
  c g\2 c,4 (c) a'8\2 c |
  c1 |
  \break
  e2. a8 g f8. c16\2 f,4\3 (f\3) a8\2 d |
  c g\2 c,4 (c) c |
  c2 r8 g a\4 g |
  \bar "||"
  \break
  \mark \markup{\box Chorus}
  \repeat volta 2 {
    c4\staccato r8 c r g16 a\4 c8 a16\4 g
    f4\staccato f'\staccato g,8 a\4 c a\4_\markup{\right-align "repeat & fade"}
  }



}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
