\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Blues for Basie"
  subtitle = "from album “Oscar Peterson Plays Count Basie”"
  subsubtitle = "(Clef 1955)"
  instrument = "Bass"
  composer = "Oscar Peterson"
  arranger = "bass: Ray Brown"
  poet = "Transcription Guillaume Ottaviani"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=120
    \override Glissando.style = #'trill
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

contrabass = \relative c {
  \global
  % Music follows here.
f2 fis2
g4. d8 c4. g'8
f2 fis2
g4. d8 c4. g'8
% \break
f2 fis2
g4. d8 c4 g'
f a, bes b!
c \xNotesOn c c \xNotesOff g'8 d
\bar "||"
c4 bes  a c
bes f' bes b!
c a bes g
a8 \xNotesOn d, \xNotesOff c4 g' f
% \break
bes,4 c d f8 g | % 13
bes4 c d e
f e d des
c4 bes ~ \tuplet 3/2 {bes8 a e} a,4
% \break
g4 a bes b! % 17
c f e g
f a, bes b!
c e g c8 e
\bar "||"
% \break
f4 f es es
d d des des
c c bes bes
a f es c
% \break
bes f' bes, f' |
bes, f' bes, b!
c a bes g
a f'' fis d,
% \break
g'4 e bes \tuplet 3/2 {a8 g d}
c4 bes a g
f8 f' a,4 bes b!
c4 d e g
\bar "||"
% \break
f4 g a c | % 33
bes d c bes
a f g e
f es c f8 a
% \break
bes4 d c es | % 37
d f e d
c a bes as
a!\parenthesize g8 f fis4 d8 a
% \break
g4 f e d' | % 41
c f e g8 e
f a, ~ a4 bes b!
c bes r8 f' as bes
\bar "||"
				% page break https://www.youtube.com/watch?v=thOi9Wy0xjI
b!8 b bes4 a f | % 45
g2. f8 as
\parenthesize a!8 c d c f e \tuplet 3/2 {es8 f, \xNotesOn a \xNotesOff}
d8 c r4 r f,16 g f g |
% \break
bes16 g d c ~ c8 bes c bes c bes | % 49
b!8 d16 e f g as bes (b4) r8 c, |
r8 c' ~ c a g f a c |
\grace d16 e4. d8 r4 r8 c |
% \break
f8 f es es c f, b! b\glissando | %53
bes as f es c b! bes as |
f8 c' d e g e \tuplet 3/2 {f a c} |
e8 d r4 r \tuplet 3/2 {c8 (d) (c\glissando)} |
\bar "||"
% \break
				% squirly up
es8 (d) (d) des c bes a f | % 57
bes8 f g as ~ as g4 f8\glissando |
a4 c8 bes a g f d |
%%%%% recheck from here

f8 d \tuplet 3/2 {es8 g bes} d f g a ( |
% \break
bes16)  \xNotesOn g c, a  \xNotesOff c8 bes as f a, bes | % 61
b!8 d f g bes (a) \tuplet 3/2 {bes a f} |
c8 c c bes r2 |
a8 g \tuplet 3/2 {fis a c} es d r g |
% \break
e16 (f) g e (f) g e (f) g e (f) c' r8 g8 | % 65
e16 (f) g e (f) g e (f) g e8. e'16 (f) f f |
b,!16 (c) c c a (b) b b f8 f (es) es (|
d) d (des) des (c) c bes4 |
\bar "||"
% \break
f4 g a c | % 69
bes f' bes g |
c4 d es e! |
f8 f fis4 g a |
% \break
bes8 d,, a4 bes c | % 73
d4 f bes d |
\tuplet 3/2 {c8 g d} c4 g bes' |
\tuplet 3/2 {r8 a f} a,4 d c |
% \break
bes4 a g d' | % 77
g8 \xNotesOn d \xNotesOff c4 e g |
f4 a bes g |
c cis d e |
\bar "||"
% \break
f4 f, es es' | % 81
d d, cis cis' |
c! c, bes bes' |
a g f es |
% \break
d4 g c, f | % 85
bes, b! c d |
c4 c bes bes'8 g |
a4 a, d as' |
% \break
g8 g f4 e g | % 89
c4 cis d e |
f g a bes |
c bes a bes |
% \break
\bar "||"
				% line starting at 91 missing - also at musescore!
c b!4 c a | %93
b4 as g e |
f4 d es des | % 95
d! bes c a |
% \break
bes4 d, es e! | % 97
f a, bes b! |
c c bes bes |
a a as as |
% \break
g4 a bes a' | % 101
c8 c (cis) cis (d) d (e) e |
f8 a,, ~ a4 bes b! |
c8 d e f r4 f,4 |
\bar "|."
% is there any more? not on this
}

chordsPart = \new ChordNames \chordNames

contrabassPart = \new Staff { \clef bass \contrabass }

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}
