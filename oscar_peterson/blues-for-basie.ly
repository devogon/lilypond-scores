\version "2.19.57"
\language "nederlands"

\header {
  title = "Blues for Basie"
  instrument = "Contrabas"
  composer = "Oscar Peterson"
  arranger = "played by Ray Brown"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key f \major
  \time 4/4
  \tempo 4=108
   c4 bes a c | % 1
   bes4 f' bes b! | % 2
   c4 a bes g | % 3
   a4 (\grace d,8) c4 g' f | % 4
   \break
   bes,4 c d f (\grace g8) | % 5
   bes4 c d e | % 6
   f4 e d des | % 7
   c4 bes \tuplet 3/2 {b!8 a f} a,4 | % 8
   \break
   g4 a bes b! | % 9
   c4 f e g (\grace e8) | % 10
   f4 a, bes b! | % 11
   c4 e g c ( | % 12
   \break
   \grace e8) f4 f es es | % 13
   d4 d des des | % 14
   c4 c bes bes | % 15
   a4 f es c | % 16
   \break
   bes4 f' bes, f' | % 17
   bes,4 f' bes, b! | % 18
   c4 a bes g | % 19
   a4 f'' fis d, | % 20
   \break
   g4 d' bes \tuplet 3/2 {a8 g\staccato d} | % 21
   c4 bes a g | % 22
   f8 f' (<f a,>4) bes,4 b! | % 23
   c4 d e g | % 24
   \break
   f4 g a c | % 25
   bes4 d c bes | % 26
   a4 f g e | %
   f4 es c f ( | %
   \break
    \grace a8)  bes4 d c es | %
   d4 f e d | %
   c4 a bes gis | %
   a4 g (\grace d8) fis4 d ( | %
   \break
   \grace g,8) g4 f e d' | %
   c4 f e g8 e | %
   f8 a,4. bes4 b! | %
   c4 r r8 f8^\markup{solo} as bes | %
   \break
   b!8 b bes4 \grace g8 as4 f | %
   g2. f8 as | %
   a8 c d c f e \tuplet 3/2 {es8 g, bes} | %
   d8 c r4 r f,16 g f a | %
   \break
   \tuplet 3/2 {bes8 f d} c8 bes c bes c bes | %
   b!16 cis d e f g as bes b!4 r8 cis, | %
   a8 c' r a g f a c ( | %
   e4.) d8 r4 r8 c | %
   \break
   f8 f es es c f, b! b | %
   bes8 as f es c bes as a! | %
   r8 c d e g e \tuplet 3/2 {f8 a c} | %
   e8 d r4 r \tuplet 3/2 {c8 d (c)} | %
   \break
   es4 d8 des c bes a f | %
   bes8 f g as (as) g4 f8 | %
   \grace gis8 a4 c8 bes a gis f d | %
   f8 d \tuplet 3/2 {es8 g bes} d8 f g bes ( | %
   \break
   bes8)   \override NoteHead.style = #'cross \tuplet 3/2 {bes,16 bes bes} \revert NoteHead.style c8 bes as f a, bes | %
   b8 d f g \tuplet 3/2 {bes8 a bes} \tuplet 3/2 {a8 f c (} | %
   c8) c c bes r2 | %
   a8 g \tuplet 3/2 {fis8 a c} es d r g | %
   e16 f g\marcato e f g\marcato e f g\marcato e f c'\marcato r8 g | %
   e16 f g e f g e f g c, r8 e'16 f f f | % 58
   b,!16 c c c gis a a a f4 (\grace f8) es4 ( | %
   \break
   \grace es8) d4 (\grace d8) des4 (\grace des8) c4 (\grace c8) e,4 | %
   f4 g a c | %
   bes4 f' bes g | %
   c4 d dis e | %
   \break
   f8 f fis4 g^\markup{g} a | %
   bes 4 (\grace d,,8) a4 bes c | %
   d4 f bes d | %
   \tuplet 3/2 {c8 g d} c4 bes bes' | %
   \break
   \tuplet 3/2 {r8 a e} a,4 d c | %
   bes4 a g d'  | %
   g4 (\grace c,8) c4 e g | %
   f4 a bes g | %
   \break
   c4 cis d e | %
   f4 f, es e' | %
   d4 d, des d' | %
   c4 c, bes bes' | %
   \break
   a4 g f es | %
   d4 g c, f | %
   a,4 bes b! d | %
   c4 c bes bes | %
   \break
   a4 a d as | %
   g4 (\grace g'8) f4 d g | %
   c4 cis d e | %
   f4 g^\markup{g} a bes^\markup{b\teeny \flat} | %
   \break
   c4^\markup{c} bes a^\markup{a} bes | %
   c4^\markup{c} b! c a | %
   bes4^\markup{b\teeny \flat} as g e | %
   f4 des es c | %
   \break
   c4 bes c a | %
   bes4 d, es e! | %
   f4 a, bes b! | %
  c4 c bes bes | %
    \break
   a4 a as as | %
   g4 a bes g' | %
   c8 c cis cis d d e e | %
   f8 a,,4. bes4 b! | %
   c8 d e f (f) r f,4\fermata | %
   \bar "|."
}

contrabass = \relative c {
  \global
  % Music follows here.

}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}


\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
  \hspace #1
}

\markup {
  \teeny
  \date
}