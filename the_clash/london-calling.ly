\version "2.19.50"
\language "nederlands"

\header {
  title = "London Calling"
  instrument = "Bass"
  composer = "Paul Simonon"
  meter = ""
  copyright = "The Clash"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=144
}

electricBass = \relative c, {
  \global
  % Music follows here.
    r1
    r1
    r1
    r2 r4 b |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b, |
    \break
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b |
    e,2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b, |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    \break
    c2 (c4) b |
    e,4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    \break
    g4 g r r8 b |
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    \break
    g4 g r r8 b |
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    \break
    g4 g r r8 b |
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    \break
    g4 g r r8 b |
    e4 e e fis |
    g4 g8 g g4 d |
    e4 e e fis |
    g4 g8 g g4 d |
    \break
    e4 e e fis |
    g4 g8 g g4 d |
    e4 e e e |
    d4 cis c! b |
    d4 cis c! b |
    \break
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r r8 b |
    \break
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r r8 b |
    \break
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r r8 b |
    \break
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r r8 b |
    \break
    e4 e e fis |
    g4 g8 g g4 d |
    e4 e e fis |
    g4 g8 g g4 d |
    e4 e e fis |
    \break
    g4 g8 g g4 d |
    e4 e e e |
    d4 cis c! b |
    d4 cis c! b |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    \break
    c2 (c4) b, |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c,2 (c4) b4 |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b, |
    \break
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c,2 (c4) b |
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 {a8 ais b} |
    \break
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r r8 b |
    e4 e r8 b \tuplet 3/2 {a8 ais b} |
    f'4 f r8 b, \tuplet 3/2 { a8 ais b} |
    \break
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    c4 c r r8 b |
    e4 e e fis |
    g4 g8 g g4 d |
    e4 e e fis |
    \break
    g4 g8 g g4 d |
    e4 e e fis |
    g4 g8 g g4 d |
    e4 e e e |
    d4 cis c! b |
    \break
    d4 cis c! b |
    e4 e r2 |
    r1
    r1
    r2 r4 b |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b,|
    \break
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c,2 (c4) b |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b, |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    \break
    c,2 (c4) b |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b, |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c,2 (c4) b |
    \break
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c2 (c4) b, |
    e2 (e4) \tuplet 3/2 {g8 e g} |
    c,2 (c4) b |
    e1
    (e1)
    (e1)
    (e1)
    \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "electric bass (finger)"
  } { \clef "bass_8" \electricBass }
  \layout { }
  \midi { }
}
