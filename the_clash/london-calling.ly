\version "2.19.50"
\language "nederlands"

\header {
  title = "London Calling"
  instrument = "Bass"
  composer = "Paul Simonon"
  meter = ""
  copyright = "The Clash"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=144
}

electricBass = \relative c, {
  \global
  \compressMMRests
  R1*3
  r2 r4 b (|
  \repeat volta 2 {
    e2.) \tuplet 3/2 {g8 e g} |
    c2. b,4 (
    e2.) \tuplet 3/2 {g8 e g} 
  }
  \alternative {
    {c,2. b4 (}
    {c2.) r8 b (}
  }
  \bar "||"
  \mark \markup{\box Verse}
  \break
  \repeat volta 4 {
    e4) e4\staccato r8 b \tuplet 3/2 {a8 ais b} |
    f'!4 f\staccato r8 b, \tuplet 3/2 {a8 ais b} |
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r4. b8^\markup{\bold x4} (
  }
  \break
%  \bar "||"
  \mark \markup {\box Chorus}
  \repeat percent 3 {
    e8) e e e (e4) fis |
    g8 g g g (g4) d |
  }
  e8 e e e (e4) e |
  \break
  d8. d16 cis8. cis16 c!8. c16 b8. b16 |
  d8. d16 cis8. cis16 c!8. c16 b8. b16 |
  \mark \markup{\box Verse}
  \repeat volta 4 {
    e4\segno e\staccato r8 b \tuplet 3/2 {a8 ais b}
    f'!4 f\staccato r8 b, \tuplet 3/2 {a8 ais b}
    \break
    g4 g r8 b \tuplet 3/2 {a8 ais b} |
    g4 g r4. b8^\markup{\bold x4} (
  }
  \mark \markup{\box Chorus}
    \repeat percent 3 {
    e8) e e e (e4) fis |
    g8 g g g (g4) d |
  }
  e8 e e e (e4) e |
    d8. d16 cis8. cis16 c!8. c16 b8. b16 |
  d8. d16 cis8. cis16 c!8. c16 b8. b16^\markup{\bold \raise #1 \italic "D.S."} |
  \bar "||" \break
  e4 e\staccato r2 |
  R1*2
  r2 r4 b (
  \repeat volta 4 {
    e2.) \tuplet 3/2 {g8 e g} |
  c2. b,4 ( |
  e2.) \tuplet 3/2 {g8 e 8}
  c2. b4^\markup{\bold x4}
  }
  \break
  e1
  (e)
  (e)
  (e)
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "electric bass (finger)"
  } { \clef "bass_8" \electricBass }
  \layout { }
}
