\version "2.19.36"
\language "nederlands"

\header {
  title = "Rolling in the Deep"
  instrument = "Bass"
  composer = "Adele"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
  \tempo 4=104
}

electricBass = \relative c, {
  \global
  % Music follows here.
  c8^\markup{\tiny "0:09"} c c c c c c c |
  g' g g g g g g g |

  bes,^\markup{\tiny "0:14"} bes bes bes bes bes bes bes |
  g' g g g bes, bes bes bes |

  c^\markup{\tiny "0:18"} c c c c c c c |
  g' g g g g g g g

  bes,^\markup{\tiny "0:23"} bes bes bes  bes bes bes bes
  g' g g g bes, bes bes bes |

  c^\markup{\tiny "0:28"} c c c c c c c |
  g' g g g g g g g |

  bes,^\markup{\tiny "0:32"} bes bes bes bes bes bes bes |
  g' g g g bes, bes bes bes |

  c^\markup{\tiny "0:36"} c c c c c c c |
  g' g g g g g g g |

  bes,^\markup{\tiny "0:41"} bes bes bes bes bes bes bes |
  g' g g g bes, bes bes bes |

  aes'^\markup{\tiny "0:46"} aes aes aes aes aes  aes aes |
  bes bes bes bes  bes bes bes bes |

  g^\markup{\tiny "0:51"} g g g  g g g g |
  aes aes aes aes  aes aes aes aes |

  aes^\markup{\tiny "0:55"} aes aes aes  aes aes aes aes |
  bes bes bes bes  bes bes bes bes |

  g^\markup{\tiny "1:00"} g g g  g g g g |
  g g g g  g g g g |

  c,^\markup{\tiny "1:04"} c c c  c c c c |
  bes bes bes bes  bes bes bes bes |

  aes^\markup{\tiny "1:09"} aes aes aes  aes aes aes aes |
  aes aes aes aes  bes bes bes bes |

  c^\markup{\tiny "1:13"} c c c  c c c c |
  bes bes bes bes  bes bes bes bes |

  aes^\markup{\tiny "1:18"} aes aes aes  aes aes aes aes |
  aes aes aes aes  bes bes bes bes |

  c^\markup{\tiny "1:22"} c c c  c c c c |
  g'g g g g g g g |

  bes,^\markup{\tiny "1:28"} bes bes bes bes bes bes bes |
  g' g g g bes, bes bes bes |

  c^\markup{\tiny "1:32"} c c c  c c c c |
  g' g g g  g g g g |

  bes,^\markup{\tiny "1:36"} bes bes bes bes bes bes bes |
  g' g g g  bes, bes bes bes |

  aes'^\markup{\tiny "1:41"} aes aes aes aes aes aes aes |
  bes bes bes bes bes bes bes bes |

  g^\markup{\tiny "1:45"} g g g g g g g |
  aes aes aes aes aes aes aes aes |

  aes^\markup{\tiny "1:50"} aes aes aes aes aes aes aes |
  bes bes bes bes bes bes bes bes |

  g^\markup{\tiny "1:55"} g g g g g g g |
  g g g g g g g g |

  c,^\markup{\tiny "1:59"} c c c c c c c |
  bes bes bes bes bes bes bes bes |

  aes^\markup{\tiny "2:04"} aes aes aes aes aes aes aes |
  aes aes aes aes  bes bes bes bes |

  c^\markup{\tiny "2:08"} c c c c c c c |
  bes bes bes bes bes bes bes bes |

  aes^\markup{\tiny "2:13"} aes aes aes aes aes aes aes |
  aes aes aes aes bes bes bes bes |

  aes^\markup{\tiny "2:17"} aes aes aes aes aes aes aes |
  bes bes bes bes bes bes bes bes |

  c^\markup{\tiny "2:22"} c c c c c c c |
  bes bes bes bes bes bes bes bes |

  aes^\markup{\tiny "2:26"} aes aes aes aes aes aes aes |
  aes aes aes aes aes aes aes aes |

  bes^\markup{\tiny "2:31"} bes bes bes bes bes bes bes |
  bes bes bes bes bes bes bes bes |

  r^\markup{\tiny "2:36 count these!"} r r r r r r r |

  r^\markup{\tiny "2:45"} c r c r c r c |
  r c r c r bes r bes |

  r^\markup{\tiny "2:49"} c r c r c r c |
  r c r c r bes r bes |

  c1^\markup{\tiny "2:53"}-> |
  bes1-> |

  aes1^\markup{\tiny "2:58"}-> |
  aes2-> bes2 -> |

  c1^\markup{\tiny "3:03"}->
  bes1-> |

  aes1^\markup{\tiny "3:08"}-> |
  r2 bes2-> |

  c8^\markup{\tiny "3:12"} c c c c c c c |
  bes bes bes bes bes bes bes bes |

  aes aes aes aes aes aes aes aes
  aes aes aes aes  bes bes bes bes
  c c c c  c c c c
  bes bes bes bes  bes bes bes bes
  aes aes aes aes  aes aes aes aes
  aes aes aes aes  bes bes bes bes
  c c c c  c c d c
  bes bes bes bes  bes bes bes bes
  aes aes aes aes  aes aes aes aes
  aes aes aes aes  bes bes bes bes
  c c c c  c c d c
  bes bes bes bes   bes bes bes bes
  aes aes aes aes  aes aes aes aes
  aes aes aes aes  bes bes bes bes
  c1->
}
%https://www.youtube.com/watch?v=ACFKdQFB-j4

\score {
  \new Staff \with {
    midiInstrument = "electric bass (finger)"
  } { \clef "bass_8" \electricBass }
  \layout { }
  \midi { }
}
