\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Comfortably Numb"
  subtitle = "(the wall)"
  instrument = "Bass"
  composer = "Roger Waters & David Gilmour"
%  meter = "63"
  copyright = "yes"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=63
}

electricBass = \relative c, {
  \global
  % Music follows here.
  b4.. b16 b2
  b2 b | % 2
  \bar "||"
  b4.. b16 b2 | % 3
  a2 a |
  \break
  g4. fis8 e2 | % 5
  b'2 b |
  b4.. b16 b2 |
  a2 a |
  \break
  g4. fis8 e2 | % 9
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

chordsPart = \new ChordNames \chordNames

\score {
  <<
    \electricBassPart
    \chordsPart
  >>
  \layout { }
}
