\version "2.21.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Another Brick in the Wall Pt 2"
  subtitle = "Pink Floyd - The Wall - 1979"
  instrument = "Bass"
  composer = "Roger Waters"
  tagline = \date
  meter = "Drop D tuning DADG"
}

\paper {
  indent = 0.0\cm
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=105
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  d1:m
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  g1
  s1
  s1
  s1
  d1:m
  s1
  f1
  c1
  d1:m
  s1
  f1
  c1
  d1:m
  s1
  d1:m
  s1
  s1
  s1
  s1
  s1
  s1
  s1
  s1

}

electricBass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 2 {
      r1
      r1
      d,1 (
      r1)
      r1
      r1
      d4. c'8 d4\3 r |
      %\break
      d4.\3 d8\3 a4 c |
      d,4. c'8 d4\3 r |
      d4.\3 d8\3 a4 c |
      d,4.\3 c'8 d4\3 r |
      d4.\3 d8\3 a4 c |
      \bar "||"
      g1
      g1
      g2 g
      g4 g g g\glissando |
      d'4.\3 c8 d4\3 r |
      d4.\3 d8\3 a4 g |
      f1
      c'
      d,4.\4 c'8 d4\3 r |
      d4.\3 d8\3 a4 c |
      f1
      c2. c4
  }
  \alternative {
    {d4\3 r r2 r1}
    {d,4. c'8 d4\3 r }
    {d4.\4 d8\3 a4 c }
  }
  \repeat volta 2 {
    d4.\3^\markup{\italic "Guitar Solo"} c8 d4\3 r |
    d4.\3 d8\3 a4 c |
    d,4. c'8 d4\3 r |
    d4.\3 c8 d4\3 r |
  }
  d4.\3 c8 d4\3 r |
  d4.\3 c8 d4\3 r |
  d4.\3 c8 d4\3 r |
  r1_\markup{\right-align "How can you have any pudding..."}
  \bar "|."
}


chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-drop-d-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
