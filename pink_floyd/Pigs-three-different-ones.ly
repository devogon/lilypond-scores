\version "2.19.36"
\language "nederlands"

\header {
  title = "Pigs"
  subtitle = "Three Different Ones"
  instrument = "Bass"
  composer = "Roger Waters"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=64
}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*3 |
  r2 r16 b''32 a b8 ~ b8. b32 a | % 4
  b2\breathe  a8 g8 fis16 g32 fis e16 d | % 5
  e4 e8. e16 r4 r8 b | % 6
  b'4. r8 d32 b8. ~ b4 ~ b32 | % 7
  c4 ~ c8. r16 b16 r32 a r8 g16 a32 g fis16 g32 fis | % 8
  e2 ~ e8 r16 fis8 d16 r g | % 9
  c,2 ~ c8 ~ c r4 | % 10

}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

melody = \relative c'' {
  \global
  % Music follows here.

}

verse = \lyricmode {
  % Lyrics follow here.

}

electricBassPart = \new Staff \with {
  midiInstrument = "electric bass (finger)"
} { \clef "bass_8" \electricBass }

leadSheetPart = <<
  \new ChordNames \chordNames
  \new Staff { \melody }
  \addlyrics { \verse }
>>

\score {
  <<
    \electricBassPart
    \leadSheetPart
  >>
  \layout { }
  \midi { }
}
