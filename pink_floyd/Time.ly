\version "2.19.36"
\language "nederlands"

\header {
  title = "Time"
  composer = "Roger Waters"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=64
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

classicalGuitar = \relative c' {
  \global
  % Music follows here.

}

electricBass = \relative c, {
  \global
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
  % Music follows here.
  r1
  e,1 ~
  e1
  fis1 ~
  fis1
  e1 ~
  e1
  fis1 ~
  fis1
  e1 ~
  e1
  e2. e16 f8. | % 12
  fis1 ~
  fis1
  e2. e16 f8. | % 15
  fis2. b16 cis e8 | % 16
  \break
  fis,1
  fis1 ~
  fis1
  a1
  e'2 fis,4 fis | % 21
  e2 e | % 22
  e'2. e,8 f | % 23
  fis1
  fis4^\markup{\small "end intro"} ~ fis2. | % 25
  fis'4 fis8 r16 e fis8 r8 fis16 g r gis | % 26


%  <e,~\6>1
%   <e\6>1
%   <fis~\6>1
%   <fis\6>1
%   <e~\6>1
%   <e\6>1
%   <fis~\6>1

}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

melody = \relative c'' {
  \global
  % Music follows here.

}

verse = \lyricmode {
  % Lyrics follow here.

}

classicalGuitarPart = \new Staff \with {
  %midiInstrument = "acoustic guitar (nylon)"
  %instrumentName = "Classical guitar"
  shortInstrumentName = "Classical guitar"
} { \clef "treble_8" \classicalGuitar }

electricBassPart = \new Staff \with {
  midiInstrument = "electric bass (finger)"
  %instrumentName = "Electric bass"
  %shortInstrumentName = "Electric bass"
} { \clef "bass_8" \electricBass }

leadSheetPart = <<
  \new ChordNames \chordNames
  \new Staff { \melody }
  \addlyrics { \verse }
>>

\score {
  <<
  %  \classicalGuitarPart
    \electricBassPart
  %  \leadSheetPart
  >>
  \layout { }
  \midi { }
}
