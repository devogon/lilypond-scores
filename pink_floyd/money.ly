\version "2.22.1"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

% repeatBracket snippet
% will add .----Nx----. above a bar, where N is the number of repetitions
% based on the wonderful spanner by David Nalesnik (see: http://lists.gnu.org/archive/html/lilypond-user/2014-10/msg00446.html )
#(define (test-stencil grob text)
   (let* ((orig (ly:grob-original grob))
          (siblings (ly:spanner-broken-into orig)) ; have we been split?
          (refp (ly:grob-system grob))
          (left-bound (ly:spanner-bound grob LEFT))
          (right-bound (ly:spanner-bound grob RIGHT))
          (elts-L (ly:grob-array->list (ly:grob-object left-bound 'elements)))
          (elts-R (ly:grob-array->list (ly:grob-object right-bound 'elements)))
          (break-alignment-L
           (filter
            (lambda (elt) (grob::has-interface elt 'break-alignment-interface))
            elts-L))
          (break-alignment-R
           (filter
            (lambda (elt) (grob::has-interface elt 'break-alignment-interface))
            elts-R))
          (break-alignment-L-ext (ly:grob-extent (car break-alignment-L) refp X))
          (break-alignment-R-ext (ly:grob-extent (car break-alignment-R) refp X))
          (num
           (markup text))
          (num
           (if (or (null? siblings)
                   (eq? grob (car siblings)))
               num
               (make-parenthesize-markup num)))
          (num (grob-interpret-markup grob num))
          (num-stil-ext-X (ly:stencil-extent num X))
          (num-stil-ext-Y (ly:stencil-extent num Y))
          (num (ly:stencil-aligned-to num X CENTER))
          (num
           (ly:stencil-translate-axis
            num
            (+ (interval-length break-alignment-L-ext)
              (* 0.5
                (- (car break-alignment-R-ext)
                  (cdr break-alignment-L-ext))))
            X))
          (bracket-L
           (markup
            #:path
            0.1 ; line-thickness
            `((moveto 0.5 ,(* 0.5 (interval-length num-stil-ext-Y)))
              (lineto ,(* 0.5
                         (- (car break-alignment-R-ext)
                           (cdr break-alignment-L-ext)
                           (interval-length num-stil-ext-X)))
                ,(* 0.5 (interval-length num-stil-ext-Y)))
              (closepath)
              (rlineto 0.0
                ,(if (or (null? siblings) (eq? grob (car siblings)))
                     -1.0 0.0)))))
          (bracket-R
           (markup
            #:path
            0.1
            `((moveto ,(* 0.5
                         (- (car break-alignment-R-ext)
                           (cdr break-alignment-L-ext)
                           (interval-length num-stil-ext-X)))
                ,(* 0.5 (interval-length num-stil-ext-Y)))
              (lineto 0.5
                ,(* 0.5 (interval-length num-stil-ext-Y)))
              (closepath)
              (rlineto 0.0
                ,(if (or (null? siblings) (eq? grob (last siblings)))
                     -1.0 0.0)))))
          (bracket-L (grob-interpret-markup grob bracket-L))
          (bracket-R (grob-interpret-markup grob bracket-R))
          (num (ly:stencil-combine-at-edge num X LEFT bracket-L 0.4))
          (num (ly:stencil-combine-at-edge num X RIGHT bracket-R 0.4)))
     num))

#(define-public (Measure_attached_spanner_engraver context)
   (let ((span '())
         (finished '())
         (event-start '())
         (event-stop '()))
     (make-engraver
      (listeners ((measure-counter-event engraver event)
                  (if (= START (ly:event-property event 'span-direction))
                      (set! event-start event)
                      (set! event-stop event))))
      ((process-music trans)
       (if (ly:stream-event? event-stop)
           (if (null? span)
               (ly:warning "You're trying to end a measure-attached spanner but you haven't started one.")
               (begin (set! finished span)
                 (ly:engraver-announce-end-grob trans finished event-start)
                 (set! span '())
                 (set! event-stop '()))))
       (if (ly:stream-event? event-start)
           (begin (set! span (ly:engraver-make-grob trans 'MeasureCounter event-start))
             (set! event-start '()))))
      ((stop-translation-timestep trans)
       (if (and (ly:spanner? span)
                (null? (ly:spanner-bound span LEFT))
                (moment<=? (ly:context-property context 'measurePosition) ZERO-MOMENT))
           (ly:spanner-set-bound! span LEFT
             (ly:context-property context 'currentCommandColumn)))
       (if (and (ly:spanner? finished)
                (moment<=? (ly:context-property context 'measurePosition) ZERO-MOMENT))
           (begin
            (if (null? (ly:spanner-bound finished RIGHT))
                (ly:spanner-set-bound! finished RIGHT
                  (ly:context-property context 'currentCommandColumn)))
            (set! finished '())
            (set! event-start '())
            (set! event-stop '()))))
      ((finalize trans)
       (if (ly:spanner? finished)
           (begin
            (if (null? (ly:spanner-bound finished RIGHT))
                (set! (ly:spanner-bound finished RIGHT)
                      (ly:context-property context 'currentCommandColumn)))
            (set! finished '())))
       (if (ly:spanner? span)
           (begin
            (ly:warning "I think there's a dangling measure-attached spanner :-(")
            (ly:grob-suicide! span)
            (set! span '())))))))

\layout {
  \context {
    \Staff
    \consists #Measure_attached_spanner_engraver
    \override MeasureCounter.font-encoding = #'latin1
    \override MeasureCounter.font-size = 0
    \override MeasureCounter.outside-staff-padding = 2
    \override MeasureCounter.outside-staff-horizontal-padding = #0
  }
}

repeatBracket = #(define-music-function
     (parser location N note)
     (number? ly:music?)
      #{
        \override Staff.MeasureCounter.stencil =
        #(lambda (grob) (test-stencil grob #{ #(string-append(number->string N) "×") #} ))
        \startMeasureCount
        \repeat volta #N { $note }
        \stopMeasureCount
      #}
     )

\paper {
  indent = 0.0\cm
%  page-count = 1
  }
#(set-global-staff-size 17)

\header {
  title = "Money"
  subtitle = "Pink Floyd - album - year"
  %poet = "Bass"
  %composer = "Roger Waters"
  tagline  = \date
}

global = {
  \key d \major
  \numericTimeSignature
  \time 7/4
  \tempo "Moderate"
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  b1*7/4:m
  s1*7/4
  b1*7/4:m7
  s1*6/4
  s1*2/4
  s1*2/4
  s1*4/4
  b1*7/4:m7
  s1*7/4
  s1*6/4
  s1*2/4
  s1*2/4
  s1*4/4
  b1*7/4:m
  fis1*4/4:m
  s1*4/4
  e1*6/4:m7
  b1*7/4:m7
  s1*7/4
  b1*7/4:m7
  s1*7/4 s1*7/4 s1*7/4 s1*7/4 s1*7/4 s1*7/4 s1*7/4
  e1*7/4:m7
  s1*7/4 s1*7/4 s1*7/4
  b1*7/4:m7
  s1*7/4 s1*7/4 s1*7/4
  fis1*4/4:m7
  s1*4/4
  e1*6/4:m
  b1*4/4:m s1
  b1:m
  s1 s1 s1 s1 s1 s1 s1
  e:m
  s1 s1 s1
  b1:m
  s1 s1 s1
  fis:m
  s1
  e:m
  s1
  b1:m
  s1 s1 s1
  b1*7/4:m7
  s1*7/4
  b1*4/4:m7
  s1 s1 s1
}

runOne = { b4 b'8. fis16 b,4\staccato fis a b }

electricBass = \relative c, {
  \global
  % Music follows here.
  \repeatBracket 4 {
    \repeat percent 2 {\runOne d }
  }
  \mark \markup{\box " verse "}
  \repeat volta 2 {
    b4\segno b'8. fis16 b,4\staccato fis a b d
    \time 6/4
    \runOne %b4 b'8. fis16 b,4\staccato fis a b
    \time 2/4
    d->	b
    d-> b
    \time 4/4
    fis a b d
    \time 7/4
    \runOne d
    \runOne d
    \time 6/4
     \runOne
      \time 2/4
    d->	b
    d-> b
    \time 4/4
    fis a b d
    \time 7/4
    \runOne d
    \time 4/4
    fis4 fis fis8. cis16 fis,4
    a cis fis f!
    \time 6/4
    e8. b16 e,4 g a b d |
    \time 7/4
%    \repeat percent 2 {b4\coda^\markup{\teeny "TO CODA"} b'8. fis16 b,4\staccato fis a b d }
      \repeat percent 2 {b4\coda b'8. fis16 b,4\staccato fis a b d }

  }
%  \break
  \mark \markup{\box " sax solo "}
  \repeat percent 8 {\runOne d }
  \repeat percent 4 {e4 e'8. b16 e,4\staccato b d e g}
  \repeat percent 4 { b,4 b'8. fis16 b,4\staccato fis a b d }
  \time 4/4
  fis4 fis fis8. cis16 fis,4
  a cis fis f!
  \time 6/4
  e8. b16 e,4 g a b d
  \time 4/4
  \repeat percent 2 {\repeat unfold 4 {\tuplet 3/2 {b8 b b}}}
 % \break
  \repeat volta 3 {
  \mark \markup{\box " guitar solo "}
  \repeat percent 8 {b4 d cis c!}
  \repeat percent 4 {e4 es d cis}
  \repeat percent 4 {b d cis c!}
  \repeat percent 2 {fis, fis' f! e}
  b' a g fis
  e d cis c
  }
  \alternative {
    {\repeat percent 4 {b d cis c}}
    {\time 7/4 b4 b'8. fis16 b,4\staccato fis a b d
     b4 b'8. fis16 b,4\staccato fis a b d_\markup{\right-align \tiny "D.S. al coda"}}
  }
  \time 4/4
  \repeat volta 2 {
    b\coda d b d
    b8. b16 d4 b d
    b d b d
    b d8. d16 b4 d_\markup{\right-align \tiny "repeat ad lib to fade"}
  }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
