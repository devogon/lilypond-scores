\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Comfortably  Numb"
  subtitle = "Pink Floyd - 1979"
  instrument = "Bass"
  composer = "Roger Waters"
  copyright = "yes"
  poet = "transcribed by Eduardo Chuffa"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=63
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  b1:m
  b1:m
  b1:m
  a1
  g2 e2:m
  b1:m
  b1:m
  b1:m
  a1
  g2 e2:m
  b1:m
  d1
  a1
  d1
  a1
  c1
  g1
  c1
  g1
  g1
  a1
  c2 g
  d1
  d1
  a1
  d1
  a1
  c1
  g
  c
  g
  a
  c2 g
  d1
  b1
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  d
  a
  d
  a
  c
  g
  c
  g
  d
  a
  d
  a
  c
  g
  c
  g
  a
  c
  d
  d
  b
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  b
  a
  g2 e2:m
  b1
  b1

}

bass = \relative c, {
  \global
  % Music follows here.
  b4.^\markup{Intro} ~ b16 b16\staccato b2 | % 1
  b2 b | % 2
  \repeat volta 2 {
    b4.^\markup{Verse} ~ b16 b16\staccato b2  | % 3
    a2 a | % 4
    % \break
    g4 r8 fis8\staccato e2 | % 5
  }
  \alternative {
    {b'4. r8 b2}
    {b2 b}
  }
  b4. ~ b16 b16\staccato b2 | % 8
  % \break
  a4. ~ a16 a\staccato a2 | % 9
  g4 r8 fis8\staccato e2 | % 10
  b'2 b | % 11
  \repeat volta 2 {
    d2. d4\glissando | % 12
    % \break
    a1 | % 13
    d2. d4\glissando | % 14
    a2. ~ a16 a8. | % 15
    c!4 c c2 | % 16
    % \break
      d2. r16 b8. | % 17
      c!1 | % 18
  }
  \alternative {
    {g2. ~ g8 r}
    {g1}
  }
  % \break
  a4. a8\staccato a4 r16 b8. | % 21
  c!2 g | % 22
  d'2 d | % 23
  d2^\markup{Guitar Solo} d | % 24
  % \break
  a4. ~  a16 a\staccato a4 b8\glissando (c16) r16 | % 25
  %% \break
  d2 d | % 26
  a4. ~ a16 a\staccato a4 a16 (b8.) | % 27
  c!4. ~ c16 c\staccato c2 | % 28
  % \break
  g4. ~ g16 d' g4 r16 d16 g r | % 29
  c,!2 c |  % 30
  g4. ~  g16 d' g,2 | % 31
  a2 a4 ~ a16 b8. | % 32
  % \break
  c!4. c16 (d) g,2 | % 33
  d'4. ~ d16 d\staccato d2 | % 34
  b4^\markup{Verse} r8. b16 b r r8 r4 | % 35
  a4 r a r  | % 36
  % \break
  g4 r8 fis\staccato e4 r | % 37
  b'2. ~ b8. b16 | % 38
  b2. b8 r | % 39
  a4 r a8\staccato a r4 | % 40
  % \break
  g4 r8 fis\staccato e2 | % 41
  b'1 | % 42
  d2.^\markup{Chorus} d4\glissando | % 43
  a2. ~ a16 a'8. | % 44
  % \break
  d,4 d2 d4\glissando | % 45
  a2 ~ a8 r16  \override NoteHead.style = #'cross
 a \revert NoteHead.style a'\staccato b,8. | % 46
  c!2. c16\glissando (d8.) | % 47
  g,4. ~ g16 g\staccato g4 r16 c!8. | % 48
  % \break
  c!2 c4 c\glissando | % 49
  g1 | % 50
  d'2 d4 d\glissando | % 51
  a1 |  % 52
  % \break
  d2. d4\glissando | % 53
  a2 ~ a8.r16 g'\staccato b,8. | % 54
  c!2 c4 c16\glissando (d8.) | % 55
  g,2. ~ g16 b8.| % 56
  % \break
  c!4 c c c | % 57
  g4 g g2 | % 58
  a4. a8\staccato a4 a16 b8. | % 59
  c!4. c8 g2 | % 60
  % \break
  d'4 d8. d16\staccato d4 d | % 61
  d4 d d2\glissando | % 62
  \bar "||"
  b2^\markup{Guitar Solo} b | %63
  a2 a | % 64
  % \break
  g4. fis8 e2 | % 65
  b'2 b8\staccato  b d16 b r8 | % 66
  b2 b8\staccato b8 ~ b4 | % 67
  a2 a8\staccato a ~ a4 | % 68
  % \break
  g4. fis8 e2 | % 69
  b'2 b8\staccato b d16 b r8 | % 70
  b2 b8\staccato b r16 b8\staccato r16 | % 71
  a2 a8\staccato a r16 a8\staccato r16 | % 72
  % \break
  g4. fis8 e2 | % 73
  b'2 b4 b'8 a16 fis | % 74
  b,2 b8\staccato b ~ b4 | % 75
  a2 a8\staccato a ~ a4 | % 76
  % \break
  g4. fis8 e2 | % 77
  b'8. b16 b'4 b,2 | % 78
  b2 b4 b'4\glissando | % 79
  a,4 a' a, a' | % 80
  % \break
  g,4 g'8 fis, e4 e' | % 81
  b4 b' b, b'8 a16 fis | % 82
  b,8\staccato b\staccato b'\staccato b,\staccato b\staccato b\staccato b'\staccato b\staccato | % 83
  a,8\staccato a\staccato a'\staccato a\staccato a,\staccato a\staccato a'\staccato a\staccato | % 84
  % \break
  g,8\staccato g g' fis, e\staccato e e' e | % 85
  b8\staccato b b'4 b, b'8 a16 fis | % 86
  b,8\staccato b b'4 b,8\staccato b\staccato b'4  | % 87
  a,8\staccato a a'4 a,8\staccato a\staccato a'4 | % 88
  % \break
  g,8\staccato r16 g g'8 fis, e4 e'4 | % 89
  b8\staccato r16 \override NoteHead.style = #'cross
 b \revert NoteHead.style b'8. fis16 b,4 b'8 a16 fis | % 90
  b,1 | % 91
  r1
  \bar "|."

}

chordsPart = \new ChordNames \chordNames

bassPart = \new Staff { \clef "bass_8" \bass }

\score {
  <<
    \chordsPart
    \bassPart
  >>
  \layout { }
}
