\version "2.19.57"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël bosold on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Concerto in E minor for Recorder and Flute"
  subtitle = "I"
  instrument = "Contrabas/Cello"
  composer = "Georg Philipp Telemann"
  piece = "I"
  copyright = "Public Domain, 1740"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

%\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g \major
  \numericTimeSignature
  \time 3/4
  \tempo "Largo" 4=50
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
 % \set Score.markFormatter = #format-mark-rounded-box-letters
  \set Score.markFormatter = #format-mark-box-alphabet

}



contrabass = \relative c {
  \global
  % Music follows here.
  e4\f e e | % 1
  e4 r r | %  2
  dis4 dis dis | % 3
  dis r r | % 4
  e4 e e | % 5
  d4 d d | % 6
  c4 c c | % 7
  gis4 gis gis | % 8
  a2 e'4 | % 9
  d4 c2 | % 10
  b4 b b | % 11
  b2. | % 12
  \mark \default
  R1*3/4*11  | % 13-
  \mark \default
  R1*3/4*7 | % ? - ?
  \mark \default
  R1*3/4*14
  r1*3/4^\markup{\teeny harpsichord} | %
  \mark \default
  e4\f e e | %
  e4 r r | %
  dis4 dis dis | %
  dis4 r r | %
  e4 e e | %
  d4 d d | %
  c4 c c | %
  gis gis gis | %
  a2 e'4 | %
  d4 c2 | %
  b4 b b | %
  b2.
  \bar "|."

}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

% \markup {
%   \typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime
%   \typewriter \teeny " // git commit " \typewriter \teeny \gitCommit
% }

