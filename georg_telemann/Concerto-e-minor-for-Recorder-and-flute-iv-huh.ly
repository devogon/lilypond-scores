\version "2.19.57"
\language "nederlands"

\header {
  title = "Concerto in E minor for Recorder and Flute"
  subtitle = "IV"
  instrument = "Contrabas/Cello"
  composer = "Georg Philipp Telemann"
  piece = "IV"
  copyright = "Public Domain, 1740"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g \major
  \numericTimeSignature
  \time 2/2
  \tempo "Presto" 2=170
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
 % \set Score.markFormatter = #format-mark-rounded-box-letters
  \set Score.markFormatter = #format-mark-box-alphabet

}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

contrabass = \relative c {
  \global
  % Music follows here.
  e1\f (  % 1
  e1\staccatissimo\staccatissimo )( % 2
  e1\staccatissimo )( % 3
  e1\staccatissimo )( % 4
  e1\staccatissimo )( % 5
  e1\staccatissimo )( % 6
  e1\staccatissimo )( % 7
  e1\staccatissimo )( % 8
  e1\staccatissimo )( % 9
  % \break
  e1\staccatissimo )( % 10
  e1\staccatissimo )( % 11
  e1\staccatissimo )( % 12
  e1\staccatissimo )( % 13
  e1\staccatissimo )( % 14
  e1\staccatissimo )( % 15
  e1\staccatissimo )( % 16
  e1\staccatissimo )( % 17
  % \break
  e2.) g,4 | % 18
  a2 b | % 19
  e1 | % 20
  R1*2/2*30 | % 21-50
 % g4_\markup{\italic sine Violone} fis e b | % 21
 % g'4 fis e b | % 22
 % e2 e | % 23
 % e2 r | % 24
 % % \break
%  g4 fis e b | % 25%
%  g'4 fis e b | % 26
%  e2 e | % 27
%  e2 r | % 28
% b'4 a g d | % 29
%  % \break
%  b'4 a g d | % 30
%  g2 g | % 31
%  g2 r | % 32
%  b4 a g d | % 33
%  b'4 a g d | % 34
%  g2 g | % 35
%  % \break
%  g2 r | % 36
%  fis2 g | % 37
 % fis2 g | % 38
 % fis2 g | % 39
 % d2 r | % 40
 % b2 c | % 41
 % % \break
 % b2 c | % 42
 % b2 c | % 43
 % g2 r | % 44
 % g'8 a b4 a g | % 45
 % fis8 g a4 d, d | % 46
  % \break
 % d4 d d d | % 47
 % d4 d d d | % 48
 % g8 a b4 a g | % 49
 % fis8 g a4 d, fis | % 50
  g2 d | % 51
  g,2 r | % 52
  % \break
  e'1 ( | % 53
  e1\staccatissimo )( | % 54
  e1\staccatissimo )( | % 55
  e1\staccatissimo )( | % 56
  e1\staccatissimo )( | % 57
  e1\staccatissimo )( | % 58
  e1\staccatissimo )( | % 59
  e1\staccatissimo )( | % 60
  e1\staccatissimo )( | % 61
  % \break
  e1\staccatissimo )( | % 62
  e1\staccatissimo )( | % 63
  e1\staccatissimo )( | % 64
  e1\staccatissimo )( | % 65
  e1\staccatissimo )( | % 66
  e1\staccatissimo )( | % 67
  e1\staccatissimo )( | % 68
  e1\staccatissimo )( | % 69
  % \break
  e2.) g,4 | % 70
  a2 b | % 71
  e1\fermata | % 72
  % r1 | % 73
  % r1 | % 74
  % r1 | % 75
  % r1 | % 76
  % % \break
  % d2_\markup{\italic sine Violone} r | % 77
  % r1 | % 78
  % d2 r | % 79
  % d2 r | % 80
  % r1 | % 81
  % r1 | % 82
  % r1 | % 83
  % % \break
  % r1 | % 84
  % r1 | % 85
  % r1 | % 86
  % r1 | % 87
  % r2 d'4\p b | % 88
  % c2 b4 a | % 89
  % b2 c4 d | % 90
  % % \break
  % g,2 fis4 g | % 91
  % d'2 d, | % 92
  % r2 r8 d b g | % 93
  % fis'4 g d2 | % 94
  % r1 | % 95
  % r2 r8 d b g | % 96
  % % \break
  % fis'4 g fis g | % 97
  % fis4 g d c | % 98
  % b2 r | % 99
  % g2 r8 d' b d | % 100
  % g,2 r8 d' b d | % 101
  % g,2 r8 d' b d | % 102
  % % \break
  % g,4 b c d | % 103
  % g4 fis g e | % 104
  % fis2 r4 fis | % 105
  % g4 fis g e | % 106
  % fis2 r | % 107
  % d2 r | % 108
  % ais2 r | % 109
  % % \break
  % b2 r4 b' | % 110
  % e,2 fis | % 111
  % b,2 b'4 d, | % 112
  % e2 fis | % 113
  % d2 e | % 114
  % fis2 r | % 115
  % d2 r | % 116
  % % \break
  % ais2 r | % 117
  % b2 r4 b' | % 118
  % e,2 fis | % 119
  % b,2 b'4 b, | % 120
  % e2 fis | % 121
  R1*2/2*49
  b4 b'8 a g4 fis | % 122
  % \break
  e1 (  | % 123
  e1\staccatissimo )( | % 124
  e1\staccatissimo )( | % 125
  e1\staccatissimo )( | % 126
  e1\staccatissimo )( | % 127
  e1\staccatissimo )( | % 128
  e1\staccatissimo )( | % 129
  e1\staccatissimo )( | % 130
  e1\staccatissimo )( | % 131
  % \break
  e1\staccatissimo )( | % 132
  e1\staccatissimo )( | % 133
  e1\staccatissimo )( | % 134
  e1\staccatissimo )( | % 135
  e1\staccatissimo )( | % 136
  e1\staccatissimo )( | % 137
  e1\staccatissimo )( | % 138
  e1\staccatissimo )( | % 139
  % \break
  e2.) g,4 | % 140
  a2 b | % 141
  e,4 e'8 d c4 b8 a | % 142
  g2\p r | % 143
  g2 r | % 144
  g2 r | % 145
  g2 r | % 146
  % \break
  g'2 r | % 147
  g2 r | % 148
  g2 r | % 149
  g2 r | % 150
  c,2 r | % 151
  c2 r | % 152
  c2 r | % 153
  % \break
  c2 r | % 154
  c'2 r | % 155
  c2 r | % 156
  c2 r | % 157
  c2 r | % 158
  g2 fis4.\trill e16 fis | % 159
  g4 e dis e | % 160
  % \break
  g2 fis4.\trill e16 fis | % 161
  g4 e dis e | % 162
  c'4 c8 (d) b4 b8 (c) | % 163
  a4 a8 (b) g4 g8 (a) | % 164
  fis4 g fis e | % 165
  b'2 b, | % 166
  e1 (  | % 123
  e1\staccatissimo )( | % 124
  e1\staccatissimo )( | % 125
  e1\staccatissimo )( | % 126
  e1\staccatissimo )( | % 127
  e1\staccatissimo )( | % 128
  e1\staccatissimo )( | % 129
  e1\staccatissimo )( | % 130
  e1\staccatissimo )( | % 131
  % \break
  e1\staccatissimo )( | % 132
  e1\staccatissimo )( | % 133
  e1\staccatissimo )( | % 134
  e1\staccatissimo )( | % 135
  e1\staccatissimo )( | % 136
  e1\staccatissimo )( | % 137
  e1\staccatissimo )( | % 138
  e1\staccatissimo )( | % 139
  % \break
  e2.) g,4 | % 140
  a2 b | % 141
  e1\fermata
  \bar "||"
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup {
  \typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime
  \typewriter \teeny " // git commit " \typewriter \teeny \gitCommit
}

\markup {
  \teeny
  \date
}
