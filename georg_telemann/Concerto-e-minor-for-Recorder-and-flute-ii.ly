\version "2.19.55"
\language "nederlands"

date = \markup {\small{Engraved by daniël bosold on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Concerto e-minor for Recorder and flute"
  instrument = "Bass, Cello"
  composer = "Georg Philipp Telemann"
  piece = "II"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo "Allegro" 4=120
}


contrabass = \relative c {
  \set Score.markFormatter = #format-mark-box-alphabet
  \global
  % Music follows here.
  \mark \default
  e2_\markup{ con violone } fis  | % 1
  g4. a8 b a g fis | % 2
  e8 fis g e a4 g8 fis | % 3
  e4 g8 e b'4 b, | % 4
  e8 e, e e' a a, a a' | % 5
  d,8 d d d g g, g g' | % 6
  cis8 cis, cis cis' fis, e d e | % 7
  fis 8 e fis fis, b cis d b | % 8
  e4 d8 cis b4 d8 b | % 9
  % \break
  fis'4 fis, b'8 b, b b' | % 10
  e,8 e, e e' a, b c a | % 11
  d4 c8 b a4 c8 a | % 12
  e'4 e, a'8 a, a a' | % 13
  d8 d, d d' g, g, g g' | % 14
  % \break
  c8 c, c c' fis, fis, fis fis'  | % 15
  b8 a g a b a b b, | % 16
  e8 e, e e' a a, a a' | % 17
  d,8 d d d g g, g g' | % 18
  % \break
  c,8 c c c fis fis, fis fis' | % 19
  b,8 a' g a b4 b, | % 20
  \mark \default
%  \mark \markup{\box "B"}
  e4 r r2 | % 21
  r1 | % 22
  % \break
  r1 | % 23
  r1 | % 24
  r1 | % 25
  r1 | % 26
  r1 | % 27
  % \break
  \mark \default
  g,4\p_\markup{\italic sine Violone} r8 g' g,4 r | % 28
  g'4 fis g r | % 29
  b4 a g d | % 30
  b'4 a g d8 fis | % 31
  % \break
  g4 c,8 d g,4 r | % 32
  \mark \default
  g4 cis d r | % 33
  r1 | % 34
  r1 | % 35
  % \break
  r1 | % 36
  r1 | % 37
  r1 | % 38
  \mark \default
  a4\p r8 a' a,4 r | % 39
  % \break

  a'4 gis a r | % 40
  c4 b a e | % 41
  c'4 b a e8 gis | % 42
  a4 d,8 e a,4 r | % 43
  % \break
  d4 r g r | % 44
  c,4 r r2 | % 45
  c4 r r2 | % 46
  d4 r8 d e d e fis | % 47
  g4 r8 c, d4 d | % 48
  % \break
  \mark \default
  g,8\f_\markup{\italic con Violone} a b g c4 b8 a | % 49
  g4. e'8 d c b a | % 50
  g8 a b g c4 b8 a | % 51
  g8 a b g d'4 d | % 52
  g,4 r8 g' c c, c c' | % 53
  % \break
  fis,8 fis, fis fis' b b, b b' | % 54
  e,8 e, e e' a a, a a' | % 55
  a8 a, a a' b4 r | % 56
  b,\p_\markup{\italic sine Violone} r b r | % 57
  % \break
  b4\p r r2 | % 58
  b'8 (b) b (b) b (b) b (b)  | % 59
  e,4 r e r | % 60
  e,4 r e r | % 61
  % \break
  e4 r r2 | % 62
  e'8 (e) e (e) e (e) e (e)  | % 63
  a,4 r a' r | % 64
  a,4 r a r | % 65
  % \break
  a4 r r2 | % 66
  a'8 a a a a a a a | % 67
  d,4 r r2 | % 68
  % \break
  g8 g g g g g g g | % 69
  c,4 e8 g c,4 e8 g | % 70
  c,4 e8 g c,8 c' b g | % 71
  a8 g a b c4 r8 f,! | % 72
  % \break
  g8 g, gis' e a4 r8 d, | % 73
  e8 d e e,\f_\markup{\italic con Violone} a'2 | % 74
  b2 c4. d8 | % 75
  e8 d c b a b c a | % 76
  d4 c8 b a4 c,8 a | % 77
  % \break
  e'4 e, a r8 a' | % 78
  d8 d, d d' g, g, g g' | % 79
  c8 c, c c' d d, d d' | % 80
  g,8 e e e' a, a, a a' | % 81
  % \break
  d,8 c b c d c d d | % 82
  g,4 r8 g' g,4 r8 g' | % 83
  g,4 r8 g' g,4 g' | % 84
  c,4\f_\markup{\italic con Violone} c'8 b a fis b b, | % 85
  % \break
  e4 fis8 e d b e e, | % 86
  a4\p_\markup{\italic sine Violone} r8 a' a,4 r8 a'8 | % 87
  a,4  r8 a' a,4 a' | % 88
  d,4\f_\markup{\italic con Violone} d'8 cis b gis cis cis, | % 89
  % \break
  fis4 g8 fis e cis fis fis, | % 90
  b4 c'8 b a fis b b, | % 91
  e4 r a\p_\markup{\italic sine Violone} r | % 92
  d,4 r a' r | % 93
  % \break
  c,4 r fis r | % 94
  b,4 dis e dis | % 95
  e4 b r2 | % 96
  r1 | % 97
  % \break
  r4 r8 b'8 cis b cis dis | % 98
  e4 r8 e, b' a b b, | % 99
  e4\f_\markup{\italic con Violone} c'8 b a fis b b, | % 100
  e4 f!8 e d b e e, | % 101
  a4 b'8 a g e a a, | % 102
  % \break
  d4 e8 d c a d d | % 103
  g,4 r cis\p_\markup{\italic sine Violone} r | % 104
  d4 r g, r | % 105
  c4 r fis, r | % 106
  % \break
  b4 r e r | % 107
  a,4 r r2 | % 108
  a4 r r2 | % 109
  b4 r8 b'8 cis b cis dis | % 110
  e4 r8 e, b' a b b, | % 111
  % \break
  e2\f_\markup{\italic con Violone} fis | % 112
  g4. a8 b a g fis | % 113
  e4 g8 fis e4 d8 cis | % 114
  b4 d8 b fis'4 fis, | % 115
  % \break
  b4 r8 b'8 e e, e e' | % 116
  a,8 a, a a' d d, d d' | % 117
  g,8 g, g g' c c, c c' | % 118
  % \break
  fis,8 fis, fis fis' b a g a | % 119
  b8 a b b, e4 f!8 e | % 120
  d8 b e e, a4 b'8 a | % 121
  % \break
  g8 e a a, d4 e8 d | % 122
  c8 a d c b r c'\staccato r | % 123
  a8\staccato r b\staccato r e,\staccato r c\staccato r | % 124
  a8\staccato r b\staccato r e,4 r | % 125
  \bar "|."
}

\score {
  \new Staff \with {
 %   instrumentName = "Contrabass"
 %   shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}
