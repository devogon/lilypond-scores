\version "2.19.57"
\language "nederlands"

date = \markup {\teeny{Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Concerto in E minor for Recorder and Flute"
  subtitle = "III"
  instrument = "Contrabas/Cello"
  composer = "Georg Phnilipp Telemann"
  piece = "III"
  copyright = "Public Domain, 1740"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



global = {
  \key e \major
  \numericTimeSignature
  \time 4/4
  \tempo "Largo" 4=50
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
 % \set Score.markFormatter = #format-mark-rounded-box-letters
  \set Score.markFormatter = #format-mark-box-alphabet

}



contrabass = \relative c {
  \global
  % Music follows here.
  e4\mf r dis r | % 1
  cis4 r gis r | % 2
  a4 gis fis b | % 3
  \mark \default
  e2 r8 e8^\markup{\small pizz.} dis b | % 4
  r8 e e, gis r cis a gis | % 5
  \break
  r8 e' e, gis' r a a, a' | % 6
  r8 b b, b' r dis, e e, | % 7
  \mark \default
  b'8 dis e fis r b b, dis | % 8
  r8 gis e fis r b b, dis | % 9
  r8 e e, e' r fis fis, fis' | % 10
  \break
  r8 ais b b, fis' ais fis ais | % 11
  \mark \default
  r8 b b, dis r e e, gis' | % 12
  r8 g,! a' fis r b b, b' | % 13
  r8 e, e, e' r a, a' e | % 14
  \break
  r8 a, a' e a, d! e e, | % 15
  r8 a a' a, r gis gis' gis, | % 16
  r8 cis cis' cis, r fis, fis' cis | %
  r8 fis, fis' cis fis, b' cis cis,
  fis,8 fis' gis cis r ais fis ais | %
  \break
  \mark \default
  r8 b, cis fis r dis b dis | %
  r8 e a,! b r e e, gis | %
  r8 cis a b r e e, gis' | %
  r8 a a, a' r b b, b' | %
  r8 dis, e e, b' b dis b | %
  \break
  r8 e, e' cis r fis fis, fis' | %
  r8 b b, b' r e, e' b | %
  r8 e, e' b e, a b b, | %
  r8 e, e' cis r fis fis, fis' | %
  r8 b b, b' r e, e' b | %
  \break
  r8 e, e' b e, a b b, | %
  \mark \default
  e4 r dis^\markup{arco} r | %
  cis4 r gis r | %
  a4 gis fis b | %
  e2 r | %
  \bar "|."

}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
