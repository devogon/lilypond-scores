\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}


\header {
  title = "The Man Who Sold the World"
  subtitle = "Nirvana"
  instrument = "Bass"
  %composer = "Nirvana"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=114
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1

}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c {
  \global
  % Music follows here.
  \mark \markup{\box A Intro}
  R1*2
  g1 ~
  g1
  f8\segno f f c c c4 e8 | % 4
  \break
  f8 f f c ~ c f e dis | % 5
  d1 ~
  d2 d4. e,8 |
  \bar "||"
  \break
  \mark \markup{\box B Verses}
  a4 a8\staccato e' ~ e a e e | % 6
  a,4 r8 a a gis a bes |
  d4 d8 a' ~ a d, a' d, |
  \break
  d4 d8 d d g f e |
  a,4 a'8 e ~ e  a, a a |
  a4 a8 a ~ a a g' fis |
  f4 f8 c' ~ c f, f f |
  \break
  f4 f8 f ~ f g f d |
  c4 c8 g' ~ g c, a c |
  c4 c8 c ~ c d c a |
  a4 e'8 a, a a e' a, |
  \break
  \time 2/4
  a4 a8 e' | % 20
  \time 4/4
  d4 d8 a' ~ a d, d' d, |
  d4 d8 a' ~ a d, d d |
  \bar "||"
  \break
  \mark \markup{\box C Chorus}
  c8 d e f g a bes c | % 23
  c,8 d e f g a bes c |
  c,8 d e f g a bes c |
  f,, g a bes c d e f |
  \break
  cis4. cis8 f4 cis | % 27
  f,8 g a bes c d e f |
  c8 d e f g a bes c |
  f,,8 g a bes c d e f |
  \break
  cis4. cis8 f4 cis | % 31
  a'1 ~
  a |
  d1 ~ |
  d^\markup{to coda - D.S. al Coda} |
  \bar "||"
  \break
  d4.\coda a8 ~ a d d a, | % 36
  c d e f g a bes c |
  f,, g a bes c d e f |
  cis4. cis8 f4 cis |
  \break
  f,8 g a bes c d e f | % 40
  c d e f g a bes c |
  f,, g a bes c d e f |
  cis4. cis8 f4 cis  |
  \break
  a'1 ~ | % 44
  a2. r4 |
  d1 ~ |
  d4. a8 ~ a d d e,, |
  \break
  \bar "||"
  \mark \markup{\box D Outro}
  \repeat volta 2 {
    f'8 c f c ~ c c4 e8 | % 48
    f c f c ~ c f e dis |
    d d d a ~ a a4 cis8 |
    d d d a ~ a d cis bes |
    \break
    a a a e' ~ e a, a a | % 52
    a a a a' ~ a a, bes cis |
    d d d a ~ a a4 cis8|
  }
  \alternative {
    {d d d a ~ a a d e \break}
    {d d d a a a d e}
  }
  f1
  r
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff \with {
%  instrumentName = "Electric bass"
} { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
