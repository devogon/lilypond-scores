\version "2.19.80"
\language "nederlands"

date = \markup {\teeny {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Summa"
  subtitle = "für streichorchester (1977/1991)"
  instrument = "violoncello, contrabas"
  composer = "Arvo Pärt"
  arranger = "(* 1935)"
  copyright = "something something something"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
%  indent = 0.0\cm
  page-count = 2
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \numericTimeSignature
  \time 9/4
  \tempo 4=100
  \set Score.markFormatter = #format-mark-box-numbers
}

cello = \relative c' {
  \global
  % Music follows here.
  \mark \default
  R1*9/4 |
  \time 14/4 g2 a4 bes (g) c2 d4 es (d) f \breathe es (bes) d | % 2
  % \break
  \time 9/4 c4 bes (g) a g f es \breathe f (d) | % 3
  \mark \default
  g bes c d es (d) f es (bes) \breathe | % 4
  % \break
  \time 14/4 d2 c4 bes (g) a g2 f4 es2 \breathe f4 (d) a | % 5
  \time 8/4 R1*8/4 | % 6
  \mark \default
  \time 10/4 R1*10/4 | % 7
  % \break
  \time 14/4 g'4 c2 d4 es (d) f \breathe es (bes) d c2 bes4 (g) | % 8
  \time 8/4 a4 g f es f (d) a' bes | % 9
  % \break
  \mark \default
  \time 9/4 g4 d' es (d) \breathe f es (bes) d c | % 10
  \time 14/4 bes4 (g) a g2 f4 es2 f4 (d) a' \breathe bes (g) c  | % 11
  \time 9/4 R1*9/4 | % 12
  \mark \default
  \time 10/4 R1*10/4 | % 13
  \time 12/4 g4 es' f es (bes) d c2 bes4 (g) \breathe a4 g | % 14
  % \break
  \time 9/4 f4 es f (d) a' bes (g) c \breathe d  | % 15
  \mark \default
  g,4 f' es (bes) d c bes (g) \breathe a | % 16
  % \break
  \time 12/4 g f es2 f4 (d) a' bes (g) c d es | % 17
  \time 8/4 R1*8/4 | % 18
  \mark \default
   R1*8/4 | % 19
   % \break
  \time 12/4 g,4 es' d c2 bes4 (g) a g f2 es4 \breathe | % 20
  \time 10/4 f4 (d) a' bes g c d es (d) \breathe f | % 21
  \mark \default
  \time 8/4 g,4 d' c bes (g) a g f | % 22
  \time 12/4 es4 f (d) a' bes (g) c d es (d) f es | % 23
  % \break
  \time 10/4 R1*10/4 | % 24
  \mark \default
  \time 8/4 R1*8/4 | % 25
  \time 13/4 g,4 c2 bes4 (g) \breathe a g f2 es4 f (d) a' \breathe | % 26
  % \break
  \time 10/4 bes4 (g) c d es (d) f es (bes) d \breathe | % 27
  \mark \default
  \time 8/4 g, bes a g f es f (d) | % 28
  % \break
  \time 13/4 a' \breathe bes (g) c d es (d) f es (bes) d2 c4  | % 29
  \time 10/4 R1*10/4 | % 30
  \mark \default
  \time 8/4 R1*8/4 | % 31
  % \break
  \time 12/4 g4 a g f2 es4 f (d) a' bes (g) c \breathe | % 32
  \time 9/4 d4 es (d) f es (bes) d c bes \breathe | % 33
   % \break
   \mark \default
  \time 9/4 g4 g f \breathe es f (d) a' bes (g)| % 34
  \time 13/4 c4 d \breathe es (d) f es (bes) d2 c4 bes (g) a | % 35
  % \break
  \time 10/4 R1*10/4| % 36
  \mark \default
  \time 8/4 R1*8/4 | % 37
  \time 14/4 g4 f2 es4 f (d) \breathe a'4 bes (g) c2 d4 es4 (d) | % 38
  % \break
  \time 9/4 f4 es (bes) \breathe d c bes (g) a g | % 39
  \mark \default
  \time 9/4 g4 es f (d) a' \breathe bes (g) c d | % 40
  % \break
  \time 14/4 es4 (d) f es (bes) d2 c4 bes (g) a g2 f4 | % 41
  \time 9/4 R1*9/4 | % 42
  \mark \default
  R1*9/4 | % 43
  \time 14/4 g4 f2 a4 bes (g) c2 d4 es (d) f es (bes) | % 44
  \time 8/4 d4 c bes (g) \breathe a g f es | % 45
  % \break
  \mark \default
  \time 9/4 g4 a bes (g) c d es (d) f | % 46
  \time 14/4 es4 (bes) d c2 \breathe bes4 (g) a g2 f4 es2 f4 | % 47
  \override Staff.NoteHead.style = #'baroque
  \time 8/4 g\breve | % 48
   g\breve | % 49
  \bar "|."
}

contrabass = \relative c {
  \global
  % Music follows here.
  R1*9/4
  R1*14/4
  R1*9/4
  R1*9/4
  <d' d,>2^\markup{div.} <c c,>4 <bes bes,> <g g,> <a a,> <g g,>2 <f f,>4 <es es,>2 \breathe <f f,>4 (<d d,>) <a' a,> | % 5
  R1*8/4 |
  R1*10/4
  R1*14/4
  R1*8/4
  R1*9/4
  <bes bes,>4 (<g g,>) <a a,> <g g,>2 <f f,>4 <es es,>2 <f f,>4 (<d d,>4) <a' a,> \breathe <bes bes,> (<g g,>) <c c,> | % 11
  \time 9/4 R1*9/4 | % 12
  \time 10/4 R1*10/4 | % 13
  \time 12/4 R1*12/4 | % 14
  \time 9/4 R1*9/4 | % 15
  R1*9/4 | % 16
  <g g,>4 <f f,> <es es,>2 <f f,>4 (<d d,>) <a' a,> <bes bes,> (<g g,>) <c c,> <d d,> <es es,> | % 17
 \time 8/4 R1*8/4 | % 18
 R1*8/4 | % 19
 R1*12/4 | % 20
 R1*10/4 | % 21
  R1*8/4 | % 22
  <es, es,>4 <f f,> <d d,> <a' a,> <bes bes,> (<g g,>) <c c,> <d d,> <es es,> (<d d,>) <f f,> <es es,> | % 23
  R1*10/4 | % 24
  R1*8/4 | % 25
  R1*13/4 | % 26
  R1*10/4 | % 27
  R1*8/4 | % 28
  <a, a,>4 \breathe <bes bes,> (<g g,>) <c c,> <d d,> <es es,> (<d d,>) <f f,> <es es,> (<bes bes,>) <d d,>2 <c c,>4 | % 29
  R1*10/4 | % 30
  R1*8/4 | % 31
  R1*12/4 | % 32
  R1*9/4 | % 33
  R1*9/4 | % 34
  <c c,>4 <d d,> <es es,> (<d d,>) <f f,> <es es,> (<bes bes,>) <d d,>2 <c c,>4 <bes bes,> (<g g,>) <a a,> | % 35
  R1*10/4 | % 36
  R1*8/4 | % 37
  R1*14/4 | % 38
  R1*9/4 | % 39
  R1*9/4 | % 40
  <es' es,>4 (<d d,>) <f f,> <es es,> (<bes bes,>) <d d,>2 <c c,>4 <bes bes,>4 (<g g,>) <a a,> <g g,>2 <f f,>4 | % 41
  R1*9/4 | % 42
  R1*9/4 | % 43
  R1*14/4 | % 44
  R1*8/4 | % 45
  R1*9/4 | % 46
  <es' es,>4 (<bes bes,>) <d d,> <c c,>2 \breathe <bes bes,>4 (<g g,>) <a a,> <g g,>2 <f f,>4 <es es,>2 <f f,>4 | % 47
  \override Staff.NoteHead.style = #'baroque
  <g g,>\breve | % 48
  <g g,>\breve | % 49
}

celloPart = \new Staff \with {
  instrumentName = "Violoncello"
  shortInstrumentName = "Vc."
} { \clef bass \cello }

contrabassPart = \new Staff \with {
  instrumentName = "Contrabass"
  shortInstrumentName = "Cb."
} { \clef bass \contrabass }

\score {
  \new GrandStaff <<
    \celloPart
    \contrabassPart
  >>
  \layout { }
}
