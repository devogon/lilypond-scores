\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Serenada"
  subtitle = \markup{\teeny "für fünf streichinstrumente, Nachtwächter-bas und cembalo"}
  composer = "Heinrich I. Fr. Biber"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
  footer = "archiv Nr. 112"
  % ermwut2 = "Alle Rechte vorbehalten; Copyright 1934 by Adolph Nagel, Hannover"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
  \set Score.markFormatter = #format-mark-box-alphabet
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

serenada = \relative c {
   \global
 %    \tempo "Serenada" % 4=110
  \partial 16 c16  %
  c2 r | % 1
  c8 c c4 c r | % 2
  r2 r4 r8 r16 c16\p | % 3
  c2 r | % 4
  c8 c c4 c r | % 5
  \break
  g'8\f e f d c a g e' | % 6
  f8 d g c, g4 c8 e, | % 7
  f8\p d' g, c g4 c | % 8
  r1 | % 9
  \tempo "Adagio"
  e2 e,4. fis8 | % 10
  \break
  gis4 gis a b | % 11
  c4 d e8 b e d | % 12
  cis4 d8 a b d c!4 | % 13
  g'8 d e c b2 | % 14
  c4. c8 b2 | % 15
  \break
  a2 g2 | % 16
  c4 e f fis | % 17
  g1 | % 18
  g2 c,8.\p d16 e4 | % 19
  f4 fis g2 | % 20
  g,1 | % 21
  c2 r | % 22
  \bar "|."
}

allamanda = \relative c {
  \global
  \time 2/2
%  \tempo "Allamanda" % 4=110
  \repeat volta 2 {
    \partial 8 c8 | % 0
    c8. d16 e8 c g'4 e | % 1
    f8 g a b c b a4 | % 2
    g4. f8 e4. e8 | % 3
    f8 a b c a4. g8 | % 4
    \break
    g2 (g8) f e d | % 5
    c8 c' a d b g g8. fis16 | % 6
    g8 b c d e4. d16 c | % 7
    d2 d, | % 8
    g2 g4.  % 9
  }
  \repeat volta 2 {
    \partial 8 g8 |
    g2. f8 d | % 10
    c16 c' b a gis4 a8 g!16 f e8. d16 | % 11
    c4 cis d8 e f4 | % 12
    e2 f8 e d4 | % 13
    \break
    cis2 d8 e f fis | % 14
    g4 a g8 c a b | % 15
    c4 f, g c,8 e | % 16
    f4 c d2 | % 17
    c4 f g2 | % 18
    c2 c4. % 19
  }
}

aria = \relative c {
  \global
  \time 3/2
  \repeat volta 2 {
    c'4. d8 e c b4 g b | % 1
    c4 c,8 d e c g'2. | % 2
    c,8 d e c f e d e f g a g | % 3
    \break
    f8 g a b c b a e fis a g b | % 4
    c8 a d4 d, g8 a b g c b | % 5
    g8\p e fis a g b c a d4 d, | % 6
    g1. | % 7
    \break
  }
  \repeat volta 2 {
    g2. c2.  | % 8
    a4 g e f a bes | % 9
    c4 cis d g, a2 | % 10
    d,2. e2. | % 11
    \break
    f2 e4 a2 g4 | % 12
    c4 g2 c,4 d e | % 13
    f2\p e4 a2 g4 | % 14
    c4 g2 c,2. | % 15
  }
}

gavotte = \relative c {
  \global
  \time 2/2
  \repeat volta 2 {
    a'4 gis a b | % 1
    c8 c, d4 e8. fis16 gis4 | % 2
    a4 gis a b | % 3
    c8 c, d4 e r | % 4
    R1*2*2/2 | % 5-6
    % \clef treble
    % http://lilypond.org/doc/v2.18/Documentation/notation/multiple-voices
    % a'8^\markup{\teeny "viol. 1"} c e b c a' d, g  | % 7
    R1
    % e8 c' b8. a16 gis4 r | % 8
    R1
    % \clef bass
    g4 a b8. cis16 dis4 | % 9
    e8 g, a b e,2 | % 10
    g4\p a b8. cis16 dis4 | % 11
    e8 g, a b e,2 | % 12
  }
  \break
  \repeat volta 2 {
    e'4 cis d b | % 13
    c4 f, g c, | % 14
    d4 e f8 d e4 | % 15
    a8 f d e a4 r | % 16
    R1*2*2/2 | % 17-18
    \break
    R1 | % 19
    R1 | % 20
    c4 b c d | % 21
    e2 a, | % 22
    c4\p b c d | % 23
    e2 a, | % 24
  }
}

retirada = \relative c {
  \global
  \repeat volta 2 {
    c4 d e f8 c | % 1
    d8 b c a g4 c8 d | % 2
    e8 c g'4 e f8 c | % 3
    d8 b c a g2 | % 4
  }
  \break
  \repeat volta 2 {
    g4 gis a b | % 5
    c4 d e f | % 6
    e2 a,4 b | % 7
    c4 d e a, | % 8
    b2 e, | % 9
  }
  \repeat volta 2 {
    e'4 r8 e a,4 g | % 10
    \break
    c4 a g r8 e' | % 11
    f4 r8 e f g a b | % 12
    c4 r8 b, c d e fis | % 13
    g4 a g2 | % 14
    a8 g f4 g2 | % 15
    c,1 | % 16
  }
}

ciacona = \relative c {
  \global
  \time 3/2
  R1*20*3/2 | %  1-20
  c1 d2 | % 21
  e1 f2 | % 22
  g1 g2 | % 23
  e1\fermata e2 | % 24
  c1 d2 | % 25
  e1 f2 | % 26
  g1 g2 | % 27
  \break
  e1\fermata g2 | % 28
  f1 d2 | % 29
  g1. | % 30
  e1\fermata g2 | % 31
  g1 g2 | % 32
  f1 e2 | % 33
  d1. ( | % 34
  \break
  c1\fermata) e2 | % 35
  f1 g2 | % 36
  d1 (d4) e | % 37
  c1. | % 38
  R1*19*3/2 | % 39-57
  \break
  c1 d2 | % 58
  e1 f2 | % 59
  g1 g2 | %
  e1\fermata e2 | %
  c1 d2 | %
  e1 f2 | %
  \break
  g1 g2 | %
  e1\fermata g2 | %
  f1 d2 | %
  g1. | %
  e1\fermata g2 | %
  g1 g2 | %
  f1 e2 | %
  \break
  d1. | %
  c1\fermata e2 | %
  f1 g2 | %
  d1 (d4) e | %
  c1. | %
  R1*20*3/2
  \bar "|."
}

words = \lyricmode {
  Lost Ihr Herrn Undt last euch sagn, der Ham mer der hat ney ne geschlagn, hüets Fey er hüets wohl, Undt lo bet Gott den Herrn,  Undt Un ser lie- be Frau.
  Lost Ihr Herrn Undt last euch sagn, der Ham mer der hat Zeh ne geschlagn, hüets Fey er hüets wohl, Undt lo bet Gott den Herrn, -  Undt Un ser lie- be Frau.
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \serenada }
  \layout { }
  \header {
    piece = \markup{\bold \larger "Serenada"}
  }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \allamanda }
  \layout { }
    \header {
    piece = \markup{\bold \larger "Allamanda"}
  }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \aria }
  \layout { }
    \header {
    piece = \markup{\bold \larger "Aria"}
  }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \gavotte }
  \layout { }
    \header {
    piece = \markup{\bold \larger "Gavotte"}
  }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \retirada }
  \layout { }
    \header {
    piece = \markup{\bold \larger "Retirada"}
  }
}

\score {
%  <<
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \ciacona \addlyrics \words }
%  \new Lyrics \lyricsto "one"
%  \lyrics
  \layout { }
    \header {
    piece = \markup{\bold \larger "Ciacona"}
  }
%  >>
}

% \markup \column {
%   \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
%   \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
%   \hspace #1
% }
%
% \markup {
%   \teeny
%   \date
% }