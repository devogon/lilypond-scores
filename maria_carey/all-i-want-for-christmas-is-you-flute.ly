\version "2.19.80"
\language "nederlands"

date = \markup {\teeny {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "All I Want for Christmas is You"
  subtitle = "versie 2"
  instrument = "Dwarsfluit"
  composer = "Maria Carey / Walter Afanasieff"
  arranger = "Marijn van Prooijen"
%  meter = "150"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo "Moderately fast shuffle" 4=150
}

flute = \relative c'' {
  \global
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
  R1*4^\markup{\teeny \box "intro"}
  R1*4^\markup{\teeny \box "uwc maastricht"}
  R1*4^\markup{\teeny \box "de notenbalk"}
  R1*4^\markup{\teeny \box "de vogelaar"}
  \break
  b'!2.\mf (a4) | % 17
  b4 ( a g2) | % 18
  c,4\< (es g a!) | % 19
  bes4\> ( a! g2\!) | % 20
  \break
  g4^\markup{\box "REFREIN 1"} ( a fis g  | % 21
  e fis dis2 ) | % 22
  g4 ( a fis g | % 23
  e fis es2) | % 24
  d4 ( e! g d') | % 25
  \break
  c1 | % 26
  b4 ( a g e) | % 27
  es2 ( fis!) | % 28
  g1 ~ | % 29
  g2 r | % 30
  R1*2 | %
  \break
  R1*4^\markup{\teeny \box "de octopus"}
  R1*4^\markup{\teeny \box "de zonnebloem"}
  R1*4^\markup{\teeny \box "de casembroot"}
  R1*4^\markup{\teeny \box "de poolster"}
  \break
  g4^\markup{\box "REFREIN 2"} ( a fis g  | % 49
  e fis dis2 ) | % 50
  g4 ( a fis g | % 51
  e fis es2) | % 52
  d4 ( e! g d') | % 53
  \break
  c1 | % 54
  b4 ( a g e) | % 55
  es2 ( fis!) | % 56
  g1 ~ | % 57
  g2 r | % 58
  R1*2 | % 59
  \break
  R1*16^\markup{\box "BRIDGE - KLAPPEN"}
  R1*4^\markup{\teeny \box "mgr zwijsenschool"}
  R1*4^\markup{\teeny \box "de cirkel"}
  R1*4^\markup{\teeny \box "op streek"}
  R1*4^\markup{\teeny \box "het lichtschip"}
  \break
  g4^\markup{\box "REFREIN 3b"} ( a fis g  | % 93
  e fis dis2 ) | % 94
  g4 ( a fis g | % 95
  e fis es2) | % 96
  d4 ( e! g d') | % 97
  c1 | % 98
  \break
  b4 ( a g e) | % 99
  es2 ( fis!) | % 100
  \bar "||"
  g4^\markup{\box "CODA"} ( a fis g  | % 101
  e fis dis2 ) | % 102
  g4 ( a fis g | % 103
  \break
  e fis es2) | % 104
  d4 ( e! g d') | % 105
  c1 | % 106
  b4^\markup{\bold "rall."} ( a g e) | % 107
  es2 (fis) | % 108
  g1\fermata | % 109
  \bar "|."
}

\score {
  <<
  \new Staff \with {
%   instrumentName = "Flute"
 %   shortInstrumentName = "Fl."
  } \flute
  \context NoteNames \flute
  >>
  \layout { }

}
