\version "2.19.80"
\language "nederlands"

date = \markup {\teeny {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "All I Want for Christmas is You"
  subtitle = "versie 2"
  instrument = "Cello"
  composer = "Maria Carey / Walter Afanasieff"
  arranger = "Marijn van Prooijen"
  %meter = "150"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent	= 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo "Moderately fast shuffle" 4=150
}

cello = \relative c {
    \global
    \compressFullBarRests
    \override MultiMeasureRest #'expand-limit = 1
    R1*4^\markup{\teeny \box "Koor"}
    R1*4^\markup{\teeny \box "UWC Maastricht"}
    R1*4^\markup{\teeny \box "De Notenbalk"}
    R1*4^\markup{\teeny \box "De Vogelaar"}
    \break
    e1\mf ~ | % 17
    e2 g4 (e) | % 18
    es1\< ~ | % 19
    es1\> | % 20
    \break
    b'1\!(~ | % 21
    b2 a)  | % 22
    g1 ~ | % 23
    g1 | % 24
    g1 | % 25
    \break
    gis1 | % 26
    g!1 | % 27
    fis1 | % 28
    g1 ~ | % 29
    g2 r | % 30
    R1*2 | %
    \break
    R1*4^\markup{\teeny \box "De Octopus"}  | %
    R1*4^\markup{\teeny \box "De Zonnebloem"}
    R1*4^\markup{\teeny \box "De Casembroot"}
    R1*4^\markup{\teeny \box "De Poolster"}
    \break
    b1^\markup{\box "REFREIN 2"} ( ~  | % 49
    b2 a) | % 50
    g1 ~ | % 51
    g1 | % 52
    g1 | % 53
    \break
    gis1 | % 54
    g!1 | % 55
    fis1 | % 56
    g1 ~ | % 57
    g2 r | % 58
    R1*2 | %
    \break
    R1*16^\markup{\box "BRIDGE - KLAPPEN"}
    R1*4^\markup{\teeny \box "Mgr. Zwijsenschool"}
    R1*4^\markup{\teeny \box "De Cirkel"}
    R1*4^\markup{\teeny \box "Op Streek"}
    R1*4^\markup{\teeny \box "Het Lichtschip"}
    \break
    b1^\markup{\box "REFREIN 3b"} ( ~ | % 93
    b2 a) | % 94
    g1 ~ | % 95
    g1 | % 96
    g1 | % 97
    gis1 | % 98
    \break
    g!1 | % 99
    fis1 | % 100
    \bar "||"
    b1^\markup{\box "CODA"} ~ ( | % 101
    b2 a) | % 102
    g1 ~ | % 103
    \break
    g1 | % 104
    g1 | % 105
    gis1 | % 106
    g!1^\markup{\bold "rall."} | % 107
    fis1 | % 108
    g1\fermata
    \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Cello"
 %   shortInstrumentName = "Cl."
  } { \clef bass \cello }
  \layout { }
}
