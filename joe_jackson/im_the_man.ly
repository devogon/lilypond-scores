\version "2.23.13"

\header {
  title = "I‘m The Man"
  subtitle = "Joe Jackson - I‘m The Man - 1980"
  instrument = "Bass"
  composer = "Joe Jackson"
  arranger = "Arcellius Sykes"
  poet = "bass: Graham Maby"
}

\paper { indent = #0 }

global = {
  \key a \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s2
  e2
  a2
}

electricBass = \relative c, {
  \global
  % Music follows here.
  \partial 2 r4 r |
  \set Score.currentBarNumber = #0
  e,8 e e g! a a a b | %  2
  d d d b a a a fis | % 3
  e8 e e g! a a a b | %  4
  d d d b a a a fis | % 5
  \break
  e8 e e g! a a a b | %  6
  d d d b a a a b | % 7
  d d d b a a a b | % 8
  d d d b a a a fis | % 9
  e e' \repeat unfold 30 {e}
  \break
  \bar "||"
  \mark \markup{\box "Verse 1"}
  \repeat unfold 16 {e}
  e, e e g!\glissando e' e e e |
  \repeat unfold 8 {e} |
  \break
  d d d b a a a fis |
  \repeat unfold 8 {e} |
  \repeat unfold 8 {b'} |
  a a a a a a d dis |
  \break
  \repeat unfold 16 {e} |
  e, e e b' \repeat unfold 12 {e} |
  \break
  d d d b a a r fis |
  e e e e e gis a ais |
  b b b b b fis b b |
  b fis b fis b fis b cis |
  \break
  \bar "||"
  \mark \markup{\box "Pre-Chorus 1"}
  cis cis r b a a r b |
  cis cis r b a a r b |
  e e e e e e a, ais |
  b b b fis fis b b c! |
  \break
  cis cis r b a a r b |
  cis cis r b a a r a |
  b b fis' fis b, b fis' b, | % or b b fis fis b b fis b
  d d a' a d, d a' b, | % or d d a a d d a d
  % bottom of first page
  \break
  \bar "||"
  \mark \markup{\box "Chorus 1"}
  e,8 e e fis a a a b |
  d d d b a a a fis |
  e e e fis a a a b |
  d d d b a a a b |
  \break
  e,8 e e fis a a a b |
  d d d b a a a fis |
  e e e fis a a a b |
  d1 ~ |
  d |
  \break
  e,4 r8 e' e e e e |
  \repeat unfold 24 {e}
  \break
  \bar "||"
  \mark \markup{\box "Verse 2"}
  \repeat unfold 16 {e}
e, e e g!\glissando e' e e e |
  e e e e e e e e |
  \break
  d8 d d b a a a fis |
  e e e e e gis a ais |
  b b b b b b b b |
  a a cis cis' d, d' dis, dis' |
  \break
  \repeat unfold 16 {e,}
  e, e e b' e e e e |
  e e e e e e e e |
  \break
  d d d b a a r fis |
  e e e e e gis a ais |
  b b b b b fis' b, b |
  b fis' b, fis' b, fis' b, fis' |
  \bar "||"
  \break
  \mark \markup{\box "Pre-Chorus 2"}
  cis cis r b a a a' a, |
  cis cis cis' cis, a a a' a, |
  e' e d d cis cis c! c |
  b b ais ais b4 b8 c! |
  \break
  cis8 cis r b a a r b |
  cis cis cis' cis, a a a' a, |
  b b fis' fis b, b fis' b, |
  d d a a d d a d |
  \break
  \bar "||"
  \mark \markup{\box "Chorus 2"}
   e,8 e e fis a a a b |
  d d d b a a a b |
  e, e e fis a a a b |
  d d d b a a a fis |
  \break
   e8 e e fis a a a b |
  d d d b a a a fis |
  e e e fis a a a b |
  d1 ~ |
  d |
  \break
  \mark \markup{\box "Guitar Solo"}
  e,8 e e' e e e e e |
  e e, e e e e' e e |
  e e, e e e e' e e |
  e b e e e b e e |
  \break
  e b e e e b e e |
  e b e e d d cis b |
  e e e e e e e e |
  e e e e e b e e |
  \break
  e e e e gis e a gis |
  e e e e gis e a gis |
  e e e e gis e a gis |
  e e e e gis e a gis |
  \break
  e e e e gis e a gis |
  e e cis cis d d dis dis |
  e e e e gis e a gis |
  e e e e gis e a gis |
  \break
  e e cis' cis e, e d' d |
  e, e cis' cis e, e d' d |
  e, e e e gis e a gis |
  e e e e gis e a gis |
  \break
    e e cis' cis e, e d' d |
    e, e cis' cis e, e d' d |
  e, e cis' e, d' e, cis' d, |
\break
d' e, cis' e, d' e, cis' e, |
d' e, cis' e, d' e, cis' r |
\grace d (e) e e e e e e  b |
\break
\bar "||"
  \mark \markup{\box "Verse 3"}
  e,, e e e e' e e e |
  e e e e e e e a,|
  a2 e4 e |
  e'8 e e e e e e a, |
  d d d b a a a fis |
  \break
  e e e e e gis a ais |
  b b b b b b b b |
  a a cis a d a dis a |
  \bar "||"
  \repeat unfold 8 {e'}
  \break
  \repeat unfold 8 {e}
  a,2 e4 ~ e8 e' |
  e e e e e e e a, |
  d d d b a a r fis |
  e e e e e fis a gis |
  \break
  b b fis' fis b, b b' b |
  b, b fis' fis b, b b' b |
  \mark \markup{\box "Pre-Chorus 3"}
  cis, cis cis' a, a a a' a, |
  cis cis cis' a, a a a' a, |
  \break
  e' e gis gis a a ais ais |
  c! fis, dis fis b, ais b c! |
  cis cis r b a a r b |
  cis cis r b a a r b |
  \break
  b fis b fis b fis b fis |
  d' a d a d a d a
  \bar"||"
\mark \markup{\box "Chorus 3"}
  e e e fis a a a b|
  d d d b a a a b |
  \break
  e, e e fis a a a b|
  d d d b a a a fis |
  e e e fis a a a b|
  d d d b a a a b |
  \break
  e, e e fis a a a b|
  d1 ~ |
  d2. a8 (b) |
  e, e e fis a a a b|
  d d d b a a a b |
  \break
  e, e e fis a a a b |
  d d d b a a a fis |
  e e e fis a a a b |
  d d d b a a a fis |
  \break
  e e e fis a a a b |
  d d d d d d dis dis |
  e e cis b a a a b |
  d d d b a b d a |
  \break
  e' e cis b a a a b|
  d d d b a b d a |
  e' e cis b a a a b |
  d d d b a b d a |
  \break
  e e e b' a a a b |
  d d cis cis d d dis dis |
  e e e e a, a a a' |
  d, d d e a, b d b |
  \break
  e e cis b a a a b |
  d d d e a, a b d
  e e e e a, a a b |
  d d d b a b d a |
  \break
  e e e e a a a b |
  d d cis cis d d dis dis |
  e,1\fermata |
  e8\staccato-> r r4 r2 |
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  % \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
