\version "2.23.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

% copied transcription by constatine isslamow
\header {
  title = "Pictures of You"
  subtitle = "The Cure - Disintigration - 1989"
  poet = "Robert Smith"
  arranger = "bass: Simon Gallup"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key a \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

etob = {e8\3 e16\3 e16\3 a8\2 e16\3 a16\2 ~ a\2 e16\3 e16\3 e16\3 a16\2 (b16\2) b16\2 b16\2 }
ftoc = {fis8\3 fis16\3 fis16\3 b8\2 fis16\3 b16\2 ~ b\2 fis16\3 fis16\3 fis16\3 b16\2 (cis16\2) cis16\2 cis16\2 |}
etobLower = {e,8\3 e16\3 e16\3 a8\2 e16\3 a16\2 ~ a\2 e16\3 e16\3 e16\3 a16\2 (b16\2) b16\2 b16\2 }

preverseaE = {a,8 a16 a a8 a16 a ~ a a a a e'' e e e }
preversedF = {d,8 d16 d d8 d16 d ~ d d d d fis' fis fis fis}
preverseaELower = {a,,8 a16 a a8 a16 a ~ a a a a e'' e e e }

bridgeeE = {e,8\3 e16\3 e16\3 e8\3 e16\3 e16\3 ~ e\3 e\3 e8\3 e'16 e e e}
bridgeedE  = {d,8\3 d16\3 d16\3 d8\3 d16\3 d16\3 ~ d\3 d\3 d8\3 e'16 e e e}
solodD  = {d,8\3 d16\3 d16\3 d8\3 d16\3 d16\3 ~ d\3 d\3 d8\3 fis'16 fis fis fis}


electricBass = \relative c, {
  \global
  \set countPercentRepeats = ##t
  \omit Voice.StringNumber
  % Music follows here.
  %  \etob
  %   \ftoc

  \repeat percent 16 {
    \etob
    \ftoc
    %    \etobLower
    %     \ftoc
  }
  \break
  %    \repeat volta 2 {
  \mark \markup{Pre-Verse}
  \repeat percent 4 {
    \preverseaE
    \preversedF
  }
  \break
  \mark \markup{Verse}
  \repeat percent 4 {
    \preverseaELower
    \preversedF
  }
  %  }
  \break
  \mark \markup{Pre-Verse}
  \repeat percent 4 {
    \preverseaELower
    \preversedF
  }
  \break
  \mark \markup{Verse}
  \repeat percent 4 {
    \preverseaELower
    \preversedF
  }
  \break
  \mark \markup{Pre-Verse}
  \repeat percent 8 {
    \preverseaELower
    \preversedF
  }
  \break
  \mark \markup{Verse}
  \repeat percent 8 {
    \preverseaELower
    \preversedF
  }
  \break
  \mark \markup{Bridge}
  \repeat percent 4 {
    \bridgeeE
    \bridgeedE
  }
  \break
  \mark \markup{Solo}
  \repeat percent 9 {
    \preverseaELower
    \bridgeedE
  }
  \preverseaELower
  \solodD
  \preverseaELower
  d,8\3 d16\3 d16\3 d8\3 d16\3 d16\3 ~ d\3 d\3 d8\3 e'16 e e e
  \preverseaELower
  \solodD
  a,,8 a16 a16 a8 a16 a8 a16 a8 a16  a a a  | % 129
  b8 b16 b16 b8 b16 b8 b16 b8 b16  b b b  | % 130
  cis8 cis16 cis16 cis8 cis16 cis8 cis16 cis8 cis16  cis cis cis  | % 131
  d8\3 d16\3 d16\3 d8\3 d16\3 d8\3 d16\3 d8\3 d16\3  d\3 d\3 d\3  | % 132
  cis8\3 cis16\3 cis16\3 cis8\3 cis16\3 cis8\3 cis16\3 cis8\3 cis16\3  cis\3 cis\3 cis\3  | % 133
  d8\3 d16\3 d16\3 d8\3 d16\3 d8\3 d16\3 d8\3 d16\3  d\3 d\3 d\3  | % 134
  e8\3 e16\3 e16\3 e8\3 e16\3 e8\3 e16\3 e8\3 e16\3  e\3 e\3 e\3  | % 135
  fis8\3 fis16\3 fis16\3 fis8\3 fis16\3 fis8\3 fis16\3 fis8\3 fis16\3  fis\3 fis\3 fis\3  | % 136


  \break
  \mark \markup{Verse}
  a,8 a16 a16 a8 a16 a8 a16 a8 a16  a a a  | % 137
  b8 b16 b16 b8 b16 b8 b16 b8 b16  b b b  | %
  cis8 cis16 cis16 cis8 cis16 cis8 cis16 cis8 cis16  cis cis cis  | %
  d8\3 d16\3 d16\3 d8\3 d16\3 d8\3 d16\3 d8\3 d16\3  d\3 d\3 d\3  | %
  cis8\3 cis16\3 cis16\3 cis8\3 cis16\3 cis8\3 cis16\3 cis8\3 cis16\3  cis\3 cis\3 cis\3  | %
  d8\3 d16\3 d16\3 d8\3 d16\3 d8\3 d16\3 d8\3 d16\3  d\3 d\3 d\3  | %
  e8\3 e16\3 e16\3 e8\3 e16\3 e8\3 e16\3 e8\3 e16\3  e\3 e\3 e\3  | %
  fis8\3 fis16\3 fis16\3 fis8\3 fis16\3 fis8\3 fis16\3 fis8\3 fis16\3  fis\3 fis\3 fis\3  | %
  \break
  \mark \markup{Outro}
  a,8 a16 a16 a8 a16 a8 a16 a8 a16  a a a  | % 145
  b8 b16 b16 b8 b16 b8 b16 b8 b16  b b b  | %
  cis8 cis16 cis16 cis8 cis16 cis8 cis16 cis8 cis16  cis cis cis  | %
  d8\3 d16\3 d16\3 d8\3 d16\3 d8\3 d16\3 d8\3 d16\3  d\3 d\3 d\3  | %
  cis8\3 cis16\3 cis16\3 cis8\3 cis16\3 cis8\3 cis16\3 cis8\3 cis16\3  cis\3 cis\3 cis\3  | %
  d8\3 d16\3 d16\3 d8\3 d16\3 d8\3 d16\3 d8\3 d16\3  d\3 d\3 d\3  | %
  e8\3 e16\3 e16\3 e8\3 e16\3 e8\3 e16\3 e8\3 e16\3  e\3 e\3 e\3  | %
  fis8\3 fis16\3 fis16\3 fis8\3 fis16\3 fis8\3 fis16\3 fis8\3 fis16\3  fis\3 fis\3 fis\3  | %
  a1\2\fermata
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
