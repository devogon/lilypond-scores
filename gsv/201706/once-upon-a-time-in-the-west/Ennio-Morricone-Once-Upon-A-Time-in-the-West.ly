\version "2.19.57"
\language "nederlands"

\header {
  title = "Once Upon  A Time in the West"
  subtitle = "(Jill‘s Theme and Frank’s Theme)"
  instrument = "Cello 2"
  composer = "Ennio Morricone"
  arranger = "Marcel den Os"
  meter = "12/8"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key d \major
  \numericTimeSignature
  \time 12/8
  \tempo "Lentemente" 4=45
}

cello = \relative c {
  \global
  % Music follows here.
  d,1. (| % 1
  d1.) | % 2
  g1. | % 3
  a1. | % 4
  d,1. | % 5
  d1. | % 6
  g1. | % 7
  a1. ( | % 8
  a4.) (a8) d,4 (d2.) | % 9
  \time 4/4
  d'2 cis | % 10
  b2 a | % 11
  g1 | % 12
  d'1 | % 13
  b1 | % 14
  fis1 | % 15
  g1 | % 16
  d1 | % 17
  b'1\< | % 18
  b1 | % 19
  bes2 bes2\!\ff| % 20
  a2\mf g2 | % 21
  a2 a2\fermata | % 22
  fis'2\f (fis8 g fis e) | % 23
  d2. (d8 d) | % 24
  g,1 | % 25
  d'1 | % 26
  b1 | % 27
  fis1 | % 28
  g2 a | % 29
  d1 | % 30
  g,2 a | % 31
  d1 | % 32
  \key es \major
  bes1 | % 33
  es2 d | % 34
  c2 bes | % 35
  as1 | % 36
  es'1 | % 37
  c1 | % 38
  g1 | % 39
  as2 bes | % 40
  es1 | % 41
  as,2 bes | % 42
  es1 | % 43
  as,1 | % 44
  es'2.\fermata r4 | % 45
  \key c \major
  r1 | % 46
  r1\pp | % 47
  a1\pp ( | % 48
  a1) | % 49
  g1 ( | % 50
  % \break
  g) | % 51
  f1 ( | % 52
  f) | % 53
  e1 ( | % 54
  e) | % 55
  e1^\markup{keep your loving brother happy} ( | % 56
  e1) | % 57
  a,1:32\ff\> ( | % 58
  a1:16) | %  59
  g1:32\mf\! ( | % 60
  g1:16) | % 61
  f1:32 ( | % 62
  f1:16) | % 63
  e1:32 | % 64
  d'1:16 | % 65
  c1:32 ( | % 66
  c1:16) | % 67
  b1:32 ( | % 68
  b1:16) | % 69
  f'1:32 ( | % 70
  f1:16) | % 71
  e1:32 (| % 72
  e1:16) | % 73
  a,4\staccato\f a8\staccato a\staccato (a8) a\staccato a4\staccato | % 74
  % \break
  g4\staccato g8\staccato g\staccato (g8) g8\staccato g4\staccato | % 75
  f4\staccato f8\staccato f\staccato (f8) f8\staccato f4\staccato | % 76
  e4\staccato e8\staccato e\staccato (e8) e8\staccato g4\staccato | % 77
  c4\staccato c8\staccato c\staccato (c8) c\staccato c4\staccato | % 78
  b4\staccato b8\staccato b\staccato (b8) b\staccato b4\staccato | % 79

a4\staccato a8\staccato a\staccato (a8) a8\staccato a4\staccato | % 80
  g4\staccato g8\staccato g\staccato (g8) g8\staccato g4\staccato | % 81
  f4\staccato f8\staccato f\staccato (f8) f8\staccato f4\staccato | % 82
  d'4\staccato d8\staccato d\staccato (d8) d8\staccato d4\staccato | % 83'
  e4 e8 e e e e16 e e e | % 84
  a,4\staccato a8\staccato a\staccato (a8) a8\staccato a4\staccato | % 85
  g4\staccato g8\staccato g\staccato (g8) g8\staccato g4\staccato | % 86
  f4\staccato f8\staccato f\staccato (f8) f8\staccato f4\staccato | % 87
  e4\staccato e8\staccato e\staccato (e8) e8\staccato g4\staccato | % 88
  c4\staccato c8\staccato c\staccato (c8) c8\staccato c4\staccato | % 89
  b4\staccato b8\staccato b\staccato (b8) b8\staccato b4\staccato | % 90
  a4\staccato a8\staccato a\staccato (a8) a8\staccato a4\staccato | % 91
  g4\staccato g8\staccato g\staccato (g8) g8\staccato g4\staccato | % 92
  f4\staccato f8\staccato f\staccato (f8) f8\staccato f4\staccato | % 93
  d'4\staccato d8\staccato d\staccato (d8) d8\staccato d4\staccato | % 94
  e4 e8 e e e e16 e e e | %  95
  d4\staccato d8\staccato d\staccato (d8) d8\staccato d4\staccato | % 96
  e4 e8 e e e e16 e e e | % 97
  a,1\fermata | % 98
  \bar "|."
}

\score {
  \new Staff \with {
    instrumentName = "Cello"
    shortInstrumentName = "Cl."
  } { \clef bass \cello }
  \layout { }
}

\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
}
\markup{
  \teeny
  \date
}