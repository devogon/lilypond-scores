\version "2.23.6"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Life During Wartime"
  subtitle = "Talking Heads - Fear of Music - 1979"
  tagline = \date
  poet = "bass: Tina Weymouth"
}

\paper {
  indent = 0.0\cm
}

global = {
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  
}

electricBass = \relative c, {
  \global
  \mark \markup{verse}
    a4 g8 a c a r a |
    c a c a c a r g |
    a r g a c a g e |
    a4 e8 a4 e8 g e |
    \break
    a4 g8 a c a r a |
    c a c a c a r g |
    a r g a c a g e |
    a4 d8 r c r b r |
    \break
    a4 g8 a c a r a |
    c a c a c a r g |
    a r g a c a g e |
    a4 e8 a4 e8 g e |
    \break
    a4 g8 a c a r a |
    c a c a c a r g |
    a r g a c a g e |
    a4 d c2 |
    \mark \markup{chorus}
    e4 b e8 b4 e8 ~ |
    \break
    e b e,4 a8 b d4 |
    e4 b e8 b4 e8 ~ |
    e b e,4 e'8 d a4 |
    e' b e8 b4 e8 ~ |
    e b e,4 a8 b d4 |
    \break
    e b e8 b4 e8 ~ |
    e b e,4 e'8 d b4 |
    \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff \with {
    midiInstrument = "electric bass (finger)"
  } { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
  \midi {
    \tempo 4=136
  }
}
