\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Psycho Killer"
  subtitle = "Talking Heads"
  instrument = "Bass"
  composer = "David Byrne"
  arranger = "Tina Weymouth"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=121
}

electricBass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 6 {a4\mf\staccato^\markup{Intro} a\staccato a\staccato a\staccato a\staccato a\staccato a8 e g4^\markup{6x} }
  \break
  \repeat volta 6 {a4\mf\staccato^\markup{Verse} a\staccato a\staccato a\staccato a\staccato a\staccato a8 e g4^\markup{6x} }
  \break
  \repeat volta 2 {f'4^\markup{Chorus} d8 c f4 d8 c
                   g'4 e8 d g4 e8 d
                   \break
                   a4\staccato a8 g a4\staccato a8 g
                   a4\staccato a8 g a4\staccato a
                   \break
                   f'4 d8 c f4 d8 c
                   g'4 e8 d g4 e8 d
                   \break
                   c4\staccato c8 b c4\staccato c8 b
                   c4\staccato c8 b c4\staccato c
    }
    \break
   f4 d8 c f4 d8 c
   g'4 e8 d g4 e8 d
   \break
   \repeat volta 2 {
  a4\staccato^\markup{Verse 2} a4\staccato a\staccato a\staccato
  a\staccato a\staccato a8 e g4^\markup{4x}
   }
   \break
     \repeat volta 2 {f'4^\markup{Chorus 2} d8 c f4 d8 c
                   g'4 e8 d g4 e8 d
                   \break
                   a4\staccato a8 g a4\staccato a8 g
                   a4\staccato a8 g a4\staccato a
                   \break
                   f'4 d8 c f4 d8 c
                   g'4 e8 d g4 e8 d
                   \break
                   c4\staccato c8 b c4\staccato c8 b
                   c4\staccato c8 b c4\staccato c
    }
    \break
    f4 d8 c f4 d8 c
    g'4 e8 d g4 e8 d
    \break
    \repeat volta 2 {
      fis4\staccato^\markup{Bridge} e\staccato d\staccato cis\staccato
      b\staccato a\staccato b\staccato cis\staccato
      \break
      g'4\staccato fis\staccato e\staccato d\staccato
      g\staccato fis\staccato e\staccato d\staccato
    }
    \break
    r4 fis8 e d a ~ a4 |
    fis'8 e d a ~ a2 |
    \break
    r4 b'8 a g d ~ d4 |
    b'8 a g d ~ d2 |
    \break
    \repeat volta 4 {
    a4\staccato^\markup{Shout} \repeat unfold 5 {a4\staccato} a8 e g4^\markup{4x}
    }
    \break

}

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
