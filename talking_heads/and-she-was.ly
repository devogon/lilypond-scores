\version "2.23.2"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

% https://tabs.ultimate-guitar.com/tab/talking-heads/and-she-was-bass-614732

\header {
  title = "And She Was"
  subtitle = "Talking Heads - 18]985"
  instrument = "Bass"
  composer = "bass: Tina Weymouth"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key e \major
  \time 4/4
  \tempo 4=126
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  \mark \markup {\tiny "6x"} \repeat volta 6 {
    e,8 e gis b a a cis e |
    e,4\staccato r r2
  % e,4\staccato r8 e a4 cis8 a |
  % e4\staccato r r2
}
\key f \major
bes'4 r8 bes f'4 r8 f |
c4 r8 c f4. r8 |
bes,4 r8 bes c4 r8 c |
f2 f,4. r8 |
\break
bes4 r8 bes f'4 r8 f |
c4 r8 c f4. r8 |
bes,4 r8 bes g4 r8 g |
c4 r8 c d4 c |
\break
\key e \major
\mark \markup {\tiny "4x"} \repeat volta 4 {
  e,4\staccato r8 e a4\staccato r8 a |
   d!4\staccato r8 d a a cis a |
}
  \mark \markup {\tiny "6x"} \repeat volta 6 {
        e8 e gis b a a cis e |
    e,4\staccato r r2
  % e4\staccato r8 e a4 cis8 a |
  % e4\staccato r4 r2 |
}

\key f \major
bes'4 r8 bes f'4 r8 f |
c4 r8 c f4. r8 |
bes,4 r8 bes c4 r8 c |
f2 f,4. r8 |
\break
bes4 r8 bes f'4 r8 f |
c4 r8 c f4. r8 |
bes,4 r8 bes g4 r8 g |
c4 r8 c d4 c |
\break
\key e \major
\mark \markup {\tiny "4x"} \repeat volta 4 {
  e,4\staccato r8 e a4\staccato r8 a |
  d!4\staccato r8 d a a cis a |
}
\mark \markup {\tiny "2x"} \repeat volta 2 {
  b4. a8 b4. a8 |
  b8 fis' e d! b4 a |
  g!4. fis8 g4. fis8 |
  g!4. fis8 g4 a |
}
  \mark \markup {\tiny "6x"} \repeat volta 6 {
        e8 e gis b a a cis e |
    e,4\staccato r r2
  % e4\staccato r8 e a4 cis8 a |
  % e4\staccato r4 r2 |
}

\break
\key f \major
bes'4 r8 bes f'4 r8 f |
c4 r8 c f4. r8 |
bes,4 r8 bes c4 r8 c |
f2 f,4. r8 |
\break
bes4 r8 bes f'4 r8 f |
c4 r8 c f4. r8 |
bes,4 r8 bes g4 r8 g |
c4 r8 c d4 c |
\break
\key e \major
\mark \markup {\tiny "12x"} \repeat volta 12 {
  e,4\staccato r8 e a4\staccato r8 a |
  d!4\staccato r8 d a a cis a |
}

e4\staccato r8 e a4\staccato r8 a |
d!4\staccato
r8 d a cis a e
\bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup \with {
  \consists "Instrument_name_engraver"
 % instrumentName = "Electric bass"
} <<
  \new Staff { \clef "bass_8" \electricBass }
%   \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
