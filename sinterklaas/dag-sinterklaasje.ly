\version "2.19.80"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Dag Sinterklaasje!"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
%  \tempo 4=100
}

scoreAFluteI = \relative c'' {
  \global
  % Music follows here.
  c4 a8. g16 f4 c' |
  d4 (bes) c4 (a) |
  bes4 (g) a (f) |
  g4 a g2 |
  c4 a8. g16 f4 c' |
  d4 (bes) c4 (a) |
  bes4 g a f |
  g4 a f2 |
  \bar "|."
}

scoreAFluteII = \relative c'' {
  \global
  % Music follows here.
  a4 f f a |
  bes2 a |
  g2 f |
  c'4 b! c2 |
  a4 f f a |
  bes2 a |
  g2 f |
  c'4 c a2 |
}


scoreAVerse = \lyricmode {
  % Lyrics follow here.
  Dag Sint -er -klaas -je, daag, daag,
  daag, daag, Zwar -te Piet!
  Dag Sint -er -klaas -je, daag, daag,
  luis -ter naar ons af -scheids -lied!
}

scoreAFluteIPart = \new Staff \with {
 % instrumentName = "Flute I"
 % shortInstrumentName = "Fl. I"
} {\scoreAFluteI}
\addlyrics {\scoreAVerse}

scoreAFluteIIPart = \new Staff \with {
 % instrumentName = "Flute II"
 % shortInstrumentName = "Fl. II"
} \scoreAFluteII



\score {
  <<
    \scoreAFluteIPart
    \scoreAFluteIIPart

  >>
  \layout { }
}
