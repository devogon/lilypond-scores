\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "zachtjes gaan de paardenvoetjes"
%  composer = "composer"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


%\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 3/4
 % \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = {
   \global
  \partial 4 g8\downbow g
  c4 c c8 d
  c'8 (b) b2
  d'8 g d' g d' e'
  \break
  c'2\downbow g8\downbow g
  c'4 c' c'8 d'
  c'8 (b) b2
  d'8 g d' g d' e'
  \break
  c'2\downbow c'8\downbow b
  a4 b c'8 a
  g2 g8\upbow g
  f4 g a8 f
  \break
  e2 g8\downbow g
  a4 b c'8 d'
  e'2 c'8 e'
  d'4 a4 b8 (d')
  c'2
  \bar "|."
}

contrabastwo = \relative c {
  \global
  \partial 4 r4
  r r c8\downbow c
  f4 f g8 a
  g4 f8 (e) f g
  e8 (f) g (f) e d
  c4\downbow r c8\downbow c
  f4 f g8 a
  g4 f8 (e) f g
  e4. (d8) c4
  f4 g a8 f
  f4 (e) e
  d4 c b
  c8 (b) c d e4
  f4 g a
  gis4 e a8 g!
  fis8 e d4 <g g,>8 (f)
  <e g, c,>2
  \bar "|."
}

\score {
  \new StaffGroup
  <<
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \new Staff {\clef bass \contrabastwo}
  >>
  \layout { }

}
