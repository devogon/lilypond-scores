\version "2.19.80"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Zwarte Piet, wiedewiedewiet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 3/4
%  \tempo 4=100
}

scoreAFluteI = \relative c' {
  \global
  % Music follows here.
  \partial 4 {
  d'4
  }
  g4 g b
  a4 a c
  fis,4 fis a
  g2 d4
  g4 g b
  a8 a a4 c
  fis,4 fis a
  g2 d'4
  a4 a d
  b4 b d
  c4 c b
  a2 d,4
  g4 g8 (a) b (c) |
  d4 b g
  c4 a fis
  g2
  \bar "|."
}

scoreAFluteII = \relative c'' {
  \global
  % Music follows here.
  \partial 4 {
    r4
  }
  b4 b r
  e4 e r
  c4 c r
  d4 b r
  b4 b r
  e4 e r
  c4 c r
  d4 b r
  fis'4 fis r
  g4 g r
  d4 d d
  fis2 r4
  b,2 b4
  d2 d4
  e4 c a
  b2
}

scoreAFluteIII = \relative c' {
  \global
  % Music follows here.
  \partial 4 {
    r4
  }
  g'4 g r
  c4 c r
  d4 d r
  b4 g r
  g4 g r
  c4 c r
  d4 d r
  g,4 g r
  d'4 d r
  d4 d r
  fis,4 fis g8 b
  d4 d r
  g,2 g4
  b2 b4
  c4 d d
  g,2
}

scoreATenorVoice = \relative c {
\global
  \partial 4 {
  r4
  }

}

scoreAVerse = \lyricmode {
  % Lyrics follow here.
 Zie ginds komt de stoom -boot uit Span -je weer aan.
 Hij brengt ons Sint Nik -o -laas, ik zie ham al staan.
 Hoe hup -pelt zijn paard -je het dek op en neer.
 hoe waai -en de wimp -els al heen en al weer.
}

scoreAFluteIPart = \new Staff \with {
 % instrumentName = "Flute I"
 % shortInstrumentName = "Fl. I"
} {\scoreAFluteI}
\addlyrics {\scoreAVerse}

scoreAFluteIIPart = \new Staff \with {
 % instrumentName = "Flute II"
 % shortInstrumentName = "Fl. II"
} \scoreAFluteII

scoreAFluteIIIPart = \new Staff \with {
 % instrumentName = "Flute III"
 % shortInstrumentName = "Fl. III"
} \scoreAFluteIII

scoreATenorVoicePart = \new Staff \with {
  %instrumentName = "Tenor"
  %shortInstrumentName = "T."
} { \clef "treble_8" \scoreATenorVoice }
\addlyrics { \scoreAVerse }

\score {
  <<
    \scoreAFluteIPart
    \scoreAFluteIIPart
    \scoreAFluteIIIPart
%    \scoreATenorVoicePart
%\thelyrics
  >>
  \layout { }
}
