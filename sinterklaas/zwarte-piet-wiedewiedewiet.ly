\version "2.19.80"
\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Zwarte Piet, wiedewiedewiet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
%  \tempo 4=100
}

scoreAFluteI = \relative c' {
  \global
  % Music follows here.
  \partial 2 {
  f4 a
  }
  c2 d8 d d d |
  c2 bes4 bes |
  a4 a g g |
  f4 r f a
  c4 c d d |
  c2 bes4 bes
  a4 a g c
  f,2
}

scoreAFluteII = \relative c' {
  \global
  % Music follows here.
  \partial 2 {
    f4 a
  }
  a4 a bes8 bes bes bes |
  a4 a g bes |
  a4 f g g |
  f4 r f a |
  a4 a bes bes |
  a4 a g bes |
  a4 f g g |
  f2
}

scoreAFluteIII = \relative c' {
  \global
  % Music follows here.
  \partial 2 {
    f4 a
  }
  f4 f f f |
  f 4 f g g |
  f4 f f e |
  f4 r f a |
  f4 f f f |
  f4 f g g |
  f4 f f e |
  f2
  \bar "|."
}

scoreATenorVoice = \relative c {
\global
  \partial 2 {
  f4 a
  }
  c2 d8 d d d |
  c2 bes4 bes |
  a4 a g g |
  f4 r f a
  c4 c d d |
  c2 bes4 bes
  a4 a g c
  f,2
}
thelyrics = \lyrics {
  Zwarte Piet, wiedewiede wiet.
  Ik Hoor je wel maar Ik zie je niet!
  Wil je Sint de groeten doen?
  Gooi wat iin mijn lege schoen!
}

scoreAVerse = \lyricmode {
  % Lyrics follow here.
  Zwar -te Piet, wie de wie de wiet.
  'k-Hoor je wel maar 'k-zie je niet!
  Wil je Sint de groet -en doen?
  Gooi wat iin mijn le -ge schoen!
}

scoreAFluteIPart = \new Staff \with {
 % instrumentName = "Flute I"
 % shortInstrumentName = "Fl. I"
} {\scoreAFluteI}
\addlyrics {\scoreAVerse}

scoreAFluteIIPart = \new Staff \with {
 % instrumentName = "Flute II"
 % shortInstrumentName = "Fl. II"
} \scoreAFluteII

scoreAFluteIIIPart = \new Staff \with {
 % instrumentName = "Flute III"
 % shortInstrumentName = "Fl. III"
} \scoreAFluteIII

scoreATenorVoicePart = \new Staff \with {
  %instrumentName = "Tenor"
  %shortInstrumentName = "T."
} { \clef "treble_8" \scoreATenorVoice }
\addlyrics { \scoreAVerse }

\score {
  <<
    \scoreAFluteIPart
    \scoreAFluteIIPart
    \scoreAFluteIIIPart
%    \scoreATenorVoicePart
%\thelyrics
  >>
  \layout { }
}
