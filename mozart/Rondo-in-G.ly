\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Rondo in  G"
  composer = "Wolfgang Amadeus Mozart"
  piece = "erm"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
    indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 2/2
  \partial 2
  \tempo 4=100
}

vivace = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  g'4\downbow\p g |
  g2 c,4 c | %
  c2 a4 a | %
  a2 d4 d | %
  d2 g4 g | %
  g2 r | %
  \break
  r1 | % 6
  r2 d\upbow | %
  g,4 r
  }
  \repeat volta 2 {
    r2
    a'2 a, | %
    d4 r r2  | %
    d2 g,2 |
    \break
    a2 r | % 12
    a'2\mf a, |
    d2 r |
    d'2 a |
    d,2 r |
    gis4\downbow\p r e\upbow r |
    \break
    a4 r g r | % 18
    f4 r d r |
    g4 r r2 |
    R1*2 |
    f2\f g |
    d4 r g\downbow\p g |
    \break
    g2 c,4 c | % 25
    c2 a4 a |
    a2 d4 d |
    d2 g4 g |
    g2 r |
    r1 |
    \break
    r2 d | % 31
    g,4^\markup{to coda O - FIXME} r
  }
  \repeat volta 2 {
   r2
   r2 d'\upbow\p |
   g2 r
   r2 f!2\upbow
   bes,2 r |
   \break
   r2 a'\upbow | % 37
   d,2 d
   g, a
   d2^\markup{\tiny wr org dn 8}
  }
  d'4\upbow\f d8 d
  f!4 (b,) b b8 b |
  \break
  d4 (c) c c8 c |
  es4 (a,) a a8 a |
  c4 (bes) es,2\p |
  c2 d
  g,2 g'
  es2 f! |
  \break
  bes2 r  | % 48
  r1
  r2 fis4\upbow\f fis 8 fis |
  g2 es
  d2 d
  g,1
  d'2^\markup{dc al coda - FIXME}
  \bar "||"
  g4\f^\markup{al coda - FIXME} g | %
  g2 c,4 c | % 55
  c2 a4 a
  a2 d4 d
  d2 g4 g
  g1
  r1
  r2 c8 b c a |
  \break
  b8 a b fis g fis g a | % 62
  b4 r c8 b c a |
  b a b fis g fis g a |
  b4 r r2
  d,1\p (
  g,4) r r2
  \bar "|."

}

andante = \relative c {
  \global
  % Music follows here.

}

\score {
  \new Staff { \clef bass \vivace }
  \layout { }
  \header { piece = \markup{\bold vivace}}
}

\score {
  \new Staff { \clef bass \andante }
  \layout { }
  \header { piece = \markup{\bold andante}}
}
