\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}


\header {
  title = "Ave verum Corpus"
  subtitle = "K 618"
  composer = "W. A. Mozart"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
    indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 2/2
%  \partial 2
  \tempo "Adagio" 4=63

}

contrabas = \relative c {
  \global
  r1
  r4 fis_\markup{\italic "sotto voce"}  (a) a|
  d,2 d |
  d2 d |
  cis2 cis |
  d2 d |
  a2. a4\< |
  d4 d cis\! cis |
  \break
  d4 d\< fis d |
  a4 a' e\! cis\breathe |
  a2\downbow a'\< |
  a4 (b) b2\! |
  gis2\> e2 |
  eis4\! (fis) fis2 |
  r2 b, |
  cis2 d |
  \break
  e1 |
  a,2.\breathe cis4\downbow ( |
  fis2) e4 dis |
  e2 e, |
  a4 a' e cis |
  a2 a' |
  a4 (g) g2 |
  c,!2 c |
  c!4 (f!) f2\breathe |
  \break
  f!2 f |
  e4 d cis a |
  bes4 bes a gis |
  a4 a'2\breathe g8\upbow (e) |
  d1\downbow\p g2.\downbow g4\upbow |
  g4\downbow (fis) e\upbow (a) |
  a2.\downbow a4\upbow |
  \break
  a4\upbow (g) fis\upbow (b) |
  b2\downbow a4\upbow gis\downbow |
  a2\upbow a,2\downbow |
  b2\upbow r |
  r2 a' |
  bes2 a |
  \break
  gis1 |
  g2 fis4\upbow b, |
  a1 |
  d2 r4 fis, ( |
  b2.) g4 |
  a1 |
  d1
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabas }
  \layout { }
  \header { }
}