\version "2.19.83"
\language "nederlands"

date = \markup{\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Beat It"
  instrument = "Bass"
  composer = "Michael Jackson"
  copyright = "copy"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key ges \major
  \time 4/4
  \tempo 4=100
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

electricBass = \relative c, {
  \global
				% Music follows here.
  \mark \markup{\box \tiny synth}
  \bar "||"
  R1*6
  \mark \markup{\box \tiny drums}
    \bar "||"
  \break
  R1*7
  r2 r4 r8 es, ~ |
  \break
    \mark \markup{\box \tiny "guitar and bass"}
 \repeat volta 2 {
    es8 ges bes ges' es4. f8 ~ | % 1
    f8 es des4\staccato des\staccato r8 es, |
    es8 ges bes ges' es4. f8 ~ | % 3
  }
  \alternative {
    {f8 es des4\staccato r r8 es, }
%    es8 ges bes ges' es4. f8 ~ | % 1
%    f8 es des4\staccato des\staccato r8 es, |
%    es8 ges bes ges' es4. f8 ~ | % 3
    {f'8 es des4\staccato r2}
  }
  \break
  \bar "||"
    \mark \markup{\box \tiny stanza}
    \repeat volta 2 {
      r8 es es4\staccato es4\staccato r8 des8  |
      es4\staccato es\staccato r2 |
      r8 es8 es4\staccato es\staccato r8 des8 |
      des4\staccato des\staccato r2 |
      \break
      r8 ces8 ces4\staccato ces\staccato r8 des8 |
      des4\staccato des\staccato r2 |
      r8 es8 es4\staccato es\staccato r8 des8 |
    }
  \alternative {
    { des4\staccato des\staccato r2 }
    {des4\staccato des\staccato r4 r8 es,8}
  }
  \break
    \bar "||"
  \mark \markup{\box \tiny refrain}
  \repeat volta 3 {
      es8 ges bes ges' es4. f8 ~ | 
    f8 es des4\staccato des\staccato r8 es, |
    es8 ges bes ges' es4. f8 ~ |
  }
  \alternative {
    {f8 es des4\staccato r r8 es, }
    {f'8 es des4\staccato r2}
  }
  \break
    \bar "||"
    \mark \markup{\box \tiny stanza}
    \repeat volta 2 {
      r8 es es4\staccato es4\staccato r8 des8  |
      es4\staccato es\staccato r2 |
      r8 es8 es4\staccato es\staccato r8 des8 |
      des4\staccato des\staccato r2 |
      \break
      r8 ces8 ces4\staccato ces\staccato r8 des8 |
      des4\staccato des\staccato r2 |
      r8 es8 es4\staccato es\staccato r8 des8 |
    }
  \alternative {
    { des4\staccato des\staccato r2 }
    {des4\staccato des\staccato r4 r8 es,8}
  }
  \break
    \bar "||"
    \mark \markup{\box \tiny refrain}
     \repeat volta 4 {
      es8 ges bes ges' es4. f8 ~ | 
    f8 es des4\staccato des\staccato r8 es, |
    es8 ges bes ges' es4. f8 ~ |
  }
  \alternative {
    {f8 es des4\staccato r r8 es, }
    {f'8 es des4\staccato r2}
  }
  \break
    \bar "||"
  \mark \markup{\box \tiny "Pre solo"}
  \repeat volta 4 {
    r8 es8 es4\staccato es\staccato r8 es |
    es4\staccato es\staccato r2 |
    r8 es es4\staccato es\staccato r8 es8 |
    es4\staccato es\staccato r2
  }
  \break
    \bar "||"
  \mark \markup{\box \tiny "guitar solo"}
  \repeat volta 2 {
    r8 es es4\staccato es\staccato r8 des |
    des4\staccato des\staccato r2 |
    r8 es es4\staccato es\staccato r8 des8 |
    des4\staccato des\staccato r2 |
    r8ces8 ces4\staccato ces\staccato r8 des |
    des4\staccato des4\staccato r2 |
    r8 es es4\staccato es\staccato r8 des|
  }
  \alternative {
    { des4\staccato des\staccato r2}
    {des4\staccato des\staccato r4 r8 es,8}
  }
  \break
    \bar "||"
  \mark \markup {\box \tiny "refrain"}
  \repeat volta 2 {
    es8 ges bes ges' es4. f8 ~|
    f8 es des4\staccato des\staccato r8 es,8 ~ |
    es ges bes ges' es4. f8 ~ |
    f8 es des4\staccato r4 r8 es,^\markup{repeat and fade}
  }
}


chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

chordsPart = \new ChordNames \chordNames

\score {
  <<
    \electricBassPart
    \chordsPart
  >>
  \layout { }
}
