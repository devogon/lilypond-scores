\version "2.19.81"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "New Year‘s Day"
  instrument = "Bass"
  composer = "U2"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=144
}

electricBass = \relative c {
  \global
  % Music follows here.
  \mark \markup {"3x"} \repeat volta 3 {
    a8 a c a4 a8 a a, | % 1
    c8 c e c4 c8 c e | % 2
    e8 e g e4 e8 e a, | % 3
    e'8 e g e4 e8 e a | % 4
  }
  \break
    \set Score.currentBarNumber = #13
  a8 a c a4 a8 a a, | % 5
  c8 c e c4 c8 c e | % 6
  \repeat unfold 16 {g}
  \break
  \repeat unfold 15 {f} g8
  \repeat unfold 16 {a}
  \repeat unfold 15 {g8} f8
  \repeat unfold 15 {f8} a8
%  \repeat unfold 16 {e8}
\break
  \mark \markup {"4x"} \repeat volta 4 {
    a8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a, | %
    e'8 e g e4 e8 e a | %
  }
  \break
    a8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a, | %
    e'8 e g e4 e8 r a | %
  \break
   a8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a, | %
    e'8 e g e4 e8 e a | %
  \break
    \set Score.currentBarNumber = #49
  \repeat unfold 16 {g8}
  a2. a8 a
  \repeat unfold 8 {a8}
  \break
  \repeat unfold 16 {g8}
  \repeat unfold 15 {f8} a
  \break
    %\mark \markup {"5x"}
    \repeat volta 5 {
    a8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a, | %

  }
  \alternative {
    { e'8 e g e4 e8 e a}
      {e8 e g e4 e8 e g16 g}
  }
%{   \break
  a8 a c a4 a8 a a, | %
  c8 c e c4 c8 c e | %
  e8 e g e4 e8 e a, | %
 %}   | %
  \break
      \set Score.currentBarNumber = #77
  \repeat unfold 16 {g8}
  \repeat unfold 16 {a8}
  \break
  \repeat unfold 16 {g8}
  \repeat unfold 16 {f8}
  \break
  \repeat unfold 16 {g8}
  \repeat unfold 6 {a8} a4
  \repeat unfold 8 {a8}
  \break
  \repeat unfold 15 {g8} e,
  f4. f'8 f,8 f f f |
  f8 f' f, f f f f e |
  \break
   %     \set Score.currentBarNumber = #77
\mark \markup {"4x"} \repeat volta 4 {
    a'8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a, | %
    e'8 e g e4 e8 e a | %
  }
  \break
  \set Score.currentBarNumber = #109
  a8 a c a4 a8 a a, | %
  c8 c e c4 c8 c e |
  \repeat unfold 16 {g8}
  \break
  \repeat unfold 15 {f8} a
  \repeat unfold 15 {a8} c,
  \break
  \repeat unfold 15 {g'8} e,
  \repeat unfold 15 {f8} e
  \break
  \mark \markup {"2x"} \repeat volta 2 {
    a'8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a, | %
    e'8 e g e4 e8 e a | %
  }
  \break
      a8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a,16 a | %
    e'8 e g e4 e8 e g | %
    \break
          a8 a c a4 a8 a a, | %
    c8 c e c4 c8 c e | %
    e8 e g e4 e8 e a,8 | %
    e'8 e g e4 e8 e a | %
    \break
    \repeat unfold 6 {g8} g4
    \repeat unfold 8 {g8}
    \repeat unfold 16 {a8}
    \break
    \repeat unfold 16 {g8}
    \repeat unfold 16 {f8}
    \break
    \repeat unfold 16 {g8}
    \repeat unfold 16 {a8}
    \break
    \repeat unfold 15 {g8} e,
    \repeat unfold 12 {f8} f4 f8 a
    %\break
    a1
    \bar "|."
}

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
