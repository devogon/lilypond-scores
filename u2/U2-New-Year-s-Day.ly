\version "2.19.81"
\language "nederlands"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "New Year‘s Day"
  instrument = "Bass"
  composer = "U2"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  inner-margin = 1.5\cm
  outer-margin = 2\cm
  % left-margin = 2\cm
				% right-margin = 2\cm
  two-sided = ##t
}

\layout {
  \omit Voice.StringNumber
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=144
}

chordNames = {
  \global
}

electricBass = \relative c {
  \global
  % Music follows here.
  \mark \markup {"3x"} \repeat volta 3 {
    as8\2 as\2 b\1 as4.\2 as8\2 as\2  | % 1
    b,8 b es b4. b8 b  | % 2
    es8\3 es\3 ges\2 es4\3 es8\3 es\3 as\4  | % 3
    es8\3 es\3 ges\2 es4\3 es8\3 es\3 b\4 | % 4
  }
\break
    \set Score.currentBarNumber = #13
  % a8 a c a4 a8 a a, | % 5
  % c8 c e c4 c8 c e | % 6
  \repeat unfold 16 {ges'\2}
%  % \break
  \repeat unfold 16 {e\2}
  \repeat unfold 16 {as\2}
  \repeat unfold 16 {ges8\2}
  \repeat unfold 16 {e8\2}
%  \repeat unfold 16 {e8}
\break
    \mark \markup {"6x"} \repeat volta 6 {
    as8\2 as\2 b\1 as4.\2 as8\2 as\2  | % 1
    b,8 b es b4. b8 b  | % 2
    es8\3 es\3 ges\2 es4\3 es8\3 es\3 as,\4  | % 3
    es'8\3 es\3 ges\2 es4\3 es8\3 es\3 b\4 | % 4
  }
\break

    \repeat unfold 16 {ges'8\2}
    \repeat unfold 16 {as\2}
    \repeat unfold 16 {ges8\2}
    \repeat unfold 16 {e8\2}
\break
    \mark \markup {"5x"} \repeat volta 5 {
    as8\2 as\2 b\1 as4.\2 as8\2 as\2  | % 1
    b,8 b es b4. b8 b  | % 2
    es8\3 es\3 ges\2 es4\3 es8\3 es\3 as,\4  | % 3
    es'8\3 es\3 ges\2 es4\3 es8\3 es\3 b\4 | % 4
  }
  \break
      \repeat unfold 16 {ges'8\2}
    \repeat unfold 16 {as\2}
    \repeat unfold 16 {ges8\2}
    \repeat unfold 16 {e8\2}
    % \break
    \repeat unfold 16 {ges8\2}
    \repeat unfold 16 {as\2}
    \repeat unfold 16 {ges8\2}
    \repeat unfold 16 {e8\2}
  % \break
\break
    \mark \markup {"5x"} \repeat volta 5 {
    as8\2 as\2 b\1 as4.\2 as8\2 as\2  | % 1
    b,8 b es b4. b8 b  | % 2
    es8\3 es\3 ges\2 es4\3 es8\3 es\3 as,\4  | % 3
    es'8\3 es\3 ges\2 es4\3 es8\3 es\3 b\4 | % 4
  }
\break
      \repeat unfold 16 {ges'\2}
  \repeat unfold 16 {e8\2}
  % \break
      \repeat unfold 16 {as\2}
    \repeat unfold 16 {ges8\2}
    \repeat unfold 16 {e8\2}
\break
  \mark \markup {"repeat until fade"} \repeat volta 5 {
    as8\2 as\2 b\1 as4.\2 as8\2 as\2  | % 1
    b,8 b es b4. b8 b  | % 2
    es8\3 es\3 ges\2 es4\3 es8\3 es\3 as,\4  | % 3
    es'8\3 es\3 ges\2 es4\3 es8\3 es\3 b\4 | % 4
  }
  % \break
    \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
%    \chordsPart
    \electricBassPart
    >>
  \layout { }
}
