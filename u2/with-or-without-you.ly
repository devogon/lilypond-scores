\version "2.19.83"
\language "nederlands"
date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "With or Without You"
  subtitle = "U2"
  subsubtitle = "The Joshua Tree - 1987"
  instrument = "Bass"
  composer = "U2"
  arranger = "producer: Daniel Lanois/Brian Eno"
  poet = "(D A Bm G)"
  copyright = ""
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
%   ragged-last = ##t
}

\layout {
  \omit Voice.StringNumber
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 4/4
  \tempo 4=110
}


chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*4
  \break
  \repeat unfold 8 {d8\4}
  \repeat unfold 8 {a\4}
  \repeat unfold 8 {b\4}
  \repeat unfold 8 {g\4}
  \break
    \mark \markup{\small \bold "23x"}
  \repeat volta 23 {
     b32 (d16.) \repeat unfold 7 {d8\4}
      \repeat unfold 8 {a\4}
      \repeat unfold 8 {b\4}
	\repeat unfold 8 {g\4}
    }
      \mark \markup{\small \bold "3x"}
      \repeat volta 12 {
	\repeat unfold 8 {d'\4}
	\repeat unfold 8 {d\4}
	\repeat unfold 8 {d\4}
	\repeat unfold 8 {d\4}
      }
  \repeat unfold 8 {d\4}
  \repeat unfold 8 {a\4}
  \repeat unfold 8 {b\4}
  \repeat unfold 8 {g\4}
  \mark \markup{\small \bold "4x"}
  \repeat volta 4 {
    b32 (d16.) \repeat unfold 7 {d8\4}
    \repeat unfold 8 {a\4}
    \repeat unfold 8 {b\4}
    \repeat unfold 8 {g\4}
  }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}

% about: https://en.wikipedia.org/wiki/With_or_Without_You