\version "2.25.12"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Jump"
  subtitle = "Van Halen - 1984"
  composer = "bass: Michael Anthony"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  g2 c2
  f2 g2
  c2 f1
   g2:sus4

  g2/c c2
  f2 g
  c2 f1
   g2:sus4

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4
  g2/c c
  f2/c g/c
  c2 f1/c
  s2

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

  a1:m7
  s1
  f2 c4 d:m
  s1

  f2 c4 d:m
  s1
  f2 c4 g
  s1
  s1

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

  a1:m7
  s1
  f2 c4 d:m
  s1

  f2 c4 d:m
  s1
  f2 c4 g
  s1
  s1

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

  g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

  bes4.:m ges2 as2
  des2 s8
    bes2:m ges2 as4.
  des2 s8

  bes4.:m ges2 as2
  des2 s8
    bes4.:m ges2 as2
  des2 s8

  c1:5
  s1
  s1
  s1

  f1:5
  s1
  g1:5
  s1

  bes1:5
  s1
  a1:sus4
  s1

  as1:sus2
  s1
  g1:5
  s1

  c1:5
  s1

  g2 c
  f g
  c2 f
  s2 g2:sus4

  g2/c c
  f2 g
  c2 f
  s2 g2:sus4

    g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

    g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

    g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

    g2/c c
  f2/c g/c
  c2 f1/c
  g2:sus4

    g2/c c
  f2/c g/c
  c2 f1/c


}

electricBass = \relative c, {
  \global
  % Music follows here.
  r1 r1 r1
  r2 r8 g' c g |
  \break
  c,4 r2 r4
  R1*2
  r8
  f,4 g8 ~ g8 g' e g |
  \break
  \repeat volta 2 {
    c,8 c c c c c c c
    c c c c c c c c
    c c c c c c c c
    c f,4 g8 ~ g g g g |
  }
    \mark \markup{\box Verse}
  c8 c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 ~ g g g g
  \break
  c8 c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 ~ g g g g
  \break
  c8 c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 ~ g g g g
  \break
  \mark \markup{\box Pre-Chorus}
  a4\staccato r8 g r4 a\staccato |
  r8 g r a r g r4 |
  f4 f8 f c' c c d ~ |
  d4. d8 d e, d' g, |
  \break
  f4 f8 f c'8 c c d ~ |
  d8 d d d d e, d' g, |
  f4 f c'8 e, f g ~ |
  g8 g g g g d' b g |
  g8 g d'16 (e) g8 ~ g e d bes
  \bar "||"
  \break
  \mark \markup{\box Chorus}
  \repeat volta 2 {
     c8 c c c c c c c
    c c c c c c c c
    c c c c c c c c
    c f,4 g8 ~ g g g g |
  }
    \break
    c8 g c c c c c c
    c c c c c c c c
    c c c c c c c c
    c f,4 g8 ~ g g g g |
    \break
    c8 c c c c c c c
    c c c c c c c c
    c c c c c c c c
    c f,4 g8 ~ g g g g |
\mark \markup{\box Pre-Chorus}
  a4\staccato r8 g r4 a\staccato |
  r8 g r a r g r4 |
  f4 f8 f c'4 c8 d ~ |
  d d d8 d d d a g |
  \break
  f4 f c'8 c c d ~ |
  d8 d d d d d d g, |
  f4 f c'8 e, f g ~ |
  g8 g g g g g e' g, |
  g4 g8 g g d' f d |
  \bar "||"
  \break
  \mark \markup{\box Chorus}
  c8 c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 g g g' g |
  \break
  c,8 c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 g g g g |
  \break
  \key des \major
  \mark \markup{\box "Guitar solo"}
  bes8 bes bes ges ges ges r as ~ |
  as4 r8 des des des ~ des4 |
  bes8 bes' bes ges, ges' ges, r8 as ~ |
  as4 r8 des des des ~ des4 |
  \break
  bes8 bes' bes, ges ges ges r8 as ~ |
  as4 r8 des8 des des ~ des4 |
  bes8 bes' bes, ges ges ges r8 as ~ |
  as4 r8 des8 des4 r
  \bar "||"
  \break
  \key c \major
  \mark \markup{\box "Synth solo"}
  c1 ~
  c2 ~ c4 ges'8 c |
  c,1 ~ c2 ~ c4 g8 ges |
  \break
  f1 ~
  f2 ~ f4 ~ f8. fis16 |
  g1 ~
  g4. g'8 ~ g g ~ g16 g8. |
  \break
  bes,1 ~ |
  bes4 bes' ~ bes8 c, c bes |
  a4. a8 ~ a a a a ~ |
  a a ~ a4 ~ a8 f f e |
  \break
  as1 ~
  as2 ~ as4. e8 |
  g1
  \tuplet 3/2 {g4 g g} \tuplet 3/2 {a'4 g d} |
  \break
  c1 ~
  c1
  \bar "||"
  \break
  R1*3
  r2 r8 g' c g |
  \break
  c,4 r r2 |
  r1
  r
  r8 f,4 g8 ~ g g' e g |
  \break
  \mark \markup{\box Chorus}
  c, c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 g g g g|
  \break
  c c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 g g e' g|
  \break
  c, c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 g e f\glissando g|
  \break
  c c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 g g g g|
  \break
  c c c c c c c c
  c c c c c c c c
  c c c c c c c c
  c f,4 g8 s s s s
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup \with {
  \consists "Instrument_name_engraver"
%  instrumentName = "Electric bass"
} <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
