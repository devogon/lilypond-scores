\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Rocket Man"
  subtitle = "from Honky Chateau, 1972"
  instrument = "Bass"
  composer = "Elton John"
  poet = "Bernie Taupin"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
  \tempo 4=69
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c {
  \global
  % Music follows here.
  R1*8^\markup{\bold{Verse } Piano/Vocal}
  g2.~ g8 \tuplet 3/2 {g16 d g,} |  % 9
  c1 | % 10
  g'2.~ g8 \tuplet 3/2 {g16 d g,} | % 11
  c'2. ~ c16 d8. | % 12
  \break
  es4. r8 d4. r8 | % 13
  c4. r8 bes4. r8 | % 14
  a4. \tuplet 3/2 {r16 bes b} c4. \tuplet 3/2 {r16 es e} | % 15
  f4. c8\glissando f,4\glissando^\markup{\bold "Drums enter"} f,4 | % 16
  \bar "||"
  \break
  bes2^\markup{\bold "Chorus"} d4 f | % 17
  es2 g4 bes | % 18
  bes,2 d4 f | % 19
  es4 r8 es8\staccato d4 r8 d8\staccato  | % 20
  c4. r16 g c8\staccato c4 d8\staccato | % 21
  es4. r16 bes es8 es4 f8  | % 22
  \break
  bes,4. r16 f bes8 bes4 bes8\glissando | % 23
  es4. r16 bes' es,8 es4 f8 | % 24
  bes,2 d4 f | % 25
  es2 g4 bes | % 26
  bes,2 d4 f | % 27
  es4 r8 bes' d,4 r8 bes' | % 28
  \break
  c,4. r16 g c8 c16 g c8. d16 | % 29
  es4. r16 bes es8 es4f8 | % 30
  bes,4. r16 bes' bes8 bes4 f8 | % 31
  es4. bes'16 bes bes8 bes4 c,8\glissando | % 32
  \break
  g1^\markup{\bold "Verse" Drums out} | % 33
  c4. c8 ~ c2 | % 34
  g1 | % 35
  c4. c8 ~ c4 c16 d8. | % 36
  es4. r8 d4. r8 | % 37
  c4. r8 bes4. r8 | % 38
  \break
  g4. \tuplet 3/2 {r16 g bes} c4. \tuplet 3/2 {r16 es e!} | % 39
  f2. ~ f8 f8\staccato | % 40
  g2. ~ g8 \tuplet 3/2 {g16 d f,} | % 41
  c'4. c8 ~ c c'4 r8 | % 42
  g4. r8 r4. \tuplet 3/2 {g16 d g,} | % 43
  \break
  c8. g'16 c8 c, ~ c4 c8 d | % 44
  es'4. es8 d4. d8 | % 45
  c4. c8 bes4.  bes8 | % 46
  a4 a,8 bes16 b! c4. \tuplet 3/2 {es16 e! f} | % 47
  f4 ~ f8. g16 f4 es16 d c8 | % 48
  \break
  bes2^\markup{\bold Chorus} d4 f | % 49
  es2 g4 bes | % 50
  bes,2 d4 f | % 51
  es4 r8 es8\staccato d4 r8 d8\staccato | % 52
  c4. r16 g c g c4 d8 | % 53
  \break
  es4. r16 bes es bes es4 f8 | % 54
  bes,4. r16 f bes8 bes4 bes8\glissando % 55
  es4. r16 bes' es,8 bes'4 es,8 | % 56
  bes2 bes'4 d, | % 57
  es2 g4 bes | % 58
  bes,2 d4 f | % 59
  \break
  es4 r8 bes' d,4 r8 bes' | % 60
  c,4. r16 g c8 c16 g c8. d16 | % 61
  es4. r16 bes es8 es4 f8 | % 62
  bes,4. bes'8 bes4. f8 | % 63
  es4. \tuplet 3/2 {es'16 es es} es4. \grace a,16 (bes8) | % 64
  \bar "||"
  \break
  bes,4.^\markup{\bold "Outro"} bes'8 ~ bes16 bes8. ~ bes4 | % 65
  es,4. es'8 ~ es2 | % 66
  bes,4. bes'8 ~ bes2 | % 67
  es,4. es'8 ~ es4 d8\staccato c\staccato | % 68
  \break
  \repeat volta 2 {
    bes,2^\markup{repeat and fade...} d4 f | % 69
    es2 g4 bes | % 70
    bes,2 d4 f | % 71
    es2 g4 bes | % 72
  }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
