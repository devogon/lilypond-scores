\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Funeral for a Friend"
  instrument = "Bass"
  composer = "Elton John"
  poet = "Bernie Taupin"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
    indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=104
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*9 |
  R1*3 |
  R1*9 |
  R1*2 |
  r1 |
  \break
  r2 r4 e,| % 25
  a2. a4 | % 26
  d2 c |
  b1 |
  gis2 r4 gis
  a2. ~ a8 a |
  bes2.  bes4 | % 31
  \break
  e2. b4 | % 32
  e,2. e4 |
  a2. a4 |
  d2 c |
  b2. ~ b8 fis16 g |
  gis2. gis4 |
  a2. a4 | % 38
  \break
  bes2. bes4 | % 39
  e,2. e4 |
  a2. a8 a16 gis |
  g2 g16 r g8 a b |
  c4. ~ c16 g c4. c16 b | % 43
  \break
  bes2 bes16 r bes8 c d | % 44
  es4. ~ es16 bes es4 d |
  c2 c16 r c8 d16 r e r |
  f4. ~ f16\staccato c f4 es | % 47
  \break
  d4. ~ d16\staccato a d r d4 d8 | % 48
  g4. ~ g16 d g4. g8 |
  gis2 gis,16 r gis8. gis8. |
  a2 g16 r g4 e16 f | % 51
  \break
  fis2 d'16 r d4 es8 | % 52
  e2 ~ e8 e4 b'8 ~ |
  <b e,>2 fis4 gis |
  a1 ~ |
  a1 ~ |
  a1 |
  r1 | % 58
  \break
  a,8. r16 r4 r2 | % 59
  a8. r16 r4 r e |
  a8. r16 r4 r2 |
  e16 r e4 a8 ~ a8. r16 r4 |
  a4 r r r8 e | % 63
  \break
  a4 r r2 | % 64
  a8. r16 r4 r2 |
  e16 r e4 a8 ~a2 |
  a8. a16 a r a8 ~ a e4 g8 |% 67
  \break
  a8. a16 a r a8 ~ a e4 g8 |% 68
  a8. a16 a8 a ~ a\staccato g4 f8 |
  e8 g gis a ~ a a a e |
  a8. a16 a8 a ~ a a4 e8 | % 71
  \break
  a8. a16 a r a8 ~ a e4 g8 | % 72
  a8. a16 a8 a~ a a4 a8 |
  e8. e16 e r a8 ~ a a4 gis8 |
  g8. g16 g r g8 g ~ g a b | % 75
  \break
  c8. c16 c r c8 ~ c\staccato c b a | % 76
  g8. g16 g8 g ~ g16\staccato g g r a8 b |
  c8. c16 c r c8 ~ c16\staccato c c r c8 b | % 78
  \break
  a8. a16 a r a8 ~ a16\staccato a a8 b cis |% 79
  d8. d16 r8 d ~ d16\staccato d d r f,8 g |
  a8. a16 a r a8 ~ a16\staccato a a8 b cis | % 81


}

\score {
  \new Staff { \clef "bass_8" \electricBass }
  \layout { }
}
