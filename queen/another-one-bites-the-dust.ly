\version "2.19.84"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Another One Bites the Dust"
  subtitle = "Queen - The Game - 1980"
  instrument = "Bass"
  composer = "bassist: John Deacon"
  tagline = \date
}

\layout {
  indent = 0.0\cm
  \omit Voice.StringNumber
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

global = {
  \key e \minor
  \time 4/4
  \tempo "Moderate Rock"
}

DScoda = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup { \small "D.S. al coda" }
}

electricBass = \relative c, {
  \global
  % Music follows here.
  \mark \markup {\box A  Intro}
  r2. r8 a16\4 g |
  e8\staccato r e\staccato r e\staccato r r8. e16 |
  e8\staccato e\staccato g e16 a\4 r4 r8 a16\4 g |
  \repeat volta 2 {
    e8\staccato r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4^\markup{4x} r2 |
  }
  \break
  \mark \markup {\box B Verse}
    \repeat volta 2 {
    e8\staccato\segno r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4 r2 |
  }
    c8\staccato c\staccato r16 c8 cis16 d\3 g8\staccato\2 r16  g,4\staccato  |
    \break
    c8\staccato c\staccato r16 c8 cis16 d4\3  g,4\staccato |
    c8\staccato c\staccato r16 c8 cis16 d\3 g8\staccato\2 r16  g,4\staccato  |
  a8\staccato\4 a\staccato\4 r16 a\staccato\4 r bes\4 b!8\4\staccato r b16\4 g8.\4 |
  \bar "||" \break

  \mark \markup {\box C Chorus}
  \repeat volta 3 {
    e8\staccato r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4 r2^\markup{3x} |
  }
  fis8\staccato r8 r16 b8 bes16 a8\staccato\4 a8\staccato\4^\markup{To Coda} a8\staccato\4 r|
  f8 r8 r4 r g |
  \break
  e8\staccato r e\staccato r e\staccato r r8. e16 |
  e8\staccato e\staccato g e16 a\4 r2 |
  e8\staccato r e\staccato r e\staccato r r8. e16 |
  e8\staccato e\staccato g e16 a\4 r4. \DScoda a16\4 g |
  %\break
  fis8\coda r r4 r g8 gis |
  \bar "||"
\break
  \mark \markup {\box D Guitar Break}
  fis8 r r4 r2 |
  R1*17 |
  \bar "||"
\break
  \mark \markup {\box E Verse}
  R1*3
  r2. r8 a16\4 g |
 % \break
  \repeat volta 2 {
    e8\staccato r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4 r2 |
  }
\break
  \mark \markup {\box F Verse}
    e8\staccato r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4 r4 r8 a16\4 g |
    %\break
    e8\staccato r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4 r2 |
      c8\staccato c\staccato r16 c8 cis16 d\3 g8\staccato\2 r16  g,4\staccato  |
    c8\staccato c\staccato r16 c8 cis16 d4\3  g,4\staccato |
   % \break
    c8\staccato c\staccato r16 c8 cis16 d\3 g8\staccato\2 r16  g,4\staccato  |
    a8\staccato\4 a\staccato\4 r16 a\staccato\4 r bes\4 b!8\4\staccato r b16\4 g8.\4 |
  %  \bar "||"
\break
    \mark \markup {\box G Chorus}
    \repeat volta 2 {
      e8\staccato r e\staccato r e\staccato r r8. e16 |
    e8\staccato e\staccato g e16 a\4 r2 |
    }

    \alternative {
      {e8\staccato r e\staccato r e\staccato r r8. e16 | \noBreak
      e8\staccato e\staccato g e16 a\4 r2 |}
      {fis8\staccato r8 r16 b8\4 bes16\4 a8\staccato\4 a8\staccato\4 a8\staccato\4 r|
      fis8\staccato r8 r4 r g }
    } \break
    \repeat volta 2 {
      e8\staccato r e\staccato r e\staccato r r8. e16 |
    }
    \alternative {
      {e8\staccato e\staccato g e16 a\4 r4 r8 a16\4 g |}
      {e8\staccato e\staccato g e16 a\4 r2 |}
    }
    \bar "||"
    \break
    c8\staccato c\staccato r16 c8 cis16 d\3 g8\staccato\2 r16  g,4\staccato  |
    c8\staccato c\staccato r16 c8 cis16 d4\3  g,4\staccato |
    c8\staccato c\staccato r16 c8 cis16 d\3 g8\staccato\2 r16  g,4\staccato  |
    a8\staccato\4 a\staccato\4 r16 a\staccato\4 r bes\4 b!8\4\staccato r8 r4%r b16\4 g8.\4 |
%    e8\staccato r8 r4 r2 |
R1*2
    \bar "|."
}

\score {
  \new StaffGroup <<
    \new Staff \with {
   %   midiInstrument = "electric bass (finger)"
    } { \clef "bass_8" \electricBass }
   % \new TabStaff \with {
%      stringTunings = #bass-tuning
%    }     \electricBass
  >>
  \layout { }
  % \midi {
  %   \tempo 4=110
  % }
}
