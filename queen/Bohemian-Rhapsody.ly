\version "2.21.6"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Bohemian Rhapsody"
  subtitle = "Queen - A Night at the Opera - 1975"
  instrument = "Bass"
  composer = "Freddy Mercury"
  arranger = "John Deacon"
  meter = "Rubato"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key es \major
  \time 4/4
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  \mark \markup{Intro}
  r8 d\3 d\3 d\3 d4\3 d\3 |
  \time 5/4
  r8 d\3 d\3 d\3 d\3 d\3 d4\3 r4 |
  \time 9/8
  d8\3 d\3 d\3 d4\3 d8\3 d\3 d\3 d\3 |
  \time 5/4
  d8\3 d\3 d\3 d\3 d\3 d\3 (d4\3) r
  \bar "||"
  \break
  \time 4/4
  \tempo 4=70
  R1*10
  \bar "||"
  \break
  bes1\4
  bes1\4
  \bar "||"
  \mark \markup{Verse}
  bes1\4 |
  g2. g8 r |
  c1\4
  c2\4 f'4 (f)\glissando |
  bes,,1
  g2. (g8) r |
  c4.   \override NoteHead.style = #'cross c8 \revert NoteHead.style b!4 bes |
  a!4. r8 as4 g |
  \break
  es'2.\3 d4\3 |
  c2. (c8) r |
  f4 r8 e!8 es4 d |
  bes4.. bes16 bes8. bes16 bes4\glissando  |
  \break
  es4. r8 d2 |
  c4. r8 as'4. (as8)\glissando ( |
  es'1)
  \time 2/4
  r2
  \time 4/4
  bes,1
  bes1
  \bar "||"
  \break
  \mark \markup{Verse}
  bes2. (bes8) r |
  g2. (g8) r |
  c1
  c2 f'4 (f)\glissando |
  bes,,2. (bes8) r |
  g1
  c4. r8 b!4 bes |
  a!4. r8 as4 g |
  \break
  es'2. d4 |
  c1 |
  f4. e!8 es4 d |
  bes8 bes16 bes bes8. bes16 bes8. bes16 bes8 (bes)\glissando |
  \break
  \mark \markup{Guitar Solo}
  es4. r8 d2 |
  c4.. g'16 c,2 |
  f4. e!8 es4 d |
  bes8. bes16 bes8. f'16 bes8. f16 bes16 f bes8\glissando |
  \break
  es,4. es16 es d4. d8 |
  c4 r8 c c4 r16 g' c g |
  f4 r8 e! es4 d |
  des4 ( \tuplet 3/2 {d8) d-> c->} b!4-> bes->\glissando |
  \bar "||"
  \break
  \key a \major
  \mark \markup{Opera Section}
  \tempo "double time" 4=110
  a4-> r r2 |
  r1
  \bar "||"
  r1
  r1
  r1
  r1
  \break
  gis'4 gis8 gis gis4 gis |
  gis4 gis e, e |
  a4\staccato r r2 |
  r1
  r1
  r1
  \time 2/4
  r2
  \bar "||"
  \break
  \key es \major
  \time 4/4
  r1
  r1
  es'4 es8 es es4 es |
  es8 es es4 es es |
  \break
  as4 as8 as g4 g8 g |
  f es d c bes4 r |
  r1
  \break
  r1
  r2. bes4-> |
  es4-> bes-> es-> r8 bes |
  bes8 bes bes bes bes4 r |
  \break
  r4 bes4-> es-> bes8-> bes |
  bes8 bes bes bes bes4 r |
  r bes-> es-> bes8-> bes |
  \break
  bes8 bes bes bes bes4 r |
  bes8 bes bes bes bes4 r |
  bes8 bes bes bes bes4 r |
  r1
  b!4-> a!-> d-> des-> |
  \time 2/4
  ges4-> bes,-> | % 87
  \break
  \time 4/4
  es4-> r r2 | % 88
  r4 es8 es as es d c |
  \time 2/4
  bes4\staccato r |
  \time 4/4
  es4. es8 as,4 as |
  d8 d d d g4 g |
  \break
  \repeat unfold 12 {bes,8} \tuplet 3/2 {bes4 bes8} bes4
  bes4 bes bes \tuplet 3/2 {bes4 bes8} |
  bes4 bes bes bes\glissando |
  \break
  \bar "||"
  \tempo 4=140
  \time 12/8
  es4 g,8 (g) as4 bes c8 d es4 |
  es4 g,8 (g) as4 bes8 c bes (bes4.) |
  \break
  es4 g,8 (g) as4 bes c8 d es4 |
  f4 a,!8 (a) bes4 c8 d c (c4.) |
  \break
  bes4 bes8 bes bes4 bes bes8 bes bes4 |
  bes4 bes8 bes8 bes4 es4 es8 es es4 | % 102
  bes4 bes8 bes4. bes'8 as f bes as f |
  \time 6/8
  des4 des'8 (des4.) |
  \break
  \time 12/8
  bes,4 bes8 bes bes4 bes4 bes8 bes bes4 | % 105
  bes4 bes8 bes bes4 es es8 (es) es4 |
  as4 as8 as4. (as4) as8-> (as) g4-> |
  \break
  f4 f8 (f4.) f4 c8 f c4 | % 108
  bes4 bes8 bes4. bes4 bes8 bes bes4 |
  f'4 f8 f f4 f c8 f c r |
  \break
  bes4 bes8 bes bes4 bes bes'8 bes bes4 |
  f4 c8 f c4 bes bes'8 bes bes4|
  f c8 f c4 bes bes8 bes bes4 |
  \bar "||"
  \break
  es4 g,8 (g) as4 bes c8 d es4 | % 114
  es4 g,8 (g) as4 bes8 c bes (bes4.) |
  es4 g,8 (g) as4 bes c8 d es4 |
  \break
  f4 a,!8 (a) bes4 c d8 es f4 | % 117
  ges8 as bes (bes) as bes c4 bes8 c d4 |
  b!4\staccato r8 r4. r4. r4. |
  as4\staccato r8 r4. r4. r4. |
  bes4\staccato r8 r4. r4. r4. |
  \tempo "Rall..."
  R1*12/8
  R1*12/8
  \bar "||"
  \tempo 4=70
  \time 4/4
  es,8. bes'16 es8 bes16 es, d8 d d' d, | % 124
  c8 (c'4.) b,4 c |
  b!4 c  b'16 as g f es4 |
  d8. as'16 d8 as16 d, g2 |
  as4. as8\staccato
  es4. es16 \override NoteHead.style = #'cross d \revert NoteHead.style |
  \break
  c2 g |
  c2 g |
  c'2 r |
  r1
  \break
  r1
  r1
  r1
  r1
  \break
  r1
  r1
  f,,2\fermata_\markup{\tiny \column {
    \line{(not on the record, but you might need} \line{to play this on a gig)}}}
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { \omit Voice.StringNumber }
}
