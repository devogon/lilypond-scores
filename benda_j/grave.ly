\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Grave"
  subtitle = "from Violin concerto G-Dur"
  subsubtitle = "checkme"
  instrument = "Violoncello e Basso"
  composer = "J. Benda"
  % Remove default LilyPond tagline
  tagline = \date
}
%
\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \numericTimeSignature
  \time 3/4
  \tempo "Grave" 4=60
}

cello = \relative c {
  \global
  % Music follows here.
  e8\pp r e r e r | % 1
  dis8 r dis r dis r
  e8 r c\< (a16 b) fis8 (fis')\!
  b,8\mf\> r b (dis)\! e r
  r1*3/4
  \break
  b4\mf e g,
  d' g c,
  fis g,\> b
  e8\!\pp r e r e r
  dis r dis r dis r
  \break
  e8 r c\< (a16 g) fis8 (fis')
  b,8\!\mf r b\> (b') c r\!
  r1*3/4
  g4\f c, fis
  b, e a,^\markup{dim.}
  \break
  b8^\markup{rall} c d4\< g,8 r\!
  gis8\pp^\markup{a tempo} r gis' r gis r
  a8 r f! r f r
  e8 r e4 ~ ( e8 d)
  c8 c a4 c8 (a)
  \break
  fis'8 r dis r b^\markup{dim} r
  e8 r d r c\pp r
  cis8 r^\markup{cresc} d r dis r
  e8 r c ( a16 g) fis8 (fis')
  b,8\f r b dis\> e r\!
  \break
  r1*3/4
  b4\mf e a
  d, g c,
  \tempo "Largo"
  fis8 ~ ( fis16 e) dis8\< (e) c^\markup{rit.} (fis,)
  g8\!\f (a) b4 b
  e,2.->
  \bar "|."
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}
