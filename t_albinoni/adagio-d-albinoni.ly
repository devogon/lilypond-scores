\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "Adagio d' Albinoni"
  composer = "attr to: T. Albinoni (1671-1751"
  arranger = "trans: Bernard Dewagtere"
  instrument = "contrabasse"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key bes \major
  \time 3/4
  \tempo "Adagio" 4=70
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
    \repeat volta 2 {
      g4\mf g' g, |
    f f' f, |
    es' es' es, |
    d d' d, |
    c c' c, | % 5
    \break
    cis cis' cis, |
    d d' d, |
    g, g' g, |
    g g' g, |
    a a' a, | % 10
    \break
    a a' a, | % 11
    bes bes' bes, |
    bes bes' bes, |
    c c' c, |
    a a' a, | % 15
    \break
    bes bes' bes, | % 16
    g g' g, | % 17
    a a' a, | % 18
    bes\> bes' c, | % 19
    d2\fermata\! r4 | % 20
    \break
    R1*3/4 | % 21
    b!4 b' b, | % 22
    c c' c, | % 23
    bes bes' bes, | % 24
    as as' as, | % 25
    \break
    g g' g, | % 26
    d' d' d, | % 27
    }
    \alternative {
      { g, g' g,}% 28
      { g2 r8. b!16}
    }
    c2.\> | % 30
    \break
    cis2\! r8. cis16 | % 31
    d4 d' d, | % 32
    c c' c, | % 33
    cis cis' cis, | % 34
    \break
    d d' d, | % 35
    g,2. ~ g2.\> ~ g2.\fermata\!
    \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}



\markup {
  \teeny
  \date
}