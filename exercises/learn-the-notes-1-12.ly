\version "2.19.54"
\language "nederlands"

\header {
  title = "learn the notes / positions"
  instrument = "contrabas"
  arranger = "me"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
}

bass = \relative c, {
  \global
  % Music follows here.
  c8^\markup{"C"} c c c c c c c |
  c c c c c c c c |
  c'8 c c c c c c c |
  c8 c c c c c c c |
  \break
  f,,^\markup{"F"} f f f f f f f |
  f' f f f f f f f |
  f f f f f f f f |
  f' f f f f f f f |
  \break
  bes,,^\markup{B\flat} bes bes bes bes bes bes bes |
  bes bes bes bes bes bes bes bes |
  bes' bes bes bes bes bes bes bes |
  bes bes bes bes bes bes bes bes |
  \break
  es,^\markup{E\flat} es es es es es es es |
  es es es es es es es es |
  es es es es es es es es |
  es' es es es es es es es |
  \break
  as,,^\markup{A\flat} as as as as as as as |
  as' as as as as as as as |
  as as as as as as as as |
  as as as as as as as as |
  \break
  des,^\markup{D\flat} des des des des des des des |
  des des des des des des des des |
  des' des des des des des des des |
  des des des des des des des des |
  \break
  ges,,^\markup{G\flat} ges ges ges ges ges ges ges |
  ges' ges ges ges ges ges ges ges |
  ges ges ges ges ges ges ges ges |
  ges' ges ges ges ges ges ges ges |
  \break
  b,,^\markup{"B"} b b b b b b b |
  b b b b b b b b |
  b' b b b b b b b |
  b b b b b b b b |
  \break
  e,,^\markup{"E"} e e e e e e e |
  e' e e e e e e e |
  e e e e e e e e |
  e' e e e e e e e |
  \break
  a,,^\markup{"A"} a a a a a a a |
  a' a a a a a a a |
  a a a a a a a a |
  a a a a a a a a |
  \break
  d,^\markup{"D"} d d d d d d d |
  d d d d d d d d |
  d' d d d d d d d |
  d d d d d d d d |
  \break
  g,,^\markup{"G"} g g g g g g g |
  g' g g g g g g g |
  g g g g g g g g |
  g' g g g g g g g |
}

\score {
  \new Staff { \clef "bass_8" \bass }
  \layout { }
}

\markup {
  \column {
    \line{"Say the note name as you play it. Once you've gotten the notes on the strings in order (EADG),"}
    \line{"mix up the string order."}
  }
}