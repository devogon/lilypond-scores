\version "2.19.36"
\language "nederlands"

\header {
  title = "Scales"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  %\key c \major
  \time 4/4
  \tempo 4=100
}

scoreAContrabass = \relative c, {
  \global
  % Music follows here.
    \key f \major
    f2-1 g-4
    a-0 bes-1
    c-4 d-0
    e-2 f-4
    \bar "||"
    f-4 e-2
    d-0 c-4
    bes-1 a-0
    g-4 f-1
    \bar "|."
}

\score {
  \new Staff { \clef bass \scoreAContrabass }
  \layout { }
  \header { piece = "F Major Scale" }
}

scoreBContrabass = \relative c {
  \global
  \key bes \major
  % Music follows here.
  bes2-1 c-4
  d-0 es-1
  f-4 g-0
  a-2 bes-4
  \bar "||"
  bes2-4 a-2
  g-0 f-4
  es-1 d-0
  c-4 bes-1
  \bar "|."
}

\score {
  \new Staff { \clef bass \scoreBContrabass }
  \layout { }
  \header { piece = "B flat Major Scale" }
}

scoreCContrabass = \relative c {
  \global
  % Music follows here.

}

\score {
  \new Staff { \clef bass \scoreCContrabass }
  \layout { }
}

scoreDContrabass = \relative c {
  \global
  % Music follows here.

}

\score {
  \new Staff { \clef bass \scoreDContrabass }
  \layout { }
}

scoreEContrabass = \relative c {
  \global
  % Music follows here.

}

\score {
  \new Staff { \clef bass \scoreEContrabass }
  \layout { }
}

scoreFContrabass = \relative c {
  \global
  % Music follows here.

}

\score {
  \new Staff { \clef bass \scoreFContrabass }
  \layout { }
}
