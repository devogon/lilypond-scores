\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "scales"
%  composer = "composer"
  poet = \markup{A \tiny{" Ais/Bes "} B C \tiny{" Cis/Des "} D \tiny{" Dis/Es "} E F \tiny{" Fis/Ges "} G \tiny{" Gis/As "} A}
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
 % \key g \major
  \time 4/4
%  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c'' {
   \global

  \key a \major a^\markup{A major} b cis d e fis gis a
  \key a \minor a,^\markup{a minor}-0 b-1 c-2 d-0 e-1 f-1 g-1 a-1
  \key a \minor a,^\markup{a melodic minor} b c d e fis gis a

  \break
  \key d \major d,^\markup{D major} e fis g a b cis d
  \key d \minor d,^\markup{d minor}-0 e-1 f-2 g-0 a-1 bes-4 c-2 d-4
  \key d \minor d,^\markup{d melodic minor} e f g a b! cis d
  \break
  \key g \major g,,^\markup{G major} a b c d e fis g
  \key g \minor g,^\markup{g minor} a bes c d es f g
  \key g\minor g,^\markup{g melodic} a bes c d e fis g
  \break
  \key c \major c,^\markup{C major} d e f g a b c
  \key c \minor c,^\markup{c minor} d es f g as bes c
  \key c \minor c,^\markup{c melodic minor} d es f g a b c
  \break

  \key es \major es,^\markup{E\flat major} f g as bes c d es
  \key es \minor es,^\markup{e\flat minor} f ges aes bes ces des es
  \key es \minor es,^\markup{e\flat melodic minor} f ges as bes c d es
  \break
  \key f \major f,,^\markup{F major} g a bes c d e f
  \key f \minor f,^\markup{f minor} g as bes c des es f
  \key f \minor f,^\markup{f melodic minor} g as bes c d e f
  \break
  \key bes \major bes,^\markup{B\flat major} c d es f g a bes
  \key bes \minor bes,^\markup{b\flat minor} c des es f ges aes bes
  \key bes \minor bes,^\markup{b\flat melodic minor} c des es f g a bes
  \break
  \key as \major as,^\markup{A\flat major} bes c des es f g as
  \key as \minor as,^\markup{A\flat minor} bes ces des es fes ges as
  \key as \minor as,^\markup{A\flat melodic minor} bes ces des es f g as
  \break
  \key cis \major cis,^\markup{C\sharp major} dis eis fis gis ais bis cis
  \key cis \minor cis,^\markup{C\sharp minor} dis e fis gis a b cis
  \key cis \minor cis,^\markup{C\sharp mel. minor} dis e fis gis ais bis cis
  \break
  \key des \major des,^\markup{D\flat major} es f ges as bes c des
  \key des \minor des,^\markup{D\flat minor (but C\sharp minor!} es fes ges as beses ces des
  \key des \minor des,^\markup{D\flat mel minor} es fes ges as bes c des
  \break

  \key fis \major fis,,^\markup{F\sharp major} gis ais b cis dis eis fis
  \key fis \minor fis,^\markup{F\sharp minor} gis a b cis d e fis
  \key fis \minor fis,^\markup{F\sharp mel. minor} gis a b cis dis eis fis
  \break
  \key b \major b,^\markup{B major} cis dis e fis gis ais b
  \key b \minor b,^\markup{B minor} cis d e fis g a b
  \key b \minor b,^\markup{B mel. minor} cis d e fis gis ais b
  \break
  \key e \major e,,^\markup{E major} fis gis a b cis dis e
  \key e \minor e,^\markup{E minor} fis g a b c d e
  \key e \minor e,^\markup{E mel. minor} fis g a b cis dis e
  \break
  \key gis \major gis,^\markup{G\sharp maj \tiny{see  A\flat major}} ais bis cis dis eis fisis gis
  \key gis \minor gis,4^\markup{g\sharp minor} ais b cis dis e fis gis
  \key gis \minor gis,^\markup{g\sharp mel. minor} ais b cis dis eis fisis gis
  \break
  \key es \major es^\markup{E\flat major} f g as bes c d es
  \key es \minor es,^\markup{e\flat minor} f ges as bes ces des es
  \key es \minor es,^\markup{e\flat melodic minor} f ges as bes c d es
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef treble \contrabas }
  \layout { }
}
