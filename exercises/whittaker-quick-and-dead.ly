\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "quick and dead"
  subtitle = "an exercise"
  composer = "dennis whittaker"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


global = {
  \key c \major
  \time 4/4
  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = {
   \global
   \mark \default
   g8\flageolet d\flageolet d d^\markup{"*"}
   a d d d^\markup{"*"}
   b d d d^\markup{"*"}
   c' d d d^\markup{"*"}
   d' d d d^\markup{"*"}
   c' d d d^\markup{"*"}
   b d d d^\markup{"*"}
   a d d d^\markup{"*"}
   g d d d^\markup{"*"}
   b d d d^\markup{"*"}
   a d d d^\markup{"*"}
   c' d d d^\markup{"*"}
   b d d d^\markup{"*"}
   d' d d d^\markup{"*"}
   c' d d d^\markup{"*"}
   a d d d^\markup{"*"}
   g2\flageolet g\fermata
   \bar "||"

   \break
   \mark \default
   g8 d d^\markup{"*"} d
   a d d^\markup{"*"} d
   b d d^\markup{"*"} d
   c' d d^\markup{"*"} d
   d' d d^\markup{"*"} d
   c' d d^\markup{"*"} d
   b d d^\markup{"*"} d
   a d d^\markup{"*"} d
   g d d^\markup{"*"} d
   b d d^\markup{"*"} d
   a d d^\markup{"*"} d
   c' d d^\markup{"*"} d
   b d d^\markup{"*"} d
   d' d d^\markup{"*"} d
   c' d d^\markup{"*"} d
   a d d^\markup{"*"} d
   g2 g\fermata

   \break
   \mark \default
   g8 d^\markup{"*"} d d
   a d^\markup{"*"} d d
   b d^\markup{"*"} d d
   c' d^\markup{"*"} d d
   d' d^\markup{"*"} d d
   c' d^\markup{"*"} d d
   b d^\markup{"*"} d d
   a d^\markup{"*"} d d
   g d^\markup{"*"} d d
   b d^\markup{"*"} d d
   a d^\markup{"*"} d d
   c' d^\markup{"*"} d d
   b d^\markup{"*"} d d
   d' d^\markup{"*"} d d
   c' d^\markup{"*"} d d
   a d^\markup{"*"} d d
   g1\fermata
   }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

% source: https://www.youtube.com/watch?v=-O8ha91JhqM