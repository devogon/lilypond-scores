\version "2.19.57"

\language "nederlands"

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\header {
  title = "scales - two octave"
%  composer = "composer"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \markup { \teeny  \date }
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
 % \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
   c-2^\markup{\bold \underline "C major"} d-0 e-1 f-2 |
   g-0 a-1 b-4 c\finger "--1" |
   d-4 e\finger "--2" f-4 g\finger "-T" |
   a-1 b-2 c-3 b-2 |
   a-1 g\thumb f\finger "--4" e-2 |
   d-4 c-1 b\finger "--4" a-1 |
   g-0 f-2 e-1 d-0 |
   c1-2 |
   \break
   \key g \major
   g4-2^\markup{\bold \underline  "G major"} a-0 b-1 c-2 |
   d-0 e-1 fis-4 g-0 |
   a-1 b-4 c\finger "--1" d-4 |
   e\finger "--1" fis-3 g-4 fis-3 |
   e-1 d\finger "--4" c-1 b\finger "--4" |
   a-1 g-0 fis-4 e-1 |
   d-0 c-2 b-1 a-0 |
   g1-2 |
   \break

   \key d \major
   d'4^\markup{\bold \underline "D major"} e fis g |
   a b cis d |
   \clef treble
   e fis g a |
   b cis d cis |
   b a g fis |
   \clef bass |
   e d cis b |
   a g fis e |
   d1 |
   \break

   \key a \major
   a4^\markup{ "A major"} b cis d |
   e fis gis a |
   b cis d e |
   fis gis a gis |
   fis e d cis |
   b a gis fis |
   e cis d b |
   a1 |
   \break

   \key e \major
   e4^\markup{ "E major"} fis gis a |
   b cis dis e |
   fis gis a b |
   cis dis e dis |
   cis b a gis |
   fis e dis cis |
   b a gis fis |
   e1 |
   \break

   \key b\major
   b'4^\markup{ "B major"} cis dis e |
   fis gis ais b |
   cis dis e fis |
   gis ais b ais |
   gis fis e dis |
   cis b ais gis |
   fis e dis cis |
   b1
   \break

   \key ges \major
   ges4^\markup{{G\flat major}} as bes ces
   des es f ges
   as bes ces des
   es f ges f
   es des ces bes
   as ges f es
   des ces bes as
   ges1
   \break

   \key des \major
   des'4^\markup{{D\flat major}} es f ges
   as bes c des
   \clef treble
   es f ges as
   bes c des c
   bes as ges f
   \clef bass
   es des c bes
   as ges f es
   des1
   \break

   \key aes \major
   as4^\markup{{A\flat major}} bes c des
   es f g as
   bes c des es
   f g as g
   f es des c
   bes as g f
   es des c bes
   as1
   \break

   \key  es \major
   es4^\markup{{E\flat major}} f g as
   bes c d es
   f g as bes
   c d es d
   c bes as g
   f es d c
   bes as g f
   es1
   \break

   \key bes \major
   bes'4^\markup{\bold \underline {B\flat major}}-1 c-4 d-0 es-1
   f-4 g-0 a-2 bes-4
   c\finger "--1" d-4 es\finger "--1" f-4
   g\finger "--T" a-2 bes-3 a-2
   g\finger "T" f\finger "--4" es-1 d\finger "--4"
   c-1 bes\finger "--4" a-1 g-0
   f-4 es-1 d-0 c-4
   bes1-1
   \break

   \key f \major
   f4^\markup{\bold \underline {F major}} g a bes
   c d e f
   g a bes c
   d e f e
   d c bes a
   g f e d
   c bes a g
   f1
   \break

   \key c \minor
   c'4^\markup{{c natural minor}} d es f
   g as bes c
   d es f g
   as bes c bes
   as g f es
   d c bes as
   g f es d
   c1
   \break

   \key g \minor
   g4^\markup{{g natural minor}} a bes c
   d es f g
   a bes c d
   es f g f
   es d c bes
   a g f es
   d c bes a
   g1
   \break

   \key d \minor
   d'4^\markup{{d natural minor}} e f g
   a bes c d
   e f g a
   bes c d c
   bes a g f
   e d c bes
   a g f e
   d1
   \break

   \key a \minor
   a4^\markup{\bold {a natural minor}} b c d |
   e f g a |
   b c d e |
   f g a g |
   f e d c |
   b a g f |
   e c d b |
   a1 |
   \break

   \key e \minor
   e4^\markup{{e natural minor}} fis g a |
   b c d e |
   fis g a b |
   c d e d |
   c b a g |
   fis e d c |
   b a g fis |
   e1 |
   \break

   \key b \minor
   b4^\markup{{b natural minor}} c d e
   \break

   \key fis \minor
   f,4^\markup{{f\sharp natural minor}} g a b
   \break

   \key cis \minor
   cis4^\markup{{c\sharp minor}} d e f
   \break

   \key as \minor
   as,4^\markup{{a\flat minor}} b c d
   \break

   \key es \minor
   es,4^\markup{{e\flat minor}} f g a
   \break

   \key bes \minor
   bes4^\markup{{b\flat minor}} c d e
   \break

   \key f \minor
   f,4^\markup{{f minor}} g a b
   \break

% harmonic minor scales
   \key c \minor
   c4^\markup{{c harmonic minor}} d es f
   g as b c
   d es f g
   as b c b
   as g f es
   d c b as
   g f es d
   c1
   \break

   \key g \minor
   g4^\markup{{g harmonic minor}} a bes c
   d es f g
   a bes c d
   es f g f
   es d c bes
   a g f es
   d c bes a
   g1
   \break

   \key d \minor
   d'4^\markup{{d harmonic minor}} e f g
   a bes c d
   e f g a
   bes c d c
   bes a g f
   e d c bes
   a g f e
   d1
   \break

   \key a \minor
   a4^\markup{{a harmonic minor}} b c d
   \break

   \key e \minor
   e,4^\markup{{e harmonic minor}} f g a
   \break

   \key b \minor
   b4^\markup{{b harmonic minor}} c d e
   \break

   \key fis \minor
   f,4^\markup{{f\sharp harmonic minor}} g a b
   \break

   \key cis \minor
   cis4 ^\markup{{c\sharp harmonic minor}} d e f
   \break

   \key as \minor
   as,4^\markup{{a\flat harmonic minor}} b c d
   \break

   \key es \minor
   es,4^\markup{{e\flat harmonic minor}} f g a
   \break

   \key bes \minor
   bes4^\markup{{b\flat harmonic minor}} c d e
   \break

   \key f \minor
   f,4^\markup{{f harmonic minor}} g a b
   f1
   \break

% melodic minor scales
   \key c \minor
   c'4^\markup{{c melodic minor}} d es f
   g a! b! c
   d es f g
   a! b! c b
   a! g f es
   d c b! a!
   g f es d
   c1
   \break

   \key g \minor
   g4^\markup{{g melodic minor}} a bes c
   d e! fis g
   a bes c d
   es fis g fis
   es d c bes
   a g fis e!
   d c bes a
   g1
   \break

   \key d \minor
   d'4^\markup{{d melodic minor}} e f g
   a b! cis d
   e f g a
   b! cis d cis
   b! a g f
   e d cis b!
   a g f e
   d1
   \break

   \key a \minor
   a4^\markup{{a melodic minor}} b c d
   \break

   \key e \minor
   e,4^\markup{{e melodic minor}} f g a
   \break

   \key b \minor
   b4^\markup{{b melodic minor}} c d e
   \break

   \key fis \minor
   f,4^\markup{{f\sharp melodic minor}} g a b
   \break

   \key cis \minor
   cis4^\markup{{c\sharp melodic minor}} d e f
   \break

   \key as \minor
   as,4^\markup{{a\flat melodic minor}} b c d
   \break

   \key es \minor
   es,4^\markup{{e\flat melodic minor}} f g a
   \break

   \key bes \minor
   bes4^\markup{{b\flat melodic minor}} c d e
   \break

   \key f \minor
   f,4^\markup{{f melodic minor}} g a b
   \break
 }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

