\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Autumn Leaves - Simplified"
  instrument = "Bass"
  arranger = "TalkingBass.com"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
  \tempo 4=100
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  c1:m7
  f1:7
  bes1:maj7
  es1:maj7
  \break
  a1:m7dim
  d1:7
  g1:m7
  g1:7
  c1:m7
  f1:7
  bes:maj7
  es1:maj7
  a:m7dim
  d1:7
  g1:m7
  s1
  a1:m7dim
  d1:7
  g1:m7
  s1
  c1:m7
  f1:7
  bes:maj7
  es:maj7
  a:min7dim
  d1:7
  g2:m7 fis:7
  f:m7 e:7
  es1:maj7
  a2:m7dim d:7
  g1:m7
  g:7
}

bass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 2 {
  c4 d es g
  f g a c
  bes a g f
  es d c bes
  \break
  a bes c es
  d e! fis a
  g f d bes
  g a b! d
  \break
  c d es g
  f g a c
  bes a g f
  es d c bes
  \break
  a bes c es
  d e! fis a
  g f d bes
  g fis g gis
  \break
  a bes c es
  d e! fis a
  g f d bes
  g a bes d
  \break
  c d es g
  f g a c
  bes a g f
  es d c bes
  \break
  a bes c es
  d e! fis a
  g g fis fis
  f f e! e
  \break
  es d c bes
  a c d fis
  g f d bes
  g a b! d
  }
}

chordsPart = \new ChordNames \chordNames

bassPart = \new Staff { \clef "bass_8" \bass }

\score {
  <<
    \chordsPart
    \bassPart
  >>
  \layout { }
}
