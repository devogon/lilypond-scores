\version "2.23.13"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Zombie"
  subtitle = "The Cranberries"
  %subsubtitle = \markup{Everybody  Else Is Doing it, So Why Can't We?  (1993)}
  %instrument = "Bass"
  composer = "D. O‘Riordan"
  poet = "Bass: Michael Hogan"
  tagline = \date
}

\paper {
  indent = 0.0\cm
  page-count = 1
}
global = {
  \key g \major
  \time 4/4
  \tempo 4=82
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s1
  s1
  s1
  s1
  e1:m
  c1:maj7
}

electricBass = \relative c, {
  \global
  \omit  Voice.StringNumber
  % Music follows here.
  \mark \markup{Intro}
  R1*3
  r2 r4 e\4\glissando |
  \break
  \repeat volta 2 {e,8 e e e e e g e |
  c' c c c c c d c |
  g g g g g g a g |
  fis fis fis fis fis g fis g |}
  \break
%    e8 e e e e e g e |
%   c' c c c c c d c |
%   g g g g g g a g |
%   fis fis fis fis fis g fis g |
%   \break
  \mark \markup{Interlude}
  e8 e e e e e e e |
  c' c c c c c c c |
  g g g g g g g g |
  fis fis fis fis fis g fis g |
  \break
  \mark \markup{Verse 1}
  \repeat volta 4 {
    e8 e e e e e e e |
    c' c c c c c c c |
    g g g g g g g g |
    fis fis fis fis fis g fis g^\markup{x 4} |
  \break
  }
%   e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis fis fis g fis g |
%   \break
%   e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis fis fis g fis g |
%   \break
%   e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis fis fis g fis g |
  \break
  \mark \markup{Chorus}
  \repeat volta 3 {
  e8 e e e e e e e |
  c' c c c c c c c |
  g g g g g g g g |
  fis fis fis g fis g fis g^\markup{x 3} |
  \break
  }
  % e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis g fis g fis g |
%   \break
%   e8 e e e e e g e |
%   c' c c c c c d c |
%   g g g g g g a g |
%   fis fis fis fis fis g fis g |
%   \break
  \mark  \markup{Interlude}
  e8 e e e e e e e |
  c' c c c c c c c |
  g g g g g g g g |
  fis fis fis fis fis g fis g |
  \break
  \mark \markup{Verse 2}
  \repeat volta 4 {
    e8 e e e e e e e |
  c' c c c c c c c |
  g g g g g g g g |
  fis fis fis fis fis g fis g |
  \break
  }
 %  e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis fis fis g fis g |
%   \break
%   e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis fis fis g fis g |
%   \break
%   e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis fis fis g fis g |
%   \break
  \mark \markup{Chorus}
  \repeat volta 4 {
    e8 e e e e e e e |
  c' c c c c c c c |
  g g g g g g g g |
  fis fis fis g fis g fis g |
  \break
  }
%     e8 e e e e e e e |
%   c' c c c c c c c |
%   g g g g g g g g |
%   fis fis fis g fis g fis g |
%   \break
%     e8 e e e e e g e |
%   c' c c c c d c a |
%   g g g g g g a g |
%   fis fis fis fis fis g fis g |
%   \break
%   e8 e e e e e g e |
%   c' c c c c c d c |
%   g g g g g g a g |
%   fis fis fis fis fis g fis g |
%   \break
  \mark \markup{Pre-Solo}
  e8 e e e16 e e8 e g e |
  c' c c c16 c d8 c a fis |
  e8 e e e16 e e8 e g e |
  c' c c c16 c d8 c a fis |
  \break
  \mark \markup{Guitar Solo}
   e8 e e e16 e e8 e g e |
  c' c c c16 c d8 c a fis |
  e8 e e e16 e e8 e g e |
  c' c c c16 c d8 c a fis |
  \break
  e8 e e e e e g e |
  c' c c c c d c a |
  g g g g g g g g |
  fis fis fis fis fis g fis g |
  \break
  e8 e e e e e g e |
  c' c c c c d c a |
  g g g g g g g g |
  fis fis fis fis fis g fis g |
  \break
  e8 e e e e e g e |
  c' c c c c d c a |
  g g g g g g g g |
  fis fis fis fis fis g fis g |
  \break
  \mark \markup{Outro}
   e8 e e e16 e e8 e g e |
  c' c c c16 c d8 c a fis |
  e8 e e e16 e e8 e g e |
  c' c c c16 c d8 c a fis |
  \break
  e e e e16 e e e e8 g e |
  c' c c c16 c d8 c a fis |
  e8 r r4 r2 |
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
 %  \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
