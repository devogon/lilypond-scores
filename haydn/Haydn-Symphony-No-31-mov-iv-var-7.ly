\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Symphony  No. 31"
  subtitle = "EX 1"
  instrument = "Cello"
  composer = "Haydn"
  piece = "Movement IV: Var 7 (with repeats)"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \numericTimeSignature
  \time 2/4
  \tempo "Moderato molto." 4=100
  \partial 8
}

scoreACelloI = \relative c {
  \global
  % Music follows here.
  \clef tenor
  \repeat volta 2 {
    a'8
    d8. fis32 e d8 a | % 1
    \tuplet 3/2 {b16 (cis d} \tuplet 3/2 {e fis g)} a8 a,16 a' | % 2
    a16 (g) b,, g'' g (a) a,, a'' | % 3
    fis4 (e8) a, | % 4
    d8. fis32 e d8 d | % 5
    \break
    d8. fis32 e \tuplet 3/2 {d16 (fis e} \tuplet 3/2 {fis e d)} | % 6
    \tuplet 3/2 {cis16 (d e} \tuplet 3/2 {d cis b)} a8 b | % 7
    a4 r8
  }
  \repeat volta 2 {
    \partial 8 a8
    a'8. fis32 g a8 b | %
    \grace a32 (g8.) fis16 e8 \tuplet 3/2 {fis16 (g a)} |
    \grace a32 (g8.) fis16 \grace fis32 e8. d16 | %
    \break
    d4 (cis16) a32 b cis d e fis |
    g8. e32 fis g8. a16 |
    \grace g32 (fis8.) e16 d8 a |
    \tuplet 3/2 {b16 (cis d} \tuplet 3/2 {e fis g)} fis16 d a, cis' |
  }
  \alternative {
    {d,16 e32 fis g a b cis d8}
    {d,16 e32 fis g a b cis d8 r8}
  }
  \bar "|."
}

scoreACelloII = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  r8
  d, r fis r |
  g8 r d r |
  e cis' d d, |
  a'8 a a r |
  d8 r cis r |
  b8 r fis gis |
  a d, e e |
  a8 a' a,
}
  \repeat volta 2 {
  \partial 8
  r8
  fis r dis r
  e8 fis g dis |
  e fis g gis |
  a8 a a r |
  e4 b'8 cis |
  d8 d,16 e fis8 fis |
  g4 a8 a |
}
\alternative {
{  d,4 r8}
{  d4 r}
}
}

scoreACelloIPart = \new Staff \with {
  instrumentName = "Vcl. Solo"
} { \clef bass \scoreACelloI }

scoreACelloIIPart = \new Staff \with {
  instrumentName = "Vcl."
} { \clef bass \scoreACelloII }

\score {
  <<
    \scoreACelloIPart
    \scoreACelloIIPart
  >>
  \layout { }
}
