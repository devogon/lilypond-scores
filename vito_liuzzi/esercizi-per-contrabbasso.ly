\version "2.19.83"
\language "nederlands"

%\include "/home/ack/src/openlilylib/notation-fonts/package.ily"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Esercizi per Contrabbasso"
  instrument = "Contrabas"
  composer = "vito_liuzzi"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  %print-all-headers = ##t
  #(set-paper-size "a4")
  indent = 0.0\cm
%   #define fonts
%   (set-global-fonts
%   #:roman "Sunset Boulevard"
%   ))
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
%  \tempo 4=100
  \set Score.markFormatter = #format-mark-box-numbers

}

contrabass = \relative c {
  \global
  % Music follows here.
  \set countPercentRepeats = ##t
 % \override TextScript.extra-offset = #'(2 . 4)
 % s1-\markup{\pad-markup #0.7 "Facilmente eseguibili a memoria - Gino  Mucci"}
 % \break
  %
% s1-\markup{\pad-markup #-2.9 \column {\line{sono degli esercizi insignificanti ma molto utili per sciogliere} \line{la mano sinistra ed ancora piu pacronanza dell arco}}}
  \break
  \mark \markup{\bold I}
  \repeat percent 2 {g'16\downbow (gis a gis bes a gis a)} |
  \repeat percent 2 {g\downbow (a bes a b! bes a bes)} |
  \repeat percent 2 {g16\downbow (ais b ais c b ais b)} |
  \repeat percent 2 {g\downbow (b c b des c b c)} |
  \break
  \repeat percent 2 {g16 ( c des c d! des c des)} |
  \repeat percent 2 {g,16 (c d c es d c d)} |
  \repeat percent 2 {g,16 (d' es d e! es d es)} |
  \repeat percent 2 {g,16 (dis' e dis f e dis e)} |
  \break
  \repeat percent 2 {g,16 (e' f e ges f e f)} |
  \repeat percent 2 {g,16 (g' ges g a! as g as)} |
  \repeat percent 2 {g,16 (f' g f as g f g)} |
  \repeat percent 2 {g,16 (g' as g a! as g as)} |
  \break
  \repeat percent 12 {g,16 (fis' g fis as g f g)} |
  g,16 d bes g bes d g bes e g d bes d8 g |
  \break
%  s1-\markup{eseguire la stessa progressione in II carda sempre piu ve?ice}
  % see https://www.vitoliuzzi.com/brief-exercises-to-become-a-virtuoso/
  \break
  \mark \markup{\bold II}

}

due = \relative c {
  \global
  % Music follows here.
  \set countPercentRepeats = ##t
 \bar "[|:"
  \repeat volta 2 {
    \override TextScript.extra-offset = #'(0 . 0)
    c8^\markup{ {nei ritornellare sempre più veloce}}_\markup{Farlo anche pizz.} d e f g a b c |
    d c b a g f e d |
    e f g a b c d e |
    f e d c b a g f |
    g a b c d e f g |
    \break
    a g f e d c b a |
    b c d e f g a b |
    \clef treble
    c b a g f e d c |
    \clef bass
    b a g f e d c b |
  }
  \bar ":|]"
  \repeat percent 2 {c1\fermata}
  \break
  c8^\markup{1}^\markup{Sono boche arcate ma le piu proficue} (d) e f g (a) b\staccato c\staccato |
  \repeat percent 2 {d (c) b\staccato a\staccato } |
  \repeat percent 2 {c,8^\markup{2} (d) d (e) e (f) f (g) g (a) a (b) b (c) c (d)} |
  \repeat percent 2 {c,8^\markup{3} d e\staccato f\staccato g\staccato a\staccato b\staccato c\staccato d\staccato e\staccato s s }|
  \repeat percent 2 {c,\staccato^\markup{4} d\staccato e\staccato f\staccato} |
  \repeat percent 2 {c8.^\markup{5} (c16\staccato) d8. (d16\staccato) e8. (e16\staccato) f8. (f16\staccato) } |
  \repeat percent 2 {c8.^\markup{6} (d16\staccato) e8. (f16\staccato) g8. (a16\staccato) b8.  (c16\staccato)} |
  \repeat percent 2 {c,8.^\markup{7} c16 (d8.) d16 (e8.) f16 (g8.) s16} |
  \repeat percent 2 {c,8.^\markup{8} d16 (e8.) f16  (g8) s4.}|
  \bar ".."
  \break
}
tre = \relative c {
  \global
  % Music follows here.
  \set countPercentRepeats = ##t
%  \override TextScript.extra-offset = #'(2 . 4)
  \mark #9
  \tuplet 3/2 {c8.\downbow (c16\staccato) c8\upbow} \tuplet 3/2 {d8.\downbow (d16\staccato) d8\upbow} \tuplet 3/2 {e8.\downbow (e16\staccato) e8\upbow-\markup{ecc.}} s8 s|
  \mark \default
  \tuplet 3/2 {c8. (d16\staccato) e8\staccato} \tuplet 3/2 {f8. (g16\staccato) a8\staccato} \tuplet 3/2 {b8. (c16\staccato) d8\staccato} \tuplet 3/2 {c8. (b16\staccato) a8\staccato} |
  \tuplet 3/2 {c,8. (d16\staccato) e8\staccato}
    \tuplet 3/2 {f8. (g16\staccato) a8\staccato}
    \tuplet 3/2 {b8. (c16\staccato) d8\staccato}
    \tuplet 3/2 {e8. (f16\staccato) g8\staccato}
    \tuplet 3/2 {a8. (b16\staccato) c8\staccato}
    \tuplet 3/2 {b8. (a16\staccato) g8\staccato}
    \tuplet 3/2 {f8. (e16\staccato) d8\staccato}
    \tuplet 3/2 {c8. (b16\staccato) a8\staccato}
    \tuplet 3/2 {g8. (f16\staccato) e8\staccato}
    \tuplet 3/2 {d8. (c16\staccato) b8\staccato}
  c2\fermata
  \bar "||"
  \break
  c8\downbow^\markup{Picchelato con note riballute nella Scala di Do  (Esercizio di tutti i giorni)}  c16\staccato\upbow \repeat unfold 13 {c\staccato}
  c8\downbow c16\staccato\upbow \repeat unfold 13 {c\staccato}
  c8\downbow c16\staccato\upbow \repeat unfold 13 {c\staccato}
  \bar ""
  \break
  c8\downbow^\markup{anche a sestine ben misurate} c16\staccato\upbow \repeat unfold 13 {c\staccato}
  g1^\markup{Ottenute il bicchettato con quella sciollezza e resistenza, esercitarsi nel}

}

\markup{
  \column {
    \line {
     \wordwrap {Facilmente eseguibili a memoria - Gino  Mucci
      }
    }
    \vspace #1
    \line {
      \wordwrap {
        sono degli esercizi insignificanti ma molto utili per sciogliere la mano sinistra ed ancora piu pacronanza dell arco}
        }
        \vspace #1
    }
}


\score {
  \new Staff { \clef bass \contrabass }
  \layout {
    \context {

    }
  }
  \header {
    piece = \markup{\huge \bold "I"}
  }
}

\markup{
  \column {
    \line {
        eseguire la stessa progressione in II carda sempre piu ve?ice
    }
    \vspace #1
  }
}

\score {
  \new Staff { \clef bass \due }
  \layout {
    \context {  }
  }
  \header {
    piece = \markup{\huge \bold "II"}
  }
}

\pageBreak

\markup{
  \column{
    \line{
      \wordwrap {Colui che vorra approfondirsi nelle arcate : 1.2.3.4.5.6.7.8 = come i Contrabbassisti sanno, si trovano ambiamente compilate nel I e II esercizio degli studi del Kreutzez = raccolta Negri - cosi il ritmo 9 e 10 che segue, si adatta di I esercizio della raccolta Negri, ma per esercitarsi a memoria é sufficiente la siccola progressione II, come nella semplice Scala di Do}
        }
         \vspace #1
      %\line {.}
     }
}

\score {
  \new Staff { \clef bass \tre }
  \layout {
    \context { \Staff }
  }
    \header {
    piece = \markup{\huge \bold "III"}
  }
}

%