\version "2.19.55"
\language "nederlands"

\header {
  title = "Esercizi Giornalieri"
  opus = \markup{\huge \box "B"}
  instrument = "Contrabas"
  composer = "Vito Liuzzi"
  meter = "48/100"
  % Remove default LilyPond tagline
  tagline = ##f
  copyright = "Vito D. Liuzzi"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
  ragged-last = ##t
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key g \major
  \time 4/4
  \tempo "48 or 100" 4=100
}

contrabass = \relative c {
  \global
  % Music follows here.
  g8\downbow-3 a16\upbow (b) c d-0 e fis g-0 fis e d c b a g | % 1
  a8-0 b16-1 (c) d-0 e fis g a-1 g fis e d c b a | % 2
  \break
  b8 c16 (d) e fis g-0 a-1 b-4 a g fis e d c b | % 3
  c8-3 d16 (e) fis g a-1 b-3 c-4 b a g fis e d c | % 4
  \break
  d8-0 e16 (fis) g-1 a b c-1 d c b a g fis e d | % 5
  e8-1 fis16 (g) a-1 b-3 c-4 d-1 e d c b a g fis e | % 6
  \break
  fis8-4 g16-0 (a-1) b c-1 d e-1 fis e d c b a g fis | % 7
  g8-0 a16-1 (b-4) c-1 d e-1 fis g-4-0 fis-4 e d c b a g | % 8
  \break
  a8-1 b16-4 (c-1) d e-1 fis g-+ a-1 g fis e d c b a | % 9
  \clef tenor
  b8-1 c16-1 (d-4) e-1 fis g-+ a-1 b-3 a g fis e d c b | % 10
  \break
  c8-1 d16-4 (e-1) fis g-+_\markup{diat.} a-1 b-2 c-3 b a g fis e d c-1 | % 11
  d8-4 e16-1 (fis) g-+_\markup{s.cr.} a-1 b-3 c-1_\markup{cr.} d-3 c b a g fis e d | % 12
  \break
  e8-1 fis16-4 (g-+_\markup{diat.}) \clef treble a-1 b-2 c-3 d-+_\markup{s.cr.} e-1 d c b a g fis e | % 13
  fis8-4 g16-+_\markup{diat.} (a-1) b-2 c-3 d-+_\markup{s. cr.} e-1 fis-3 e d c b a g fis | % 14
  \break
  g8-+_\markup{diat.} a16-1 (b-2) c-3 d-+_\markup{diat.} e-1 fis-2 g-3 fis e d c b a g | % 15
  fis8^\markup{idem nella fase discendente} g16 (a) b c d e fis e d c b a g fis | % 16
  \break
  e8-1 fis16 (g) a b c d-+ e-1 d c b a g fis e | % 17
  d8-1 e16-1 (fis) g-+ a-1 b-3 c-1 d-3 c b a g fis-4 e d-4 | % 18
  \break % 19 20
  c8-1 d16-4 (e) fis g a b c b a g fis e d c | % 19
  \clef tenor
  b8-1 c16-1 (d-4) e fis g a b a g fis e d c b | % 20
  \break % 21 22
  a8-1 b16-4 c d e fis g a-1 g fis e d c b a | % 21
  g8-0 a16 b c d e fis-4 g-0-4 fis e d c b a g | % 22
  \break
  \clef bass
  fis8_\markup{eccetera} g16 a b c d e fis e d c b a g fis  | % 23
  e8 fis16 g a b c d e d c b a g fis e | % 24
  \break
  d8 e16 fis g a b c d c b a g fis e d | % 25
  c8 d16 e fis g a b c b a g fis e d c | % 26
  \break
  b8 c16 d e fis g a b a g fis e d c b | % 27
  a8 b16 c d e fis g a g fis e d c b a | % 28
  \break
  g8 a16 (b) c d e fis g fis e d c b a g | % 29
  fis8 g16 (a) b c d e fis e d c b a g fis | % 30
  \break
  g1\downbow
  \bar ":|."
}

\score {
  \new Staff \with {
 %   instrumentName = "Contrabass"
 %   shortInstrumentName = "Cb."
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \layout { }
  \midi { }
}

% \markup \column {
% \line {\teeny  "https://www.vitoliuzzi.com/technique-for-double-bass-the-double-stops/" }
% \line {\teeny "A very useful exercise in three octaves (also a good warm up)" }
% \line {\teeny "first two images here above"}
% \hspace #1
%  }

\markup {
  \teeny
  \date
}