\version "2.19.83"
\language "nederlands"

\header {
  title = "Bad Company"
  subtitle = "(Bad Company - 1999)"
  instrument = "Bass"
  composer = "Kirke, Rodgers"
  arranger = "Boz Burrell (bass)"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key ges \major
  \time 4/4
  \tempo 4=114
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  r1
  es1:m

}

electricBass = \relative c, {
  \global
  % Music follows here.
  r1
  r1
  ges''8 (f ges2. ~ ges2) ges8 (f) r ges (
  as1 ) |
  bes2 \tuplet 3/2 {bes8 bes bes} \tuplet 3/2 {bes bes bes} |
  \break
  \tuplet 3/2 {bes bes bes} \tuplet 3/2 {bes bes bes} \tuplet 4/4 {bes16 bes bes bes} \tuplet 4/4 {bes bes bes bes ~}
  bes1 |
  r1
  \bar "||"
  r1^\markup{VERSE}
  r2. r8 des, ( |
  \break
  es2. es8) ges\glissando |
  f\glissando ges\glissando f\glissando ges\glissando f\glissando ges as8 r8 |
  r2. r8 des,8\glissando |
  es1 |
  r1
  \break
  <bes es>1 |
  r2 bes,4 des |
  \repeat volta 2 {
  es1^\markup{VERSE} ~ |
  es2 bes4 des |
  es1 ~ |
  es2 bes4 des |
  }
  es1 ~ |
  \break
  es2 es4\glissando ges, | % 24
  \bar "||"
  des'2^\markup{CHORUS} r8 f, ges r |
  as8 as4. r2 |
  es'8 es4. ~ es4 bes8 des |
  es8 es4. es8 es4.\glissando |
  ges,2 ges8 ges4 ges8 |
  \break
  ges8 ges4. ~ ges8 c des d! | % 30
  es8 es4. ~ es8 bes des bes |
  es8 es4. r16 bes bes8 des bes |
  es8 es4. r16 bes bes8 des bes |
  es8 es4. bes4 des |
  \break
  es1 ~ |
  es2 bes4 des |
  es1 ~ |
  es2 bes4 des |
  \repeat volta 2 {
      es1^\markup{VERSE} ~ |
      es2 bes4 des |
      es1 ~ |
      \break
      es2 bes4 des |
  }
  es1 ~ | % 42
  es4 es4^\markup{cross head!} bes' bes |
  \repeat volta 2 {
    es,1 ~ |
    es2 bes'4 des |
    es,1  |
  }
  \alternative {
    { es2 bes'4 des }
  %  \break
    {\break es,2\glissando bes'4 des\glissando }
  }
  es,1 ~ |
  es2 es4\glissando ges, |
  \bar "||"
  des'8^\markup{CHORUS} des4. r16 f, f8 ges g! |
  as8 as4. ~ as8 c des d! |
  \break
  es8 es4. ~ es8 bes des r | % 54
  es8 es4. es4 es\glissando |
  ges,8 ges4. ges8 ges4 ges8 |
  as8 as4. ~ as8 c des d! |
  es8 es4. ~ es8 bes des bes |
  \break
  es8 es4. r16 bes bes8 des bes | % 59
  es8 es4. r8 bes des bes |
  es8 es4. bes4 des |
  \bar "||"
  es1^\markup{INTERLUDE} |
  es8 des es2. |
  ges'4. f8 ~ f4 des ~ |
  \break
  des8 as4. ges'4 f\glissando | % 65
  es,2 r16 bes' bes8 des bes |
  es,8 es4. r16 bes bes8 des bes |
  es8 es4. r16 bes bes8 des bes |
  es8 es4. bes4 des |
  es1 |
  \break
  es8 des des2. | % 72
  ges'4. f8 ~ f f des4 |
  des8 as4. ges'4 f\glissando |
  es,2 r16 bes bes8 des bes |
  es8 es4. r16 bes bes8 des bes |
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
