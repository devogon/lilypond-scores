\version "2.19.57"
\language "nederlands"

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

\header {
  title = "When the Levee Breaks"
  instrument = "Bass"
  composer = "Led Zeppelin"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key bes \major
  \time 4/4
  \tempo 4=140
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

electricBass = \relative c, {
  \global
  % Music follows here.
  R1*4 | % 1
  \set Score.currentBarNumber = #3
  as'8 f f4 f2 | % 2
  as8 f4 as8 f4 bes | %  3
  as8 f f4 f f | % 4
  \break
  as8 f4 as8 f f bes4 | %  5
  as8 f f4 f f | % 6
  as8 f4 as8 f8 f bes4 | %  7
  as8 f f4 f f | % 8
  \break
  as8 f4 as8 f f bes4 | % 9
  as8 f f4 f es8 e! | % 10
  f4 f f e! | % 11
  f8 f f4 f f | % 12
  \break
  as8 f4 as8 f f bes4 | % 13
  as8 f f4 f es8 e! | % 14
  f4 f8 f f4 e! | % 15
  f8 f f4 f f  | % 16
  \break
  as8 f4 as8 f f bes4 | % 17
  as8 f f4 f es8 e! | % 18
  f4 f8 f f4 e! | % 19
  f8 f f4 f f | % 20
  \break %% renumber measures from here!
  as8 f4 as8 f f bes4 | % 22
  as8 f f4 f f | % 23
  as8 f4 as8 f f b4 | % 24
  as8 f f4 f es8 e! | % 25
  \break
  f8 f f f f4 e! | % 26
  f8 f f4 f f | % 27
  as8 f4 as8 f f bes4 | % 28
  as8 f f4 f es8 e! | % 29
  \break
  f4 f f e!8 f  | % 30
  f8 f f4 f f | %  31
  as8 f4 as8 f f bes4 | % 32
  as8 f f4 f es8 e! | % 33
  \break
  f8 f f f f4 e! | % 34
  f8 f f4 f f | % 35
  as8 f4 as8 f f b4 | % 36
  as8 f f4 f f | % 37
  \break
  as8 f4 as8 f f bes a | % 38
  as,8 as as as as r16 as16 r4 | % 39
  bes8 bes bes c c c4 es8 | % 40
  es8 as4 es8 f4 | % 41
}

\score {
  \new Staff \with {
  %  instrumentName = "Electric bass"
  %  shortInstrumentName = "E.Bs."
  } { \clef "bass_8" \electricBass }
  \layout { }
}

\markup \column {
 \line { \typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime }
 \line { \typewriter \teeny "git commit: " \typewriter \teeny \gitCommit }
 \hspace #1
}
\markup {
% \line { \teeny \gitRevision }
% \line { \teeny \gitCommit}
\teeny \date
}