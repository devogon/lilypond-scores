\version "2.21.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Dazed and Confused"
  subtitle = "(Led Zeppelin - 1969 - Led Zeppelin)"
  instrument = "Bass"
  poet = "Jake Holmes"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key g \major
  \time 12/8
  \tempo 4=158
  \omit Voice.StringNumber
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  \partial 8 e,8 |
  \bar "||"
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  \bar "||"
  \break
  \mark \markup {Verse}
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  d'4.\3 cis4.\3 c!4.\3 b8\3 r e, | %
  g'4.\3 fis4.\3 f!4.\3 e8\3 r e, | %
  \time 9/8
  d'4.\3 cis4.\3 c!4.\3 | %
  \bar "||"
  \break
  \time 6/8
  b8\3\f b b e16 fis a b e, fis | %
  b,8\3 b b e16 fis a b e, fis | %
  b,8\3 b b e16 fis a b e, fis | %
  b,8\3 b b e16 fis a b e, fis | %
  \time 12/8
  \bar "||"
  \break
  e,4. g'4.\3 fis4.\3 f!4.\3 |
  e8\3 r e, d'4.\3 cis4.\3 c4.\3 |
  b8 r e, g'4.\3 fis4.\3 f!4.\3 |
  e8\3 r e, d'4.\3 cis4.\3 c4.\3 |
  \bar "||"
  \break
  \mark \markup {Verse}
  b8\3 r e, g'4.\3 fis4.\3 f!4.\3 |
  e8\3 r e, d'4.\3 cis4.\3 c!4.\3 |
  b8\3 r e, g'4.\3 fis4.\3 f!4.\3 |
  e8\3 r e, d'4.\3 cis4.\3 c!4.\3 |
  \time 6/8
  \bar "||"
  b8\3\f b b e16 fis a b e, fis | %
  b,8\3 b b e16 fis a b e, fis | %
  b,8\3 b b e16 fis a b e, fis | %
  b,8\3 b b e16 fis a b e, fis | %
  \bar "||"
  \break
  \mark \markup{Interlude}
  e16 e e e d8 r4. |
  g16\2 g\2 g\2 g\2 e8\staccato\3 r4. |
  a16\2 a\2 a\2 a\2 e8\staccato\3 r4. |
  g16\2 g\2 g\2 g\2 e8\staccato\3 r4. |
  e16\3 e\3 e\3 e\3 d8\3 r4. |
  \break % pg 18 in the book second line

}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
