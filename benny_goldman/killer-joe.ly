\version "2.19.83"
\language "nederlands"

\header {
  title = "Killer Joe"
 % instrument = "Bass"
  composer = "Benny  Golson"
  subsubtitle = "(bass) Addison Farmer"
  meter = "Medium Swing"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=110
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  c1
  bes1:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  e1:m7.5-
  a1:7
  es1:m7
  as1:7
  a1:7
  as1:7
  e:m7
  a1:7
  c1:7
  bes:7
  c1:7
  bes:7
  c1:7
  bes1:7
  c1:7
  bes1:7
  c1:7
}

contrabass = \relative c {
  \global
  % Music follows here.
  c'4 g c b | % 1
  bes f bes, b % 2
  c g c c' | % 3
  bes f d8. g16 b,!4 | %  4
  \break
  c d e g | % 5
  bes f bes d | % 6
 % \break
  c8. g16 e4 f g | % 7
  f8 f d4 bes b! | % 8
  \break
  c4 e g e | % 9
  f4 d bes g  % 10
  c8 g' \tuplet 3/2 {e' f fis} \tuplet 3/2 {g e c} \tuplet 3/2 {g e c} | % 11
%  \break
  bes4 f' bes, d | % 12
  \break
  c e bes'8. g16 a4 | % 13
  bes,4 f' as8. f16 g4 | % 14
  \tuplet 3/2 {c8 bes g} \tuplet 3/2 {e c g} e4 f | % 15
  g4 as bes c | % 16
  \break
  \mark \markup{\box B}
  e g bes d | % 17
%  \break
  cis4 g a e | % 18
  es f ges g | % 19
  as ges es c | % 20
  \break
  a e' b' a | % 21
  as ges f es | % 22
  e fis g b! | % 23
%  \break
  \tuplet 3/2 {a8 a e} cis4 a b! | % 24
  \break
  \mark \markup{\box C}
  c8 g' e dis e g \tuplet 3/2 {d'!8 bes g} |
  bes,4 g bes d |
  \tuplet 3/2 {c4 e,8 ~} e4 f g |
  f bes d f |
 % \break
  \break
  e c g' e' | % 29
  f d bes' as |
  \tuplet 3/2 {g8 e c} \tuplet 3/2 {bes g e} \tuplet 3/2 {c bes g} e4 |
  f4 g as bes |
  %\break
  c4 r r2
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

contrabassPart = \new Staff { \clef bass \contrabass }

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}
