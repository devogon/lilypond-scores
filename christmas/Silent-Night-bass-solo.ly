\version "2.21.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Silent Night"
  subtitle = "(bass solo)"
  arranger = "Mark from TalkingBss"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key a \major
  \numericTimeSignature
  \time 3/4
  \tempo 4=78
  \omit Voice.StringNumber
}


electricBass = \relative c, {
  \global
  % Music follows here.
  <a\3 a' cis>4 a8 a'\2 cis a\2 | % 1
  <a,\3 a' cis>4 a8 a'\2 cis a\2 | % 2
%  \bar "||"
  \break
  \repeat volta 2 {
    <a, cis' e>4 a8 fis''\1 e\1 a,, | % 3
    <a\3 a' cis>4 a8 a'\2 cis a\2 | % 4
        <a, cis' e>4 a8 fis''\1 e\1 a,, | % 5
    <a\3 a' cis>4 a8 a'\2 cis4 | % 6
    \break
    <e,,\4 gis'' b>4 e8\4 gis''\2 b4\1 | % 7
    <e,,,\4 e''\2 gis\1>4 e8\4 e''\2 gis\1 e\2 |% 8
    <a,,\3 cis'\2 a'\1>4 a8\3 cis'\2 a'\1 a,,\3 |% 9
    <a cis' e>4 a8 cis'\2 a,4 | %  FIXME 10
    \break
    <d\2 a' fis'>4 a'8\3 d,\2 fis'\1 d,\2 |% 11
    % <16 0 14> <14 0 13> <12 0 11> 3-2-1
    <d\2 cis' a'>4 <d\2 b'\3 gis'\1> <d\2 a'\3 fis'\1> | % 12 (and 16)
    <a cis' e>4 a8 fis''\1 e\1 a,, | % 13
    <a\3 a' cis>4 a8 a'\2 cis a\2 |  % 14
    \break
    <d,\2 a' fis'>4 a'8\3 d,\2 fis'\1 d,\2 | % 15
    <d\2 cis' a'>4 <d\2 b'\3 gis'\1> <d\2 a'\3 fis'\1> | % 16
    <a cis' e>4 a8 fis''\1 e\1 a,, | % 17
    <a\3 a' cis>4 a8 a'\2 cis a\2 | % 18
    \break
    <e,\4 gis'' b>4 e8\4 gis''\2 b\1 gis\2 | % 19
    <e,,\4 gis''\2 d'\1>4 b'''8 gis\2 <e,,	\4 e'' gis>4| % 20
    <a\3 cis'\2 a'\1>4 a8\3 fis''\1 e\1 a,,\3 |% 21
    <a\3 a''\2 cis>4 a8\3 a''\2 cis4 |% 22
    \break
    <a,,\3 cis'\2 a'\1>4 a8\3^\markup{here} e''\1 cis\2 a,\3 |% 23
     <e'\3 gis\2 e'\1>4 e8\3 d'\1 b\2 a,\3 |  % 24
     <a\3 a' cis>4 a8 a'\2 cis a\2 | % 25
      <a,\3 a' cis>4 a8 a'\2 cis a\2 | % 26
  }
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout { }
}
