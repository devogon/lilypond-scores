\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Jingle Bells 2"
%  meter = "Lively"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo "Lively" 4=110
}

violin = \relative c'' {
  \global
  % Music follows here.
  r1
  r2 r4 r8 d,8\mf\upbow
  d8 b' a g d4\downbow r8 d16 d |
  d8 b' a g e4 r8 e\upbow |
  \break
  e8 c' b a fis4 r |
  d'8\downbow d c a b4 r8 d,\upbow |
  d8\downbow b' a g d4 r8 d |
  d8 b' a g e4 r8 e |
  \break
  e8 c' b a d d d d |
  e8 d c a g4 r |
  b8\f\downbow b b4 b8 b b4  |
  b8 d g,8. (a16) b4\upbow r |
  \break
  c8\downbow c c8. ~ c16\staccato c8 b b b16 b |
  b8 a a b a4-> d-> |
  b8 b b4 b8 b b4 |
  b8 d g,8. (a16\staccato) b4. r8 |
  \break
  c8 c c8. ~ c16\staccato c8 b b b16 b |
  d8 d c a g4. % d'8 |
  \bar "|."
}

flute = \relative c'' {
  \global
  % Music follows here.
  r1
  r2 r4 r4
  b,8\downbow b b b b4 r8 b16 b |
  b8 b b b c4 r8 c8\upbow |
  c8 c c c c4 r |
  fis8 fis fis fis g4 r8 d |
  d8 d d d d4 r8 d |
  d8 d d d c4 r8 c |
  \break
  c8 e e e fis fis fis fis |
  fis8 fis fis fis d4 r |
  g8 g g4 g8 g g4 |
  d4 e g r |
  \break
  e8 e e4\staccato e8 d d d16 d |
  cis8 cis cis cis d4-> d-> |
  g8 g g4 g8 g g4 |
  d4 e g4. r8 |
  \break
  e8 e e4 e8 d d d16 d |
  fis8 fis fis fis b,4. % r8 |
}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

flutePart = \new Staff \with {
  instrumentName = "Flute"
} \flute

\score {
  <<
    \violinPart
 %   \flutePart
  >>
  \layout { }
}
