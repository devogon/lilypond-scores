\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Jingle Bells"
  instrument = "flute, violin"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=100
}

flute = \relative c'' {
  \global
  % Music follows here.
  b4 b b2
  b4 b b2
  b4 d g, a
  b1
  \break
  c4 c c c
  c4 b b b8 b
  b4 a a b
  a2 d
  \break
  b4 b b2
  b4 b b2
  b4 d g, a
  b2. b4
  \break
  c4 c c c
  c4 b b b8 b
  d4 d c a
  g1
  \bar "|."
}

violin = \relative c'' {
  \global
  % Music follows here.
  g4 g g2
  g4 g g2
  g4 g g fis
  g4 a b2
  a4 a fis fis
  a4 g g g
  g2 g
  fis4 a c a
  g4 g g2
  g4 g g2
  g4 g g fis
  g4 a b g
  g4 g a a
  a4 g g g8 g
  b4 g fis fis
  g1
}

flutePart = \new Staff \with {
  instrumentName = "Flute"
  shortInstrumentName = "Fl."
} \flute

violinPart = \new Staff \with {
  instrumentName = "Violin"
  shortInstrumentName = "Vl."
} \violin

\score {
  <<
    \flutePart
    \violinPart
  >>
  \layout { }
}

\markup  \column {
%\markuplines {
  \line {Jingle bells, jingle bells, jingle all the way,}
  \line {o what fun it is to ride in a one-horse open sleigh!}
  \line {Jingle bells, jingle bells, jingle all the way,}
  \line {o what fun it is to ride in a one-horse open sleigh!}
%}
}