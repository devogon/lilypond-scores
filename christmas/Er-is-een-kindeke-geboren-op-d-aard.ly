\version "2.19.82"
\language "nederlands"

\header {
  title = "Er is een kindeke geboren op d‘aard"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \numericTimeSignature
  \time 3/4
  \tempo 4=105
}

violin = \relative c'' {
  \global
  % Music follows here.
  \partial 4 g4
  \repeat volta 2 {
  b2 a4
  g8 g g4 b
  d4 d d
  }
  \alternative {
  {d2 g,4}

  %\break
%   b2 a4
%   g8 g g4 b
%   d4 d d
  {d'2.}
  }
  \break
  c4 e c
  b d b
  a b c
  d2.
  \break
  c4 e c
  b d b
  a b a
  g2
  \bar "|."
}

flute = \relative c'' {
  \global
  % Music follows here.
  g4
  \repeat volta 2 {
  g2 fis4
  g8 g g4 g
  b4 b c
  }
  \alternative {
    {b2 g4}
    {b2.}
  }
 %
%   g2 fis4
%   g8 g g4 g
%   b4 b c
  g4 c g
  g b g
  fis g a
  b a g
  g c g
  g b g
  fis g fis g2
}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

flutePart = \new Staff \with {
  instrumentName = "Flute"
} \flute

\score {
  <<
    \violinPart
    \flutePart
  >>
  \layout { }
}
