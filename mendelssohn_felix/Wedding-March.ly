\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Wedding March"
  instrument = "Violoncello"
  composer = "Felix Mendelssohn"
  arranger = "Wand Sobieska"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent=0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \partial 4
  \tempo "allegro vivace" 4=60
}

cello = \relative c {
  \global
  % Music follows here.
  \tuplet 3/2 {c'8\f c c} |
  c4 r r \tuplet 3/2 {c8 c c} |
  c4 r r \tuplet 3/2 {c8 c c} |
  c4 \tuplet 3/2 {c8 c c} c4 \tuplet 3/2 {c8 c c} |
  c4 \tuplet 3/2 {c8 c c} c4 \tuplet 3/2 {c8 c c} |
  a,2\p b4. b8 |
  \break
  e2 f4\upbow f\downbow |
  g2\upbow g4 g |
  c,4 r r2 |
  a2 b4. b8 |
  e2 f4\upbow f\downbow |
  g2\upbow g4 g, |
  g2 (c4)
  \repeat volta 2 {
  \tuplet 3/2 {c'8 c c} |
  \break
  c4 r r \tuplet 3/2 {c8 c c} |
  c4 r r \tuplet 3/2 {c8 c c} |
  c4 \tuplet 3/2 {c8 c c} c4 \tuplet 3/2 {c8 c c} |
  c4 \tuplet 3/2 {c8 c c} c4 \tuplet 3/2 {c8 c c} |
  a,2 b4. b8 |
  e2 f4\upbow f\downbow |
  \break
  g2\upbow g4 g |
  c,4 r r2 |
  a2 b4. b8 |
  e2 f4\upbow f\downbow |
  g2\upbow g4 g, |
  g2 (c4) r\breathe |
  c2\mp e4. c8 |
  g2. g4 |
  \break
   c4 c e c | % 27
   g2. g'4 |
   c4 g e c |
   f,2. f4 |
   f4 f' fis fis, |
   g4 r r2 |
   a2 b4. b8 |
   e2 f4\upbow f\downbow |
   \break
   g2\upbow g4 g | %  35
   c,4 r r2 |
   a2 b4. b8 |
   e2 f4\upbow f\downbow |
   g2\upbow g4 g, |
   g2 (c4) r |
   c2 e4. c8 |
   g2. g4 |
   \break
   c4 c e c | % 43
   g2. g'4 c4 g e c |
   f,2. f4 |
   f4 f' fis fis, |
   g4 r r2 |
   a2 b4. b8 |
   \break
   e2 f4\upbow f\downbow | % 50
   g2\upbow g4 g |
   c,4 r r2 |
   a2 b4. b8 |
   e2 f4\upbow f\downbow |
   g2\upbow g4 g, |
   g2 (c4)
  % \bar "|."
  }
}

\score {
  \new Staff { \clef bass \cello }
  \layout { }
}
