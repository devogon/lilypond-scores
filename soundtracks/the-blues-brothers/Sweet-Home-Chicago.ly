\version "2.19.54"
\language "nederlands"

\header {
  title = "Sweet Home Chicago"
  subtitle = "The Blues Brothers"
  instrument = "Bass"
  arranger = "Bass by Donald Duck Dunn"
  % Remove default LilyPond tagline
  copyright = \markup \column {
    \teeny { \line {Transcribed by Ian 'Dodge' Paterson. 2010. Check www.dodgebass.co.uk for more funk, soul, blues & jazz transcriptions. Thanks to William for corrections.}
           \line{ For educational and personal use only - not to be sold but please share! If you find it useful why not give a small donation to a charity of your choice?}
  }}
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key e \major
  \time 4/4
  \tempo "Blues shuffle" 4=60 % \markup { \concat {
  % ( \note #"8 8" #1
  %  " = "
  %  \note #"4" #1
  %  )
  %  }
  % display as "blues shuffle" 4 = 60 (8 8 = \tuplet 3/2 4 8)
  }

bass = \relative c, {
  \global
  % Music follows here.
  R1
  R1
  R1
  r8 a bes b! (b2) | %4
  e8 e cis cis b b gis b | % 5
  a8 a fis' fis e e cis b | % 6
  e,4 cis'8 cis b b gis4 | % 7
  e4 cis'8 cis b b gis b | % 8
  a4 fis'8 fis e e cis cis | % 9
  a4 fis' e cis | % 10
  e,4 cis'8 cis b b \tuplet 3/2 {gis8 b cis} | % 11
  e,4 cis'8 cis b b \tuplet 3/2 {gis8 b cis} | % 12
  b4 \grace {fis'16} gis4 fis dis8 b | % 13
  a4 \grace {e'16} fis4 e \tuplet 3/2 {cis8 b gis} | % 14
  e8 e cis' cis b b gis gis | % 15
  e8 a bes b! (b) b \tuplet 3/2 {cis8 b gis} | % 16
  e4 cis'8 cis b4 \tuplet 3/2 {gis8 b cis} | % 17
  a4 fis' e cis8 b | % 18
  e,4 cis'8 cis b4 gis8 b | % 19
  e,4 cis' b gis8 b | % 20
  a4 fis'8 fis e e cis cis | % 21
  a8 a fis'4 e cis8 b | % 22
  e,4 cis'8 cis b4 \tuplet 3/2 {gis8 b cis} | % 23
  e,4 cis'8 cis b4 \tuplet 3/2 {gis8 b cis} | % 24
  b4 \grace {fis'16} gis4 fis dis | % 25
  a4 \grace{e'16} fis4 e \tuplet 3/2 {cis8 e fis} | % 26
  e,4 cis'8 cis b4 \tuplet 3/2 {gis8 b cis} | % 27
  e,8 a bes b! (b) b \tuplet 3/2 {cis8 b gis} | % 28
  e8-> r r4 r2 | % 29
  e8-> r r4 r2 | % 30
  e8-> r r4 r2 | % 31
  a8 e fis4 g! gis | % 32
  a4 fis'8 fis e4 fis8 e | % 33
  g!4 fis e cis8 b | % 34
  e,4 cis'8 cis b4 cis8 cis | % 35
  e,4 cis'8 cis b4 cis8 cis | % 36
  b4 \grace{cis16} dis4 fis dis8 b | % 37
  a4 fis' e \tuplet 3/2 {cis8 e fis} | % 38
  e4 \grace {fis16} gis4 b \tuplet 3/2 {gis8 b cis} | % 39
  e,4 \grace{b'16} cis4 b gis8 gis | % 40
  \bar "||"
  e8 e d!4 cis b | % 41
  a4 cis d! dis | % 42
  e4 \grace {fis16} gis4 b gis8 fis | % 43
  e4 \grace {b'16} cis4 b \tuplet 3/2 {gis8 (fis) e} | % 44
  a,4 cis e cis8 cis | % 45
  a4 cis d! dis | % 46
  e \grace{fis16} gis4 b gis8 fis | % 47
  e4 \grace{b'16} cis4 b gis | % 48
  b,4 dis b bes | % 49
  a4 cis d! \tuplet 3/2 {cis8 e fis} | % 50
  e,4 cis' b cis | % 51
  e,8 a bes b! (b) b \tuplet 3/2 {cis8 b gis} | % 52
}

\score {
  \new Staff { \clef "bass_8" \bass }
  \layout { }
}

\markup {
  \teeny
  \date
}