\version "2.19.81"
\language "nederlands"

date = \markup {\teeny {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Pirates of the Caribbean"
  subtitle = \markup \teeny{"A medley including: Fog Bound - The Medallion Calls - To the Pirates Cave - The Black Pearl - One Last Shot - He‘s A Pirate"}
  instrument = "String Bass"
  composer = "Klaus Badelt"
  arranger = "Ted Ricketts"
  copyright = "2003 Walt Disney Music Company"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \numericTimeSignature
  \time 12/8
  \tempo "With a bounce" %4=100
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

contrabass = \relative c {
  \global
  % Music follows here.
  R1*4*12/8^\markup{\box "Fog Bound"}
  \mark \markup{\box 5}
  R1*4*12/8
  \mark \markup{\box 9}
  R1*2*12/8
  d4.^\markup{\italic pizz.}\mf r4. r2. | %  11
  R1*9*12/8
  \break
%  \mark \markup{\box 21}
  \time 3/4
  \tempo "Much Slower"
  R1*3/4^\markup{\box "The Medallion Calls"}
  d'2.\mp^\markup{\italic arco} | % 22
  d2. | % 23
  a2. | % 24
  bes2. ~ | % 25
  bes2 r4 | % 26
  \mark \markup{\box 27}
  \time 4/4
  R1*2
  \time 3/4
  R1*2*3/4^\markup{\bold Majestically}
  \break
  d,8\mf d d d d d | % 31
  d8 d d d d d | % 32
  d8 d d d d d | % 33
  \mark \markup{\box 34}
  d4 d d | % 34
  c4 c c | % 35
  f,4 f f | % 36
  c'4 c c8 c | % 37
  \break
  f,4 f8 g g4 | % 38
  a4 a a | % 39
  d4 d d | % 40
  a16\f a a8\staccato r8 a8\staccato a8\tenuto r | % 41
  a16\f a a8\staccato r a16 a a8\staccato r | % 42
  \mark \markup{\box 43}
  d4\mf d8\staccato d8\staccato d4 | % 43
  \break
  c4 c8\staccato c8\staccato c4 | % 44
  f,4. g8\staccato a4 | % 45
  bes4 bes bes8\staccato a\staccato | % 46
  d4 e f | % 47
  g,4 g g | % 48
  a4 a a | % 49
  a4 a a | % 50
  d2.\> | % 51
  \break
  \mark \markup{\box 52}
  d2.\!^\markup{Tuba \small \italic legato}
  a2.
  d2.
  a2 r4
  \mark \markup{\box 56}
  bes4\p^\markup{Play} bes bes | % 56
  f4 f f | % 57
  g4 g g8 g | % 58
  a4\mf\< a a |% 59
  \mark \markup{\box 60}
  d4\f\!-> r4 r8 d | % 60
  \break
  d4-> r r8 c-> | % 61
  d4-> r r | % 62
  r8 c->\downbow r c->\downbow r c->\upbow | % 63
  d4-> r r8 d-> | % 64
  d4-> r r8 c-> | % 65
  d4-> r r | % 66
  \time 4/4
  r8 a->\downbow\< r a->\downbow r a->\!\upbow a->
  \downbow\ff a->\upbow | % 67
  \time 3/4
  \mark \markup{\box 68}
  d4_\markup{\italic marcato} d d | % 68
  c4 c c | % 69
  f,4 f f | % 70
  c'4 c c | % 71
  f,4 f8 g g4 | % 72
  a4-> a2-> | % 73
  a2-> a4-> | % 74
  \break
  \time 4/4
  \tempo "Forcefully"
  \mark \markup{\box 75}
  d8->^\markup{\box "To the Pirates Cave"} d d-> d d-> d d-> d | % 75
  d8 d-> d d->   d d-> d-> d-> | % 76
  d8-> d d-> d   d-> d d d-> | % 77
  d8 d-> d d->   d d-> d-> d-> | % 78
  d8-> d d-> d   d-> d d d-> | % 79
  \break
  d8 d-> d d->   d d-> d-> d->| % 80
  d8-> d d-> d   d-> d d d-> | % 81
  d8 d-> d d->   d d-> d-> d-> | % 82
  d8-> d d-> d   d-> d d\tenuto d\marcato | % 83
  R1
  \break
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
