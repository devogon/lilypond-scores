\version "2.19.57"

\language "nederlands"

\header {
  title = "Music from the Pirates of the Caribbean"
  subtitle = \markup{\teeny "A medley including The Medallion Calls : Blood Ritual: The Black Pearl"}
  composer = "Klaus Badelt"
  arranger = "arranged by Larry Moore"
 % poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key f \major
  \time 4/4
%  \tempo "Rubato" %4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
   \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
\tempo "Rubato" 4=60
   \partial 4 r4^\markup{\italic \tiny{The Medallion Calls }} |
  f1\downbow\mp\>
  d1\downbow\!\mp\> |
  bes4\downbow\tenuto\mp\! r g\downbow\tenuto r |
  \time 3/4
  \tempo "Majestically" 4=96
  R2.*2 |
  d'8\downbow\mf d d\marcato d d d |
  d4 d d |
  d4 d d |
  \break
  d4 d d |
  c4 c c |
  f,4 f f |
  c'4 c c8\upbow\staccato (c\staccato) |
  f,4.\downbow g8 g4 |
  a4 a a |
  d4 d d |
  a8\downbow a r a a r |
  \break
  a8\downbow a r a a r |
  d4\downbow d d |
  c4 c c |
  f4\downbow r8 f\downbow a4 |
  bes4 bes bes8 a |
  d,4\downbow e f |
  g4 g g |
  a4 a a |
  \break
  a,4 a a |
  d2.\downbow\> |
  d2.\mp\upbow\!^\markup{\italic \bold Legato} |
  a2. |
  d2. |
  a2 a4\upbow |
  bes4\downbow bes bes |
  f4 f f |
  g4 g g8 g |
  a4\< a\upbow\tenuto (a\tenuto\!) |
  \break
  d4\f\downbow r_\markup{\italic marcato} r8 d\upbow |
  d4\downbow_\markup{\large \bold "Kanon Boem"} r r8 c8\upbow |
  d4\downbow r r |
  r8 c\downbow r c\upbow\staccato (r c\staccato\upbow) |
  d4\downbow r r8 d\upbow |
  d4\downbow r r8 c\upbow |
  d4\downbow r r |
  \time 4/4
  \break
  r8 a\upbow\< r a\downbow r a\upbow a\ff a |
  \time 3/4
  d4\downbow\marcato d d |
  c4 c c |
  f,4 f f |
  c'4 c c |
  f,4\downbow f8 g g4 |
  a4 a a |
  \break
  d4 d c |
  d8\downbow d r d\upbow-> d\downbow-> d\upbow-> |
  d8\downbow r r4 r |
  \bar "||"
  \time 4/4
  \tempo "Slower" 4=60
   R1*4^\markup{\tiny \italic {Blood Ritual}} |
  bes1\p\downbow
  f1\upbow |
  g1\downbow |
  a1\upbow |
  \break
  \bar "||"
  \time 3/4
  \tempo "Driving" 4=162
   d4\staccato\f\downbow^\markup{\tiny \italic {The Black Pearl}}

  r r |
  d4\staccato\downbow r r |
  d4\staccato\mf\downbow r r |
  d4\staccato\downbow r r |
  d4\staccato\downbow r r |
  d4\staccato\downbow f\staccato\upbow (c\upbow\staccato) |
  d4\staccato\downbow r r |
  d4\staccato\downbow r r |
  \break
  d4\staccato\downbow r r |
  d4\staccato\downbow r r |
  d4\staccato\downbow c\staccato\upbow (g\staccato) |
  a4\staccato\downbow_\markup{\italic {accel e cresc.}} r r
  a4\staccato\downbow r r |
  a4\staccato\downbow r r |
  a4\staccato\downbow\< a\staccato a\staccato |
  a4\staccato a\staccato a\staccato |
  \break
  \tempo "A Little Faster" 4=170
  d4\!->\downbow\ff r r |
  a4\staccato\f\downbow r a\staccato\upbow |
  d4->\downbow r r |
  c4\staccato\downbow r r |
  g'4\downbow-> r g\staccato\upbow |
  d4\staccato\downbow r d\staccato\downbow |
  c\staccato\upbow c\staccato a\staccato |
  d4\staccato r r |
  \break
  d4\downbow-> r d\staccato\upbow |
  a4\staccato r a\staccato |
  d4-> r d\staccato |
  c4\staccato r c\staccato |
  g'4\downbow-> r g\staccato\upbow |
  d4\staccato\downbow r d\staccato\downbow |
  c4\staccato\upbow c\staccato a\staccato |
  d4\staccato r r |
  \break
  d4\staccato\downbow d\staccato r |
  d2.\ff\downbow ( |
  d2.) \breathe |
  f2.\downbow |
  c2. |
  g'2.\downbow |
  d2. |
  a2. |
  d2.|
  d2.\downbow ( |
  d2. ) |
  \break
  % 103
  f2.\downbow |
  c2. |
  a'8\downbow\mp a a_\markup{\italic cresc.} a g g |
  f8 f e e d d |
  f8 f e e d d |
  c8 c bes bes gis gis |
  a8\< a a a a a\! |
  \break
  % 110
  \bar "||"
  d4\downbow\f^\markup{\column{\line{\tiny \italic{The Medallion Calls}} \line{\bold {Molto Carcato}}}} d d |
  c4 c8 c c4 |
  f,4 f8 f f4 |
  c'4 c c |
  f,4\downbow f8 g g4 |
  a4 a a |
  d4 d c |
  \break
  % 117
  d4 d\staccato\upbow (a\staccato) |
  d4\downbow d d |
  c4 c c |
  f4 f8 g a4 |
  bes4 bes bes8 a |
  d,4\downbow e f |
  g4 g g |
  \break
  % 124
  a4 a a |
  a4 a a |
  a4 a\staccato\upbow (a\staccato) |
  a8\downbow\mp a a_\markup{\italic cresc.} a g g |
  f8 f e e d d |
  c8 c bes bes gis gis |
  \break
  % 131
  a8 a a a g' g |
  f8\downbow f e e d d |
  c8 c bes bes gis gis |
  a4\ff-> a\downbow a\downbow |
  a4\downbow a\downbow a\downbow |
  a4\downbow d\downbow-> r |
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

\markup \column {
  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
  \hspace #1
}

\markup {
  \teeny
  \date
}