\version "2.19.57"

\language "nederlands"

\header {
  title = "Star Wars: The Force Awakens"
  subtitle = \markup{\teeny "Star Wars (Main Theme) : Rey's Theme : March of the Resistance : The Jedi Steps and Fina..."}
  composer = "John Williams"
  arranger = "arranged by Robert Longfield"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
%  system-system-spacing = #'((basic-distance . 0.1) (padding . 0))
  ragged-last-bottom = ##f
  ragged-bottom = ##f

}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g \major
  \time 4/4
%  \tempo "Majestically" % 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

contrabas = {
   \global
   \tempo "Majestically" 4=92
  g4\ff	\downbow^\markup{\tiny \italic {Star Wars (Main Theme)} } (g8) r r2 |
  %g4\ff	\downbow^\markup{\column {\line{\tiny \italic {Star Wars (Main Theme)}} \line{\bold Majestically}}} (g8) r r2 |
  r1 |
  \tuplet 3/2 {c8\downbow-> g, d } f!2-> \tuplet 3/2 {d8-> d d} |
  r4 g\downbow-> r g\upbow-> |
  r4 d\downbow-> d8 d g g |
  % \break
  r4 d\downbow-> d8 d g g |
  r4 d\downbow-> d8-> d-> d4-> | %  7
  g4\f\downbow (fis) fis (g) |
  e4 e8 (d) c b, a, g, | % 9
  d4 e8 fis g4 d |
  % \break
  r4 \tuplet 3/2 {d8\upbow\> d d} d4 d |
  \bar "||"
  g8\!\mf->^\markup{\bold {Suddenly Slower}} r r4 r2 |
  r1_\markup{\italic rit.} |
  \bar "||"
  \break
  \tempo "Moderately" 4=108
  R1*4^\markup{\tiny \italic {Rey's Theme}} |
%  R1*4^\markup{\column{\line{\tiny \italic {Rey's Theme}} \line{\bold Moderately}}} |
  r4^\markup{\italic pizz.}\mp e8 b, e4 r |
  r4 e8 b, e4 r |
  % \break
  r4 e8 b, e4 r |
  r4 a8 e a4 r |
  r4 e8 b, e4 r |
  r4 a8 e a4 r |
  r4 e8\f g e4 r |
  % \break
  r4 a8 g d c f!4 |
  r4 b, e r |
  r4 b, a, d |
  r4 g c g |
  r4 g g d |
  % \break
  % 30
  r4 e\mf r2 |
  r1 |
  R1*2_\markup{\italic rit.} |
  <e e,>1\p\downbow^\markup{\italic arco} ( |
  <e e,>1\fermata\upbow) |
  \bar  "||"
  \break
  \key c \major
  %e8\downbow\f->^\markup{ \column{\line{\tiny \italic{March of the Resistance}} \line{\bold {With Resolve}} }} e-> e-> r r4 e8\downbow-> e-> |
  \tempo "With Resolve" 4=152
  e8\downbow\f->^\markup{\tiny \italic{March of the Resistance}} e-> e-> r r4 e8\downbow-> e-> |
  e8-> e-> e-> r r4 e8\downbow-> e-> |
  % \break
  a,1\downbow-> |
  r4 c'\staccato\downbow a\staccato e\staccato |
  c4\staccato a,2.-> |
  r4 a8\downbow a a-> a a4\staccato |
  r4 d\downbow r b, |
  r a4 r f |
  % \break
  % 44
  e4 r r2 |
  f4\ff\downbow-> e-> d (c8\staccato) b,\staccato |
  a,4\downbow-> r r2 |
  r2 a4\staccato\mf\downbow \breathe a8\downbow a |
  a8-> a a4 r2 |
  r2 a4\staccato\downbow \breathe a8\downbow a |
  % \break
  a4\staccato a\staccato r2 |
  r4 a\staccato\downbow e\staccato a8 a |
  a4\staccato a\staccato r2 |
  a4\staccato\downbow r r e\ff\upbow-> |
  a,2-> b,-> |
  c8-> b,-> c->r r4 c\downbow-> (|
  % \break
  % 56
  c4) b,8 c d4 c8 d |
  e8 e 	e r r4 a,\staccato\upbow |
  r4 d\f\downbow r f\upbow |
  r4 e r a, |
  r4 e r e |
  r4 c r b, |
  % \break
  % 62
  r4 a,8 a, a,4 a, |
  r4 e\< r e-> |
  f4\ff	\downbow-> r r2 |
  r2 as4\upbow-> f-> ( |
  f8) r r4 as4\upbow-> f4-> ( |
  f8) r r4 d2\fermata\upbow-> |
  \break
  % 68
  \bar "||"
  \tempo "Fast" 4=120
  %   g8\downbow->^\markup{\column {\line{\tiny \italic{The Jedi Stops and Finale} } \line{\bold Fast} }} r r4 r2 |
  g8\downbow->^\markup{\tiny \italic{The Jedi Stops and Finale}}  r r4 r2 |
  g,2\downbow->^\markup{\italic Soli} d2-> |
  \tuplet 3/2 {c8 b,\staccato (a,\staccato)} g2-> d4 |
  \tuplet 3/2 {c8 b,\staccato (a,\staccato)}  g2-> d4 |
  \tuplet 3/2 {f!8 e\staccato (f\staccato)} d2-> d4-> |
  r4 a2\downbow-> a,4-> |
  % \break
  r4 a2-> a,4-> |
  r4 es\downbow-> d-> c-> |
  r4_\markup{\italic rit.} c'\upbow-> bes-> a-> |
  \bar "||"
  \tempo "Majestically" 4=104
  g8-> r r4 r2 |
  r1 |
  g1\p\< ( |
  g8\!\ff) r \tuplet 3/2 {g8-> g-> g->} g8-> r r4 |
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}

%\markup \column {
%  \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
%  \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
%  \hspace #1
%}

\markup {
  \teeny
  \date
}