\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "a major scale"
  instrument = "Bass"
  composer = "Joseph Prunner"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key a \major
  \time 4/4
%  \tempo 4=100
}

stringNumberSpanner =
#(define-music-function (parser location StringNumber) (string?)
  #{
    \override TextSpanner.style = #'solid
    \override TextSpanner.font-size = #-5
    \override TextSpanner.bound-details.left.stencil-align-dir-y = #CENTER
    \override TextSpanner.bound-details.left.text = \markup { \box \number #StringNumber }
  #})

contrabass = \relative c {
  \global
%\clef bass
\stringNumberSpanner "A" \textSpannerDown a16-0 \startTextSpan b-1 cis-4 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-2 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1  d-0 \stopTextSpan \stringNumberSpanner "A" \textSpannerDown cis-4 \startTextSpan b-1 a-0 |
b-1 cis-4 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-1 b-4 b-4 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 d-0 \stopTextSpan \stringNumberSpanner "A" \textSpannerDown cis-4 \startTextSpan b-1 |
cis-4 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-2 b-1 cis-4 cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 d-0 \stopTextSpan cis-4_\markup{\box A} |
\stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-1 b-4 cis-2 d-4 d-4 cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 d-0 |
\break
e-1  fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-2 b-1 cis-4 d-1 e-4 e-4 d-1 cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 |
fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-1 b-4 cis-2 d-4 e-1 fis-4 fis-4 e-4 d-1 cis-4 b-1 a-2 gis-1 \stopTextSpan fis-4_\markup{\box D} |
\stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-2 b-1 cis-4 d-1 e-4 fis-1 gis-3 gis-4 fis-1 e-4 d-1 cis-4 b-1 a-2 gis-1 |
a-1 b-4 cis-2 d-4 e-1 fis-4 gis-2 a-3 a-3 gis-2 fis-4 gis-1 d-4 cis-2 b-4 a-1 |
b-4 cis-2 d-4 e-1 fis-4 gis-1 \clef treble a-2 b-3 b-3 a-2 \clef bass gis-1 fis-+ e-4 d-1 cis-4 b-1 |
cis-4 d-1 e-4 fis-1 \clef treble gis-3 a-1 b-2 cis-3 cis-3 b-2 a-1 gis-+ \clef bass fis-4 e-1 d-2 cis-1 |
d-2 e-1 fis-4 gis-1 \clef treble a-2 b-1 cis-2 d-3 d-3 cis-2 b-1 a-2 \clef bass gis-1 fis-+ e-4 d-1 |
e-1 fis-4 \clef treble gis-1 a-2 b-3 cis-1 d-2 e-3 e-3 d-2 cis-1 b-3 a-2 gis-1 \clef bass fis-4 e-1 |
fis-4 gis-+ \clef treble a-1 b-2 cis-3 d-1 e-2 fis-3 fis-3 e-2 d-1 cis-3 b-2 a-1 \clef bass gis-3 fis-1 |
gis-3 a-1 \clef treble b-2 cis-3 d-+ e-1 fis-2 gis-3 gis-3 fis-2 e-1 d-+ cis-3 b-2 a-1 gis-+ |
\break
a-1 b-2 cis-3 d-1 e-2 fis-1 gis-2 a-3 ~  a4 r  |
a16-3 gis-2 fis-1 e-2 d-1 cis-3 b-2 a-1 a-1 b-2 cis-3 d-1 e-2 fis-1 gis-2 a-3 |
\break
gis-2 fis-1 e-2 d-1 cis-3 b-2 a-1 gis-+ gis-+ a-1 b-2 cis-3 d-+ e-1 fis-2 gis-2 |
fis-2 e-1 d-+ cis-3 b-2 a-1 gis-3 fis-1 fis-1 gis-3 a-1 b-2 cis-3 d-1 e-2 fis-3 |
e-2 d-1 cis-3 b-2 a-1 gis-+ \clef bass fis-4 e-1 e-1 fis-4 \clef treble gis-1 a-1 b-2 cis-3 d-2 e-3 |
d-2 cis-1 b-2 a-2 \clef bass gis-1 fis-+ e-4 d-1 d-1 e-1 fis-4 gis-1 \clef treble a-2 b-1 cis-2 d-3 |
cis-2 b-1 a-2 gis-1 \clef bass fis-4 e-1 d-2 cis-1 cis-1 d-2 e-1 fis-4 \clef treble gis-+ a-1 b-2 cis-3 |
b-2 a-1 \clef bass gis-3 fis-1 e-4 d-1 cis-4  b-1 b-1 cis-2 d-4 e-1 fis-4 gis-1 \clef treble a-2 b-1 |
a-2 gis-1 \clef bass fis-4 e-1 d-4 cis-2 b-4 a-1 a-1 b-4 cis-2 d-4 e-1 fis-4 gis-2 a-3 |
gis-3 fis-1 e-4 d-1 cis-4 b-1 a-2 gis-1 gis-1 a-2 b-1 cis-4 d-1 e-4 fis-1 gis-3 |
fis-1 e-4 d-1 cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-1 b-4 cis-2 d-4 e-1 fis-4 |
e-4 d-1 cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-2 b-1 cis-4 d-1 fis-4 |
\break
d-1 cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 d-0 d-0 e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-1 b-4 cis-2 d-4 |
cis-4 b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 d-0 \stopTextSpan \stringNumberSpanner "A" \textSpannerDown cis-4 \startTextSpan cis-4 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown  gis-1 \startTextSpan a-2 b-1 cis-4 |
b-1 a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown  fis-4 \startTextSpan e-1 d-0 \stopTextSpan \stringNumberSpanner "A" \textSpannerDown cis-4 \startTextSpan b-1 b-1 cis-4  \stopTextSpan \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 fis-4 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown gis-1 \startTextSpan a-1 b-4 |
a-2 gis-1 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown fis-4 \startTextSpan e-1 d-0 \stopTextSpan \stringNumberSpanner "A" \textSpannerDown cis-4 \startTextSpan b-1 a-0 ~ a4 r \stopTextSpan |
\bar "|."
\break
<a-4 cis-2>16 <b-1 d-0> <cis-4 e-2> <d-4 fis-2> <e-2 gis-1> <fis-4 a-1> <gis-4 b-1> <a-4 cis-2> <a-4 cis-2> <gis-4 b-1> <fis-4 a-1> <e-2 gis-1> <d-4 fis-2> <cis-4 e-1> <b-1 d-0> <a-4 cis-2> |
<b-1 d-0> <cis-4 e-1> <d-4 fis-2> <e-2 gis-1> <fis-4 a-1> <gis-4 b-1> <a-4 cis-2> <b-4 d-1> <b-4 d-1> <a-2 cis-1> <gis-4 b-1> <fis-4 a-1> <e-2 gis-1> <d-4 fis-2> <cis-4 e-1> <b-1 d-0> |
% c16-2_\markup{\box "A"} \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 f-2 \stopTextSpan  \stringNumberSpanner "G" \textSpannerDown g-0 \startTextSpan  a-1 b-2 c-4 c-4 b-4 a-1 g-0 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown f-2 \startTextSpan e-1 d-0 \stopTextSpan c-2_\markup{\box A}  |
%   \stringNumberSpanner "D" \textSpannerDown d-0 \startTextSpan e-1 f-2 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown  g-0 \startTextSpan a-1 b-4 c-1 d-4 d-4 c-1 b-4 a-1 g-0 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown f-2 \startTextSpan e-1 d-0\stopTextSpan |
%   \break
%   \stringNumberSpanner "D" \textSpannerDown e-1 \startTextSpan f-2 \stopTextSpan \stringNumberSpanner "G" \textSpannerDown  g-0 \startTextSpan a-1 b-2 c-4 d-1 e-4 e-4 d-4 c-1 b-4 a-1 g-0 \stopTextSpan \stringNumberSpanner "D" \textSpannerDown f-2 \startTextSpan   e-1 |
%   f-2 g-0 a-1 b-4 c-1 d-4 e-2 f-4 f-4 e-2 d-4 c-1 b-4 a-1 g-0 f-2 \stopTextSpan |
%   \break
%   \stringNumberSpanner "G" \textSpannerDown g-0 \startTextSpan a-1 b-2 c-4 d-1 e-4 f-1 g-3 g-3  f-4 e-2 d-4 c-1 b-4 a-1 g-0 |
%   a-1 b-4 c-1 d-4 e-2 f-4 g-1 a-3 a-3 g-1 f-4 e-2 d-4 c-1 b-4 a-1 \stopTextSpan |
%   \break
%   \stringNumberSpanner "G" \textSpannerDown b-4 \startTextSpan c-1 d-4 e-2 f-4 g-1 \clef treble  a-2 b-3 b-3 a-2 \clef bass g-1 f-4 e-4 d-1 c-2 b-1 |
%   c-2 d-1 e-4 f-1 g-3 a-1 \clef treble b-2 c-3 c-3 b-2 \clef bass a-1 g-+ f-4 e-2 d-4 c-1 \stopTextSpan |
%   \break
%   \stringNumberSpanner "G" \textSpannerDown d-4 \startTextSpan e-2 f-4 g-1 \clef treble a-2 b-1 c-2 d-3 d-3 c-2 b-1 a-2 \clef bass g-1 f-+ e-4 d-1 |
%   e-2 f-4 g-1 a-2 \clef treble b-3 c-1 d-2 e-3 e-3 d-2 c-1 b-3 \clef bass a-2 g-1 f-2 e-1 \stopTextSpan |
%   \break
%   \stringNumberSpanner "G" \textSpannerDown f-2 \startTextSpan g-+ \clef treble a-1 b-2 c-3 d-1 e-2 f-3 f-3 e-2 d-1 c-3 b-2 a-1 \clef bass g-3 f-1 |
%   g-3 a-1 \clef treble b-2 c-3 d-+ e-1 f-2 g-3 g-3 f-2 e-1 d-+ c-3 b-2 \clef bass a-1 g-+ \stopTextSpan |
%   \break
%   \stringNumberSpanner "G" \textSpannerDown a-1 \startTextSpan b-2 \clef treble c-3 d-1 e-2 f-1 g-2 a-3 a-3 g-2 f-1 e-2 d-1 c-3 b-2 a-1 |
%   b-2 c-3 d-1 e-2 f-3 g-1 a-2 b-3 b-3 a-2 g-1 f-3 e-2 d-1 c-2 b-1 \stopTextSpan |
%   \break
%   \stringNumberSpanner "G" \textSpannerDown c-2 \startTextSpan d-+ e-1 f-2 g-3 a-1 b-2 c-3 ~ c4 \stopTextSpan r |
%   \stringNumberSpanner "G" \textSpannerDown c16-3 \startTextSpan b-2 a-1 g-+ f-3 e-2 d-1 c-+ c-+ d-1 e-2 f-3 g-+ a-1 b-2 c-3 \stopTextSpan|
%   \break
%

}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

contrabassPart = \new Staff { \clef bass \contrabass }

chordsPart = \new ChordNames \chordNames

\score {
  <<
    \contrabassPart
    \chordsPart
  >>
  \layout { }
}

% see Prunner book
% c scales begin pg
% c major 3-octave begins (bk) pg 107
% c maj arpeggios (bk) pg 108
% a minor (la minor) arpeggio pg 54
% a min (la minor) harmonic pg 52
% a minor (la minor) melodic pg 51
% c maj chromatic pg 40