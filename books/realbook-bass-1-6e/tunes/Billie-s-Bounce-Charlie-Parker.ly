\version "2.19.82"
\language "nederlands"

  \include "/home/ack/src/lilyjazz/stylesheet/lilyjazz.ily"
   \include "/home/ack/src/lilyjazz/stylesheet/jazzextras.ily"
   \include "/home/ack/src/lilyjazz/stylesheet/jazzchords.ily"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = \markup{\larger "Billie‘s Bounce"}
  instrument = "Bass"
  composer = "Charlie Parker"
  piece = "24"
  % Remove default LilyPond tagline
  poet = \markup{\bold "(Blues)"}
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s8
  f1:7
  bes2:7 b:dim
  f1:7
  s1
  bes1:7
  s1
  f1:7
  a2:m d:7
  g1:m
  c1:7
  f2:7
  d:7
  g2:m7 c:7
  f1:7
  s1
  s1
  c2:m f:7
  s8 bes4:7 s8 s2
  s1
  f1:7
  a2:m d2:7
  g1:m
  c1:7
  f1:7
  c:7
}

contrabass = \relative c {
  \global
  % Music follows here.
  \partial 8 c8 |
  \repeat volta 2 {
    b8 c f as a! f d f ~ |
    f8 d f r r f4 d8 |
    f8 r4 f8 ~ f d f d |
    \break
    as'8 a! \tuplet 3/2 {f16 g f} d8 f8 g f f |
    r4 r8 a bes f r as ~ |
    as8 bes4. e8 c f e |
    \break
    r8 f8 c4 r r8 e ~ |
    e4 g,8 e fis es' c cis  |
    d4 r8 g \tuplet 3/2 {f16 g f} d8 b f |
    \break
    f'4 r8 f e e e e |
    c4 r8 f, ~ f d f4 |
    r8 f ~ f d f4 r8 \parenthesize d
  }
  \break
  f4^\markup{\bold Solo} g8 as a!4 bes8 b! |
  c8 a \tuplet 3/2 {bes8 c bes} a8 f d c |
  \tuplet 3/2 {g'16 a g} f8 es f ~ f4 r |
  \break
  r2 r4 r8 f' ~ |
  f8 c16 b a8 f g f e d |
  c4 b8 f ges g bes b |
  \break
  c8 d16 c a8 c e d4. |
  r1 |
  r4 r8 \tuplet 3/2 {g16 bes d} f8 e d des |
  \break
  c8 a bes c16 bes a8 f d c |
  g'8 f g a ~ a4 r |
  r1
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

contrabassPart = \new Staff { \clef bass \contrabass }

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}
