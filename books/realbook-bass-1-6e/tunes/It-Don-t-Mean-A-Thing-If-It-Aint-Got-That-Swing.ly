\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "It Don‘t Mean A Thing If It Aint Got That Swing"
  instrument = "Contrabas"
  composer = "Duke Ellington"
  arranger = "Irving Mills"
%  meter = "Swing"
  copyright = "1932 Mills Music, Inc."
  % Remove default LilyPond tagline
  tagline = \date
  % real book - bass clef 6th edition page 224
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
  \partial 4
  \tempo "Swing" 4=162
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s4
  g2:m g2:m/fis
  g2:m/f g2:m/e
  es:7 d2:7
  g1:m
  c1:7
  f1:7sus4
  bes1:6
  d1:7+5
  bes1:6
  f1:m7
  bes1:7
  es1:maj7
  s1
  g1:min7
  c1:7
  f1:7
  d1:7
  g2:m g2:m/fis
  g2:m/f g2:m/e
  es:7 d2:7
  g1:m
  c1:7
  f1:7sus4
  bes1.:6
  s4
  d4:7+5
}

contrabass = \relative c {
  \global
  % Music follows here.
  d'4 |
  \repeat volta 2 {
    g,2 g4 bes | % 1
    d2 g,4 bes | % 2
    des2 c8 bes4 g8 | % 3
    \break
    g1 | % 4
    r4 bes8 bes8 ~ bes8 bes8 bes4 | % 5
    bes8 bes4 bes8 bes4 bes8 bes8 ~ | % 6
    \break
    bes8 bes8 bes4 bes8 bes4 bes8 | % 7
  }
  \alternative {
    {bes4 r r bes}
    {bes2. c4}
  }
  c4 c c8 bes4 c8 ~ | %
  c4 bes4 c8 bes4. | %
  g1 ~ |
  g2. c4 |%
  \break
  c4 c c8 c4. |
  c4 bes c8 c4. |
  f,1 |
  r2 r4 bes |
  \break
  g2 g4 bes |
  d2 g,4 bes |
  bes2 c8 bes4 g8 ~ |
  g1 |
  \break
  r4 bes8 bes ~ bes bes bes4 |
  bes8 bes4 bes8 bes4 	bes8 bes ~ |
  bes8 bes bes4 bes8 bes4 bes8 |
  bes4 r r \parenthesize bes
  \bar "|."
}


chordsPart = \new ChordNames \chordNames

contrabassPart = \new Staff { \clef bass \contrabass }

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}
