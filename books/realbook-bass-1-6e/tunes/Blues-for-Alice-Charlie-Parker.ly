\version "2.19.82"
\language "nederlands"

  \include "/home/ack/src/lilyjazz/stylesheet/lilyjazz.ily"
   \include "/home/ack/src/lilyjazz/stylesheet/jazzextras.ily"
   \include "/home/ack/src/lilyjazz/stylesheet/jazzchords.ily"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = \markup{\larger "Blues for Alice"}
  instrument = "Bass"
  composer = "Charlie Parker"
  % Remove default LilyPond tagline
  piece = \markup{\bold "57"}
  poet = \markup{\bold "(Jazz)"}
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm

}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=100
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  f1:7
  e2:m7.5- a:7.9-
  d2:m7 g:7
  c2:m7 f:7
  bes1:7
  bes2:m7 es:7
  a2:m7 d:7
  as:m7 des:7
  g1:m7
  c1:7
  f2 d:m7
  g2:m7 c:7

}

contrabass = \relative c {
  \global
  % Music follows here.
  f'4 c8 a e'4 c8 a |
  d8 d b! c cis bes g gis |
  a4 f8 d g8 a f e |
  \break
  \tuplet 3/2 {es8 g bes} d8 des r f \tuplet 3/2 {f8 f f} |
  c4 bes8 f as bes, r g' |
  es'8 des as f c f g a! ~ |
  \break
  a4 e8 c d4 r8 des' ~ |
  des4 ces8 ges bes4 r8 as |
  \tuplet 3/2 {f4 f' f} f8 d bes g |
  \break
  a8 g c bes es4 r8 c ~ |
  c4 a8 f g4 r8 d' ~ |
  d4 bes8 f a4 r |
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

contrabassPart = \new Staff { \clef bass \contrabass }

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}

\markup {Charlie Parker - "Swedish Schnapps"}