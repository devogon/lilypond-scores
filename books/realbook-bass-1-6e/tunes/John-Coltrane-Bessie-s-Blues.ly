\version "2.19.81"
\language "nederlands"

\header {
  title = "Bessie‘s Blues"
  %subtitle = "42"
  poet = \markup{\large \bold 42}
%  instrument = "Bass"
  composer = "John Coltrane"
  meter = "Bright Blues"
  % Remove default LilyPond tagline
  copyright = "1977 JOWCOL Music"
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
%  \tempo 4=100
}

contrabass = \relative c' {
  \global
  % Music follows here.
  \repeat volta 2 {
    g4^\markup{\italic {E\flat 7}} es8 des ~ des4 r8 c' ~ | % 1
    c4^\markup{\italic {A\flat 7}} as8 ges ~ ges4. bes,8 | % 2
    g'8^\markup{\italic {E\flat 7}} es g4 g g8 g ~ | % 3
    \break
    g8 es d des ~ des2 | % 4
    r8^\markup{\italic {A\flat 7}} c'4 e,8 c' b4 c8 ~ | % 5
    c4. a8 ~ a ges4 e8 | % 6
    \break
    g8^\markup{\italic {E\flat 7}} e g4 g g8 g ~ | % 7
    g8 e d des r4 g8 f ~ | % 8
    f4^\markup{\italic {B\flat 7}} d8 c bes c d f | % 9
    \break
    es8^\markup{\italic {A\flat 7}} c as es c'4 es8 es ~ | % 10
    es2^\markup{\italic {E\flat 7}} r | % 11
  }
  \alternative {
    {r4 r8 bes f' g as bes}
    {r1}
  }
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
