\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "The Girl from Ipanema"
  subtitle = "Garota de Ipanema"
  composer = "Antonio Carlos Jobin/Norman Gimbel/Vincius de Moraes"
  meter = "Bossa"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo 4=140
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  f1:maj7
  s1
  g1:7
  s1
  \break
  g1:7
  g1:7-.5-
  f1:maj7
  g1:7-.5-
  \break
  f1:maj7
  s1
  ges1:maj7
  s1
  b1:7
  s1
  fis1:m7
  s1
  d:7
  s1
  g1:m7
  s1
  es1:7
  s1
  a1:m7
  d1:7-.5-.11+
  g1:7
  c1:8.9-.11+
  f:maj7
  s1
  g1:7
  s1
  g1:m7
  g1:7-.5-
  f1:maj7
  g1:7-.5-
}

electricBass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 2 {
    g'4. e8 e d4 g8 ~ | % 1
    g4 e8 e ~ e e d g ~ | % 2
    g4 e e d8 g ~ | % 3
    g8 g e e ~ e e d f ~ | %
    \break
    f8  d4 d8 ~ d d c e ~ | % 5
    e8 c4 c8 ~ c c bes4 | % 6
  }
  \alternative {
    { r4 c2. ~ c2 r }
    { r4 c2. ~ c2 r }
  }
  f1 ~ | %
  \tuplet 3/2 {f4 ges f} \tuplet 3/2 {es4 f es} |
  \break
  cis4. dis8 ~ dis2 ~ |
  dis2. r8 gis ~ |
  gis1 ~ |
  \tuplet 3/2 {gis4 a gis} \tuplet 3/2 {fis4 gis fis} |
  \break
  e4. fis8 ~ fis2 ~ |
  fis2. r8 a8 ~ |
  a1 ~ |
  \tuplet 3/2 {a4 bes a} \tuplet 3/2 {g a g} |
  \break
  f4. g8 ~ g2 ~ |
  g2 \tuplet 3/2 {r4 a bes} |
  \tuplet 3/2 {c4 c, d} \tuplet 3/2 {e f g} |
  gis2. a4 |
  \break
  \tuplet 3/2 {bes4 bes, c} \tuplet 3/2 {d e f} |
  fis1 |
  \bar "||"
  g4. e8 e d4 g8 ~ |
  g4 e8 e ~ e e d g ~ |
  \break
  g4 e e d8 g ~ |
  g8 g e e ~ e e d a' ~ |
  a4. f8 f f d c' ~ |
  c4. e,8 \tuplet 3/2 {e4 e d} |
  \break
  e1 |
  r1
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
