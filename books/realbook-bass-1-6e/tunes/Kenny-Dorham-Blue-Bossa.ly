\version "2.19.81"
\language "nederlands"

\header {
  title = "Blue Bossa"
%  instrument = "Bass"
poet = \markup{\large \bold 50}
  composer = "Kenny  Dorham"
  meter = "Med. Up Bossa"
  copyright = "1965 (renewed  1993) Second Floor Music"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
%  \tempo 4=100
}

contrabass = \relative c' {
  \global
  % Music follows here.
  \partial 4 g4 | %
  \repeat volta 2 {
    g'4.\segno^\markup{\italic {C-}} f8 es d r c ~ | % 1
    c2 ~ c8 bes r as ~ | % 2
    as2^\markup{\italic {F-7}} ~ as8 g' r f ~ | % 3
    f2.^\markup{\italic {B\flat7}} r4 | % 4
    \break
    f4.^\markup{\italic {D-8\flat5}} es8 d c r bes ~ | % 5
    bes2 ~ bes8 as r g ~ | % 6
    g2 ~ g8 f' r g ~ | % 7
    g2. r4 | %8
    \break
    es4. des8 c bes r as ~ | % 9
    as2 ~ as8 ges r ges ~ | % 10
    ges4. f8 bes4. as8 | % 11
    as2. r4 | % 12
    \break
    as4 	g8 bes ~ bes4. as8 | % 13
    as4 g8 bes ~ bes4. as8 | % 14
    g1 ~ | % 15
    g2_\markup{play head twice}_\markup{after solos}^\markup{DS al Coda} r4 \parenthesize g | % 16
  }
  \break
  g1\coda | % 17
  r1 | % 18
  as4 g8 bes ~ bes4. as8 | % 19
  as4 g8 es' ~ es4 b! | % 20
  \break
  d1 | % 21
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}
