\version "2.19.82"
\language "nederlands"

  \include "/home/ack/src/lilyjazz/stylesheet/lilyjazz.ily"
   \include "/home/ack/src/lilyjazz/stylesheet/jazzextras.ily"
   \include "/home/ack/src/lilyjazz/stylesheet/jazzchords.ily"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = \markup{\larger "Green Dolphin Street"}
  instrument = "Bass"
  composer = "Kaper/Washington"
  piece = "179"
%    poet = \markup{\bold "(Latin)"}
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo "Latin" % 4=100
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  c1:7
  s1
  c1:m7
  s1
  d1:7/c
  s2 des2/c
  c1:7
  s1
  d1:m7
  g1:7
  c1:7
  s1
  f1:m7
  bes1:7
  es1:7
  s2 g2:7
  d2:m7 d2:m/c
  b2:dim7 e:7.9-
  a2:m7 a:m/g
  fis2:dim7 b:7
  e2:m7 a:7
  d2:m7 g:7
  c1:7
  d2:m7 g:7
}

contrabass = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  c2 c ~ |
  c2 \tuplet 3/2 {b4 d f} |
  bes1 ~
  bes1
  \break
  a2 a ~ |
  a2 \tuplet 3/2 {as4 f des}  |
  g1 ~ |
  g1 |
  \bar "||"
  \break
}
\alternative {
  {
    r8 g4. d4 e |
    f4 g as bes |
    g1 ~ g1
    \break
    r8 bes4. f4 g |
    as4 bes ces des |
    bes1 ~ bes2 b! |
  }
  {\break
    r8 g4. d4 e |
    f g gis e' |
    d4. c8 ~ c2 |
    c,4 d dis b' |
    \break
    a4. g8 ~ g2 |
    r8 g4. g4 g |
    g1 ~ g
  }
}
\bar "|."
}

chordsPart = \new ChordNames \chordNames

contrabassPart = \new Staff { \clef bass \contrabass }

\score {
  <<
    \chordsPart
    \contrabassPart
  >>
  \layout { }
}

\markuplist {
  \line {\bold {"Sonny Rollins on Impulse!"}}
  \line {\bold {Bill Evans - "The Tokyo Concert"}}
}