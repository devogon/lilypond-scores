\version "2.22.1"


\header {
  title = "Badge"
  composer = "Words and music by Eric Clapton and George Harrison"
  subtitle = "Cream"
}

#(define ((bars-per-line-engraver bar-list) context)
  (let* ((working-copy bar-list)
         (total (1+ (car working-copy))))
    `((acknowledgers
       (paper-column-interface
        . ,(lambda (engraver grob source-engraver)
             (let ((internal-bar (ly:context-property context 'internalBarNumber)))
               (if (and (pair? working-copy)
                        (= (remainder internal-bar total) 0)
                        (eq? #t (ly:grob-property grob 'non-musical)))
                   (begin
                     (set! (ly:grob-property grob 'line-break-permission) 'force)
                     (if (null? (cdr working-copy))
                         (set! working-copy bar-list)
                         (begin
                           (set! working-copy (cdr working-copy))))
                           (set! total (+ total (car working-copy))))))))))))


\paper {
  indent = 0.0\cm
}

global = {
  \key g \major
  \time 4/4
  \tempo "Heavy Rock Feel" 4=106
}

chordNames = \chordmode {
  \global
  % Chords follow here.
     \set additionalPitchPrefix = #"add"
  a1:m
  d1
  a1:m
  d1
  a1:m
  d1
  e1:m
  s1
  a1:m
  d1
  e1:m9^7 % -\markup{\super "add9"}
  % https://lilypond.org/doc/v2.22/Documentation/notation/displaying-chords
  e1:m
  c1
  a1:m
  b1:m
  a1:m9^7 % add9
  s1
  % second verse
  a1:m
  d1
  e1:m9^7 % add9
  e1:m 
  e1:m
  d1
  e1:m9^7 % add9
  e1:m
  c1
  a1:m
  b1:m
  a1:m9^7 % add9
  d2 c:maj7
  % bridge
  g4/b g2.
  d2. c2:maj7
  g2/b g4
  d2. c2:maj7
  g2/b g4
  d2. c4:maj7
  s4. g4./b g4
  d2. s8 c2:maj7
  g4./b g4
  d2. s8 c2:maj7
  g4./b g4
  d2. s8 c2:maj7
  s8 g4./b g8
  d2. s8 c2
  g4./b g4
  d2. c2.:maj7
  g4/b g4
  d2. s8 c2:maj7
  s8 g4/b g4
  d2. s8 c2:maj7
  s8 g4/b g4
  d2. c2.:maj7 g4/b g4
  d2. s16 c8:maj7
  s2 g4/b g4
  d2. s8 c8.:maj7
  s1
  d1
  a1:m
  d1
  e1:m9^7 % add9
  s1
  a1:m
  d1
  e1:m
  s1
  c1
  a1:m
  b1:m
  a1:m9^7 % add9
}

electricBass = \relative c, {
  \global
  % Music follows here.
  a4\4 r8 c\3 r e\3 r c\3 ( |
  d8\3) d\3 d\3 d\3 d4\staccato\3 r8 g,16\4 (gis\4 |
  a4\4) r8 c\3 r e\3 r c\3 ( |
  d\3) d\3 d\3 d\3 d\3 fis,4\tenuto\4 g16\4 ( gis\4 |
  \bar "||"
  \mark \markup{\box Verse}
  % \break
  a4) r8 c r e r c\glissando |
  d8 d d d d4\staccato r8 d16 ( dis |
  e8) e e e e e r d16 ( dis |
  e8) e e e ~ e d c b |
  a4 r8 c r e r c\glissando ( |
  d ) d d d d4\staccato r8 d16 ( dis |
  e8) e e e e e r d16 (dis |
  e8\staccato) e'\staccato d\staccato c\staccato b\staccato a\staccato g16 fis e d |
  c8 c r g c g r e |
  a8 e a e a4\staccato r8 fis |
  b8 fis' b fis, b fis b a ( |
  % \break
  a4.) r8 r2 |
  r2 r8 e g gis |
  \bar "||"
  % \break
  \mark \markup{\box "Second Verse"}
  a4 r8 c r e r c\glissando ( |
  d8) d d d d4\staccato r8 d16 (dis |
  e8 ) e e e e e r d16 ( dis |
  e8) e e d16 dis e8 d c b |
  % \break
  % top image 011
  a4 r8 c r e r c |
  d4\staccato d8 d d4\staccato r8 d16 ( dis |
  e8) e e e e e r d16 (dis |
  % \break
  e8\staccato ) e'\staccato d\staccato c\staccato b\staccato a\staccato g16 fis e d |
  c4\tenuto r8 g c g c r |
  a8 e a e' a4\staccato r8 fis, |
  % \break
  b8 fis' b fis, b fis b a ( |
  a4. ) r8 r2 |
  r1_\markup{\italic "Bass is tacet"}
  \bar "||"
  % \break
  \bar "||"
  \mark \markup{\box Bridge}
  r1
  r1
  r1
  % \break
  % top img012
  r2 r4 r8 c-> ~ |
  c4. b8-> ~b4 g4\staccato |
  d'2 ( d8 ) d\staccato r c-> ~ |
  % \break
  c4. b8->~ b4 g4\staccato |
  d'2 (d8) d\staccato r c-> ~ |
  c4. b8-> ~ b4 g4\staccato |
  % \break
  d'8 d d d  d d d c-> ~ |
  c4. b8-> ~ b4 g |
  d'8 d d d d d d c-> ~ |
  % \break
  c4. b8-> ~ b4 g8 c16 cis |
  d8 d' d c,16 cis d8 d d cis-> ( |
  \bar "||"
  \mark \markup{\box "guitar solo"}
  c4.) b8-> ~ b4 g8 c16 cis |
  % \break
  %  img 013
  d8 d d' d d a d,\staccato c-> ~ |
  c8 c c b-> ~ b b g \override NoteHead.style = #'cross c16 cis \revert NoteHead.style |
  d8 d'16 d r d d r d d d, d d8\staccato c-> ~ |
  % \break
  c4. b8-> ~ b4 g |
  d'4\glissando a'8\glissando a g fis d c-> ~ |
  c c c b ~ b r g4\staccato |
  % \break
  d'4 ~ d16 a' d d d c a g f! d c8 ~ |
  c4 c8 b-> ~ b4 g4\staccato |
  d'4\staccato r16 \override NoteHead.style = #'cross a' c \revert NoteHead.style d d c a g f! d c8-> ~ |
  % \break
  c8 c c b-> ~ b b16 a g4\staccato |
  d'2 d8 d d  c-> ~ |
  c8 c c b-> ~ b4 g\staccato |
  % \break
  % top img 014
  d'8 d d d d des c b |
  \bar "||"
  % \break
  \mark \markup{\box "Third Verse"}
  a4. e'16 g r g a r a,8 r |
  d4. g16 a r c d r g, a r d, |
  % \break
  e8 e e e e e r b\glissando ( |
  e8 ) e e e e d c b |
  a4. e'8 g16 gis a8\staccato a,4 |
  % \break
  d4. g16 a r c d r g, a8\staccato d,16 |
  e8 e e e e e r b\glissando ( |
  e ) e e' d c b a16 g e d |
  % \break
  c4 r8 g c g r e |
  a e a e a4\staccato r8 fis |
  b8 b b fis b fis b a-> ~ a1\fermata^\markup{Fine} |
  \bar "|."
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  % \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } 
%   \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout {
    \omit Voice.StringNumber
    \context {
      \Score
            \consists #(bars-per-line-engraver '(4))
    }
  }
 }
