\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Horen, Lezen, Spelen - Dwarsfluit"
  subtitle = "ex 4.11 - Guten Abend, gut' Nacht"
  composer = "Johannes Brahms (1833-1897)"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  system-system-spacing =
    #'((basic-distance . 20)
       (minimum-distance . 8)
       (padding . 1)
       (stretchability . 60))
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()

  }
}



global = {
  \key es \major
  \time 3/4
  \tempo "Andante"
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c' {
   \global
  r1*3/4
  r2 g''8\p g
  \bar "||"
  bes4. (g8) g4
  bes2 g8 (bes)
  \break
  es4 d4. (c8)
  c4 (bes)\breathe f8 (g)
  as4 (f) f8 (g)
  as2 f8 (as)
  \break
  d8 (c) bes4 d
  es2 \breathe es,8\mf es
  es'2 c8 (as)
  bes2 g8 (es)
  \break
  as4 (bes c)
  g8 bes ~ bes4 \breathe es,8 es
  es'2 c8 ( as)
  bes2 g8 (es)
  \break
  as4 ( g f)
  es2.
  \bar "|."
   }

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef treble \contrabas }
  \layout {
    }
}