\version "2.19.44"
\language "nederlands"

\header {
  title = "Übungsmusik für kontrabaß"
  subtitle = "aus orgelwerken von J.S. Bach"
  subsubtitle = "1. Teil"
  arranger = "Heinrich Schneikart"
  tagline = "Verlag Doblinger - wien-München (03 906)"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
}

contrabass = \relative c {
  \global
  % Music follows here.
  r8 c'8\upbow-4 b-2 a-1 b-4 g a b-2 |
  c8 a-1 g fis-2 g-4 e-1 f! g |
  a8 f e d e c d e |
  \break
  f8-1 d-4 c-2 b c a b c |
  d,8 e r g-4 (g) f!16-2 e f8 a |
  d8 r r f (f) e16 d e8 g-4 |
  c,8 f16-2 e f8 a (a) g r e |
  \break
  a,8 a' g-4 fis g e-1 f! g |
  a8 f e d e c d e |
  f8-1 g-4 a4-1 (a8) g16-4 f e8-1 d |
  e8 c f4 (f8) e16 d e8 c |
  \break
  a4 b c8 e d c |
  b8 e, e'4 (e8) d16 c b8 a |
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout {
  }
  \header {
    piece = \markup{\bold \underline "Nr .3"}
  }
}
