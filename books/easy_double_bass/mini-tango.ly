\version "2.18.2"
\language "nederlands"
%#(set-global-staff-size 26)
\header {
  title = "mini tango"
  instrument = "contrabas"
  composer = "Ben Faes"
  meter = " " % "Liberamente"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  ragged-last-bottom = ##f
}

global = {
  \key c \major
  \numericTimeSignature
  \time 4/4
  \tempo "Liberamente" 4=110
}

contrabass = \relative c {
  \global

  % Music follows here.
  a2^\markup { \teeny "H.P." } \p g4 \(f\) | % 1
  g1 \breathe | % 2
  e'2 d4 (c) |  % 3
  g'2. (f4) | % 4
  e1 \breathe | % 5
  %\break
  a,2 g4 (f) | % 6
  g1 \> | % 7
  a1 ( | % 8
  a4)^\markup { \italic \teeny "count" } r8\! e'8\upbow^\markup { \bold "Andante" } \mp (b') r e,4\downbow (| % 9
  a1 ~| % 10
  %\break
  a4) a^\markup { \teeny "II" } (b c) | % 11
  b1 (    | % 12
  b4) d,^\markup { \teeny "I" } (e f) | % 13
  b,4. (c8) d2 ( |%14
  d4) b e (gis,) | % 15
  %\break
  a1 | % 16
  r4 r8 e'8\upbow\mp (b') r e,4\downbow ( | % 17
  a1 ~ | % 18
  a4) a (b c) | % 19
  b1 ( | % 20
  %\break
  b4) d, (e f) % 21
  b,4.\< (c8) d4. (e8) | % 22
  fis4. (gis8) a4. (b8\!) | % 23
  c1\f | % 24
  r4 r8 e,8\upbow\ff (b') r e,4\mp\downbow ( | % 25
  %\break
  a1 ( % 26
  a4)) f ( g a) % 27
  e1 ( % 28
  e4) c (d e) % 29
  f2 g4 ( f) | % 30
  %\break
  e1\>
  a1
  r1\! | % 33
  c1\downbow\mf ( | % 34
  c4) c (d e) | % 35
  d1 ( | % 36
  d4) f, ( g a) | % 37
  gis4 r r2 |
  r2 e,\sf\downbow | % 39
  \repeat percent 4 {a4. e'8 (a4) e} | % 40
  \repeat percent 2 {d4. f8 (a4 f)} | % 44
  e4. gis8 (b4) gis | % 46
  e4 d c b | % 47
  %\break
  a4. e'8 (a4) e | % 48
  g4 a\tenuto e\tenuto g\tenuto | % 49
  f4. (g8) a2 (
  a4) a4\< ( b c) | % 51
  %\break
  c1
  b1
  a1\!
  r4 r8 e8\upbow\mf (b') r e,4\downbow (
  (a1
  %\break
  a4)) fis (gis a) | % 57
  e1 (
  e4) cis (d e)
  fis2\< gis4 (a)
  %\break
  gis2 e\! | % 61
  b'4.\p_\markup{\italic "sub."} (a8) a2 (
  a1)
    %\break
  b4. (a4) a2 (

  a1\flageolet)
  a1\flageolet (
  a1\upbow\flageolet) (
  a1\downbow\flageolet\>) (
  a1\flageolet\!)
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
  } { \clef bass \contrabass }
  \layout { indent = 0.0\cm  \override SpacingSpanner #'common-shortest-duration = #(ly:make-moment 1 2) }
}
