\version "2.19.84"

\header {
  title = "Incredibly Useful Exercises"
  composer = "Dennis Whittaker"
}

\paper {
  indent = 0.0\cm
}

global = {
  \time 4/4
  \set Score.markFormatter = #format-mark-box-alphabet
}

\include "parts/silence.ly"


centering = \relative c {
  \global
}

tonalization = \relative c {
  \global
}

glowingTones = \relative c {
  \global
}

trampolines = \relative c {
  \global
}

\include "parts/breathingscales.ly"

stringCrossingCurves = \relative c {
  \global
}

\include "parts/bowtuners.ly"

longTones = \relative c {
  \global
}

colorAndDynamics = \relative c {
  \global
}

sixthPositionHarmonicSongs = \relative c {
  \global
}

quickAndDead = \relative c {
  \global
}

tempoAndDynamics = \relative c {
  \global
}

tapers = \relative c {
  \global
}

ribbits = \relative c {
  \global
}

digItUp = \relative c {
  \global
}

spiccato = \relative c {
  \global
}

chompers = \relative c {
  \global
}

silence = \relative c {
  \global
}

scoreASilence = \new Staff { \clef bass \Silence }
scoreABreathingScales = \new Staff { \clef bass \breathingScales }
scoreAStringCrossingCurves = \new Staff { \clef bass \stringCrossingCurves }
scoreABowTuners = \new Staff { \clef bass \bowTuners }
scoreALongTones = \new Staff { \clef bass \longTones }
scoreAColorAndDynamics = \new Staff { \clef bass \colorAndDynamics }

\score {
  \scoreASilence
  \layout { }
  \header { piece = \markup{ \bold \underline "Silence"} }
}

\score {
  \scoreABreathingScales
  \layout { }
  \header { piece = \markup{\bold "Breathing Scales" }
  }
}

\score {
  \scoreAStringCrossingCurves
  \layout { }
  \header { piece = \markup{\bold "String Crossing Curves" }
  }
}

\score {
    \scoreABowTuners
  \layout { }
  \header { piece  = \markup{\bold "Bow Tuners"} }
}

\score {
    \scoreALongTones
  \layout { }
}
\score {
    \scoreAColorAndDynamics
  \layout { }
}


\score {
  \scoreASilence
  \layout { }
  \header { piece = \markup{\bold "Silence"}
  }
}
