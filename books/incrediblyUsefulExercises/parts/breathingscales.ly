breathingScales = \relative c {
  \global
  \key g \major
  g1\downbow_\markup{Exhale}
  a\upbow_\markup{Inhale}
  b\downbow
  c\upbow
  d
  e
  fis
  g
  a
  b
  c
  d
  \break
  \clef treble
  e\downbow
  fis
  g
  a
  b
  c
  d
  e
  fis
  g
  fis
  e
  \break
  d\downbow
  c
  b
  a
  g
  fis
  e
  \clef bass
  d
  c
  b
  a
  g
  \break
  fis\downbow
  e
  d
  c
  b
  a
  g
  g\fermata_\markup{Center}
  g2\downbow_\markup{Exhale}\downbow (a)
  b\upbow_\markup{Inhale} (c)
  d (e)
  fis (g)
  \break
  a\downbow (b)
  c (d)
  \clef treble
  e (fis)
  g (a)
  b (c)
  d (e)
  fis (g)
  fis (e)
  d (c)
  b (a)
  \break
  g\downbow (fis)
  e (d)
  \clef bass
  c (b)
  a (g)
  fis (e)
  d (c)
  b (a)
  g1\fermata\upbow_\markup{Center}
  \bar "||"
  \time 3/4
  g4\downbow_\markup{Exhale} (a b)
  c\upbow_\markup{Inhale} (d e_\markup{etc.})
  \bar "||"
  \break
  \time 4/4
  g,\downbow_\markup{Exhale} (a b c)
  d\upbow_\markup{Inhale} ( e fis g)
  \bar "||"
  \tuplet 5/4 {g,4\downbow_\markup{Exhale} (a b c d)} |
  \tuplet 5/4 {e\upbow_\markup{Inhale} (fis g a b)} |
  \tuplet 3/2 {g,4\downbow_\markup{Exhale} (a b} \tuplet 3/2 {c d e)} |
  \tuplet 3/2 {fis4\upbow_\markup{Exhale} (g a} \tuplet 3/2 {b c d_\markup{etc.})} |
  \break
  \tuplet 7/4 {g,,4\downbow_\markup{Exhale} (a b c d e fis)} |
  \tuplet 7/4 {g\upbow_\markup{Inhale} (a b c d e fis)} |
  \bar "||"
  g,,8\downbow_\markup{Exhale} (a b c d e fis g) |
  g8\upbow_\markup{Inhale} (a b c d e fis g_\markup{etc.}) |
  \bar "|."
  %\pageBreak
}