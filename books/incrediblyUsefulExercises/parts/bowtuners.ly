bowTuners = \relative c {
  \global
  \key c \major
  \mark \default
  d2\flageolet_\markup{Sol}^\markup{\bold I} d_\markup{D} |
  \clef treble
  d'2\flageolet-+^\markup{\bold IV} d |
  d'2\flageolet-+^\markup{\bold VI} d |
  d,2\flageolet-+^\markup{\bold IV} d |
  \clef bass
  d,2^\markup{\bold I} d |
  \clef treble
  d''2^\markup{\bold VI} d |
  \clef bass
  d,,2^\markup{\bold I} d\fermata |
  \break
  \bar "||"
  \mark \markup{\box \bold A1}
  d4-0 e^\markup{\bold 1} d-0 d |
  \clef treble
  d'\flageolet-+ e^\markup{\bold 1} d\flageolet-+ d
  d'\flageolet-+ e^\markup{\bold 1} d\flageolet-+ d
  d, e d d
  \break
  \clef bass
  d, e d d
  \clef treble
  d'' e d d
  \clef bass
  d,, e d d
  d2 d\fermata_\markup{Center}
  \bar "||"
  \break
  \mark \markup{\bold \box A2}
  d4-0 e-1 fis-2 g-4
  g fis e d
  \clef treble
  d'\flageolet-+ e-1 fis-2 g-3
  g fis e d
  d'\flageolet-+ e-1 fis-2 g-3
  g fis e d
  d,\flageolet-+ e-1 fis-2 g-3
  g fis e d
  \clef bass
  d,\flageolet-+ e-1 fis-2 g-3
  g fis e d
  d1\fermata_\markup{Center}
  \bar "||"
  \break
  \mark \default
  g2\fermata_\markup{Sul} g
  \clef treble
  g'\fermata-+ g
  g'\fermata-+ g
  g,\fermata-+ g
  \clef bass
  g, g
  \clef treble
  g'' g
  \clef bass
  g,,2 g\fermata_\markup{Center}
  \bar "||"
  \break
  \mark \markup{\box \bold B1}
  g4-0 a-2 g-0 g
  \clef treble
  g'\flageolet-+ a-1 g\flageolet-+ g
  g'\flageolet-+ a-1 g\flageolet-+ g
  g,\flageolet-+ a-1 g\flageolet-+ g
  \clef bass
  \break
  g,\flageolet-+ a-1 g\flageolet-+ g
  \clef treble
  g'\flageolet-+ a-1 g\flageolet-+ g
  \clef bass
  g,\flageolet-+ a-1 g\flageolet-+ g
  g2 g\fermata_\markup{Center}
  \bar "||"
  \break
  \mark \markup{\bold \box B2}
  g4-0 a-1 b-2 c-4
  c b a g
  \clef treble
  g'\flageolet-+ a-1 b-2 c-3
  c b a g
  g'\flageolet-+ a-1 b-2 c-3
  \break
  c b a g
  g, a b c
  c b a g
  \clef bass
  g, a b c
  c b a g
  \clef treble
  g'' a b c
  c b a g
  \clef bass
  g,, a b c
  c b a g
  g1\fermata_\markup{Center}
}
