\version "2.23.5"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Get on de Train"
  instrument = "Bass"
  arranger = "Sheila M. Nelson"
  composer = "Traditional"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

march = \relative c {
  \key g \major
  \numericTimeSignature
  \time 4/4
  \tempo "Allegro moderato" 4=110
  \compressMMRests

  \repeat volta 2 {
    g'4\f fis g d |
    R1
    a'4 gis a e |
    a4 gis a fis |
    g4 fis g d |
    \break
    R1
    a'4 gis a e |
    c'4 a8 r es4 d8 r
  }
  \repeat volta 2 {
    g,4\p d'8 (c) b g a b |
    \tuplet 3/2 {b8 b b} b8. (e16\staccato) d8 b a g |
    c2 d4 c8 b |
    \break
    e4 d8 c d (b) a g |
    fis'4 e g d |
    c4\< b a b\! |
    d4\mf e fis g8 a |
    g8 g4 fis8 g8 d b a |
  } \break
  \repeat volta 2 {
    b8\mp b4 (e8\tenuto) d8 b a g |
    g2 b |
    \tuplet 3/2 {a8 c e} \tuplet 3/2 {a c e} a2 |
    R1 |
    \break
    \tuplet 3/2 {g,,8 b d} \tuplet 3/2 {g b e} g2 |
    a,4\upbow gis a e |
    fis8\f a4 g8 fis e d c |
  }
  \alternative {
    {c8 r d4 es d}
    {b4 d b r}
  }
  \bar "|."
}

\score {
  \new Staff \with { }
  { \clef bass \march}
  \layout { }
}