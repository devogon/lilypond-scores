\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Ostrich Waltz"
  instrument = "bass"
  arranger = "arr: Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key d \major
  \time 3/4
  \tempo "Allegretto" 4=110
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  } 
}


ostrichWaltz = \relative c {
  \global
  \repeat volta 2 {
    \partial 4 { r4 } |
    d4\mp a r |
    d4 (fis) d'8 cis |
    b4 ( a g\!)   |
    g4\> (fis) r\! |
    d4 a r |
    \break
    r4 d8\< e fis g a b cis b a g\! | %  6
    fis2\>
  }
  \repeat volta 2 {
    r4\! g8\p g b4 r |
    fis8 fis a4 r |
    d,4\< e (fis\!) |
    \break
    a,8\> a a'4\! d8\mf (cis) |
    d8 b g b d cis |
    d8 a fis a d b |
    cis\< e b8 b cis e, |
    fis gis a4\! r |
    d,4\mp a r |
    d4 (fis) d'8 cis | % 18
    \break
    b4\< (a g\!) |
    g4\> (fis) r\! |
    d4 a r |
    r4 d8\< e fis g |
    a b cis b a g\! |
  }
  \alternative {
    {fis2\>}
    {fis2\! r4}
  }
  \break
  \repeat volta 2 { % 25
    g,8\f g b2 |
    c!8 c! e2 |
    d8 c! d (c b a) |
    g4 (b2) |
    g8 g b2 |
    c!8 c e2 |
    \break
  }
  \alternative {
    {d4 d d8 fis g2 fis4}
    {e8 e g2}
  }
  g4\>^\markup{\bold rall.} fis8 e d e\! |
  d4\mp^\markup{\bold "a tempo"} a4 r |
  d4 (fis) d'8 cis |
  \break
  b4\< ( a g\!) |
  a4\> (fis\!) r |
  d4 a r |
  r4 d8\< (e fis g |
  a b cis b a g\!) |
  fis2\f
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \ostrichWaltz }
  \layout { }
}

