\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "tops and tails"
  instrument = "bass"
  arranger = "Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key d \major
  \time 4/4
  \tempo "vivace" 4=150
}

topsAndTails = \relative c {
  \global
				% Music follows here.
  \repeat volta 2 {
    fis2\f_\markup{repeat p} fis4 fis |
    g4 g r2 |
    e2 b'4 b |
    fis4 cis4 d  r |
    fis2 fis4 fis |
    g4 g r2 |
    r2 cis8 b a g |
    fis4 g8 g fis4 r^\markup{\right-align \bold {Fine}}  |
  }
  d2\mf fis4 fis |
  d4 d r2 |
  b2 e4 4 |
  d4 d r2 |
  fis2 fis4 fis |
  a4 a r2 |
  fis2\< a8 b cis d\! |
  cis4\> b a r\!
  d,2\p d8 e fis e |
  d4 d r2 |
  b2 g'8 fis e4 |
  d4 d r2 |
  e4 4 d8 e fis g |
  a4 a a8 g fis4 |
  b8\< a g fis e fis g4 |
  fis8 g a g fis4 r\!_\markup{\right-align \bold "D.C. al Fine"} |
  \bar "||"
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \topsAndTails }
  \layout { }
}

