\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Tempo  di Menuetto"
  instrument = "bass"
  composer = "F.J. Haydn"
  arranger = "arr: Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key g \major
  \time 3/4
  \tempo "" 4=96
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  } 
}


gavotte = \relative c {
  \global
  \compressMMRests
  \repeat volta 2 {
    R1*2*3/4
    r4 r b'8\mf d |
    b4 (a) g |
    R1*2*3/4
    d4 g (fis) |
    \break

    g4 r r | % 8
    r4 g e |
    a4 r fis\upbow |
    b r g |
    a e\staccato (a,\staccato) |
    a'2\f g8 e |
    \break

    d8 (fis) e d cis  a | % 14
    a'2\p g8 e |
    d8 (fis e d cis a') |
    b4\f b8 a b a |
    g4 e d |
    d8 b' a fis g e |
    \break

    d4 r r | % 20
    e'4 e e8 cis |
    a8 (fis) e g fis4 |
    e4\p e e8 cis |
    a8 (fis') e g fis4 |
    d4_\markup{\italic cresc.} e fis |
    g a b |
    \break

    cis d e |
    fis\f fis fis |
    fis2. |
    b,4. (g8) fis e |
    fis a g b a d |
    e,8 g fis e d cis |
    d4 r r |
  } \break

  \repeat volta 2 {
    d'2\p e8 b | % 34
    c2 d8 a |
    b2 d8 f,! |
    e4 r r |
    e4 r r |
    e4 r r |
    \break

    r4 e8 c' e, c | % 40
    b4 (a) r |
    R1*3/4*2
    r4 r b'8\mf d |
    a2 g4 |
    R1*3/4 |
    \break

    a4\upbow g g | % 47
    g8 f! e4 e8 c |
    b4 a2 |
    a'4 a a8 (fis) |
    d8 (b') a c b4 |
    fis4\p a a8 fis |
    \break

    d8 (b') a c b4 |  % 53
    g,4\staccato_\markup{\italic cresc.} a\staccato b\staccato |
    c\staccato d\staccato e\staccato |
    fis\staccato g\staccato a\staccato |
    b\f b b |
    b2. |
    e,4. (c8) b a |
    \break

    b4 r r | % 60
    d'2 c8 a |
    g8 (b) a g fis d |
    d'2\p c8 a |
    g8 (b) a g fis d |
    g8 (fis) e d e d |
    \break

    c4 a' (g) | % 66
    g8 (e) d b c a |
    b8 d c e d g |
    e c' b a g fis |
    b,8 d c e d g |
    \tuplet 3/2 {g8 fis e} \tuplet 3/2 {d8 c b} \tuplet 3/2 {a b c} |
    b4 g r
  }
   \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \gavotte }
  \layout { }
}

