\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Polly Oliver"
  instrument = "bass"
  arranger = "Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key g \major
  \time 3/4
  \tempo "Vivace" 4=150
}

pollyOliver = \relative c {
  \global
  \partial 4 {r4} |
  a2\mp c4 |
  d2 e4 |
  d4 c a |
  d c b |
  b'2 e4 |
  cis4 b r |
  b2 a8 (g) |
  \break
  fis2 d4\mf |
  e8 fis g4 a |
  fis4 d8 (cis) b a |
  g4. (a8) b4 |
  e2 d'4\f |
  d c b |
  a g fis |
  \break
  e2 d8 (c) |
  b2 r4 |
  r4 b'2\mp |
  b4 b2 |
  r4 r g |
  fis2 g4\mf |
  \break
  e8 e r e\upbow fis g |
  a fis d d e fis |
  g a b4 a8 (g) |
  fis8\> g a fis g a\! |
  g4\mp e'8 d cis4 |
  d4 a2 |
  \break
  r4 b\< g |
  g2 a4 |
  d4\!\f c b8 g |
  fis4\> e r |
  d2 d4\! |
  d8\< c b4 fis'8 (a\!) |
  \break
  g4 c b |
  a4. (fis8) g (a) |
  d,4\> e d8 c\! |
  b4.\< (c8) d4\! |
  e8\> c a4 c\! |
  b2. ( |
  b4) r
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \pollyOliver }
  \layout { }
}

