\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Gavotte"
  instrument = "bass"
  composer = "William Boyce"
  arranger = "arr: Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key g \major
  \time 4/4
  \tempo "Moderato" 4=96
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  }
}


gavotte = \relative c {
  \global
  \repeat volta 2 {
    \partial 4 {r4} % 1
    d2\mp r4 fis | % 2
    b,2 r4 d'4 | % 3
    g,2 r4 d' | % 4
    fis,4 g r2 | % 5
    d2 r4 fis | % 6
    \break
    b,2 r4 e'4 | % 7
    a, g fis e | % 8
    fis2. % 9
  }
  \repeat volta 2 {
    d8\mf (e)
    fis8 g a fis fis (e) fis (cis) |
    d4 d' d a8 (fis) |
    \break
    b8 cis d b b (a) d (fis,) |
    g4 fis r d' |
    g,2 r4 d' |
    fis,2 r4 r8 c'\p |
    b8 g d' b g (d') c (b) |
    \break
    c4 a a4. r8 | % 16
    d,2\mf r4 a' |
    d,2\mf r4 a' |
    d,2\mf r4 b'8 g |
    g4 fis r a |
    \break
    b8_\markup{\italic cresc.} c d b b (a) b (fis) |
    g4 r r a  |
    d,2\f r4 fis |
    b,2 r4 d' |
    g,2 r4 e |
    \break
    a4 fis r a |
    d,2 r4 a' |
    d,2 r4 a' |
    d,4 c b a |
    b2.
  }

}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \gavotte }
  \layout { }
}

