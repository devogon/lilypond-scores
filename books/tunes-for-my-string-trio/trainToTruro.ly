\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Train to Truro"
  instrument = "bass"
  arranger = "Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}

global = {
  \key d \major
  \time 2/4
  \tempo "Lento" 4=60

}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  } 
}


traintotruro = \relative c {
  \global
\compressMMRests
  R2*8
  \tempo "accel poco a poco" 4=69
  fis8\p_\markup{\italic "cresc. poco a poco"}  g a fis |
  d'8 cis d cis |
  b a b a |
  g fis fis a |
  fis g a fis |
  d' cis d cis |
  \break
  b8 a b a |
  g8 (a) fis4 |
  \tempo "accel. poco a poco" 4=80
  fis8\mp_\markup{\italic cresc.} g a fis |
  d'8 cis d cis |
  b8 a b a |
  g fis g fis |
  fis g a fis |
  d' cis d cis |
  \break
  b8 a b a |
  g8 (a) fis4 |
  \tempo 4=108
  b,8\mf_\markup{\italic cresc.} c! d b |
  e d e d |
  c!8 d e c! |
  fis8 e fis e |
  d8 e fis d |
  g8 fis g fis |
  \break
  e8 fis g a |
  b8 b r b\upbow |
  \tempo 4=120
  b,8\f c! d b |
  e d e d |
  c! d e c! |
  fis e fis e |
  d e fis g |
  \break
  a8 b cis d | % 38
  e d e b |
  cis (b) a4 |
  fis8_\markup{\italic "dim. al fine"}^\markup{\bold "Poco a poco meno mosso"} g a fis |
  d' cis d cis |
  b a b a |
  g fis g a |
  \break
  fis g a fis | % 45
  d' cis d cis |
  b a b a |
  g (a) fis4 |
  fis8 g a fis |
  d' cis d cis |
  b a b a |
  \break
  g fis g a | % 52
  fis g a fis |
  d' cis d cis |
  b a b a |
  g4 a |
  fis2\p^\markup{\bold Lento} ( |
  fis8) r r4 |
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \traintotruro }
  \layout { }
}

