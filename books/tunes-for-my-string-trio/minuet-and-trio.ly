\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Minuet and Trio"
  instrument = "bass"
  composer = "W.A. Mozart"
  arranger = "arr: Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key c \major
  \time 3/4
  \tempo "Allegretto" 4=96
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  } 
}


gavotte = \relative c {
  \global
  \repeat volta 2 {
    \partial 4 {r4} % 1
    g'2.\p ( |
    g2. |
    g4) b (c) |
    c4 (b) g\f (|
    g4) c4\staccato d\staccato |
    e4\staccato e\staccato r |
    \break

    c4\staccato c\staccato r |
    b8\p (d b d b d) |
    c8 (d c d c d) |
    b8 (d b d b d) |
    c8 (d c d c d) |
    b4 r
  } \break

  
  \repeat volta 2 {
    <g g'>\f |
    <d d'>\staccato (<c c'>) <g' g'>\staccato |
    <bes, bes'> (<a a'>) <g' g'>\p (~ |
    <g g'> <f f'>) <e e'> (~|
    <e e'> <d d'>) c'4 ( |
    b c ) d (~|
    d c bes) |
    a2 (f4) |
    \break

    e4 (d) r |
    g2.\downbow |
    gis2. |
    a4 (b) c (~ |
    c b) g\staccato\f |
    c\staccato c\staccato c\staccato |
    c2. |
    \break

    % 27
    c4. (e8) d (b) |
    b8 (\p g e g e g) |
    f8 (g f g f g) |
    e (g e g e g) |
    f (g f g f g) |
  }
  \alternative {
    {e4 r}
    { e4 r r}
  } \break
  \bar "||"
  \key f \major
  \mark "TRIO"
  \repeat volta 2 {
    c'2.\f ~ |
    c4 r r |
    bes2.\p ( |
    a4) r r |
    c2.\f ~ |
    c4 r r |
    \break
    bes4\p bes bes |
    a r r |
  }
  \repeat volta 2 {
    a2.\downbow\f ~ |
    a2. |
    g,8\p (bes! a c) bes! (d) |
    \break

    c8 (e) d (f) e4 |
    c'2.\f ( |
    d4) r r |
    bes4\p bes bes |
  }
  \alternative {
    {a4 r r}
    {a4_\markup{\italic "Menuetto da Capo"} r}
  }
  \bar "||"
%   \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \gavotte }
  \layout { }
}

