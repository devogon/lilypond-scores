\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "March"
  instrument = "Bass"
  arranger = "Sheila M. Nelson"
  composer = "G.P. Telemann"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

march = \relative c {
  \key g \major
  \numericTimeSignature
  \time 4/4
  \tempo "Allegro moderato" 4=110
  \compressMMRests

  \repeat volta 2 {
    b'2\downbow\f fis2 |
    e2. e4 |
    b'4 b d, fis |
    g2 d4\staccato (fis\staccato) |
    g8 b a g fis a g fis |
    g b a g fis a g fis |
    \break
    g2. b4 | % 7
    a4 fis e cis |
    d8 fis e d cis e d cis |
    d8 fis e d cis e d cis |
    d4 d d cis |
  }
  \alternative {
    {d1     \break}
    {d1} % 13
  }
  \repeat volta 2 {
    a'4\downbow\mf fis fis a |
    g2. b4 |
    d b b d |
    c2 g4\staccato (b\staccato) |
    c8 e d c b d c b |
    \break
    c8 e d c b d e c | % 18
    c2 b |
    e,2. d4 |
    c4 c' b g8 f! |
    e4 e8 f! g4 g\p |
    \break
    g4 g fis g | % 23
    fis2 fis |
    g2. fis4  |
    e2. g4 |
    c,2. d4 |
    \break
    fis2 r | % 28
    b2\f fis |
    e2. e4 |
    a4 a d, fis |
    g2 d4\staccato (fis\staccato) |
    \break
    g8 b a g fis a g fis |
    g8 b a g fis a g fis |
    g4. (g8\tenuto) fis4 d8 c |
  }
  \alternative {
    {b1}
    {b1}
  }
  \bar "|."
}

\score {
  \new Staff \with { }
  { \clef bass \march}
  \layout { }
}