\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Sofka and Sam"
  instrument = "bass"
  arranger = "Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key d \major
  \time 6/8
  \tempo "Allegretto" 4=100
}

sofkaAndSam = \relative c {
  \global
  \repeat volta 2 {
  d'4.\mp cis4. |
  fis,8 ( a fis) e (a e) |
  d'4. cis4. |
  d4. e4. |
  d4. cis4. |
  fis,4. g4. |
  \break
  b8 ( d, b') a (d a) |
  g8 (fis e) e (fis g)|
  fis8\< (g a) a (g fis) |
  b8 ( d, b') a (d, a')\! |
  e8\> (fis g) g (fis e) |
  fis8 (e fis) d4.\!^\markup{\right-align Fine} |
} \break
  \repeat volta 2 {
    b4.\mf d4 (d8\tenuto) |
    b2. |
    c!4. e4 (e8\tenuto) |
    c!2. |
    c!4.\< c!4. |
    c!4. c!4.\! |
    \break
    b4\> (c!8) d4 (c!8) |
    b2. |
    b4.\!\p d4 (d8\tenuto) |
    a2. |
    c!4.\mp e4 (e8\tenuto) |
    c!2. |
    \break
    d4.\< d4. |
    fis4 (fis8\tenuto) g4 (g8\tenuto) |
  }
  \alternative {
    {e4 e8\tenuto e4 (g8\!) fis4.\> d4.\!}
    {a'4. g4. fis8\< (g a) a (b cis)\!^\markup{\right-align \bold{D.C. al Fine}} }
  }
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \sofkaAndSam }
  \layout { }
}

