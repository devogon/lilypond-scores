\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "The Lodestars"
  instrument = "bass"
  composer = "William Shield"
  arranger = "arr: Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key g \major
  \time 4/4
  \tempo "vivace" 4=150
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  } 
}


lodestars = \relative c {
  \global
  r1
  r2 g'2\upbow\mp |
  e4 d c b |
  g'4 (fis) r2 |
  r2 r4 d |
  g4 fis e d |
  \break
  e2 c |
  b2. r4 |
  r2 r4 g'4\p |
  fis4. (e8) fis4\staccato (d\staccato) |
  e2 cis' |
  d4 a fis r |
  r2 r4 a |
  \break
  a4. (g8) fis4 r |
  r2 r4 a |
  a4. (g8) fis4\staccato (d'\staccato) |
  d2 b4\staccato (g\staccato) |
  fis2 b8 (a) b a |
  fis2\mf e |
  fis2 r |
  \break
  r1 % 21
  r2 a2\mp\upbow |
  e4 d c b |
  g'4 (fis) r2 |
  r2 r4 d |
  g4 fis e d |
  e2 c |
  \break
  b2 r | % 28
  r2 b'4\p g |
  r2 a4 fis |
  r2 e4 a |
  g2 (fis4) r |
  r2 r4 b\pp |
  a2 a |
  \break
  a2 (g4) d | % 35
  c'2\< c |
  c4 (b) a g\! |
  g1\> |
  g2 fis |
  g2\! r |
  r2 r4 b |
  \break
  a2_\markup{\italic cresc.} a |
  a2 (g4) d |
  c'2 c |
  c2\f (b4) g |
  g4 b a g |
  g2 g |
  g1
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \lodestars }
  \layout { }
}

