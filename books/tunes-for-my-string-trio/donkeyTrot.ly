\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Donkey Trot"
  instrument = "bass"
  arranger = "Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key d \major
  \time 2/4
  \tempo "Allegro" 4=130
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.S. al Fine"}
%    \line{\small \right-align "no repeat"}
    }
  } 
}


donkeyTrot = \relative c {
  \global
  \repeat volta 2 {
    r2 r2
    d4\mf d |
    b'4 a |
    d,8 e fis a16 a |
    b8 a a4 |
    a8 g fis e |
    fis4 fis
  } \break
%  \mark \segno
  g4\p\segno fis |
  d8 d fis4 |
  g4 fis |
  d8 d fis4 |
  g,8\mp a b d16 d |
  e8 d d4 |
  \break
  r2 r2 r2 r2
  a'8\f g fis e |
  d4 a' |
  a8 b b g |
  fis4 fis^\markup{\right-align \bold "Fine"} |
  \break
  \bar "||"
  fis4\p d |
  d8 d d4 |
  d8 e fis4 |
  b8 a g4 |
  fis4 d |
  d8 fis16 fis g8 a |
  a8 b cis a |
  a8 g fis4_\markup{\right-align \bold "D.S. al Fine"}
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \donkeyTrot }
  \layout { }
}

