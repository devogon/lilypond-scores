\version "2.23.4"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Divertimento"
  instrument = "bass"
  arranger = "Sheila M. Nelson"
  tagline = \date
}

\paper  {
  indent = 0.0\cm
}
global = {
  \key g \major
  \time 4/4
  \tempo "Allegro" 4=130
}

DCfine = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup {
    \column {\line{\right-align \small \bold "D.C. al Fine"}
    \line{\small \right-align "no repeat"}
    }
  } 
}


divertimento = \relative c {
  \global
  \repeat volta 2 {
    r4 g'8\mf a g4 r |
    r4 fis8 g fis4 r |
    r4 g8 a b4 b |
    b4 r r2 |
    r4 a8 a c4 a |
    \break
    r4 g8 g b4 g |
  }
  \alternative {
    {fis1 fis4 r r2}
    {r4 e4 r d b2. r4^\markup{\right-align \bold"Fine"}}
  }
  \bar "||"
  \break
  \repeat volta 2 {
    b2.\p g4 |
    a4 e'8 e b b b4 |
    b2. g4 |
    a4 c b r |
  }
  \alternative {
    {e8\pp e e e e e e e
     e     e a a a a a a
     e\< e e e e e e e\!
     e\> e a a fis4\! r}
    {fis8_\markup{\italic cresc} fis g g a a g g
     fis8 fis d' d d d d d
     d4\f b8 b g4 b
     e4 e, fis8 e d4\DCfine
   }
    }
  \bar "|."
}

\score {
  \new Staff \with {
%    instrumentName = "Viola"
  } { \clef bass \divertimento }
  \layout { }
}

