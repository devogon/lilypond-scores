\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Body and  Soul"
  instrument = "Bass"
  composer = "Jimmy Blanton"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key des \major
  \time 4/4
  \tempo 4=60
}

contrabass = \relative c {
  \global
  r8^\markup{\underline \bold ARCO} es \tuplet 3/2 {f8 e! es} f4 \tuplet 3/2 {es8 f ges} | % 1
  \tuplet 3/2 {bes8 bes bes ~} bes4  a! b!16  as b! as | %  2
  as4 \tuplet 3/2 {bes8 a! as}  \tuplet 3/2 {bes8 r as ~} as8\glissando es' ~ | %  3
  \break
  es4 \tuplet 3/2 {des8 es \tuplet 3/2 {des16 es des}} \tuplet 3/2 {c8 b! bes ~} bes4 | % 4
  r8 des ~ des4 \glissando bes16 ges es a,! bes4 | % 5
  f'4 r8 a,!16 c es ges c es ges4 | % 6
  f4 \tuplet 3/2 {e!8 fis \tuplet 3/2 {es16 f es}} \tuplet 3/2 {es8 f! \tuplet 3/2 {es16 f es}} \tuplet 3/2 {des8 e! \tuplet 3/2 {des16 e des}} | % 7
  \break
  \tuplet 3/2 {des8 es \tuplet 3/2 {des16 es des}} \tuplet 3/2 {b!8 cis \tuplet 3/2 {b16 c b}} \tuplet 3/2 {bes8 b! c}  f,4 | % 8
  \bar "||"
  r8 es \tuplet 3/2 {f e! es} f4 \tuplet 3/2 {es8 f ges} | % 9
  \tuplet 3/2 {bes8 es des} \tuplet 3/2 {des b! \tuplet 3/2 {des16 c bes}} bes16 ges f es a!4 | % 10
  \break
  r8 as \tuplet 3/2 {bes a! as} \tuplet 3/2 {bes8 r as ~} as4 | % 11
  r16 as16 bes c des es f as g! fis32 f e!16 des bes g! e! a, | % 12
  bes4 r16 es f ges bes es des bes ges d! es g! | % 13
  \break
  as4 bes16 as g! as \tuplet 3/2 {a!8 c es} ges4 | % 14
  f16 as ~ as8 e!16 g! ~ g8 es16 ges ~ ges8 d!16 f ~ f8 | % 15
  des4 r16 a,! b! cis e! fis a b  \tuplet 3/2 {c8 e es} |% 16
  \bar "||"
  \break
  \key d \major
  d4 ~ d8 e16 f^\markup{kruis!} a,2 | % 17
  r16 d, e fis a b a b g bes ~ bes8 ~ bes16 bes bes bes |% 18
  a16\glissando d ~ d8^\glissando a16 fis ~ fis8 e16 b' ~ b8 a16 a, ~ a8 |% 19
  d4 \tuplet 3/2 {e8 fis a} d2 |% 20
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
  } { \clef bass \contrabass }
  \layout { }
}
