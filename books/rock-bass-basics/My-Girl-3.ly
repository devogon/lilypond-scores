\version "2.19.83"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "My Girl"
  subsubtitle = "example 3"
  subtitle = "Rock Bass Basics"
  instrument = "Bass"
  arranger = "Tim Bogart & Albert Nigro"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key e \major
  \time 4/4
%  \tempo "Ballad" 4=100
}

electricBass = \relative c,, {
  \global
  % Music follows here.
  e4.^\markup{E}^\markup{\bold Intro} fis8 gis b cis e |
  e,4.^\markup{E} fis8 gis b cis e |
  \repeat volta 2 {
    e,4.^\markup{E}^\markup{\bold Verse} fis8 gis b cis e |
    \break
    a,4.^\markup{A} b8 cis e fis a |
    e,4.^\markup{E} fis8 gis b cis e |
    a,4.^\markup{A} b8 cis e fis a |
    e,4.^\markup{E} fis8 gis b cis e |
    \break
    a,4.^\markup{A} b8 cis e fis a |
    e,4.^\markup{E} fis8 gis b cis e |
    a,4.^\markup{A} b8 cis e fis a |
    \bar "||"
    e,4.^\markup{E}^\markup{\bold Bridge} e8 fis4.^\markup{F\sharp m} fis8 |
    \break
    a4.^\markup{A} a8 b4.^\markup{B} b8 |
    e,4.^\markup{E} e8 fis4.^\markup{F\sharp m} fis8 |
    a4.^\markup{A} a8 b4.^\markup{B} b8 |
    e4.^\markup{\bold Chorus} e,8 e2 |
    \break
    e'4. e,8 e2 |
    fis4.^\markup{F\sharp m} f8 f2|
  }
  \alternative {
    {b8^\markup{B} b a^\markup{A} a gis^\markup{G\sharp m} gis fis^\markup{F\sharp m} fis }
    {b8^\markup{B} b r r2}
  }
  \bar "|."
}

\score {
  \new StaffGroup <<
    \new Staff { \clef "bass_8" \electricBass }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \electricBass
  >>
  \layout { }
}
