\version "2.19.50"
\language "nederlands"

\header {
  title = "Jingle Bells"
  arranger = "James Lord Pierpont"
  meter = "med. fast"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
     ragged-last-bottom = ##f
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=100
}

contrabass = \relative c {
  \global
  % Music follows here.
    % 2 8th notes = quarter + an eighth?

    \repeat volta 3 {
      d4 b' a g | % 1
      d2 r4 d8 d | % 2
      d4 b' a g | % 3
      e2. r4 | % 4
      e4 c' b a | % 5
      fis2. fis4 | % 6
      d'4 d c a | % 7
      b2. d,4 | % 8
      d4 b' a g | % 9
      d2. d4 | % 10
      d4 b' a g | % 11
      e2. e4 | % 12
      e4 c' b g | % 13
      d'4 d d d
      e4 d c a
      g2 d'
      \bar "||"
      b4 b b2
      b4 b b2
      b4 d g,4. a8
      b1
      c4 c c c
      c4 b b b8 b
      b4 a a b
      a2 d2
      b4 b b2
      b4 b b2
      b4 d g,4. g8
      b1
      c4 c c c
      c4 b b b8 b
      d4 d c a
    }
    \alternative {
      {g2. d4 }
      % {g2. d4}
      {g2. r4}
    }
    \bar "|."
}

verseone = \lyricmode {
  Dash -- ing through the snow, in a one horse o -- pen sleigh, o'er the fields we go _
  laugh -- ing all the way. _ Bells on bob -- tail ring, _ ma -- king spir -- its bright, what
  fun it is to ride and sing a sleigh -- ing song to -- night! oh! jin -- gle bells, jin -- gle
  bells, jin -- gle all the way. Oh what fun it is to ride in a one horse o -- pen
  sleigh! _ Jin -- gle bells, jin -- gle bells, jin -- gle all the way. Oh, what fun it is
  to ride in a one horse o -- pen sleigh! A
}
versetwo = \lyricmode {
  day or two a -- go _ I thought I'd take a ride, soon Miss
  Fan -- nie Bright was seat -- ed by my side. the horse was lean
  and lank, mis -- for -- tune seemed his lot, he got in -- to a
  drift -- ed bank and we, we got up -- sot! oh! jin -- gle bells, jin -- gle
  bells, jin -- gle all the way. Oh what fun it is to ride in a one horse o -- pen
  sleigh! _ Jin -- gle bells, jin -- gle bells, jin -- gle all the way. Oh, what fun it is
  to ride in a one horse o -- pen sleigh!
}

versethree = \lyricmode {
  Now the ground is white, _ _ go it while you're young. Take the girls
  to -- night and sing this sleigh -- ing song. Just  get a bob -- tail bay,
  two -- for -- ty for his speed, then hitch him to an o -- pen sleigh and
  crack! you'll take the lead! oh! jin -- gle bells, jin -- gle
  bells, jin -- gle all the way. Oh what fun it is to ride in a one horse o -- pen
  sleigh! _ Jin -- gle bells, jin -- gle bells, jin -- gle all the way. Oh, what fun it is
  to ride in a one horse o -- pen _ _ sleigh!
}
\score {
  <<
  \new Staff \with {
     %instrumentName = "Contrabass"
    %shortInstrumentName = "Cb."
    midiInstrument = "contrabass"
  } { \clef bass \contrabass }
  \addlyrics \verseone
  \addlyrics \versetwo
  \addlyrics \versethree
  %\layout { }
  %\midi { }
  >>
}
