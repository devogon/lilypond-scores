\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Here Comes Santa Claus"
  subtitle = "(Right Down Santa Claus Lane)"
  instrument = "Bass"
  composer = "Gene Autry/Oakley Haldeman"
  copyright = "1947 (Renewed) Gene Autry‘s Western Music Publishing Co."
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
  \tempo "Medium (2 quarter = quarter+eighth)" 4=98
}

chordNames = \chordmode {
  \global
  % Chords follow here.

}

electricBass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 4 {
    bes4 g' f8 f es4 | % 1
    bes4 g' f8 f es4 | % 2
    bes4 as' as8 as g4 | % 3
    \break
    as1 | % 4
    bes,4 as'8 as g4 f8 f | % 5
    bes,4 as' g f8 f | % 6
    \break
    bes,4 bes' bes a! | % 7
    bes1 | % 8
    c4 es d c | % 9
    bes4 d c bes | % 10
    \break
    as4 as \tuplet 3/2 {c4 bes as} | % 11
    g1 | % 12
    c4 es d c8 c | % 13
    bes4 d c g | % 14
    \break
  }
  \alternative {
    {bes8 bes as4 g f es1}
   {bes'8 bes as4 g f es1\fermata}
  }
}

verseone = \lyricmode {
  Here comes San- ta Claus
  Here comes San- ta Claus
  Right down San- ta Claus Lane
  Vix- en and Blit- zen and all of his rein- deer
  Are pulling on the reins
  Bells are ring- ing, child- ren sing- ing:
  All is mer- ry and bright
  Hang your stock- ings and say your prayers
  'Cause San- ta Claus is coming tonight
}
versetwo = \lyricmode {
  Here comes San- ta Claus
  Here comes San- ta Claus
  Right down San- ta Claus Lane
  He's got a bag that is filled with toys
  For the boys and girls a- gain
  Hear those sleigh- bells jing- le jang- le
  What a beaut- i- ful sight
  Jump in bed co- ver up your head
  'Cause San- ta Claus comes to- night.
}

versethree = \lyricmode {
  Here comes San- ta Claus
  Here comes San- ta Claus
  Right down San- ta Claus Lane
  He does- n't care if you're rich or poor, _ he loves you just the same
  Santa Claus knows we're all gods' child- ren, that makes e- very- thing right
  So fill your hearts with Christ- mas cheer
  'Cause San- ta Claus comes to- night.}

versefour = \lyricmode {
  Here comes San- ta Claus
  Here comes San- ta Claus
  Right down San- ta Claus Lane
  Vi- xen and Blit- zen and all of his rein- deer
  Pul- lin' on the reins.
  Bells are ring- in', child- ren sing- in',
  All is mer- ry and bright,
  So jump in bed, and cover your head,
  'Cause _ _ _ _ _ _ San- ta Claus comes to- night.
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
    \addlyrics \verseone
    \addlyrics \versetwo
    \addlyrics \versethree
    \addlyrics \versefour
   %  \lyrics { San- ta Claus comes to- night.}
  >>
  \layout { }
}
