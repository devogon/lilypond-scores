\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "All I Want for Christmas is You"
  instrument = "Bass"
  composer = "Maria Carey / Walter Afanasieff"
  meter = "Med."
  copyright = "yes"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 4/4
  \tempo 4=98
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  g1
  g1:maj7
  g1
  g1:maj7
  c1
  c:6
  c1:m6/es
  s1
  g1
  g1:maj7
}

electricBass = \relative c, {
  \global
  % Music follows here.
  \repeat volta 2 {
    g4 b d g8 g ~ | % 1
    g8 fis4. e8 d4. | % 2
    a'4 g g8 fis4 g8 ~ | % 3
    \break
    g8 fis4 e8 d4 d8 c | % 4
    c4 e g a8 b ~ | % 5
    b8 a4 g8 g e4. | % 6
    \break
    c4 es8 g ~ g4 a8 bes~ | % 7
    bes8 a4 f!8 ~ f es4. |  % 8
    a4 g8 g ~ g4 fis8 g ~ | % 9
    \break
    g8 fis4 e8 ~ e8 d4. | % 10
   }
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new Staff { \clef "bass_8" \electricBass }

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
