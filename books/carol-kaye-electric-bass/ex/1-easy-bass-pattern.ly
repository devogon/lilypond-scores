\version "2.19.83"
\language "nederlands"

% https://www.youtube.com/watch?v=-4jMYOnUnRc

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "easy bass pattern"
  subtitle = "1"
  instrument = "Bass"
  composer = "Carol Kaye"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=100
}

bass = \relative c, {
  \global
  \mark \markup{\circle \bold 1}
  c4 r8 g' bes b! c c, ~ |
  c4. g8 g g bes4 |
  c4 r8 g' bes b! c c, ~ |
  c4 c8 es ~ es c es e! |
  \break
  f4 r8 c es e! f f, ~ |
  f4. c'8 ~ c es f4 |
  c4 r8 g' bes b! c c, ~ |
  c8 e,4 f fis fis8 |
  \break
  g4 r8 d' f fis g f! ~ |
  f4. c8 es es f4 |
  c r8 g' bes b! c c, ~ |
  c8 e,4 f fis g8^\markup{etc.} |
  \bar "||"
}

\score {
  \new Staff { \clef "bass_8" \bass }
  \layout { }
}
