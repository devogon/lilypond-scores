\version "2.23.80"

\header {
  title = "Lesson 4:"
  subtitle = "Warmup Exercises"
  subsubtitle = "(100 rock lessons - bass)"
  composer = "Steve Gorenberg"
}

\paper { indent = 0.0\cm }

global = {
  \key c \major
  \time 4/4
  \tempo moderately
}

exampleOne = \relative c, {
  \global
  % Music follows here.
  \mark \markup{\teeny \box "example 1"}
  \repeat volta 2 {
    f,8 as g as f as g as
  }
  \repeat volta 2 {
    bes8 des c des bes des c des
  }
  \repeat volta 2 {
    es8 ges f ges es ges f ges
  }
  \repeat volta 2 {
    as8 ces bes ces as ces bes ces
  }
  \break

  \mark \markup{\teeny \box "example 2"}
  f,, g fis gis bes c b! cis
  es f e! fis as bes a! b!
  b a bes as fis e f! es
  cis b c! bes fis fis g! f!
  \bar "||"
  \break


  \mark \markup{\teeny \box "example 3"}
  f as bes des g, bes c es
  a, c d f b, d e g
  cis, e fis g g fis e cis
  g' e d b f' d c a
  es' c bes g des' bes as f
  \bar "||"
  \break
  \omit Voice.StringNumber
  \mark \markup{\teeny \box "example 4"}
  \repeat volta 2 {
    a8\4 c\4 a\4 e'\3 d\3 e\3 g\2 a\2 g\2 d'\1 c d
  }
  \break

  \mark \markup{\teeny \box "example 5"}
  \key a \major
  \repeat volta 2 {
      a,8 e' a, a' a, e' a, a'
  }
  \repeat volta 2 {
    d, a' d, d' d, a' d, d'
  }
 \break

\mark \markup{\teeny \box "example 6"}
  \repeat volta 2 {
    a,8_\markup{let ring} e' a e a, e' a e
  }
  \repeat volta 2 {
  d_\markup{let ring} a' d a d, a' d a
  }
  \bar "||"
}

exampleTwo = \relative c, {
  \global
  % Music follows here.
  e f g r
}

exampleThree = \relative c, {
  \global
  % Music follows here.

}

exampleFour = \relative c, {
  \global
  % Music follows here.

}

exampleFive = \relative c, {
  \global
  % Music follows here.

}

exampleSix = \relative c, {
  \global
  % Music follows here.

}



\score {
  \new StaffGroup \with {
    \consists "Instrument_name_engraver"
  } <<
    \new Staff { \clef "bass_8" \exampleOne }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \exampleOne
  >>
  \layout { }
}
