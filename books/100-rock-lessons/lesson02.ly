\version "2.23.80"

\header {
  title = "Lesson 2:"
  subtitle = "Fingerstyle Basics"
  subsubtitle = "(100 rock lessons - bass)"
  composer = "Steve Gorenberg"
}

\paper { indent = 0.0\cm }

global = {
  \key c \major
  \time 4/4
}

exampleOne = \relative c, {
  \global
  % Music follows here.
  \tempo "slow"
  \mark \markup{\teeny \box "example 1"}
  a8 a a a a a a a
  d d d d a a a a
  e e e e a a a a
  d d d d g g g g
  \bar "||"
  \break
  \mark \markup{\teeny \box "example 2"}
  \tempo "slow"
  e,8 e e f f f g g
  g a a a g g g c
  c c d d d e e e
  \bar "||"
  \break
  \tempo "slow"
  \mark \markup{\teeny \box "example 3"}
  \repeat volta 2 {
   \time 6/4
   a,8 ais b d dis e g gis a! c cis d!
  }
  \bar "||"

}

exampleTwo = \relative c, {
  \global
  % Music follows here.
  e f g r
}

exampleThree = \relative c, {
  \global
  % Music follows here.

}

exampleFour = \relative c, {
  \global
  % Music follows here.

}

exampleFive = \relative c, {
  \global
  % Music follows here.

}

exampleSix = \relative c, {
  \global
  % Music follows here.

}

exampleSeven = \relative c, {
  \global
  % Music follows here.

}

\score {
  \new StaffGroup \with {
    \consists "Instrument_name_engraver"
  } <<
    \new Staff { \clef "bass_8" \exampleOne }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \exampleOne
  >>
  \layout { }
}
