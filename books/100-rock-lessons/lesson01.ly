\version "2.23.80"

\header {
  title = "Lesson 1:"
  subtitle = "First-position notes"
  subsubtitle = "(100 rock lessons - bass)"
  composer = "Steve Gorenberg"
}

\paper { indent = 0.0\cm }

global = {
  \key c \major
  \time 4/4
}

exampleOne = \relative c, {
  \global
  % Music follows here.
  \mark \markup{\teeny \box "example 1"}
  e,4 a d g
  \bar "||"
  %\break
  s4 s s s
  \mark \markup{\teeny \box "example 2"}
  e,4 f g r
  \bar "||"
  s1
  \mark \markup{\teeny \box "example 3"}
  a4 b c r
  \bar "||"
  \break
  \mark \markup{\teeny \box "example 4"}
  e,4 f g2
  a4 b c2
  e,4 f e g
  c b a2
  \bar "||"
  \break
  \mark \markup{\teeny \box "example 5"}
  d4 e f r
  \bar "||"
  s1
  \mark \markup{\teeny \box "example 6"}
  d4 e f r
  \bar "||"
  \break
 \mark \markup{\teeny \box "example 7"}
    \tempo "slow" 4=80
    d4 f e f
    g a b2
    a4 b a g
    f e d2
    \bar "||"
}

exampleTwo = \relative c, {
  \global
  % Music follows here.
  e f g r
}

exampleThree = \relative c, {
  \global
  % Music follows here.

}

exampleFour = \relative c, {
  \global
  % Music follows here.

}

exampleFive = \relative c, {
  \global
  % Music follows here.

}

exampleSix = \relative c, {
  \global
  % Music follows here.

}

exampleSeven = \relative c, {
  \global
  % Music follows here.

}

\score {
  \new StaffGroup \with {
    \consists "Instrument_name_engraver"
  } <<
    \new Staff { \clef "bass_8" \exampleOne }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \exampleOne
  >>
  \layout { }
}
