\version "2.23.80"

\header {
  title = "Lesson 3:"
  subtitle = "Using A Pick"
  subsubtitle = "(100 rock lessons - bass)"
  composer = "Steve Gorenberg"
}

\paper { indent = 0.0\cm }

global = {
  \key c \major
  \time 4/4
}

exampleOne = \relative c, {
  \global
  % Music follows here.
  \tempo "slow"
  \mark \markup{\teeny \box "example 1" \eyeglasses}
  a8\downbow a\upbow a\downbow a\upbow a\downbow a\upbow a\downbow a\upbow
  e\downbow e\upbow e\downbow e\upbow e\downbow e\upbow e\downbow e\upbow
  a\downbow a\upbow a\downbow a\upbow d\downbow d\upbow d\downbow d\upbow
  g\downbow g\upbow g\downbow g\upbow d\downbow d\upbow d\downbow d\upbow
  \bar "||"
  \break
  \mark \markup{\teeny \box "example 2"}
  \tempo "slow"
  \key d \major
  \repeat volta 2 {
    \repeat unfold 8 {fis,8\downbow}
    fis\downbow e\downbow fis\downbow fis\downbow fis\downbow e\downbow fis\downbow e\downbow
  }
  \bar "||"
  \break
  \omit Voice.StringNumber
  \tempo "slow"
  \key c \major
  \mark \markup{\teeny \box "example 3"}
  a\downbow\4 b\upbow\4 c\downbow\4 (d\downbow\3) e\3\upbow f\downbow\3 (g\downbow\2) a\2\upbow
  a\downbow\2 g\upbow\2 (f\upbow\3) e\downbow\3 d\upbow\3 (c\upbow\4) b\downbow\4 a\upbow\4

  \bar "||"

}

exampleTwo = \relative c, {
  \global
  % Music follows here.
  e f g r
}

exampleThree = \relative c, {
  \global
  % Music follows here.

}

exampleFour = \relative c, {
  \global
  % Music follows here.

}

exampleFive = \relative c, {
  \global
  % Music follows here.

}

exampleSix = \relative c, {
  \global
  % Music follows here.

}

exampleSeven = \relative c, {
  \global
  % Music follows here.

}

\score {
  \new StaffGroup \with {
    \consists "Instrument_name_engraver"
  } <<
    \new Staff { \clef "bass_8" \exampleOne }
    \new TabStaff \with {
      stringTunings = #bass-tuning
    } \exampleOne
  >>
  \layout { }
}
