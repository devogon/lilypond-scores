\version "2.19.57"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "100 rock & roll workouts"
  composer = "chris matheos"
%  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}


%\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
%  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabas = \relative c {
   \global
   \repeat volta  2 {
     g^\markup{ex. 1} b d e
     g, b d e
   }
   \repeat volta 2 {
     g,^\markup{ex. 2} b d e
     f e d b
   }
   \break
   \repeat volta 2 {
     g^\markup{ex. 3} b d e
     g e d b
   }
   \repeat volta 2 {
     g'^\markup{ex. 4} e d e
     g e d b
   }
   \break
   \repeat volta 2 {
   g'8^\markup{ex. 9} g e e d d e d
   g g e e d d e e
 }
   \break
   \repeat volta 2 {
   g8^\markup{ex. 10} g e e d d \tuplet 3/2 {g e d}
   g g e e d d \tuplet 3/2 {d e g}
 }
   \break
   \repeat volta 2 {
    g,8^\markup{ex. 11} g b d f e d b
    g g b d f e d g
  }
   \break
      \repeat volta 2 {
    c,8^\markup{ex. 12} e bes' e,  a e \tuplet 3/2 {es e! g}
    c, e bes' e, a e \tuplet 3/2 {es e g}
   }
   \break
   \repeat volta 2 {
     a,8^\markup{ex. 13} a fis'  a, g' a, \tuplet 3/2 {c cis g'}
     a,8 a fis'  a, g' a, \tuplet 3/2 {c cis g'}
   }
   \break
      \repeat volta 2 {
	gis,8^\markup{ex. 14} a a a gis a a a
	gis a a a ~ a fis g! gis
   }
   \break
   \repeat volta 2 {
     a'8^\markup{ex. 15} cis, fis cis g' cis, \tuplet 3/2 {fis e cis}
     a'8 cis, fis cis g' cis, \tuplet 3/2 {fis e cis}
   }
   \break
   \repeat volta 2 {
     a'8^\markup{ex. 16} a cis, cis e e fis e
     a a cis, cis e e fis g
   }
   \break
   \repeat volta 2 {
     f,8^\markup{ex. 17} f a c es16 es es es d8 c
     f,8 f a c es16 es es es d8 f
   }
   \break
   \repeat volta 2 {
     f8^\markup{ex. 18} f a, c es es d c
     f f a, c es es d c
   }
   \break
   \repeat volta 2 {
     d8^\markup{ex. 19} b e b f' b, e d
     r4 r8 g8 ~ g4. b,8
   }
   \break
   \repeat volta 2 {
     c'8^\markup{ex. 20} c e, g bes16 bes bes bes a8 g
     c8 c e, g bes16 bes bes bes a8 g
   }
   \break
   \repeat volta 2 {
     f,4.^\markup{ex. 21} gis8 a c d c
     f,4. gis8 a c d f
   }
   \break
   \repeat volta 2 {
     c8^\markup{ex. 22} c dis e c c dis e
     c c dis e c c dis e
   }
   \break
   \time 12/8
   \repeat volta 2 {
     g,4.^\markup{ex. 23} b8 d e g4. b,8 d e
   }
   \break

   \repeat volta 2 {
     a,4^\markup{ex. 24} a8 cis4 cis8 e4 e8 f fis g
   }
   \break
   \time 6/8
   \repeat volta 2 {
     d4.^\markup{ex. 25} d4.
     b4. b4.
     g4. g4.
     a4. a4.
   }
   \break
   \time 12/8
   \repeat volta 2 {
     g4^\markup{ex. 26} ais8 b d e f4 f8 e d b
     d4 f8 fis a b c4 c8 b a fis
     d4 f8 fis a b c4 c8 b a fis
     g,4 ais8 b d e f4 f8 e d b
   }
   \break
   \time  6/8
   \repeat volta 2 {
     f8^\markup{ex. 27} gis8 a dis d! c
     f, gis8 a dis d! c
   }
   \break
   \repeat volta 2 {
     f4^\markup{ex. 28} gis,8 a c d
     f,4 gis8 a c d
   }
   \break
   \time 4/4
   \repeat volta 2 {
     g4^\markup{ex. 29} g8 g d4 \tuplet 3/2 {d8 e d}
     g4 g8 g d4 d16 e d d
   }
}


\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \contrabas }
  \layout { }
}
