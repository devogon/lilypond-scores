\version "2.19.36"
\language "nederlands"

\header {
  title = "Simandl New Method"
  subtitle = "bz 29"
  subsubtitle = "ex 7"
  instrument = "Contrabas"
  composer = "F. Simandl"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

global = {
  \key e \major
  \time 4/4
%  \tempo 4=100
}

contrabass = \relative c {
  \global
    \override HorizontalBracket.direction = #UP
  % Music follows here.
  e2-1\startGroup^\markup{\small "IV"} e'2-4
  dis2 b2
  e4-4\stopGroup gis,-1^\markup{\small "h.position"} a-1 b-4
  e,4 gis, a b
  cis2-1\startGroup cis'2-4
  bis2-2 gis-4
  cis4\stopGroup e,\startGroup fis gis
  cis,\stopGroup e, fis gis
  a b cis d
  %% \break
  e4 cis b a
  gis b dis-2\startGroup e-4
  gis-2\stopGroup b-4\startGroup dis-2 e-4\stopGroup
  fis,,4 gis a b
  cis a gis fis
  eis gis bis cis

  eis4 gis-4\startGroup bis-2 cis-4\stopGroup
  d!-1 e-4 d! cis
  b cis d!-1\startGroup b-4\stopGroup
  %% \break
  % third line
  cis4-2 d!-4 cis b-4
  a4-1 b-4 cis-2 a-4
  b-1 cis-4 b-1 a-2
  gis-1 a-1 b-4 gis-1
  a4-1 b-4 a-1 gis-1
  a-2 b-1 cis-4 e-4
  d! e d! cis-4\startGroup
  b-1 fis-1\stopGroup d'!-1^\markup{\small "IV"}\startGroup b-4\stopGroup
  cis-2 d! cis b
  %% \break
  % start the fourth line next
  a4 e cis'-2 a-4
  b-1 cis-4 b-1 a-2
  gis-1 e-2 b'-1 gis-4
  e-1 eis-2 fis-4 gis-2\startGroup
  a-4 e gis, r
  dis'2-2\stopGroup cis4-4 b-1
  a b cis dis
  e-1 fis-4 gis-2\startGroup a-4
  gis2\stopGroup e2-2^\markup{\small "h.p."}
  eis2-4 dis4-1 cis
  %% \break
  % start fifthe next
  bis4 cis dis-1 eis-4
  fis-1\startGroup^\markup{\small \column{ {II}{III}}} gis-4\stopGroup a-1\startGroup^\markup{\small "IV"} b-4
  a2-1 fis-4
  dis'2-2\stopGroup cis-4
  b4-1 bis-2 cis-4 dis-2
  e4-4 b-1\startGroup gis-4 b\stopGroup
  e2-4\startGroup^\markup{\small "IV"} b
  dis4 b fis b
  dis2 b
  e4\stopGroup b-1\startGroup gis-4 cis-4
  %% \break
  % start sixth next
  b-1 gis-4\stopGroup e-1 b'-4
  dis2-4 cis4-1 b-4
  a-1 b-1 cis-4 dis-2
  e-4 cis-4b-1 a-2
  gis-1 fis-4 e2-1
  cis4-4 fis-4 gis-2 a-4
  b,4 e fis gis-2
  a4-4 fis-4 b-4 b,-1
  e4 b e, r
  \bar "|."
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout {
    \context {
      \Voice
      \consists "Horizontal_bracket_engraver"
    }
  }
}
\markup {
  \teeny
  \date
}