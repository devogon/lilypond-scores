\version "2.19.82"

\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Simandl New Method"
  subtitle = "Octaves"
  composer = "Franz Simandl"
  poet = "poet"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \time 4/4
%  \tempo "Andante con Amore" 4=110
  \set Score.markFormatter = #format-mark-box-alphabet
}

octaveC = {
  \global
  \key c \major
   c2\upbow-1 c'2\downbow-4
   d2-1 d'2-4
   e2-1 e'2-4
   f2-1 f'2-4
   g2-1 g'2-3
   \bar "||"
   f'2-4 f2-1
   e'2-4 e2-1
   d'2-4 d2-1
   c'2-4 c2-1
   b2-4 b,2-1
   a2-1 a,2-0
   g2-0 g,2-2
   c2-2 r
   \bar "|."
 }

octaveF = {
  \global
  \key f \major
%  \autoLineBreaksOff
  f,4-1 f-4
  g,-4 g-2
  a,-4 a-0
  bes,-1 bes-4
  c-1 c'-4
  d-1 d'-4
  e-1 e'-4
  f-1 f'-4
  \bar "||"
%  \noBreak
  f' f
  e' e
  d' e
  c' c
  bes bes,
  a a,
  g g,
  f f,
  \bar "|."
  }

octaveBes = {
  \global
  \key bes \major
  bes,2 bes
  c c'
  d d'
  es es'
  f f'
  \bar "||"
  f' f
  es' es
  d' d
  c' c
  bes bes,
  a a,
  g g,
  f f,
  bes, r
  \bar "|."
}
octaveEs = {
  \global
  \key es \major
  es2
  f,4 f
  g, g
  as, as
  bes, bes
  c c'
  d d'
  es es'
  \bar "||"
  es'\downbow es\upbow
  d' d
  c' c
  bes bes,
  as as,
  g g,
  f f,
  es2
  \bar "|."
}
octaveAs = {
  \global
  \key as \major
  as,4\upbow-4 as\downbow-1
  bes,-1 bes-4
  c c'
  des des'
  es es'
  f-1 f'-4
  g-1 g'-3
  as-1 as'-3
  \bar "||"
  as'\downbow-3 as-1
  g'-3 g-1
  f'-4 f-1
  es' es
  des' des
  c' c
  bes bes,
  as as,
  \bar "|."
}

octaveDes = {
  \global
  \key des \major
  des2\upbow-4
  es\downbow-1
  f4\upbow f,\downbow
  ges, ges
  as, as
  bes, bes
  c c'
  des des'
  es es'
  f f'
  \bar "||"
  f' f
  es' es
  des' des
  c' c
  bes bes,
  as as,
  f f,
  es as,
  des2
  \bar "|."
}

octaveG = {
  \global
  \key g \major
  g,4 g
  a a
  b b
  c c
  d d
  e e
  fis fis
  g g
  \bar "||"
  g g
  fis fis
  e e
  d d
  c c
  b b
  a a
  g g
  \bar "|."
}

octaveD = {
  \global
  \key d \major
  d2\downbow
  e\upbow
  fis4\downbow fis,\upbow
  g g,
  a a,
  b b,
  cis' cis
  d' d
  e' e
  fis' fis
  \bar "||"
  fis fis'
  e e'
  d d'
  cis cis'
  b, b
  a, a
  g, g
  fis, fis
  e, e
  d2
    \bar "|."
}

octaveA = {
  \global
  \key a \major
  a,4 a
  b, b
  cis cis'
  d d'
  e e'
  fis fis'
  gis gis'
  a a'
  \bar "||"
  a' a
  gis' gis
  fis' fis
  e' e
  d' d
  cis' cis
  b b,
  a a,
  \bar "|."
}

octaveE = {
  \global
  \key e \major
  e,4^\markup{fix from here} e
  fis fis
  gis gis
  a a
  b b
  cis cis
  dis dis
  e e
  \bar "||"
  e e
  dis dis
  cis cis
  b b
  a a
  gis gis
  fis fis
  e e
  \bar "|."
}


octaveB = {
  \global
  \key b \major
  b,2 b
  cis cis
  dis dis
  e e
  fis fis
  \bar "||"
  fis fis
  e e
  dis dis
  cis cis
  b b
  a a
  gis gis
  fis fis
  b2 r
  \bar "|."
}

octaveFis = {
  \global
  \key fis \major
  fis,4 fis
  gis gis
  ais ais
  b b
  cis cis
  dis dis
  eis eis
  fis fis
  \bar "||"
  fis fis
  eis eis
  dis dis
  cis cis
  b b
  ais ais
  gis gis
  fis fis
  \bar "|."
}


\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveC }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveF }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveBes }
  \layout { }
}
\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveEs }
  \layout { }
}
\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveAs }
  \layout { }
}
\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveDes }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveG }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveD }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveA }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveE }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveB }
  \layout { }
}

\score {
  \new Staff \with {
%    instrumentName = "Contrabass"
%    shortInstrumentName = "Cb."
  } { \clef bass \octaveFis }
  \layout { }
}