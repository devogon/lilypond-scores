\version "2.19.54"
\language "nederlands"

\header {
  title = "30 Etudes"
 % subtitle = "1"
  instrument = "Contrabas"
  composer = "Franz Simandl"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \numericTimeSignature
  \time 4/4
  \tempo "Maestoso." 4=100
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

contrabass = \relative c {
  \global
  % Music follows here.
  c2\f g2 |
  b2 g2 |
  c2. d4 |
  e2 c2
  f2 d2
  g2 g,
  e'2. f4 |
  g2 e2
  \break
  a2 g
  f4. e8 d4. a8 |
  g'2 f
  e4. d8 c4. g8 |
  f2 fis
  g2 gis
  a4 b c cis |
  d2 g
  \break
  c,2 g
  b2 g
  c2. d4
  e2 c
  f2 d
  g2 g,
  e'2. f4
  g2 e
  a2 g
  \break
  f4. e8 d8.\tenuto (c16\tenuto) b8.\tenuto (a16\tenuto) |
  g'2 f
  e4. d8 c8.\tenuto (b16\tenuto) a8.\tenuto (g16\tenuto) |
  f4 fis g gis |
  a4 b c e
  f d g4. g,8 |
  \break
  c2 r
  g2 a
  b4 c d b
  c2 g
  e2 e'
  d2 b
  g2 f'
  e2 c
  g4 b c e |
  \break
  g2 a
  b4 c d b |
  c2 g
  e2 e'
  d2 b
  g2 f'
  e2 c
  g4 e d c |
  \break
  a2. f4 |
  b2 g
  c4. g8 fis4 g |
  e'2 c
  d4. g,8 fis4 g |
  f'2 d
  e4. c8 b4 c |
  g'2 e
  \break
  a4 g f e
  d c b4. a8
  g4 a b c
  d e f g
  f e d c
  b a g4. fis8
  g2 gis2
  \break
  a2 b
  c2 g
  b2 g
  c2. d4
  e2 c
  f2 d
  g2 g,
  e'2. f4
  g2 e
  \break
  a4. g8 f4 e
  d c b4. a8
  g4. f8 e4 f
  g a b4. c8
  d4.\tenuto (e8\tenuto) f4.\tenuto (fis8\tenuto) |
  g4.\tenuto (gis8\tenuto) a4.\tenuto (b8\tenuto) |
  c2 g
  c,2 r
  \bar "|."
}



\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
  \header { piece = \markup {\bold "1."} }
}

\markup { \override #'(line-width . 105)
  \justify-string #'"In the study of these Etudes strees is to be laid on the breadth of tone, precision of rhythm and correct intonation (particularly on the A and E strings). Preceding the study of each Etude it is suggested that the scale of the key in which it is written be played through with the various bowings. The Etudes are arranged in progressive order and should be practised slowly at first."
}

\markup { \vspace #2 }
\markup {
  \teeny
  \date
}