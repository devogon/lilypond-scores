\version "2.19.54"
\language "nederlands"

\header {
  title = "30 Etudes"
 % subtitle = "2"
  instrument = "Contrabas"
  composer = "Franz Simandl"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \numericTimeSignature
  \time 3/4
  \tempo "Andante con moto." 4=112
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

contrabass = \relative c {
  \global
  % Music follows here.
  f,2\mf g4 |
  a4. (bes8) c4 |
  d2 e4 |
  f2 f,4 |
  d'2 c4
  bes2 a4
  g8 f e4 f |
  g2 r4 |
  \break
  e2 f4 |
  a4. (bes8) c4 |
  c2 d4
  e2 e,4
  d'2 c4
  bes2 g4
  e8 f g4 c
  a2 r4
  \break
  f4 g a8 bes |
  c4 d8 e f4 |
  g4 f e8 d |
  c4 bes8 a g4 |
  a4 bes c8 d |
  e4 f8 g a4 |
  bes4 a g8 f |
  \break
  e4 d8 c bes4
  c4 a'8 g f4 |
  d8 c a4 f |
  bes8 bes' g f e d |
  c8 bes g4 e |
  a'8 g f e d c |
  \break
  bes8 a g4 f |
  g'8 f e d c bes |
  a8 g f4 e |
  f2.
  fis2.
  g2 f4
  e4 (c') e, |
  g2.
  gis2.
  \break
  a2 g4 |
  f4 (c') f, |
  a2.
  bes2.
  d2 c4
  bes4 (d) g, |
  bes2.
  b!2.
  c8 cis d4 e |
  \break
  f4 f, r |
  a'2.\downbow
  a,2\downbow g4 |
  fis2 es'4 |
  d2 c4
  b!2.
  g2\downbow f4
  e2 des'4 |
  c2 bes4
  \break
  e,4 c' c'
  f,4 a8 g f4 |
  fis4 d d' |
  g,4 bes8 a g4 |
  e8 d c bes a g |
  f8 g a bes c d |
  g,8 a bes c d e |
  \break
  a,8 bes c d e f |
  g8 a bes c d e |
  f e d c bes a |
  g f e d c e |
  f4 f, r |
  \bar "|."

}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
  \header { piece = \markup {\bold "2."} }
}

\markup {
  \teeny
  \date
}