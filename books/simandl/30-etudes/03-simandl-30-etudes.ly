\version "2.19.54"
\language "nederlands"

\header {
  title = "30 Etudes"
 % subtitle = "3"
  instrument = "Contrabas"
  composer = "Franz Simandl"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
%  system-system-spacing = #'((basic-distance . 0.1) (padding . 0))
  ragged-last-bottom = ##f
  ragged-bottom = ##f

}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \numericTimeSignature
  \time 4/4
  \tempo "Marciale." 4=108
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

contrabass = \relative c {
  \global
				% Music follows here.
  bes2 d |
  g4 f2\downbow es4\prall |
  d4 c\prall bes a\prall |
  g2. r4 |
  c2 es2 |
  a4 g2\downbow f4\prall
  % \break
  es4 d\prall c8. bes16 a8. c16 |
  es2 d4 r |
  bes2 bes'4. (bes8) |
  f8 d4\downbow c8 bes4 a |
  g2 g'4. (g8) |
  d8 c4\downbow bes8 a4 g |
  % \break
  es2 g4.\tenuto (g8\tenuto) |
  f8 g4\downbow g8 a4 bes |
  c4 d es8 f4\downbow g,8 |
  bes2. r4
  bes8 a bes c d c d es |
  f8 g a g f g f es |
  % \break  % begin second page from book
  d8 es d c bes c bes a |
  g8 a bes d g d bes g |
  c8 bes c d es d es f |
  g8 a bes a g a g f |
  % \break
  es8 f es d c bes a c |
  es f c es d f d c |
  bes8 d f a bes f d bes |
  f'8 es d c bes c a bes |
  % \break
  g8 bes d fis g d bes g |
  d'8 c bes a g a fis g |
  es'8 g bes g es c bes g |
  f8 e! f g a g a bes |
  % \break
  c8 bes c d es d es f |
  bes, bes' f d bes4 r |
  es2 g |
  c4 bes2\downbow as4\prall |
  g4 f\prall es d\prall |
  c2. r4
  % \break
  f2 as2-2 |
  d4 c2\downbow bes4\prall |
  as4 g\prall f8. e16 d8. f16 |
  as2 g4 r |
  es2-1 es'4. es8 |
  bes8 g4\downbow f8 es4 d |
  % \break
  c2 c'4. c8 |
  g8 es4\downbow d8 c4 bes |
  as2 c4. c8 |
  bes8 c4\downbow c8 d4 es |
  f4 g as8 bes4\downbow bes,8 |
  es2. r4 |
  es8 d es f g f g as |
  % \break
  bes8 c d c bes c bes as|
  g8 as g f es f es d |
  c8 d es g c g es c |
  f8 es f g as g as bes |
  % \break
  c8 d es d c d c bes |
  as8 bes as g f es d f |
  as8 bes f as g bes g f |
  es8 g bes d es bes g es |
  % \break
  bes'8 as g f es f d es |
  c8 es g b! c g es c |
  g'8 f es d c d b! c |
  as'8 c es c as f es c |
  % \break
  bes8 a bes c d c d es |
  f8 es f g as g as bes |
  es,8 es' bes g es4 r |
  f2 a-4 |
  d4 c2\downbow es,4\prall |
  % \break
  d4 c\prall bes c\prall |
  d2. r4 |
  fis2 a-2 |
  es'4 d2\downbow fis,4\prall-2 |
  g4 es\prall d c\prall |
  bes2. r4 |
  a2 g4. g8 |
  f8 g4 g8 a4 bes |
  % \break
  c8 d4 d8 es4 f |
  g4 a bes8 c4 d8 |
  es2 g,4. ges8 |
  f8 es4 d8 c4 bes |
  a4 g f8 f'4 f,8 |
  bes2. r4|
  \bar "|."
}



\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
  \header { piece = \markup {\bold "3."} }
}

\markup {
  \teeny
  \date
}