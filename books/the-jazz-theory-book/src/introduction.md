# Introduction

A great jazz solo consists of:


    1% magic
    99% stuff that is
         Explainable
         Analyzable
         Categorizeable
         Doable


This book is mostly about the 99% stuff.

There is no one single, all inclusive "jazz theory." In fact, that's
why the subject is called jass _theory_ rather than jazz _truth._ The
only truth is in the music itself. "Theory" is the littel intellectual
dance we do around teh music, attempting to come up with rules so we
can understand why Charlie Parker and John Coltrane sounded the way
they did. There are almost as many "jazz theories" as there are jazz
musicians.

Having said this, it's OK to come back to reality and state that there _is_ a common thread of development in jazz theory, a thread that has evolved logically from the earliest days of jazz through Louis Armstrong, James P. Johnson, Duke Ellington, Art Tatum, Lester Young, Charlie Parker, Thelonious Monk, John Coltrane, Bobby Hutcherson, Wayne Shorter, McCoy Tyner, Joe Henderson, to Mulgrew Miller and beyond. All these musicians could have played with each other and understood one another, even though their terminology may have differed. Louis Armstrong recorded with Duke Ellington,[^1] Duke Ellington recorded with John Coltrane,[^2] and all three souunded as though they enjoyed the ecounters.

[^1]: Louis Armstrong and Duke Ellington, The Great Reunion, Vogue, 1961

[^2]: Duke Ellington and John Coltrane, MCA/Impulse, 1962.

Charlie Parker once said "learn the changes and then forget them." As you study jazz theory, be aware of what your ultimate goal is in terms of what he said: _to get beyond theory._

When you're listening to a great solo, the player is _not_ thinking "II-V-I," "blues lick," "A A B A," "altered scale," and so forth. He or she ha done that already, many years ago. Experienced musicians have internalized this information to the point that they no longer have to think about it very much, if at all. The great players have also learned what the scales _look and feel like_ on their instrument. Be aware of what your eyes see nad what your hands feel when you play. Do this just as much as you focus your mind on the mental stuff, and you'll get beyond theory—where you just flow with the music. Aim for that state of grace, when you no longer have to think about theory, and you'll find it much easier to tap into the magical 1%.

In order to reach this point of mastery, you'll have to think about—and practice—theory a great deal. That's the 99% part.

## The Piano

Many of the examples in the book are written for piano. You don't need any "piano technique" to use this book. You just need to be able to read the notes. Because many people reading this book won't be pianists, many of the piano transcriptions have been simplified, and are marked as such. If a piano example looks too difficult for you to decipher, have your teacher or a piano-playing friend play it for you.

Unlike other instruments, the piano lets you "see" what you play, and that makes it easier to put all the pieces together. _Almost all the great jazz players, regardless of instrument, play some piano._ This includes Max Roach, Woody Shaw, Clifford Brown, Kenny Dorham, Joe Henderson, Art Blakey, Sonny Rollins, Hank Mobley, Benny Carter, Coleman Hawkins, Freddie Hubbard, Kenny Clarke, Dizzy Gillespie, Miles Davis, Philly Joe Jones, Carmen McRae, and Fats Navarro, just to name a few. Some of them played well enough to record on piano, including bassist Charles Mingus, [^3] and drummers Jack BeJohnette [^4] and Joe Chambers.[^5]

[^3]: Charles Mingus, _Mingus Plays Piano,_ Mobile Fidelity, 1964.

[^4]: Jack DeJohnette, _The Piano Album,_ Landramk, 1985.

[^5]: Joe Chambers and Larry Young, _Double Exposure,_ 1977.

## How Good Do You Want To Be?

There are certain prerequisites for becoming a good jazz musician. You must have:

    Talent (ears, time, a sense of form)
    Direction (exposure to the right music for you)
	Education (teachers, mentors)
	Ambition
	
Number 4—ambition—is perhaps the most important of all. I don't mean ambition in the sense of wanting to be a star, but in the sense of __having the will, desire, and stamina to practice._ If you don't have this quality, all the talent in the world means nothing.

As you go through this book, FIXME


## A Note on Terminology and Chord Symbols

## Terms, Lingo, Musicians' Nicknames

Glossary
