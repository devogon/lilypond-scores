\version "2.21.6"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "100 blues lessons"
  subtitle = " lesson 1 the blues scale"
%  instrument = "Bass"
  tagline = \date
}

\layout { ragged-last = ##f }
\paper {
  indent = 0.0\cm
  page-count = 1
}

global = {
 % \key c \major
  \time 4/4
 % \tempo "Moderately Slow" 4=80
  \omit Voice.StringNumber
}

scoreAElectricBassI = \relative c, {
  \global
  % Music follows here.
     \tempo "Moderately Slow" 4=80
    \key e \major
    e,4\4^\markup{1} g!^\markup{\teeny {\flat} \normalsize 3} a^\markup{4} bes^\markup{\teeny {\flat} \normalsize 5} |
    b^\markup{5} d^\markup{\teeny \flat \normalsize 7} e^\markup{8} d^\markup{\teeny \flat \normalsize 7} |
    b^\markup{5} bes^\markup{\teeny \flat \normalsize 5} a^\markup{4} g!^\markup{\teeny \flat \normalsize 3} |
    e1^\markup{1} |
    \bar "|."

}

scoreAElectricBassII = \relative c, {
  \global
  % Music follows here.
   \key c \major
  \tempo "Moderately slow" 4=90
  c4 es\3 f ges |
  g\2 bes c bes |
  g\2 ges f es\3
  c1
  \bar "|."
}

scoreAElectricBassIII = \relative c, {
  \global
    \tempo "Moderately slow" 4=85
  % Music follows here.
  \key d \major
  e,8 g! a\4 bes\4 b!\4 d!\3 e4\3 |
  e8\3 g!\3 a\2 bes\2 b!\2 d! e4 |
  \ottava #1
  e8 g a bes b! d! e4 |
  e8 d! b bes a g e4 |
  \ottava #0
  e8 d b\2 bes\2 a\2 g\3 e4\3 |
  e8\3 d\3 b\4 bes\4 a\4 g\4 e4 |
  \bar "|."
}

scoreAElectricBassIV = \relative c, {
  \global
  % Music follows here.
    \tempo "Moderately slow" 4=84
  c4\4^\markup{C7} c'16\2 bes\2 c8\2 r16 f,	\3 ges\3 g!\3 bes8\2 g\3 |
  c,4\4 c'16\2 bes\2 c8\2 r16 f,	\3 ges\3 g!\3 es4\4 |
  \bar "|."
}

scoreAElectricBassV = \relative c, {
  \global
    \tempo "Moderately slow" 4=80
  % Music follows here.
    bes'16^\markup{C7} fis g\2 bes fis g\2 bes g\2 c8 bes g16\2 f! es8\3 |
    bes'16^\markup{C7} fis g\2 bes fis g\2 bes g\2 c8 bes es,4\3 |
    \bar "|."
}

scoreAElectricBassVI = \relative c, {
  \global
      \tempo "Moderately slow" 4=92
  % Music follows here.
  \repeat volta 2 {
    c4.^\markup{C7} g8 bes g bes g
  }
  \alternative {
    {c4. g8 bes g f'16\2 fis\2 g8\2}
    {c,4 es c bes4}
  }
  \bar "|."
}

% scoreAElectricBassVII = \relative c, {
%   \global
%       \tempo "Moderately slow" 4=94
%   % Music follows here.
%   \repeat volta 2 {
%     d4 d d d
%     d d d d
%     d d d d
%     d d d d
%     \bar ":|"
%   }

%}

rs = {
  \once \override Rest.stencil = #ly:percent-repeat-item-interface::beat-slash
  \once \override Rest.thickness = #0.48
  \once \override Rest.slope = #1.7
  r4
}
scoreAElectricBassVII = \relative c, {
  \global
  % Music follows here.
% Macro to print single slash
 \tempo "Moderately slow" 4=94
 \repeat volta 2 {
  \repeat unfold 16 {\rs}
  }
}

scoreAElectricBassIPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassI }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \scoreAElectricBassI
>>

scoreAElectricBassIIPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassII }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \scoreAElectricBassII
>>

scoreAElectricBassIIIPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassIII }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \scoreAElectricBassIII
>>

scoreAElectricBassIVPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassIV }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \scoreAElectricBassIV
>>

scoreAElectricBassVPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassV }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \scoreAElectricBassV
>>

scoreAElectricBassVIPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassVI }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \scoreAElectricBassVI
>>

scoreAElectricBassVIIPart = % \new StaffGroup <<
  \new Staff { \clef "bass_8" \scoreAElectricBassVII }
 %  \new TabStaff \with {
%     stringTunings = #bass-tuning
%   } \scoreAElectricBassVII
% >>

\score {
  <<
    \scoreAElectricBassIPart
 %    \scoreAElectricBassIIPart
%     \scoreAElectricBassIIIPart
%     \scoreAElectricBassIVPart
%     \scoreAElectricBassVPart
%     \scoreAElectricBassVIPart
%     \scoreAElectricBassVIIPart
  >>
  \layout { }
  \header {
    piece = \markup{\bold "Example 1"} }
}

\score {
  <<
    \scoreAElectricBassIIPart
 %    \scoreAElectricBassIIPart
%     \scoreAElectricBassIIIPart
%     \scoreAElectricBassIVPart
%     \scoreAElectricBassVPart
%     \scoreAElectricBassVIPart
%     \scoreAElectricBassVIIPart
  >>
  \layout { }
  \header { piece  = \markup{\bold "Example 2"} }
}

\score {
  <<
    \scoreAElectricBassIIIPart
 %    \scoreAElectricBassIIPart
%     \scoreAElectricBassIIIPart
%     \scoreAElectricBassIVPart
%     \scoreAElectricBassVPart
%     \scoreAElectricBassVIPart
%     \scoreAElectricBassVIIPart
  >>
  \layout { }
  \header {
    piece = \markup{\bold "Example 3"} }
}

\score {
  <<
    \scoreAElectricBassIVPart
 %    \scoreAElectricBassIIPart
%     \scoreAElectricBassIIIPart
%     \scoreAElectricBassIVPart
%     \scoreAElectricBassVPart
%     \scoreAElectricBassVIPart
%     \scoreAElectricBassVIIPart
  >>
  \layout { }
  \header {
    piece = \markup{\bold "Example 4"} }
}

\score {
  <<
    \scoreAElectricBassVPart
 %    \scoreAElectricBassIIPart
%     \scoreAElectricBassIIIPart
%     \scoreAElectricBassIVPart
%     \scoreAElectricBassVPart
%     \scoreAElectricBassVIPart
%     \scoreAElectricBassVIIPart
  >>
  \layout { }
  \header {
    piece = \markup{\bold "Example 5"} }
}

\score {
  <<
    \scoreAElectricBassVIPart
 %    \scoreAElectricBassIIPart
%     \scoreAElectricBassIIIPart
%     \scoreAElectricBassIVPart
%     \scoreAElectricBassVPart
%     \scoreAElectricBassVIPart
%     \scoreAElectricBassVIIPart
  >>
  \layout { }
  \header {
    piece = \markup{\bold "Example 6"}
  }
}

\score {
  <<
    \scoreAElectricBassVIIPart
    >>
  \layout { }
  \header {
    piece = \markup{\bold{C7 Groove } \tiny{It's your turn to create some bass lines. This track is a groove over a C7 chord.}}
  }
}