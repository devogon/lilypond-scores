\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Suite"
  composer = "James Hook (1740 - 1827)"
  tagline = \date
  meter = "Allegro non troppo"
}

global = {
  \key g \major
  \numericTimeSignature
  \time 4/4
  \partial 4
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
  d4\mf |
  b4. c8 d4 b |
  a4 d d e8 fis |
  a8 (g) fis e d4 c |
  c2 (b4)\breathe d |
  e4 g g fis8 (e) |
  \break
  d4 g b, d |
  c4 c b8 (a) b c |
  b2 (a4)
  }
  \repeat volta 2 {
    a4
    d4. e8 d4 c |
    c2 (b4) d |
    g4. a8 g4 f! |
    \break
    f!2 (g4)\breathe d |
    c4 a' a c, |
    b g' g b, |
    d8 (c) b a b4 cis |
    d2.\breathe d8 (c) |
    b4. c8 d4 b |
    \break
    a4 d d e8 fis |
    a8 (g) fis e d4 c |
    c2 (b4)\breathe d4 |
    e g g fis8 (e) |
    d4 g b d |
    d4 c b a |
    \break
    g2-> fis2-> |
    f!2.-> d4 |
    e4 e fis fis |
    g4 fis8 e d4 c |
    b4 g' a, fis' |
    g4 b,-> c-> d-> |
    g,2.->
  }
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    d4
    g4. a8 b4 g |
    fis4 a8 (g) fis4 d |
    e4 a8 (g) fis4 d |
    e8 (d e fis g4)\breathe b4 |
    c4 e e d8 (c) |
    b2 g4 b |
    a4 a g8 (fis) g a |
    g4 d8 (e) fis4
  }
  \repeat volta 2 {
      r4
      r2 r4 d|
      g4. a8 g4 f! |
      e2. g4 |
      c4. d8 c4 b |
      a4 fis fis a |
      g4 b b g |
      a4 fis g e |
      fis4 a d8 (c) b (a) |
      g4. a8 b4 g |
      fis4 a8 (g) fis4 d |
      e4 a8 (g) fis4 d |
      e8 (d e fis g4)\breathe b4 |
      c4 e e d8 (c) |
      b2 g4 b |
      b4 a g d |
      g2-> a2-> |
      b2.-> b4 |
      c4 c a a |
      b4 d8 c b4 a |
      g4 e c d |
      g4 b,-> c-> d-> |
      g,2.->
  }
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
  %  \clarinetPart
  >>
  \layout { }
}
