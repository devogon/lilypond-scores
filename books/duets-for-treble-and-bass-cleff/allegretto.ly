\version "2.23.10"

\header {
  title = "Allegretto"
  composer = "Willem de Fesch (1687 - 1761)"
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  g4\mp c2 |
  e8 (d) c (b) c4 |
  g4 e'2 |
  g8 (f) e (d) e4 |
  g4 g,2 |
  b8 a g4 c |
  f4 e8 d e f |
  d2 r4 |
  \break
  g,4 c2 |
  e8 (d) c (b) c4 |
  g4 e'2 |
  g8 (f) e (d) e4 |
  g4 g,2 |
  a4 g8 (g) c (g) |
  f'8 d d4. (c8) |
  c2. |
  \break
  e16 ( f g8) g2 |
  b,16 ( c d8) d2 |
  a16 (b c8) b4 a |
  c b c |
  d e e, |
  g fis g |
  a8 b c4 b |
  b4 a2 |
  \break
  c8\f (a) fis' (c) a' (c,) |
  b8 (d) c (b) a (g) |
  c8\f (a) fis' (c) a' (c,) |
  b8 (d) c (b) a (g) |
  d'2.\mf |
  e4 fis8 (d) g (d) |
  e8 c a4. (g8) |
  g2. |
  \break
  g4\mp c2 |
  e8 (d) c (b) c4 |
  g4 e'2 |
  g8 (f) e (d) e4 |
  g4 g,2 |
  b8 a g4 c |
  f4 e8 d e f |
  d2. |
  \break
  g,4 c2 |
  e8 (d) c (b) c4 |
  g4 e'2 |
  g8 (f) e (d) e4 |
  g4 g,2 |
  a4 b8 (g) c (g) |
  f'8 d d4. (c8)
  c2. |
  \break
  e4 d8 c b a |
  f'4 e a |
  gis8 a b4 d, |
  d16 (c b8) a4 r |
  f'4 f (e) |
  e4 d2 |
  c8 (b) d (c) b (a) |
  gis8 (a) b4 r |
  \break
  c4_\markup{\italic cresc.} c (b) |
  c4 (cis2) |
  d4 (dis2) |
  e2. |
  f2\mf a,4 |
  gis4 d'2 |
  c16\> (b a8) b4. (a8) |
  a2.\mp\!
  \break
  g4 c2 |
  e8 (d) c (b) c4 |
  g4 e'2 |
  g8 (f) e (d) e4 |
  g4 g,2 |
  b8 a g4 c |
  f e8 d e f |
  d2 r4 |
  \break
  g,4 c2 |
  e8 (d) c (b) c4 |
  g4 e'2 |
  g8 (f) e (d) e4 |
  g4 g,2 |
  a4 b8 (g) c (g) |
  f' d_\markup{\italic rit.} d4. (c8) |
  c2.\fermata |
  \bar "|."
}

cello = \relative c {
  \global
  % Music follows here.
  c4 e2 |
  g8 (f) e (d) e4 |
  c4 c'2 |
  e8 (d) c (b) c4 |
  e4 e,2 |
  g8 f e4 e'4 |
  d c c, |
  g'8 a g f e d |
  \break
  c4 e2 |
  g8 (f) e (d) e4 |
  c4 c'2 |
  e8 (d) c (b) c4 |
  e4 e,2 |
  f2 e4 |
  f4 g g, |
  c2. |
  \break
  c'4 e c |
  g b g |
  fis g d |
  r g a |
  b c2 |
  d4 c b |
  fis2 g4 |
  b,4 b b |
  \break
  d4 d d |
  g2. |
  d4 d d |
  g2. |
  b4 a g |
  c2 b4 |
  c d d, |
  g8\> a g f e d |
  \break
  c4\! e2 |
  g8 (f) e (d) e4 |
  c4 c'2 |
  e8 (d) c (b) c4 |
  e4 e,2 |
  g8 f e4 e' |
  d c c, |
  g'8 a g d e d |
  \break
  c4 e2 |
  g8 (f) e (d) e4 |
  c4 c'2 |
  e8 (d) c (b) c4 |
  e4 e,2 |
  f2 e4 |
  f g g, |
  c2. |
  \break
  g'2.  |
  d'4 c a |
  b gis e |
  a c a |
  d gis, a |
  b gis e |
  a f d |
  e gis e |
  \break
  a4 e2 |
  a2 g4 |
  fis4 b a |
  gis e a |
  d, e f |
  e2 gis4 |
  a e e, |
  a4. e8 f g |
  \break
  c4 e2 |
  g8 (f) e (d) e4 |
  c4 c'2 |
  e8 (d) c (b) c4 |
  e4 e,2 |
  g8 f e4 e' |
  d c c, |
  g'8 a g f e d |
  \break
  c4 e2 |
  g8 (f) e (d) e4 |
  c4 c'2 |
  e8 (d) c (b) c4 |
  e4 e,2 |
  f2 e4 |
  f g g, |
  c2.\fermata
  \bar "|."
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  \new GrandStaff <<
    \violinPart
    \celloPart
 %   \clarinetPart
  >>
  \layout { }
}
