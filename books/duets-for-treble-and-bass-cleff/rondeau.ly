\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Rondeau"
  composer = "J. B. de Boiismortier (1682 - 1765)"
  meter = "Grazioso"
  tagline = \date
}

\paper {
  page-count = 1 }

global = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
  \partial 4 d4\mf
  g8 a bes a g fis |
  g2 d4 |
  es d c |
  bes g d' |
  es d c |
  bes g a |
  \break
  bes8 c c4.\trill (bes16 c) |
  d2 a4 |
  bes a g |
  fis'4\grace e!8 d4 g |
  c,8 es a,4. g8 |
  g2
  }
  \mark \default
  \partial 4 d'4
  d4 (es f) |
  f (c) d |
  d (es) f |
  f (c) d |
  d (es) f |
  f (es) d |
  c2 bes4 |
  c es, f' |
  \break
  g4 a bes |
  a g8 (f) es (d) |
  g4 c,4. bes8 |
  bes2\breathe \mark \default f'4 |
  g8 a bes a g fis |
  g2 d4 |
  \break
  es4 d c |
  bes g d' |
  es d c |
  bes g a |
  bes8 c c4.\trill (bes16 c) |
  d2 a'4 |
  bes a g |
  \break
  fis4\grace e8 (d4) g |
  c,8 es a,4. g8 |
  g2\breathe \mark \default d'4 |
  g a e! |
  f g a |
  d, e!4. f8 |
  cis4 a a' |
  \break
  e!4 f g |
  f e! a |
  e!8 f e f g a |
  f4 e! a |
  bes8 a g f e! d |
  a'4. cis,8 d4 ( |
  \break
  d8) e!8 e4. d8 |
  d2\breathe \mark \default d4 |
  g8 a bes a g fis |
  g2 d4 |
  es4 d c |
  bes4 g d' |
  es4 d c |
  \break
  bes4 g a |
  bes8c c4.\trill ( bes16 c) |
  d2 a'4 |
  bes4 a g |
  fis4\grace e!8 (d4) g |
  c,8_\markup{\italic rit.} es8 a,4. g8 |
  g2\fermata
  \bar "|."
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
     \partial 4 g'4
     bes8 c d c bes a |
     bes2 g4 |
     c bes a |
     g8 a bes4 g |
     c4 bes a |
     g2 fis4 |
     \break
     g4 es2 |
     d2 d4 |
     g c, es |
     d2 bes'4 |
     a8 g fis4. g8 |
     g2
  }
  \partial 4 bes4
  bes (c) d |
  \break
  d (a) bes |
  bes (c) d |
  d (a) bes |
  bes (c) d |
  g, (a) bes |
  a2 g4 |
  f2 d4 |
  \break
  es8 d c4 bes |
  f' a bes |
  es, f2 |
  bes2\breathe g4 |
  bes8 c d c bes a |
  bes2 g4 |
  \break
  c4 bes a |
  g8 a bes4 f |
  c' bes a |
  g2 fis4 |
  g4 es2 |
  d2 d4 |
  g4 c, es |
  \break
  d2 bes'4 |
  a8 g fis4. g8 |
  g2\breathe g4 |
  bes a g |
  d'2 c4 |
  bes8 a g f e! d |
  a'2 cis4 |
  \break
  cis4 b! a |
  d a cis |
  cis b! a |
  d a f |
  g4. a8 bes4 |
  a4 g f |
  \break
  g4 a2 |
  d,2\breathe g4 |
  bes8 c d c bes a |
  bes2 g4 |
  c4 bes a |
  g8 a bes4 g |
  c4 bes a |
  \break
  g2 fis4 |
  g4 es2 |
  d2 d4 |
  g4 c, es |
  d2 bes'4 |
  a8 g fis4. g8 |
  g2\fermata
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  \new StaffGroup
  <<
    \violinPart
    \celloPart
 %   \clarinetPart
  >>
  \layout { }
}
