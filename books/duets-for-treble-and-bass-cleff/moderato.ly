\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Moderato"
  composer = "J. B. de Boiismortier (1682 - 1765)"
  tagline = \date
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    a4\p e'2 |
    c2 b4 |
    c8 (d) d4. (c16 d) |
    e2 a,4 |
    c (b) a |
    d (c) b |
    e c2 |
    b2. |
  }
  \break
  \repeat volta 2 {
    d4. e8 c4 |
    b2 a4 |
    b gis a |
    d4. e8 c4 |
    b2 a4 |
    b gis a |
    b4 b2 |
    a2.
  }
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    a'4 gis2 |
    a2 e4 |
    a8 (b) b4. (a16 b) |
    c2. |
    a4 b c |
    b a b |
    gis a2 |
    e2. |
  }
  \break
  \repeat volta 2 {
    b'4. c8 a4 |
    gis2 a4 |
    d4. e8 c4 |
    b2 a4 |
    gis2 a4 |
    e2 f4 |
    d4 e2 |
    a2.
  }

}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  \new StaffGroup <<
    \violinPart
    \celloPart
     %\clarinetPart
  >>
  \layout { }
}
