\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Andantino"
  tagline = \date
}

global = {
  \key c \major
  \numericTimeSignature
  \time 2/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  e4\p\grace e8 (d16) (c d e) |
  c4 b8 c |
  d e\grace g8 f e |
  e8 d c16 (b) a (g) |
  e'4\grace e8(d16) (c d e) |
  c4 b8 c |
  \break
  d16 (a' f d) c8 b |
  d4 (c)\breathe
  d4 e |
  g16 (f) e f d8 d |
  e8 c (b c) |
  e4 (d) |
  \break
  d4 e |
  g16 (f) e f d8 d |
  e a g fis |
  g16 (fis) g^\markup{\italic rit.} a g (e) f! d |
  e4_\markup{a tempo}\grace e8 (d16 c d e) |
  c4 b8 c |
  \break
  d8 e\grace g8 (f8) e |
  e d c16 (b) a (g) |
  e'4\grace e8 (d16 c d e) |
  c4 b8 c |
  d16 (a' f d) c8 b |
  d4 (c)
  \bar "|."
}

cello = \relative c {
  \global
  % Music follows here.
  c'4 g |
  e4 d8 c |
  b c d c |
  g g' r4 |
  c8 g f16 (e f g) |
  e4 d8 c |
  \break
  f4 r8 g |
  f g e c\breathe |
  b' (g) c (g) |
  a (g) b (g) |
  c e, (d e) |
  c' (g) b (g)\breathe |
  \break
  b (g) c (g) |
  a (g) b (g) |
  c a d d, |
  g4^\markup{\italic{rit.}} r |
  c4 g |
  e4 d8 c |
  \break
  b8 c d c |
  g g' r4 |
  c8 g f16 {e f g} |
  e4 d8 c |
  f4 r8 g |
  f8 g c,4 |
  \bar "|."
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
 %   \clarinetPart
  >>
  \layout { }
}
