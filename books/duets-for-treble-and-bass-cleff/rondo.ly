\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Rondo"
  meter = "Allegretto"
  tagline = \date
}

global = {
  \key c \major
  \numericTimeSignature
  \time 2/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  e8\staccato\mp e\staccato c\staccato c\staccato |
  d\staccato d\staccato a r |
  c16  (b) a g c (b) c d |
  e16 (d) e f e8 (d) |
  e8\staccato\f e\staccato c\staccato c\staccato |
  d8\staccato d\staccato a r |
  \break
  c16 (b) a g c (d) e f |
  e8\staccato d\staccato c r |
  g'8\staccato\mf g\staccato e4 |
  f8\staccato f\staccato d4 |
  f16 (e) d c a' (g) f e |
  d16 (c) b a g8 r |
  \break
  g'8\staccato g\staccato e4 |
  f8\staccato f\staccato d4 |
  f16 (e) d c c'16\< (b) a g |
  fis (g) a fis g8\! r |
  e8\staccato\f e\staccato c\staccato c\staccato |
  d\staccato d\staccato a r |
  \break
  c16 (b) a g c (b) c d |
  e (d) e f e8 (d) |
  e8\staccato e\staccato c\staccato c\staccato |
  d\staccato d\staccato a r |
  c16 (b) a g c (d) e f |
  e8\staccato d\staccato c r |
  \bar "||"
  \key es \major
  \break
  es8\mf es c c |
  d d b! r |
  c8 d es16 (g) f es |
  d16 (c) b! a! g8 r |
  es'8 es c c |
  d d b! r |
  \break
  c8^\markup{\italic cresc.} d es g |
  f^\markup{\italic rit.} fis g4\f\fermata |
  \key c \major
  \bar "||"
  e8\p\staccato^\markup{\italic"a tempo"} e\staccato c\staccato c\staccato |
  d\staccato d\staccato a r |
  c16 (b) a g c (b) c d |
  \break
  e16 (d) e f e8 (d) |
  e8\staccato\f e\staccato c\staccato c\staccato |
  d\staccato d\staccato a r |
  c16 (b) a g c (d) e f |
  e8 d c4 |
  \bar "|."
}

cello = \relative c {
  \global
  % Music follows here.
  c8\staccato c\staccato e\staccato e\staccato |
  f8\staccato f\staccato fis r |
  g8 f! e d |
  c c' g4 |
  c,8\staccato c\staccato e\staccato e\staccato |
  f\staccato f\staccato fis r |
  \break
  g8 f e c |
  g'8\staccato b\staccato c r |
  r e\staccato cis\staccato a\staccato |
  r d\staccato b\staccato g\staccato |
  c4 c,8 e |
  f8 fis g r |
  \break
  r8 e'\staccato cis\staccato a\staccato |
  r d\staccato b\staccato g\staccato |
  c4 a4 |
  d8 d, g r |
  c,8\staccato c\staccato e\staccato e\staccato |
  f\staccato f\staccato fis r |
  \break
  g8 f e d |
  c c' g4 |
  c,8\staccato c\staccato e\staccato e\staccato |
  f8\staccato f\staccato fis r |
  g8 f e c |
  g'8\staccato b\staccato c r |
  \bar "||"
  \key es \major
  c,8  c es es |
  f f g r |
  es b'! c es, |
  f fis g r |
  c, c es es |
  f f g r |
  \break
  es8 b! c d |
  f16 (es) d c b!8 g\fermata |
  \key c \major
  c8\staccato c\staccato e\staccato e\staccato |
  f\staccato f\staccato fis r |
  g f e d |
  \break
  c8 c' g4  |
  c,8\staccato c\staccato  e\staccato  e\staccato  f\staccato  f\staccato fis r |
  g8 f e c |
  g' b c4 |
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
   % \clarinetPart
  >>
  \layout { }
}
