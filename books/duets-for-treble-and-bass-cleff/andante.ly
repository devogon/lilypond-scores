\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Andante"
  composer = "Carlo Tessarini (1690 - ??"
  tagline = \date
}

global = {
  \key f \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    a4\mf bes c ~ |
    c bes a |
    d c r |
    bes e g ~ |
    g8 f e d c bes |
    bes2  a4 |
    a'8 c, ( bes c) a'4 |
    g8 c, (b! c) a'4 |
    f8 (e) d c b! a |
    g4 f2 |
    e8 (g) a b! c d |
    e4. d8 c4 |
    d4 c b!
    c2.
  }
  \repeat volta 2 {
    a4 bes c |
    c8 d es c a c |
    f,4 es'2 |
    d8 (c) bes a bes4 |
    b!4 c d |
    d8 (e) f d b! d |
    g,4 f'2 |
    e8 (d) c b! c4 |
    f8 c (b! c) f4 |
    g8 bes, (a bes) g'4 |
    a8 c, (b! c) a'4 |
    bes8 (a) g f e d |
    c4 bes2 |
    a8 (c) d e f g |
    a4. g8 f4 |
    g4 f e |
    f2.
  }
  \bar "|."
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
  f4\mf g a ~ |
  a4 g f |
  bes a r |
  c,4 c c |
  e2 g4 |
  f2. |
  f4 f f |
  \break
  e4 e e |
  d'8 (c) b! a g f |
  e4 b!2  c4 r e |
  c4. d8 e4 |
  f4 e d |
  e2.
  }
  \repeat volta 2 {
    f4 g a |
    a c2 |
    a8 (bes) c a f a |
    bes (c) d c d4 |
    g,4 a b! |
    \break
    b!4 d2 |
    b!8 (c) d b! g b! |
    c8 (d) e d e4 |
    a,4 a a |
    e4 e e |
    f4 f f |
    \break
    g8 (f) e d c bes |
    a4 g2 |
    f4 r e' |
    f4. g8 a4 |
    bes4 a g |
    a2.
  }
  \bar "|."
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
    \clarinetPart
  >>
  \layout { }
}
