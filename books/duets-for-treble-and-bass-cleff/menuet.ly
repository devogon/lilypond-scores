\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}


\header {
  title = "Menuet"
  meter = "Allegretto Grazioso"
  tagline = \date
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    e4\p d e |
    f2 e4 |
    d2 c4 |
    b8 (c) d e d4\breathe ||
    e4\mf d e |
    f2 e4 |
    d c b |
    c2.
  }
  \repeat volta 2 {
    e4\p d c |
    d2. |
    e4 d c |
    d2. |
    e4\mf d e |
    f2 e4 |
    \tuplet 3/2 {d8\> (e f)} c4 b |
    c2.\p\!
  }
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    c'4 b c |
    a b c |
    f, g a |
    g2.\breathe |
    c4 b c |
    a b c |
    f, g2 |
    c,2.
  }
  \repeat volta 2 {
    c'4 b a |
    b4 g8^\markup{\italic cresc.} (a) b g |
    c4 b a |
    b4 g8 (a) b g |
    c4 b c |
    a4 b c |
    f,4 g2 |
    c,2.
  }
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
   % \clarinetPart
  >>
  \layout { }
}
