\version "2.23.10"
date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Menuet"
  composer = "Willem de Fesch (1687 - 1761)"
  meter = "Grazioso"
  tagline = \date
}

global = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    f4\mf bes, (a) |
    bes es d |
    d8 (c) bes (a) bes4 |
    \grace f'8 (es2) d4 |
    g4 a, (bes) |
    es4\grace es8 (d2) |
    c4 es8 (d c bes ) |
    \break
    a4. (g8) f4 |
    f' bes, (a) |
    bes es d |
    d8 (c) bes (a) bes4 |
    \grace f'8 (es2) d4 |
    g4 a, bes |
    \grace f'8 (es2) d4 |
    \break
    es8 (c) \grace d8 (c2) |
    bes2. |
  }
  \repeat volta 2 {
    d4 ( e! f ) |
    e!8 (f) g2 |
    f4 (g a) |
    g8 (a) bes2|
    a4 bes8 (a) bes (g) |
    \break
    a4 bes8 (a) bes (g) |
    a8 (g bes) a (g f) |
    e!8 (d) c2 |
    \grace c8 (bes) a\staccato bes4 g' |
    a,8\staccato g\staccato a4 f' |
    g,4 f' e! |
    \break
    f2 r4 |
    d8 (es) f4 f |
    f g as ~ |
    as g8 (f) es (d) |
    es8 (d) c4 r |
    c8 (d) es4 es |
    \break
    es4 f g ~ |
    g4 f es |
    \grace f8 (es4) d8 (c) d (es) |
    f4 bes, (a) |
    bes es d |
    d8 (c) bes (a) bes4 |
    \break
    \grace f'8 (es2) d4 |
    g4 a, bes |
    es4 \grace es8 (d2) |
    c4 es8 (d) c (bes) |
    a4. (g8) f4 |
    f'4 bes, (a) |
    \break
    bes4 es d |
    d8 (c) bes (a) bes4 |
    \grace f'8 (es2) d4 |
    g2. |
    \grace bes,8 (a2) bes4 |
    es8 (c) \grace d8 (c2) |
    bes2. |
  }
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    bes2 c4 |
    d c bes |
    es2 d4 |
    c f bes, |
    es2 d4 |
    c2 bes4 |
    es f g |
    \break
    f8 (g f) es d c |
    bes2 c4 |
    d c bes |
    es2 d4 |
    c f bes, |
    es2 d4 |
    c f bes, |
    \break
    es4 f f, |
    bes2.
  }
  \repeat volta 2 {
    bes'8 ( a) g4 f |
    c'4 c c |
    c c c |
    c c c |
    c c c |
    \break
    c c c |
    f, d bes |
    c c' c, |
    d e! c |
    f2 a4 |
    bes g c |
    \break
    f,4 f8 es d c |
    b!2 r4 |
    b'!2 r4 |
    b!2 g4 |
    c4 es,8 d c bes |
    a2 r4 |
    \break
    a'2 r4 |
    a2 r4 |
    bes2 r4 |
    bes,2 c4 |
    d c bes |
    es2 d4 |
    \break
    \grace s8 c4 f bes, |
    es2 d4 |
    c2 bes4 |
    es f g |
    f8 (g f) es d c |
    bes2 c4 |
    d c bes |
    es2 d4 |
    c f bes, |
    es2. |
    es2 d4 |
    es f f, |
    bes2.
  }
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
    \new GrandStaff
  <<
    \violinPart
    \celloPart
    %    \clarinetPart
  >>
  \layout { }
}
