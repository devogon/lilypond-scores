\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Gavotte"
  meter = "Allegretto"
  tagline = \date
}

global = {
  \key c \major
  \numericTimeSignature
  \time 2/2
}

violin = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    e4\mp f g f |
    e2 d |
    c4 d8 e f4 e |
    d8 c d e d2\breathe |
    e4 f g f |
    e2 d |
    c4 d8 e f4 e |
    d8 c d e c2 |
  }
  \repeat volta 2 {
    g'4 f e d |
    c d8 e d4 e8 f |
    g4 f e d |
    c4d8 e d2\breathe |
    e4 f g f |
    e2 d |
    c4 d8 e f4 e |
    d8 c d e c2 |
  }
}

cello = \relative c {
  \global
  % Music follows here.
  \repeat volta 2 {
    c4 d e f |
    g2 f |
    e d4 c |
    g'1\breathe |
    c,4 d e f |
    g2 f |
    e2 d4 c |
    g'2 c, |
  }

  \repeat volta 2 {
    e4 d c g' |
    e c g' f |
    e d c g' |
    e c g'2\breathe |
    c,4 d e f |
    g2 f |
    e2 d4 c |
    g'2 c, |
  }

}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
 %   \clarinetPart
  >>
  \layout { }
}
