\version "2.23.10"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%y at %X" (localtime (current-time)))}}

\header {
  title = "Sarabande"
  meter = "Adagio"
  tagline = \date
}

global = {
  \key c \major
  \numericTimeSignature
  \time 3/4
}

violin = \relative c'' {
  \global
  % Music follows here.
  e4\mp c a |
  f'2 e4 |
  d c4. d8 |
  b2 a4\breathe |
  e' c a |
  b e e, |
  a4. gis8 a (b) |
  gis2. |
  \break
  e'4\mp c a |
  f'2 e4 |
  d c4. d8 |
  b2 a4\breathe |
  e' c a |
  b e e, |
  a4. gis8 a (b) |
  gis2. |
  \break
  \repeat volta 2 {
    e'4\mf g,4. g8 |
    g2 (fis4) |
    d'4 f,4. f8 |
    f2 (e4) |
    c' bes d |
    gis,4. gis8 a4 |
    a8 (b) b4. (a8) |
    a2. |
  }
}

cello = \relative c {
  \global
  % Music follows here.
  c'4 c2 |
  d2 c4 |
  b4 a2 ~ |
  a4 gis a\breathe |
  a4 a2 |
  g2 e4 |
  f8 (e) f2 |
  e2. |
  \break
  c'4 a2 |
  gis8 (a) b4 c |
  b a2 ~ |
  a4 gis a\breathe |
  c a2 |
  g2 e4 |
  f8 (e) f2 |
  e2. |
  \break
  \repeat volta 2 {
    cis4 a cis |
    d (e d ) |
    b g b |
    c (b c) |
    c d2 |
    e4. d8 c4 |
    d4 e2 |
    a2. |
  }
}

clarinet = \relative c'' {
  \global
  \transposition bes
  % Music follows here.

}

violinPart = \new Staff \with {
  instrumentName = "Violin"
} \violin

celloPart = \new Staff \with {
  instrumentName = "Cello"
} { \clef bass \cello }

clarinetPart = \new Staff \with {
  instrumentName = "Clarinet"
} \clarinet

\score {
  <<
    \violinPart
    \celloPart
   % \clarinetPart
  >>
  \layout { }
}
