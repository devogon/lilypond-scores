\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "10. Hey Disco"
  instrument = "Dwarsfluit"
  arranger = "Fons van Gorp"
  poet = "Fluit, Hobo, Viool"
  copyright = "1996 De Haske Publications, BV"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
%  \partial 4
  \tempo "Disco" 4=126
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

\include "parts/hey-disco-flute.ly"

\score {
  \new Staff \flute
  \layout { }
}
