\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "12 Rag Is Back Flute"
  subsubtitle = "RAGTIME"
  instrument = "Dwarsfluit"
  arranger = "Fons van Gorp"
  poet = "Fluit, Hobo, Viool"
  copyright = "1996 De Haske Publications, BV"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 2/2
  \tempo "Two-step" 4=160
  \partial 2.
}

\include "parts/rag-is-back-flute.ly"

\score {
  \new Staff \flute
  \layout { }
}
