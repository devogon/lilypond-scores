flute = \relative c'' {
  \global
  % Music follows here.
    R1*3/4*8
    \break
    f,4\mp ( g a |
    bes2 ) d,4 ( |
    es2 d'4 |
    c2. ) |
    d,2 c'4 ( |
    bes4 c d |
    \break
    c2.\< ~ |
    c2\! ) bes4\mf ( |
    f4 e! f |
    es'2 d4 ) |
    es,4 ( d es |
    d'2 c4 ) |
    \break
    f2. (
    f2.  )(
    f2. )(
    f4\> ) r r |
    f,4\!\mp ( g a |
    bes2 ) d,4 ( |
    \break
    es2 d'4 |
    c2. ) |
    d,2 c'4 ( |
    bes4 c d |
    e!2. ~ |
    e4\< ) d ( e |
    \break
    fis2\f a,4 ) |
    e!2 (a,4) |
    d2. ~ |
    d2\> r4\! |
    f4\mf ( g f |
    e!2 d4 |
    \break
    e'!2 c4 |
    g2. ) |
    es'4 ( f es |
    d2 c4 |
    d2 bes4 |
    f2. ) |
    \break
    f2. ( |
    c'2. ) |
    f,2. ( |
    d'2. ) |
    f,2. ( |
    c'2. ) |
    \break
    f,2. ( |
    d'2. ) |
    f,2. ( |
    c'2. ) |
    bes2.\mp ~ |
    bes2. ~ |
    bes2. ~ |
    bes2. |
    bes2.\p ~ |
    bes2. |
    bes4\staccato r r |
    r1
    \bar "|."
}
