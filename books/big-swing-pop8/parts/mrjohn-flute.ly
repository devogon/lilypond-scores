flute = \relative c'' {
  \global
  % Music follows here.
  R1*4
  \break
  \repeat volta 2 {
    r4^\markup{\italic "Flute 8va ad lib"} a16\mp (bes a g ~ g8) f16 ( e ~ e8) g8\staccato  | % 5
    f8 ( d ~ d4) r2 | % 6
    r4 a'16 (bes c8 ~ 8 bes) bes8 ( a | % 7
    g2.) r4 | % 8
    \break
    r8 e16 (f g8 a16 f ~ f8 d) r4 | % 9
    r8  g16 ( a bes8 c16 a\< ~ a4) r8\! f8\staccato | % 10
  }
  \alternative {
    { d'4\mf (c bes a) g2\> (c\!) \break}
    { d4\mf (c a f)}
  }
  f4\tenuto f8\staccato\> f ~ f4\! f16\< (a c8) | % 14
  \repeat volta 2 {
    d4\tenuto\f\! f\tenuto bes,\tenuto d\tenuto | % 15
    c4\tenuto bes8 (a ~ a4) r8 f8\staccato | % 16
    \break
    bes8\tenuto bes\tenuto bes\tenuto bes\tenuto bes\tenuto\> bes\tenuto c\tenuto (d16 c ~  | % 17
    c2)\! r4 f,16\< (a c8) | % 18
    d4\tenuto\! f\tenuto bes,\tenuto d\tenuto | % 19
    c4\tenuto e8\< (f-> ~ f4) r8\! f,8\staccato\mp
    \break
  }
  \alternative {
    {a8\tenuto f\tenuto f\tenuto f\tenuto ~ f4 f8\tenuto f8 ~ f2. f16\< ( a c8)\!}
    {a8\tenuto f\tenuto f\tenuto f\tenuto ~ f4 f8\tenuto f8 ~ f2_\markup{\italic ritard.}\> f\tenuto}
  }
  f1\p\!
  \bar "|."
}