flute = \relative c'' {
  \global
				% Music follows here.
  r4 r2 |
  \bar "||"
  g8\f\tenuto^\markup{\bold Intro} g4\staccato g8-> ~ ( g a g4\staccato ) |
  r1 |
  b8\tenuto b4\staccato b8-> ~ ( b a as4\staccato ) |
  g4\staccato r r2 |
  \break
  e'4\staccato\mf\segno a8\tenuto g\staccato r e\tenuto d4\staccato |
  c4\staccato f8\tenuto e\staccato r c\tenuto a4\staccato |
  g4\staccato e'2-> d4 |
  c4\staccato r r2 |
  \break
  a4\staccato d8\tenuto c\staccato r a\tenuto g4\staccato |
  f4\staccato bes8\tenuto a\staccato r f\tenuto d4\staccato |
  c4\staccato a'2-> g4 |
  f4\staccato r r8 e' ( f fis |
  \break
  g4\staccato ) g8\tenuto ( bes-> ~ bes g\tenuto a4\staccato ) |
  f4\staccato f8\tenuto ( as-> ~ as f\tenuto g4\staccato ) |
  e4\staccato  e8\tenuto ( g-> ~ g e\tenuto f4\staccato ) |
  d2. r4 |
  \break
  e4\staccato a8\tenuto g\staccato r e\tenuto c4\staccato |
  g8\tenuto e'\staccato\< r d\tenuto c4\staccato g4\staccato |
  as8\f\!\coda (bes) as2-> g4\staccato |
  c4\staccato r r2 |
  \break
  \key f \major
  \repeat volta 2 {
    a2\mp (d4\staccato) c4\tenuto |
    r4 f, ( g as ) |
    a2 (bes4\staccato) a4\tenuto |
    r4 a ( bes a )
    \break
    a2 (g4\staccato) a\tenuto |
    r4 g ( e d |
    c2 ) r |
    r1 |
    \break
  }
  \alternative {
    { a'2\mp (d4\staccato) c\tenuto |
      r4 f, ( g gis ) |
      a2 (bes4\staccato) a\tenuto |
      r4 a (bes a) |
      \break
      a2 (gis4\staccato) c4\tenuto |
      r4 b! ( a gis) |
      a1\> ( |
      bes2\! ) r |
      \break
    }
    {
      d,4\mf ( c' bes ) f-> |
      r4 f ( g gis ) |
      a4 ( e' d ) a-> |
      r4 a\< ( bes b! ) |
      \break
      c8\f\tenuto\! c4\staccato c8 r c\tenuto c4\marcato\staccato |
      r2 a (  |
      f1\> ) |
      r\!^\markup{D.S.}
      \key c \major
    }
  }
  \break
  as8\f\coda^\markup{\bold CODA} ( bes) as2-> es4\staccato |
  a!8 (b!) a2-> e!4\staccato |
  as8 ( bes) as2-> g4\staccato |
  c4\marcato r r2 |
  r4
  \bar "|."
}