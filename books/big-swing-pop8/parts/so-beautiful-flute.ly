flute = \relative c'' {
  \global
  % Music follows here.
    r4
    \bar "||"
    r2 \segno
    \time 4/4
    r1
    \break
    \repeat volta 2 {
    e2\p^\markup{\italic{(Flute loco)}} (c4.) c16 ( d |
    e8 f g e ~  e2 ) |
    e2 (c4) c16 ( a c8 ~ |
    c2. ) bes8 ( c |
    \break
    d4\< e f g |
    }
    \alternative {
    { a2\coda\!\mf f4 ) f16 ( d f8 ~ f2 ) a4 (g8 f f1 ) }
    { \break a2\mf f4 f16 ( d a'8 ~ a2 ) a2\> (f2\!) r }
    }
    \key c \major
    a2\mf (e4.) c16 ( d |
    \break
    e8 f e d ~ d4 ) c8 <d b>8^\markup{\italic{(flute)}}_\markup{\italic{(Hobo/Viool)}} |
    e,4\< ( a c e\f ~ |
    e2. ) d4 ( |
    c2 a4. ) a16 (b) |
    \break
    c8 ( d c b ~ b4.) a8\staccato\mf |
    bes8 ( c bes a ~ a4. ) g8\staccato\mp |
    as8 ( bes as g ~ g4 ) r |
    \time 2/4
    \key f \major
    \mark "D.S."
    \time 4/4
    \break
    a'2\mf\coda (f4) f16 ( d a'8 ~ |
    \override TextSpanner.bound-details.left.text =
  \markup { \upright "rit." }
    a2\startTextSpan\> ) a |
    f1\!\p\fermata\stopTextSpan
    \bar "|."
}