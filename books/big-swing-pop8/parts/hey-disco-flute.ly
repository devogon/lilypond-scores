
flute = \relative c'' {
  \global
  % Music follows here.
  R1*3
  r2 r8 c\staccato\mf d c\staccato |
  \break
  f4->\segno d-> c8\tenuto d4\staccato f,8-> ~ |
  f2 r |
  r1
  r2 r8 c'\staccato d\tenuto c\staccato |
  \break
  f4-> d-> as8\tenuto g4\staccato f8-> ~ |
  f2 r |
  r1 |
  r2 r8 f8\staccato g\tenuto f\staccato |
  \break
  bes8 ( as g f ~ f2 ) |
  r2 r8 f8\staccato g\tenuto f\staccato |
  bes8 ( des c bes ~ bes ) d!\< ( es f-> ~ |
  f2.\f\! ) r4\coda |
  \break
  c8.\tenuto c16\staccato r8 bes8\staccato c8.\tenuto c16\staccato r8 bes8\staccato |
  c8.\tenuto c16\staccato r8 es-> ~ es2 |
  c8.\tenuto c16\staccato r8 bes8\staccato c8.\tenuto c16\staccato r8 bes8\staccato |
  c8.\tenuto c16\staccato r4 r8 c8\staccato\mf d8\tenuto c\staccato^\markup{D.S.} |
  \break
  \repeat volta 2 {
    c8.\tenuto\f^\coda c16\staccato r8 bes\staccato c8.\tenuto c16\staccato r8 bes\staccato |
    c8.\tenuto c16\staccato r8 es8-> ~ es2 |
    c8.\tenuto c16\staccato r8 bes8\staccato c8.\tenuto c16\staccato r8 bes8\staccato |
    c8\staccato bes ( a g ~ g2 ) |
  }
  \break
  c8.\tenuto^\markup{\italic {Flute 8va ad lib}} c16\staccato r8 bes8\staccato c8.\tenuto c16\staccato r8 bes8\staccato |
  c8.\tenuto c16\staccato r8 es8-> ~ es2 |
  c8.\tenuto c16\staccato r8 bes8\staccato c8.\tenuto c16\staccato r8 bes8\staccato |
  c4\marcato r r2 |
  \bar "|."
}