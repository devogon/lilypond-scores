flute = \relative c' {
  \global
  % Music follows here.
  r1 |
  r2 r4 a'8\tenuto\mf a\staccato |
  \break
  \repeat volta 2 {
    d4-> a8 ( g f d a' g-> ~ |
    g4) f8 ( g a c e d-> ~ |
    d8) a\tenuto a\tenuto a-> ~ a2 |
    r2 r4 a8\tenuto a\staccato |
    \break
    d4-> a8 ( g f d a' g-> ~ |
    g4) f8 ( g a c e d-> ~ |
    d2 ) r |
    r1
    \break
    r4 g,8\staccato\f g\staccato f ( a bes a-> ~ |
    a2 ) r |
    r4 g8\staccato g\staccato g ( a bes a-> ~ |
    a2 ) r |
    \break
    r4 g8\staccato g\staccato g ( a bes a\marcato ) |
    r2 f'8\mp ( e c d ~ |
    d2 ) r |
    r2 r4 a8\tenuto\mf a\staccato
  }
  \break
  d4\marcato\f c8 ( a c a d4\staccato ) |
  c8 ( a g a ~ a4 ) a8\tenuto a\staccato |
  d4\marcato c8 ( a c a d c-> |
  c2 ) r4 a8\tenuto a\staccato |
  \break
  d4\marcato\f c8 ( a c a d4\staccato ) |
  c8 ( a g a ~ a4 )  r4 |
  f'1->\sfp\< |
  e4\!\f\marcato e8\staccato\mp e\staccato f ( e c d ~ |
  \break
  d2 ) r |
  r1 |
  d4\f\staccato r8 d-> ~ d4 c4\staccato |
  d4\marcato r r2 |
  \bar "|."
}
