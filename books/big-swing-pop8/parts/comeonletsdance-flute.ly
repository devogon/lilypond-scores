flute = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
    R1*7 |
    r2 r4 r8 d8\mf^\markup{"(Flute loco)"}-> ~ ( |
    \break
    d8 c d c d\tenuto d\staccato) r f-> ~ ( |
    f8 d f d f4) r8 g-> ~ ( |
    g8 f g a f\tenuto d\staccato ) r8 d-> ~ |
    d2 r4 r8 d8-> ~ ( |
    \break
    d8 c d c d\tenuto d\staccato ) r g-> ~ ( |
    g8 d g d g4) r8 g8-> ~ ( |
    g8 f g a f\tenuto d\staccato ) r8 d-> ~ |
    d2 r4 r8 r16 c->^\markup{flute 8va ad lib.} ~ ( |
    \break
    c8 b c b c\tenuto d\staccato ) r c8-> ~ ( |
    c8 b c b c\tenuto d\staccato) r c-> ~ ( |
    c8 b c b c d b\tenuto a-> ~ |
    a2) r4 r8 r16 c-> ~ (
    \break
    c8  b c b c\tenuto d\staccato ) r c-> ~ ( |
    c8 b c b c\tenuto d\staccato ) r e-> ~ ( |
    e8 cis b a\staccato ) r e'-> ( cis a\> ~
    a2 ) r\!
  }
  \break
  R1*3 |
  r4 r8 g\staccato\mf^\markup{\italic "Flute 8va"} c\staccato a\staccato g16\tenuto f8\staccato d16-> ~ |
  d2 r |
  R1*2 |
  r4 r8 g\staccato\mf^\markup{\italic "Flute 8va"} c\staccato a\staccato g16\tenuto f8\staccato d16-> ~ |
  \break
  d2 r |
  R1*2 |
  r2 r8 d'8\f^\markup{\italic "Flute 8va"} ( c a ~
  \repeat volta 3 {
    a8) ) g ( f d->) r d' ( c a-> ~ |
    \break
    a) g ( f d->) r d' ( c a-> ~ |
    a) g ( f  d->) r c-> ~ c d-> ~
  }
  \alternative {
    {d2 r8 d' c a->  }
    {d,2\repeatTie r}
  }
  \bar "|."
}