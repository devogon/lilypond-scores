flute = \relative c'' {
  \global
  % Music follows here.
  r1 |
  r2 r8 bes8\mp^\markup{\italic{Flute 8va ad lib.}} ( a f ~ |
  \break
  \repeat volta 2 {
    f2.) r4 |
    r2 r8 bes ( a es ~ |
    es2.) r4 |
    r2 r4 r8 f8\staccato |
    \break
    d'8\tenuto\< d\tenuto d\tenuto d\tenuto\! ~ d4 r8 d8\staccato |
    d8\tenuto d\tenuto d\tenuto f-> ~ f8 bes,\tenuto bes\> c\tenuto ~ |
    c2.\! r4\coda
  }
  \alternative {
    { r2 r8 bes8 ( a f^\markup{fixme} )  }
    % slur bes - f in bar 3, also tie f to same f
    {\break r1 }
  }
  \repeat volta 2 {
    r4 r8 bes8\mf (a f d g ~ |
    g4) d16 ( c d8 ~ d2) |
  }
  \alternative {
    {r4 r8 bes'8 ( a f d es ~ \break es8 g\< d' c ~ c2\!\f)}
    {r4 r8 bes8 ( a f d c ~ c8) d ( es f ~ f8) g (bes c-> ~ c8) d ( es f\!\ff-> ~ f2)\DCcoda }
  }
  r2^\markup{Coda}\Coda r8 es8\p^\markup{\italic {Flute loco}} ( d bes ~
  bes2.) r4 |
  r1
  bes1
  \break
  r1
  r4^\markup{\italic {freely}}a8\pp^\markup{\italic{Flute 8va}} ( bes a f d g ~ |
  g1\fermata)
  \bar "|."
}
