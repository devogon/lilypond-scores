flute = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
  r1
  c4\tenuto\f c4\tenuto c8\tenuto d\staccato r4 | % 2
  r1 | % 3
  c4\tenuto\f c4\tenuto c8\tenuto d\staccato r4 | % 4
  \break
  r1
  c4\tenuto\f c4\tenuto c8\tenuto d\staccato r4 | % 6
  es4.\staccato d8-> ~ d2 | % 7
  es4.\staccato d8-> ~ d2 | % 8
  \break
  es4.\staccato d8-> ~ d4 c4-> ~ | % 9
  c2 r4^\markup{"1st time only"}\ottava #1 g'4\staccato\mp | % 10
  \bar "||"
  bes2. as8\tenuto g\staccato | % 11
  r8 c,8\staccato r4 r2 | % 12
  \break
  f8 (es f es f es\staccato) r8 g\staccato | % 13
  r8 c,\staccato r4 r g'\staccato | % 14
  bes2. as8\tenuto g\staccato | % 15
  r8 c\marcato r4 r2 | % 16
  \break
  f,8\mf ( es f es f\< es\staccato) r8 g->\ff\! ~ | % 17
  g2 \unset Staff.ottavation r | % 18
  }
  r1 | % 19
  \mark \markup{\italic \teeny "play"}
  g'4\tenuto^\markup{"Flute loco"}\f g\tenuto g8\tenuto f\staccato r4 | % 20
  \break
  r1 | % 21
  g4\tenuto g\tenuto g8\tenuto f\tenuto r4 | % 22
  r1 | % 23
  g4\tenuto g\tenuto g8\tenuto f\tenuto r4 | % 24
  \break
  g4. f8-> f2 | % 25
  g4. f8-> f2 | % 26
  g4. <bes bes,>8\< ~ <bes bes,>4-> <c c,>4-> ~ | % 27
  <c c,>1\ff\fermata\!
  \bar "|."
}