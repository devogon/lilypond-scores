\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Take Off"
  subtitle = "(Big Swing-Pop)"
  instrument = "Flute"
  arranger = "Fons van Gorp"
  piece = "1"
  copyright = "1996 De Haske Publications, BV"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "Disco" 4=126
}

flute = \relative c' {
  \global
  % Music follows here.
  R1 | % 1
  R1 | % 2
  R1 | % 3
  R1 | % 4
  \break
  \repeat volta 2 {
    d8\tenuto\p e4\tenuto f8\tenuto\< ~ f d4\tenuto g8\!\sfp-> ~ | % 5
    g2. f4\staccato |% 6
    d8\tenuto\p e4\tenuto f8\tenuto\< ~ f d4.\tenuto | % 7
    as'8\!\sfp g ~ g2. | % 8
    \break
    d8\tenuto\p e4\tenuto f8\tenuto\< ~ f d4.\tenuto | % 9
    as'8\!\sfp g4. ~ g8 f ( g a!\tenuto\< ~ | % 10
    a2 ~ a8) c ( g a\!-> ~ | % 11
  }
  \alternative {
    {a2) r \break}
    {a4. a8\f ( c d c d-> ~ }
  }
    d2 ~ d8) c  (a f) | % 14
    g8\staccato g ( f g ~ g4) r | % 15
    d'2-> ~ d8 c ( a f) | % 16
    \break
    g8\staccato g ( a g ~ g4) r | % 17
    d'2-> ~ d8 c ( a f) | % 18
    g8\staccato g ( f g-> ~ g ) f ( g a-> ~ | % 19
    a8) f (g a-> ~ a) f ( g a-> ~ | % 20
  \break
    a2.) r4 | % 21
    r1 | % 22
    r4 r8 d,8\f f (d) c'\tenuto_\markup{\italic rit.} b!-> ~ | % 23
    b1\fermata
    \bar "|."
}


flutePart = \new Staff \with {
  %instrumentName = "Flute"
 % shortInstrumentName = "Fl."
} \flute



\score {
  <<
    \flutePart
  >>
  \layout { }
}
