\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Lonely Rock"
  poet = "Fluit, Hobo, Viool"
  instrument = "dwarsfluit"
  arranger = "Fons van Gorp"
  copyright = "1996 De Haske Publications, BV"
  tagline = \date
  piece = "7"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 4/4
  \tempo "8 beat" 4=90
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

Coda = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#f #t #t)
  \mark \markup { \small \musicglyph #"scripts.coda" }
}

DCcoda = {
  \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  \mark \markup { \small "D.C." }
}

\include "parts/lonely-rock-flute.ly"

\score {
  \new Staff \flute
  \layout { }
}
