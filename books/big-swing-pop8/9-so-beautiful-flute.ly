\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "9. So Beautiful"
  instrument = "Dwarsfluit"
  arranger = "Fons van Gorp"
  poet = "Fluit, Hobo, Viool"
  copyright = "1996 De Haske Publications, BV"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \numericTimeSignature
  \key f \major
  \time 2/4
  \partial 4
  \tempo "Ballad" 4=68
}

\include "parts/so-beautiful-flute.ly"

\score {
  \new Staff \flute
  \layout { }
}
