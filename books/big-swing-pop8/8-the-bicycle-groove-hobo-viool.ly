\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "8. The Bicycle Groove"
  instrument = "Dwarsfluit"
  arranger = "Fons van Gorp"
  poet = "Hobo, Viool"
%  piece = "8"
  copyright = "1996 De Haske Publications, BV"
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "8 beat" 4=130
}

\include "parts/the-bicycle-groove-hobo.ly"

\score {
  \new Staff \relative c {\override Score.BarLine.stencil = ##f
 \key f \major d'1_\markup{improvisatiereeks} f g \parenthesize gis a c d }
  \layout {}
}

\score {
  \new Staff \flute
  \layout { }
}
