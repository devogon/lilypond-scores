\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Easy, Man!"
  subtitle = "Blues"
  subsubtitle = "(Big Swing-Pop)"
  instrument = "Fluit, Hobo, Viool"
  arranger = "Fons van Gorp"
  copyright = "1996 De Haske Publications, BV"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "Swing" 4=82
  \partial 4
}

flute = \relative c' {
  \global
  % Music follows here.
  r
  r1
  r2 r4 \tuplet 3/2 {c8\mp^\markup{\italic "8va ad lib."} (d c)} | % 2
  \break
  f4\tenuto f8\tenuto f\staccato r4 \tuplet 3/2 {c8 (d c)} | % 3
  f4\tenuto f8\tenuto f\staccato r4 \tuplet 3/2 {c8 (d c)} | % 4
  f4\tenuto f8\tenuto f\staccato r8 as ( g f ~ | % 5
  f2) r4 \tuplet 3/2 {c8 (d c)} | % 6
  \break
  f4\tenuto f8\tenuto f\staccato r4 \tuplet 3/2 {c8 (d c)} | % 7
  f4\tenuto f8\tenuto f\staccato r4 \tuplet 3/2 {c8 (d c)} | % 8
  f4\tenuto f8\tenuto f\staccato r8 as ( g f ~ | % 8
  f2) r4 \tuplet 3/2 {c8 (d c)} | % 10
  \break
  a'8\tenuto c-> ~ c2 a8\tenuto c,\staccato | % 11
  r4 \tuplet 3/2 {c8 (d c)} as'8 (g f d | % 12
  f4) es8\tenuto es\tenuto\> d\tenuto d\tenuto des\tenuto des\tenuto  | % 13
  c4\tenuto\! r r r8 c\staccato\mf | % 14
  \bar "||"
  \break
  a'8 ( g f d f a4) c,8\staccato | % 15
  as'8 ( g f d f as4) c,8\staccato | % 16
  a'8 (g f d f) a (bes c) ( | %  17
  c8) d (as g ~ g4) r8 c,\staccato | % 18
  \break
  as'8 ( g f d f as4) c,8\staccato | % 19
  as'8 ( g f d f as4) c,8\staccato | % 20
  a'8 (g f d f) a\< (bes c-> ~ | % 21
  c2\!) r | % 22
  \break
  bes8\f (a bes a bes c as\tenuto g\staccato) | % 23
  r4 \tuplet 3/2 {c,8\mf (d c)} as'8 (g f d | % 24
  f4) es8 (f_\markup{\italic "poco ritand."} d\tenuto f des f | % 25
  c4) as'8\tenuto g-> ~ g2\fermata  |% 26
  \bar "|."
}

\score { \new Staff {
  \clef treble
  \key f \major
  \relative c {f'1_\markup{"improvisatiestreeks"} as bes b! c es f}
  }
  \layout {indent = 0.0\cm}
}

\score {
  \new Staff \with {
    %instrumentName = "Flute"
    %shortInstrumentName = "Fl."
  } \flute
  \layout { }
}
