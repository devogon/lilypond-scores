\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Caribbean Express"
  subtitle = "(Big Swing-Pop)"
  instrument = "Fluit"
  arranger = "Fons van Gorp"
  copyright = "1996 De Haske Publications, BV"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
  page-count = 1
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key es \major
  \time 4/4
  %\partial 2
  \tempo "Latin Rock" 4=68
}

\include "parts/caribbean-flute.ly"

oboe = \relative c'' {
  \global
  % Music follows here.
  \repeat volta 2 {
  r1 | % 1
  as4\tenuto\f as4\tenuto as8\tenuto bes\staccato r4 | % 2
  r1 | % 3
  as4\tenuto\f as4\tenuto as8\tenuto bes\staccato r4 | % 4
  \break
  r1
  as4\tenuto\f as4\tenuto as8\tenuto bes\staccato r4 | % 6
  c4. bes8-> ~ bes2 | % 7
  c4. bes8-> ~ bes2 | % 8
  \break
  c4. bes8-> ~ bes4 c4-> ~ | % 9
  c2 r | % 10
  r4 a4\staccato\mp^\markup{"2nd time only"}\ottava #1  ges'8 (f es c-> ~ | % 11
  c4) c8 ( des c\tenuto es\staccato) r4 | % 12
  \break
  r4 es4\staccato f8 ( g bes g ~ | % 13
  g2) r | % 14
  r4 c4\staccato ges8 ( f es c-> ~ | % 15
  c4) c8 (des c\tenuto f\staccato) r4 | % 16
  \break
  r8 c\mf (es f g\< bes c es->\ff\! ~ | % 17
  es2)\unset Staff.ottavation  r | % 18
  }
  r1 | % 19
  es4\tenuto\f^\markup{"Flute loco"} es\tenuto es8\tenuto d\staccato r4 | % 20
  \break
  r1 | % 21
  es4\tenuto es\tenuto es8\tenuto d\staccato r4 | % 22
  r1 | % 23
  es4\tenuto es\tenuto es8\tenuto d\staccato r4 | % 24
  \break
  es4. d8-> ~ d2 | % 25
  es4. d8-> ~ d2 | % 26
  es4. d8->\< ~ d4 f4-> ~ | % 27
  f1\ff\fermata\! | % 28
  \bar "|."
}

flutePart = \new Staff \with {
%  instrumentName = "Flute"
%  shortInstrumentName = "Fl."
} \flute

oboePart = \new Staff \with {
  instrumentName = "Oboe"
  shortInstrumentName = "Ob."
} \oboe

\score { \new Staff {
  \clef treble
  \key es \major
  \relative c {c'1_\markup{"improvisatiestreeks over gedeelte maat 11-18"} es f fis g bes c}
  }
  \layout {indent = 0.0\cm}
}
\score {
  <<
    \flutePart
%    \oboePart
  >>
  \layout { }
}
