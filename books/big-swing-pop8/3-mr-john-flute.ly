\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Mr. John"
  subtitle = "(Big Swing-Pop)"
  instrument = "Fluit, Hobo, Viool"
  arranger = "Fons van Gorp"
  copyright = "1996 De Haske Publications, BV"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "8 beat" 4=68
  \compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

\include "parts/mrjohn-flute.ly"

\score {
  \new Staff \with {
%    instrumentName = "Flute"
%    shortInstrumentName = "Fl."
  } \flute
  \layout { }
}
