\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "11. Waltz for L."
  instrument = "Dwarsfluit"
  arranger = "Fons van Gorp"
  poet = "Fluit, Hobo, Viool"
  copyright = "1996 De Haske Publications, BV"
  tagline = \date
%  meter = \markup{"Jazzwaltz (" \tiny \note-by-number #2 #0 #UP " = 168)"}
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \numericTimeSignature
  \time 3/4
  \tempo \markup {"Jazzwaltz " \tiny \note-by-number #2 #0 #UP " = 168"
    \concat {
      (
      \smaller \general-align #Y #DOWN \note #"8" #1
            \smaller \general-align #Y #DOWN \note #"8" #1
      " = "
            \smaller \general-align #Y #DOWN \note #"4" #1
      \smaller \general-align #Y #DOWN \note #"8" #1
      )
    }
  }
%  \tempo "Jazzwaltz" 4=168
\compressFullBarRests
\override MultiMeasureRest #'expand-limit = 1
}

\include "parts/waltz-for-l-flute.ly"

\score {
  \new Staff \flute
  \layout { }
}
