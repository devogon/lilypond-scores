\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "It‘s Rocktime, Baby"
  subtitle = "(Big Swing-Pop)"
  instrument = "Flute"
  arranger = "Fons van Gorp"
  copyright = "1996 De Haske Publications, BV"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  %page-count = 1
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo "Rock" 4=136
}

flute = \relative c' {
  \global
  % Music follows here.
  r1
  r1
  r1
  r1
  \break
  \repeat volta 2 {
    r4 f'\f f8\tenuto e\staccato r f-> ~ | % 5
    f4 f\staccato f8\tenuto e\staccato r d-> ~ | % 6
    d4. c8-> ~ c4. c8-> ~  | % 7
    c4 f4\staccato f8\tenuto e\staccato r4 | % 8
    \break
    r4 f\staccato f8\tenuto e\staccato r f-> ~ | % 9
    f4 f\staccato f8\tenuto e\staccato r d-> ~ | % 10
    d4. c8-> ~ c4. c8-> ~ | % 11
    c4 f\staccato f8 e\staccato r4 | % 12
    \bar "||"
    \break
    r1 | % 13
    r4 r8 g,8\staccato\mp b (d b g ~ |% 14
    g2 ) r2 | % 15
    r8 g\staccato b ( d ) e ( d b g ~ | % 16
    \break
    g2) r | % 17
    r4 r8 g\staccato b\< ( d b d\!\f ~ | % 18
    d8 ) \repeat unfold 7 {d8\upbow} | % 19
    c8\upbow c8\upbow c\tenuto f-> ~ f4 r | % 20
  }
  \break
    r4 a,\staccato\f^\markup{\italic "Flute 8a ad lib"} a8\tenuto g\staccato r a-> ~ | % 21
    a4 a\staccato a8\tenuto c\staccato r bes-> ~ | % 22
    bes4. a8-> ~ a4. g8-> ~ | % 23
    g4 a4\staccato a8\tenuto g\staccato r4 | % 24
    \break
    r4 a4\staccato a8\tenuto g\staccato r a-> ~ | % 25
    a4 a4\staccato a8\tenuto c\staccato r bes-> ~ | % 26
    bes4. a8-> a4. c8-> ~ | % 27
    c1 | % 28
    \bar "|."
  }


% chordNames = \chordmode {
%   \global
%   % Chords follow here.
%
% }
%
% right = \relative c' {
%   \global
%   % Music follows here.
%   r4 <f-5 a,-1>4\staccato\f <f a,>8\tenuto <e-4 g,-1>\staccato r <f-5 a,-1>-> ~ | % 1
%   <f a,>4 <f a,>\staccato <f a,>8\tenuto <e-4 g,-1>\staccato r8 <d-5 f,-1>-> ~ | % 2
%   <d f,>4. <c-5 a-3 f-1>8-> ~ <c a f>4. <c-5 e,-1>8 ~ | %  3
%   \break
%   <c e,>4 <f-5 a,-1>\staccato <f a,>8\tenuto <e-4 g,-1>\staccato r4 | % 4
%   \repeat volta 2 {
%     r4 <f-5 a,-1>4\staccato\f <f a,>8\tenuto <e-4 g,-1>\staccato r <f-5 a,-1>-> ~ | % 5
%     <f a,>4 <f a,>\staccato <f a,>8\tenuto <e-4 g,-1>\staccato r8 <d-5 f,-1>-> ~ | % 6
%     <d f,>4. <c-5 a-3 f-1>8-> ~ <c a f>4. <c-5 e,-1>8 ~ | % 7
%     \break
%     <c e,>4 <f-5 a,-1>\staccato <f a,>8\tenuto <e-4 g,-1>\staccato r4 | % 8
%     r4 <f-5 a,-1>4\staccato\f <f a,>8\tenuto <e-4 g,-1>\staccato r <f-5 a,-1>-> ~ | % 9
%     <f a,>4 <f a,>\staccato <f a,>8\tenuto <e-4 g,-1>\staccato r8 <d-5 f,-1>-> ~ | % 10
%     <d f,>4. <c-5 a-3 f-1>8-> ~ <c a f>4. <c-5 e,-1>8 ~ | % 11
%     <c e,>4 <f-5 a,-1>\staccato <f a,>8\tenuto <e-4 g,-1>\staccato r4 | % 12
%     r4\mp <b-4 d,-1>\staccato r8 <c-5 e,-1>\staccato ~ <c e,> <d-5 f,-1>-> ~ | % 13
%     <d f,>4 <c-5 e,-1>8\tenuto <b-4 d,-1>\staccato r2 | % 14
%     r4 <b d,>\staccato r8 <c e,>\staccato ~ <c e,> <d f,>-> | % 15
%     <d f,>4 <c e,>8\tenuto <b d,>\staccato r2 | % 16
%     r4 <b d,>4\staccato r8 <c e,>8\staccato ~ <c e,> <d f,>-> ~ | % 17
%     <d f,>4 <c e,>8\tenuto <b d,>\staccato\< r4 r8 <d-5 bes-3 f-1>\f\!-> ~ | % 18
%     % 19 below
%     <d bes f>8 <d bes f>\upbow <d bes f>\upbow <d bes f>\upbow <d bes f>\upbow <d bes f>\upbow <d bes f>\upbow <d bes f>\upbow | % 19
%     <c-4 a-2 f-1>8\upbow <c a f>\upbow <c a f>\tenuto <c a f>-> ~ <c a f>4 r | % 20
%   }
%   r4\f <f a,>\staccato <f a,>8\tenuto <e g,>\staccato r8 <f a,>-> ~ | % 21
%   <f a,>4 <f a,>4\staccato <f a,>8\tenuto <e g,>\staccato r8 <d f,>8-> ~ | % 22
%   <d f,>4. <c a f>8-> ~ <c a f>4. <c e,>8 ~ | % 23
%   <c e,>4 <f a,>\staccato <f a>8 <e g>\staccato r4 | %  24
%   r4 <f a,>4\staccato <f a,>8\tenuto <e g,> r8 <f a,>-> ~ %  25
%   <f a,>4 <f a,>4\staccato <f a,>8 <e g,>\staccato r <d f,>-> | % 26
%   <d f,>4. <c a f>8-> ~ <c a f>4. <c e,>8 ~ | % 27
%   <c e,>1
% }
%
% left = \relative c {
%   \global
%   % Music follows here.
%   \repeat unfold 4 {r1}
%   \repeat volta 2 {
%     <c,-1 c'-5>2 ~ <c c'>8 g'-3 a-2 <c, c'>8-> ~ | % 5
%     <c c'>2 ~ <c c'>8 g'-3 a-2 bes-1-> ~ | % 6
%     bes8 a-2 g-1 f-> ~ f es d <c c'>-> ~ | % 7
%     <c c'>2 ~ <c c'>8 g'-3 a <c c,>-> ~ | % 8
%     <c c,>2 ~ <c c,>8 g a <c c,>8-> ~ | % 9
%     <c c,>2 ~ <c c,>8 g a bes->-1 ~ | % 10
%     bes-2 a-1 g f-> ~ f es d <c c'>-> ~ | % 11
%     <c c'>2 ~ <c c'>8 f-3 ( fis-2 g-1 ~ ) | % 12
%     g4. g8-2 g4.-1 g8 ~ | % 13
%     g2 ~ g8 f-3 ( fis g ~ | % 14
%     g4. ) g8 g4. g8 ~ | % 15
%     g2 ~ g8 f-3 ( fis g ~  | % 16
%     g4. ) g8 g4. g8 ~  | % 17
%     g2 ~ g8 g-3 ( a-2 bes-1 ~  | % 18
%     bes8 ) \repeat unfold 7 {bes\upbow}  | % 19
%     f8\upbow f\upbow f\tenuto f-> ~ f g\tenuto-3 a4\staccato-2 | % 20
%   }
%   <c c,>2 ~ <c c,>8 g a <c c,>-> ~ | % 21
%   <c c,>2 ~ <c c,>8 g a bes-> ~  | % 22
%   bes8 a g f-> ~ f es d <c c'>8-> ~ | % 23
%   <c c'>2 ~ <c c'>8 g' a <c c,>8 ~ | % 24
%   <c c,>2 ~ <c c,>8 g a <c c,>-> ~ | % 25
%   <c c,>2 ~ <c c,>8 g a bes-> ~ | % 26
%   bes a g f-> ~ f es d <c c'>-> ~  | % 27
%   <c c'>1 | % 28
%   \bar "|."
% }

flutePart = \new Staff \with {
  %instrumentName = "Flute"
  %shortInstrumentName = "Fl."
} \flute

% chordsPart = \new ChordNames \chordNames
%
% pianoPart = \new PianoStaff \with {
%   instrumentName = "Piano"
%   %shortInstrumentName = "Pno."
% } <<
%   \new Staff = "right" \right
%   \new Staff = "left" { \clef bass \left }
% >>

\score {
  <<
    \flutePart
    % \chordsPart
%     \pianoPart
  >>
  \layout { }
}
