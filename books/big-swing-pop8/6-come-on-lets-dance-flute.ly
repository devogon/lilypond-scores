\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = \markup{\larger "Come On Let‘s Dance"}
  subtitle = "(Big Swing-Pop)"
  instrument = "Flute, Oboe, Violin"
  arranger = "Fons van Gorp"
  % Remove default LilyPond tagline
  tagline = \date
  copyright = "1996 De Haske Publications, BV"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo "Dance" 4=140
  \compressFullBarRests
  \override MultiMeasureRest #'expand-limit = 1
}

\include "parts/comeonletsdance-flute.ly"

\score { \new Staff {
  \clef treble
  \key c \major
  \relative c' {d1_\markup{improvisation row} \parenthesize e f g a c d}
  }
  \layout {indent = 0.0\cm}
}

\score {
  \new Staff \flute
  \layout { }
}
