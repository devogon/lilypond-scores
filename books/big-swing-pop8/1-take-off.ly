\version "2.19.82"
\language "nederlands"

date = \markup {\tiny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Take Off"
  subtitle = "(Big Swing-Pop)"
  instrument = "Piano/Keyboard"
  arranger = "Fons van Gorp"
  piece = "1"
  copyright = "1996 De Haske Publications, BV"
  % Remove default LilyPond tagline
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 4/4
  \tempo "Disco" 4=126
}

flute = \relative c' {
  \global
  % Music follows here.
  R1 | % 1
  R1 | % 2
  R1 | % 3
  \break
  R1 | % 4
  \repeat volta 2 {
    d8\tenuto\p e4\tenuto f8\tenuto\< ~ f d4\tenuto g8\!\sfp-> ~ | % 5
    g2. f4\staccato |% 6
    \break
    d8\tenuto\p e4\tenuto f8\tenuto\< ~ f d4.\tenuto | % 7
    as'8\!\sfp g ~ g2. | % 8
    d8\tenuto\p e4\tenuto f8\tenuto\< ~ f d4.\tenuto | % 9
    \break
    as'8\!\sfp g4. ~ g8 f ( g a!\tenuto\< ~ | % 10
    a2 ~ a8) c ( g a\!-> ~ | % 11
  }
  \alternative {
    {a2) r \break}
    {a4. a8\f ( c d c d-> ~ }
  }
    d2 ~ d8) c  (a f) | % 14
    g8\staccato g ( f g ~ g4) r | % 15
    \break
    d'2-> ~ d8 c ( a f) | % 16
    g8\staccato g ( a g ~ g4) r | % 17
    d'2-> ~ d8 c ( a f) | % 18
   \break
    g8\staccato g ( f g-> ~ g ) f ( g a-> ~ | % 19
    a8) f (g a-> ~ a) f ( g a-> ~ | % 20
    a2.) r4 | % 21
  \break
    r1 | % 22
    r4 r8 d,8\f f (d) c'\tenuto_\markup{\italic rit.} b!-> ~ | % 23
    b1\fermata
    \bar "|."
}

chordNames = \chordmode {
  \global
  % Chords follow here.
  s1
  s1
  s1
  s1
  d2:m s4.
  bes8:7
  s1
  d1:m
  bes1:7
  d1:m
  bes2:7 s4. a8:sus4
  s2 s4. a8
  s1
  s2. s8 d8:m
  s1
  g1
  d1:m
  g1
  d1:m
  g2 s4. a8:sus4
  s2 s4. a8
  s1
  s1
  s2 s4. as8:7.10-
  s1
}

right = \relative c'' {
  \global
  % Music follows here.
  r1\p\< | % 1
  r1 | % 2
  r1 | % 3
  r1 | % 4
  \repeat volta 2 {
    <d, a>2\!\p ~ <d a>4. <g d as>8-> ~ | % 5
    <g d as>1 | % 6
    <d a>1\p | % 7
    <g d as>1-> | % 8
    <d a>1 | % 9
    <g d as>2-> ~ <g d as>4. <a d,>8\< ~ | % 10
    <a d,>2 ~ <a d,>4. <a cis,>8->\!\f ~ | % 11
  }
  \alternative {
    {<a c,>2 r2}
    {<a cis,>2. ~ <a cis,>8 <a f d>8->\f ~ }
  }
  <a f d>1 | % 14
  <g-4 b,!-1>4 <g b,> <g b,> <g b,> | % 15
  <a f d>1-> | % 16
  <g b,!>4 <g b,!> <g b,!> <g b,!> | % 17
  <a f d>1-> | % 18
  <g b,!>4 <g b,!> <g b,!>4. <a d,>8-> ~ | % 19
  <a d,>4. <a d,>8-> ~ <a d,>4. <a cis,>8-> ~ | % 20
  <a cis,>2. r4 | % 21
  r1 | % 22
  r2 r4 r8 <b! f c>8-> ~ | % 23
  <b f c>1

}

left = \relative c, {
  \global
  % Music follows here.
  a8-3^\markup{a} a8-2 a8-1 a8-3 a8-2 a8-1 \repeat unfold 22 {a8} a8\f a8 a8 a8
  \repeat volta 2 {
    d4\tenuto d4\tenuto d4\tenuto d4\tenuto | % 5
    bes4\tenuto bes4\tenuto bes4\tenuto bes4\tenuto  | % 6
    d4\tenuto d4\tenuto d4\tenuto d4\tenuto | % 7
    bes4\tenuto bes4\tenuto bes4\tenuto bes4\tenuto  | % 8
    d4\tenuto d4\tenuto d4\tenuto d4\tenuto | % 9
    bes4\tenuto bes4\tenuto bes4\tenuto bes4\tenuto  | % 10
    a4\tenuto a4\tenuto a4\tenuto a4\tenuto | % 11
  }
  \alternative {
    {a4\tenuto a4\tenuto a4\tenuto a4\tenuto }
    {a4\tenuto a4\tenuto a4\tenuto a4\tenuto }
  }
  d8-3 d8-2 d8-1 d8-3 d8-2 d8-1 d8-3 d8-2 | % 14
  g8-1 g8-3 g8-2 g8-1 g8-3 g8-2 g8-1 g8-2 | % 15
  d8-3 d8 d8 d8 d8 d8 d8 d8 | % 16
  \repeat unfold 8 {g8} | % 17
  \repeat unfold 8 {d8} | % 18
  g8 g8 g8 g8 g8 g8 g8 a8 | % 19
  a8 a8 a8 a8 a8 a8 a8 a8 | % 20
  a8 a8 a8 a8 a8 a8 a4 | % 21
  d,8 d8 d8 d8 d8 d8 d8 d8 | % 22
  d8 d8 d8 d8 d4 r8 <es' as,>8-> ~ | % 23
  <es as,>1 | % 24
  \bar "|."
}

flutePart = \new Staff \with {
  instrumentName = "Flute"
 % shortInstrumentName = "Fl."
} \flute

chordsPart = \new ChordNames \chordNames

pianoPart = \new PianoStaff \with {
  instrumentName = "Piano"
%  shortInstrumentName = "Pno."
} <<
  \new Staff = "right" \right
  \new Staff = "left" { \clef bass \left }
>>

\score {
  <<
    \flutePart
    \chordsPart
    \pianoPart
  >>
  \layout { }
}
