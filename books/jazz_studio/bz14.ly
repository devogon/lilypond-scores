\version "2.19.36"
\language "nederlands"

\header {
  title = "Jazz Studio"
  subtitle = "bz 14"
  composer = "John Fischer"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 2/2
 % \tempo 4=120
}

scoreAGlobal = {
  \key f \major
  \time 2/2
  \partial 4
  %\tempo 4=120
}

scoreAContrabass = \relative c {
  \scoreAGlobal
  % Music follows here.
  d8 f
  f4 bes-+
  a8 f ~ f4
  e8 f-+ g e c4 a'8-+ f (
  f2) r8 c d f
  bes8-+ a f c bes-+ c ~ c4
  \break
  d'4 d d8 c a f
  d'8 d4 d8 r4 c8 b! (
  bes2) g8 a-+ f e (
  e4) r r2
  \break
  c'4 c c8 a f e-+
  es4 d'8-+ c ~ c4 r
  c4-+ c-+ c8-+ bes f d
  des4 c'8-+ bes ~ bes4 r
  \break
  a4 bes-+ a8 f ~ f4
  e8 f g e c4 a'8-+ f (
  f4) a8 c bes-+ a f c
  as'8-+ f g f ~ f2
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreAContrabass }
  \header {
    piece = \markup{\circle{22}“Careless love (Solo)”}
  }
  \layout { }
  \midi { }
}

scoreBContrabass = \relative c {
  \global
  % Music follows here.
  bes'4 a-+ g f
  es4-+ d c-+ d
  bes4 d g f
  a4-+ bes as d,
  es4 as-+ g c
  bes4 g es f-+
  \break
  bes,4 d es-+ c-+
  d4 f g g,
  c4 es g-+ f
  es4 d-+ c f
  bes8 d,4. es4-+ f
  g4 a8-+ bes ~ bes4 r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreBContrabass }
  \header {
    piece = \markup{\circle{23}“Blues (begleitung)”}
  }
  \layout { }
  \midi { }
}

scoreCGlobal = {
  \key f \major
  \time 2/2
 % \tempo 4=120
}

scoreCContrabass = \relative c {
  \scoreCGlobal
  % Music follows here.
  f4 e-+ d c
  bes4 g c e
  f4 a, bes-+ c
  f,4 g-+ a c
  f4 g-+ a g-+
  \break
  fis4 e-+ d fis
  g4 a-+ b! a-+
  g4 f-+ e c
  f4 g-+ a c
  f,4 es d-+ c
  \break
  bes c-+ d f
  bes,4 c-+ des bes
  a4 c bes-+ a
  g4 bes a-+ g
  f4 a bes-+ c
  f4 c f, r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreCContrabass }
  \header {
    piece = \markup{\circle{24}“Careless love (begleitung)”}
  }
  \layout { }
  \midi { }
}
