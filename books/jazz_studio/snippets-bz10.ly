\version "2.19.36"
\language "nederlands"

\header {
  title = "Jazz Studio"
  subtitle = "bz 10"
  composer = "John Fischer"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0.0\cm
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key f \major
  \time 2/2
%  \tempo 4=120
}

scoreAContrabass = \relative c {
  \global
  % Music follows here.
  f4 r c r
  g'4 r c, r
  f4 r c r
  f4 r a c
  f,4 r c r
  \break
  d4 r fis r
  g4 r d r
  c4 r e g
  f4 r c r
  f4 r es c
  bes4 r f' r
  \break
  bes4 r bes, r
  f'4 r c r
  g'4 r c, e
  f4 d c a
  f4 r f r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreAContrabass }
  \header { piece = \markup {\circle{9}"Careless love (Begleitung)"} }
  \layout { }
  \midi { }
}

scoreBContrabass = \relative c {
  \global
  % Music follows here.
  f4 a c a
  g bes c, e
  f d c a
  f a c d
  f f c f
  \break
  d fis a d,
  g g d g
  c, e g c,
  f f c c
  f es c f
  bes, d f g
  \break
  bes g f des
  c c f f
  g g c, e
  f d c a
  f c' f r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreBContrabass }
  \header { piece = \markup {\circle{10}"(walking bass)"} }
  \layout { }
  \midi { }
}

scoreCContrabass = \relative c {
  \global
  % Music follows here.
  a'4 c f,2
  r4 g bes e,
  f2 c4 r
  r f a c
  d4. c8 a4 f
  \break
  fis4 a d, fis
  g2 f!4 d
  e g c, r
  r f a c
  es,2 f4 es
  \break
  d4 f bes  r
  r des,4 f bes
  a c f,2
  r4 g bes e,
  f2 d4 c
  f4 c f, r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreCContrabass }
  \header { piece = \markup {\circle{11}"Careless love (solo)"} }
  \layout { }
  \midi { }
}

scoreDContrabass = \relative c {
  \global
  % Music follows here.
  bes4 f' g f
  bes4 f g f
  bes,4 d d f
  bes f as bes8. bes,16
  e4 bes' c bes
  e,4 des'4 bes g
  \break
  bes4 g f g
  bes f d bes
  f'4 f a c
  es4 c a f8. a16
  bes4 g f d
  bes4 f' bes, r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreDContrabass }
  \header { piece = \markup {\circle{12}"Blues (Begleitung)"} }
  \layout { }
  \midi { }
}
