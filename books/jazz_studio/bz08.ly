\version "2.19.36"
\language "nederlands"

\header {
  title = "Jazz Studio"
  subtitle = "bz 08"
  composer = "John Fischer"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  indent = 0
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key bes \major
  \time 2/2
%  \tempo 4=120
}

scoreAContrabass = \relative c {
  \global
  % Music follows here.
  d4 f bes bes
  d,4 f bes r
  d,4 f bes bes
  d,4 f bes r
  es,4 g bes bes
  es,4 g bes r
  \break
  d,4 f bes bes
  d,4 f bes r
  f4 a c c
  f,4 a c r
  d,4 f bes bes
  d,4 f bes r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreAContrabass }
  \header {
    piece = \markup{\circle{1}“Blues mit Riff-melodie”}
  }
  \layout { }
  \midi { }
}


scoreBContrabass = \relative c {
  \global
  % Music follows here.
  bes4 r f' r
  bes r f r
  bes, r f' r
  bes r f d
  es r bes' r
  g r es r
  \break
  bes r f' r
  d r bes r
  f' r c' r
  a r f r
  bes, r f' r
  bes f bes, r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreBContrabass }
  \header {
    piece = \markup{\circle{2}“Blues (begleitung)”}
  }
  \layout { }
  \midi { }
}

scoreCContrabass = \relative c {
  \global
  % Music follows here.
  bes'4 bes f f
  bes,4 bes f' f
  bes4 bes f f
  bes,4 bes bes' bes
  es,4 es bes bes
  es4 es bes' bes
  \break
  bes,4 bes f' f
  bes4 f d bes
  f'4 f c' c
  f,4 f c f
  bes,4 bes f' f
  bes4 f bes, r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreCContrabass }
  \header { piece = \markup{\circle{3}} }
  \layout { }
  \midi { }
}

scoreDContrabass = \relative c {
  \global
  % Music follows here.
  bes4 r d r
  f4 r bes r
  bes,4 r d r
  f4 r d bes
  es4 r bes r
  es4 r g bes
  \break
  bes,4 r d r
  f4 r d bes
  f'4 r c r
  f4 r c f
  bes,4 r d f
  bes4 f bes, r
  \bar "|."
}

\score {
  \new Staff \with {
    midiInstrument = "contrabass"
  } { \clef bass \scoreDContrabass }
  \header {
    piece = \markup{\circle{4}}
  }
  \layout { }
  \midi { }
}