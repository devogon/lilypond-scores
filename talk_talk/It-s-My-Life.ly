\version "2.22.0"

date = \markup {\teeny{Engraved by daniël b. on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "It‘s My Life"
  subtitle = "Talk Talk - 1984 - It‘s My Life"
  tagline = \date
}

\paper {
  indent = 0.0\cm
}

global = {
  \key c \major
  \time 4/4
  \tempo 4=124
}

chordNames = \chordmode {
  \global
  % Chords follow here.
}

electricBass = \relative c, {
  \global
    \omit Voice.StringNumber
  %\compressFullBarRests
  % Music follows here.
   %\compressMMRests
   \compressEmptyMeasures
   \override MultiMeasureRest #'expand-limit = 1
  \textLengthOn
  R1*8^\markup { [Drums] } %  \mark \markup{\box Drums}
  R1*7^\markup { [Synth] }
  \textLengthOff
  \break
  r2 r4. a8\4 ( |
  \repeat volta 4 {
    a8\4) c d\3 c d4\staccato\3 d8\3 a\4 ( |
  }
  \alternative {
    { g8) a\4 c a\4 c4\staccato c8 a\4 }
    { g8\4^\markup{fix slur to here} a\4 c a\4 c4\staccato r8 f, (}
  }
  f1 )(
  f )
  \key es \major
  \bar "||"
  \break
  \mark \markup{ [Verse] }
  \repeat volta 2 {
    r4 es'4\3\staccato d8\3 es\3 r bes\4 |
    r4 es4\3\staccato d8\3 es\3 r f |
    r4 f\staccato es8\3 f r as\2 |
    r4 as4\2\staccato f8 as\2 r bes\1 |
    \break
    r4 es,4\3\staccato d8\3 es\3 r bes\4 |
    r4 es4\3\staccato d8\3 es\3 r f |
    r4 f\staccato es8\3 f r as\2 |
  }
  \alternative {
    {r4 as4\2\staccato f8 as\2 r f |}
    {r4 as\2\staccato f8 as\2 as\2 f |}
  }
  ais4\2\staccato e!\staccato\3 d8\3 e\3 r e\3 |
  r8 es\3 d\3 bes\4 e,!\4 fis\4 a\4 bes\4 |
  \key c \major
  \break
  a8\4 d\3 e\3 a\2 r g\2 e\3 g, ( |
  g) a\4 b\3 c r c r f, ( |
  f8) c' d\3 f (f) d\3 c f, ( |
  f8) c' c4\staccato f8 c c4\staccato |
  f,8 c' d\3 f g, d'\3 e a,8\4 (|
  \bar "||"
  \break
  \mark \markup{ [Chorus] }
  \repeat volta 4 {
    a8\4 )
  }
  
}

chordsPart = \new ChordNames \chordNames

electricBassPart = \new StaffGroup <<
  \new Staff { \clef "bass_8" \electricBass }
  \new TabStaff \with {
    stringTunings = #bass-tuning
  } \electricBass
>>

\score {
  <<
    \chordsPart
    \electricBassPart
  >>
  \layout { }
}
