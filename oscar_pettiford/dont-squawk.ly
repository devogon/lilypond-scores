\version "2.19.55"
\language "nederlands"

\header {
  title = "Don‘t Squawk"
  instrument = "Bass"
  composer = "bassist: Oscar Pettiford"
  % Remove default LilyPond tagline
  tagline = "93"
}

% bassist: oscar pettiford
% title: don't squawk
% record: Oscar Pettiford
% lable: Bethlehem Records
% blues

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}
global = {
  \key f \major
  \time 4/4
  \tempo 4=112
  \compressFullBarRests
}

contrabass = \relative c {
  \global
  %    \chords { c2 g:sus4 f e }
  % Music follows here.
  R1*4_\markup{\bold {intro - flute solo}}
  \break

  f4_\markup{\italic \bold "F7"} a, c f | % 1
  bes,4_\markup{\italic \bold {B\flat7}}	 d f bes,8 g' | % 2
  f4_\markup{\italic \bold F7} a, c g' | % 3
  f4 a c f,8 g | % 4
  \break
  bes4_\markup{\italic \bold {B\flat7}} d, g f8 d | % 5
  bes4 f' d bes8 c | % 6
  f4_\markup{\italic \bold F7} a, c_\markup{\italic \bold (C7)} d | % 7
  f4_\markup{\italic \bold F7} a d d,_\markup{\italic \bold D7} | % 8
  \break
  g4_\markup{\italic \bold Gm7} bes d d, | % 9
  g4_\markup{\italic \bold Gm7} bes c_\markup{\italic \bold C7} c, | % 10
  f4_\markup{\italic \bold F7} f d_\markup{\italic \bold (D7)} d | % 11
  g4_\markup{\italic \bold Gm7} g c,_\markup{\italic \bold C7} c | % 12
  \break
  f4_\markup{\italic \bold F7} a, c f
  bes,_\markup{\italic \bold {B\flat7}} d f bes,
  f'_\markup{\italic \bold F7} a, c g'
  f a c f,
  \break
  bes_\markup{\italic \bold {B\flat7}} d, g f
  bes, d f bes
  f_\markup{\italic \bold F7} a, c d
  f_\markup{\italic \bold F7} c d_\markup{\italic \bold D7} d'
  \break
  g,_\markup{\italic \bold Gm7} bes d g,
  c,_\markup{\italic \bold C7} g' c c,
  f_\markup{\italic \bold F7} f d_\markup{\italic \bold D7} d
  g_\markup{\italic \bold Gm7} g c,_\markup{\italic \bold C7} c
  \bar "|."
}

thechords = {
    \chords { c2 g:sus4 f e }
}

\score {
  \new Staff { \clef bass \contrabass }
  \layout { }
}

\markup {
  \teeny
  \date
}