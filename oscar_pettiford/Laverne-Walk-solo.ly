\version "2.19.57"
\language "nederlands"

date = \markup {\teeny {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Laverne Walk"
  subtitle = "Solo"
  instrument = "Contrabass"
  composer = "Oscar Pettiford"
 % poet = "as played by Pettiford"
  tagline = \date %"(Montmartre Blues 5 juli 1960)"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key g \major
  \time 4/4
%  \tempo 4=152
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabass = \relative c {
  \global
  b'8 a gis b d fis e d | % 1
  c8 a as g fis es' d c | % 2
  \tuplet 3/2 {b8 c b} bes a gis fis e d |% 3
  b8 c e g b fis r4 | %4
  \break
  g4 a8 ais b d e d | % 5
  g8 e c b ais cis fis e | % 6
  d8 e \tuplet 3/2 {b8 c cis} d2 | %7
  c8 e, g b ais fis r4 | % 8
  \break
  b4 d,8 e (e2) | % 9
  c'8 e, eis fis (fis2) | % 10
  d'8 fis, g a b c d e ( | % 11
  e4) g8 es (es2) | % 12
  \break
  d8 b \tuplet 3/2 {g8 fis! f! (} f2) | % 13
  g'8 e \tuplet 3/2 {c8 b bes(} bes2) | % 14
  d8 b g e c'4. ais8 | % 15
  b8 g r4 r2 | % 16
  \break
  e8 g a g bes16 a8. e8 g ( | % 17
  g2.) <b d f>4^\markup{\teeny "b-d-f"} | % 18
  e8 es d c b a g fis | % 19
  f!8 a, c e dis b r4 | % 20
  \break
  c4 d8 dis e g a g | % 21
  bes16 a8. e8 g (g2\glissando) | % 22
  b4 d,8 c cis4 bes'8 gis | % 23
  a8 d, r4 r2 | % 24
  \break
  d8 g b4 d4. b8 | % 25
  c8 a' (a2) c,8 ais | % 26
  b8 g' (g2) b,8 gis | % 27
  a8 fis' (fis) es16 fis d4 (d8) c16 d | % 28
  b8 d e16 d g8 (g2) | % 29
  e8 g a16 g bes8 (bes2) | % 30
  bes8 b! d, des c a'-3 f^\markup{\small d} g | % 31
  r4 b,8 a g e d c | % 32
  \break
  b8 g r4 c cis8 d | % 33
  s1^\markup{enz...}
  s1
  s1

}

\score {
  \new Staff \with {
    %instrumentName = "Contrabass"
    %shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}

%   \markup \column {
%     \line {solos: G rhythm changes with B section}
%     \hspace #1
%   }
  %\markup{\hspace #1}
% \markup \column {
%   \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
%   \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
%   \hspace #1
% }
%
% \markup {
%   \teeny
%   \date
% }