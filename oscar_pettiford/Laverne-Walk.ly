\version "2.19.57"
\language "nederlands"

date = \markup {\teeny {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}}

\header {
  title = "Laverne Walk"
  instrument = "Contrabass"
  composer = "Oscar Pettiford"
  poet = "as played by Pettiford"
%  tagline = \markup{(Montmartre Blues 5 juli 1960)\n \date}
  tagline = \date
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}



\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
  \tempo "unison with piano" 4=152
  \set Score.markFormatter = #format-mark-box-alphabet
}

contrabass = \relative c {
  \global  r4 b'8 a g e d c |
  \repeat volta 2 {
    b8 g r4 c cis8 d |
    r4 d'8 c b g d c |
    b8 g' r4 c, cis8 d |
    r4 b'8 a g e d c |
    b8 g (g4) b8 d e d |
    c8 d e g a g fis g |
    b8 g d cis cis a' fis g |
  }
  \alternative {
    {r4 b8 a g e d c}
    {r1}
  }
  a'8 g e c (c4) r |
  bes'4 a8 g r g' r g ( |
  g4) f8 e d c a ais |
  b8 g a fis e d r4 |
  a'8 g e c (c4) r |
  bes'4 a8 g (g4) r |
  b4 (d8) bes (bes4) (d8) a |
  r4 b8 a g e d c |
  b8 g r4 c4 cis8 d |
  r4 d'8 c b g d c |
  b8 g' r4 c, cis8 d |
  r4 b'8 a g e d c |
  b8 g (g4) b8 d e d |
  c8 dis e g a g fis g |
  b8 g d cis c! a' fis g |
  r1 |
  \bar "|."
}

\score {
  \new Staff \with {
    %instrumentName = "Contrabass"
    %shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}

%   \markup \column {
%     \line {solos: G rhythm changes with B section}
%     \hspace #1
%   }
  %\markup{\hspace #1}


% \markup {
%   \teeny
%   \date
% }