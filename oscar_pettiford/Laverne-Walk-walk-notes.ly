\version "2.19.57"
\language "nederlands"

\header {
  title = "Laverne Walk"
  subtitle = "walking"
  instrument = "Contrabass"
  composer = "Oscar Pettiford"
  poet = "as played by Pettiford"
  tagline = "(Montmartre Blues 5 juli 1960)"
}

\paper {
  #(set-paper-size "a4")
  indent = 0.0\cm
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

date = \markup {Engraved on #(strftime "%d-%m-%Y at %X" (localtime (current-time)))}

\include "/home/ack/src/openlily-snippets/general-tools/git-commands/definitions.ily"

global = {
  \key c \major
  \time 4/4
  \tempo 4=152
  \set Score.markFormatter = #format-mark-box-alphabet
}


contrabass = \relative c {
  \global
  \repeat volta 2 {
  g'4_\markup{\huge G} f e_\markup{\huge Em} bes
  a4_\markup{\huge Am\super{7}} es' d_\markup{\huge D\super{7}} f
  g4_\markup{\huge G} f' e_\markup{\huge Em} bes
  a4_\markup{\huge Am\super{7}} es d_\markup{\huge D\super{7}} f,
  \break
  g_\markup{\huge Dm\super{7}G} f e_\markup{\huge G} d'
  c4_\markup{\huge C\super{7}} c cis_\markup{\huge C\sharp\super{o}} cis
  d dis e bes'
  }
  \alternative {
    {a es d d}
    {g d dis d}
  }
  \break
  c' g e c
  cis f e' cis
  g f e d
  g, a bes b!
  \break
  c g e c'
  cis g' e' cis
  d, cis d bes
  a es' c as8 fis8
  \break
  g4_\markup{\huge G} f e_\markup{\huge Em} bes'
  a4_\markup{\huge Am\super{7}} es' d_\markup{\huge D\super{7}} f
  g4_\markup{\huge G} f e_\markup{\huge Em} bes'
  a4_\markup{\huge Am\super{7}} es d_\markup{\huge D\super{7}} f
  \break
  g f e d
  c c cis cis
  d dis e bes
  g f e d'
 %  \chords {
%   \repeat volta 2 {
% %   g2 e2:m
%  %  a2:m7 d2:7
% %   g2 e2:m
% %   a2:m7 d2:7
%    \break
%    d2:m7 g2
%    c:7 cis:dim
%    g2:/d d2:7
%   }
%      \alternative {
%        {a2:m7 d2:7}
%        {g2:7/d d2:7}
%      }
%  %   a2:m7 d2:7
%
%  % g2:7/d d2:7
%   \break
%   c1:7
%   cis:dim
%   g2/d d2:7
%   g2: g2:7
%   \break
%   c1:7
%   cis1:dim
%   g2:/d e:m
%   a2:m d2:7
%   \break
%   g2: e2:m
%   a2:m d2:7
%   g2: e2:m
%   a2:m7 d2:7
%   \break
%  % g2: e2:m
%  % a2:m d2:7
%  % g2: e2:m
%  % a2:m7 d2:7
%   d2:m g2:
%   c2:7 cis2:dim
%   g2:/d d2:7
%   g1:
%   }
  \bar "|."
}


\score {
  \new Staff \with {
    %instrumentName = "Contrabass"
    %shortInstrumentName = "Cb."
  } { \clef bass \contrabass }
  \layout { }
}

%   \markup \column {
%     \line {solos: G rhythm changes with B section}
%     \hspace #1
%   }
  %\markup{\hspace #1}
% \markup \column {
%   \line {\typewriter \teeny  "git date: " \typewriter \teeny \gitDateTime}
%   \line {\typewriter \teeny "git commit: " \typewriter \teeny \gitCommit}
%   \hspace #1
% }

\markup {
  \teeny
  \date
}